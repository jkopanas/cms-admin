<?php
if (!defined('DB')) {
	include('init.php');
}

if ($_POST['var']) { $posted_data = form_settings_array($_POST['var']);}
$Languages = new Languages();

if ($_GET['action'] == 'LoadBox') {

	if ($boxIDs['settings']['get']['filter']) {
				$EmbedBox=$s->getBox($boxIDs['settings']['get']['filter']);
				$EmbedBox['embedid']=$boxIDs['id'];
				include($EmbedBox['file']);		
	}
	
		
	if (empty($s->boxids)) {
		$smarty->assign("settings",$boxIDs['settings']);
		$smarty->assign("mode","single");
		$s->boxsAssigns[$boxIDs['id']]= array('template'=> $smarty->fetch($boxIDs['template']), 'box'=> $boxIDs, 'a'=>$_GET);
		echo json_encode($s->boxsAssigns);
		exit;
	} else {
	
		$smarty->assign("settings",$boxIDs['settings']);
		$s->boxsAssigns[$boxIDs['id']]= array('template'=> $smarty->fetch($boxIDs['template']), 'box'=> $boxIDs, 'a'=>$_GET);
		
	}
	
return;
}

if ($_GET['action'] == 'FilterLanguages') {
	
	if (isset($_POST['filter'])) {
		
		$tables = array ("languages" => array( "pk" => "name"));
		$start = ($_POST['page']) ? ($_POST['page']*$_POST['take'])-$_POST['take'] : 0;
		$Filter = new FilterClass(array("filter" => $_POST['filter'], "tables" => $tables,"limit" =>array("start" => $start,"end" => $_POST['take'] )));

		$result = $Filter->buildquery();

		$options = array( "total" => $result['number'] );	
		$value = array(array( "text" => "common", "value" => "common"),array("text" => "admin", "value" => "admin") , array("text" => "javascript", "value" => "javascript"));
		$res =array ('data' => $result['data'] , "option" => $options , "listbox" => array( "topic" => $value) );
		
	}
	#$res = $Languages->getTranslations(array('code'=>$_POST['code'],'topic'=>$posted_data['topic'],'filter'=>$posted_data['filter'],
	#'results_per_page'=> $_POST['take'], 'page'=> $_POST['page'], 'debug'=>0));
	echo json_encode($res);
	exit();
	
}//END ACTIONS

if ($_GET['action'] == 'saveLanguageVariable') {
	$posted_data = form_settings_array($_POST['data']);
	$p = new Parse();
	foreach ($posted_data as $k => $v)
	{
		list($field,$name)=split("-",$k);
		$upd[] =  "$field = '".$p->toDB($v)."'";
		$where = "WHERE name = '$name'";
	}
	if ($upd) {
		$sql->db_Update("languages",implode(',',$upd)." $where");
//		echo "update languages SET ".implode(',',$upd)." $where";
		if (is_numeric(USE_CACHE)) {
			$memcache->delete(md5('languages'));
		}
	}
}//END ACTION

if ($_GET['action'] == 'addTranslationVariable') {
		
	$posted_data = $_POST['data'];
	$Languages->addTranslation($posted_data);
	$row=$Languages->loadSingleVariable($_POST['data']['Varname']);
	$Languages->Create_json_translations();
	
	echo json_encode($row);
	exit();
	
}//END ACTION

if ($_GET['action'] == 'LoadLanguage') {
	echo json_encode($Languages->LoadLanguage($_POST['code']));
}//END ACTION


if ($_GET['action'] == 'loadSingleVariable') {

	if (isset($_GET['uid'])) {
		$smarty->assign('var',$Languages->loadSingleVariable($_GET['name']));
		$smarty->assign("name",$_GET['name']);
		$smarty->assign("uid",$_GET['uid']);
	} else {
		$smarty->assign('var',$Languages->AvailableLanguages());
	}
	$smarty->assign("mode",'single');
	$smarty->display('admin2/lang_select.tpl');
	
}//END ACTION

if ($_GET['action'] == 'saveSingleTranslation') {

	$Languages->updateSingleTranslation($_POST['name'],$_POST['data']);
	$url =md5($_SERVER['HTTP_HOST'].'_languages');
	if (is_numeric(USE_CACHE)) {
		$memcache->delete(md5('languages'));
	}
	$Languages->Create_json_translations();
	echo json_encode(array("uid" => $_POST['uid'],"data" => $_POST['data']));
	exit();
	
}//END ACTION

if ($_GET['action'] == 'setDefaultLanguage') {
	
	if ($_GET['var'] == 'default_lang') {
		$Languages->moveTranslationData($Languages->getTablesToTranslate(),DEFAULT_LANG,$_GET['code']);
	}
	$sql->db_Update('config',"value = '".$_GET['code']."' WHERE name = '".$_GET['var']."'");

}//END ACTION



if ($_GET['action'] == 'deleteTranslationVariable') {
$sql->db_Delete("languages","name = '".$_GET['var']."'");
}//END ACTION




if ($_GET['action'] == 'deleteLanguage') {
       $countryCode = ($_GET['code']) ? $_GET['code'] : $_POST['code'];
		
        $sql->db_Delete("languages","code = '$countryCode'");
		$sql->db_Delete("countries","code = '$countryCode'");
		$sql->db_Delete("translations","code = '$countryCode'");
		
		echo $countryCode;
		exit();
}//END ACTION

if ($_GET['action'] == 'GetLang') {
	$countries=$Languages->getAllCountries(BACK_LANG);
	foreach ($countries as $key => $value) {
		$c[] = array("text" => $value['country'], "value" => $value['code']);
	}
	$lang=$Languages->AvailableLanguages(array());
	foreach( $lang as $key => $value) {
		$lang[$key]['active'] = ( $value['active'] == "Y" ) ? 1 : 0 ;
		$lang[$key]['adefault'] = (DEFAULT_LANG_ADMIN == $value['code']) ? 1 : 0 ;
		$lang[$key]['sdefault'] = (DEFAULT_LANG == $value['code']) ? 1 : 0 ;
	}
	$langs['data'] = $lang;
	$langs['listbox'] = array( "country" => $c,"active" => array(array("text" => "Nai", "value" => 1 ),array("text" => "Oxi", "value" => 0 )) );
	echo json_encode($langs);
	exit();	
}


if ($_GET['action'] == 'addNewLanguage') {
	$posted_data = $_POST['data'] ;
	$insert=$Languages->addNewLanguage($posted_data);
	$return_lang = array_merge($insert, array("id" => $_POST['id']));
	echo json_encode($return_lang);
	exit();	
}//END ACTION


if ($_GET['action'] == 'setLanguageAvailability') {
		list($field,$code)=split("-",$_GET['var']);
		$sql->db_Update("countries","active = '".$_GET['active']."' WHERE code = '$code'");
//		echo "active = '".$_GET['active']."' WHERE code = '$code'";
}//END ACTION

if ($_GET['action'] == 'saveContentTranslations'){

		$posted_data = $_POST['data'];
		if ($_GET['itemid'] AND is_numeric($_GET['itemid'])) {
			$q = "AND (settings LIKE '%\"field\":\"itemid\"%' AND settings LIKE CONCAT('%\"value\":',".$_GET['itemid'].",'%'))";
		}
		$sql->db_Select("translations","id,field,code","itemid = ".$_GET['id']." AND module = '".$_GET['module']."' $q");
		$existing = execute_multi($sql);
		

		foreach ($posted_data as $k=>$v)
		{
			list($field,$code)=split("-",$k);
			$tmp = explode('@',$field);
			$tables[$field] = $v['table'];
			$new[$code][$field] = $v['value'];
		}

		if ($existing) {
			foreach ($existing as $v)//BEGIN UPDATING EXISTING
			{
				if ($new[$v['code']][$v['field']]) {
	//				echo "UPDATE translations SET translation = '".$new[$v['code']][$v['field']]."' WHERE id = ".$v['id']."<br>";
					$sql->db_Update("translations","translation = '".$new[$v['code']][$v['field']]."' WHERE id = ".$v['id']);	
					unset($new[$v['code']][$v['field']]);
				}
				else {//EMPTY FIELDS. NEVER SUBMITED THUS THE USER WANTS TO CLEAN THEM
					$cleanUp[] = $v['id'];
				}
			}
			if ($cleanUp) {
				$sql->db_Delete("translations","id IN (".implode($cleanUp).")");
			}
		}

		foreach ($new as $k=>$v)//BEGIN INSERTING NEW
		{
			if ($v AND $_GET['id']) {
				$date_added = time();
				foreach ($v as $key=>$value) {
				$table = $tables[$key];
				if ($_GET['itemid']) {
					$settings = json_encode(array('extraField'=>array('field'=>'itemid','value'=>$_GET['itemid'])));
				}
				$inserts[] = "'','$k','".$_GET['id']."','$table','$key','".$_GET['module']."','$value','$date_added','$date_added','1','$settings'";
				}//END LOOP FIELDS
			}//END IF VALUES
		}//END  LOOP LANGUAGES

		if ($inserts) {
            $sql->db_Insert('translations', implode("),(",$inserts) );
			//mysql_query("INSERT INTO translations VALUES ".implode(",",$inserts));
		}
}//END ACTION

if ($_GET['action'] == 'saveTranslateEfield') {
		$posted_data = $_POST['data'];
		$original_data = $_POST['originalData'];
		foreach ($posted_data as $k=>$v)
		{
			list($field,$code)=split("-",$k);
			$tmp = explode('@',$field);
			$tables[$tmp[1]] = $tmp[0];
			$field = $tmp[1];
			$new[$code][$field] = $v;
			
		}
}//END ACTION

if ($_GET['action'] == 'translateEfield') {
	$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
	for ($i=0;count($availableLanguages) > $i;$i++)
	{
		$availableLanguages[$i]['item'] = $Languages->itemTranslations($_GET['fieldid'],$availableLanguages[$i]['code'],$_GET['module'],
		array('groupby'=>'field','debug'=>0,'table'=>'extra_fields'));
	}
    
	$smarty->assign("availableLanguages",$availableLanguages);//WE WANT FRONT END ONLY
	$smarty->assign("currentLanguage",$availableLanguages[0]);//WE WANT FRONT END ONLY
	$smarty->assign("defaultLanguage",$Languages->getDefaultLanguage('default_lang',array('return'=>'array','languageDetails'=>1)));//WE WANT FRONT END ONLY
	$smarty->assign("fieldid",$_GET['fieldid']);
	$smarty->assign("mode",'translateEfield');
	$smarty->display('admin/lang_select.tpl');
}//END ACTION


if ($_GET['action'] == 'translateEfieldGroup') {
	$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
	for ($i=0;count($availableLanguages) > $i;$i++)
	{
		$availableLanguages[$i]['item'] = $Languages->itemTranslations($_GET['groupid'],$availableLanguages[$i]['code'],$_GET['module'],
		array('groupby'=>'field','debug'=>0,'table'=>'extra_fields_groups'));
	}
    
	$smarty->assign("availableLanguages",$availableLanguages);//WE WANT FRONT END ONLY
	$smarty->assign("currentLanguage",$availableLanguages[0]);//WE WANT FRONT END ONLY
	$smarty->assign("defaultLanguage",$Languages->getDefaultLanguage('default_lang',array('return'=>'array','languageDetails'=>1)));//WE WANT FRONT END ONLY
	$smarty->assign("groupid",$_GET['groupid']);
	$smarty->assign("mode",'translateEfieldGroup');
	$smarty->display('admin/lang_select.tpl');
}//END ACTION

if ($_GET['action'] == 'saveTranslateEfieldGroup') {
		$posted_data = $_POST['data'];
		$original_data = $_POST['originalData'];
		foreach ($posted_data as $k=>$v)
		{
			list($field,$code)=split("-",$k);
			$tmp = explode('@',$field);
			$tables[$tmp[1]] = $tmp[0];
			$field = $tmp[1];
			$new[$code][$field] = $v;
			
		}
}//END ACTION


if ($_GET['action'] == 'translateEfieldValues') {
	$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
	for ($i=0;count($availableLanguages) > $i;$i++)
	{
		$availableLanguages[$i]['item'] = $Languages->itemTranslations($_GET['fieldid'],$availableLanguages[$i]['code'],$_GET['module'],
		array('groupby'=>'field','debug'=>0,'table'=>'extra_field_values','extraField'=>array('field'=>'itemid','value'=>$_GET['itemid'])));
	}
    
	$Efields = new ExtraFields(array('module'=>$loaded_modules[$_GET['module']],'debug'=>0));
	$smarty->assign("field",$Efields->ExtraField($_GET['fieldid']));
	$smarty->assign("itemid",$_GET['itemid']);//WE WANT FRONT END ONLY
	$smarty->assign("availableLanguages",$availableLanguages);//WE WANT FRONT END ONLY
	$smarty->assign("currentLanguage",$availableLanguages[0]);//WE WANT FRONT END ONLY
	$smarty->assign("defaultLanguage",$Languages->getDefaultLanguage('default_lang',array('return'=>'array','languageDetails'=>1)));//WE WANT FRONT END ONLY
	$smarty->assign("fieldid",$_GET['fieldid']);
	$smarty->assign("mode",'translateEfieldValues');
	$smarty->display('admin/lang_select.tpl');
}//END ACTION

if ($_GET['action'] == 'translateRoomValues') {
    
    	$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
	for ($i=0;count($availableLanguages) > $i;$i++)
	{
		$availableLanguages[$i]['item'] = $Languages->itemTranslations($_GET['fieldid'],$availableLanguages[$i]['code'],'rooms',
		array('groupby'=>'field','debug'=>0,'table'=>'rooms'));
	}
    
    
    
	$smarty->assign("availableLanguages",$availableLanguages);//WE WANT FRONT END ONLY
	$smarty->assign("currentLanguage",$availableLanguages[0]);//WE WANT FRONT END ONLY
	$smarty->assign("defaultLanguage",$Languages->getDefaultLanguage('default_lang',array('return'=>'array','languageDetails'=>1)));//WE WANT FRONT END ONLY
	$smarty->assign("roomid",$_GET['fieldid']);
	$smarty->assign("mode",'translateRoomValues');
    $smarty->display('admin/lang_select.tpl');
}

if($_GET['action']=='saveRoomTranslations')
{
    $posted_data = form_settings_array($_POST['data']);
    
    //print_r($posted_data);
    
    foreach ($posted_data as $k=>$v)
    {
    	list($field,$code)=split("-",$k);
    	$tmp = explode('@',$field);
    	$tables[$tmp[1]] = $tmp[0];
    	$field = $tmp[1];
    	$new[$code][$field] = $v;
    	
    }
    
    $sql->db_Select("translations","id,field,code","itemid = ".$_GET['id']." AND module = 'rooms'");
    $existing = execute_multi($sql);
        
    if ($existing) {
		foreach ($existing as $v)//BEGIN UPDATING EXISTING
		{
			if ($new[$v['code']][$v['field']]) {
//				echo "UPDATE translations SET translation = '".$new[$v['code']][$v['field']]."' WHERE id = ".$v['id']."<br>";
				$sql->db_Update("translations","translation = '".$new[$v['code']][$v['field']]."' WHERE id = ".$v['id']);
				unset($new[$v['code']][$v['field']]);
			}
			else {//EMPTY FIELDS. NEVER SUBMITED THUS THE USER WANTS TO CLEAN THEM
				$cleanUp[] = $v['id'];
			}
		}
		if ($cleanUp) {
			$sql->db_Delete("translations","id IN (".implode($cleanUp).")");
		}
    }
    
	foreach ($new as $k=>$v)//BEGIN INSERTING NEW
	{
		if ($v AND $_GET['id']) {
			$date_added = time();
			foreach ($v as $key=>$value) {
			$table = $tables[$key];
			
			$inserts[] = "'','$k','".$_GET['id']."','rooms','$key','rooms','$value','$date_added','$date_added','1','$settings'";
			}//END LOOP FIELDS
		}//END IF VALUES
	}//END  LOOP LANGUAGES
	if ($inserts) {
	   foreach($inserts as $insert)
        {
            $sql->db_Insert('translations', $insert );
        }
        //$sql->db_Insert('translations', implode(",",$inserts) );
		//mysql_query("INSERT INTO translations VALUES ".implode(",",$inserts));
	}
       /*
        print_r($existing);
        echo '<br/>';
        print_r($tables);
        echo '<br/>';
        print_r($field);
        echo '<br/>';
        print_r($new);
        */
     
        
}

?>