<?php
include("../manage/init.php");


if($_GET['action']=='addCredits')
{
    $bill = new billing();
    $bill->addUserCredits($_POST['data']['uid'],$_POST['data']['credits']);
    
}

if($_GET['action']=='getusers')
{
    $user = new user();
    $data = $_POST;
	$data['secondaryData']['debug'] = 0;
	$data['secondaryData']['return'] = 'multi';
	$data['secondaryData']['results_per_page'] = 10;
	$data['likeData']['uname'] = $_POST['uname'];
	$users = $user->searchUsers($user->setupSearchData($data));
	$i=0;
	foreach ($users as $v)
	{
		$tmp[$i]['id'] = $v['id'];
		$tmp[$i]['label'] = $v['uname'];
		$tmp[$i]['value'] = $v['uname'];
		$i++;
	}
	echo json_encode($tmp);
	exit();
}

if ($_GET['action'] == 'search') {
	$billing = new billing();
    //print_r($_POST);
	$posted_data = $_POST['data'];
	$secondaryData = $_POST['secondaryData'];
	$secondaryData['searchFieldsLike'] = $_POST['likeData'];
	
	if ($secondaryData['custom_field'] AND $secondaryData['custom_value']) {
		$secondaryData['searchFieldsLike'][$secondaryData['custom_field']]=$secondaryData['custom_value'];
	}
    
	if ($secondaryData['missing']) {
		$searchFilters['missing'] = $secondaryData['missing'];
	}
    
	$secondaryData['searchFields'] = $posted_data;
    
    if ($secondaryData['rangeActive']) {
		  $secondaryData['searchFieldBETWEEN']['creditsChange'] = $secondaryData['amountFrom']." AND ".$secondaryData['amountTo'];
    }
    
	if($secondaryData['dateFrom'])
	{
		$date = explode("/",$secondaryData['dateFrom']);
		
		$date = mktime(0,0,0,$date[1],$date[0],$date[2]);
		
		$secondaryData['greaterThan']=array('TransDate'=>$date);	
		unset($secondaryData['dateFrom']);	
	}
	
	if($secondaryData['dateTo'])
	{
		$date = explode("/",$secondaryData['dateTo']);
		
		$date = mktime(0,0,0,$date[1],$date[0],$date[2]);
		
		$secondaryData['lessThan']=array('TransDate'=>$date);
		unset($secondaryData['dateTo']);	
	}
	
	$secondaryData['filters'] = $searchFilters;
	$secondaryData['debug'] = 0;
	$secondaryData['return'] = 'paginated';
    $secondaryData['fields'] = 'billing.credits,billingTransactions.id,billingTransactions.transDate,users.uname,billingTransactions.creditsChange,billingTransactions.prevCredits';
	$secondaryData['page'] = ($_GET['page']) ? $_GET['page'] : 1;
	
    $result = $billing->searchTransactions($secondaryData);

	$smarty->assign("items",$result);
	
	$smarty->display("modules/billing/admin/resultsTable.tpl");
	
	exit();
}//END SEARCH

?>