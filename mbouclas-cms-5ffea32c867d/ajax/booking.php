<?php
include("../manage/init.php");

$book = new bookings();
//print_r($_POST);
if ($_GET['action'] == 'edit') {
    $parent = $_GET['parent'];
    if(is_numeric($_GET['id']))
    {
        $room = $book->getRoom($_GET['id']);
        $smarty->assign('item',$room);
    }
    else
    {
        $room['listing_id']=$parent;
        $smarty->assign('item',$room);
    }
    
    $smarty->display('modules/listings/admin/rooms/form.tpl');
}

if ($_GET['action'] == 'create') {
    
    $book->createRoom($_POST['data']);
    $smarty->assign('rooms',$book->getRooms($_POST['data']['listing_id']));
    $smarty->display('modules/listings/admin/rooms/list.tpl');
}

if ($_GET['action'] == 'update') {
    $book->updateRoom($_POST['data']);
    $smarty->assign('rooms',$book->getRooms($_POST['data']['listing_id']));
    $smarty->display('modules/listings/admin/rooms/list.tpl');
}

if ($_GET['action'] == 'delete') {
    $book->deleteRoom($_POST['id']);
    $smarty->assign('rooms',$book->getRooms($_POST['parent']));
    $smarty->display('modules/listings/admin/rooms/list.tpl');
}




?>