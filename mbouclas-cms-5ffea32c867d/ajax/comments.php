<?php
include("../manage/init.php");

$posted_data = $_POST['var'];
$comments = new UserComments();

if($_GET['action']=="addComment")
{
    $comments->addComment($posted_data);   
}

if($_GET['action']=="updateComment")
{
    echo $posted_data['comment'];
    $comments->updateComment($posted_data);
}

if($_GET['action']=="deleteComment")
{
    $comments->removeComment(array('id'=>$_GET['id']));
}

if($_GET['action']=='search')
{
    $settings=array('searchFields'=>array('itemid'=>$posted_data['itemid'],'module'=>$posted_data['module']),'getUserDetails'=>1,'orderby'=>'date_added','way'=>'desc','return'=>'paginated','results_per_page'=>5,'page'=>$_GET['page']);
    $data = $comments->getComments($settings);
    //print_r($data);
    
    $smarty->assign('commentitemid',$posted_data['itemid']);
    $smarty->assign('commentmodule',$posted_data['module']);
    $smarty->assign('commenttype',"profile");
    $smarty->assign('commentstatus',"1");
    $smarty->assign('ID',$posted_data['uid']);
    
    $smarty->assign('comments',$data);
    $smarty->display('modules/users/UserComment.tpl');
}

?>