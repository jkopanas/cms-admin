<?php
  		
if (!isset($_GET['action'])) {
		$smarty->assign("settings",$box['settings']);
		$smarty->assign("box",$box);
   		foreach ($loaded_modules as $key=>$value){
 			if ($value['is_item']==1){
 				$modules[]=$value['name'];
 			}
		 }
 
		$smarty->assign("curr_modules",$modules);
		$template = $smarty->fetch($box['template']);
		$output = array('template'=> $template, 'name' => $box['name'], 'box'=> $box, 'a'=>$_GET);
		if (!$buffer) {
			echo json_encode(array('template'=> $template, 'name' => $box['name'], 'box'=> $box, 'a'=>$_GET));
		}
}


if ($_GET['action']=='drawposts') {
	if (isset($_GET['module']) && $_GET['module']!="") {
	
		$sql->q("SELECT count( id ) as posts , FROM_UNIXTIME( `date_added` , '%M' ) as month
				FROM ".$_GET['module']." WHERE FROM_UNIXTIME( `date_added` , '%Y' ) = YEAR( CURDATE( ) )
				GROUP BY FROM_UNIXTIME( `date_added` , '%M' )");
	
				$items[$_GET['module']]=execute_multi($sql);
  
 		 	foreach ($items as $key=>$value) {
	  			if (!empty($value)) {
					foreach ($value as $k=>$v) {
						$graph[]=array("posts"=>$v['posts'],"months"=>$v['month']);
		  			}	
				}
	  		}

		echo json_encode($graph);
	  	exit;
	}
}	

if ($_GET['action']=='drawactinact') {
	
	if (isset($_GET['module']) && $_GET['module']!="") {
	
			$sql->q("SELECT count( id ) as active FROM ".$_GET['module']." WHERE active = 1");
			$active=execute_multi($sql);
		
  			$sql->q("SELECT count( id ) as inactive FROM ".$_GET['module']." WHERE active = 0");
			$inactive=execute_multi($sql);	
		
 		 	foreach ($active as $key=>$value) {
					foreach ($inactive as $k=>$v) {
						$graph[]=array("active"=>$value['active'],"inactive"=>$v['inactive']);
		  			}	
	  		}
  

		 echo json_encode($graph);
		 exit;
	}
	
}

?>