<?php
include("../manage/init.php");

$deals = new deals();

$categories = $deals->dealCategories();
foreach($categories as $category)
{
    $temp[$category['categoryid']]=$category;
}
$categories=$temp;

$shops = $deals->dealShops();
foreach($shops as $shop)
{
    $temp[$shop['id']]=$shop;
}
$shops=$temp;



if($_GET['action']=='search')
{
    $settings=array('fields'=>'*','return'=>'paginated');
    
    if(isset($_REQUEST['page']))
        $settings['page'] = $_REQUEST['page'];
    
    
    if(isset($_POST['data']['results_per_page']))
        $settings['results_per_page'] = $_POST['data']['results_per_page'];
    else
         $settings['results_per_page'] = 10;
         
    $result = $deals->searchDeals($settings);
    
    $smarty->assign('categories',$categories);
    $smarty->assign('shops',$shops);
    $smarty->assign('deals',$result);
    $smarty->display("modules/deals/admin/list.tpl");
}

?>