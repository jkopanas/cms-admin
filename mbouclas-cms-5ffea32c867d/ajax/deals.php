<?php
include("../manage/init.php");

//print_r($_POST);
if ($_GET['action'] == 'applyFilter') {
	
	$d = new deals();
    $d->tables['deals_items']='deals_items_primary';
	if (!is_array($_POST['data'])) {//CLICK FROM LINK
		$posted_data = base64_decode($_POST['data']);
		$posted_data = form_settings_array($posted_data);
		
		$data['data'][$posted_data['type']] = $posted_data['value'];
	}
	else {//CHECKBOXES
		foreach ($_POST['data'] as $k=> $v)
		{
		  
			if (is_array($v)) {
				foreach ($v as $key=> $val)
				{
					$tmp = form_settings_array(base64_decode($val));
					$filters[$tmp['type']][] = (is_numeric($tmp['value'])) ? $tmp['value'] : "'".$tmp['value']."'";
				}
			}
			else {
				$tmp = form_settings_array(base64_decode($v));
				$filters[$tmp['type']][] = (is_numeric($tmp['value'])) ? $tmp['value'] : "'".$tmp['value']."'";
			}
//			$filters[key($tmp)][] = "'".$val['value']."'";
		}//END SETUP FILTERS
		$data['data'] = $filters;
	}//END ARRAYs
 
//exit();

    
    if($filters['categoryid'])
    {   
        
        $catsettings['category']=$filters['categoryid'];
        $catsettings['group']='itemid';
        //$catsettings['extra_args']=" AND parent='0'";
        //print_r($catsettings);
        $data['data']['id']=$d->getDealsForCategories($catsettings);
        unset($filters['categoryid']);
        unset($data['data']['categoryid']);
    }
    
    if($_POST['parent']['parent'])
    {
        unset($data['data']);
        $data['data']['parent']=$_POST['parent']['parent'];
    }
    else
    {
        $data['data']['parent']=0;
    }
    //print_r($data);
	$data['secondaryData']['results_per_page'] = 10;
	$data['secondaryData']['debug'] = 0;
	$data['secondaryData']['page'] = ($_POST['page']) ? $_POST['page'] : 1;
	$data['secondaryData']['fields'] = "id,url,image,title,permalink,purchases,extract,price,discount,FROM_UNIXTIME(startDate,'%d/%m/%Y @ %H:%i') as startDate,FROM_UNIXTIME(endDate,'%d/%m/%Y @ %H:%i') as endDate,endDate as timeEnd,previous_price,(previous_price-price) as savings,@parentid := id as parentid, ( SELECT count( id ) FROM deals_items_primary WHERE `parent` = @parentid ) AS childs";
    
	$deals = $d->searchDeals($d->setupSearchData($data));
	$data['secondaryData']['debug'] = 0;
	$data['secondaryData']['return'] = 'count';
	$data['secondaryData']['fields'] = 'id';
	
    $countDeals = $d->searchDeals($d->setupSearchData($data));
	/*
    foreach ($deals as $k => $v)
	{
		$deals[$k]['description'] = strip_tags(myTruncate($v['description'],150));
	}
    */
//	echo "<br><br>----------------------<br>".count($deals)."<br>";
//	$display['items'] = $smarty->fetch('modules/themes/items.tpl');
	$display['items'] = $deals;

	
    if($data['data']['parent']!=0)
    {
        $smarty->assign("mode","child");
        
    }
    else
    {
        $smarty->assign("mode","parent");
        
    }
    
	$display['template'] = $smarty->fetch('modules/themes/items.tpl');
	$display['totalItems'] = $countDeals;
	$display['totalSources'] = count($filters['shopid']);
unset($display['items']['settings']);
/*	if ($_POST['clicked'] AND $countDeals > 0) {
		$data['secondaryData']['return'] = 'multi';
		$data['secondaryData']['fields'] = 'id';
		$data['secondaryData']['debug'] = 0;
		$deals = $d->searchDeals($d->setupSearchData($data));
		foreach ($deals as $v)
		{
			$foundIds[] = $v['id'];
		}
		switch ($_POST['clicked']) {
			case 'category':
				$reCalculate = $d->recalculateItems('md5(deals_shops.id) as id','deals_shops','id',implode(",",$foundIds));
				$display['counts']['shop'] = $reCalculate;
				break;
			case 'shop':
				
				$reCalculate = $d->recalculateItems('md5(deals_categories.categoryid) as id','deals_categories','categoryid',implode(",",$foundIds));
				$display['counts']['category'] = $reCalculate;
				break;
				
			default:
				break;
		}
		
	}*/
	echo json_encode($display);
}//END APPLY FILTER

?>