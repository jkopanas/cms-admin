<?php
if (!$_GET['mode']) return;
include($_SERVER['DOCUMENT_ROOT']."/ajax/init.php");
//LOAD SMARTY
define("DEFAULT_LANG","en");
define("SKIN_PATH",ABSPATH."/skin");
define("SKIN_URL","/skin");
define("SKIN_URL",$skin['skin_url']);
define("TEMPLATES_PATH",ABSPATH."/templates_c".$skin['skin_path']);
include(ABSPATH."/includes/smarty.php");
$maps_module = module_is_active("maps",1,1);
$smarty->assign("mapid",$_GET['mode']);
if ($_GET['mode'] == "item") 
{
	$point = get_map_items(0,$_GET['itemid'],$_GET['mapid'],'product');
	$point['item'] = get_products($_GET['itemid'],DEFAULT_LANG,$_GET['active'],1);
	$smarty->assign("point",$point);
}
if ($_GET['mode'] == "All_Items") 
{
	$locations_module = module_is_active("locations",1,1);
	$module = module_is_active("products",1,1);
	$items = array();
	$i=0;
	
$products =  get_map_items(0,0,$_GET['mapid'],'product',0,0,0,array('lang'=>DEFAULT_LANG));

	if ($products) 
	{
		for ($i=$i+1,$j=0;count($products) > $j;$i++,$j++)
		{
			$items[$i] = $products[$j];
			if ($products[$j]['settings']) 
			{
			$items[$i]['settings'] = '';
			$items[$i]['settings'] = form_settings_array($products[$j]['settings'],"###",":::");
			}
				
			$items[$i]['description'] = $products[$j]['description'];
			$items[$i]['title'] = $products[$j]['location_title'];
			$items[$i]['icontype'] = $items[$i]['settings']['icontype'];
			$items[$i]['mapZoom'] = $items[$i]['settings']['mapZoom'];
			$items[$i]['markerCategory'] = "products";
			$items[$i]['URL'] = URL."/property/".$products[$j]['itemid']."/";
			$items[$i]['mapid'] = $_GET['mapid'];
		}
	}//END PRODUCTS
	
		$locations = get_map_items(0,0,$_GET['mapid'],'location');

	if ($locations) 
	{
		
		for ($i=$i+1,$j=0;count($locations) > $j;$i++,$j++)
		{
			$items[$i] = $locations[$j];
			if ($locations[$j]['settings']) 
			{
			$items[$i]['settings'] = '';
			$items[$i]['settings'] = form_settings_array($locations[$j]['settings'],"###",":::");
			}
				
			$items[$i]['description'] = $locations[$j]['description'];
			$items[$i]['title'] = $locations[$j]['location_title'];
			$items[$i]['icontype'] = $items[$i]['settings']['icontype'];
			$items[$i]['mapZoom'] = $items[$i]['settings']['mapZoom'];
			$items[$i]['markerCategory'] = "locations";
			$items[$i]['URL'] = URL."/location/".$locations[$j]['itemid']."/";
			$items[$i]['mapid'] = $_GET['mapid'];
		}
	}//END LOCATIONS

	$smarty->assign("items",$items);
	$mode = "location";
}

if ($_GET['mode'] == "location") 
{
	$locations_module = module_is_active("locations",1,1);
	$module = module_is_active("products",1,1);
	$items = array();
	$i=0;

	
	if (!$_GET['EditMode']) 
	{
	######### ANALYZE CATEGORY SETTINGS FOR MAP ###################
	$category = get_locations_category($_GET['catid'],DEFAULT_LANG,1);
	if ($category['settings']['map_x'] AND $category['settings']['map_y']) 
	{
		$map['map_x'] = $category['settings']['map_x'];
		$map['map_y'] = $category['settings']['map_y'];
		$map['mapZoom'] = $category['settings']['mapZoom'];
		$map['description'] = $category['decription'];
		$map['icontype'] = $locations_module['settings']['cat_icontype'];
		$map['title'] = $category['category'];
		$map['selected'] = "true";
		$map['markerCategory'] = "main";
		$smarty->assign("map",$map);
		$items[$i] = $map;
	}

		
	
	############ GET PRODUCTS ###############
	$products =  get_map_items(0,0,$_GET['mapid'],'product',0,0,0,array('location_based' => 1, 'lang'=>DEFAULT_LANG,'locid'=>$_GET['catid']));

	if ($products) 
	{
		for ($i=$i+1,$j=0;count($products) > $j;$i++,$j++)
		{
			$items[$i] = $products[$j];
			if ($products[$j]['settings']) 
			{
			$items[$i]['settings'] = '';
			$items[$i]['settings'] = form_settings_array($products[$j]['settings'],"###",":::");
			}
				
			$items[$i]['description'] = $products[$j]['description'];
			$items[$i]['title'] = $products[$j]['location_title'];
			$items[$i]['icontype'] = $items[$i]['settings']['icontype'];
			$items[$i]['mapZoom'] = $items[$i]['settings']['mapZoom'];
			$items[$i]['markerCategory'] = "products";
			$items[$i]['URL'] = URL."/property/".$products[$j]['itemid']."/";
			$items[$i]['mapid'] = $_GET['mapid'];
		}
	}//END PRODUCTS
	}//END DISPLAY MODE
	
	$locations = get_map_items(0,0,$_GET['mapid'],'location',0,0,$_GET['catid']);

	if ($locations) 
	{
		
		for ($i=$i+1,$j=0;count($locations) > $j;$i++,$j++)
		{
			$items[$i] = $locations[$j];
			if ($locations[$j]['settings']) 
			{
			$items[$i]['settings'] = '';
			$items[$i]['settings'] = form_settings_array($locations[$j]['settings'],"###",":::");
			}
				
			$items[$i]['description'] = $locations[$j]['description'];
			$items[$i]['title'] = $locations[$j]['location_title'];
			$items[$i]['icontype'] = $items[$i]['settings']['icontype'];
			$items[$i]['mapZoom'] = $items[$i]['settings']['mapZoom'];
			$items[$i]['markerCategory'] = "locations";
			$items[$i]['URL'] = URL."/location/".$locations[$j]['itemid']."/";
			$items[$i]['mapid'] = $_GET['mapid'];
		}
	}

	$smarty->assign("items",$items);
}//END LOCATIONS
if ($_GET['mode'] == "content") 
{
	$content_module = module_is_active("content",1,1);
	$items = array();
	$i=0;

	######### ANALYZE CATEGORY SETTINGS FOR MAP ###################
	$category = get_content_category($_GET['catid'],DEFAULT_LANG,1);
	if ($category['settings']['map_x'] AND $category['settings']['map_y']) 
	{
		$map['map_x'] = $category['settings']['map_x'];
		$map['map_y'] = $category['settings']['map_y'];
		$map['mapZoom'] = $category['settings']['mapZoom'];
		$map['description'] = $category['decription'];
		$map['icontype'] = '/images/maps/colour125.png';
		$map['title'] = $category['category'];
		$map['selected'] = "true";
		$map['markerCategory'] = "main";
		$smarty->assign("map",$map);
		$items[$i] = $map;
	}
	
	############ GET PRODUCTS ###############
	$products =  get_map_items(0,0,$_GET['mapid'],'product');
	if ($products) 
	{
		for ($i=$i+1,$j=0;count($products) > $j;$i++,$j++)
		{
			$items[$i] = $products[$j];
			if ($products[$j]['settings']) 
			{
			$items[$i]['settings'] = '';
			$items[$i]['settings'] = form_settings_array($products[$j]['settings'],"###",":::");
			}
				
			$items[$i]['description'] = $products[$j]['description'];
			$items[$i]['title'] = $products[$j]['location_title'];
			$items[$i]['icontype'] = $items[$i]['settings']['icontype'];
			$items[$i]['mapZoom'] = $items[$i]['settings']['mapZoom'];
			$items[$i]['markerCategory'] = "products";
			$items[$i]['URL'] = URL."/property/".$products[$j]['itemid']."/";
			$items[$i]['mapid'] = $_GET['mapid'];
		}
	}
	
	$content = get_map_items(0,0,$_GET['mapid'],'content',0,0,$_GET['catid']);

	if ($content) 
	{
		
		for ($i=$i+1,$j=0;count($content) > $j;$i++,$j++)
		{
			$items[$i] = $content[$j];
			if ($content[$j]['settings']) 
			{
			$items[$i]['settings'] = '';
			$items[$i]['settings'] = form_settings_array($content[$j]['settings'],"###",":::");
			}
				
			$items[$i]['description'] = $content[$j]['description'];
			$items[$i]['title'] = $content[$j]['location_title'];
			$items[$i]['icontype'] = $items[$i]['settings']['icontype'];
			$items[$i]['mapZoom'] = $items[$i]['settings']['mapZoom'];
			$items[$i]['markerCategory'] = "content";
			$items[$i]['URL'] = URL."/location/".$content[$j]['itemid']."/";
			$items[$i]['mapid'] = $_GET['mapid'];
		}
	}

	$smarty->assign("items",$items);
}//END CONTENT
if ($_GET['mode'] == "product") 
{
	$module = module_is_active("products",1,1);
	$items = array();
	$i=0;
	######### ANALYZE CATEGORY SETTINGS FOR MAP ###################
	$category = get_category($_GET['catid'],DEFAULT_LANG,1);
	if ($category['settings']['map_x'] AND $category['settings']['map_y']) 
	{
		$map['map_x'] = $category['settings']['map_x'];
		$map['map_y'] = $category['settings']['map_y'];
		$map['mapZoom'] = $category['settings']['mapZoom'];
		$map['description'] = $category['decription'];
		$map['icontype'] = '/images/maps/colour125.png';
		$map['title'] = $category['category'];
		$map['selected'] = "true";
		$map['markerCategory'] = "main";
		$map['mapid'] = $_GET['mapid'];
		$smarty->assign("map",$map);
		$items[$i] = $map;
	}

	############ GET PRODUCTS ###############
		$products =  get_map_items(0,0,$_GET['mapid'],'product',0,0,$_GET['catid']);
		if ($products) 
		{
			for ($i=0,$j=0;count($products) > $j;$i++,$j++)
			{
				$items[$i] = $products[$j];
				if ($products[$j]['settings']) 
				{
				$items[$i]['settings'] = '';
				$items[$i]['settings'] = form_settings_array($products[$j]['settings'],"###",":::");
				}
					
				$items[$i]['description'] = $products[$j]['description'];
				$items[$i]['title'] = $products[$j]['location_title'];
				$items[$i]['icontype'] = $items[$i]['settings']['icontype'];
				$items[$i]['mapZoom'] = $items[$i]['settings']['mapZoom'];
				$items[$i]['markerCategory'] = "products";
				$items[$i]['URL'] = URL."/property/".$products[$j]['itemid']."/";
				$items[$i]['mapid'] = $_GET['mapid'];
			}
		}
		


//GET locations data
	$locations_module = module_is_active("locations",1,1);
	$locations = get_map_items(0,0,$_GET['mapid'],'location',0,0,$_GET['catid']);

	if ($locations) 
	{
		
		for ($i=$i+1,$j=0;count($locations) > $j;$i++,$j++)
		{
			$items[$i] = $locations[$j];
			if ($locations[$j]['settings']) 
			{
			$items[$i]['settings'] = '';
			$items[$i]['settings'] = form_settings_array($locations[$j]['settings'],"###",":::");
			}
				
			$items[$i]['description'] = $locations[$j]['description'];
			$items[$i]['title'] = $locations[$j]['location_title'];
			$items[$i]['icontype'] = $items[$i]['settings']['icontype'];
			$items[$i]['mapZoom'] = $items[$i]['settings']['mapZoom'];
			$items[$i]['markerCategory'] = "locations";
			$items[$i]['URL'] = URL."/location/".$locations[$j]['itemid']."/";
			$items[$i]['mapid'] = $_GET['mapid'];
		}
	}
	$smarty->assign("items",$items);
//print_r($items);
}//END PRODUCTS

if ($_GET['mode'] == "missing_items") 
{
	$type = $_GET['type'];
	if ($type == "product") 
	{
		$table = "products";
		$table_join = "page_categories";
		$item_join = "productid";
		$module = module_is_active("products",1,1);
	}
	elseif ($type == "location") 
	{
		$table = "locations";
		$table_join = "locations_page_categories";
		$item_join = "locationsid";
		$locations_module = module_is_active("locations",1,1);
	}
	elseif ($type == "content") 
	{
		$table = "content";
		$table_join = "content_page_categories";
		$item_join = "contentid";
		$content_module = module_is_active("content",1,1);
	}
	//First find all products that contain map information
	$sql->db_Select("maps_items","itemid","type = '$type' AND catid = ".$_GET['catid']);
//	echo "SELECT itemid FROM maps_items WHERE type = '$type' AND catid = ".$_GET['catid'];
	if ($sql->db_Rows()) 
	{
		$items_on_map = execute_multi($sql);
		foreach ($items_on_map as $k => $v)
		{
			$tmp[] = $v['itemid'];
		}
		$items_flat_list = implode(",",$tmp);
	}

	//Now get all the items that ARE NOT on the map
	$tables = "$table INNER JOIN $table_join ON ($table.id=$table_join.$item_join)";
	$fields = "$table.id";
	$glue = ($items_flat_list) ? "$table.id NOT IN ($items_flat_list) AND " : "";
	$q = "$glue $table_join.catid = ".$_GET['catid'];
	$sql->db_Select($tables,$fields,$q);
//	echo "SELECT $fields FROM $tables WHERE $q";
	if ($sql->db_Rows()) 
	{
		$res = execute_multi($sql);
//		print_r($res);

		foreach ($res as $v)
		{
			if ($type == 'product') 
			{
				$items[] = get_products($v['id'],DEFAULT_LANG,$_GET['active'],1);
			}
			elseif ($type == 'location')
			{
				$items[] = get_locations($v['id'],DEFAULT_LANG,$_GET['active'],1);
			}
			elseif ($type == 'content')
			{
				$items[] = get_content($v['id'],DEFAULT_LANG,$_GET['active'],1);
			}
		}
		
		$smarty->assign("items",$items);
	}
}//END OF MISSING ITEMS
###################LOCATION CATEGORIES ###########################
	if ($_GET['mode'] == 'loc_cat') 
	{
		$locations_module = module_is_active("locations",1,1);
		$category = get_locations_category($_GET['catid'],DEFAULT_LANG,1);

		if ($category['settings']['map_x'] AND $category['settings']['map_y']) 
		{
		$map['map_x'] = $category['settings']['map_x'];
		$map['map_y'] = $category['settings']['map_y'];
		$map['mapZoom'] = $category['settings']['mapZoom'];
		$map['title'] = $category['category'];
		$map['icontype'] = $locations_module['settings']['cat_icontype'];
		$map['categoryid'] = $category['categoryid'];
		$map['markerCategory'] = 'main';
		$map['mapid'] = $_GET['mapid'];
		$map['URL'] = URL."/locations/".$category['categoryid']."/";
		$smarty->assign("item",$map);
		}
	}
if ($_GET['mode'] == 'server_bounds') 
{
	$module = module_is_active("products",1,1);
	$content_module = module_is_active("content",1,1);
	$locations_module = module_is_active("locations",1,1);
list($nelat,$nelng) = explode(',',$_GET['ne']);
list($swlat,$swlng) = explode(',',$_GET['sw']);
$nelng=(float)$nelng;
$swlng=(float)$swlng;
$nelat=(float)$nelat;
$swlat=(float)$swlat;

	$sql->db_Select("maps_items","*","(map_y > $swlng AND map_y < $nelng) AND (map_x <= $nelat AND map_x >= $swlat) GROUP BY itemid");
//echo "SELECT * FROM maps_items WHERE (map_y > $swlng AND map_y < $nelng) AND (map_x <= $nelat AND map_x >= $swlat) GROUP BY itemid";
	$items = execute_multi($sql,1);
$j = 0;
if ($items) 
{
	

	foreach ($items as $k=>$v)
	{
		if ($v['type'] == 'product') 
		{
			$tmp = get_products($v['itemid'],DEFAULT_LANG,$_GET['active'],1);
			$settings = form_settings_array($v['settings'],"###",":::");
			$b[$j]['map_x'] = $v['map_x'];
			$b[$j]['map_y'] = $v['map_y'];			
			$b[$j]['description'] = $tmp['description'];
			$b[$j]['id'] = $tmp['id'];
			$b[$j]['catid'] = $tmp['catid'];
			$b[$j]['title'] = $tmp['title'];
			$b[$j]['image'] = $tmp['image_full']['thumb'];
			$b[$j]['icontype'] = $settings['icontype'];
			$b[$j]['mapZoom'] = $settings['mapZoom'];
			$b[$j]['markerCategory'] = $v['type'];
			$b[$j]['URL'] = URL."/property/".$tmp['id']."/";
			$b[$j]['mapid'] = $_GET['mapid'];
		}
		if ($v['type'] == 'content') 
		{
			$tmp = get_content($v['itemid'],DEFAULT_LANG,$_GET['active'],1);
			$settings = form_settings_array($v['settings'],"###",":::");
			$b[$j]['map_x'] = $v['map_x'];
			$b[$j]['map_y'] = $v['map_y'];
			$b[$j]['id'] = $tmp['id'];
			$b[$j]['catid'] = $tmp['catid'];
			$b[$j]['description'] = $tmp['description'];
			$b[$j]['image'] = $tmp['image_full']['thumb'];
			$b[$j]['title'] = $tmp['title'];
			$b[$j]['icontype'] = $settings['icontype'];
			$b[$j]['mapZoom'] = $settings['mapZoom'];
			$b[$j]['markerCategory'] = $v['type'];
			$b[$j]['URL'] = URL."/page/".$tmp['id']."/";
			$b[$j]['mapid'] = $_GET['mapid'];
		}
		if ($v['type'] == 'location') 
		{
			$tmp = get_locations($v['itemid'],DEFAULT_LANG,$_GET['active'],1);
			$settings = form_settings_array($v['settings'],"###",":::");
			$b[$j]['map_x'] = $v['map_x'];
			$b[$j]['map_y'] = $v['map_y'];
			$b[$j]['id'] = $tmp['id'];
			$b[$j]['catid'] = $tmp['catid'];
			$b[$j]['description'] = $tmp['description'];
			$b[$j]['image'] = $tmp['image_full']['thumb'];
			$b[$j]['title'] = $tmp['title'];
			$b[$j]['icontype'] = $settings['icontype'];
			$b[$j]['mapZoom'] = $settings['mapZoom'];
			$b[$j]['markerCategory'] = $v['type'];
			$b[$j]['URL'] = URL."/locations/".$tmp['id']."/";
			$b[$j]['mapid'] = $_GET['mapid'];
		}
		$j++;
	}//END FOREACH
	}//END ITEMS
	$smarty->assign("items",$b);
	$mode = "location";
}
	
$mode = (isset($mode)) ? $mode : $_GET['mode'];
$smarty->assign("mode",$mode);
$smarty->display("modules/maps/admin/draw_map.tpl");
?>