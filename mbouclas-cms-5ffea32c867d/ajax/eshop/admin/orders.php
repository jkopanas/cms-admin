<?php
include_once("eshop/admin/init.php");//load from eshop/admin!!!!
define("NAME_OF_INSTANCE","orders");
$jsonOutput['name'] = NAME_OF_INSTANCE;

$filter_fields = array('sort_field','sort_direction','status','results_per_page','payment_method','custom_field','custom_value','archive');//Setup the available filters
$status_codes = $eshop->getOrdersStatusCodes();

for ($i=0;count($status_codes) > $i;$i++)
{
	$codes[$status_codes[$i]['id']] = $status_codes[$i]['title'];
}

// Fetches Cart or orders by user_id
if(strlen($uid = ($_REQUEST['uid'])) > 0)
{
	$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'date_added');
	$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
	$perPage  = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
	$page = ($_REQUEST['page'] ? $_REQUEST['page'] : 1 );
	
	if($_REQUEST['fetchcart'])
	{
		$settings = array();
		$settings['status'] = ($_REQUEST['status']?$_REQUEST['archive']:0);
		$values = $eshop->getCartProductOptions($uid, $settings['status']);
	}
	else
	{
		$values = $eshop->getOrdersByUID($uid, ($_REQUEST['archive']?true:false),true,$page, $perPage, $sort_field, $sort_dir );
	}
	
	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
			"id" 					=> $uid,
			"values" 				=> json_encode($values),
			"headers" 				=> array_keys($values)
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

// Fetches Cart or orders by user_name
if(strlen($uid = ($_REQUEST['uname'])) > 0)
{
	$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'date_added');
	$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
	$perPage  = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
	$page = ($_REQUEST['page'] ? $_REQUEST['page'] : 1 );
	
	$values = $eshop->getOrdersByUname($uid, ($_REQUEST['archive']?true:false),true,$page, $perPage, $sort_field, $sort_dir );

	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
			"id" 					=> $uid,
			"values" 				=> json_encode($values),
			"headers" 				=> array_keys($values)
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

// Fetches Cart or orders by user_mail
if(strlen($uid = ($_REQUEST['umail'])) > 0)
{
	$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'date_added');
	$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
	$perPage  = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
	$page = ($_REQUEST['page'] ? $_REQUEST['page'] : 1 );
	
	$values = $eshop->getOrdersByUserMail($uid, ($_REQUEST['archive']?true:false),true,$page, $perPage, $sort_field, $sort_dir );

	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
			"id" 					=> $uid,
			"values" 				=> json_encode($values),
			"headers" 				=> array_keys($values)
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

// Fetches orders by their Status
if(strlen($orderStat = ($_REQUEST['order_status'])) > 0)
{
	$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'date_added');
	$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
	$perPage  = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
	$page = ($_REQUEST['page'] ? $_REQUEST['page'] : 1 );
	
	$values = $eshop->getOrdersByStatus($orderStat, ($_REQUEST['archive']?true:false),true,$page, $perPage, $sort_field, $sort_dir );
	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
			"values" 				=> json_encode($values),
			"headers" 				=> array_keys($values)
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

// Fetches orders by their pmethod
if(strlen($pmethod = ($_REQUEST['pmethod'])) > 0)
{
	$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'date_added');
	$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
	$perPage  = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
	$page = ($_REQUEST['page'] ? $_REQUEST['page'] : 1 );

	$values = $eshop->getOrdersByPaymentMethod($pmethod, ($_REQUEST['archive']?true:false),true,$page, $perPage, $sort_field, $sort_dir );
	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
			"values" 				=> json_encode($values),
			"headers" 				=> array_keys($values)
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

// Fetches orders by their Shipping_method
if(strlen($shipmethod = ($_REQUEST['shipmethod'])) > 0)
{
	$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'date_added');
	$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
	$perPage  = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
	$page = ($_REQUEST['page'] ? $_REQUEST['page'] : 1 );

	$values = $eshop->getOrdersByPaymentMethod($shipmethod, ($_REQUEST['archive']?true:false),true,$page, $perPage, $sort_field, $sort_dir );
	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
			"values" 				=> json_encode($values),
			"headers" 				=> array_keys($values)
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

// Updates Order's status
if ($_POST['action'] == 'update') {
	$eshop->doUpdateOrderStatus($_POST['id'], $_POST['status']);
	//header("Location: orders.php?id=".$_POST['id']);
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $_POST['id']);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}//END UPDATE

// Archive multiple orders
if ($_POST['action'] == 'archive') {

	foreach ($_POST['ids'] as $v)
	{
		$tmp[] = "'$v'";
	}
	$eshop->doArchiveOrder($tmp);

	//header("Location: orders.php");
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("ids" => $_POST['ids']);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

// Archive an order
if ($_GET['action'] == 'archive') {
	$eshop->doArchiveOrder($_GET['id']);
	//header("Location: orders.php?id=".$_GET['id']);
	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $_GET['id']);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

// Deletes multiple orders
if ($_POST['action'] == 'delete') {
	foreach ($_POST['ids'] as $v)
	{
		$tmp[] = "'$v'";
	}
	$eshop->doDeleteOrder($tmp);
	//header("Location: orders.php");
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("ids" => $_POST['ids']);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

if(is_array($_REQUEST['filter']))
{
	$orders = array();
	$ordersStats = array(
			"count" => 0,
			"sum" => 0
	);
	$req = $_REQUEST['filter']['filters']['0']['field'];
	$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'date_added');
	$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
	$limit = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
	$settings = array(
			'return' => 'paginated',
			'limit' => $limit,
			'page' => ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ),
			'debug'=> DEBUG,
			'filters' => array(
				'sort_field' => $sort_field,
				'sort_direction' => $sort_dir,
				'archive' => ($_REQUEST['archived']? 1 : 0 )
			),
	);
	
	if($req == 'spmethod')
	{
		$settings['filters']['shipping_method'] = $_REQUEST['filter']['filters']['0']['value'];
		$orders = $eshop->getOrders($settings);
		unset($settings['filters']['shipping_method']);
		$settings['filters']['payment_method'] = $_REQUEST['filter']['filters']['0']['value'];
		$ordersStats = $eshop->getOrdersStats($settings);
		$orders = array_merge($orders, $eshop->getOrders($settings));
	}
	elseif($req == 'text')
	{
		$limit = 20;
		unset($settings['filters']['archive']);
		$val = $_REQUEST['filter']['filters']['0']['value'];
		if(is_numeric($val))
		{
			$settings['filters']['custom_value'] = $val;
			$settings['filters']['custom_field'] = 'id';
			$ordersStats = $eshop->getOrdersStats($settings);
			$orders = array_merge($orders, $eshop->getOrders($settings));
		}
		elseif(strpos($val, "@") !== false)
		{
			$ordersStats = $eshop->getOrdersByUserMail($val, ($_REQUEST['archived']? 1 : 0 ), true, ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ), $limit,"date_added",'DESC',true);
			$orders = array_merge($orders, $eshop->getOrdersByUserMail($val, ($_REQUEST['archived']? 1 : 0 ), true, ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ), $limit));
		}
		else
		{
			$ordersStats = $eshop->getOrdersByUname($val, ($_REQUEST['archived']? 1 : 0 ), true, ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ), $limit,"date_added",'DESC',true);
			$orders = array_merge($orders, $eshop->getOrdersByUname($val, ($_REQUEST['archived']? 1 : 0 ), true, ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ), $limit));
		}
	}
	elseif(count($_REQUEST['filter']['filters'])>0)
	{
		$logic = "AND";
		if(isset($_REQUEST['filter']['logic']) && strlen($_REQUEST['filter']['logic'])>0)
		{
			$logic = $_REQUEST['filter']['logic'];
		}
		
		foreach($_REQUEST['filter']['filters'] as $vArr )
		{
			if($vArr['field'] == 'archive')
			{
				$settings['filters']['archive'] = 1;
			}
		}
		
		$ordersStats = $eshop->getOrdersByCustomFilters($_REQUEST['filter']['filters'], $settings, $logic,true);
		$orders = array_merge($orders, $eshop->getOrdersByCustomFilters($_REQUEST['filter']['filters'], $settings, $logic));
	}
	
	$jsonOutput[DATA_KEY] = array(
			"results_per_page" 	=> $limit,
			"count"				=> $ordersStats['count'],
			"sum"				=> $ordersStats['sum'],
			"values" 			=> (count($orders)>0 ? json_encode($orders): json_encode(array())),
			"headers" 			=> (count($orders)>0 ? json_encode(array_keys($orders[0])): json_encode(array()))
	);
	$jsonOutput[STATUS_KEY] = (count($orders)>0 ? SUCCESS : FAILURE);
	
	print(json_encode($jsonOutput));
	exit();
}

if ($_GET['action']=='print' && is_numeric($_GET['id']))
{
	$filters = array('custom_field'=>'id','custom_value'=>$_GET['id']);
	$settings = array(
			'filters'=>$filters,
			'limit' => 1,
			'return' => 'single',
			'debug' => 0
	);
	if (array_key_exists("eshop",$loaded_modules)) {
		$settings['get_products'] = 1;
		$settings['lang'] = DEFAULT_LANG;
	}
	$order = $eshop->getOrders($settings);
	$total_price = array_sum($order['prices']);
	$smarty->assign("total_price_no_vat",$total_price);
	$smarty->assign("vat_price",($loaded_modules['eshop']['settings']['vat']*($total_price/100)));
	$smarty->assign("order",$order);
	$content = $smarty->fetch("modules/eshop/admin/print_order.tpl");
	###### PDF #####
	require_once($_SERVER['DOCUMENT_ROOT'].'/includes/tcpdf/config/lang/eng.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/includes/tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetHeaderData('logo.gif', PDF_HEADER_LOGO_WIDTH, SITE_NAME, '#'.$order['id']);
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	$pdf->setLanguageArray($lang);
	$pdf->SetFont('freeserif', '', 10);
	$pdf->AddPage();
	$pdf->writeHTML($content, true, 0, true, 0);
	$pdf->lastPage();
	$js .= 'print(true);';
	$pdf->IncludeJS($js);
	$pdf->Output($order['id'].'.pdf', 'D');
	exit();
}

// Trick
$orderid = ($_GET['id']? $_GET['id'] : ($_POST['id']? $_POST['id'] : false));

if ($orderid) {

	$filters = array('custom_field'=>'id','custom_value'=>$orderid);
	$settings = array('filters'=>$filters,'limit'=>1,'return' => 'single','debug'=>0);

	if (array_key_exists("eshop",$loaded_modules)) {
		$settings['get_products'] = 1;
		$settings['lang'] = DEFAULT_LANG;
	}

	$order = $eshop->getOrders($settings);

	$total_price_no_vat = $order['amount'];
	$vat_price = 0;
	if ($order['details']['ExtraCharge'])
	{
		$extra_charge_vat = $eshop->getVatPriceOf($order_details['ExtraCharge'],$loaded_modules['eshop']['settings']['vat'],'minus');
		$total_price_vat = $eshop->getVatPriceOf(($total_price_no_vat - $order['details']['extra_charge_no_vat']),$loaded_modules['eshop']['settings']['vat']) + $order['details']['ExtraCharge'];
		$original_price_no_vat = $total_price_no_vat - $order['details']['extra_charge_no_vat'];
		$origial_price_vat = $eshop->getVatPriceOf($original_price_no_vat,$loaded_modules['eshop']['settings']['vat']);
		$vat_price = ($loaded_modules['eshop']['settings']['vat']*($original_price_no_vat/100)) + $order['details']['extra_charge_vat'];
	}//THIS IS FOR INSTALLMENTS
	else {
		$total_price_vat = $eshop->getVatPriceOf(($total_price_no_vat - $order['details']['extra_charge_no_vat']),$loaded_modules['eshop']['settings']['vat']);
		$vat_price = ($loaded_modules['eshop']['settings']['vat']*($total_price_no_vat/100));
	}//END NORMAL

	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
		"id" 					=> $orderid,
		"total_price_no_vat" 	=> $total_price_no_vat,
		"total_price_vat" 		=> $total_price_vat,
		"vat_price"				=> $vat_price,
		"values" 				=> json_encode($order),
		"headers" 				=> array_keys($order)
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();

}//END ORDER FOUND
else {//NO ORDER ID
	$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'date_added');
	$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
	$limit = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
	$settings = array(
			'return' => 'paginated',
			'limit' => $limit,
			'page' => ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ),
			'debug'=>DEBUG,
			'filters' => array(
				'sort_field' => $sort_field,
				'sort_direction' => $sort_dir,
				'archive' => ($_REQUEST['archived']? 1 : 0 )
			),
	);
	// TODO
	$ordersStats = $eshop->getOrdersStats($settings);
	$orders = $eshop->getOrders($settings);
	$payMethods = $eshop->getPaymentMethods();
	$shipMethods = $eshop->getShippingMethods();
	if(count($orders)>0)
	{
		$jsonOutput[DATA_KEY] = array(
					"payment_methods" 	=> $payMethods,
					"shipping_methods"	=> $shipMethods,
					"count"				=> $ordersStats['count'],
					"sum"				=> $ordersStats['sum'],
					"results_per_page" 	=> $limit,
					"values" 			=> json_encode($orders),
					"headers" 			=> json_encode(array_keys($orders[0]))
		);
		$jsonOutput[STATUS_KEY] = SUCCESS;
	}
	else
	{
		$jsonOutput[DATA_KEY] = array(
					"payment_methods" 	=> $payMethods,
					"shipping_methods"	=> $shipMethods,
					"results_per_page" 	=> $limit,
					"values" 			=> json_encode(array()),
					"headers" 			=> json_encode(array())
		);
		$jsonOutput[STATUS_KEY] = FAILURE;
	}
	
	print(json_encode($jsonOutput));
	exit();
}//END OF ALL ORDERS


/*******************************************************************
 * O => YOU ARE HERE
*
* ViewContrl -> View Controler; controler.js
* View -> the View of the box; tpl render
* ModContrl -> the modular_box controler; loader.php
* Module -> the modular_box; The php file of the box
*
* A View request is been answered, as the view asks for its data.
* Data are been processed by the modular box and returned to view
* witch should render the fetched json.
*
* 		ViewContrl			View		ModContrl			Module
* --------------------------------------------------------------------------
* -REQVIEW->_I_				 |				 |				 |
* 			| |	Call View	 |				 |				 |
* 			| |---------------------------->_I_				 |
* 							 | Smarty Displ | |Fetch view	 |
* 							_I_<------------| |				 |
* 		Ajax Display		| |				 |				 |
* <-------------------------| |				 |				 |
* 							 |				 |				 |
* 							_I_				 |				 |
* 							| |Call loader	 |				 |
* 							| |------------>_I_				 |
* 							 |				| |Ajax-Load	 |
* 							 |				| |------------>_I_
* 							 |					Return Data	|O| Fetch Data
* 							_I_<----------------------------|O|
* 							| |Render Data
* 			Display Data	| |
* <-------------------------| |
*
* A Request is been handled, as the command should be proccesed
* and return its state.
*
* -REQ-CMD---Module+Comonent+RequestCMD---->___
* 											| |Ajax-Load
* 											| |------------>___
* 												Return Data	|O| Process CMD
* 							_I_<----------------------------|O| & Fetch Data
* 							| |Render Data
* 			Display Data	| |
* <-------------------------| |
*
*******************************************************************/




/************* /
 $tmpJSON = '{"[0]":[1,2,3]}';
$tmpArr = array();
$tmpArr = json_decode($tmpJSON);//,true);
$tmpObj = new stdClass();
foreach($tmpArr as $k => $v )
{
$tmpObj->$k = $v;
}
var_dump($tmpObj);
echo $tmpObj->{'[0]'};
exit();
/************/
/********************************************************************************* /
 global $sql;
$sql->db_Select("eshop_orders","*");
$orders = execute_multi($sql,1);

foreach($orders as $vArr)
{
$tmpArr = form_settings_array($vArr['details'],"###",":::");
$vArr['details'] = $tmpArr;

$vArr['details']['productids'] 			= $vArr['details']['productids'];
$vArr['details']['invoice_type'] 		= $vArr['details']['invoice_type'];
$vArr['details']['company_name'] 		= $vArr['details']['company_name'];
$vArr['details']['proffession'] 		= $vArr['details']['proffession'];
$vArr['details']['address'] 			= ($vArr['details']['address']? $vArr['details']['address'] : $vArr['details']['order_address'] );
$vArr['details']['postcode'] 			= $vArr['details']['postcode'];
$vArr['details']['afm'] 				= $vArr['details']['afm'];
$vArr['details']['doy'] 				= $vArr['details']['doy'];
$vArr['details']['phone'] 				= ($vArr['details']['phone']? $vArr['details']['phone'] : $vArr['details']['user_phone'] );
$vArr['details']['payment_method'] 		= $vArr['details']['payment_method'];
$vArr['details']['shipping'] 			= $vArr['details']['shipping'];
$vArr['details']['order_name'] 			= ($vArr['details']['order_name']? $vArr['details']['order_name'] : $vArr['details']['user_name'] );
$vArr['details']['order_surname'] 		= ($vArr['details']['order_surname']? $vArr['details']['order_surname'] : $vArr['details']['user_surname'] );
$vArr['details']['order_phone'] 		= ($vArr['details']['order_phone']? $vArr['details']['order_phone'] : $vArr['details']['user_phone'] );
$vArr['details']['order_mobile'] 		= ($vArr['details']['order_mobile']? $vArr['details']['order_mobile'] : $vArr['details']['user_mobile'] );
$vArr['details']['order_address']		= ($vArr['details']['order_address']? $vArr['details']['order_address'] : $vArr['details']['address'] );
$vArr['details']['order_municipality'] 	= $vArr['details']['order_municipality'];
$vArr['details']['order_city'] 			= $vArr['details']['order_city'];
$vArr['details']['order_postcode'] 		= $vArr['details']['order_postcode'];
$vArr['details']['Period'] 				= $vArr['details']['Period'];
$vArr['details']['ExtraCharge'] 		= $vArr['details']['ExtraCharge'];

$vArr['details'] = array_merge($vArr['details'],$eshop->getDefaultCartOptions($vArr['details']));
print_r($vArr['details']);
$sql->db_Update("eshop_orders"," details = '". json_encode($vArr['details']) ."' WHERE id = ".$vArr['id']." ");
}
exit();
/*********************************************************************************/


?>