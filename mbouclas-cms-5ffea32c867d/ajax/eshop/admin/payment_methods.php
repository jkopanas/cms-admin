<?php
include_once("eshop/admin/init.php");//load from eshop/admin!!!!
define("NAME_OF_INSTANCE","payment_methods");
$jsonOutput['name'] = NAME_OF_INSTANCE;

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

if(is_array($_REQUEST['filter']))
{
	$id = $_REQUEST['filter']['filters'][0]['value'];
}

if ($_REQUEST['action'] == "addnew")
{
	$t = new textparse();
	$title = $t->formtpa($_REQUEST['title']);
	$description = $t->formtpa($_REQUEST['description']);
	$orderby = $_REQUEST['orderby'];
	$surcharge = $_REQUEST['surcharge'];
	$surcharge_type = $_REQUEST['surcharge_type'];
	$active = $_REQUEST['active'];
	$date_added = $_REQUEST();
	
	$settings = array();
	$settings['accid'] 				= $_REQUEST['accid'];
	$settings['accname'] 			= $_REQUEST['accname'];
	$settings['key'] 				= $_REQUEST['key'];
	$settings['apikey'] 			= $_REQUEST['apikey'];
	$settings['authkey'] 			= $_REQUEST['authkey'];
	$settings['email'] 				= $_REQUEST['email'];
	$settings['uname']				= $_REQUEST['uname'];
	$settings['pass'] 				= $_REQUEST['pass'];
	$settings['sucurl'] 			= $_REQUEST['sucurl'];
	$settings['failurl'] 			= $_REQUEST['failurl'];
	$settings['cburl'] 				= $_REQUEST['cburl'];
	$settings['extra'] 				= $_REQUEST['extra'];
	$settings['payment_processor'] 	= $_REQUEST['payment_processor'];
	$settings = json_encode($settings);
	
	$shipping_methods = "";
	if(is_array($_REQUEST['shipping']))
	{
		$tmpArr = array();
		foreach ($_REQUEST['shipping'] as $v)
		{
			$tmpArr[] = $v['id'];
		}
		// Custom JSON encode
		$_REQUEST['shipping_methods'] = '['.implode(',', $tmpArr).']';
	}
	
	$shipping_methods = $_REQUEST['shipping_methods'];
	
	
	if($sql->db_Insert(
			"eshop_payment_methods",
			"'','$title','$description','$orderby','$active','$surcharge','$surcharge_type','$settings','$date_added','$shipping_methods'")) {
			
		$jsonOutput[DATA_KEY] = array("id" => $sql->last_insert_id);
		$jsonOutput[STATUS_KEY] = SUCCESS;
	}
	else {
		$jsonOutput[DATA_KEY] = array("id" => -1);
		$jsonOutput[STATUS_KEY] = FAILURE;
	}
	//header("Location: payment_methods.php?id=".mysql_insert_id()."#addcat");
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	print(json_encode($jsonOutput));
	exit();
}

if ($_REQUEST['action'] == "modify")
{
	$t = new textparse();
	$query = "";
	
	unset($_REQUEST['action']);
	unset($_REQUEST['boxID']);
	unset($_REQUEST['shipping']);
	unset($_REQUEST['shipping_methods']);
	unset($_REQUEST['settings']);
	
	foreach ($_REQUEST as $key => $val)
	{
		$val = $t->formtpa($val);
		$query .= " $key = '$val'  ,";
	}
	
	$query = substr($query, 0, -1);
	$query .= " WHERE id = $id";
	$sql->db_Update("eshop_payment_methods",$query);
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $id);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

if ($_REQUEST['action'] == "modifysettings")
{
	$tmpArr = array();	
	
	$tmpArr['accid'] 				= $_REQUEST['accid'];
	$tmpArr['accname'] 				= $_REQUEST['accname'];
	$tmpArr['key'] 					= $_REQUEST['key'];
	$tmpArr['apikey'] 				= $_REQUEST['apikey'];
	$tmpArr['authkey'] 				= $_REQUEST['authkey'];
	$tmpArr['email'] 				= $_REQUEST['email'];
	$tmpArr['uname']				= $_REQUEST['uname'];
	$tmpArr['pass'] 				= $_REQUEST['pass'];
	$tmpArr['sucurl'] 				= $_REQUEST['sucurl'];
	$tmpArr['failurl'] 				= $_REQUEST['failurl'];
	$tmpArr['cburl'] 				= $_REQUEST['cburl'];
	$tmpArr['extra'] 				= $_REQUEST['extra'];
	$tmpArr['payment_processor'] 	= $_REQUEST['payment_processor'];
	
	$sql->db_Update("eshop_payment_methods"," settings = '".json_encode($tmpArr)."' WHERE id = $id ");

	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $id);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

if ($_GET['action'] == "delete")
{
	$sql->db_Delete("eshop_payment_methods","id = $id");

	//header("Location: payment_methods.php");
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $id);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

if ($_REQUEST['action'] == "addnewshipmethod" && is_numeric($_REQUEST['id']))
{
	$tmpArr = array();
	$tmpArr = @$eshop->getPaymentMethods($_REQUEST['id'],0,array('shipping_methods'=>true));
	
	$tmpToken = array();
	foreach ($tmpArr['shipping'] as $v)
	{
		$tmpToken[] = $v['id'];
	}
	$tmpToken[] = $_REQUEST['smID'];
	// Custom JSON encode
	$tmpArr['shipping_methods'] = '['.implode(',', $tmpToken).']';
	
	$sql->db_Update("eshop_payment_methods"," shipping_methods = '".$tmpArr['shipping_methods']."' WHERE id = ".$_REQUEST['id'] );

	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $id);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

if ($_REQUEST['action'] == "deleteshipmethod" && is_numeric($_REQUEST['id']))
{
	$tmpArr = array();
	$tmpArr = @$eshop->getPaymentMethods($_REQUEST['id'],0,array('shipping_methods'=>true));

	$tmpToken = array();
	foreach ($tmpArr['shipping'] as $v)
	{
		if(((int) $v['id']) !== ((int) $_REQUEST['smID']))
		{
			$tmpToken[] =  $v['id'];
		}
	}
	// Custom JSON encode
	$tmpArr['shipping_methods'] = '['.implode(',', $tmpToken).']';
	
	$sql->db_Update("eshop_payment_methods"," shipping_methods = '".$tmpArr['shipping_methods']."' WHERE id = ".$_REQUEST['id'] );

	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $id);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

if (is_numeric($id))
{
	$tmpArr = @$eshop->getPaymentMethods($id,0,1);
	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
						"payment_methods" 	=> @json_encode($tmpArr),
						"shipping" 			=> @json_encode($eshop->getShippingMethods(0,1)),
						"payment_processors"=> @json_encode($eshop->getPaymentProcessors()),
						"id"				=> $id,
						"values" 			=> @json_encode($tmpArr),
						"headers" 			=> @json_encode(array_keys($tmpArr[0])),
						"settings"			=> $tmpArr['settings'],
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

$tmpArr = @$eshop->getPaymentMethods(0,0,array('shipping_methods'=>true));

$jsonOutput[RENDER_METHOD_KEY] = DEFAULT_RENDER_METHOD;
$jsonOutput[DATA_KEY] = array(
						"payment_methods" 	=> @json_encode($tmpArr),
						"shipping" 			=> @json_encode($eshop->getShippingMethods(0,1)),
						"payment_processors"=> @json_encode($eshop->getPaymentProcessors()),
						"values" 			=> @json_encode($tmpArr),
						"headers" 			=> @json_encode(array_keys($tmpArr[0]))
);
$jsonOutput[STATUS_KEY] = SUCCESS;
print(json_encode($jsonOutput));
exit();

?>