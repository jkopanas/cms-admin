<?php
include_once("eshop/admin/init.php");//load from eshop/admin!!!!
define("NAME_OF_INSTANCE","pclasses");
$jsonOutput['name'] = NAME_OF_INSTANCE;

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

if ($_POST['mode'] == "add")
{
	$t = new textparse();
	$title = $t->formtpa($_POST['title']);
	$classtext = $t->formtpa($_POST['classtext']);
	$type = $_POST['type'];
	$module_id = $_POST['module_id'];
	$orderby = $_POST['orderby'];
	$option_type = $_POST['option_type'];
	$availability = $_POST['active'];

	$sql->db_Insert("eshop_classes","'','$orderby','$availability','$title','$classtext','$module_id','$type','$option_type'");

	if($sql->db_Insert("eshop_classes","'','$orderby','$availability','$title','$classtext','$module_id','$type','$option_type'")) {
		$jsonOutput[DATA_KEY] = array("id" => $sql->last_insert_id);
		$jsonOutput[STATUS_KEY] = SUCCESS;
	}
	else {
		$jsonOutput[DATA_KEY] = array("id" => -1);
		$jsonOutput[STATUS_KEY] = FAILURE;
	}
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	print(json_encode($jsonOutput));
	exit();
	//header("Location: classes.php?id=".mysql_insert_id());
}
//print_r($_POST);
if ($_POST['mode'] == "delete_class")
{
	unset($tmp);
	foreach ($_POST['classid'] as $key => $val)
	{
		if ($val == 1)//eliminates the check all/none box
		{
			$tmp = eshop_classes($key);
			$sql->db_Delete("eshop_classes","id = $key");
			$sql->db_Delete("eshop_classes_options","classid = $key");
			$sql->db_Delete("eshop_classes_exclusions","class_id = $key");
		}
	}//END OF WHILE
	header("Location: classes.php");
	exit();
}

if ($_POST['mode'] == "update_class")
{
	foreach ($_POST as $k => $v)
	{

		if (strstr($k,"-"))
		{
			list($field,$code)=split("-",$k);
			$sql->db_Update("eshop_classes"," $field = '$v' WHERE id = ".$code);
		}
	}

	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $code);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}


if ($_POST['mode'] == "update")
{
	$t = new textparse();
	unset($tmp);
	foreach ($_POST as $k => $v)
	{
		if (strstr($k,"options-"))
		{
			list($field,$code)=split("-",str_replace("options-",'',$k));
			//			echo "$field --- $code<br>";
			//gather up elements
			$tmp[$code][$field] = $v;
			$tmp[$code]['optionid'] = $code;
		}
		$sql->db_Update("eshop_classes"," $k = '$v' WHERE id = ".$_POST['id']);
	}
	
	if ($_POST['remove_list'])
	{
		$r_m = explode(",",$_POST['remove_list']);
		foreach ($r_m as $m)
		{
			if ($m)
			{
				$sql->db_Delete("eshop_classes_options","optionid = ".str_replace("TR","",$m));
			}
		}
	}//END OF REMOVE
	
	if ($tmp)
	{
		foreach ($tmp as $b)
		{
			$sql->db_Select("eshop_classes_options","optionid","classid = '".$_POST['id']."'  AND optionid = ".$b['optionid']);
			//	echo "classid = '".$_POST['id']."'  AND optionid = ".$b['optionid']."<Br>";
			$count = $sql->db_Rows();
			if ($count) //Exists update
			{
				foreach ($b as $y => $u)
				{
					$u = $t->formtpa($u);
					$sql->db_Update("eshop_classes_options","$y = '$u' WHERE classid = '".$_POST['id']."'  AND optionid = ".$b['optionid']);
					//			echo "$y = '$u' WHERE optionid = ".$b['optionid']."<Br>";
				}
			}
			else //New option
			{

				$sql->db_Insert("eshop_classes_options","'',".$_POST['id'].",'".$b['orderby']."','".$b['option_value']."','".$b['price_modifier']."','".$b['modifier_type']."','".$b['option_name']."'");
				//		echo "'',".$_POST['id'].",'".$b['orderby']."','".$b['option_value']."','".$b['price_modifier']."','".$b['modifier_type']."','".$b['title']."'<br>";
			}
		}
	}//END IF
	
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $id);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}//END UPDATE

if (is_numeric($id))
{
	$tmpArr = eshop_classes($id,0,1);
	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
							"pclasses" 			=> json_encode($tmpArr),
							"id"				=> $id,
							"values" 			=> json_encode($tmpArr),
							"headers" 			=> json_encode(array_keys($tmpArr[0]))
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
}

$tmpArr = eshop_classes(0,0,1);
$jsonOutput[RENDER_METHOD_KEY] = DEFAULT_RENDER_METHOD;
$jsonOutput[DATA_KEY] = array(
							"pclasses" 			=> json_encode($tmpArr),
							"values" 			=> json_encode($tmpArr),
							"headers" 			=> json_encode(array_keys($tmpArr[0]))
);
$jsonOutput[STATUS_KEY] = SUCCESS;
print(json_encode($jsonOutput));
exit();

?>