<?php
include_once("eshop/admin/init.php");//load from eshop/admin!!!!
define("NAME_OF_INSTANCE","shipping_methods");
$jsonOutput['name'] = NAME_OF_INSTANCE;

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
$code = ($code) ? $code: 1;
/************************************************************************************ /
$equationVarsArr = array();
$equationVarsArr['weight'] = 2.1;
$equationVarsArr['base_cost'] = 3.25;
print($eshop->getCostByEquation($equationVarsArr, "((weight * base_cost)*2)"));
exit();
/************************************************************************************/

if ($_REQUEST['action'] == "addnew")
{
	$t = new textparse();
	$insertToken = array();
	$insertToken['shipping'] = $t->formtpa($_REQUEST['shipping']);
	$insertToken['shipping_time'] = $t->formtpa($_REQUEST['shipping_time']);
	$insertToken['description'] = $t->formtpa($_REQUEST['description']);
	$insertToken['destination'] = $_REQUEST['destination'] ? $t->formtpa($_REQUEST['destination']) : 0;
	$insertToken['code'] = $_REQUEST['code'] ? $_REQUEST['code'] : 0;
	$insertToken['parent'] = $_REQUEST['parent'];
	$insertToken['orderby'] = $_REQUEST['orderby'] ? $_REQUEST['orderby'] : 0;
	$insertToken['active'] = $_REQUEST['active'] ? $_REQUEST['active'] : 0;
	$insertToken['weight_min'] = $_REQUEST['weight_min'];
	$insertToken['weight_limit'] = $_REQUEST['weight_limit'];
	$insertToken['date_added'] = time();
	$insertToken['base_cost'] = $_REQUEST['base_cost'];
	
	$tmpSettsArr = $_REQUEST['settings'];
	
	$tmpSettsArr = array();
	$tmpSettsArr['costequation'] = ($_REQUEST['costequation'] ? $_REQUEST['costequation'] : "");
	$tmpSettsArr['costequationvars'] = array();
	
	$tmpCQV = $_REQUEST['costequationvars'];
	$tmpCQV = trim($tmpCQV);
	$tmpCQV = explode(";", $tmpCQV);
	$tmpCQVToken = array();
	if(is_array($tmpCQV))
	{
		foreach ($tmpCQV as $v)
		{
			$vv = array();
			$vv = explode('=', $v);
			$tmpCQVToken[$vv[0]] = $vv[1];
		}
	}
	else
	{
		$vv = array();
		$vv = explode('=', $tmpCQV);
		$tmpCQVToken[$vv[0]] = $vv[1];
	}
	$tmpSettsArr['costequationvars'] = $tmpCQVToken;
	$tmpSettsArr['costequationvars']['base_cost'] = $insertToken['base_cost'];
		
	$insertToken['settings'] = json_encode($tmpSettsArr);
		
	if($sql->db_Insert("eshop_shipping",
		"'','".$insertToken['shipping']
		."','".$insertToken['shipping_time']
		."','".$insertToken['destination']
		."','".$insertToken['code']
		."','".$insertToken['parent']
		."','".$insertToken['orderby']
		."','".$insertToken['active']
		."','".$insertToken['weight_min']
		."','".$insertToken['weight_limit']
		."','".$insertToken['date_added']
		."','".$insertToken['description']
		."','".$insertToken['settings']
		."','".$insertToken['base_cost']."'")) 
	{
		$jsonOutput[DATA_KEY] = array("id" => $sql->last_insert_id);
		$jsonOutput[STATUS_KEY] = SUCCESS;
	}
	else 
	{
		$jsonOutput[DATA_KEY] = array("id" => -1);
		$jsonOutput[STATUS_KEY] = FAILURE;
	}
	
	//header("Location: shipping_methods.php");
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	print(json_encode($jsonOutput));
	exit();
}

if ($_REQUEST['action'] == "modify")
{
	$t = new textparse();
	$insertToken = array();
	$insertToken['shipping'] = $t->formtpa($_REQUEST['shipping']);
	$insertToken['shipping_time'] = $t->formtpa($_REQUEST['shipping_time']);
	$insertToken['description'] = $t->formtpa($_REQUEST['description']);
	$insertToken['destination'] = $_REQUEST['destination'] ? $t->formtpa($_REQUEST['destination']) : 0;
	$insertToken['code'] = $_REQUEST['code'] ? $_REQUEST['code'] : 0;
	$insertToken['parent'] = $_REQUEST['parent'];
	$insertToken['orderby'] = $_REQUEST['orderby'] ? $_REQUEST['orderby'] : 0;
	$insertToken['active'] = $_REQUEST['active'] ? $_REQUEST['active'] : 0;
	$insertToken['weight_min'] = $_REQUEST['weight_min'];
	$insertToken['weight_limit'] = $_REQUEST['weight_limit'];
	$insertToken['date_added'] = time();
	$insertToken['base_cost'] = $_REQUEST['base_cost'];
	
	$tmpSettsArr = $_REQUEST['settings'];
	
	$tmpSettsArr = array();
	$tmpSettsArr['costequation'] = ($_REQUEST['costequation'] ? $_REQUEST['costequation'] : "");
	$tmpSettsArr['costequationvars'] = array();
	
	$tmpCQV = $_REQUEST['costequationvars'];
	$tmpCQV = trim($tmpCQV);
	$tmpCQV = explode(";", $tmpCQV);
	$tmpCQVToken = array();
	if(is_array($tmpCQV))
	{
		foreach ($tmpCQV as $v)
		{
			$vv = array();
			$vv = explode('=', $v);
			$tmpCQVToken[$vv[0]] = $vv[1];
		}
	}
	else
	{
		$vv = array();
		$vv = explode('=', $tmpCQV);
		$tmpCQVToken[$vv[0]] = $vv[1];
	}
	$tmpSettsArr['costequationvars'] = $tmpCQVToken;
	$tmpSettsArr['costequationvars']['base_cost'] = $insertToken['base_cost'];
		
	$insertToken['settings'] = @json_encode($tmpSettsArr);
	
	$query = "";
	foreach ($insertToken as $key => $val)
	{
		$query .= " $key = '$val'  ,";
	}
	
	$query = substr($query, 0, -1);
	$query .= " WHERE id = $id";
	$sql->db_Update("eshop_shipping",$query);
		
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $id);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(@json_encode($jsonOutput));
	exit();
}

if ($_REQUEST['action'] == "delete")
{
	$sql->db_Delete("eshop_shipping","id = $id");
	$sql->db_Delete("eshop_shipping","parrent = $id");
	
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $id);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(@json_encode($jsonOutput));
	exit();
	//header("Location: shipping_methods.php");
	//exit();
}

if (is_numeric($id))
{
	$tmpArr = @$eshop->getShippingMethods($id);
	$jsonOutput[RENDER_METHOD_KEY] = WINDOWPREVIEW_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array(
							"shipping_methods" 	=> @json_encode($tmpArr),
							"id"				=> $id,
							"values" 			=> @json_encode($tmpArr),
							"headers" 			=> @json_encode(array_keys($tmpArr[0]))
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(@json_encode($jsonOutput));
	exit();
	
	//$smarty->assign("shipping", eshop_shipping($id));
	//$smarty->assign("action","update");
}

$tmpArr = @$eshop->getShippingMethods((is_numeric($_REQUEST['parrent']) ? $_REQUEST['parrent'] : 0 ),0);
$jsonOutput[RENDER_METHOD_KEY] = DEFAULT_RENDER_METHOD;
$jsonOutput[DATA_KEY] = array(
						"shipping_methods" 	=> @json_encode($tmpArr),
						"code"				=> ($code? $shipping_methods[0]['code'] : $code),
						"values" 			=> @json_encode($tmpArr),
						"headers" 			=> @json_encode(array_keys($tmpArr[0]))
);
$jsonOutput[STATUS_KEY] = SUCCESS;
print(json_encode($jsonOutput));
exit();

$shipping_methods = eshop_shipping();
$smarty->assign("shipping_methods",$shipping_methods);


/*************************************************** /
foreach ($_REQUEST as $key => $val)
{
	$val = $t->formtpa($val);
	if (strstr($key,"settings-"))
	{
		list($dump,$field)=split("-",$key);
		$ss[$field] = $val;

	}
}
if (is_array($ss) AND !empty($ss)) {
	$settings = form_settings_string($ss,"###",":::");
}


if ($_REQUEST['action'] == "update")
{
	foreach ($_REQUEST as $key => $val)
	{
		if (strstr($key,"settings-"))
		{
			list($dump,$field)=split("-",$key);
			$ss[$field] = $val;
		}
		$sql->db_Update("eshop_shipping","$key = '$val' WHERE shippingid = $id");
	}
	if (is_array($ss) AND !empty($ss)) {
		$q = form_settings_string($ss,"###",":::");
		$query = "settings ='$q' WHERE shippingid = $id";
		$sql->db_Update("eshop_shipping",$query);
	}
	
}

if ($_REQUEST['action'] == "delete")
{

	foreach ($_REQUEST['ids'] as $key => $val)
	{

		if ($val == 1)//eliminates the check all/none box
		{
			$sql->db_Delete("eshop_shipping","shippingid = $key");
		}
	}
	$jsonOutput[RENDER_METHOD_KEY] = NOTIFY_RENDER_METHOD;
	$jsonOutput[DATA_KEY] = array("id" => $id);
	$jsonOutput[STATUS_KEY] = SUCCESS;
	print(json_encode($jsonOutput));
	exit();
	//header("Location: shipping_methods.php?id=$id#methods");
	//exit();
}

/***************************************************/

?>