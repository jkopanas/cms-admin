<?php
set_time_limit(600); // 10 min
define("EXECUTION_PERIOD",10);
$url = "http://74.125.224.72/"; // Google's IP
print("URL:\t".$url."\n");
print("fopen:\n");
$fPTR = null;
for ($i = 0; $i < EXECUTION_PERIOD; $i++)
{
	$stop = 0;
	$start = microtime(true);
	if(fread($fPTR = fopen($url, "r"),512))
	{
		fclose($fPTR);
		$stop = microtime(true);
	}
	print("\t");
	print($stop - $start);
	print("\n");
}

print("fsockopen:\n");
$fPTR = null;
$errno = null;
$errstr = null;
for ($i = 0; $i < EXECUTION_PERIOD; $i++)
{
	$stop = 0;
	$start = microtime(true);
	$fPTR = fsockopen($url, 80, $errno, $errstr, 30);
	if(!feof($fPTR))
	{
		fclose($fPTR);
		$stop = microtime(true);
	}
	else
	{
		fclose($fPTR);
		$stop = microtime(true);
		print($errno);
		print("\n");
		print($errstr);
		print("\n");
		
	}
	print("\t");
	print($stop - $start);
	print("\n");
}

print("file_get_contents:\n");
$fPTR = null;
for ($i = 0; $i < EXECUTION_PERIOD; $i++)
{
	$stop = 0;
	$start = microtime(true);
	if($fPTR = file_get_contents($url))
	{
		fclose($fPTR);
		$stop = microtime(true);
	}
	print("\t");
	print($stop - $start);
	print("\n");
}

print("cURL:\n");
for ($i = 0; $i < EXECUTION_PERIOD; $i++)
{
	$stop = 0;
	$start = microtime(true);
	$curlPTR = curl_init();
	curl_setopt($curlPTR,CURLOPT_URL, $url);
	curl_setopt($curlPTR,CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlPTR,CURLOPT_CONNECTTIMEOUT, 10);
	if($fPTR = curl_exec($curlPTR))
	{
		curl_close($curlPTR);
		$stop = microtime(true);
	}
	print("\t");
	print($stop - $start);
	print("\n");
}
?>
