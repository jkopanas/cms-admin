<?php
include("../init.php");
$eshop = new eshop();

$id = $_POST['id'];
$module = $_POST['module'];
//unset($_SESSION['cart']);
if ($_POST['action'] == 'addToCart') {

$item = new Items(array('module'=>$loaded_modules[$module]));
$product = $item->GetItem($id,array('debug'=>0,'fields'=>'id,title,permalink','thumb'=>1));
$price = ($loaded_modules['eshop']['settings']['cartAddTax']) ? $eshop->vatPrice($product['eshop']['price'],$eshop->tax) : $product['eshop']['price'];
$product['eshop']['vatPrice'] = $price;
$_SESSION['cart'][$id]['quantity'] = $_SESSION['cart'][$id]['quantity'] + 1;
$_SESSION['cart'][$id]['price'] = $price;
$_SESSION['cart'][$id]['total'] = $price + $_SESSION['cart'][$id]['total'];
$_SESSION['cart'][$id]['formated_total'] = formatprice($_SESSION['cart'][$id]['total']);
$_SESSION['cart'][$id]['saved'] = 0;
//GET TOTALS

if (is_numeric(ID)) {
	$_SESSION['cart'][$id]['saved'] = 1;
	$eshop->doAddToCart(ID,$product,array("quantity" => $_SESSION['cart'][$id]['quantity'] ),FRONT_LANG);
}

foreach ($_SESSION['cart'] as $k=>$v)
{
	$quantities[] = $v['quantity'];
	$prices[] = $v['total'];
}//END TOTALS

$total_prices = array_sum($prices);
$total_quantities = array_sum($quantities);
$smarty->assign("item",$product);
$smarty->assign("total_quantities",$total_quantities);
$smarty->assign("total_price",$total_prices);
	
	
	echo json_encode(array('status'=>'OK','total_quantities'=>$total_quantities,'formatedPrice'=>formatprice($total_prices),'vat'=>$eshop->tax,'item'=>$product));
}//END ADD TO CART


if ($_GET['action'] == 'finished') {
		global $sql;
		
		$posted_data=$_POST['data']['save'];
		$account = $_POST['data']['AccountData'];
		$details['invoice_type'] = $posted_data['invoice_type']['value'];
		$current_module = $loaded_modules['products'];
		$Content = new Items(array('module'=>$current_module));	
		$smarty->assign("total_price_no_vat",array_sum($total_prices));
		$smarty->assign("vat_price",($loaded_modules['eshop']['settings']['vat']*(array_sum($total_prices)/100)));
		$item = array();
		foreach ($_SESSION['cart'] as $key => $value) {
			$product=$Content->GetItem($key);
			$item[$key] = $value; 
			$item[$key]=array_merge($item[$key],$product);
			 $ids .= $product['id'].",";
		}
		$details['productids'] = substr($ids, 0, -1);
		
		foreach($account as $key => $value) {
			$details[$key] = $value;
		}
		
		//$details['order_phone'] = $_POST['data']['ShippingInfo']['tel'];
		$smarty->assign("place", $posted_data['shipping_info']['value'] );
		$smarty->assign("Shipping_method",$_POST['data']['ShippingInfo']);
		$smarty->assign("invoice_type",$posted_data['payment_method']['value']);
		$note=$smarty->fetch("modules/eshop/admin/notes_info.tpl");
		
		$settings = json_encode($details,true);
		$details ='';
		$details = str_replace("\\", "\\\\", addslashes($settings));
		$id = time();
		
		$sql->db_Insert("eshop_orders", "'".$id."',".ID.",'".$account['email']."',1,".array_sum($total_prices).",'".$posted_data['payment_method']['value']."','".$posted_data['shipping_method']['value']."','".$note."','".time()."','".$details."',0,''");

		$sql->db_Update("eshop_cart"," order_id = '".$id."' WHERE uid = ".ID);
		unset($_SESSION['cart']);
		echo json_encode(array());
		exit;
		
}

if ($_GET['action'] == 'lastCheckout') {
	
		$posted_data=$_POST;
		$current_module = $loaded_modules['products'];
		$Content = new Items(array('module'=>$current_module));	
		$smarty->assign("total_price_no_vat",array_sum($total_prices));
		$smarty->assign("vat_price",($loaded_modules['eshop']['settings']['vat']*(array_sum($total_prices)/100)));
		$item = array();
		foreach ($_SESSION['cart'] as $key => $value) {
			$product=$Content->GetItem($key,array("thumb"=>1,"images"=>1));
			$item[$key] = $value; 
			$item[$key]=array_merge($item[$key],$product);
		}
		$smarty->assign("items",$item);
		
	if ($posted_data['data'] == 1 ) {
		
		///// paypal Starts //////////////////////////////
		
		$paypal = new GoPayPal(BUY_NOW);
		//$paypal->sandbox = true;
		$paypal->openInNewWindow = true;
		$paypal->set('business', 'mbouclas@gmail.com');
		$paypal->set('currency_code', 'EUR');
		$paypal->set('country', 'CY');
		$paypal->set('return', 'https://www.acmpromo.com/ajax/payment_complete.php');
		//$paypal->set('cancel_return', 'http://tovrika.gr/ajax/payment_complete.php');
		$paypal->set('notify_url', 'https://www.acmpromo.com/ajax/payment_complete.php'); # rm must be 2, need to be hosted online
		$paypal->set('rm', 2); # return by POST
		$paypal->set('no_note', 0);
		$paypal->set('custom', md5(time()));
		//$paypal->set('tax','0.99');
		//$paypal->set('shipping','5.00');
		$paypal->set('cbt', 'You must return to WWW.ACMPROMO.COM to validate your payment and finish transaction!'); # caption override for "Return to Merchant" button

		# Add item for Buy Now
		$item = new GoPayPalCartItem();
		$item->set('item_name', 'Buy sms');
		//$item->set('item_number', 10);
		$item->set('quantity', 1);
		$item->set('amount',array_sum($total_prices));

		$paypal->addItem($item);	
		# If you set your custom button here, PayPal subscribe button will be displayed.
		//$paypal->setButton('<button type="submit">Pay PayPal - The safer, easier way to pay online!</button>');
		$smarty->assign("button_pay", $paypal->html());

		if(sizeof($_POST)){
			//echo '<pre>'; print_r($_POST); echo '</pre>';
		}
		//// end paypal///////		
	}
	
		$template = $smarty->fetch("modules/eshop/bank_field_connect.tpl");
		echo json_encode(array("template" => $template),true);
		exit;
		
}


if ($_POST['action'] == 'updateQuantities') {
		$id = $_POST['itemid'];
		$price = $_POST['price'];
	if ($_POST['mode'] == 'add') {
		$_SESSION['cart'][$id]['quantity']++;
		$_SESSION['cart'][$id]['total'] = $price + $_SESSION['cart'][$id]['total'];
		$_SESSION['cart'][$id]['formated_total'] = RJ983706181GB($_SESSION['cart'][$id]['total']);
//GET TOTALS
foreach ($_SESSION['cart'] as $k=>$v)
{
	$quantities[] = $v['quantity'];
	$prices[] = $v['total'];
}//END TOTALS
$total_price = array_sum($prices);
echo json_encode(array('status'=>'OK','total_quantities'=>array_sum($quantities),'totalPrice'=>formatprice($total_price),'itemTotalPrice'=>formatprice($_SESSION['cart'][$id]['total'])
,'totalPriceNoVat'=>formatprice($total_price),'vatValue'=>$eshop->tax*($total_price/100)));
	}//END ADD
	else {//REMOVE
		if ($_SESSION['cart'][$id]['quantity'] != 1) {// IF contains 1 item we should remove it
			$_SESSION['cart'][$id]['quantity']--;
			$_SESSION['cart'][$id]['total'] = $_SESSION['cart'][$id]['total'] - $price;
			$_SESSION['cart'][$id]['formated_total'] = formatprice($_SESSION['cart'][$id]['total']);
		}
		else {
			unset($_SESSION['cart'][$id]);
		}

		//GET TOTALS

		if (is_array($_SESSION['cart']) AND !empty($_SESSION['cart'])) {
		foreach ($_SESSION['cart'] as $k=>$v)
		{
			$quantities[] = $v['quantity'];
			$prices[] = $v['total'];
		}//END TOTALS
		$total_quantities = array_sum($quantities);
		$total_price = array_sum($prices);
		}
		else {
			$total_price =0;
			$total_quantities =0;
		}
		
echo json_encode(array('status'=>'OK','total_quantities'=>array_sum($quantities),'totalPrice'=>formatprice($total_price),'itemTotalPrice'=>formatprice($_SESSION['cart'][$id]['total'])
,'totalPriceNoVat'=>formatprice($total_price),'vatValue'=>$eshop->tax*($total_price/100)));	
	}//END REMOVE
}//END QUANTITIES

?>