<?php
$module = ($_GET['module']) ? $_GET['module'] : $_POST['module'];
$current_module = $loaded_modules[$module];

$categoryid  = ($_GET['cat'] ? $_GET['cat'] : $_POST['cat']);
$Efields = new ExtraFields(array('module'=>$current_module));



if ( $_GET['action'] == "show" ) {
	
		if (is_array($_POST['filter']['filters'])) {//KENDO GRID FILTERS
			foreach ($_POST['filter']['filters'] as $key => $value) {
				if ($value['field'] != "groupid") {
				switch ($value['operator']) {
					case 'eq': $op = 'EQ';
					break;
					case 'contains': $op = 'LIKE';
					break;
					case 'neq': $op = 'NE';
					break;
				}
				$posted_data['searchFields'][$value['field']]= array('type'=>$op,'val'=>$value['value']);
				} else {
					$groupid=$value['value'];
				}
			}
		}
		

		$results_per_page = ($_POST['pageSize']) ? $_POST['pageSize'] : 10; 
	    $AllExtraFields = $Efields->GetExtraFields(array('debug'=>0,'page' => $_POST['page'], 'searchFields' => $posted_data['searchFields'], 'results_per_page' => $results_per_page, 'groupid' => $groupid,'itemid'=>$id,'catid'=>$categoryid,'way'=>'ASC'));
	  
	    $AllExtraFieldsCount = $Efields->GetExtraFields(array('debug'=>0,'countItems' => 1,'itemid'=>$id,'groupid' => $groupid,'catid'=>$categoryid,'way'=>'ASC'));
	  
	    $groups = ($Efields->searchGroups(array('debug'=>0,"orderby"=>"orderby","itemCount"=>1,'searchFields'=>array('module'=>$current_module['name'])))) ? $Efields->searchGroups(array('debug'=>0,"orderby"=>"orderby","itemCount"=>1,'searchFields'=>array('module'=>$current_module['name']))) : array();
	    echo json_encode(array("groups" => $groups, "data" => array( 'total' => $AllExtraFieldsCount), "extrafields" => $AllExtraFields ));
    	exit;
    	
}
if ( $_GET['action'] == "GroupEdit" ) {
	

     $group = $Efields->getGroup($_GET['id']); 
    $AllExtraFields = $Efields->GetExtraFields(array('debug'=>0,'itemid'=>$id,'catid'=>$categoryid,'way'=>'ASC'));
	
    foreach($AllExtraFields as $k=>$v)
    {
    	if (!isset($group['items'][$v['fieldid']])) {
        	$fields[]=$v;
    	} else {
    		$tmp[$v[fieldid]]=$v;
    	}
    	$themes = get_themes();

    	$selectedThemes = $group['settings']['themes'][0];

    	if(is_array($selectedThemes))
    	{
        	$selectedThemes=array_flip($selectedThemes);
    		
        	for($i=0;$i<count($themes);$i++)
        	{
        	    if(key_exists($themes[$i]['id'],$selectedThemes))
        	    {
        	        $themes[$i]['checked']="selected";    
        	    }
       		}
    	}
	}
		
		$ar= array();
		foreach($group['items'] as $key => $value ) {
			$ar[] = $tmp[$key];
		}
		$group['items']=$ar;

	    echo json_encode(array("themes" => $themes, "group" => $group, "extrafields" => $fields ));
    	exit;
}

if ( $_GET['action'] == "GroupNew" ) {

    	$themes = get_themes();
	 	echo json_encode(array("themes" => $themes ));
		exit;
}

if ( $_GET['action'] == "SaveGroupExtraOrder" ) {
		
	if (!is_numeric($_GET['id'])) { exit; }
	global $sql;
	print_ar($_POST['data']);
    foreach ($_POST['data'] as $k=>$v)
    {
		$sql->db_Update('extra_fields_groups_items',"orderby = $k WHERE groupid = '".$_GET['id']."' AND itemid='".$v."'");
		print_ar($sql);
	}
	exit;	
}

if ($_GET['action'] == 'SaveEfieldsGroupsOrder') {
    global $sql;
    foreach ($_POST['data'] as $k=>$v)
	{
		$sql->db_Update('extra_fields_groups',"orderby = $k WHERE id = $v");        
	}
}


if ($_GET['action'] == 'Efields') {
	
    $Efields = new ExtraFields(array('module'=>$current_module));
    	
    if ($_GET['mode'] == 'SaveEfieldGroup') {
            if(!empty($_GET['groupid']))
            {
            	$posted_data=$_POST['data'];
            	$posted_data['groupid']=$_GET['groupid'];
                $ar=$Efields->updateGroup($posted_data);
            }
            else
        {
                $ar=$Efields->newGroup($_POST['data']);      
            }  
            echo json_encode($ar);
            exit();
        }
    
    if ($_GET['mode'] == 'SaveEfield') {
		
    	$posted_data= $_POST['data'];
        if ($_GET['fieldid']) {//MODIFY
        	$posted_data['fieldid']=$_GET['fieldid'];
           	$ar=$Efields->ModifyExtraField($posted_data,array('debug'=>0));
        }//END MODIFY
        else {//END ADD
            $ar=$Efields->AddExtraField($posted_data,array('debug'=>0));
        }//END NEW
         echo json_encode($ar);
         exit();
    }//END SAVE FIELD
    elseif ($_GET['mode'] == 'ActivateChecked') {
    	$posted_data= $_POST['data'];
        $sql->db_Update("extra_fields","active = 'Y' WHERE fieldid IN (".implode(",",$posted_data['checkboxes']).") AND module = '".$_GET['module']."'");
         echo (is_array($posted_data['checkboxes'])) ? json_encode($posted_data['checkboxes']) : json_encode(array($posted_data['checkboxes']));
        exit();
    }//END ACTIVATE
    elseif ($_GET['mode'] == 'DeactivateChecked') {
    	$posted_data= $_POST['data'];
        $sql->db_Update("extra_fields","active = 'N' WHERE fieldid IN (".implode(",",$posted_data['checkboxes']).") AND module = '".$_GET['module']."'");  
          echo (is_array($posted_data['checkboxes'])) ? json_encode($posted_data['checkboxes']) : json_encode(array($posted_data['checkboxes']));
        exit();
    }//END DEACTIVATE
    elseif ($_GET['mode'] == 'DeleteChecked') {
    	$posted_data= $_POST['data'];
        $sql->db_Delete("extra_fields","fieldid IN (".implode(",",$posted_data['checkboxes']).") AND module = '".$_GET['module']."'");    
        $sql->db_Delete("extra_field_categories","fieldid IN (".implode(",",$posted_data['checkboxes']).")"); 
        $sql->db_Delete("extra_field_values","fieldid IN (".implode(",",$posted_data['checkboxes']).")"); 
        $sql->db_Delete("extra_fields_groups_items","itemid IN (".implode(",",$posted_data['checkboxes']).")"); 
        echo (is_array($posted_data['checkboxes'])) ? json_encode($posted_data['checkboxes']) : json_encode(array($posted_data['checkboxes']));
        exit();
    }//END ACTIVATE
    elseif ($_GET['mode'] == 'DeleteCheckedGroups') {
   	   $posted_data= $_POST['data'];
       $sql->db_Delete("extra_fields_groups","id IN (".implode(",",$posted_data).")");    
       $sql->db_Delete("extra_fields_groups_items","groupid IN (".implode(",",$posted_data).")"); 
        echo json_encode(array($posted_data['delete']));
        exit();
    }//END ACTIVATE
    
}//END SAVE EFIELDS

if ($_GET['action'] == 'ExtraFields') {
	
	if ($_GET['mode'] == "template") {
		
						
			if ($_GET['fieldid']) { //EDIT A FIELD

				$Field = $Efields->ExtraField($_GET['fieldid'],array('catid_list'=>1,'debug'=>0));
				$FieldGroups = ($Efields->getItemGroups($_GET['fieldid'])) ? $Efields->getItemGroups($_GET['fieldid']) : array();
				
			}
			
				$groups = ($Efields->searchGroups(array('debug'=>0,"orderby"=>"orderby","itemCount"=>1,'searchFields'=>array('module'=>$current_module['name'])))) ? $Efields->searchGroups(array('debug'=>0,"orderby"=>"orderby","itemCount"=>1,'searchFields'=>array('module'=>$current_module['name']))) : array();
	
				foreach ($groups as $key => $value) {
					
					if (is_array($FieldGroups) AND in_array($value['id'],array_keys($FieldGroups))) {
						$groups[$key]['selected']="selected";
					} else {
						$groups[$key]['selected']="";
					}
				}
				
				
				$c = new Items(array('module'=>$current_module));
				$allCategories = $c->AllItemsTreeCategories();
				$i=0;
				foreach($allCategories as $key => $value) {
						if (isset($Field['catids'][$key])) {
							$allCategories[$key]['selected']='selected';
							$i++;
						}			
				}
			if ($i == count($allCategories)) {
				$smarty->assign("checkedAll",'checked');
			} else if ( $i == 0 ) {
				$smarty->assign("checkedNone",'checked');
			} else {
				$smarty->assign("checkedCat",'checked');
			}
		
			$Field['settings'] = ($Field['settings']) ? $Field['settings'] : array();
			$Field['catids']=$allCategories;
				
				
			$smarty->assign("extra_fields",$allCategories);
			$smarty->assign("field",$Field);
			$smarty->assign("groups",$groups);
			

			$template = $smarty->fetch($box['template']);
			$output = array('template'=>$template,'box'=>$box,'a'=>$_GET);
			
			echo json_encode($output);
			exit;
			
	} 
			
	if ($_GET['fieldid']) { //EDIT A FIELD
		
		$Field = $Efields->ExtraField($_GET['fieldid'],array('catid_list'=>1,'debug'=>0));
		$FieldGroups = ($Efields->getItemGroups($_GET['fieldid'])) ? $Efields->getItemGroups($_GET['fieldid']) : array();
		
		$c = new Items(array('module'=>$current_module));
		$allCategories = $c->AllItemsTreeCategories();

		foreach($allCategories as $key => $value) {
				if (isset($Field['catids'][$key])) {
						$allCategories[$key]['selected']='selected';
				}			
		}
		
		$Field['settings'] = ($Field['settings']) ? $Field['settings'] : array();
		$Field['catids']=$allCategories;
		
		echo json_encode(array("fieldGroups" => $FieldGroups, "field" => $Field));
		exit;
	} else {
		$c = new Items(array('module'=>$current_module));
		$allCategories = $c->AllItemsTreeCategories();
		$Field['catids']=$allCategories;
		$Field['settings']=array();
		echo json_encode(array("fieldGroups" => array(), "field" => $Field));
		exit;
	}
	
}

?>