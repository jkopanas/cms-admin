<?php
include("../init.php");
$current_module = $loaded_modules[$_POST['data']['module']];
$Content = new Items(array('module'=>$current_module));

$posted_data = json_decode(base64_decode($_POST['data']['posted_data']),true);

$smarty->assign("posted_data",$posted_data);
//print_r($posted_data);
$mode = ($_POST['mode'] == 'true') ? 1 : 0;

/*if ($posted_data['filter']) {
	foreach ($posted_data['filter'] as $k=>$v)
	{
		if ($k == 'efields') {
			foreach ($v as $key=>$val)
			{
				$searchHooks[$key] = $val;
			}
		}
		else {
			$searchHooks[$k] = $v;
		}
	}
}*/

if ($_POST['efields']['filters']) {
	foreach ($_POST['efields']['filters'] as $v)
	{
		$f  = form_settings_array(base64_decode($v));
		$searchHooks[] = $current_module['name'].".id IN (SELECT itemid FROM extra_field_values WHERE fieldid = ".$f['fieldid']." AND value =  '".$f['value']."')";
		$efieldFilters[md5($f['fieldid']."-".$f['value'])] = $f['value'];
		$f[md5($f['fieldid']."-".$f['value'])]['type'] = 'efield';
		$appliedFilters[md5($f['fieldid']."-".$f['value'])] = $f;
	}
	$smarty->assign('efieldFilters',$efieldFilters);
}//END EXTRA FIELDS

$posted_data['page'] = ($_POST['page']) ? $_POST['page'] : 1 ;
$posted_data['sort'] = ($_POST['sort']['orderby']) ? $_POST['sort']['orderby'] : $current_module['settings']['default_sort'];
$posted_data['sort_direction'] = ($_POST['sort']['way']) ? $_POST['sort']['way'] : $current_module['settings']['default_sort_direction'];

$filterEncoded = ($_POST['data']['filter']) ? $_POST['data']['filter'] : $_POST['filter'];

$filter = form_settings_array(base64_decode($filterEncoded));
if ($filter) {

if ($filter['type'] == 'commonCategory') {
	if ($posted_data['filter']['commonCategories'][$filter['tableID']][$filter['categoryid']]) {
		$searchHooks[] = $posted_data['filter']['commonCategories'][$filter['tableID']][$filter['categoryid']];
	}
	else {
	if ($filter['categoryid']) {
		
	$sql->db_Select("common_categories_tree","categoryid_path","categoryid = '".$filter['categoryid']."'");
	$r = execute_single($sql);
	$categoryid_path = $r['categoryid_path'];
	$sql->db_Select("common_categories_tree","categoryid","categoryid = '".$posted_data["categoryid"]."' OR categoryid_path LIKE '$categoryid_path/%'");
	$categoryids_tmp = execute_multi($sql,0);

	if ($categoryids_tmp) {
		foreach ($categoryids_tmp as $v)
		{
			$catids[] = $v['categoryid'];
		}
		$catids[] = $filter['categoryid'];
		$catids = " IN (".implode(',',$catids).")";
	}
	else {
		$catids = " = ".$filter['categoryid'];
	}
		$posted_data['filter']['commonCategories'][$filter['tableID']][$filter['categoryid']] = $searchHooks[] = $current_module['name'].".id IN (SELECT itemid FROM common_categories_tree_items WHERE tableID = ".$filter['tableID']." AND catid $catids)";
	}
	}

	$s = $Content->CommonCategoriesTree($filter['tableID'],$filter['categoryid'],array('fields'=>'categoryid,category,tableID'));
	if ($s) {
		foreach ($s as $v) {
			$display['commonCategories'][$filter['tableID']][] = array('item'=>$v,'val' => base64_encode('tableID:::'.$v['tableID'].'###categoryid:::'.$v['categoryid'].'###type:::commonCategory###field:::'.$v['category']));
		}
	}
}//END COMMON CATEGORIES
elseif ($filter['type'] == 'category') {
//	$posted_data['categoryid'] = $filter
}//END CATEGORIES
	$smarty->assign("filter",$filterEncoded);
}
if ($_POST['price']) {
$searchHooks[] = 	$current_module['name'].".id IN (SELECT bookings_periods.itemid FROM bookings_seasons INNER JOIN bookings_periods ON (bookings_seasons.id=bookings_periods.seasonID) WHERE price BETWEEN ".$_POST['price']['minPrice']." AND ".$_POST['price']['maxPrice']." GROUP BY itemid)";
}
if ($_POST['categoryid']) {
	$tmp = form_settings_array(base64_decode($_POST['categoryid']));
	$posted_data['categoryid'] = $tmp['categoryid'];
} else { 
	unset($posted_data['categoryid']);
}

if (strstr($_POST['sort']['orderby'],'price')) {
	list($sort,$way)=split("-",$_POST['sort']['orderby']);
	$fieldsHooks[] = "@itemid := listings.id,(SELECT AVG(price) from bookings_periods WHERE itemid = @itemid) as price";
	$posted_data['sort_direction'] = $way;
	$posted_data['sort'] = $sort;
}

$posted_data['availability'] = 1;
$posted_data['justIds'] = 0;

$items = $Content->ItemSearch($posted_data,array('hooks'=>$searchHooks,'tablesHooks'=>$tablesHooks,'fieldsHooks'=>$fieldsHooks),$posted_data['page'],0);

######################### GRAB RESULT SPECIFIC STUFF #############################
$posted_data['justIds'] = 1;

$allItemIds = $Content->ItemSearch($posted_data,array('hooks'=>$searchHooks,'tablesHooks'=>$tablesHooks,'fieldsHooks'=>$fieldsHooks),$posted_data['page'],0);

if ($allItemIds) {
	$implodedItemIds = implode(',',$allItemIds);
	$Efields =  new ExtraFields(array('module'=>array('name'=>$current_module['name'])));
	$efieldValues = $Efields->filterItemsEfields($implodedItemIds,$current_module['name'],array('grouped'=>1,'debug'=>0,'searchable'=>1,'count'=>1,'values'=>1));
//	$smarty->assign('efields',$efieldValues);
if ($efieldValues) {
	

	foreach ($efieldValues as $k=>$v){
		foreach ($v['data'] as $b=>$m) {
			if ($m['type'] == 'radio'){
			$efieldValues[$k]['data'][$b]['id'] = md5($m['fieldid']."-".$m['values'][0]['value']);
			}
			else{
				foreach ($efieldValues[$k]['data'][$b]['values'] as $g=>$r){
					$efieldValues[$k]['data'][$b]['values'][$g]['id'] = md5($m['fieldid']."-".$r['value']);
				}
			}
		}
	}
}
	$filters = $Content->applyCategoryFilters($current_module['name'],$implodedItemIds,array('commonCategories'=>1,'efields'=>1,'recursive'=>1));
	$smarty->assign("commnonCategories",$filters['commonCategories']);	
	
}

$filter = ($filter['tableID'] AND $filter['categoryid']) ? $filter : array('tableID'=>1,'categoryid'=>0);
	$cat = $Content->CommonCategoriesTree($filter['tableID'],$filter['categoryid'],array('debug'=>0,'fields'=>'categoryid,category','num_items'=>1,'num_subs'=>1,'filteredItems'=>$implodedItemIds));

	if ($cat) {
		$nav = $Content->commonTreeCatNav($filter['tableID'],$filter['categoryid'],array('debug'=>0));
		if ($nav) {
			foreach ($nav as $k=>$v){ 
				$nav[$k]['link'] = base64_encode('tableID:::'.$filter['tableID'].'###categoryid:::'.$v['categoryid'].'###type:::commonCategory###field:::'.$v['category']);
			}
		}
		$display['commonTree'][$filter['tableID']]['nav'] = $nav;
		$display['commonTree'][$filter['tableID']]['items'] = $cat;
		foreach ($display['commonTree'][$filter['tableID']]['items'] as $k=>$v) {
			$display['commonTree'][$filter['tableID']]['items'][$k]['link'] = base64_encode('tableID:::'.$filter['tableID'].'###categoryid:::'.$v['categoryid'].'###type:::commonCategory###field:::'.$v['category']);
		}
	}


	if ($loaded_modules['bookings']) {
		$book = new bookings(array('module'=>$current_module));
		$display['priceRanges'] =$book->priceRanges($allItemIds);
	}

         if ($loaded_modules['maps']) {
         	$settings = array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid','debug'=>0);
         	$map = new maps(array('module'=>$current_module));
         	$mapItems = $map->getItems($implodedItemIds,$settings);
			if ($mapItems){ $m = $map->formatItems($mapItems,array('module'=>$current_module)); }
				
				$display['map'] = $m;
         	
         }//END MAPS
######################### END GRAB RESULT SPECIFIC STUFF #########################
$smarty->assign("ajax",1);
$smarty->assign("prefix",$_POST['data']['prefix']);
$smarty->assign("current_module",$current_module);

$smarty->assign("items",$items['results']);
$display['items'] = $smarty->fetch("modules/themes/items.tpl");
$display['itemsCount'] = count($allItemIds);
$display['efields'] = $efieldValues;
$display['locations'] = $filters['commonCategories'];
$display['list'] = $smarty->getTemplateVars('list');

$display['filters'] = json_encode($appliedFilters);
echo json_encode($display);
//$smarty->display("modules/themes/items.tpl");
?>