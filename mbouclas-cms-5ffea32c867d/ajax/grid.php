<?php
include($_SERVER['DOCUMENT_ROOT']."/ajax/init.php");
include($_SERVER['DOCUMENT_ROOT']."/modules/common_functions.php");
define("DEFAULT_LANG","en");

define("SKIN_PATH",ABSPATH."/skin");
define("SKIN_URL","/skin");
define("SKIN_URL",$skin['skin_url']);
define("TEMPLATES_PATH",ABSPATH."/templates_c".$skin['skin_path']);
include(ABSPATH."/includes/smarty.php");

$page = (!$_REQUEST['page']) ? 1 : $_REQUEST['page']; // POST the requested page
$results_per_page = ($_REQUEST['limit']) ? $_REQUEST['limit'] : 10; // POST how many rows we want to have into the grid

$sidx = ($_REQUEST['sidx']) ? $_REQUEST['sidx'] : "date_added"; // POST index row - i.e. user click to sort
$sord = ($_REQUEST['sord']) ? $_REQUEST['sord'] : "DESC"; // POST the direction
if(!$sidx) $sidx =1;
// connect to the database
$posted_data = array();

		 

if ($_REQUEST['module'] == 'products') 
{
	$module = module_is_active("products",1,1);
	if ($_REQUEST['mode'] == 'latest') 
	{
		
		$posted_data = array();//clear array
		 foreach ($_REQUEST as $key => $value) //build the array that will hold the search data
		 {   
			 $posted_data[$key] = $value;
		 }//END OF FOREACH
		 $posted_data['sort_direction'] = $sord;
		 $posted_data['sort'] = $sidx;
		 $posted_data['results_per_page'] = $results_per_page;
		 $posted_data['start'] = $_REQUEST['start'];		 
		 if ($_REQUEST['title']) { $posted_data['substring'] = $_REQUEST['title']; $posted_data['search_main'] = 1;}
		 if ($_REQUEST['query']) { $posted_data['substring'] = $_REQUEST['query']; $posted_data['search_main'] = 1;}
if ($_REQUEST['fields']) 
{
$_REQUEST['fields'] = str_replace('\"',"",$_REQUEST['fields']);//This is how ext transmits checkboxes
$_REQUEST['fields'] = str_replace('[',"",$_REQUEST['fields']);//This is how ext transmits checkboxes
$_REQUEST['fields'] = str_replace(']',"",$_REQUEST['fields']);//This is how ext transmits checkboxes
$fields = explode(",",$_REQUEST['fields']);
foreach ($fields as $v) 
{
//	echo $v;
}
}//END FIELDS FILTERS

if ($_REQUEST['filter']) 
{
$posted_data['search_main'] = 1;	

foreach ($_REQUEST['filter'] as $f_key => $f_value) 
{
	$f_value['data']['value'] = ($f_value['data']['value'] == 'false') ? 0 : 1;
	if ($f_value['field'] == 'title') 
	{
		$posted_data['substring'] = $f_value['data']['value'];
	}
	elseif ($f_value['field'] == 'active')
	{
		$posted_data['availability'] = $f_value['data']['value'];
	}
	else {
		$posted_data[$f_value['field']] = $f_value['data']['value'];
	}
}
}//END FILTERS

	if (!$posted_data['availability'] OR $posted_data['availability'] == 0) 
	{
		$posted_data['availability'] = (!$_REQUEST['availability']) ? 3 : $_REQUEST['availability'];
	}
		 $posted_data['categoryid'] = (!$_REQUEST['categoryid']) ? "%" : $_REQUEST['categoryid'];
		$res = search($posted_data,$module,$db_info,DEFAULT_LANG,0,0,0);
		$smarty->assign("MODULE_FOLDER",URL."/".$module['folder']."/admin");
		$smarty->assign("MODULE_SETTINGS",$module['settings']);
		$smarty->assign("current_page",$page);
		$smarty->assign("data",$res['data']);
		$smarty->assign("items",$res['results']);
		$smarty->assign("mode","items");
	}//END ITEMS
	if ($_REQUEST['mode'] == 'categories') //RETURN A LIST OF CATEGORIES
	{
		$smarty->assign("allcategories",get_all_categories(DEFAULT_LANG));//assigned template variable allcategories
		$smarty->assign("mode","categories");
	}
		$smarty->assign("module",$_REQUEST['module']);
}//END PRODUCTS
elseif ($_REQUEST['module'] == 'content')
{
	$content_module = module_is_active("content",1,1);
	if ($_REQUEST['mode'] == 'latest') 
	{
		
		$posted_data = array();//clear array
		 foreach ($_REQUEST as $key => $value) //build the array that will hold the search data
		 {   
			 $posted_data[$key] = $value;
		 }//END OF FOREACH
		 $posted_data['sort_direction'] = $sord;
		 $posted_data['sort'] = $sidx;
		 $posted_data['results_per_page'] = $results_per_page;
		 $posted_data['start'] = $_REQUEST['start'];		 
		 if ($_REQUEST['title']) { $posted_data['substring'] = $_REQUEST['title']; $posted_data['search_main'] = 1;}
		 if ($_REQUEST['query']) { $posted_data['substring'] = $_REQUEST['query']; $posted_data['search_main'] = 1;}
if ($_REQUEST['fields']) 
{
$_REQUEST['fields'] = str_replace('\"',"",$_REQUEST['fields']);//This is how ext transmits checkboxes
$_REQUEST['fields'] = str_replace('[',"",$_REQUEST['fields']);//This is how ext transmits checkboxes
$_REQUEST['fields'] = str_replace(']',"",$_REQUEST['fields']);//This is how ext transmits checkboxes
$fields = explode(",",$_REQUEST['fields']);
foreach ($fields as $v) 
{
//	echo $v;
}
}//END FIELDS FILTERS

if ($_REQUEST['filter']) 
{
$posted_data['search_main'] = 1;	

foreach ($_REQUEST['filter'] as $f_key => $f_value) 
{
	$f_value['data']['value'] = ($f_value['data']['value'] == 'false') ? 0 : 1;
	if ($f_value['field'] == 'title') 
	{
		$posted_data['substring'] = $f_value['data']['value'];
	}
	elseif ($f_value['field'] == 'active')
	{
		$posted_data['availability'] = $f_value['data']['value'];
	}
	else {
		$posted_data[$f_value['field']] = $f_value['data']['value'];
	}
}
}//END FILTERS

	if (!$posted_data['availability'] OR $posted_data['availability'] == 0) 
	{
		$posted_data['availability'] = (!$_REQUEST['availability']) ? 3 : $_REQUEST['availability'];
	}
		 $posted_data['categoryid'] = (!$_REQUEST['categoryid']) ? "%" : $_REQUEST['categoryid'];
		$res = content_search($posted_data,$content_module,$db_info,DEFAULT_LANG,0,0,0);
		$smarty->assign("MODULE_FOLDER",URL."/".$content_module['folder']."/admin");
		$smarty->assign("MODULE_SETTINGS",$content_module['settings']);
		$smarty->assign("current_page",$page);
		$smarty->assign("data",$res['data']);
		$smarty->assign("items",$res['results']);
		$smarty->assign("mode","items");
	}//END ITEMS
	if ($_REQUEST['mode'] == 'categories') //RETURN A LIST OF CATEGORIES
	{
		$smarty->assign("allcategories",get_all_content_categories(DEFAULT_LANG));//assigned template variable allcategories
		$smarty->assign("mode","categories");
	}
	$smarty->assign("module",$_REQUEST['module']);
}//END CONTENT
	if ($_REQUEST['task'] == 'update') 
	{
		$active = $_REQUEST['active'];
		if ($_REQUEST['module'] == 'products') 
		{
			$sql->db_Update("products","active = $active WHERE id = ".$_REQUEST['id']);
			$sql->db_Update("lng","title = '".$_REQUEST['title']."' WHERE productid = ".$_REQUEST['id']);
			//Check the category by name
			$tmp = explode("/",$_REQUEST['category']);
			$cat = $tmp[count($tmp)-1];
			$sql->db_Select("categories_lng","categoryid","category = '$cat'");
			$res = execute_single($sql,1);
			$catid = $res['categoryid'];
			$sql->db_Update("page_categories","catid = '$catid' WHERE main = 'Y' AND productid = '".$_REQUEST['id']."'");
			echo 1;
			exit();
		}
		elseif ($_REQUEST['module'] == 'content')
		{
			$sql->db_Update("content","active = $active WHERE id = ".$_REQUEST['id']);
			$sql->db_Update("content_lng","title = '".$_REQUEST['title']."' WHERE contentid = ".$_REQUEST['id']);
			//Check the category by name
			$tmp = explode("/",$_REQUEST['category']);
			$cat = $tmp[count($tmp)-1];
			$sql->db_Select("content_categories_lng","categoryid","category = '$cat'");
			$res = execute_single($sql,1);
			$catid = $res['categoryid'];

			$sql->db_Update("content_page_categories","catid = '$catid' WHERE main = 'Y' AND contentid = '".$_REQUEST['id']."'");
			echo 1;
			exit();
		}

	}


///////////// CRUCIAL!!!!!!!!!!!!!!!! 
 if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) 
 { header("Content-type: application/xhtml+xml;charset=utf-8"); } 
 else { header("Content-type: text/xml;charset=utf-8"); }
$smarty->display("modules/categories_xml.tpl");

?>