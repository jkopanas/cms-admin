<?php
define("ABSPATH",$_SERVER['DOCUMENT_ROOT']);
include("../includes/config.php");
include("../includes/dataStore.php");
include("../includes/sql_driver.php");
include("../includes/pagination.php");
include("../includes/functions.php");
include("../includes/common.php");
include("../includes/pass_crypt.php");
include(ABSPATH."/manage/security.php");
include("../includes/smarty.php");
define('SKIN_DIR','../skin/admin2/');
define('CURRENT_MODULE',$_GET['module']);
################### BEGIN Cryptography Settings #####################################################
$CRYPT_SALT = 85; # any number ranging 1-255
$START_CHAR_CODE = 100; # 'd' letter
################### END Cryptography Settings #######################################################
spl_autoload_register('smartyAutoload'); 
spl_autoload_register("__autoload");

function __autoload($class_name)
{
    require_once ABSPATH.'/modules/'."$class_name.php";
//  throw new Exception("Unable to load ".ABSPATH.'/modules/'."$class_name.php");
}
define("e_SELF", "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']); //full address + filename
define("e_QUERY", eregi_replace("&|/?PHPSESSID.*", "", $_SERVER['QUERY_STRING']));//everything on the string after the ?
define('e_BASE',$link_prefix);
define("e_REQUEST_URI",$_SERVER['REQUEST_URI']);
define("e_FILE",basename($_SERVER['PHP_SELF']));
define("TIME_LOGIN",12);//HOURS TO STAY LOGED IN, IF WE NEED DAYS WE MULTIPLY HOURS BY DAYS. e.g. 24*2 (2 days)


$sql = new db();
$sql->db_Connect(DBHOST,DBUSER,DBPASS,DB);
############################# BEGIN GLOBAL VARIABLE DEFINITIONS #####################################
$config["Sessions"]["session_length"] = 3600;
define("USER_TYPE","visitor");//for stats purposes

define("e_HTTP", "/");
$url_prefix=substr($_SERVER['PHP_SELF'],strlen(e_HTTP),strrpos($_SERVER['PHP_SELF'],"/")+1-strlen(e_HTTP));
$num_levels=substr_count($url_prefix,"/");

init_session();//get the user data id a cookie or session is set

for($i=1;$i<=$num_levels;$i++)
{
	$link_prefix.="../";
}
			
define("e_SELF", "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']); //full address + filename
define("e_QUERY", eregi_replace("&|/?PHPSESSID.*", "", $_SERVER['QUERY_STRING']));//everything on the string after the ?
define('e_BASE',$link_prefix);
define("e_REQUEST_URI",$_SERVER['REQUEST_URI']);


/* PLUGINS */
if (!empty($plugins)) {
	foreach ($plugins as $k=>$v) {
		if (in_array("X", $v['area']) ) {
			include_once(ABSPATH."/plugins/".$v['path']);
		}
	}
}


############################# BEGIN SITE PREFS ##################################################
$prefs = load_prefs();
foreach ($prefs as $key => $value) 
{ 
   	if (!preg_match("[0-9]",$key)) 
	{
		define(strtoupper($key),$value);
	}//END OF IF
}//END OF FOR 

$Languages = new Languages();
if ($_GET['lang']) 
{ 
$Languages->setLanguage($_GET['lang']);
$front_language = $Languages->checkLanguage($_GET['lang'],'Y');//ONLY ACTIVE LANGUAGES
if (DEFAULT_LANG != $_GET['lang']) {
	define('TRANSLATE',$_GET['lang']);
}
}//END OF IF
if (empty($front_language)) 
{  
 	$lang = $Languages->LoadLanguage(DEFAULT_LANG,array('common','front'));
 	define("FRONT_LANG",DEFAULT_LANG);
 	$front_language = DEFAULT_LANG;
}//END OF IF
else 
{  
	$front_language = $Languages->checkLanguage($front_language);
	$lang = $Languages->LoadLanguage($front_language);
}//END OF ELSE
############################# END GLOBAL VARIABLE DEFINITIONS ###################################

############################# BEGIN LOAD LANG ###################################################
//check if language change
$Languages = new Languages();

if ($_GET['sl']) 
{  
	$Languages->setLanguage($_GET['sl'],'back_lang');
	$back_language = $Languages->checkLanguage($_GET['sl']);
}//END OF IF
if (empty($back_language)) 
{  
	$lang = $Languages->LoadLanguage(DEFAULT_LANG_ADMIN);
	define("BACK_LANG",DEFAULT_LANG_ADMIN);
	$back_language = DEFAULT_LANG_ADMIN;
}//END OF IF
else 
{  
	$back_language = $Languages->checkLanguage($back_language);
	$lang = $Languages->LoadLanguage($back_language);
}//END OF ELSE
define("BACK_LANG",$back_language);
$smarty->assign("BACK_LANG",$back_language);//assigned template variable front_language
$smarty->assign("lang",$lang);//assigned template variable lang
$country = $Languages->get_country(DEFAULT_LANG_ADMIN);
setlocale (LC_ALL, $country['locale']);
############################# END LOAD LANG ###################################################

$loaded_modules = load_modules(1,0,array('return'=>'byKey','fields'=>'id,name'));
$smarty->assign('loaded_modules',$loaded_modules);
############################# LOAD ACTIVE MODULES ###############################################
	 $loaded_modules = array();
	 global $loaded_modules;
	 $modules_list = load_modules(1,0,array('return'=>'byKey','fields'=>'id,name,startup'));
############################# END LOAD ACTIVE MODULES ###########################################
########################### LOAD MODULES ###################################
foreach ($modules_list as $m)
{
	if ($m['startup'] == 1) 
	{
			if ($loaded_modules_array[] =  module_is_active($m['name'],1,1))
		{		
			$smarty->assign($m['name'],$loaded_modules[$m['name']]);
			if (strstr($_SERVER['PHP_SELF'],$m['name']))
			{
				define('CURRENT_MODULE',$m['name']);
			}
			elseif ($_GET['module'])//AJAX
			{
				define('CURRENT_MODULE',$_GET['module']);
			}


			
		}
	}
}




?>