<?php

$cur_module = module_is_active($_GET['module'],1,1);
$module_cat= new Items(array('module'=>module_is_active($_GET['module'],1,1)));
$module = $_GET['module'];

if ($_GET['action'] == "Viewtables") {
	$TreeCategories = $module_cat->ListCommonCategories($module,array('type'=>'tree'));
	 if (!empty($TreeCategories)) {     	
			echo json_encode($TreeCategories);
	 } else {
	 		$results=array("results"=>"null");
	 		echo json_encode($results);
	 }
	exit;
}//END LIST TABLES

if ($_GET['action'] == "AddTable") {
	
	$_POST['data']['module']=$module;
	$_POST['data']['type']='tree';
	
	foreach ($_POST['data'] as $k => $v)
		{
			$keys[] = $k;
			$values[] = "'$v'";
		}
		
	$sql->db_Insert("common_categories_tables"." (".implode(",",$keys).")",implode(",",$values));
	
} //end action

if ($_GET['action'] == "deleteTable") {
	
	$sql->db_Delete('common_categories_tables',"id=".$_GET['id']);
	$sql->db_Delete('common_categories_tree',"tableID=".$_GET['id']);
	
} //end action


if ($_GET['action'] == "updateTable") {
	
	foreach ($_POST['models']['0'] as $k=>$v)
	{
		$update[] = "$k = '$v'";
	}
	$sql->db_Update("common_categories_tables",implode(",",$update)." WHERE id = ".$_POST['models']['0']['id']);
	
} //end action



if ($_GET['action'] == "getCategories") {

   if (isset ($_POST['node_id']) && $_POST['node_id']!="") {
		
   		$id=explode("list",$_POST['node_id']);
		$cat=$id[1];
	}
	 else {

	 	$cat=0;
	 }

	$categories=$module_cat->CommonCategoriesTree($_GET['tableID'],$cat,array('debug'=>0,'num_items'=>1,'num_subs'=>1));	
	
	if (!empty($categories)){
	    foreach ($categories as $key=>$value) {

		  $subs = ($value['num_sub']!=0) ? $subs='1' : $subs='0';
		  $childs[]=array('id'=>$value['categoryid'],'parentid'=>$value['parentid'],'text'=>$value['category'],'sub_cat'=>$subs);
	
	    }
    }
  echo json_encode(array( "results" => $childs));
  exit;
	

} //end action

if ($_GET['action']=='open_form'){

	 	$sql->q("SELECT categoryid,category from common_categories_tree where module="."'$module'");

		$categories=execute_multi($sql);
		$smarty->assign("categories",$categories);
		$smarty->assign("themes",get_themes());
		 $p = new SettingsManager();
		 $box['file']='ajax/items/itemsGeneralForm.php';
		$smarty->assign("pageSettings",$p->getSettings($module,basename($box['file']),array('debug'=>0)));
		echo $smarty->fetch('modules/common_categories/edit_add_form.tpl');
}// end action

if ($_GET['action']=='new_category'){
 	
		$sql->q("SELECT MAX(order_by) as id FROM common_categories_tree where tableID=".$_GET['tableID']); 
		$order_by=execute_single($sql);
		$_POST['data']['orderby']=($order_by['id']=="")? 0 : $order_by['id']+1; 
			
		$ClsCat = new CategoriesActions(array('module'=>$loaded_modules[$module],'debug'=>0));
		$cat = $ClsCat->AddCommonTreeCategory($_GET['tableID'],$_POST['data'],array('return'=>'cat','file'=>'content_category','debug'=>0));
		$sql->db_Select('common_categories_tree',"categoryid,categoryid_path","category='".$_POST['data']['title']."'");
		$result=execute_single($sql);
		
		$has_parent = ($_POST['data']['parent_catid']!=0) ? $subs='1' : $subs='0';
 		$new_node=array('id'=>$result['categoryid'],'text'=>$_POST['data']['title'],'has_parent'=>$subs,'parent'=>$_POST['data']['parent_catid'],'path'=>$result['categoryid_path']);
 	
 		echo json_encode($new_node);
		exit;
		
} //end action


if ($_GET['action']=='edit_category'){
 	 
 	 	if (isset ($_GET['node_id']) && $_GET['node_id']!="") {

			$id=explode("edit",$_GET['node_id']);
			$cat=$id[1];
 	 		$cat_details=$module_cat->commonCategoryTree($_GET['tableID'],$cat);
 	 		$sql->q("SELECT categoryid,category from common_categories_tree");
			$categories=execute_multi($sql);
			$smarty->assign("categories",$categories);
 	 		$smarty->assign("cat_details",$cat_details);
 	 		$smarty->assign("themes",get_themes());
 	 		 $p = new SettingsManager();
			$smarty->assign("pageSettings",$p->getSettings($module,basename($box['file']),array('debug'=>0)));
	       echo $smarty->fetch('modules/common_categories/edit_add_form.tpl');
 	  } 
} //end action
 	
if ($_GET['action']=='edit_category_item'){
 		$sql->db_Select('common_categories_tree',"categoryid_path,order_by","categoryid='".$_POST['data']['old_id']."'");
		$result=execute_single($sql);
		
 		$data = $_POST['data'];
		$data['cat'] = $_POST['data']['old_id'];
		$data['description']=$_POST['data']['desc'];
		$data['orderby']=$result['order_by'];
		
		$ClsCat = new CategoriesActions(array('module'=>$loaded_modules[$module],'debug'=>0));
		
 		$ClsCat->ModifyCommonTreeCategory($_GET['tableID'],$data,array('debug'=>0));
		$has_parent = ($_POST['data']['parent_catid']!=0) ? $subs='1' : $subs='0';
 		$updated_node=array('id'=>$_POST['data']['old_id'],'text'=>$_POST['data']['title'],'has_parent'=>$subs,'parent'=>$_POST['data']['parent_catid'],'path'=>$result['categoryid_path']);
 	
 		echo json_encode($updated_node);
		exit;
}//end action


if ($_GET['action']=='delete_category'){
 	
  if (isset ($_POST['node_id']) && $_POST['node_id']!="") {

		$id=explode("delete",$_POST['node_id']);
		$cat=$id[1];
	
 	
	    $sql->db_Select("common_categories_tree","categoryid","categoryid_path LIKE '%$cat/%'");
	 if ($sql->db_Rows() > 0) {
	 	$tmp = execute_multi($sql,0);
	 	for ($i=0;count($tmp) > $i;$i++)
	 	{
	 	$sql->db_Delete("common_categories_tree","tableID = ".$_GET['tableID']." AND categoryid=".$tmp[$i]['categoryid']);
	 	$sql->db_Delete("common_categories_tree_items","catid=".$tmp[$i]['categoryid']);
	 	
//	 	echo "DELETE FROM categories WHERE categoryid=".$tmp[$i]['categoryid'].";<br>";
	 	}
	 }
//	 echo "DELETE FROM categories WHERE categoryid=".$cat.";<br>";
	 $sql->db_Delete("common_categories_tree","categoryid= $cat");
	 $sql->db_Delete("common_categories_tree_items","catid=$cat");
	    
	$main_cateogory[]=$cat;
   if (!empty($tmp)){
	 foreach ($tmp as $key=>$value) {
	 	foreach ($value as $k=>$v) {
	 		$more_cat[]=$v;
	 }
	 	 }
	 $categories=array_merge($main_cateogory,$more_cat);
	
	foreach($categories as $key=>$value) {
	 	$cats[]=array("id"=>$value);
	 }
  } else {
		
		$cats=array("id"=>$main_cateogory[0]);
	}
	 
	
		 echo json_encode($cats);
		 exit;
 }
} //end action



if ($_GET['action']=='change_order'){

 if (isset($_POST['source']) && $_POST['source']!="" && isset($_POST['destination']) && $_POST['destination']!="") {
 			
 		$dest=explode("parent_list",$_POST['destination']);
 		$sor=explode("parent_list",$_POST['source']);
 		$destination=$dest[1];
 		$source=$sor[1];

 		$sql->db_Select("common_categories_tree","categoryid,categoryid_path,parentid,order_by","categoryid='$destination'");
 		$parent_path=execute_single($sql);
 		$sql->db_Select("common_categories_tree","categoryid_path,parentid","categoryid='".$parent_path['categoryid']."'");
 		$pa_paths=execute_single($sql);
 		$sql->db_Select("common_categories_tree","categoryid_path","categoryid='".$pa_paths['parentid']."'");
 		$par_paths=execute_single($sql);
 	    $sql->db_Select("common_categories_tree","categoryid,categoryid_path,parentid","categoryid_path LIKE '%$source%'");
 	    $children_paths=execute_multi($sql);
 	   
 	   /// start of mergin and unmergin nodes /////
 	   
 	foreach ($children_paths as $key=>$value) {	
 	     $arr[$value['categoryid']]=array($value['categoryid_path'],$value['parentid']);   	
 	  }
 	   
 	   foreach ($children_paths as $key=>$value) {
 	    	 if (empty($arr[$value['parentid']])) {
 	   		       $p=$value['parentid'];
 	    	 }
 	   }
 
 	 foreach ($children_paths as $key=>$value) {
 	    	$pnew=explode($p."/",$value['categoryid_path']);
 	    	$pnew=end($pnew);

 	  if (empty($arr[$value['parentid']])){
 		if (!empty($_GET['over'])){
 			  $sql->q("UPDATE common_categories_tree SET categoryid_path='".$parent_path['categoryid_path']."/".$pnew."' ,parentid=".$destination." WHERE categoryid=".$source." and tableID=".$_GET['tableID']); 	
 		 } else {	
 		 	$new_path=($parent_path['parentid']!=0)? $par_paths['categoryid_path']."/".$source : $source;	
 			$sql->q("UPDATE common_categories_tree SET categoryid_path='".$new_path."' ,parentid='".$parent_path['parentid']."' WHERE categoryid=".$source." and tableID=".$_GET['tableID']);
 		 }
 	  } else {
 	  		if (!empty($_GET['over'])){
 			   $sql->q("UPDATE common_categories_tree SET categoryid_path='".$parent_path['categoryid_path']."/".$pnew."' WHERE categoryid=".$value['categoryid']." and tableID=".$_GET['tableID']); 	
 			} else {
 			     $new_path=($parent_path['parentid']!=0) ? $par_paths['categoryid_path']."/".$pnew : $pnew;
 			    $sql->q("UPDATE common_categories_tree SET categoryid_path='".$new_path."' WHERE categoryid=".$value['categoryid']." and tableID=".$_GET['tableID']);	
 			}
 	  	}
 	 }	/// end of mergin and unmergin nodes /////	
 	  			
 	   
 					/// rearrange nodes on before after/////	
if( $_POST['drop_position']!='over') {
  
 	 $sql->db_Select("common_categories_tree","categoryid,order_by","parentid='".$parent_path['parentid']."'and order_by >='".$parent_path['order_by']."' and tableID=".$_GET['tableID']);  	 
 	 $paths=execute_multi($sql);
 		
		foreach ($paths as $key=>$value){
			$paths_cat[$value['categoryid']]=$value['order_by'];					
		 }
 		
		unset($paths_cat[$source]);
		
	 if($_POST['drop_position']=='before' ){	
			$sql->q("UPDATE common_categories_tree SET order_by=".$parent_path['order_by']." WHERE categoryid=".$source);
	 } else if ($_POST['drop_position']=='after' ) {
		  unset($paths_cat[$parent_path['categoryid']]);
		  $new_order=$parent_path['order_by']+1;
		  $sql->q("UPDATE common_categories_tree SET order_by=".$new_order." WHERE categoryid=".$source);
	   }
		 
		 $value="";
	  foreach ($paths_cat as $key=>$value){
	  		  $value=$value+1;
		 $sql->q("UPDATE common_categories_tree SET order_by=".$value." WHERE categoryid=".$key);
		 $value="";
	  }
 }
 
  if( $_POST['drop_position']=='over') {
  	$sql->q("SELECT MAX(order_by) as id FROM common_categories_tree where parentid=".$parent_path['categoryid']);
  	$max_order=execute_single($sql);
  	$new_order=$max_order['id']+1; 
  	$sql->q("UPDATE common_categories_tree SET order_by=".$new_order." WHERE categoryid=".$source);
  }
 
 }// end if	
} //end action change_order
   /// end rearrange nodes on before after/////

?>