<?php
$module = $current_module = $loaded_modules[$_GET['module']];
$c = new Items(array('module'=>$current_module));

$item_settings = array('fields'=>'*','thumb'=>0,'CatNav'=>0,'debug'=>0,'main'=>0,'parse'=>'toForm','GetCommonCategoriesTree'=>0,'returnSimple'=>1);
$item = $c->GetItem($_GET['id'],$item_settings);

if (is_array($item['category'])) {
foreach ($item['category'] as $v)
{
	$simplified_categories[$v['catid']] = $v['main'];
}	
}

$ExtraFields = new ExtraFields(array('module'=>$current_module,'debug'=>0));
$AllExtraFields = $ExtraFields->GetExtraFields(array('debug'=>0,'match_values'=>1,'itemid'=>$_GET['id'],'catids'=>array_flip($simplified_categories),'way'=>'ASC'));
//print_r($AllExtraFields);
$smarty->assign("extra_fields",$AllExtraFields);

$smarty->assign("item",$item);
$smarty->assign("current_module",$current_module);
$template = $smarty->fetch($box['template']);
$output = array('template'=>$template,'box'=>$box,'a'=>$_GET);
if (!$buffer) {
	echo json_encode($output);
}
?>