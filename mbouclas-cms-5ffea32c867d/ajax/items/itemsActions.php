<?php
$module = $loaded_modules[$_GET['module']];
$c = new Items(array('module'=>$module));
if ($_GET['action'] == 'saveItem') {
	$mode = ($_GET['id']) ? 'modify' : 'add';
	$save = $c->saveItem($_POST,$_GET['id'],array('complexFields'=>1,'debug'=>0));
	if ($_GET['returnItem']) {
		$id = ($_GET['id']) ? $_GET['id'] : $save;
		$item = $c->GetItem($id, array('fields'=>'*','thumb'=>0,'CatNav'=>0,'debug'=>0,'main'=>0,'cat_details'=>1,'parse'=>'toForm','GetCommonCategoriesTree'=>1,'returnSimple'=>1));
		$moreItems = $c->LatestItems(array('results_per_page'=>10,'active'=>2,'get_provider'=>0,'get_user'=>0,'debug'=>0,'just_results'=>1,'location'=>0,'GetCommonCategoriesTree'=>0
		,'categoryid'=>$item['main_category']['categoryid']));
	}

	echo json_encode(array('id'=>$save,'debug'=>'','mode'=>$mode,'item'=>$item,'moreItems'=>$moreItems));
	exit();
}//END SAVE ITEM


if ($_GET['action'] == 'saveImage') {
	$current_module = $module;
	$media = (!$_GET['media']) ? 'image' : $_GET['media'];
    $MediaFilesSettings = MediaFiles(array('single'=>1,'type'=>$media)); 
    
    $targetPath = $_SERVER['DOCUMENT_ROOT'] . $MediaFilesSettings['folder']."/".$current_module['name']."_".$_GET['id'];
    if (!is_dir($targetPath)) {
    mkdir($targetPath,0755);
    }//END IF

 	$fileParam = ($_GET['fileParam']) ? $_GET['fileParam'] : $_POST['fileParam'];
    $files = $_FILES[$fileParam];
    if (isset($files['name']))
    {
        $error = $files['error'];

        if ($error == UPLOAD_ERR_OK) {
        	$targetFile = $targetPath."/".basename($files["name"]);
            $uploadedFile = $files["tmp_name"];
            if (is_uploaded_file($uploadedFile)) {
                if (!move_uploaded_file($uploadedFile, $targetFile)) {
                    echo "Error moving uploaded file";
                }
                else {
					include($_SERVER['DOCUMENT_ROOT']."/modules/common_functions.php");
						$a = new mediaFiles(array('module'=>$module));
						$fileType = (!$_GET['fileType']) ? 0 : $_GET['fileType'];
						if ($media == 'image') {
							$imageCategory = ($_GET['imageCategory']) ? $_GET['imageCategory'] : $_POST['imageCategory'];
							$imageCategory = (!$imageCategory) ? 0 : $imageCategory;
							$up = $a->handleImageUpload($_GET['id'],basename($files["name"]),$targetPath,$targetPath,array('fileType'=>$fileType,'imageCategory'=>$imageCategory,'keepOriginal'=>$module['settings']['keep_original_image']));
							if ($module['settings']['keep_original_image']) {

							}//END ORIGINALS
						}//END IMAGES
						elseif ($media == 'video') {
							$up = $a->SaveVideo($_GET['id'],basename($files["name"]),$a->CheckDocument($targetFile,$targetPath,basename($files["name"])),$_POST);
						}//END VIDEO
						else {
							$up = $a->SaveDocument($_GET['id'],basename($files["name"]),$a->CheckDocument($targetFile,$targetPath,basename($files["name"])));
						}//END OTHER TYPES
				}//END MOVE
                }//END ALL IS WELL
                

            }
        } else {
            // See http://php.net/manual/en/features.file-upload.errors.php
            echo "Error code " . $error;
        }
	
	header('Content-Type: text/plain;');
 
	$data = array('foo' => $_GET, 'status' => 'ok','uploadedFile'=>$up);
	 
	echo json_encode($data);
	exit();
}
if ($_GET['action'] == 'MediaActions') {
	$module = $_GET['module'];
	switch ($_GET['media'])
	{
		case 'document': $table = 'item_documents';
		break;
		case 'video': $table = 'item_videos';
		break;
		default: $table = 'item_images';
		break;
	}
/* NEED TO RUN CLEAN UP OF THE ACTUAL FILES */
if ($_GET['mode'] == 'enableMedia') {
	$sql->db_Update($table,"available = 1 WHERE id IN (".implode(",",$_POST['ids']).") AND module = '$module'");
//	echo "UPDATE $table SET available = 1 WHERE id IN (".implode(",",$_POST['ids']).") AND module = '$module'";
}

elseif ($_GET['mode'] == 'disableMedia') {
	$sql->db_Update($table,"available = 0 WHERE id IN (".implode(",",$_POST['ids']).") AND module = '$module'");
}
elseif ($_GET['mode'] == 'deleteMedia') {
	$sql->db_Delete($table,"id IN (".implode(",",$_POST['ids']).") AND module = '$module'");
}
elseif ($_GET['mode'] == 'updateMedia') {
	foreach ($_POST as $k=>$v)
	{
		if (strstr($k,'-'))
		{
			list($field,$id)=split("-",$k);
				$updates[$id][$field] = $v;
		}
	}
	foreach ($updates as $key=>$val) {
		$r = array();
		foreach ($val as $k=>$v) {
			$r[] = "$k = '$v'";
		}
//		$q[] = implode(",",$r). " WHERE id = $key";
//echo "UPDATE $table SET ".implode(",",$r). " WHERE id = $key\n";
		$sql->db_Update($table,implode(",",$r). " WHERE id = $key");
	}
	
}
elseif ($_GET['mode'] == 'sortOrder') {
	foreach ($_POST['ids'] as $k=>$v) {
		$sql->db_Update($table,"orderby = $k WHERE id = ".str_replace('Copies','',$v)." AND module = '".$_GET['module']."'");
//		echo "UPDATE $table SET orderby = $k WHERE id = ".str_replace('Copies','',$v)." AND module = '".$_GET['module']."'\n";
	}
	echo json_encode($_POST);
}
}//END MEDIA


if ($_GET['action'] == 'saveEfields') {
	$c->UpdateExtraFieldsValues($_POST['data'],$_GET['id']);
}
?>