<?php

$cur_module = module_is_active($_GET['module'],1,1);
$module_cat= new Items(array('module'=>module_is_active($_GET['module'],1,1)));
$module = $_GET['module'];

if ($_GET['action'] == "getCategories") {
	
	$posted_data=$_POST;
	if (isset ($_POST['node_id']) && $_POST['node_id']!="") {
		$id=explode("list",$_POST['node_id']);
		$cat=$id[1];
	} else {
		$cat=0;
	}
		
	$posted_data['page'] = ($posted_data['page']) ? $posted_data['page'] : 1;
    $posted_data['results_per_page'] = ($posted_data['pageSize']) ? $posted_data['pageSize'] : "";        
        
    $current_page = ($posted_data['page']) ? $posted_data['page'] : 1;
    $results_per_page = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $settings['settings']['items_per_page'];
	
        if (isset($posted_data['start'])) 
        {
                $start = $posted_data['start'];
        } else {
                $start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0; 
        }	

        	$posted_data['results_per_page'] = $start.",".$results_per_page;

	
	if (!$_GET['subs']) {
		
		$categories=$module_cat->ItemTreeCategories($cat,array('num_items'=>1,'num_subs'=>1,'debug'=>0));
		$categoriescount=$module_cat->ItemTreeCategories($cat,array('num_items'=>1,'num_subs'=>1,'debug'=>0));
		//	print_ar($categories);
	} else {
		$categories=$module_cat->AllItemCategories(array( 'limit' => $posted_data['results_per_page'] ));
		$categoriescount=$module_cat->AllItemCategories();
		foreach ($categories as $key=>$value) {
			$categories[$key]['category'] = end(explode("/",$value['category']));
		}
	}
	
	if (!empty($categories)){
		foreach ($categories as $key=>$value) {
			$subs = ($value['num_sub']!=0) ? $subs='1' : $subs='0';
			$childs[] = array('id'=>$value['categoryid'],'parentid'=>$value['parentid'],'text'=>$value['category'],'sub_cat'=>$subs);
		}
	}
	echo json_encode(array( "data"=> array("total" => count($categoriescount)),"results" => $childs));
	exit;

} // end action



if ($_GET['action'] == 'loadSingleTranslation') {

		$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));

		for ($i=0;count($availableLanguages) > $i;$i++)
		{	
			 $ar=$Languages->itemTranslations($_POST['id'],$availableLanguages[$i]['code'],false,$_POST);
			 $results[$i]=array_merge(array('uid' => $_POST['id'],'field' => $ar['field'],  'value' => array('category' => $ar['category']['translation'], 'description' => $ar['description']['translation'] ), 'table'=>$_POST['table'], 'code' => $availableLanguages[$i]['code']),$_POST);
		}

		$smarty->assign("LangTranslate",$results);
		echo $smarty->fetch($_POST['tpl']);
		exit();
}

if ($_GET['action']=='open_form'){

	 	$sql->q("SELECT categoryid,category from ".$module."_categories");
	 	$ar=execute_multi($sql);
		$categories = (!empty($ar)) ? $ar : array(); 
		$smarty->assign("categories",$categories);
		$smarty->assign("themes",get_themes());
		 $p = new SettingsManager();
		$box['file']='ajax/itemsCategories.php';
		$smarty->assign("pageSettings",$p->getSettings($module,basename($box['file']),array('debug'=>0)));
		echo $smarty->fetch('modules/categories/edit_add_form.tpl');
		
}// end action
	
if ($_GET['action']=='new_category'){
 	
		$sql->q("SELECT MAX(order_by) as id FROM ".$module."_categories"); 
		$order_by=execute_single($sql);
		$_POST['data']['orderby']=($order_by['id']=="")? 0 : $order_by['id']+1; 
			
		$ClsCat = new CategoriesActions(array('module'=>$loaded_modules[$module],'debug'=>0));
		$ClsCat->AddTreeCategory($module.'_categories',$_POST['data'],array('return'=>'cat','file'=>$module.'_category','debug'=>0));
		$sql->db_Select($module.'_categories',"categoryid,categoryid_path","category='".$_POST['data']['title']."'");
		$result=execute_single($sql);
		$has_parent = ($_POST['data']['parent_catid']!=0) ? $subs='1' : $subs='0';
 		$new_node=array('id'=>$result['categoryid'],'text'=>$_POST['data']['title'],'has_parent'=>$subs,'parent'=>$_POST['data']['parent_catid'],'path'=>$result['categoryid_path']);
 	
 		echo json_encode($new_node);
		exit;
		
} //end action

 if ($_GET['action']=='edit_category'){
 	 
 	 	if (isset ($_GET['node_id']) && $_GET['node_id']!="") {

			$id=explode("edit",$_GET['node_id']);
			$cat=$id[1];
 	 		$cat_details=$module_cat->ItemCategory($cat,array('table'=>'content_categories','settings'=>1));
 	 		$sql->q("SELECT categoryid,category from ".$module."_categories");
			$categories=execute_multi($sql);
			$smarty->assign("categories",$categories);
 	 		$smarty->assign("cat_details",$cat_details);
 	 		$smarty->assign("themes",get_themes());
 	 		 $p = new SettingsManager();
 	 		$box['file']='ajax/itemsCategories.php';
			$smarty->assign("pageSettings",$p->getSettings($module,basename($box['file']),array('debug'=>0)));
	       echo $smarty->fetch('modules/categories/edit_add_form.tpl');
 	  } 
} //end action
 	
if ($_GET['action']=='edit_category_item'){
 		$sql->db_Select($module.'_categories',"categoryid_path,order_by","categoryid='".$_POST['data']['old_id']."'");
		$result=execute_single($sql);
 		$data = $_POST['data'];
		$data['cat'] = $_POST['data']['old_id'];
		$data['description']=$_POST['data']['desc'];
		$data['orderby']=$result['order_by'];
		
		$ClsCat = new CategoriesActions(array('module'=>$loaded_modules[$module],'debug'=>0));
		$ClsCat->ModifyTreeCategory($module.'_categories',$data,array('debug'=>0));
 		
		$has_parent = ($_POST['data']['parent_catid']!=0) ? $subs='1' : $subs='0';
 		$updated_node=array('id'=>$_POST['data']['old_id'],'text'=>$_POST['data']['title'],'has_parent'=>$subs,'parent'=>$_POST['data']['parent_catid'],'path'=>$result['categoryid_path']);
 	
 		echo json_encode($updated_node);
		exit;
}//end action
if ($_GET['action']=='delete_category'){
 	
  if (isset ($_POST['node_id']) && $_POST['node_id']!="") {

		$id=explode("delete",$_POST['node_id']);
		$cat=$id[1];
	
 	
	      $sql->db_Select($module."_categories","categoryid","categoryid_path LIKE '%$cat/%'");
	
	   if ($sql->db_Rows() > 0) {
	    	$tmp = execute_multi($sql,0);
	 	
	    	for ($i=0;count($tmp) > $i;$i++)
	 	    {
	    	$sql->db_Delete($module."_categories","categoryid=".$tmp[$i]['categoryid']);
	 	    }
	   }
	    $sql->db_Delete($module."_categories","categoryid= $cat");
	  	 $sql->db_Delete("item_featured","categoryid= $cat");
	    
	$main_cateogory[]=$cat;
   if (!empty($tmp)){
	 foreach ($tmp as $key=>$value) {
	 	foreach ($value as $k=>$v) {
	 		$more_cat[]=$v;
	 }
	 	 }
	 $categories=array_merge($main_cateogory,$more_cat);
	
	foreach($categories as $key=>$value) {
	 	$cats[]=array("id"=>$value);
	 }
  } else {
		
		$cats=array("id"=>$main_cateogory[0]);
	}
	 
	
		 echo json_encode($cats);
		 exit;
 }
} //end action

 	
if ($_GET['action']=='change_order'){

 if (isset($_POST['source']) && $_POST['source']!="" && isset($_POST['destination']) && $_POST['destination']!="") {
 			
 		$dest=explode("parent_list",$_POST['destination']);
 		$sor=explode("parent_list",$_POST['source']);
 		$destination=$dest[1];
 		$source=$sor[1];

 		$sql->db_Select($module."_categories","categoryid,categoryid_path,parentid,order_by","categoryid='$destination'");
 		$parent_path=execute_single($sql);
 		$sql->db_Select($module."_categories","categoryid_path,parentid","categoryid='".$parent_path['categoryid']."'");
 		$pa_paths=execute_single($sql);
 		$sql->db_Select($module."_categories","categoryid_path","categoryid='".$pa_paths['parentid']."'");
 		$par_paths=execute_single($sql);
 	    $sql->db_Select($module."_categories","categoryid,categoryid_path,parentid","categoryid_path LIKE '%$source%'");
 	    $children_paths=execute_multi($sql);
 	   
 	   /// start of mergin and unmergin nodes /////
 	   
 	foreach ($children_paths as $key=>$value) {	
 	     $arr[$value['categoryid']]=array($value['categoryid_path'],$value['parentid']);   	
 	  }
 	   
 	   foreach ($children_paths as $key=>$value) {
 	    	 if (empty($arr[$value['parentid']])) {
 	   		       $p=$value['parentid'];
 	    	 }
 	   }
 
 	 foreach ($children_paths as $key=>$value) {
 	    	$pnew=explode($p."/",$value['categoryid_path']);
 	    	$pnew=end($pnew);

 	  if (empty($arr[$value['parentid']])){
 		if (!empty($_GET['over'])){
 			  $sql->q("UPDATE ".$module."_categories SET categoryid_path='".$parent_path['categoryid_path']."/".$pnew."' ,parentid=".$destination." WHERE categoryid=".$source); 
 		 } else {	
 		 	$new_path=($parent_path['parentid']!=0)? $par_paths['categoryid_path']."/".$source : $source;	
 			$sql->q("UPDATE ".$module."_categories SET categoryid_path='".$new_path."' ,parentid='".$parent_path['parentid']."' WHERE categoryid=".$source);
 		 }
 	  } else {
 	  		if (!empty($_GET['over'])){
 			   $sql->q("UPDATE ".$module."_categories SET categoryid_path='".$parent_path['categoryid_path']."/".$pnew."' WHERE categoryid=".$value['categoryid']); 	
 			} else {
 			     $new_path=($parent_path['parentid']!=0) ? $par_paths['categoryid_path']."/".$pnew : $pnew;
 			    $sql->q("UPDATE ".$module."_categories SET categoryid_path='".$new_path."' WHERE categoryid=".$value['categoryid']);	
 			}
 	  	}
 	 }	/// end of mergin and unmergin nodes /////	
 	  			
 	   
 					/// rearrange nodes on before after/////	
if( $_POST['drop_position']!='over') {
  
 	 $sql->db_Select($module."_categories","categoryid,order_by","parentid='".$parent_path['parentid']."'and order_by >='".$parent_path['order_by']."'");  	 
 	 $paths=execute_multi($sql);
 		
		foreach ($paths as $key=>$value){
			$paths_cat[$value['categoryid']]=$value['order_by'];					
		 }
 		
		unset($paths_cat[$source]);
		
	 if($_POST['drop_position']=='before' ){	
			$sql->q("UPDATE ".$module."_categories SET order_by=".$parent_path['order_by']." WHERE categoryid=".$source);
	 } else if ($_POST['drop_position']=='after' ) {
		  unset($paths_cat[$parent_path['categoryid']]);
		  $new_order=$parent_path['order_by']+1;
		  $sql->q("UPDATE ".$module."_categories SET order_by=".$new_order." WHERE categoryid=".$source);
	   }
		 
		 $value="";
	  foreach ($paths_cat as $key=>$value){
	  		  $value=$value+1;
		 $sql->q("UPDATE ".$module."_categories SET order_by=".$value." WHERE categoryid=".$key);
		 $value="";
	  }
 }
 
  if( $_POST['drop_position']=='over') {
  	$sql->q("SELECT MAX(order_by) as id FROM ".$module."_categories where parentid=".$parent_path['categoryid']);
  	$max_order=execute_single($sql);
  	$new_order=$max_order['id']+1; 
  	$sql->q("UPDATE ".$module."_categories SET order_by=".$new_order." WHERE categoryid=".$source);
  }
 
 }// end if	
} //end action change_order
   /// end rearrange nodes on before after/////

if ($_GET['action'] == "allCatMod") {
	echo json_encode($module_cat->AllItemsTreeCategories());
	exit();
}

if ($_GET['action'] == "getFeauturedCategories") {
  if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id']!=""){	
	$cat=$_GET['id'];
	$f_categories=$module_cat->FeaturedContent($cat,array('num_items'=>1,'num_subs'=>1,'debug'=>0));
		
	if (!empty($f_categories)){
		foreach($f_categories as $key=>$value){
			 if ($value['type']=='itm'){
			$f_id_itm[]=$value['itemid'];
	     	 } else {
	     	 	$f_id_cat[]=$value['itemid'];
	     	 }
		}	
			if (!empty($f_id_itm)) {
 			    $sql->q("SELECT id,title FROM ".$module." WHERE id IN (".implode(',',$f_id_itm).")");
 			    $articles=execute_multi($sql);
 			    $sql->q("SELECT itemid,orderby FROM item_featured WHERE categoryid =".$f_categories[0]['categoryid']." and itemid IN (".implode(',',$f_id_itm).")");
 			    $articles_order=execute_multi($sql);
			}
			
 		   if (!empty($f_id_cat)) {	
 			  $sql->q("SELECT categoryid as id,category as title FROM ".$module."_categories WHERE categoryid IN (".implode(',',$f_id_cat).")");
 			  $categories=execute_multi($sql);
 			  $sql->q("SELECT itemid,orderby FROM item_featured WHERE categoryid =".$f_categories[0]['categoryid']." and itemid IN (".implode(',',$f_id_cat).")");
 			    $categories_order=execute_multi($sql);
 			}
 	}
	
 	
	 if (!empty($articles)){
		 foreach ($articles as $key=>$value) {
		   	foreach ($articles_order as $k=>$v) {
		  		if ($value['id']==$v['itemid']) {
		  			 $articles_f[$value['id']]=array('id'=>$value['id'],'text'=>$value['title']."(".$lang['page'].")",'type'=>'itm','orderby'=>$v['orderby']);  
	  			 }
			 }
		  }
	  }
	  
		
	if (!empty($categories)){
	   foreach ($categories as $key=>$value) {
	    	foreach ($categories_order as $k=>$v) {
		  		if ($value['id']==$v['itemid']) {  		
		     		$categories_f[$value['id']]=array('id'=>$value['id'],'text'=>$value['title']."(".$lang['category'].")",'type'=>'cat','orderby'=>$v['orderby']);
		  		}
			 }
		  }
	  }

		if(empty($articles_f)){
 				$cat_itm=$categories_f;
 			} else if (empty($categories_f)) {
 				    $cat_itm=$articles_f;
 				} else if(!empty($articles_f) && !empty($categories_f)) {
 						$cat_itm=array_merge($articles_f,$categories_f);
 					} 
 					
 		if (!empty($cat_itm)){	
   			$data = orderBy($cat_itm, 'orderby');
 		}
 		
 		
	echo json_encode(array("results" =>$data));
	exit;
  }		
} // end action


 if ($_GET['action'] == "allFeauturedCategories") {
        
        $posted_data = $_POST;
        $posted_data['page'] = ($posted_data['page']) ? $posted_data['page'] : 1;
        $posted_data['results_per_page'] = ($posted_data['pageSize']) ? $posted_data['pageSize'] : $current_module['settings']['items_per_page'];        
        
        $current_page = ($posted_data['page']) ? $posted_data['page'] : 1;
        $results_per_page = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $settings['settings']['items_per_page'];
        

        if (isset($posted_data['start'])) 
        {
                $start = $posted_data['start'];
        } else {
                $start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
                
        }
        
 	if (is_array($_POST['filter']['filters'])) {//KENDO GRID FILTERS
		foreach ($_POST['filter']['filters'] as $key => $value) {
				switch ($value['operator']) {
					case 'eq': $op = '=';
					break;
					case 'contains': $op = 'LIKE';
					break;
					case 'neq': $op = 'NE';
					break;
				}
				if ($value['field'] !='q') {//specific key
					$posted_data['searchFields'][$value['field']] = array('type'=>$op,'val'=>$value['value']);
				}
		}
 	}
   
	if ($_POST['filter']['filters'][0]['value'] != "" ) {
 		foreach ($_POST['filter']['filters'] as $key => $value) {
 		
			if ($value['operator'] == 'contains') {
				 $condition[] = $value['field'] ." LIKE '%".$value["value"]."%'";
			}
			elseif ($value['operator']  == 'eq') { 
				$condition[] = $value['field']."= '".$value["value"]."'";
			}

		}
	}

	if ($condition) {
		$search_condition = "and ".implode(" AND ", $condition);
	}
        $cat_id=$_GET['category_id'];
        $sql->db_Select($module."_categories","categoryid","categoryid!='$cat_id' $search_condition");
     #   echo $module."_categories","categoryid","categoryid!='$cat_id' $search_condition";
        $cat=execute_multi($sql);
   #$     print_ar($sql);
        $sql->q("SELECT itemid as categoryid FROM item_featured WHERE type='cat' and categoryid='$cat_id'");
        $f_cat=execute_multi($sql);
        
   if (!empty($cat)){        
         foreach($cat as $key=>$value){
                foreach($value as $k=>$v){        
                    $result_cat[]=$v;
                  }        
         }
   }
   
   
   
   if (!empty($f_cat)){        
        foreach($f_cat as $key=>$value){
                foreach($value as $k=>$v){        
                     $result_f[]=$v;
            }        
         }
   }

   $results=(empty($f_cat)) ? $result_cat : array_diff($result_cat, $result_f);
                        
         if (!empty($results)) {                
                         
                $sql->q("SELECT count(categoryid) as num FROM ".$module."_categories WHERE categoryid IN (".implode(',',$results).")");
                $total=execute_single($sql);
        
                $sql->q("SELECT categoryid,category FROM ".$module."_categories WHERE categoryid IN (".implode(',',$results).") LIMIT ".$start.",".$results_per_page);
                $cats=execute_multi($sql);
                
         } else {
                 
                 $categories=array();
        $total['num']=0;
        
         }        
         
        if (!empty($cats)){
                foreach($cats as $key=>$value){
                           $categories[]=array('id'=>$value['categoryid'],'text'=>$value['category']);
                }
        }        
                                  
                echo json_encode(array( "data" => array("total" => $total['num']), "results" => $categories));
             exit;
}//end action

if ($_GET['action'] == "change_order_featured") {
	
  if (isset($_POST['source_f']) && $_POST['source_f']!="" && isset($_POST['destination_f']) && $_POST['destination_f']!="") {
 			

 		$dest=explode("parent_item_f",$_POST['destination_f']);
 		$sor=explode("parent_item_f",$_POST['source_f']);
 		$destination=$dest[1];
 		$source=$sor[1];
 		$cat_id=$_POST['cat_id'];

 		$sql->q("SELECT orderby FROM item_featured WHERE itemid='".$destination."' and categoryid=".$cat_id);  	 
 		$dest_order=execute_single($sql); 		
 		$sql->q("SELECT orderby FROM item_featured WHERE itemid='".$source."' and categoryid=".$cat_id);  	 
 		$source_order=execute_single($sql);
 				
 	    $sql->q("SELECT itemid,orderby FROM item_featured WHERE  orderby<='".$source_order['orderby']."'and orderby >='".$dest_order['orderby']."' and categoryid=".$cat_id);  	 
 	    $orders=execute_multi($sql);
 	    
 	if (!empty($orders)) {
		foreach ($orders as $key=>$value){
			$paths_cat[$value['itemid']]=$value['orderby'];					
		 }
 	}	 

  	unset($paths_cat[$source]);

	 if($_POST['drop_position']=='before' ){
	 		
		   $sql->q("UPDATE item_featured SET orderby=".$dest_order['orderby']." WHERE itemid=".$source." and categoryid=".$cat_id);
	 
	 } else if ($_POST['drop_position']=='after' ) {
	 	
		    unset($paths_cat[$destination]);
		    $new_order=$dest_order['orderby']+1;
			$sql->q("UPDATE item_featured SET orderby=".$new_order." WHERE itemid=".$source." and categoryid=".$cat_id);
	 }
		 
		 $value="";
		 
	  foreach ($paths_cat as $key=>$value){
	  	
	  	 $value=$value+1;
		 $sql->q("UPDATE item_featured SET orderby=".$value." WHERE itemid=".$key." and categoryid=".$cat_id);
		 $value="";
		 
	  } 
		 
  }		
} //end action

?>
