<?php
if (!call_user_func($_GET['action'],$_GET,$_POST)) {
	exit();
}

function addCat($getData,$postData=0) {
	global $sql,$loaded_modules;
	$t = new Parse();
	foreach ($postData['data'] as $k=>$v){
		if (!strstr($k,'settings_')) {
			$insert[$k] = "'".$t->toDB($v['value'])."'";
			$keys[$k] = $k;
		}
		else {
			$insert['settings'][str_replace('settings_','',$k)] = $v['value'];
			$keys['settings'] = 'settings';
		}
	}
	$insert['parentid'] = (!empty($postData['data']['parent_catid']['value']) OR $postData['data']['parent_catid']['value']) ? "'".$postData['data']['parent_catid']['value']."'" : 0; $keys['parentid'] = 'parentid';
	
	$insert['categoryid_path'] = ($postData['data']['parent_catid']['categoryid_path']) ? "'".$postData['data']['parent_catid']['categoryid_path']."'" : "''"; $keys['categoryid_path'] = 'categoryid_path';
	unset($insert['parent_catid']);unset($keys['parent_catid']);
	
	$insert['settings'] = "'".json_encode($insert['settings'])."'";

	$insert['date_added'] = time(); $keys['date_added'] = 'date_added';
	$sql->db_Select($getData['module'].'_categories',"MAX(order_by)+1 as a",'parentid = '.$insert['parentid']);
	$a = execute_single($sql);
	$insert['order_by'] = ($a['a']) ? $a['a']+1 : 1; $keys['order_by'] = 'order_by';
	$sql->db_Insert($getData['module']."_categories (".implode(',',$keys).")",implode(',',$insert));
	$lastID = $sql->last_insert_id;
	if ($postData['data']['parent_catid']['value'] == 0) {
		$q = "parentid = 0,categoryid_path = $lastID";
	}
	else {
		$q = "categoryid_path = CONCAT(categoryid_path,'/$lastID')";
		$target = str_replace("'",'',$insert['parentid']);
	}
	$sql->db_Update($getData['module']."_categories","$q WHERE categoryid = $lastID");
	$c = new Items(array('module'=>$loaded_modules[$getData['module']]));
	$new = $c->ItemCategory($lastID,array('table'=>'content_categories','settings'=>1,'num_sub'=>1));
	$new['num_sub'] = 0;
	$new['settings_array'] = $new['settings'];
	if ($postData['all']) {
		$all = $c->recurseCategories(0,0,array('num_subs'=>1));
	}
	echo  json_encode(array('target'=>$target,'items'=>array($new),'all'=>$all));
	return ;
}

function updateOrder($getData,$postData=0) {
	global $sql,$loaded_modules;
	foreach ($postData['data'] as $k=>$v) {
		$sql->db_Update($getData['module']."_categories","parentid = ".$v['parentid'].", categoryid_path = '".$v['categoryid_path']."',order_by = '".$v['order_by']."' WHERE categoryid = ".$v['id']);
	}
	$c = new Items(array('module'=>$loaded_modules[$getData['module']]));
	$new = $c->ItemCategory($lastID,array('table'=>'content_categories','settings'=>1,'num_sub'=>1));
	if ($postData['all']) {
		$all = $c->recurseCategories(0,0,array('num_subs'=>1));
	}
	echo json_encode(array('success'=>true,'all'=>$all));
}


function updateCat($getData,$postData=0) {
	global $sql,$loaded_modules;
	$t = new Parse();
	if ($postData['mode'] == 'quick') {
		foreach ($postData['data'] as $k=>$v) {
			if (!strstr($v['field'],'settings_')){
				$update[] = $v['field']." = '".$t->toDB($v['value'])."'";
			}
			else {
				$settings[str_replace('settings_','',$v['field'])] = $v['value'];
			}
		}
		$update[] = "settings = '".json_encode($settings)."'";
//		echo "UPDATE ".$getData['module']."_categories SET ".implode(",",$update)." WHERE categoryid = ".$postData['categoryid'];
		$sql->db_Update($getData['module']."_categories",implode(",",$update)." WHERE categoryid = ".$postData['categoryid']);
		echo  json_encode(array('success'=>true));
	}
	else {//Full update
		
	}
	
}

function getCat($getData,$postData=0) {
	global $sql,$loaded_modules;
	$c = new Items(array('module'=>$loaded_modules[$getData['module']]));
	$cat = $c->ItemCategory($postData['categoryid'],array('table'=>'content_categories','settings'=>1,'num_sub'=>1));
	$Languages = new Languages();
	$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
	for ($i=0;count($availableLanguages) > $i;$i++)
	{
		if ($availableLanguages[$i]['code'])
		{
		$availableLanguages[$i]['item'] = $Languages->itemTranslations($postData['categoryid'],$availableLanguages[$i]['code'],$getData['module'],
		array('groupby'=>'field','debug'=>0,'table'=>$getData['module']."_categories"));
		}
	}
	$obj['featured'] = $c->FeaturedContent($postData['categoryid'],array('get_results'=>1,'debug'=>0,'thumb'=>0,'orderby'=>'orderby','cat_details'=>1));
	echo json_encode(array('cat'=>$cat,'featured'=>$obj['featured'],'availableLanguages'=>$availableLanguages,'currentLanguage'=>$availableLanguages[0],'defaultLanguage'=>$Languages->getDefaultLanguage('default_lang',array('return'=>'array','languageDetails'=>1))));	
}

function addFeatured($getData,$postData=0) {
	global $sql,$loaded_modules;
	$i=0;
	foreach ($postData['data'] as $v) {
		$insert[$i]['itemid'] = $v['id'];
		$insert[$i]['categoryid'] = $postData['cat']['categoryid'];
		$insert[$i]['module'] = "'".$getData['module']."'";
		$insert[$i]['orderby'] = $postData['cat']['lastOrderby']+1;
		$insert[$i]['type'] = "'".$v['type']."'";
		$i++;
	}
	foreach ($insert[0] as $k=>$v) {
		$keys[] = $k;
	}
	$ke = implode(',',$keys);
	foreach ($insert as $k=>$v) {
//		echo "INSERT INTO item_featured (".$ke.") VALUES (".implode(',',$insert[$k]).")\n\n";
		$sql->db_Insert("item_featured (".$ke.")",implode(',',$insert[$k]));
	}
	echo json_encode(array('success'=>true));
}

function deleteFeatured($getData,$postData=0) {
	global $sql,$loaded_modules;
	$sql->db_Delete('item_featured',"itemid = ".$postData['itemid']." AND module = '".$getData['module']."' AND categoryid = ".$postData['categoryid']." AND type = '".$postData['type']."'");
//	echo "itemid = ".$postData['itemid']." AND module = '".$getData['module']."' AND categoryid = ".$postData['categoryid']." AND type = '".$postData['type']."'";
	
}

function updateFeaturedOrder($getData,$postData=0) {
	global $sql,$loaded_modules;
	foreach ($postData['items'] as $k=>$v){
		$p = explode('-',$v);
		$sql->db_Update('item_featured',"orderby = $k WHERE itemid = ".$p[1]." AND type = '".$p[0]."' AND module = '".$getData['module']."' AND categoryid = ".$postData['categoryid']);
	}
}

function deleteCat($getData,$postData=0) {
	global $sql,$loaded_modules;
	
	$items = implode(',',$postData['items']);
	
	$sql->db_Delete($getData['module']."_page_categories","catid IN ($items)");
	$sql->db_Delete("extra_field_categories","catid IN ($items) AND module = '".$getData['module']."'");
	$sql->db_Delete("item_featured","categoryid IN ($items) AND module = '".$getData['module']."' AND type = 'cat'");
	$sql->db_Delete("item_featured","itemid IN ($items) AND module = '".$getData['module']."' AND type = 'cat'");
	$sql->db_Delete($getData['module']."_categories","categoryid IN ($items)");
	echo json_encode(array('success'=>true));
}
?>