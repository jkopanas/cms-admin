<?php
$current_module = $loaded_modules[$_GET['module']];
$c = new Items(array('module'=>$current_module));
if (!$box) {
	$sql->db_Select("modules_boxes",'id,name,template,settings,file',"file = '".$_GET['file']."'");
	$box = execute_single($sql);
	$box['settings'] = json_decode($box['settings'],true);
	$smarty->assign("quickEdit",$_GET['trigger']);
}
$item_settings = array('fields'=>'*','thumb'=>1,'CatNav'=>1,'debug'=>0,'main'=>0,'parse'=>'toForm','GetCommonCategoriesTree'=>1,'returnSimple'=>1);

$item = $c->GetItem($_GET['id'],$item_settings);
$smarty->assign("item",$item);
$allCategories = $c->AllItemsTreeCategories();
if (is_array($item['category'])) {
foreach ($item['category'] as $v)
{
	$simplified_categories[$v['catid']] = $v['main'];
}	
}
$p = new SettingsManager();
$tags = new tags(array('module'=>$current_module,'debug'=>0,'itemid'=>$_GET['id'],'orderby'=>'orderby'));
$smarty->assign("current_module",$current_module);
$smarty->assign("tags",$tags->getAllTags(array('debug'=>0)));
$smarty->assign("itemTags",json_encode($tags->GetTagItemsByItemid($_GET['id'])));
$smarty->assign("image_types",get_image_types($current_module['id']));
$smarty->assign("simple_categories",$simplified_categories);
$smarty->assign("allcategories",$allCategories);
$smarty->assign("pageSettings",$p->getSettings($current_module['name'],basename($box['file']),array('debug'=>0)));

$smarty->assign("box",$box);
$t = ($box['template']) ? $box['template'] : 'admin2/ajax/item/itemsGeneralForm.tpl';
$template = $smarty->fetch($t);
$output = array('template'=>$template,'box'=>$box,'a'=>$_GET);
if($_GET['simple']) { echo $template; exit(); }
if (!$buffer) {
	echo json_encode($output);
	exit();
}


?>