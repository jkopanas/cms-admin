<?php
$module = $current_module = $loaded_modules[$_GET['module']];
$c = new Items(array('module'=>module_is_active($_GET['module'],1,1)));

$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
for ($i=0;count($availableLanguages) > $i;$i++)
{
	$availableLanguages[$i]['item'] = $Languages->itemTranslations($_GET['id'],$availableLanguages[$i]['code'],$current_module['name'],
	array('groupby'=>'field','debug'=>0,'table'=>$current_module['name']));
}
$smarty->assign("availableLanguages",$availableLanguages);//WE WANT FRONT END ONLY
$smarty->assign("currentLanguage",$availableLanguages[0]);//WE WANT FRONT END ONLY
$smarty->assign("defaultLanguage",$Languages->getDefaultLanguage('default_lang',array('return'=>'array','languageDetails'=>1)));//WE WANT FRONT END ONLY
$smarty->assign("item",$c->GetItem($_GET['id']));
$smarty->assign("current_module",$current_module);

$template = $smarty->fetch($box['template']);
$output = array('template'=>$template,'box'=>$box,'a'=>$_GET);

if (!$buffer) {
	echo json_encode($output);
}
?>