<?php
include("../includes/config.php");
if (is_object($memcache))
{
 $url =md5($_SERVER['HTTP_HOST'].'_languages');
//    $memcache->delete($url);
	$value = $memcache->get($url);
	if ($value) {
		echo json_encode($value);
		exit();
	}
}
include("../includes/sql.php");
include("../includes/common.php");
include("../includes/functions.php");
include("../modules/Languages.php");
include("../modules/Parse.php");
$CRYPT_SALT = 85; # any number ranging 1-255
$START_CHAR_CODE = 100; # 'd' letter
$sql = new db();
$sql->db_Connect(DBHOST,DBUSER,DBPASS,DB);
mysql_query(' set character set utf8 ');
mysql_query("SET NAMES 'utf8'") or die('Query failed: ' . mysql_error());

$Languages = new Languages();
if ($_GET['action'] == 'LoadLanguage') {
	$translations = $Languages->LoadLanguage($_POST['code']);
	if (is_numeric(USE_CACHE)) {
		$memcache->set($url,$translations,0,TTL);
	}
	echo json_encode($translations);
}//END ACTION
?>