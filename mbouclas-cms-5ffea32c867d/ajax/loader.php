<?php
include('init.php');
if ($_GET['file'])
{
	include_once($_GET['file']);
	exit();
}//END HOOKS
elseif ($_REQUEST['boxID'])
{
	$s = new siteModules();
	
	// If REQVIEW isset then the tpl should be displayed
	if($_REQUEST['view']) 
	{
		$boxView = $s->getBoxView($_REQUEST['view']);
		
		$smarty->assign("name",$boxView['name']);
		if(isset($boxView['settings']['jsIncludes']))
		{
			$smarty->assign("jsIncludes",$boxView['settings']['jsIncludes']);
		}
		if(isset($boxView['settings']['boxViewTemplate']))
		{
			$smarty->assign("boxViewTemplate",$boxView['settings']['boxViewTemplate']);
		}
		$smarty->display($boxView['file']);
		exit();
	}
	else
	{
		// otherwise, the box's content should be fetched
		HookParent::getInstance()->doTriggerHookGlobally("BeforeLoadBox");
		if (is_array($_GET['boxID'])) {
			$boxes = $s->getBox($_GET['boxID'],array('debug'=>0,'setupKeys'=>0));
			$buffer = 'buffer';
			foreach ($boxes as $k=>$v) {
				$box = $v;
				include_once($box['file']);
				$a[] = $output;
				unset($box);
			}
			echo json_encode($a);
			exit();
		}
		else {
			$box = $s->getBox($_GET['boxID'],array('debug'=>0,'setupKeys'=>0));
			include_once($box['file']);
		}
		exit();
	}
}
elseif ($_REQUEST['boxIDs']) {
	$s = new siteModules();
	$s->setupMultiBox($_REQUEST['boxIDs']);
	$s->getBoxIDs();
	exit();
	
}

/**
 * 
 * 
 * SQL Create Box_Views
 * 
 * CREATE TABLE `modules_boxes_views` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `box_id` int(8) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `file` varchar(150) NOT NULL,
  `settings` text,
  `area` varchar(10) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING HASH
)
 */

/*******************************************************************
 * O => YOU ARE HERE
 * 
 * ViewContrl -> View Controler; controler.js
 * View -> the View of the box; tpl render
 * ModContrl -> the modular_box controler; loader.php
 * Module -> the modular_box; The php file of the box
 * 
 * A View request is been answered, as the view asks for its data. 
 * Data are been processed by the modular box and returned to view
 * witch should render the fetched json.
 *
 * 		ViewContrl			View		ModContrl			Module
 *---------------------------------------------------------------------------
 *-REQVIEW->_I_				 |				 |				 |
 * 			| |	Call View	 |				 |				 |
 * 			| |---------------------------->_I_				 |
 * 							 | Smarty Displ |O|Fetch view	 |
 * 							_I_<------------|O|				 |
 * 		Ajax Display		| |				 |				 |
 * <------------------------| |				 |				 |
 * 							 |				 |				 |
 * 							_I_				 |				 |
 *  						| |Call loader	 |				 |
 *  						| |------------>_I_				 |
 *  						 |				|O|Ajax-Load	 |
 * 							 |				|O|------------>_I_
 * 							 |					Return Data	| | Fetch Data
 * 							_I_<----------------------------| |
 * 							| |Render Data
 * 			Display Data	| |
 * <------------------------| |
 *
 * A Request is been handled, as the command should be proccesed 
 * and return its state.
 *
 * -REQ-CMD---Module+Comonent+RequestCMD--->___
 * 											|O|Ajax-Load
 * 											|O|------------>___
 * 												Return Data	| | Process CMD 
 * 							_I_<----------------------------| | & Fetch Data
 * 							| |Render Data
 * 			Display Data	| |
 * <------------------------| |
 *
 *******************************************************************/

/**
 * Module_box_instance
 * 
 * CMS instance
 * 	1: DB row @ module_boxes table
 * 		as it is fetched by siteModules::getBox([box_id//name])
 * 	2: php file @ ajax/[module_name]/[area]/[box_name].php
 * 		as it presents the requested data by using the module class
 * 	3: as the same data may be represented by multiple views
 * 		a: view tpl+Kendo-template+js @ skin/modules/[module_name]/[area]/[view_name].tpl
 * 		b: DB row @ module_boxes_views table
 * 
 */

/**
 * TPL Example
 * 
{if count(jsIncludes) gt 0}
<script type="text/javascript">
	{foreach from=jsIncludes key=k item=v}
		head.js('{$v}');
	{/foreach}
</script>
{/if}

<script type="text/x-kendo-template" id="boxViewTemplate">
{if isset($boxViewTemplate)}
	{$gridColumns}
{else}
	Title #= titleVar #
{/if}
</script>
 */


?>