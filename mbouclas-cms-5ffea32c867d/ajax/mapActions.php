<?php
include("init.php");//load from manage!!!!
$posted_data = $_POST['data'];
if ($_GET['module']) {
	$loaded_modules[$_GET['module']] = module_is_active($_GET['module'],0);
}

if ($_GET['action'] == 'placeItemOnMap') { //WILL SAVE AN ITEMS MARKER TO DB
$fields['id'] = "'".$posted_data['id']."'";
$fields['itemid'] = "'".$posted_data['itemid']."'";
$fields['module'] = "'".$posted_data['module']."'";
$toUpdate['title'] = $fields['title'] = "'".$posted_data['title']."'";
$toUpdate['content'] = $fields['content'] = "'".$posted_data['content']."'";
$toUpdate['geocoderAddress'] = $fields['geocoderAddress'] = "'".$posted_data['geocoderAddress']."'";
$toUpdate['lat'] = $fields['lat'] = "'".$posted_data['lat']."'";
$toUpdate['lng'] = $fields['lng'] = "'".$posted_data['lng']."'";
$toUpdate['location'] = $fields['location'] = "GeomFromText(CONCAT('POINT(',".$posted_data['lat'].",' ',".$posted_data['lng'].",')'))";
$toUpdate['zoomLevel'] = $fields['zoomLevel'] = "'".$posted_data['zoomLevel']."'";
$toUpdate['MapTypeId'] = $fields['MapTypeId'] = "'".$posted_data['MapTypeId']."'";
$toUpdate['settings'] = $fields['settings'] = "'".$posted_data['settings']."'";
$fields['date_added'] = "'".time()."'";
$posted_data['itemid'] = ($posted_data['itemid']) ? $posted_data['itemid'] : 0;

	$sql->db_Select("maps_items","id","itemid = ".$posted_data['itemid']." AND module = '".$posted_data['module']."'");
	if ($sql->db_Rows()) { //UPDATE
		$r = execute_single($sql);	
		foreach ($toUpdate as $k=>$v)
		{
			$q[] = "$k = $v";
		}
		$sql->db_Update("maps_items",implode(" , ",$q)." WHERE id = ".$r['id']);
	} //END UPDATE
	else {//NEW - INSERT
		unset($fields['id']);
		foreach ($fields as $k=>$v){
			$keys[] = $k;
		}
		$fields = implode(",",$fields);
		$keys = implode(',',$keys);
		$sql->db_Insert("maps_items ($keys)",$fields);
//		echo "INSERT INTO maps_items VALUES ($fields)";
	}//END NEW
	
	exit();
} //END SAVE AN ITEMS MARKER TO DB

if ($_GET['action'] == 'getItemMarkers') {//GET ITEMS MARKERS
	$module = module_is_active($_POST['module'],1,1);
	$settings = array('fields'=>'maps_items.id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid','debug'=>0,'active'=>1);
	$map = new maps(array('module'=>$module));
	if ($_POST['lat'] AND $_POST['lng']) {
		$settings['distanceFromPoint'] = 1;
		$settings['lat'] = $_POST['lat'];
		$settings['lng'] = $_POST['lng'];
	}
	$settings['limit'] = $_POST['limit'];
	$items = $map->getItems($_POST['data'],$settings);
	
	if ($items) {
		$res = $map->formatItems($items,array('module'=>$module,'fields'=>$_POST['fields']));
		echo json_encode($res);
	}
}//END GET ITEMS MARKERS
if ($_GET['action'] == 'getSingleItem') {//GET ITEMS MARKERS
	
	$map = new maps(array('module'=>$loaded_modules[$_GET['module']]));
	$items = $map->getItems(array($_GET['itemid']),array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid','debug'=>0));

		if ($items) {
		$res['items'] = $items;
		foreach ($items as $k=>$v)
		{
			$lat[] = $v['lat'];
			$lng[] = $v['lng'];
			$zoom[] = $v['zoomLevel'];
			$t = json_decode($v['settings'],true);
			$res['items'][$k]['category'] = $t['category'];
		}
		$mapDetails['lat'] = min($lat);
		$mapDetails['lng'] = max($lng);
		$mapDetails['zoomLevel'] = min($zoom);
		$res['map'] = $mapDetails;

		
		echo json_encode($res);
	}
}

if ($_GET['action'] == 'getItems') {
	$map = new maps(array('module'=>$loaded_modules[$_GET['module']]));
$ids = $_POST['ids'];
	$items = $map->getItems($ids,array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid,settings'));
	if ($items) {
		$res['items'] = $items;
		foreach ($items as $k=>$v)
		{
			$lat[] = $v['lat'];
			$lng[] = $v['lng'];
			$zoom[] = $v['zoomLevel'];
			$t = json_decode($v['settings'],true);
			$res['items'][$k]['category'] = $t['category'];
		}
		$mapDetails['lat'] = min($lat);
		$mapDetails['lng'] = max($lng);
		$mapDetails['zoomLevel'] = min($zoom);
		$res['map'] = $mapDetails;
		echo json_encode($res);
	}
}

if ($_GET['action'] == 'debug') {//GET ITEMS MARKERS
	$map = new maps(array('module'=>$loaded_modules[$_GET['module']]));
	for ($i=50;71>$i;$i++)
	{
		$ids[] = $i;
	}
	$items = $map->getItems($ids,array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid,settings'));
	if ($items) {
		$res['items'] = $items;
		foreach ($items as $k=>$v)
		{
			$lat[] = $v['lat'];
			$lng[] = $v['lng'];
			$zoom[] = $v['zoomLevel'];
			$t = json_decode($v['settings'],true);
			$res['items'][$k]['category'] = $t['category'];
		}
		$mapDetails['lat'] = min($lat);
		$mapDetails['lng'] = max($lng);
		$mapDetails['zoomLevel'] = min($zoom);
		$res['map'] = $mapDetails;
		echo json_encode($res);
	}
}//END GET ITEMS MARKERS
?>