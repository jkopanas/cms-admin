<?php

$posted_data = $_POST['data'];
if ($_GET['module'] AND $_GET['action'] == 'getCategories') {
	$a = new Items(array('module'=>$loaded_modules[$_GET['module']]));
	$smarty->assign("allcategories",$Content->AllItemsTreeCategories());
	$smarty->assign("ajax",$_GET['module']);
	$smarty->assign("menuid",$_GET['menuid']);
	$smarty->display("admin/menuEditor.tpl");
	exit();
}//END GET CATEGORIES

if ($_GET['action'] == 'modifyMenuItem') {
	$menu = new menuEditor();
	$smarty->assign("menu",$menu->getMenuItem($_GET['id']));
	$smarty->assign('action','modifyMenuItem');
	$smarty->display("admin/menusItems.tpl");
	exit();
	
}//END MODIFY MENU ITEM

if ($_GET['action'] == 'editMenuItem') {
	$menu = new menuEditor();
	foreach ($_POST as $k=>$v) {
		if (!strstr($v['field'],'settings_')) {
			$tmp[$v['id']][$v['field']] = $v['field']." = '".$v['value']."'";
		}
		else {
			$tmp[$v['id']]['settings'][str_replace('settings_','',$v['field'])] =$v['value'];
		}
	}

	foreach ($tmp as $k=>$v) {
		$tmp[$k]['settings'] = "settings = '".base64_encode(json_encode($tmp[$k]['settings']))."'";
		$sql->db_Update('menus_items',implode(',',$tmp[$k])."WHERE id = $k");
		echo implode(',',$tmp[$k])."\n";
	}

	exit();
	$posted_data = $_POST['data'];
	foreach ($posted_data as $k=>$v)
	{
		if (strstr($k,'settings_')) {
			$k = str_replace('settings_','',$k);
			$settings[$k] = $v;
		}
		else {
			$update[] = "$k = '$v'";
		}
		
	}
	if ($settings) {
		$update[] = "settings = '".json_encode($settings)."'";
	}
	$sql->db_Update("menus_items",implode(",",$update)." WHERE id = ".$posted_data['id']);
	echo json_encode(array('id'=>$posted_data['id'],'title'=>$posted_data['title']));
	exit();
	
}//END MODIFY MENU ITEM

if ($_GET['action'] == 'addMenuItem') {
	$t = new Parse();
	if (count($_POST['data']) == 0) {
		exit();
	}
	$a = new Items(array('module'=>$loaded_modules[$_GET['module']]));
	$posted_data = $_POST['data'];
	$sql->db_Select("menus_items","MAX(id) as lastID, (SELECT MAX(order_by) as lastOrderBy FROM menus_items WHERE menuid = 1) as lastOrderBy");
//	echo "SELECT MAX(id) as lastID, (SELECT MAX(order_by) as lastOrderBy FROM menus_items WHERE menuid = 1) as lastOrderBy FROM menus_items<Br>";
	$tmp = execute_single($sql);
	$order = $tmp['lastOrderBy']+1;
	$lastID = (is_numeric($tmp['lastID'])) ? $tmp['lastID']+1 : 1;//FIRST ENTRY EVER

	foreach ($posted_data as $v){
	if ($v['id']) {
		$itemid = $v['id'];
		$type = $v['type'];
		$menuid = $v['menuid'];
		$parentid = ($v['parentid']) ? $v['parentid'] : 0;
		if ($v['type'] == 'cat') {
			$cat = $a->ItemCategory($v['id'],array('debug'=>0));
			$link = $_GET['module']."/".$cat['alias']."/index";
			$permalink = $cat['alias'];
			$title = $cat['category'];
			$active = 1;
			
		}//END CATEGORIES
		else {
			$item = $a->GetItem($v['id']);
			switch ($_GET['module'])
			{
				case 'content': $prefix = 'pages';
				break;
				case 'products': $prefix = 'product';
				break;
				case 'listings': $prefix = 'listing';
				break;
				case 'news': $prefix = 'news';
				break;
			}
			$link = $prefix."/".$v['id']."/".$item['permalink'];
			$permalink = $item['permalink'];
			$title = $item['title'];
			$active = $item['active'];
		}//END ITEMS
	}//END INSERT FROM DB
	else {
	$title = $t->toDB($v['title']);
	$permalink = $t->toDB($v['permalink']);
	$link = $t->toDB($v['link']);
	$menuid  = $v['menuid'];
	$active = $v['active'];
	$type = 'custom';
	}

	if (empty($settings)) {
		$settings = json_encode(array('syncWithOriginal'=>1));
	}

	$menuid_path = ($v['menuid_path']) ? $v['menuid_path'] : $lastID;
	$module = $_GET['module'];
	$sql->db_Insert("menus_items","'$lastID','$menuid','$parentid','$menuid_path','$itemid','$module','$type','$title','$permalink','$link','$order','".time()."','".base64_encode($settings)."','$active'");
	$ret[] = array('title'=>$title,'itemid'=>$itemid,'id'=>$sql->last_insert_id,'settings'=>$settings,'type'=>$type,'link'=>$link,'permalink'=>$permalink);
	$tree[$v['level']][] = $ret;
	$added[$itemid] = $sql->last_insert_id;
	$order++;
	$lastID++;
	}//END LOOP
	foreach ($posted_data as $v) {
		if ($v['menuid_path']) {
		$o = explode('/',$v['menuid_path']);
		if ($o) {
			$path = array();
			foreach ($o as $h) {
				$path[] = $added[$h];
			}
			$menuid_path = implode('/',$path);
		}
		else {
			$menuid_path = $v['id'];
		}
		$sql->db_Update('menus_items',"menuid_path = '$menuid_path', parentid = '".$added[$v['parentid']]."' WHERE id = ".$added[$v['id']]);
	}
	}
	
//	echo "'$lastID','".$posted_data['menuid']."','0','$lastID','$itemid','$module','$title','$permalink','$link','$order','".time()."','$settings','".$posted_data['active']."'";
	echo json_encode($ret);
	exit();
}//END ADD MENU ITEM

if ($_GET['action'] == 'saveState') {
	
$serial = explode("&",$_POST['serial']);

$i=1;
foreach ($serial as $v) {
	if (preg_match("/([0-9]+)/", $v, $matches)) {
		$order[$matches[0]] = $i;
		$i++;
	}
}
//print_r($serial);
//print_r($order);
$menu = new menuEditor();
$menu->recurseArray($posted_data,$order);

exit();
}//END SAVE STATE

if ($_GET['action'] == 'deleteMenuItem') {
$menu = new menuEditor();
$item = $menu->getMenuItem($_GET['id'],array('fields'=>'id,menuid'));
$ar = $menu->recurseMenuItemsSimple($item['menuid'],0,$item['id'],array('debug'=>0,'num_subs'=>1,'return'=>'simple','searchField'=>'id'));
$ids[] = $_GET['id'];
if ($ar) {//HAS CHILDREN
	
	foreach ($ar as $v)
	{
		$ids[] = $v['id'];
	}
}
else {
	
}
	$sql->db_Delete('menus_items',"id IN (".implode(",",$ids).")");
	exit();
//	echo "DELETE FROM menus_items WHERE id IN (".implode(",",$ids).")";
}//END DELETEITEM



if ($_GET['action'] == 'addMenu') {
$menu = new menuEditor();
if ($_POST['id']) {
	$action = 'modify';
	$menu->modifyMenu($_POST['id'],$_POST);
}
else {
	$action = 'add';
	$id = $menu->addMenu($_POST);
}
echo json_encode(array('action'=>$action,'id'=>$id));
exit();
}


if ($_GET['action'] == 'deleteMenu') {
	$menu = new menuEditor();
	$menu->deleteMenu($_GET['id']);
}
?>