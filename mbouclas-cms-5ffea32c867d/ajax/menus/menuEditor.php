<?php
$posted_data = $_POST['data'];

if ($_GET['action'] == 'AddMenus') {
	$menu = new menuEditor();
	$res = $menu->addMenu($_POST);
	if (is_numeric($res)) {
		$field ="id,var as vars,title,active";
		echo json_encode($menu->getMenu(0,array('num_items'=>1, 'debug'=>0, 'fields' => $field )));
	}
	exit();
}


if ($_GET['action'] == 'ViewMenu') {
	$menu = new menuEditor();
	//echo $_GET['callback']."(".json_encode($menu->getMenu(0,array('num_items'=>1,'debug'=>0))).")";
		$field ="id,var as vars,title,active";
		$ar=$menu->getMenu(0,array('num_items'=>1, 'debug'=>0, 'fields' => $field));
		$ar = (!empty($ar)) ? $ar : array();
		echo json_encode($ar);
	exit();
}

if ($_GET['action'] == 'loadSingleTranslation') {

		$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
		for ($i=0;count($availableLanguages) > $i;$i++)
		{	
			 $ar=$Languages->itemTranslations($_POST['id'],$availableLanguages[$i]['code'],false,$_POST);
			 $results[$i]=array_merge(array('uid' => $_POST['id'], 'value' => $ar['translation'], 'field'=>$_POST['field'], 'table'=>$_POST['table'], 'code' => $availableLanguages[$i]['code']),$_POST);
		}

		$smarty->assign("LangTranslate",$results);
		echo $smarty->fetch($_POST['tpl']);
		exit();
		
}

if ($_GET['action'] == 'ShowTree') {
	$menu = new menuEditor();
	$menus = $menu->getMenu($_GET['id'],array('returnItems'=>1));

	$ars=array();
	$b= array();
	
	if (!empty($menus['items'])) {
		foreach ($menus['items'] as $key => $value ) {
		
			$ar= array();
			if ($value['num_sub'] != 0 ) {
				$value['subs']= $menu->recursebuild($value['subs']);
			} 
	
			$ars[$key]['id'] = $value['id'];
			$ars[$key]['parentid'] = $value['parentid'];
			$ars[$key]['level'] = count(explode("/",$value['menuid_path']));
		//	$ars[$key]['text'] = "<span class='items' id='list_".$value['id']."' data-level='".$ars[$key]['level']."'  data-parent='".$value['parentid']."'>".$value['title']."</span>";
			$ars[$key]['text'] = $value['title'];

			$ars[$key]['items'] = $value['subs'];	
	//		$ars[$key]['encoded'] = false;
		}
	}

	echo json_encode(array("results" => $ars));
	exit();
}


if ($_GET['action'] == 'modifyMenuItem') {
	
	$menu = new menuEditor();
	$smarty->assign("menu",$menu->getMenuItem($_GET['id']));
	$smarty->assign("action","edit");
	$smarty->assign("id",$_GET['id']);
	$smarty->assign("class",'menuItem');
	
	echo $smarty->fetch('admin2/menu/menuLinkForm.tpl');
	exit();
	
}//END MODIFY MENU ITEM

if ($_GET['action'] == 'updateMenu') {
	
	$posted_data = $_POST['models'];
	
	foreach ($posted_data as $k=>$v)
	{
		foreach ($v as $key => $value ) {
			if ( $key != "num_items" && $key != "settings" && $key != "id" ) {
				if ($key == "vars") { $key = 'var'; }
				$update[] = $key."='".$value."'";
			}
		}
		$sql->db_Update("menus",implode(",",$update)." WHERE id = ".$v['id']);
	}
	
}


if ($_GET['action'] == 'editMenuItem') {
	$menu = new menuEditor();
	$posted_data = $_POST['data'];
	foreach ($posted_data as $k=>$v)
	{
		if (strstr($k,'settings_')) {
			$k = str_replace('settings_','',$k);
			$settings[$k] = $v;
		}
		else {
			$update[] = "$k = '$v'";
		}		
	}
	if ($settings) {
		$update[] = "settings = '".json_encode($settings)."'";
	}
	$sql->db_Update("menus_items",implode(",",$update)." WHERE id = ".$_POST['id']);
	echo json_encode(array('id'=>$_POST['id'],'title'=>$posted_data['title']));
	exit();
	
}//END MODIFY MENU ITEM

if ($_GET['action'] == 'addMenuItem') {
	$t = new Parse();
	$posted_data = $_POST['data'];
	$sql->db_Select("menus_items","MAX(id) as lastID, (SELECT MAX(order_by) as lastOrderBy FROM menus_items WHERE menuid = 1) as lastOrderBy");
//	echo "SELECT MAX(id) as lastID, (SELECT MAX(order_by) as lastOrderBy FROM menus_items WHERE menuid = 1) as lastOrderBy FROM menus_items<Br>";
	$tmp = execute_single($sql);
	if ($_GET['itemid']) {
		$itemid = $_GET['itemid'];
		$menuid = $_GET['menuid'];
		$type = $_GET['type'];
		$a = new Items(array('module'=>$loaded_modules[$_GET['module']]));
		if ($_GET['type'] == 'cat') {
			$cat = $a->ItemCategory($_GET['itemid']);
			$link = $_GET['module']."/".$cat['alias']."/index";
			$permalink = $cat['alias'];
			$title = $cat['category'];
			$active = 1;
		}//END CATEGORIES
		elseif ($_GET['type'] == 'BatchCat') {
			$itemid= array();
			$type="cat";
			foreach($_POST['checkboxes'] as $key => $value ) {
				$cat = $a->ItemCategory($value);	
				$catid[]=$cat['categoryid'];
				$itemid[]=$value;
				$link[] = $_GET['module']."/".$cat['alias']."/index";
				$permalink[] = $cat['alias'];
				$title[] = $cat['category'];
				$active = 1;
			}
		} else {
			
			$item = $a->GetItem($_GET['itemid']);
			switch ($_GET['module'])
			{
				case 'content': $prefix = 'pages';
				break;
				case 'products': $prefix = 'product';
				break;
				case 'listings': $prefix = 'listing';
				break;
				case 'news': $prefix = 'news';
				break;
			}
			$link = $prefix."/".$_GET['itemid']."/".$item['permalink'];
			$permalink = $item['permalink'];
			$title = $item['title'];
			$active = $item['active'];
		}//END ITEMS
	}//END INSERT FROM DB
	else {
	$title = $t->toDB($posted_data['title']);
	$permalink = $t->toDB($posted_data['permalink']);
	$link = $t->toDB($posted_data['link']);
	$menuid  = $_POST['menuid'];
	$active = $posted_data['active'];
	$type = 'custom';
		foreach ($posted_data as $k => $v) {
			if (strstr($k,'settings_')) {
				$k = str_replace('settings_','',$k);
				$settings[$k] = $v;
			}
		}
		if ($settings) {
			$settings = json_encode($settings);
		}
	}

	$order = $tmp['lastOrderBy']+1;
	$lastID = (is_numeric($tmp['lastID'])) ? $tmp['lastID']+1 : 1;//FIRST ENTRY EVER
	$module = $_GET['module'];
	if(is_array($title)) {
		foreach($title as $key => $value ) {
			$sql->db_Insert("menus_items","'$lastID','$menuid','0','$lastID','$itemid[$key]','$module','$type','$title[$key]','$permalink[$key]','$link[$key]','$order','".time()."','$settings','$active'");
			$last_id[]=$sql->last_insert_id;
			$tmpLastid=$lastID;
			$lastID++;
			if ($_GET['subs']) {		
					
					$sub=$a->ItemTreeCategories($catid[$key]);
					foreach($sub as $k => $v ) {				
						$sql->db_Insert("menus_items","'".$lastID."','".$menuid."','".$tmpLastid."','".$tmpLastid."/".$lastID."','".$v['categoryid']."','".$module."','".$type."','".$v['category']."','".$v['alias']."','".$_GET['module']."/".$v['alias']."/index','".$order."','".time()."','".$settings."','".$active."'");
						$Sublast_id[]=$sql->last_insert_id;
						$Subtitle[]=$v['category'];
						$parentid[]= $tmpLastid;
						$Subitemid[]=$v['categoryid'];
						$order = $tmp['lastOrderBy']+1;
						$lastID++;
					}
					
				}
			
		}
		echo json_encode(array('title'=>$title,'itemid'=>$itemid,'id'=>$last_id,'subs' => array('parentid'=> $parentid,'title'=>$Subtitle,'itemid'=>$Subitemid,'id'=>$Sublast_id)));
	} else {
		$sql->db_Insert("menus_items","'$lastID','$menuid','0','$lastID','$itemid','$module','$type','$title','$permalink','$link','$order','".time()."','$settings','$active'");
		echo json_encode(array('title'=>$title,'itemid'=>$itemid,'id'=>$sql->last_insert_id));
	}
	exit();
//echo "'$lastID','".$posted_data['menuid']."','0','$lastID','$itemid','$module','$title','$permalink','$link','$order','".time()."','$settings','".$posted_data['active']."'";

}//END ADD MENU ITEM

if ($_GET['action'] == 'saveState') {
	
$serial = explode("&",$_POST['serial']);

$i=1;
foreach ($serial as $v) {
	if (preg_match("/([0-9]+)/", $v, $matches)) {
		$order[$matches[0]] = $i;
		$i++;
	}
}

$menu = new menuEditor();
$menu->recurseArray($posted_data,$order);

exit();
}//END SAVE STATE

if ($_GET['action'] == 'deleteMenu') {
	
	
	$sql->db_Delete('menus',"id=".$_GET['id']);
	
}


if ($_GET['action'] == 'deleteMenuItem') {
	
	$menu = new menuEditor();
	$item = $menu->getMenuItem($_GET['id'],array('fields'=>'id,menuid'));
	$ar = $menu->recurseMenuItemsSimple($item['menuid'],0,$item['id'],array('debug'=>0,'num_subs'=>1,'return'=>'simple','searchField'=>'id'));
	$ids[] = $_GET['id'];
	if ($ar) {//HAS CHILDREN
	
		foreach ($ar as $v)
		{
			$ids[] = $v['id'];
		}
	}
	else {
	
	}
	$sql->db_Delete('menus_items',"id IN (".implode(",",$ids).")");
	
	echo json_encode(array('id'=>$_GET['id']));
	exit();
	
//	echo "DELETE FROM menus_items WHERE id IN (".implode(",",$ids).")";
}//END DELETEITEM







?>