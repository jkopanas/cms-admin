<?php
//99620247 
include("../manage/init.php");

if ($_GET['mode'] == 'ArchiveChecked') 
{
	global $sql;
	$posted_data = $_POST['data'];
	$secondaryData = $_POST['secondaryData'];
    
	$mess = new messages();
    $data = $_POST;
	
    if($data['actions']['allItemsSelected'])
    {
        if(!$data['data']['archived'])
    	{
    		$data['data']['archived']=0;
    	}
        
        $data['secondaryData']['fields']="id";
    	$data['secondaryData']['debug'] = 0;
        unset($data['secondaryData']['results_per_page']);
    	$data['secondaryData']['return'] = 'multi';
    
    	$sms = $mess->searchMessages($mess->setupSearchData($data));
    	$i=0;

        foreach($sms as $item)
        {
            $itemsids[$i++]=$item['id'];
        }   
    
        $settings = "archived=1 WHERE id IN (".implode(",",$itemsids).")";
	}
    else
	{
	   $settings = "archived=1 WHERE id IN (".implode(",",$_POST['ids']).")";
    }
	$sql->db_Update("smsDLR",$settings);
}
elseif ($_GET['mode'] == 'ExportChecked') 
{
	global $sql;
    $mess = new messages();
   	$data = $_POST;
    
    if($data['actions']['allItemsSelected'])
    {
    	if(!$data['data']['archived'])
    	{
    		$data['data']['archived']=0;
    	}
    	$data['secondaryData']['debug'] = 0;
    	$data['secondaryData']['return'] = 'multi';
        unset($data['secondaryData']['results_per_page']);
    	$sms = $mess->searchMessages($mess->setupSearchData($data));
    }
    else
    {
        $sql->db_Select("smsDLR","*"," id IN (".implode(",",$_POST['ids']).")");     
        $sms= execute_multi($sql);
    }
    
    $filename = 'export_'. time().'.csv';
    
    $fp = fopen('../downloads/'.$filename, 'w');
    
    foreach ($sms as $fields) 
    {
        fputcsv($fp, $fields);
    }
    fclose($fp);
    echo json_encode(array("href"=>'/downloads/'.$filename));
}

if (($_GET['action'] == 'search') || ($_GET['mode'] == 'ArchiveChecked')) {
//if ($_GET['action'] == 'search') {
	$mes = new messages();
	$posted_data = $_POST['data'];
	$secondaryData = $_POST['secondaryData'];
	$secondaryData['searchFieldsLike'] = $_POST['likeData'];
	
	if ($secondaryData['custom_field'] AND $secondaryData['custom_value']) {
		$searchFilters['custom_field'] = $secondaryData['custom_field'];
		$searchFilters['custom_value'] = $secondaryData['custom_value'];
	}
	if ($secondaryData['missing']) {
		$searchFilters['missing'] = $secondaryData['missing'];
	}
    
	$secondaryData['searchFields'] = $posted_data;
	
	if(!$secondaryData['searchFields']['archived'])
	{
		$secondaryData['searchFields']['archived'] = "0";
	}
    
	if($secondaryData['dateFrom'])
	{
		$date = explode("/",$secondaryData['dateFrom']);
		$date = mktime(0,0,0,$date[1],$date[0],$date[2]);
		$secondaryData['greaterThan']=array('date_updated'=>$date);	
		unset($secondaryData['dateFrom']);	
	}

    
	if($secondaryData['dateTo'])
	{
		
		$date = explode("/",$secondaryData['dateTo']);
		
		$date = mktime(0,0,0,$date[1],$date[0],$date[2]);
		
		$secondaryData['lessThan']=array('date_updated'=>$date);
		unset($secondaryData['dateTo']);	
	}
	
	$secondaryData['filters'] = $searchFilters;
	$secondaryData['debug'] = 0;
	$secondaryData['getUserDetails']=1;
	$secondaryData['return'] = 'paginated';
	$secondaryData['page'] = ($_GET['page']) ? $_GET['page'] : 1;
	
	$smarty->assign("items",$mes->searchMessages($secondaryData));
	
	$smarty->display("modules/sms/admin/resultsTable.tpl");
	
	exit();
}//END SEARCH

if($_GET['action']=="getsingle")
{
	$mess = new messages();
	echo json_encode($mess->getSingle($_GET['id']));
}



if ($_GET['action'] == 'selectAllMessages') {
	
	$mess = new messages();
	$data = $_POST;
	if(!$data['data']['archived'])
	{
		$data['data']['archived']=0;
	}
	$data['secondaryData']['debug'] = 0;
	$data['secondaryData']['return'] = 'count';

	$sms = $mess->searchMessages($mess->setupSearchData($data));
	
	echo $sms;
	exit();
}//END SELECT ALL USERS
?>