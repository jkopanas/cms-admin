<?php
include($_SERVER['DOCUMENT_ROOT']."/init.php");//load from manage!!!!
if ($_POST['var']) { $posted_data = $_POST['var'];}

if($_GET['action']=="open")
{
    $albumid=$_GET['id'];
    $images=new userImages();
    $settings = array('id'=>$albumid,'fullImages'=>1,'debug'=>0,"single"=>1);
    $album = $images->searchAlbums($settings);
    
    $smarty->assign('album',$album);
    $smarty->display("modules/users/cpanel/albumView.tpl");
}


if($_GET['action']=='uploadPhoto')
{
    $settings = form_settings_array($_POST['settings']);
    
   	$Path = $_SERVER['DOCUMENT_ROOT'] . $_POST['folder']."/".$settings['userid']."/images/";
    $targetPath = $Path."original/";
    
    if (!is_dir($targetPath)) {
        mkdir($targetPath,0755,true);
    }//END IF
    
    $tempFile = $_FILES['Filedata']['tmp_name'];
    $fileName =  $_FILES['Filedata']['name'];
    
    $file = pathinfo($fileName);
    
    $targetFile =  str_replace('//','/',$targetPath) . $fileName;
    $i=1;
    while(is_file($targetFile)){
        $fileName=$file['filename']."(".$i++.").".$file['extension'];
        $targetFile =  str_replace('//','/',$targetPath) . $fileName;
    }
    
    
    $albumid = $settings['albumid'];
    $uid = $settings['userid'];
    
    
    
    if (move_uploaded_file($tempFile,$targetFile)) {
        $userImage = new userImages();
        $data = array('original_file'=>$fileName,'albumid'=>$albumid,'uid'=>$uid,'title'=>$fileName);
        $userImage->newImage($data);
        echo "true";
    }
    /*
    
    $myFile = $_SERVER['DOCUMENT_ROOT']."/material/log.txt";
    $fh = fopen($myFile, 'w') or die("can't open file");
    
    fwrite($fh,$targetFile);
    $stringData = print_r($data);
    fwrite($fh, $stringData);
    $stringData = print_r($posted_data,true);
    fwrite($fh, $stringData);
    $stringData = print_r($_POST,true);
    fwrite($fh, $stringData);
    $stringData = print_r($_FILES,true);
    fwrite($fh, $stringData);
    fclose($fh);
    */
    
}

if($_GET['action']=="editImage")
{
    $images = new userImages();
    
    $data = $images->searchImages(array('id'=>$_GET['id'],'single'=>1));
    
    $smarty->assign('image',$data);
    $smarty->display("modules/users/cpanel/imageForm.tpl");
}

if($_GET['action']=="updateImage")
{
    $images = new userImages();
    
    $images->updateImage($posted_data);
    
    //reload album
    $albumid=$posted_data['albumid'];
    
    $settings = array('id'=>$albumid,'fullImages'=>1,'debug'=>0,"single"=>1);
    $album = $images->searchAlbums($settings);
    
    $smarty->assign('album',$album);
    $smarty->display("modules/users/cpanel/albumView.tpl");
}

if($_GET['action']=="deleteImage")
{
    $images = new userImages();
    $images->deleteImage(array('id'=>$_GET['id']));
}

if($_GET['action']=="editAlbum")
{
    $images = new userImages();
    $data = $images->searchAlbums(array('id'=>$_GET['id'],'single'=>1));
    
    $smarty->assign('album',$data);
    $smarty->display("modules/users/cpanel/albumForm.tpl");
}

if($_GET['action']=="updateAlbum")
{
    $images = new userImages();
    //submit changes
    $images->updateAlbum($posted_data);
    
    //reload album
    $albumid=$posted_data['id'];
    
    $settings = array('id'=>$albumid,'fullImages'=>1,'debug'=>0,"single"=>1);
    $album = $images->searchAlbums($settings);
    
    $smarty->assign('album',$album);
    $smarty->display("modules/users/cpanel/albumView.tpl");
}


if($_GET['action']=="deleteAlbum")
{
    $images = new userImages();
    $images->deleteAlbum($_GET['id']);
}

if($_GET['action']=="newAlbum")
{
    $smarty->display("modules/users/cpanel/albumForm.tpl");
}

if($_GET['action']=="insertAlbum")
{
    $images = new userImages();
        
    //submit changes
    $images->newAlbum($posted_data);
    
}

?>