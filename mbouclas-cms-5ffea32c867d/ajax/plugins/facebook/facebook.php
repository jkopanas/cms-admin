<?php
include ABSPATH.'/plugins/facebook/fb.php';

$user = new fb();

if ($_GET['action'] == "getShare" ) {
			
			$user->checkUserState();
			$points_list=$user->get_users_points_list(array('form_array' => 1));	
			$user->addPoints($points_list, array('action' => 'share', 'module' => 'content', "id" => $_POST['shareid'], 'uid' => $user->facebook_profile['id']));
			exit;
}

if ($_GET['action'] == "UploadImage" ) {
			
		if ($user->facebook_profile['profile']['settings']['category'] == $_POST['cat']) {
			$user->checkUserState();
			$points_list=$user->get_users_points_list(array('form_array' => 1));	
			$user->addPoints($points_list, array('action' => 'upload', 'module' => 'content', "id" => $_POST['id'], 'uid' => $user->facebook_profile['id']));
		}
			exit;
}

if ($_GET['action'] == "getComments" ) {



	if ($_POST['mode'] == "addPoints" ) {
		$user->checkUserState();
		
		if ($user->facebook_profile['profile']['settings']['category'] == $_GET['id']) {
			
			$points_list=$user->get_users_points_list(array('form_array' => 1));	
			$user->addPoints($points_list, array('action' => 'comment', 'module' => 'content', "id" => $_POST['id'], 'uid' => $user->facebook_profile['id']));
			
		}
	} else if($_POST['mode'] == "removePoints") {
				$user->checkUserState();
				$points_list=$user->get_users_points_list(array('form_array' => 1));
				$user->removePoints($points_list,array('action' => "comment","uid"=> $user->facebook_profile['id'], "itemid" => $_POST['id']));
	} else {
		echo $_GET['id']."test show comment";
	}
	exit;
	
}

if ($_GET['action'] == "removeApp" ) {
	
	global $sql;

  	$users = $user->userProfile(0,array("getUser" => 1,"searchFields" => array( "facebook_uid" => $user->sign_request['user_id']) ));

  	if (!empty($users)) {
		$user->bulkModifyUser( array($users['id']) ,array('action' => 'delete'));
  	}
  	
	exit;
	
}

if ($_POST['action'] == "insert")  {
	
	$data= $_POST['data'];
	
	$users = $user->userProfile(0,array("getUser" => 1,"searchFields" => array( "facebook_uid" => $data['id']) ));

	if (empty($users['id'])) {
		
		$ar = array( "user_name" =>$data['first_name'], "user_surname" => $data['last_name'], "email" => $data['email'], "uname" => $data['username'] );
		$gender = ($data['gender'] == "male") ? 1 : 0;
		list($day, $month, $year) = explode('/',$data['birthday']);
		$ar_profile = array( 
										"birthday" => mktime(0, 0, 0, $month, $day, $year),
										"facebook_uid" => $data['id'],
									    "gender" => $gender,
									    "settings" => json_encode(array("hometown" => $data['hometown'],"timezone" => $data['timezone'], "location" => $data['location'], "locale" => $data['locale'], "link" => $data['link']))
									 );
		$user->addUser($ar, array( "use_profile" => $ar_profile, "action" => "addUser", "debug" => 0 ));
		$users = $user->userProfile(0,array("getUser" => 1,"searchFields" => array( "facebook_uid" => $data['id']) ));
	}
	$user->GetRegisterUser($data['id']);
	echo json_encode($users);
	exit;
	
}

if ($_POST['action'] == "UpdateUserLike")  {

			global $sql;

			$users = $user->userProfile(0,array("getUser" => 1,"searchFields" => array("facebook_uid" => $_POST['data']['id']) ));
			$users['profile']['settings']=json_decode($users['profile']['settings'],true);

			if ($_POST['data']['category']==$users['profile']['settings']['category']) {
				 unset($users['profile']['settings']['category']);
				 $points_list=$user->get_users_points_list(array('form_array' => 1));
				 $user->removePoints($points_list,array("uid"=> $_POST['data']['uid']));
			} else {
				$users['profile']['settings']['category']=$_POST['data']['category'];
				$points_list=$user->get_users_points_list(array('form_array' => 1));
				$user->removePoints($points_list,array("uid"=> $_POST['data']['uid']));
				$user->addPoints($points_list, array('action' => 'like', 'module' => 'content', "id" => $_POST['data']['category'], 'uid' => $_POST['data']['uid']));
			}
		
			$sql->db_Update("users_profile", " settings ='".json_encode($users['profile']['settings'])."' where facebook_uid=".$_POST['data']['id']);

			$user->GetRegisterUser($_POST['data']['id']);
}

/*
if ($_GET['action'] == "getTemplate")  {
		global $smarty;
		$t=$smarty->fetch($_POST['template']);
		echo json_encode(array('t' => $t));
		exit;
}
*/

if ($_POST['action'] == "get")  {
	
	$users = $user->userProfile(0,array("getUser" => 1,"searchUser" => array("facebook_uid" => $_POST['id']) ));
	echo json_encode($users);
	exit;
	
}



?>