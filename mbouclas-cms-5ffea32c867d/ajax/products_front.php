<?php
include("../init.php");


if ($_GET["page"]!=""):	$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);


	$current_module = $loaded_modules['products'];
	$Content = new Items(array('module'=>$current_module));

	$cat= $_GET['cat'];

	$current_category = $Content->ItemCategory($cat,array('table'=>'products_categories','settings'=>1));

	$posted_data = array('categoryid'=>$cat,'availability'=>1,'thumb'=>1,'main'=>1);
	
    if ($_GET['sort']) {
		$posted_data['sort'] = $_GET['sort'][0]['field'];
		$posted_data['sort_direction'] = $_GET['sort'][0]['dir'];
		$posted_data['advanced'] = $_GET['sort'][0]['field'];
		
	}
	elseif (!$posted_data['sort'] AND !$posted_data['sort_direction'] AND !$current_category['orderby'])
	{
		$posted_data['sort'] = $current_module['settings']['default_content_sort'];
		$posted_data['sort_direction'] = $current_module['settings']['default_sort_direction'];
	}
	
	if ($_GET['take']) {
		$posted_data['results_per_page'] =$_GET['take'];
	} else if ($current_category['settings']['results_per_page'] AND !$posted_data['results_per_page']) {
		$posted_data['results_per_page'] = $current_category['settings']['results_per_page'];
	}
	else {
		$posted_data['results_per_page'] = ($settings['results_per_page']) ? $settings['results_per_page'] : $current_module['settings']['items_per_page'];	
	}
	
	$posted_data['page'] = $page;

	$items = $Content->ItemSearch($posted_data,$current_module,$page,0);
	
	echo json_encode($items,true);
	exit;
?>