<?php

if ($_GET['action'] == "getSettings") {
	
	$S = new SettingsManager();
	
	$file = (is_array($_POST['filter']['filters'])) ? $_POST['filter']['filters'][0]['value'] : 0;

	$res=$S->getSettings($_POST['module'],$file,array("orderby" => "orderby","way" => "asc"));

	echo json_encode(array("data"=> array( "total" => count($res) ),"results" => $res ));
	exit;
	
}

if ($_GET['action'] == "order") {
	global $sql;
	
	$i=1;
	foreach($_POST['id'] as $key => $value ) {
		$sql->db_Update("modules_settings_manager", "orderby=".$i." where id=".$value);
		$i++;
	}
	
}

if ($_GET['action'] == "openedit") {
	
	$S = new SettingsManager();
	
	$res=$S->getSetting($_GET['id']);
	echo json_encode($res);
	exit;
	
}


if ($_GET['action'] == "update") {
	
	$id = (is_numeric($_GET['id']))? $_GET['id'] : exit;
	$posted_data = $_POST;
	$S = new SettingsManager();
	
	echo json_encode($S->UpdateSettingManager($id,$posted_data));
	exit;
}

if ($_GET['action'] == "delete") {
	global $sql;
	$sql->db_Delete('modules_settings_manager',"id=".$_GET['id']);
	echo json_encode(array());
	exit;	
}

if ($_GET['action'] == "create") {
	global $sql;
	$t = new Parse();
	
	$posted_data = $_POST;
	foreach ($posted_data['data'] as $k=>$v) {
		if ( "select_type" != $k && "default" != $k  ) {
			$insertQ[] = "'".$t->toDB($v)."'";
			$fields[] = $k;
		}
	}
	
	if ($posted_data['settings']) {
		
		$tmp = array();
		foreach($posted_data['settings']['values'] as $key => $value ) {
			array_push($tmp, array('value' => $value, 'translation' => $posted_data['settings']['translation'][$key], 'default'=> "1"));
		}
		
		$posted_data['settings'] = json_encode(array('values' => $tmp));
		$insertQ[] ="'".$posted_data['settings']."'";
		$fields[] ="options";
		
	}
	
	$sql->db_Insert("modules_settings_manager (".implode(',',$fields).")",implode(",",$insertQ));
	$id = $sql->last_insert_id;
	$posted_data['data']['id'] = $id;
	echo json_encode($posted_data['data']);
	exit;
}

?>