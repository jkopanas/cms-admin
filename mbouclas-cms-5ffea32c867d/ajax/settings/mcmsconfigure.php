<?php
include_once('./init.php'); // AJAX init

global $loaded_modules;
$jsonOutput = array(
	"data" 			=> array(),
	"status" 		=> "UKNOWN", // as nothing will happend, Uknown state is assumed
//"debug"			=> DEBUG
);

$id	= ($_GET['id']) 	? $_GET['id'] 		: $_POST['id'];

if ($_POST['action'] == "updateorder")
{
	global $sql;
	unset($_POST['action']);
	$q						= "";
	$jsonOutput["status"] 	= "failure";
	for($i=0; $i<count($_POST['model']); $i++)
	{
		$q .= "UPDATE config SET orderby=".$_POST['model'][$i]['orderby']." WHERE name = ".$_POST['model'][$i]['name']."; \n ";
	}
	if($sql->q($q))
	{
		$jsonOutput["status"] 	= "success";
	}
}

if ($_POST['action'] == "update")
{
	global $sql;
	unset($_POST['action']);
	$sql->db_Select("config","name, value","name=".$id);
	
	if($sql->db_Rows()>0) // if Exists modify
	{
		$tmpArr						= execute_single($sql);
		$t 							= new textparse();
		$query 						= "";
		$insertToken 				= array();
		$insertToken 				= $_POST['model'];
		$insertToken['value']		= $insertToken['configvalue'];
		unset($insertToken['configvalue']);
		
		if(is_array($insertToken['value']) && !isJSON($tmpArr['value']))
		{
			// Create JSOn if previous setting was handling JSON
			isJSON($insertToken['value'],$insertToken['value'],true); 
		}
		elseif (is_array($insertToken['value'])) // backward complatible
		{
			$insertToken['value'] = form_settings_string($insertToken['value'],'###',':::');
		}
		
		foreach ($insertToken as $key => $val)
		{
			$query .= " $key = '$val'  ,";
		}
		
		$query = substr($query, 0, -1);
		$query .= " WHERE name = $id";
		$sql->db_Update("config",$query);
		
		$jsonOutput["data"] = array("id" => $id);
		$jsonOutput["status"] = "success";
	}
	else // else create it 
	{
		$t 							= new textparse();
		$insertToken 				= array();
		$insertToken 				= $_POST['model'];
		$insertToken['value'] 		= json_encode($insertToken['configvalue']);
		unset($insertToken['configvalue']);
		
		if($sql->db_Insert("config",
			"'".$insertToken['name']
			."','".$insertToken['value']
			."','".$insertToken['category']
			."','".$insertToken['orderby']
			."','".$insertToken['type']
			."','".$insertToken['defvalue']."'"))
		{
			$jsonOutput["data"] 	= array("id" => $insertToken['name']);
			$jsonOutput["status"] 	= "success";
		}
		else
		{
			$jsonOutput["data"] 	= array("id" => $insertToken['name']);
			$jsonOutput["status"] 	= "failure";
		}
	}
	
	print(@json_encode($jsonOutput));
	exit();
}

if ($_POST['action'] == "destroy")
{
	global $sql;
	unset($_POST['action']);
	$sql->db_Delete("config","name = $id");
	
	$jsonOutput["data"] = array("id" => $id);
	$jsonOutput["status"] = "success";
	print(@json_encode($jsonOutput));
	exit();
}

global $sql;
$tmpArr = $sql->db_Select("config","*");
$tmpArr = execute_multi($sql,1);

for($i=0; $i<count($tmpArr); $i++) 
{
	$tmpArr[$i]['configvalue'] = $tmpArr[$i]['value'];
	unset($tmpArr[$i]['value']);
	if(!isJSON($tmpArr[$i]['configvalue'],$tmpArr[$i]['configvalue'],true))
	{
		$tmpArr[$i]['configvalue'] = form_settings_array($tmpArr[$i]['configvalue'],'###',':::');
	}
}

$jsonOutput["data"] 	= ($tmpArr?$tmpArr:array());
$jsonOutput["status"] 	= "success";
print(json_encode($jsonOutput));
exit();

?>