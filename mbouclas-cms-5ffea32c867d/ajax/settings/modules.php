<?php
include_once('./init.php'); // AJAX init

global $loaded_modules;
$jsonOutput = array(
	"data" 			=> array(),
	"status" 		=> "UKNOWN", // as nothing will happend, Uknown state is assumed
//"debug"			=> DEBUG
);

$id				= ($_GET['id']) 	? $_GET['id'] 		: $_POST['id'];
$targetModule 	= ($_GET['module']) ? $_GET['module'] 	: $_POST['module'];

if ($_POST['action'] == "updateorder")
{
	global $sql;
	unset($_POST['action']);
	$q						= "";
	$jsonOutput["status"] 	= "failure";
	for($i=0; $i<count($_POST['model']); $i++)
	{
		$q .= "UPDATE modules_settings SET orderby=".$_POST['model'][$i]['orderby']." WHERE id = ".$_POST['model'][$i]['id']."; \n ";
	}
	if($sql->q($q))
	{
		$jsonOutput["status"] 	= "success";
	}
	$tmpModuleTarget 	= (isset($loaded_modules[$targetModule])? $loaded_modules[$targetModule]['id']: '0');
	$tmpArr 			= $sql->db_Select("modules_settings","*","module_id = ".$tmpModuleTarget." ORDER BY orderby ASC ");
	$tmpArr 			= execute_multi($sql,1);
	
	for($i=0; $i<count($tmpArr); $i++)
	{
		if(!isJSON($tmpArr[$i]['options'],$tmpArr[$i]['options'],true))
		{
			$tmpArr[$i]['options'] = form_settings_array($tmpArr[$i]['options'],'###',':::');
		}
	}
	
	$jsonOutput["data"] = ($tmpArr?$tmpArr:array());
	print(json_encode($jsonOutput));
	exit();
}

if ($_POST['action'] == "update")
{
	global $sql;
	unset($_POST['action']);
	$tmpModuleTarget = (isset($loaded_modules[$targetModule])? $loaded_modules[$targetModule]['id']: '0');
	$sql->db_Select("modules_settings","id, options","id=".$id);
	
	if($sql->db_Rows()>0) // if Exists modify
	{
		$tmpArr						= execute_single($sql);
		$t 							= new textparse();
		$query 						= "";
		$insertToken 				= array();
		$insertToken 				= $_POST['model'];
		$insertToken['module_id'] 	= $tmpModuleTarget;
		
		if(is_array($insertToken['options']) && isJSON($tmpArr['options']))
		{
			// Create JSOn if previous settings was handling JSON
			isJSON($insertToken['options'],$insertToken['options'],true); 
			
		}
		elseif (is_array($insertToken['options'])) // backward complatible
		{
			$insertToken['options'] = form_settings_string($insertToken['options'],'###',':::');
			
		}
		
		
		foreach ($insertToken as $key => $val)
		{
			$query .= " $key = '$val'  ,";
		}
		
		$query = substr($query, 0, -1);
		$query .= " WHERE id = $id";
		$sql->db_Update("modules_settings",$query);
		
		$jsonOutput["data"] = array("id" => $id);
		$jsonOutput["status"] = "success";
	}
	else // else create it 
	{
		$t 							= new textparse();
		$insertToken 				= array();
		$insertToken 				= $_POST['model'];
		$insertToken['options'] 	= json_encode($insertToken['options']);
		$insertToken['module_id'] 	= $tmpModuleTarget;
		
		if($sql->db_Insert("modules_settings",
			"'','".$insertToken['module_id']
			."','".$insertToken['name']
			."','".$insertToken['value']
			."','".$insertToken['type']
			."','".$insertToken['category']
			."','".$insertToken['orderby']
			."','".$insertToken['comments']
			."','".$insertToken['options']
			."','".$insertToken['description']."'"))
		{
			$jsonOutput["data"] 	= array("id" => $sql->last_insert_id);
			$jsonOutput["status"] 	= "success";
		}
		else
		{
			$jsonOutput["data"] 	= array("id" => -1);
			$jsonOutput["status"] 	= "failure";
		}
	}
	
	print(@json_encode($jsonOutput));
	exit();
}

if ($_POST['action'] == "destroy")
{
	global $sql;
	unset($_POST['action']);
	$sql->db_Delete("modules_settings","id = $id");
	
	$jsonOutput["data"] = array("id" => $id);
	$jsonOutput["status"] = "success";
	print(@json_encode($jsonOutput));
	exit();
}

if (is_numeric($id))
{
	global $sql;
	unset($_POST['action']);
	$tmpArr = $sql->db_Select("modules_settings","*","id = ".$id);
	$tmpArr = execute_single($sql);
	
	if(!isJSON($tmpArr['options'],$tmpArr['options'],true))
	{
		$tmpArr['options'] = form_settings_array($tmpArr['options'],'###',':::');
	}

	$jsonOutput["data"] 	= ($tmpArr?$tmpArr:array());
	$jsonOutput["status"] 	= "success";
	print(json_encode($jsonOutput));
	exit();
}

global $sql;
$tmpModuleTarget = (isset($loaded_modules[$targetModule])? $loaded_modules[$targetModule]['id']: '0');

$tmpArr = $sql->db_Select("modules_settings","*","module_id = ".$tmpModuleTarget." ORDER BY orderby ASC ");
$tmpArr = execute_multi($sql,1);

for($i=0; $i<count($tmpArr); $i++) 
{
	if(!isJSON($tmpArr[$i]['options'],$tmpArr[$i]['options'],true))
	{
		$tmpArr[$i]['options'] = form_settings_array($tmpArr[$i]['options'],'###',':::');
	}
}

$jsonOutput["data"] 	= ($tmpArr?$tmpArr:array());
$jsonOutput["status"] 	= "success";
print(json_encode($jsonOutput));
exit();

?>