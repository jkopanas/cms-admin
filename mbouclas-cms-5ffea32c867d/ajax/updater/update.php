<?php
include_once('./init.php'); // AJAX init

global $loaded_modules;
$current_module = $loaded_modules['updater'];

/**
 * Call URL
 * /ajax/loader.php?file=updater/update.php&action=versioncheck&version=1.0.5&callback=update
 * /ajax/loader.php?file=updater/update.php&action=fetcharchive&version=1.0.9&type=[0,6]
 * /ajax/loader.php?file=updater/update.php&action=fetchplugin&version=max&name=facebook&type=[2,8]
 * /ajax/loader.php?file=updater/update.php&action=fetchmcms&version=max&type=[1,7]
 * /ajax/loader.php?file=updater/update.php&action=validatecustomer&mcmsEmail=customer@domain.com&mcmsValidation=xxxxxxxxxxxx&callback=validate
 */

// Version Check Request
if(strcasecmp($_GET['action'], "versionCheck") == 0)
{
	ob_start();
	$settings=array();
	
	$settings['version'] = $_GET['version'];
	
	Updater::doVersionCheck($settings);
	/*********
	///ajax/loader.php?file=updater/update.php&action=versioncheck&version=1.0.5&callback=update
	$url = "http://mcms.net-tomorrow.com/ajax/loader.php?file=updater/update.php&action=versioncheck&version=".$_GET['version']."&callback=update";
	$remoteFP 	= fopen($url, "r");
	$bufStr = "";
	while (!feof($remoteFP))
	{
		$bufStr .= fread($remoteFP, 512);
	}
	fclose($remoteFP);
	*****/
	ob_end_clean();
	// TODO
	//echo $bufStr;
	echo $_GET['callback'] . '(' .json_encode($settings['versionresults']). ')';
	//echo json_encode($settings['versionresults']);
	exit();
}

// Serve the Update repository, if exists
if(strcasecmp($_GET['action'], "fetcharchive") == 0)
{
	global $sql;
	
	$sql->db_Select("updates_repository","version,title,filepath,encoded_filepath",
				" version = '".$_GET['version']."' AND type = ".($_GET['type']?$_GET['type']:0));
	$tmpArr = execute_single($sql);
	$filepath = ($_GET['type']>0?$tmpArr['encoded_filepath']:$tmpArr['filepath']);
	if(is_array($tmpArr) && is_file($_SERVER['DOCUMENT_ROOT'].$filepath))
	{
		header("Content-Disposition: atachment; filename=".$tmpArr['title'].".zip");
		header("Content-Type: application/octet-stream");
		header("Content-Length: ".filesize($_SERVER['DOCUMENT_ROOT'].$filepath));
		header("Pragma: no-cache");
		header("Expires: 0");
		$fp=fopen($_SERVER['DOCUMENT_ROOT'].$filepath,"r");
		for($i=0; $i<=filesize($_SERVER['DOCUMENT_ROOT'].$filepath); $i+=512)
		{
			print fread($fp,512);
		}
		fclose($fp);
		exit();
	}
}

// Serve MCMS repository, if exists
if(strcasecmp($_GET['action'], "fetchmcms") == 0)
{
	global $sql;
	
	$_GET['version'] = (strcasecmp($_GET['version'], "max") == 0? " versioncheck = (select MAX(versioncheck) FROM `updates_repository` where type = 1)" : " version = '".$_GET['version']."'" );
	
	$sql->db_Select("updates_repository","version,title,filepath,encoded_filepath",
				" ".$_GET['version']." AND type = ".($_GET['type']?$_GET['type']:1));
	$tmpArr = execute_single($sql);
	$filepath = ($_GET['type']>1?$tmpArr['encoded_filepath']:$tmpArr['filepath']);
	if(is_array($tmpArr) && is_file($_SERVER['DOCUMENT_ROOT'].$filepath))
	{
		header("Content-Disposition: atachment; filename=".$tmpArr['title'].".zip");
		header("Content-Type: application/octet-stream");
		header("Content-Length: ".filesize($_SERVER['DOCUMENT_ROOT'].$filepath));
		header("Pragma: no-cache");
		header("Expires: 0");
		$fp=fopen($_SERVER['DOCUMENT_ROOT'].$filepath,"r");
		for($i=0; $i<=filesize($_SERVER['DOCUMENT_ROOT'].$filepath); $i+=512)
		{
			print fread($fp,512);
		}
		fclose($fp);
		exit();
	}
}

// Serve a MCMS plugin repository, if exists
if(strcasecmp($_GET['action'], "fetchplugin") == 0)
{
	global $sql;
	
	$_GET['version'] = ($_GET['version'] == "max"? "(select MAX(versioncheck) FROM `updates_repository` where type = 2)" : $_GET['version'] );
	
	$sql->db_Select("updates_repository","version,title,filepath,encoded_filepath",
				"title = '".$_GET['name']."' AND version = '".$_GET['version']."' AND type = ".($_GET['type']?$_GET['type']:2));
	$tmpArr = execute_single($sql);
	$filepath = ($_GET['type']>2?$tmpArr['encoded_filepath']:$tmpArr['filepath']);
	if(is_array($tmpArr) && is_file($_SERVER['DOCUMENT_ROOT'].$filepath))
	{
		header("Content-Disposition: atachment; filename=".$tmpArr['title'].".zip");
		header("Content-Type: application/octet-stream");
		header("Content-Length: ".filesize($_SERVER['DOCUMENT_ROOT'].$filepath));
		header("Pragma: no-cache");
		header("Expires: 0");
		$fp=fopen($_SERVER['DOCUMENT_ROOT'].$filepath,"r");
		for($i=0; $i<=filesize($_SERVER['DOCUMENT_ROOT'].$filepath); $i+=512)
		{
			print fread($fp,512);
		}
		fclose($fp);
		exit();
	}
}

// Verify & validate Customer and its request
if(strcasecmp($_GET['action'], "validatecustomer") == 0)
{
	ob_start();
	$sql->db_Select("customers_repository","settings",
				"validation_token = '".$_GET['mcmsValidation']."' AND email = '".$_GET['mcmsEmail']."' ");
	$tmpArr = execute_single($sql);
	$type=0;
	if(is_array($tmpArr) && isJSON($tmpArr['settings'],$tmpArr['settings'],true))
	{
		$type = $tmpArr['settings']['encoded_type'];
	}
	ob_end_clean();
	echo ($_GET['callback']?$_GET['callback'] . '({"type":' .$type. '})':'{"type":' .$type. '}') ;
	exit();
}

header('HTTP/1.0 404 Not Found');
exit();


/**
 * 
 * 
CREATE TABLE `updates_repository` (
`id`  int(11) NOT NULL AUTO_INCREMENT,
`version`  varchar(10)  NOT NULL ,
`title`  varchar(25)  NULL DEFAULT NULL ,
`tokens`  varchar(255)  NULL DEFAULT NULL ,
`changelog`  text  NULL ,
`versioncheck`  int(11) NULL DEFAULT NULL ,
`filepath`  varchar(255) NULL DEFAULT NULL ,
`type`  int(2) NULL DEFAULT 0,
PRIMARY KEY (`id`)
)
 */

?>