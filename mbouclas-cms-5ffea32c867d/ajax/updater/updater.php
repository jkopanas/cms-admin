<?php
include_once('./init.php'); // AJAX init

global $loaded_modules;
$current_module = $loaded_modules['updater'];
$settings = array();
$jsonOutput = array(
	"mode" 		=> 0,
	"token" 	=> 0,
	"error" 	=> 0,
	"status" 	=> 0, 
	"success"	=> false,
	"get"		=> $_GET,
	"post"		=> $_POST
);

if($_GET['action']=="getversion")
{
	ob_start();
	$jsonOutput['version'] 	= Updater::getMCMSVersion();
	$jsonOutput['token'] 	= ($_GET['token']? $_GET['token'] : ( $_POST['token']? $_POST['token'] : Updater::generateTokenHashStr()));
	$jsonOutput['mode'] 	= ($_GET['mode']? $_GET['mode']:0);
	$jsonOutput['action'] 	= ($_GET['action']? $_GET['action']:0);
	$jsonOutput['success'] 	= ($jsonOutput['version']? true:false);
	$jsonOutput['status'] 	= ($jsonOutput['version']? 1:0);
	$jsonOutput['get']		= $_GET;
	$_POST['token']			= $jsonOutput['token'];
	$_POST['version']		= $jsonOutput['version'];
	$jsonOutput['post']		= $_POST;
	ob_end_clean();
	echo(json_encode($jsonOutput));
	exit();
}

if(strlen($_GET['mode'])>0)
{
	ob_start();
	
	$data = $_POST;
	unset($data['post']);
	unset($data['get']);
	unset($data['data']);
	$data = array_merge($data,($_POST['post']? $_POST['post'] : array()));
	$data = array_merge($data,($_POST['data']? $_POST['data'] : array()));
	//$data = array_merge($data,($_POST['get']? $_POST['get'] : array()));
	
	$settings['mode'] 		= $_GET['mode']; 	// Mode[update,trace] updating Mode or Trace to generate/Build an Update Can be managed by giving just the token -- js-php interchangeable
	$settings['token'] 		= ($data['token']		? $data['token']		 		: 0 ); // Action/Activity pointer -- js-php interchangeable 
	$settings['title']		= ($data['title']		? (strlen($data['title']['value'])>1 	? $data['title']['value']	: $data['title']) 	: '');
	$settings['version']	= ($data['version']		? (strlen($data['version']['value'])>1 	? $data['version']['value']	: $data['version']) : '');
	$settings['lastProgr'] 	= ($data['lastProgr'] 	? $data['lastProgr']			: 0); // Last Given Progress, as to point to the next -- php interchangeable 
	$settings['progress'] 	= ($data['progress'] 	? $data['progress'] 			: 0); // Progress of the Current State -- js-php interchangeable 
	$settings['lastAct'] 	= ($data['lastAct'] 	? $data['lastAct'] 				: 0); // Last Action Requested & executed -- php interchangeable 
	$settings['next'] 		= ($data['next'] 		? $data['next'] 				: 0); // Next Step of Process, maybe indecated by Token PTR -- js-php interchangeable 
	$settings['nextAct'] 	= ($_GET['action']	 	? $_GET['action'] 				: $data['nextAct']); // Specify next acttion -- js interchangeable 
	$settings['hook'] 		= ($_POST['hook'] 		? $_POST['hook'] 				: ''); // execute Specific hook -- js interchangeable 
	$settings['copies']		= ($_POST['copies'] 	? $_POST['copies'] 				: array()); // File tree that is about to be Ziped -- js interchangeable 
	$settings['deletions'] 	= ($_POST['deletions'] 	? $_POST['deletions']			: array()); // Deletion file tree commands -- js interchangeable 
	$settings['sql'] 		= ($data['sqltables'] 	? $data['sqltables']['value']	: ''); // SQL custom query -- js interchangeable
	$settings['sqlTables'] 	= ($data['sqltables'] 	? $data['sqltables']['tables']	: ''); // Tables that are about to be SQL dump -- js interchangeable 
	$settings['changelog'] 	= ($data['changelog'] 	? $data['changelog']['value']	: '');
	$settings['status'] 	= ($data['status'] 		? $data['status']				: 0); // Status of activity -- js-php interchangeable 
	$settings['error'] 		= ($data['error'] 		? $data['error']				: ''); // error Log -- js-php interchangeable 
	$settings['log'] 		= ($data['log'] 		? $data['log']					: array()); // Log Event & status -- js-php interchangeable 
	
	
	$settings['token']		= ($settings['token']==0? (strcasecmp($_GET['mode'], 'update')!=0?$settings['version']:$settings['token']):$settings['token']);
	
	$updater = new Updater($settings);
	$updater->doNext($settings);
	
	
	$_POST['token']			= $settings['token'];
	$_POST['version']		= $settings['version'];
	$_POST['mode']			= $settings['mode'];
	$_POST['lastAct']		= $_GET['action'];
	
	unset($_GET['action']);
	unset($_POST['action']);
	unset($_POST['post']);
	$jsonOutput['get']		= $_GET;
	$jsonOutput['post']		= $_POST;
	$jsonOutput['tokens']	= ($data['tokens']		? $data['tokens'] 				: array());
	$jsonOutput['token'] 	= ($settings['token']	? $settings['token'] 			: Updater::generateTokenHashStr());
	$jsonOutput['mode'] 	= $_REQUEST['mode'];
	$jsonOutput['error'] 	= $settings['error'];
	$jsonOutput['status'] 	= $settings['status'];
	$jsonOutput['success'] 	= ($settings['status']>0? true:false);
	$jsonOutput['token'] 	= ($_GET['token']		? $_GET['token'] 				: ( $_POST['token']? $_POST['token'] : Updater::generateTokenHashStr()));
	
	ob_end_clean();
	
}

echo(json_encode($jsonOutput));
exit(); // just in case


/**
 INSERT INTO `modules` (`name`, `folder`, `active`, `startup`, `main_file`, `menu_title_admin`, `orderby`, `is_item`, `on_menu`) VALUES ('updater', 'modules/updater', '1', '1', 'updater.php', 'Updater', '0', '0', '0')
 INSERT INTO `modules_boxes` (`name`, `title`, `required_modules`, `file`, `settings`, `area`, `active`, `fetchType`) VALUES ('updater', 'Updater', 'updater', 'updater/updater.php', '{}', 'back', '1', 'ajax')
 */

// token -> postdata



?>