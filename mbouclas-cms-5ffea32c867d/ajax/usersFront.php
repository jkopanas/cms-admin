<?php
$user = new user();
HookParent::getInstance()->doTriggerHook('global', "AfterInitUser");

if ($_GET['action'] == 'filterUsers') {

		$posted_data = $_POST;
		foreach($_POST as $k => $v ) {
			if (is_array($_POST[$k])) {
				foreach ($_POST[$k] as $key => $value) {
					$field= $k.".".$key;
					switch ($value['mode']) {
						case 'eq': 
							$posted_data['searchFields'][$field]= $value['value'];
						break;
						case 'contains': 
							$posted_data['searchFieldsLike'][$field]= $value['value'];
						break;
						case 'between': 
							$posted_data['searchFieldsBetween'][$field]= $value['value'];
						break;
						case 'neq': 
							$posted_data['searchFieldsNotLike'][$field]= $value['value'];
						break;
					}
				}
			}
		}

	//	$posted_data['debug']=1;
	
		$posted_data['page'] = ($posted_data['page']) ? $posted_data['page'] : 1;
        $posted_data['results_per_page'] = ($posted_data['pageSize']) ? $posted_data['pageSize'] : $current_module['settings']['items_per_page'];        
        
        $current_page = ($posted_data['page']) ? $posted_data['page'] : 1;
        $results_per_page = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $settings['settings']['items_per_page'];

        if (isset($posted_data['start'])) 
        {
                $start = $posted_data['start'];
        } else {
                $start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
                
        }	
		
        $posted_data['results_per_page'] = $start.",".$results_per_page;
		$posted_data['profile'] =1 ;
	    //$posted_data['debug'] = 1;
	    $posted_data['fields'] = "*,users.id as id";
		$users = $user->searchUsers($posted_data);
		$posted_data['return'] = "count";	
		$total = $user->searchUsers($posted_data);

	$settings=array("results_per_page"=>$posted_data['results_per_page'],"total"=>$total);
	echo json_encode(array('data'=>$settings,'results'=>$users));
	exit();
}

//// TODO GEORGE put his hand. May be, we should delete it ////
/*
if ($_GET['action'] == 'searchUsers') {
$posted_data = $_POST['data'];
$userDetails = $user->userDetails(ID,array('efields'=>1));
$userDetails['profile'] = $user->userProfile(ID,array('quick'=>1,'fields'=>'id,gender,countryID,cityID,regionID,zipcode'));

	foreach ($posted_data as $k=>$v) {
            if (strstr($k,'efield_')) {
            	$k = str_replace("efield_",'',$k);
            	
	            if (strstr($k,'from-') OR strstr($k,'to-')) {
	            	list($dest,$key)=split("-",$k);
	            	$varsRange[$key][$dest] = $v;
	            	$varNames[$k] = "'$key'";
	            	$range[$k] = $key;
	            }
	            else {
	            	$varNames[$k] = "'$k'";
	            }
            }//END EXTRA FIELDS
            if (strstr($k,'loc_')) {
            	$k = str_replace("efield_",'',$k);
            	
            	$varLocations[$k] = $v;
            	
            }//END LOCATION
            if (strstr($k,'profile_')) {
            	$k = str_replace("profile_",'',$k);
            	if (strstr($k,'from-') OR strstr($k,'to-')) {
	            	list($dest,$key)=split("-",$k);
	            	$varsRangeProfile[$key][$dest] = $v;
	            	$varNamesProfile[$k] = "'$key'";
	            	$rangeProfile[$k] = $key;
	            }
	            else {
	            	$varProfile[$k] = $v;
	            }
            	
            	
            }//END LOCATION
    }
    if ($varProfile) {
    	include("../hooks/users/profileSearch.php");
    }
    	include("../hooks/users/location.php");
    if ($varNames) {
    	include("../hooks/users/efields.php");
    }
//echo "<br><br><br>$searchQuery";
if ($ids) {
	$i=0;

if ($locationFound) {
	$smarty->assign("locationsFound",$locationFound);

}
if ($efieldFound) {
	$smarty->assign("efieldsFound",$efieldFound);
	foreach ($efieldFound as $k=>$v)
	{
		if (is_array($locationFound) AND array_key_exists($k,$locationFound)) {
			$filteredData[$k] = array_merge($v,$locationFound[$k]);
		}
		
	}
	if ($filteredData) {
	foreach ($filteredData as $k=>$v)
	{
		$percent[$k] = $v['percent'];
		$distance[$k] = $v['distance'];
	}
	array_multisort($percent, SORT_DESC,SORT_NUMERIC, $distance, SORT_ASC,SORT_NUMERIC, $filteredData);
	}

}
}
	
	$page = ($_POST['page']) ? $_POST['page'] : 1;

	$results_per_page = 10;
	$page_offset = ($page - 1) * $results_per_page;
	if ($filteredData) {
		$users = array_slice($filteredData,$page_offset,$results_per_page);
	}
	paginate_results($page,$results_per_page,count($filteredData));
	if ($users) {
		foreach ($users as $k=>$v)
		{
			$users[$k] = array_merge($v,$user->userDetails($v['itemid'],array('efields'=>1)));
			$users[$k]['profile'] = $user->userProfile($users[$k]['id'],array('quick'=>1,'fields'=>'id,gender,countryID,cityID,regionID,zipcode','efileds'=>1));
			$users[$k]['age'] = strftime("%Y",time()) - strftime("%Y",$users[$k]['user_birthday']);
			if ($locationFound[$users[$k]['id']]) {
				$users[$k]['location'] = $locationFound[$users[$k]['id']];
			}
			$users[$k]['comparison'] = $user->compareUsers($userDetails,$users[$k]);
		}//END LOOP
	}//END USERS
	$smarty->assign("users",$users);
	$smarty->assign("page",$secondaryData['page']);
	$smarty->display("modules/users/cpanel/usersList.tpl");
		

	exit();
}//END SEARCH
*/

if ($_GET['action'] == 'modifydUser') {

	$user = new user();
	$posted_data = $_POST['data'];
	if ($_POST['mode'] == 'save') {
		$posted_data['id'] = ID;
		$posted_data['settings'] = $_POST['settings'];
		$action = 'modifydUser';
		$user->addUser($posted_data,array('debug'=> 0,'action'=> $action));//WILL MODIFY	
	}	
	$mod_user=array('id'=>ID,'uname'=>$posted_data['uname'],'user_name'=>$posted_data['user_name'],'user_mobile' => $posted_data['user_mobile'],'address'=>$_POST['settings']['address'],'user_surname'=>$posted_data['user_surname'],'email'=>$posted_data['email']);
	echo json_encode(array($mod_user));
}


if ($_GET['action'] == 'saveUserDetails') {
	
	$user = new user();
	$posted_data = $_POST['data'];
	$action = ($posted_data['id']) ? 'modifydUser' : 'addUser';
	$user->addUser($posted_data,array('debug'=> 0,'action'=> $action));//WILL MODIFY
	$mod_user=array('id'=>$posted_data['id'],'uname'=>$posted_data['uname'],'user_name'=>$posted_data['user_name'],'user_surname'=>$posted_data['user_surname'],'email'=>$posted_data['email']);
	echo json_encode($mod_user);
}//END SAVE USER DETAILS

if ($_GET['action'] == 'selectAllAudience') {
	$user = new user();
	$data = $_POST;
	$data['secondaryData']['debug'] = 0;
	$data['secondaryData']['returnLists'] = 1;
	$data['secondaryData']['return'] = 'count';
	unset($data['secondaryData']['results_per_page']);
	
	$users = $user->audience(ID,0,$user->setupSearchData($data));
	echo json_encode($users);
}//END SELECT ALL USERS

if ($_GET['action'] == 'saveNewList') {
	$data = $_POST['data'];
	echo json_encode($user->audienceSaveList($data));
	exit();
}//END SELECT ALL USERS

if ($_GET['action'] == 'providerData') {
	include(ABSPATH."/material/openInviter/OpenInviter/openinviter.php");
	$data = $_POST['data'];
	$ers=array();$oks=array();$import_ok=false;$done=false;
	$inviter=new OpenInviter();
	$oi_services=$inviter->getPlugins();
	$inviter->startPlugin('gmail');
	$internal=$inviter->getInternalError();
	$email = $data['email_box'];
	$password = $data['password_box'];
	$provider = $data['provider_box'];

		if (count($ers)==0)
			{
			$inviter->startPlugin($provider);
			$internal=$inviter->getInternalError();
			if ($internal)
				$ers['inviter']=$internal;
			elseif (!$inviter->login($email,$password))
				{
					echo $provider;
				$internal=$inviter->getInternalError();
				$ers['login']=($internal?$internal:"Login failed. Please check the email and password you have provided and try again later !");
				}
			elseif (false===$contacts=$inviter->getMyContacts(array('return'=>'full')))
				$ers['contacts']="Unable to get contacts !";
			else
				{
					
				$import_ok=true;
				$step='send_invites';
				$_POST['oi_session_id']=$inviter->plugin->getSessionID();
				$_POST['message_box']='';
				}
			}
//			print_r($contacts);
$smarty->assign("contacts",$contacts);
$smarty->display('modules/users/syncGoogle.tpl');
	exit();
}//END SELECT ALL USERS

if ($_GET['action'] == 'applyListChanges') {
	
	$data = $_POST['data'];
if (($data['listidInsert'] AND $data['listidDelete'])) {
	$list = array_merge($data['listidDelete'],$data['listidInsert']);
	$ret['listsAdded'] = $data['listidInsert'];
	$ret['listsDeleted'] = $data['listidDelete'];
}
elseif ($data['listidInsert'] AND !$data['listidDelete'])
{
	$list = $data['listidInsert'];
	$ret['listsAdded'] = $data['listidInsert'];
}
elseif ($data['listidDelete'] AND !$data['listidInsert'])
{
	$list = $data['listidDelete'];
	$ret['listsDeleted'] = $data['listidDelete'];
}
else 
{
	$list = array($data['listid']);
	if ($_POST['action'] == 'add') {
		$ret['listsAdded'] = $data['listid'];
	}
	else {
		$ret['listsDeleted'] = $data['listid'];
	}
}


	if ($data['checkBoxes']) {//CAME FROM SELECTION
		//DELETE OLD ONES
		foreach ($list as $v)
		{
			$sql->db_Delete("users_audience_list_members","listid  = $v AND audienceid IN (".implode(",",$data['checkBoxes']).") AND uid = ".ID);
//			echo "DELETE FROM users_audience_list_members WHERE listid  = $v AND audienceid IN (".implode(",",$data['checkBoxes']).") AND uid = ".ID."<br>";
		}
		//ADD IF NEEDED
		if ($_POST['action'] == 'add' OR $data['listidInsert']) {
			$lists = ($data['listidInsert']) ? $data['listidInsert'] : array($data['listid']);
			foreach ($lists as $b)
			{
				$lines = array();
				foreach ($data['checkBoxes'] as $v)
				{
					$lines[] = "('".ID."','$b','$v')";
				}
				$q = "INSERT INTO users_audience_list_members VALUES  ".implode(",",$lines);
//				echo "$q<br>";
				$sql->q($q);	
				unset($lines);
			}//END ACTIONS
		}
	}//END SELECTION
	else {//CAME FROM RESULT SET
		
	$data['secondaryData']['debug'] = 0;
	$data['secondaryData']['return'] = 'multi';
	$data['secondaryData']['returnLists'] = 0;
	$data['secondaryData']['fields'] = 'id';
	unset($data['secondaryData']['results_per_page']);
	$users = $user->audience(ID,0,$user->setupSearchData($data));

	foreach ($users as $v)
	{
		$checkBoxes[] = $v['id'];
	}
	$listsToDrop = $user->memberLists($checkBoxes,array('listDetails'=>1,'debug'=>0,'groupBy'=>'listid'));
	//DROP FROM THE LISTS ABOVE 
	foreach ($listsToDrop as $v)
	{
//		echo "DELETE FROM users_audience_list_members WHERE listid  = ".$v['id']." AND audienceid IN (".implode(",",$checkBoxes).") AND uid = ".ID."<br>";
		$sql->db_Delete("users_audience_list_members","listid  = ".$v['id']." AND audienceid IN (".implode(",",$checkBoxes).") AND uid = ".ID);
	}

	if ($_POST['action'] == 'add' OR $data['listidInsert']) {
		$lists = ($data['listidInsert']) ? $data['listidInsert'] : array($data['listid']);
		foreach ($lists as $b)
		{
			foreach ($checkBoxes as $v)
			{
				$lines[] = ID.";$b;$v";
			}
		}//END ACTIONS
		$file = ABSPATH."/uploads/listMembersCSV".time().".csv";
		$fp = fopen($file,"w");
		fwrite($fp,implode("\n",$lines));
		fclose($fp);
		mysql_query("LOAD DATA INFILE '".str_replace('\\','/',$file)."' INTO TABLE users_audience_list_members FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' (uid,listID,audienceid);");
	}//END INSERTS
	
	}//END RESULT SET
	echo json_encode($ret);
		exit();
}//END APPLY LIST CHANGES

if ($_GET['action'] == 'findUser')
{
    $uid = text_decrypt($_GET['uid']);
	$users = $user->audience(ID,$uid,array('return'=>'single','detailed'=>1,'lists'=>1,'listSettings'=>array('listDetails'=>1)));
	$smarty->assign("user",$users);
	$smarty->display("modules/users/userCard.tpl");
	exit();
}//END FIND USER

if ($_GET['action']=='new_user_entry'){
 	
	$user = new user();
	$save_user=$user->addUser($_POST['data'],array('debug'=>0,'action'=>'addUser'));
	 if (!empty($_POST['data'])) {	
		$new_user=array('id'=>$save_user, 'uname'=>$_POST['data']['uname'], 'user_name'=>$_POST['data']['user_name'],'profile'=> null, 'user_surname'=>$_POST['data']['user_surname'], 'email'=>$_POST['data']['email']);
  }	
	
	echo json_encode(array('results'=>$new_user));
	exit();
		
} //end action

if ($_GET['action'] == 'uploadAudience') {
$tempFile = $_FILES['Filedata']['tmp_name'];	
$targetPath = ABSPATH."/uploads/"; 
$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];

if (move_uploaded_file($tempFile,$targetFile)) {
	$settings = form_settings_array($_POST['data']);
	$csv =$user->parseAudienceCSV($targetFile,array_merge($settings,array('lines'=>5,'import'=>0,'getHeaders'=>1)));
$smarty->assign("csv",$csv);
$smarty->assign("file",$targetFile);
$smarty->assign("posted_data",$settings);
$smarty->display("modules/users/setupCsvHeaders.tpl");

}//END UPLOAD
}//END UPLOAD AUDIENCE

if ($_GET['action'] == 'changeUploadSettings') {
	$settings = $_POST['data'];
	$targetFile = $settings['file'];
	$csv =$user->parseAudienceCSV($targetFile,array_merge($settings,array('lines'=>5,'import'=>0,'getHeaders'=>1)));
	$smarty->assign("csv",$csv);
	$smarty->assign("file",$targetFile);
	$smarty->assign("posted_data",$settings);
	$smarty->display("modules/users/setupCsvHeaders.tpl");

}//END TRY AGAIN 

if ($_GET['action'] == 'searchUsers') {
	$data = $_POST;
	
	$data['secondaryData']['debug'] = 0;
	$data['secondaryData']['detailed'] = 1;
	$data['secondaryData']['lists'] = 1;
	$data['secondaryData']['listSettings'] = array('listDetails'=>1);
	$data['secondaryData']['page'] = $data['page'];
	$users = $user->audience(ID,0,$user->setupSearchData($data));
	$smarty->assign("page",$secondaryData['page']);
	$smarty->assign("audience",$users);
	$smarty->assign("lists",$user->audienceLists(ID,0,array('debug'=>0)));
	$smarty->display("modules/users/audienceList.tpl");
	exit();
}//END SEARCH

if ($_GET['action'] == 'saveAudience') {
	$settings = $_POST['data'];
	$targetFile = $settings['file'];
	$csv =$user->parseAudienceCSV($targetFile,array_merge($settings,array('import'=>0,'getHeaders'=>1)));
	$res = $user->importAudienceCSV($csv['results'],$settings);
	$smarty->assign("res",$res);
	$smarty->display("modules/users/audienceUploadResults.tpl");
	exit();
}//END FUNCTIONS

if ($_GET['action'] == 'lookUp') {
	$field = $_POST['likeData']['field'];
	$val = $_POST['likeData']['lookup'];
	$data['likeData'][$field] = $val;
	$data['secondaryData']['debug'] = 0;
	$data['secondaryData']['fields'] = "id,".$field;
	$data['secondaryData']['return'] = 'multi';
	$data['secondaryData']['orderby'] = $field;
	$data['secondaryData']['results_per_page'] = 10;
	$data['secondaryData']['groupBy'] = $field;
	$users = $user->audience(ID,0,$user->setupSearchData($data));
	$i=0;
	foreach ($users as $v)
	{
		$tmp[$i]['id'] = $v['id'];
		$tmp[$i]['label'] = $v[$field];
		$tmp[$i]['value'] = $v[$field];
		$i++;
	}
	
	echo json_encode($tmp);
	exit();
}//END LOOKUP DATA

/*echo "Started<Br>";
ob_flush(); flush();
$targetFile = '/var/www/vh1.dev.xmasgroup.com/uploads/customers febr 2011.csv';	
	$settings = array('delimiter' =>';','enclose'=>'"','firstRowIsHeader'=>'on','file'=>$targetFile,'row-0'=>'name','row-1'=>'mobile','row-2'=>'list','row-3'=>'email','splitName'=>'surnameName');
echo "Processing...<Br>";
	$csv =$user->parseAudienceCSV($targetFile,array_merge($settings,array('import'=>0,'getHeaders'=>1)));
	$res = $user->importAudienceCSV($csv['results'],$settings);
		
	$smarty->assign("res",$res);
	$smarty->display("modules/users/audienceUploadResults.tpl");*/

?>