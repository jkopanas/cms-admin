<?php
	include("init.php");
	$eshop = new eshop();

	$box = new siteModules();
	$layout = $box->pageBoxes('eshop',e_FILE,'front',array('getBoxes'=>'full','boxFields'=>'id,title,name,template,settings',"debug" => 0));
	$smarty->assign("layout",$layout['boxes']);
	
	$smarty->assign("settings_user",json_decode(SETTINGS,true));
	$smarty->assign("nav_area","checkout");
	$smarty->assign("payment_methods",$eshop->paymentMethods(0,array('shipping_methods'=>1)));
	$smarty->assign("shipping",$eshop->getShippingMethods(155,1));
	$smarty->assign("include_file","modules/users/checkout.tpl");//assigned template variable include_file

	if (is_numeric(ID)) {
		foreach($_POST as $key => $value ){
			$id=explode("-",$key);
			$_SESSION['cart'][$id[1]]['quantity']=$value;
			$_SESSION['cart'][$id[1]]['total']=$value*$_SESSION['cart'][$id[1]]['price'];
		}
		if (!empty($_SESSION['cart'])) {
			foreach ($_SESSION['cart'] as $k=>$v)
			{
				if ($_SESSION['cart'][$k]['saved'] == 0 ) {			
					$item = new Items(array('module'=>$loaded_modules['products']));
					$product = $item->GetItem($k,array('debug'=>0,'fields'=>'id,title,permalink','thumb'=>1));
					$eshop->doAddToCart(ID,$product,array("quantity" => $_SESSION['cart'][$k]['quantity']));
					$_SESSION['cart'][$k]['saved'] = 1;
				}
			}
		}
	}


$smarty->display("home.tpl");//Display the home.tpl template

?>