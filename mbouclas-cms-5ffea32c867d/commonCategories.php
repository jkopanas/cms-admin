<?php
include("init.php");
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ###########################################
$current_module = $loaded_modules[$_GET['module']];
$smarty->assign("current_module",$current_module);
$Content = new Items(array('module'=>$current_module));
$sql->db_Select("common_categories_tree INNER JOIN common_categories_tables ON (common_categories_tables.id=common_categories_tree.tableID)","categoryid,category,tableID,table_name,common_categories_tree.settings,alias,parentid","alias = '".$_GET['cat']."' 
AND table_name = '".$_GET['table']."' AND common_categories_tree.module = '".$_GET['module']."'");
if ($sql->db_Rows()) {
	$current_category = execute_single($sql);

	$current_category['settings'] = json_decode($current_category['settings'],true);
	
	if (array_key_exists('themes',$loaded_modules)) 
	{
		$active_theme = load_theme_by_name($current_category['settings']['theme'],$theme_module['settings']);
		$theme_settings = json_decode($active_theme['settings'],true);
		$smarty->assign("theme_settings",$theme_settings);
	}//END OF content MODULE
	
	if ($current_category['settings']['orderby'] AND !$posted_data['sort'] AND !$posted_data['sort_direction']) {
		$posted_data['sort'] = $current_category['settings']['orderby'];
		$posted_data['sort_direction'] = $current_category['settings']['way'];
	}
	if ($current_category['settings']['results_per_page'] AND !$posted_data['results_per_page']) {
		$posted_data['results_per_page'] = $current_category['settings']['results_per_page'];
	}
	else {
		$posted_data['results_per_page'] = ($settings['results_per_page']) ? $settings['results_per_page'] : $current_module['settings']['items_per_page'];	
	}

//	$posted_data['results_per_page'] = 1;
	$posted_data['active'] = 1;
	$posted_data['page'] = $page;
	$posted_data['searchInSubcategories'] = 1;
	$posted_data['itemSettings'] = array('GetCommonCategoriesTree'=>1,'availability'=>1,'thumb'=>1,'main'=>1,'debug'=>0);
	if ($theme_settings['efields']) {//SEE IF THE THEME REQUIRES EFIELDS
		$posted_data['itemSettings']['efields'] = 1;
	}
	$smarty->assign("posted_data",$posted_data['itemSettings']);
	$items = $Content->CommonCategoriesTreeItems($current_category['tableID'],$_GET['module'],$current_category['categoryid'],array_merge($posted_data,array('debug'=>0,'fields'=>'itemid','return'=>'paginated')));
	$nav = $Content->commonTreeCatNav($current_category['tableID'],$current_category['categoryid']);
	$smarty->assign("items",$items);
	$smarty->assign("cat",$current_category);

	$smarty->assign("nav",$nav);//assigned template variable a
	foreach ($items as $v) {
		$tmp[] = $v['id'];
	}
	$posted_data['filter']['commonCategories'][$current_category['tableID']][$current_category['categoryid']] = $searchHooks[] = $current_module['name'].".id IN (".implode(",",$tmp).")";
	$posted_data['justIds'] = 1;
	$allItemIds = $Content->ItemSearch($posted_data,array('hooks'=>$searchHooks,'tablesHooks'=>$tablesHooks,'fieldsHooks'=>$fieldsHooks),$posted_data['page'],0);
	$implodedItemIds = implode(',',$allItemIds);
	$filters = $Content->applyCategoryFilters($current_module['name'],$implodedItemIds,array('commonCategories'=>1,'efields'=>1,'recursive'=>1));
//print_r($filters['commonCategories']);
	$fomatedArray[$current_category['table_name']]= $current_category;
	$fomatedArray[$current_category['table_name']]['nav'] = $nav;
	$fomatedArray[$current_category['table_name']]['results'] = $Content->CommonCategoriesTree($current_category['tableID'],$current_category['parentid'],array('debug'=>0,'fields'=>'categoryid,category','num_items'=>1,'num_subs'=>1,'filteredItems'=>$implodedItemIds));
	$smarty->assign("commnonCategories",$fomatedArray);
//	$smarty->assign("commnonCategories",$filters['commonCategories']);
	$Efields =  new ExtraFields(array('module'=>array('name'=>$current_module['name'])));
	$efieldValues = $Efields->filterItemsEfields(0,$current_module['name'],array('grouped'=>1,'debug'=>0,'searchable'=>1,'count'=>1,'values'=>1,'defZero'=>1,'mergeWithItems'=>$implodedItemIds));//This will get ALL fields
	$smarty->assign('efields',$efieldValues);
	$smarty->assign("more_categories",$Content->ItemTreeCategories(0,array('table'=>'listings_categories')));
	if ($loaded_modules['bookings']) {
		$book = new bookings(array('module'=>$current_module));
		$smarty->assign("priceRanges",$book->priceRanges($allItemIds,array('return'=>'int')));
		
	}
	
     if ($loaded_modules['maps']) {
 	$settings = array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid','debug'=>0);
 	$map = new maps(array('module'=>$current_module));
 	$mapItems = $map->getItems($implodedItemIds,$settings);
	
 	$smarty->assign('mapItemsRaw',json_encode($mapItems));
 	
	if ($mapItems) {
		$formatedItems = $map->formatItems($mapItems,array('module'=>$current_module));
		$smarty->assign("mapItems",$formatedItems);
		$smarty->assign("fomatedItemsRaw",json_encode($formatedItems));
	}
 	
 }//END MAPS
	
}//END ITEMS
	$smarty->assign("area","commonCategories");
	$table = $Content->ListCommonCategories($current_module['name'],array('type'=>'tree'),$current_category['tableID']);
	$smarty->assign("prefix","common/".$current_module['name']."/".$table['table_name']);
$smarty->assign("include_file",$loaded_modules['themes']['folder']."/".$active_theme['file']);//assigned template variable include_file
include(ABSPATH."/memcache.php");
$smarty->display("home.tpl");//Display the home.tpl template
?>