<?php 
include("init.php");
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ###########################################
$current_module = $loaded_modules['content'];
$smarty->assign("content_module",$current_module);
$smarty->assign("current_module",$current_module);
$hookFiles = $hooks->getHooks(basename(e_SELF));
$hooks->loadHooks($hookFiles['pre']);
if ($_GET['cat']) {
	$posted_data = array();//setup a a clean array
	$cat = $_GET['cat'];
	if (is_numeric($cat)) {
	$cat = ($cat) ? $cat : 0;
	}
	else {//REVERSE LOOKUP
		$cat = str_replace("content/","",$cat);
		$sql->db_Select("content_categories","categoryid","alias = '$cat'");
		if ($sql->db_Rows()) {
			$a = execute_single($sql);
			$cat = $a['categoryid'];
		}
	}
	
	$current_category = $Content->ItemCategory($cat,array('table'=>'content_categories','settings'=>1));
	if (array_key_exists('themes',$loaded_modules)) 
	{
		$active_theme = load_theme_by_name($current_category['settings']['theme'],$theme_module['settings']);
		$theme_settings = unserialize($active_theme['settings']);
		$smarty->assign("theme_settings",$theme_settings);
	}//END OF IF
	
	$posted_data = array('categoryid'=>$cat,'availability'=>1,'thumb'=>1,'main'=>1,'images'=>1);
//	$posted_data['sort'] = ($settings['sort']) ? $settings['sort'] : $current_module['settings']['default_sort'];
	
	if ($current_category['settings']['orderby'] AND !$posted_data['sort'] AND !$posted_data['sort_direction']) {
		$posted_data['sort'] = $current_category['settings']['orderby'];
		$posted_data['sort_direction'] = $current_category['settings']['way'];
	}
	elseif (!$posted_data['sort'] AND !$posted_data['sort_direction'] AND !$current_category['orderby'])
	{
		$posted_data['sort'] = $current_module['settings']['default_content_sort'];
		$posted_data['sort_direction'] = $current_module['settings']['default_sort_direction'];
	}
	if ($current_category['settings']['results_per_page'] AND !$posted_data['results_per_page']) {
		$posted_data['results_per_page'] = $current_category['settings']['results_per_page'];
	}
	else {
		$posted_data['results_per_page'] = ($settings['results_per_page']) ? $settings['results_per_page'] : $current_module['settings']['items_per_page'];	
	}
	if ($theme_settings['efields']) {//SEE IF THE THEME REQUIRES EFIELDS
		$posted_data['efields'] = 1;
	}
	$posted_data['page'] = $page;
	$items = $Content->ItemSearch($posted_data,$current_module,$page,0);
	$subs = $Content->ItemTreeCategories($current_category['categoryid'],array('table'=>'content_categories','debug'=>0));
	$moreCategories = $Content->ItemTreeCategories($current_category['parentid'],array('table'=>'content_categories'));
	$smarty->assign("cat",$current_category);
	$smarty->assign('nav',$Content->CatNav($current_category['categoryid'],array('debug'=>0,'table'=>'content_categories')));
	$smarty->assign("subs",$subs);
	$smarty->assign("more_categories",$moreCategories);
	########################## HACK TO DISPLAY IMAGES FROM ONE FEATURED ITEM ############################################
	if ($items['results']) {
		foreach ($items['results'] as $key => $value) {
		$Docs = new ItemMedia(array('module' => $current_module,'itemid' => $value['id'],'debug'=>0,'orderby' => 'orderby','available'=>1));
		$document=$Docs->ItemDocuments();
		}
		$smarty->assign("docs",$document);
		
	}
	if ($items['results']) {
		foreach ($items['results'] as $k=>$v) {
			$allItems[] = $v['id'];
			$reversedItems[$v['id']] = $v;
		}
		$implodedItemIds = implode(',',$allItems);
	}	
         if ($loaded_modules['maps']) {
         	$settings = array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid,title,content','debug'=>0);
         	$map = new maps(array('module'=>$current_module));
         	$mapItems = $map->getItems($implodedItemIds,$settings);
         	$smarty->assign('mapItemsRaw',json_encode($mapItems));
         	
			if ($mapItems) {
				$formatedItems = $map->formatItems($mapItems,array('module'=>$current_module,'fields'=>"id"));
				foreach ($formatedItems['items'] as $k=>$v) { 
					$formatedItems['items'][$k]['item'] = $reversedItems[$v['item']['id']];
				}
				$smarty->assign("mapItems",$formatedItems);
				$smarty->assign("fomatedItemsRaw",json_encode($formatedItems));
			}
         	
         }//END MAPS
	########################### END HACK ################################################################################
	if (array_key_exists('themes',$loaded_modules)) 
	{
		$active_theme = load_theme_by_name($current_category['settings']['theme'],$theme_module['settings']);
		$theme_settings = json_decode($active_theme['settings'],true);
		$smarty->assign("theme_settings",$theme_settings);
	}//END OF content MODULE
}
$l = new siteModules();
$layout = $l->pageBoxes('all',e_FILE,'front',array('getBoxes'=>'full','fields'=>'boxes,areas,id,settings','boxFields'=>'id,title,name,settings,fetchType,file,required_modules,template','init'=>1,'boxFilters'=>array('active'=>1),'debug'=>0));
$smarty->assign("layout",$layout['boxes']);



foreach((array) $subs as $key=>$value) {
	
	$posted_data1 = array('categoryid'=>$value['categoryid'],'availability'=>1,'thumb'=>1,'main'=>1);
//	$posted_data['sort'] = ($settings['sort']) ? $settings['sort'] : $current_module['settings']['default_sort'];
	
	$posted_data['page'] = $page;
	$items1[] = $Content->ItemSearch($posted_data1,$current_module,$page,0);	
	
}

foreach((array)$items1 as $key=>$value) {
	$items2[]=$value['results'];
}
//print_ar($subs);

$smarty->assign("items1",$items2);
$hooks->loadHooks($hookFiles['post']);
$smarty->assign("featured",$Content->FeaturedContent($cat,array('get_results'=>1,'active'=>1,'debug'=>0,'thumb'=>1,'type'=>'itm','orderby'=>'orderby','cat_details'=>1)));
$validation_arr = array( rand(1,10), rand(1,10));
$smarty->assign("validation",$validation_arr);
$smarty->assign("validation_sum",array_sum($validation_arr));
$smarty->assign("mode",$_GET['mode']);
//print_r($latest['results'][0]);
$smarty->assign("nav_area","content");
$smarty->assign("area",$current_category['alias']);
$smarty->assign("items",$items['results']);
$smarty->assign("include_file",$loaded_modules['themes']['folder']."/".$active_theme['file']);//assignassigned template variable include_file
include(ABSPATH."/memcache.php");
$smarty->caching = USE_SMARTY_CAHCHING;
$args =  array("module" => $current_module,'category'=>$current_category,'items'=>$items['results'],'subs'=>$subs,'moreCategories'=>$moreCategories,'nav'=>$nav);
HookParent::getInstance()->doTriggerHook($current_module['name'], "ContentFetch",$args);
$smarty->display("home.tpl",$url);//Display the home.tpl template
?>