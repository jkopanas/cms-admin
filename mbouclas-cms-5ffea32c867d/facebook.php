<?php 
include("init.php");
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ###########################################
$current_module = $loaded_modules['content'];
$smarty->assign("content_module",$current_module);

if ($_GET['cat']) {
	$posted_data = array();//setup a a clean array
	$cat = $_GET['cat'];
	if (is_numeric($cat)) {
	$cat = ($cat) ? $cat : 0;
	}
	else {//REVERSE LOOKUP
		$cat = str_replace("content/","",$cat);
		$sql->db_Select("content_categories","categoryid","alias = '$cat'");
		if ($sql->db_Rows()) {
			$a = execute_single($sql);
			$cat = $a['categoryid'];
		}
	}
	$current_category = $Content->ItemCategory($cat,array('table'=>'content_categories','settings'=>1));
	if (array_key_exists('themes',$loaded_modules)) 
	{
		$active_theme = load_theme_by_name($current_category['settings']['theme'],$theme_module['settings']);
		$theme_settings = unserialize($active_theme['settings']);
		$smarty->assign("theme_settings",$theme_settings);
	}//END OF IF
	
	$posted_data = array('categoryid'=>$cat,'availability'=>1,'thumb'=>1,'main'=>1);
//	$posted_data['sort'] = ($settings['sort']) ? $settings['sort'] : $current_module['settings']['default_sort'];
	
	if ($current_category['settings']['orderby'] AND !$posted_data['sort'] AND !$posted_data['sort_direction']) {
		$posted_data['sort'] = $current_category['settings']['orderby'];
		$posted_data['sort_direction'] = $current_category['settings']['way'];
	}
	elseif (!$posted_data['sort'] AND !$posted_data['sort_direction'] AND !$current_category['orderby'])
	{
		$posted_data['sort'] = $current_module['settings']['default_content_sort'];
		$posted_data['sort_direction'] = $current_module['settings']['default_sort_direction'];
	}
	if ($current_category['settings']['results_per_page'] AND !$posted_data['results_per_page']) {
		$posted_data['results_per_page'] = $current_category['settings']['results_per_page'];
	}
	else {
		$posted_data['results_per_page'] = ($settings['results_per_page']) ? $settings['results_per_page'] : $current_module['settings']['items_per_page'];	
	}
	if ($theme_settings['efields']) {//SEE IF THE THEME REQUIRES EFIELDS
		$posted_data['efields'] = 1;
	}

	$posted_data['page'] = $page;
	$items = $Content->ItemSearch($posted_data,$current_module,$page,0);

	$smarty->assign("cat",$current_category);
	$smarty->assign('nav',$Content->CatNav($current_category['categoryid'],array('debug'=>0,'table'=>'content_categories')));
	$smarty->assign("subs",$Content->ItemTreeCategories($current_category['categoryid'],array('table'=>'content_categories','debug'=>0)));
	$smarty->assign("more_categories",$Content->ItemTreeCategories($current_category['parentid'],array('table'=>'content_categories')));
	if (array_key_exists('themes',$loaded_modules)) 
	{
		$active_theme = load_theme_by_name($current_category['settings']['theme'],$theme_module['settings']);;
		$theme_settings = unserialize($active_theme['settings']);
		$smarty->assign("theme_settings",$theme_settings);
	}	//END OF content MODULE
}

$smarty->assign("nav_area","content");
$smarty->assign("area",$current_category['alias']);
$smarty->assign("items",$items['results']);
$smarty->assign("include_file",$loaded_modules['themes']['folder']."/".$active_theme['file']);//assigned template variable include_file
include(ABSPATH."/memcache.php");
$smarty->caching = USE_SMARTY_CAHCHING;
$args =  array("module" => $current_module) ;
HookParent::getInstance()->doTriggerHook($current_module['name'], "FacebookPreFetch",$args);
$smarty->display("home.tpl",$url);//Display the home.tpl template
?>