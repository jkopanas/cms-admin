<?php
include("init.php");
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ###########################################
$current_module = $loaded_modules[$_GET['module']];
$smarty->assign("current_module",$current_module);
$Content = new Items(array('module'=>$current_module));
$Efields = new ExtraFields(array('module'=>$current_module));
$field = $Efields->ExtraField($Efields->getExtraFieldID($_GET['field']));
$smarty->assign("resultName",$field['field']);
$posted_data = array('availability'=>1,'thumb'=>1,'main'=>1);
$posted_data['efield'] = $_GET['field'];
$posted_data['efieldValue'] = base64_decode($_GET['value']);
$posted_data['active'] = 1;
$posted_data['page'] = $page;

$posted_data['sort'] = ($settings['sort']) ? $settings['sort'] : $current_module['settings']['default_sort'];

if ($current_category['settings']['orderby'] AND !$posted_data['sort'] AND !$posted_data['sort_direction']) {
	$posted_data['sort'] = $current_category['settings']['orderby'];
	$posted_data['sort_direction'] = $current_category['settings']['way'];
}
elseif (!$posted_data['sort'] AND !$posted_data['sort_direction'] AND !$current_category['orderby'])
{
	$posted_data['sort'] = $current_module['settings']['default_content_sort'];
	$posted_data['sort_direction'] = $current_module['settings']['default_sort_direction'];
}
if ($current_category['settings']['results_per_page'] AND !$posted_data['results_per_page']) {
	$posted_data['results_per_page'] = $current_category['settings']['results_per_page'];
}
else {
	$posted_data['results_per_page'] = ($settings['results_per_page']) ? $settings['results_per_page'] : $current_module['settings']['items_per_page'];	
}
if ($theme_settings['efields']) {//SEE IF THE THEME REQUIRES EFIELDS
	$posted_data['efields'] = 1;
}

$settings = array('GetCommonCategoriesTree'=>1,'availability'=>1,'thumb'=>1,'main'=>1,'debug'=>0);

$res = $Content->ItemSearch($posted_data,$settings,$page,0);
$smarty->assign("items",$res['results']);
$smarty->assign("prefix","filters/".$current_module['name']."/".$_GET['field']."/".$_GET['value']);
$smarty->assign("include_file","modules/themes/searchResults.tpl");//assigned template variable include_file
include(ABSPATH."/memcache.php");
$smarty->display("home.tpl");//Display the home.tpl template
//print_r($res['results']);
?>