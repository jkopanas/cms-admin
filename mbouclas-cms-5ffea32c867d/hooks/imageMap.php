<?php
$mySettings['title'] = "imageMap";
$mySettings['var'] = "imageMap";


$hooks[$mySettings['var']]['settings'] = $mySettings;
$hooks[$mySettings['var']]['tpl'] = "modules/hooks/imageMap.tpl";
function tagReplace($input,$item,$lang,$extras=0)
{
	$find = array('{$ID}','{$FRONT_LANG}','{$PERMALINK}','{$TITLE}','{$MAIN_IMAGE}');
	$replace = array($item['id'],$lang,$item['permalink'],$item['title'],$item['image_full']['main']);
	return str_replace($find,$replace,$input);
}
?>