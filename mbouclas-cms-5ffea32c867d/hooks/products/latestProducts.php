<?php
$a = new Products(array('module'=>$loaded_modules['products']));
$s = $a->LatestItems($me['settings']);
if ($me['fetchType'] == 'static') {
	$smarty->assign("prefix",'product');
	$smarty->assign('latestProducts',$s);
}
else {
	echo json_encode(array('latestProducts'=>$s));
}
?>