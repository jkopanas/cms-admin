<?php
$mySettings['title'] = "Audience";
$mySettings['var'] = "audience";

$hooks[$mySettings['var']]['settings'] = $mySettings;
$hooks[$mySettings['var']]['tpl'] = "modules/users/audience.tpl";

$smarty->assign("lists",$user->audienceLists(ID,0,array('debug'=>0)));
$smarty->assign("audience",$user->audience(ID,0,array('debug'=>0,'return'=>'paginated','orderby'=>'date_added','way'=>'DESC','results_per_page'=>20,'detailed'=>1,'lists'=>1,'listSettings'=>array('listDetails'=>1))));

?>