<?php
$mySettings['title'] = "My Account";
$mySettings['var'] = "myAccount";

$hooks[$mySettings['var']]['settings'] = $mySettings;
$hooks[$mySettings['var']]['tpl'] = "modules/users/cpanel/myAccount.tpl";


$user= new user();
$Efields = new ExtraFields("user");
$theme = load_theme_by_name("userProfile","");

$settings=array('efields'=>1,"profile"=>1);
$details = $user->userDetails(ID,$settings);
$selectedItems=$details["efields"];

if(is_array($selectedItems))
{
    foreach($selectedItems as $v)
    {
        $myitems[] = $v["fieldid"]."_".$v['value'];
    }
    
    $useritems = array_flip($myitems);
    $smarty->assign("useritems",$useritems);
}

$groups = $Efields->getEFieldsGroups(array("theme"=>$theme['id']));
$smarty->assign("groups",$groups);

?>