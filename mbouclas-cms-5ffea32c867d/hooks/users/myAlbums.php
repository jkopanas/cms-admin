<?php

$mySettings['title'] = "My Albums";
$mySettings['var'] = "myAlbums";

$hooks[$mySettings['var']]['settings'] = $mySettings;
$hooks[$mySettings['var']]['tpl'] = "modules/users/cpanel/myAlbums.tpl";

$user= new user();
$images = new userImages();

$settings=array("uid"=>ID,'count'=>1);
$albums=$images->searchAlbums($settings);

$smarty->assign('albums',$albums);