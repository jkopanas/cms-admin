<?php
$mySettings['title'] = "profile";
$mySettings['var'] = "profile";

$hooks[$mySettings['var']]['settings'] = $mySettings;
$hooks[$mySettings['var']]['tpl'] = "modules/users/audience.tpl";
unset($tmp);

foreach ($varProfile as $k=> $v)
{
	if (!is_array($range) OR !array_key_exists($k,$range)) {
		if (is_array($v)) {
			foreach ($v as $b) {
				$tmp[] = " '$b'";
			}
			$qProfile[] = "users_profile.$k  IN (".implode(",",$tmp).") ";
		}
	}
}//END LOOP

if ($varsRangeProfile) {

	foreach ($varsRangeProfile as $k=>$v)
	{
		if ($k == 'user_birthday') {
			$tablesJoin[] = ' INNER JOIN users ON (users.id=users_profile.id)';
			foreach ($v as $key=>$b) {
				$varsRangeProfile[$k][$key] = mktime(0, 0, 0, 1, 1, date('Y')-$b+1);
			}
		}

		if ($v['from'] AND $v['to']) {
			$qProfile[] = "$k  BETWEEN \"".$varsRangeProfile[$k]['from']."\" AND \"".$varsRangeProfile[$k]['to']."\"";
		}
		elseif ($v['from'] AND !$v['to'])
		{
			$qProfile[] = "$k   > ".$varsRangeProfile[$k]['from'];
		}
		elseif (!$v['from'] AND $v['to'])
		{
			$qProfile[] = "$k  < ".$varsRangeProfile[$k]['from'];
		}
	}
	$tablesJoin = implode(" ",array_unique($tablesJoin));
}

$qProfile = " AND ".implode(' AND ',$qProfile);
?>