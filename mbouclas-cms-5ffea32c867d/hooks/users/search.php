<?php
$mySettings['title'] = "Search";
$mySettings['var'] = "search";

$hooks[$mySettings['var']]['settings'] = $mySettings;
$hooks[$mySettings['var']]['tpl'] = "modules/users/cpanel/search.tpl";
$Efields = new ExtraFields(array('module'=>$loaded_modules['users']));

$sql->db_Select("pro_country_spr","id,name");
$countries = execute_multi($sql);

$sql->db_Select("pro_city_spr","id,name","id_country = 94");
$cities = execute_multi($sql);
$smarty->assign("cities",$cities);
$smarty->assign("countries",$countries);

$fields = $Efields->GetExtraFields(array('debug'=>0,'way'=>'ASC'));
$smarty->assign("fields",$fields);
?>