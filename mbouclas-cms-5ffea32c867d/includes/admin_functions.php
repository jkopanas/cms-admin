<?php
function getperms($arg, $ap = ADMINPERMS){
        if($ap == "0"){return TRUE;}
        if($ap == ""){return FALSE;}
        $ap = ".".$ap;
        if(preg_match("#\.".$arg."\.#", $ap)){
                return TRUE;
        } else {
                return FALSE;
        }
}//END OF FUNCTION

function read_images($path,$extentions)
{
	
if ($handle = opendir($path)) 
{ 
   while (false !== ($file = readdir($handle))) 
   {
   	for ($i = 0; $i < count($extentions); $i++)
{
if (eregi("\.". $extentions[$i] ."$", $file) && $file !="null.gif") 
{
	$filetime = (file_exists($path."/".$file)) ? filemtime($path."/".$file) : "";
  $files[] = array('file' => $file, 'time' => $filetime, 'path' =>$path."/".$file);
}//END OF IF
}//END OF FOR
   }//END OF WHILE
   closedir($handle); 
}//END OF IF 
if ($files) 
{  
rsort($files);
}//END OF IF
return $files;
}//END OF FUNCTION

function watermark($img,$watermark) 
{
$watermark = imagecreatefrompng($watermark);//create watermark canvas   
$watermark_width = imagesx($watermark);//watermark width  
$watermark_height = imagesy($watermark);//watermark height  
$image = imagecreatetruecolor($watermark_width, $watermark_height);//create image canvas  
$image = imagecreatefromjpeg($img);//new file  
$size = getimagesize($img);  
$dest_x = $size[0] - $watermark_width - 5;  
$dest_y = $size[1] - $watermark_height - 5;  
imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, 100);//merge the two images  
imagejpeg($image,$img,90);//save the new file  
imagedestroy($image);//empty the memory space from the new image  
imagedestroy($watermark);//empty the memory space from the watermark
}//END OF FUNCTION watermark

function resizeToFile ($sourcefile, $dest_x, $dest_y, $targetfile, $jpegqual,$proportions)
{
/*resizeToFile resizes a picture and writes it to the harddisk
*  
* $sourcefile = the filename of the picture that is going to be resized
* $dest_x   = X-Size of the target picture in pixels
* $dest_y   = Y-Size of the target picture in pixels
* $targetfile = The name under which the resized picture will be stored
* $jpegqual   = The Compression-Rate that is to be used 
*/

/* Get the dimensions of the source picture */
$picsize=getimagesize("$sourcefile");
$source_x  = $picsize[0];
$source_y  = $picsize[1];
$source_id = imageCreateFromJPEG("$sourcefile");

//constrain proportions algorithm
if ($proportions == 1) 
{  
$w = $dest_x;
$new_width = ($source_x * $dest_y) / $source_y;
if ($new_width > $w) {
$new_width = $w;
$dest_y = ($source_y * $new_width) / $source_x;
}
$dest_x = $new_width;
$dest_y = $dest_y;
}//END OF IF
/* Create a new image object (not neccessarily true colour) */
$target_id=imagecreatetruecolor($dest_x, $dest_y);
 /* Resize the original picture and copy it into the just created image
  object. Because of the lack of space I had to wrap the parameters to 
  several lines. I recommend putting them in one line in order keep your
  code clean and readable */
$target_pic=imagecopyresampled($target_id,$source_id,0,0,0,0,$dest_x,$dest_y,$source_x,$source_y);
/* Create a jpeg with the quality of "$jpegqual" out of the
  image object "$target_pic".
  This will be saved as $targetfile */
imagejpeg ($target_id,"$targetfile",$jpegqual);
return true;
}//END OF FUNCTION

function duplicate_language($code) 
{ 
	//create a duplicate of the default language using the new language code
	$sql = new db();
	$sql2 = new db();
	$sql->db_Select("languages","*","code = '".DEFAULT_LANG."'");
		for ($i=0;$sql->db_Rows() > $i;$i++)
	 	{
	 	$r = $sql->db_Fetch();
		$sql2->db_Insert("languages","'$code','".$r['descr']."','".$r['name']."','".$r['value']."','".$r['topic']."'");
	 	}//END OF FOR
	//now insert it into the countries table
	$r = country_details($code);
	$sql->db_Insert("countries","'$code','".$r['charset']."','N','".$r['country']."','".$r['locale']."',
					 '".$r['logo']."', '".$r['image']."'"); 
}//END OF FUNCTION duplicate_language


function editor($name,$instance,$remove=0,$links=0,$content=0,$settings) 
{ 
	global $smarty,$sql;
	// create editor for main content
//$editor->set_stylesheet('/skin/css/style_front.css');
// Step 3: (required) create a new editor object:
$editor = new wysiwygPro();

// Step 4: (required) give the editor a name (equivalent to the name attribute on a regular textarea):
$editor->name = $name;
//
//print_r($settings['images_folders']);
// Note: folders used by the file browser must be made writable otherwise uploading and file editing will not work.
for ($i=0;count($settings['images_folders']) > $i;$i++)//LOOP THROUGH THE DIRECTORIES
{
	$$a = "dir_$i";
	$$a = new wproDirectory();
	

// set type of folder:
$$a->type = $settings['images_folders'][$i]['type'];
// full file path of the folder:
$$a->dir = $settings['images_folders'][$i]['images_dir'];
// URL of the folder:
$$a->URL = $settings['images_folders'][$i]['images_url'];
// Set access permissions:
$$a->editImages = true;
$$a->renameFiles = true;
$$a->renameFolders = true;
$$a->deleteFiles = true;
$$a->deleteFolders = true;
$$a->copyFiles = true;
$$a->copyFolders = true;
$$a->moveFiles = true;
$$a->moveFolders = true;
$$a->upload = true;
$$a->overwrite = true;
$$a->createFolders = true;
// You can give the directory a custom label using the name attribute:
$$a->name = $settings['images_folders'][$i]['name'];
// If you do not do this the name of the folder will be used instead.
// Note: you can give the directory a custom icon using the icon attribute. Set it to the URL of the icon image.
// add the directory to the editor
$editor->addDirectory($$a);

}


if ($settings['document_folders']) 
{
$docs = new wproDirectory();
$docs->type = 'document';
$docs->renameFiles = true;
$docs->renameFolders = true;
$docs->deleteFiles = true;
$docs->deleteFolders = true;
$docs->copyFiles = true;
$docs->copyFolders = true;
$docs->moveFiles = true;
$docs->moveFolders = true;
$docs->upload = true;
$docs->overwrite = true;
$docs->createFolders = true;

// full directory path of your documents folder for storing PDF and Word files etc:
$docs->dir = $settings['document_folders']['document_dir'];
// url of your documents folder:
$docs->URL = $settings['document_folders']['document_url'];
$editor->addDirectory($docs);

}

if ($settings['media_dir']) 
{
// full directory path of your media folder for storing video files:
$editor->mediaDir = $settings['media_dir'];
// url of your media folder:
$editor->mediaURL = $settings['media_url'];
}


$editor->theme = $settings['theme'];

if ($remove !=0) {
	$remove_buttons = implode(",",$remove);
	$editor->removebuttons($remove_buttons);
}
if ($links !=0) {
	$editor->set_links($links);
}

if ($settings['image_manager']) 
{
	$smarty->assign("file_browser",$editor->fetchFileBrowserJS('OpenFileBrowser'));
}
if ($settings['file_manager']) 
{
	$smarty->assign("editor_js",$editor->fetchFileBrowserJS($settings['file_manager']));
}

$editor->set_stylesheet(CSS_DIR."/".CSS_FRONT);
$smarty->assign_by_ref($name,$editor); 
}//END OF FUNCTION editor





function add_link($source,$dest,$bydircetional) 
{ 
  $sql = new db();
  $sql->db_Select("product_links","*","product_source = '$source' AND product_dest = '$dest'");
  if ($sql->db_Rows() == 0) 
  {   
  $sql->db_Insert("product_links","'$source','$dest',''");
  if ($bydircetional == 1) 
  {  
  	$sql->db_Select("product_links","*","product_source = '$dest' AND product_dest = '$source'");
    if ($sql->db_Rows() == 0) 
  { 	
   $sql->db_Insert("product_links","'$dest','$source',''");
  }//END OF IF
  }//END OF IF
  }//END OF IF
}//END OF FUNCTION add_link

function func_get_image_size($filename) {
   list($width, $height, $type) = getimagesize($filename);
    switch($type) {
        case "1": $type = "image/gif";
                  break;
        case "2": $type = "image/pjpeg";
                  break;
        case "3": $type = "image/png";
                  break;
        default:  $type = "";
    }
	if (!empty($type))
	    return array(filesize($filename),$width,$height,$type);
	else
		return false;
}//END OF FUNCTION

function get_translations($code,$topic,$filter="",$number) 
{
	$number = ($number == "") ? PAGINATION_TOP_ADMIN : $number;
	$topic = ($topic == "") ? "common" : $topic;
	$topic = ($topic == "all") ? "'admin','common'" : "'$topic'";
	$topic_condition = " AND topic IN ($topic) ";
	$sql = new db();
	$sql->db_Select("languages","*","code='$code' AND (name LIKE '%$filter%' OR descr LIKE '%$filter%' or value LIKE '%$filter%') $topic_condition order by name"); 

//	return execute_multi($sql,1);
	return execute_paginated($sql,1,$number);
}//END OF FUNCTION get_translations



function group_prefs() 
{ 
	$sql = new db();
	$sql->db_Select("config","category","GROUP BY category",$mode="no_where");
	return execute_multi($sql,1); 
}//END OF FUNCTION group_prefs

function resize_thumb($img,$resize,$return_folder) 
{  
 $image = (!preg_match(PRODUCT_IMAGES,$img)) ? PRODUCT_IMAGES.$img : $img;
 if ($resize == 1) 
 {
	 ##### Retrieve Filename #############
 $image_dets = explode("/",$image);//Get the image filename
 $image_file = $image_dets[count($image_dets)-1];//Should be the last element
 #### See where to save the thumb #######
 $return_folder = ($return_folder == "self") ? $image_dets[count($image_dets)-2] : "thumbs";
 ##### Create thumb filename ######
 $thumb = $_SERVER['DOCUMENT_ROOT'].PRODUCT_IMAGES."/$return_folder/t_".$image_file;
 	if (preg_match(".jpg",$image)) 
 	{  
 		########### Resize to default thumb ###############3
 		resizeToFile($_SERVER['DOCUMENT_ROOT'].$image,DEFAULT_THUMB_SIZE,DEFAULT_THUMB_SIZE,$thumb,RESAMPLED_QUALITY,RESAMPLED_ASPECT_RATIO);
 	}//END OF IF  
  #### Create a thumb filename #####
 $thumb = PRODUCT_IMAGES."/$return_folder/t_".$image_file; 
 }//END OF IF
 else 
 {  
 $thumb = $image; 
 }//END OF ELSE
 return $thumb;//return filename
}//END OF FUNCTION resize_thumb

function login_history($date_condition,$limit="no",$uname="no") 
{ 
	$sql = new db();
	$g = new convert();
	$limit = ($limit == "no") ? "" : "LIMIT $limit";
	$uname = ($uname == "no") ? "" : "AND login = '$uname'";
	$sql->db_Select("stats_login_history","*","$date_condition $uname $limit");
	if ($sql->db_Rows() > 0) 
	{  
	$login_stats = execute_multi($sql,0);
	for ($x=0;count($login_stats) > $x;$x++)
	{
		$sql->db_Select("users","id","uname = '".$login_stats[$x]['login']."'");
		$tmp = execute_single($sql,0);
		$login_stats[$x]['id'] = $tmp['id'];
		$login_stats[$x]['date_time'] = $g->convert_date($login_stats[$x]['date_time'],"long");
	}//END OF FOR
	return $login_stats;
	}//END OF IF
}//END OF FUNCTION login_history


?>