<?php
function load_theme($item_id,$type,$module_settings)
{
	global $sql;
	
	$sql->db_Select("themes_items","*","itemid = $item_id AND type = '$type'");
//	echo "itemid = $item_id AND type = '$type'";
	if ($sql->db_Rows() > 0) 
	{
		$a = execute_single($sql);
		$sql->db_Select("themes","*","id = ".$a['id']);
		return execute_single($sql);
	}
	else 
	{
		$sql->db_Select("themes","*","id = ".$module_settings['default_theme']);
		return execute_single($sql);
	}
}

function load_theme_by_name($name,$module_settings)
{
	global $sql;
	$sql->db_Select("themes","*","name = '$name'");
	return execute_single($sql);
}
function createResizedImage($srcFile,$destFile,$fix,$maxW,$maxH,$resample_ratio)
	{
		list($srcW, $srcH, $srcType, $html_attr) = getimagesize($srcFile);
		$ext = substr($srcFile, -3);
		$ext = strtolower($ext);

		if($ext == 'jpg') {
			$srcImage = @imagecreatefromjpeg($srcFile);
		} elseif($ext == 'gif') {
			$srcImage = @imagecreatefromgif($srcFile);
			
		} elseif($ext == 'png') {
			$srcImage = @imagecreatefrompng($srcFile);
		}
		if(!$srcImage) {return false;}
		//$srcW = imagesx($srcImage);
		//$srcH = imagesy($srcImage);
		if($fix == 0) {
			if($srcW / $maxW > $srcH / $maxH) {
				$factor = $maxW / $srcW;
			} else {
				$factor = $maxH / $srcH;
			}
		} elseif($fix == 1) {
			$factor = $maxW / $srcW;
		} elseif($fix == 2) {
			$factor = $maxH / $srcH;
		}
		$newH = (int) round($srcH * $factor);
		$newW = (int) round($srcW * $factor);

		$newImage = imagecreatetruecolor($newW, $newH);
		imagecopyresampled($newImage,$srcImage,0,0,0,0,$newW,$newH,$srcW,$srcH);
		$newFile = $destFile;
		imagejpeg($newImage, $newFile, $resample_ratio);
		return true;
	}

function check_var($var,$table,$fields) 
{ 
	global $sql;
	$sql->db_Select($table,$fields,"$fields = $var");
	if ($sql->db_Rows() >0) 
	 {  
	 	return $var; 
	 }//END OF IF 
	 else 
	 {  
	return 0;
	 }//END OF ELSE
}//END OF FUNCTION check_cat
	
function load_module_prefs($module_table,$module_translation_table,$conf=0,$cat="no")
{
	global $smarty,$sql;
	$fields = ($conf == 0) ? "name,value" : "*";

if ($cat == "no") 
{  
$sql->db_Select($module_table,$fields,"ORDER BY orderby",$mode="no_where");
}//END OF IF
else 
{  
$sql->db_Select($module_table,$fields,"category = '$cat' ORDER BY orderby"); 
}//END OF ELSE

$ar = execute_multi($sql);

//Simplify the array
if ($conf == 0) 
{  
 $return = array();
 foreach ($ar as $key => $value) 
 {
	$return[$value[name]] = $value[value];
 }//END OF FOREACH

 ################ LOAD CONSTANTS FROM ARRAY ###################
foreach ($return as $key => $value) 
{ 
   	if (!preg_match("[0-9]",$key)) 
	{
		define(strtoupper($key),$value);
		$smarty->assign(strtoupper($key),$value);
	}//END OF IF
}//END OF FOR
 
}//END OF IF
else 
{
	
	for ($i=0;count($ar) > $i;$i++)
	{
		$trans =  translate($module_translation_table,"comment","name = '".$ar[$i]['name']."' AND code = '".DEFAULT_LANG_ADMIN."'");
		$ar[$i]['comment'] =$trans['comment'];
	}//END OF FOR


return $ar; 
}//END OF ELSE
}//END OF FUNCTION

function execute_multi($sql,$parse=0,$toForm=0) 
{ 
	$t = new Parse();
	if ($sql->db_Rows() > 0) 
	{ 
		for ($i=0;$sql->db_Rows() > $i;$i++)
	 	{
	 	$r = $sql->db_Fetch();
	 	foreach ($r as $key => $value) 
	 	{
	 	   	if (!is_numeric($key)) 
	 	{
	 		$ar[$i][$key] = ($parse == 0) ? $value :$t->toForm($value,1,$toForm);
	 	}//END OF IF
	 	}//END OF FOREACH 
	 	}//END OF FOR 
	 	return $ar;
	 }//END OF IF
	 else 
	 {  
	 return 0; 
	 }//END OF ELSE
}//END OF FUNCTION execute

function execute_single($sql,$parse=0) 
{ 
	$t = new Parse();
	if ($sql->db_Rows() > 0) 
	{ 
	 	$r = $sql->db_Fetch();
	 	foreach ($r as $key => $value) 
	 	{
	 	   	if (!is_numeric($key)) 
	 	{
	 		if ($parse == 1) {
	 			$ar[$key] =  $t->toHTML($value);
	 		}
	 		elseif ($parse == 'toForm')
	 		{
	 		$ar[$key] =  $t->toForm($value,1);
	 		}
	 		else {
	 			$ar[$key] =  $value;
	 		}
	 	}//END OF IF
	 	}//END OF FOREACH 
	 	return $ar;
	 }//END OF IF
	 else 
	 {  
	 return 0; 
	 }//END OF ELSE
}//END OF FUNCTION execute

function paginate_results($current_page,$limit,$total,$suffix=0)
{

	global $smarty;
$pg = new pagination($current_page,$limit);
$pg->setTotalRecords($total);
$suffix = ($suffix == 0) ? "" : $suffix;
if( $total >0 ) { $total_pages = ceil($total/$limit); } else { $total_pages = 0; } 
$smarty->assign("total_pages".$suffix,$total_pages);
$smarty->assign("list".$suffix,$pg->getListCurrentRecords());
$smarty->assign("navigation".$suffix,$pg->getNavigation());
$smarty->assign("num_links".$suffix,$pg->getCurrentPages());
$smarty->assign("PAGE".$suffix,$current_page);
}

function pagination_array($array, $page = 1, $link_prefix = false, $link_suffix = false, $limit_page = 20, $limit_number = 10)
{
	if (empty($page) or !$limit_page) $page = 1;

	$num_rows = count($array);
	if (!$num_rows or $limit_page >= $num_rows) return false;
	$num_pages = ceil($num_rows / $limit_page);
	$page_offset = ($page - 1) * $limit_page;

	//Calculating the first number to show.
	if ($limit_number)
	{
		$limit_number_start = $page - ceil($limit_number / 2);
		$limit_number_end = ceil($page + $limit_number / 2) - 1;
		if ($limit_number_start < 1) $limit_number_start = 1;
		//In case if the current page is at the beginning.
		$dif = ($limit_number_end - $limit_number_start);
		if ($dif < $limit_number) $limit_number_end = $limit_number_end + ($limit_number - ($dif + 1));
		if ($limit_number_end > $num_pages) $limit_number_end = $num_pages;
		//In case if the current page is at the ending.
		$dif = ($limit_number_end - $limit_number_start);
		if ($limit_number_start < 1) $limit_number_start = 1;
	}
	else
	{
		$limit_number_start = 1;
		$limit_number_end = $num_pages;
	}
	//Generating page links.
	for ($i = $limit_number_start; $i <= $limit_number_end; $i++)
	{
		$page_cur = "<a href='$link_prefix$i$link_suffix'>$i</a>";
		if ($page == $i) $page_cur = "<b>$i</b>";
		else $page_cur = "<a href='$link_prefix$i$link_suffix'>$i</a>";

		$panel .= " <span>$page_cur</span>";
	}

	$panel = trim($panel);
	//Navigation arrows.
	if ($limit_number_start > 1) $panel = "<b><a href='$link_prefix".(1)."$link_suffix'>&lt;&lt;</a>  <a href='$link_prefix".($page - 1)."$link_suffix'>&lt;</a></b> $panel";
	if ($limit_number_end < $num_pages) $panel = "$panel <b><a href='$link_prefix".($page + 1)."$link_suffix'>&gt;</a> <a href='$link_prefix$num_pages$link_suffix'>&gt;&gt;</a></b>";

	$output['panel'] = $panel; //Panel HTML source.
	$output['offset'] = $page_offset; //Current page number.
	$output['limit'] = $limit_page; //Number of resuts per page.
	$output['array'] = array_slice($array, $page_offset, $limit_page, true); //Array of current page results.
	
	return $output;
}

function execute_paginated($sql,$parse=0,$number,$translate=0,$current_page=0,$total=0) 
{ 

		global $smarty;

$current_page = (defined(CURRENT_PAGE)) ? CURRENT_PAGE : $current_page;
	$t = new textparse();
	$pg = new pagination($current_page,$number);

	if ($sql->db_Rows() >0) 
	{ 
		if ($total) 
		{
		 	$pg->setTotalRecords($total);//total number of records
		}
		else 
		{
			$pg->setTotalRecords($sql->db_Rows()); 	//total number of records
		} 
	$list = $pg->getListCurrentRecords();//from record to record list
	$r = $pg->results($sql);//Results paginated 

		for ($i=0,$j=$list['from'];count($r) > $i;$i++,$j++)
		{
		if ($translate !=0) 
		{  
			$ar[$i] = translate($translate['table'],$translate['fields'],$translate['arg']);
		}//END OF Translate
	 	foreach ($r[$i] as $key => $value) 
	 	{
	 	   	if (!preg_match("[0-9]",$key)) 
	 	{
	 		$ar[$i][$key] = ($parse == 0) ? $value : $t->formtparev($value);
	 	}//END OF IF
	 	}//END OF FOREACH
		}//END OF FOR
//		echo $smarty->template_dir;
//print_r($pg->getNavigation());
	$smarty->assign("list",$list);
	$smarty->assign("navigation",$pg->getNavigation());
	$smarty->assign("num_links",$pg->getCurrentPages());
	}//END OF RESULTS FOUND
	 return $ar;
}//END OF FUNCTION execute_paginated


function load_modules($active=1,$load=0,$settings=0)
{
	global $sql,$smarty;
	$fields = ($settings['fields']) ? $settings['fields'] : '*';
	if ($active == 1) {
		$active = "active = 1";
	}
	elseif ($active == 0)
	{
		$active = "active = 0";
	}
	else {
		$active = "active IN (0,1)";
	}
	$sql->db_Select("modules",$fields,$active);
	$list = execute_multi($sql);
	
	if ($load !=0) 
	{
		for ($i=0;count($list) > $i;$i++)
		{
			if ($list[$i]['startup'] == 1) 
			{
				if (include(ABSPATH."/".$list[$i]['folder']."/".$list[$i]['main_file'])) 
				{
//					echo "Module ".$list[$i]['name']." Loaded<br>";
				}
				else {
					echo "FAILED";
				}
				
			}
		}
	}
	if ($settings['return'] == 'byKey') {
		foreach ($list as $k=>$v) {
			$tmp[$v['name']] = $v;
		}
		return $tmp;
	}
	return $list;
}

function available_modules($active=1)
{
	global $sql;
	if ($active) 
	{
		$sql->db_Select("modules","*","active = 1");	
	}
	else 
	{
		$sql->db_Select("modules","*");
	}
	if ($sql->db_Rows()) 
	{
		

	$res =  execute_multi($sql,1); 

	return $res;
	}
}

function module_is_active($name,$load=0,$simplify_settings=0,$ajax=0,$cat=0)
{
	global $sql,$smarty,$loaded_modules;
	//check if module is loaded
	if ($loaded_modules AND array_key_exists($name,$loaded_modules))
	{
		return $loaded_modules[$name];
	}
	else
	{
		
	
	$sql->db_Select("modules","*","name = '$name' AND active = 1");
	if ($sql->db_Rows() > 0) 
	{
		$module = execute_single($sql);
		
		if ($load !=0) 
		{
//			require_once(ABSPATH."/".$module['folder']."/".$module['main_file']);
//			echo ABSPATH."/".$module['folder']."/".$module['main_file']."<br>";

		
			$cat = ($cat) ? "category = '$cat' AND " : "";
			$sql->db_Select("modules_settings","*","$cat module_id = ".$module['id']." ORDER BY orderby");
			
			if ($sql->db_Rows() > 0)
			{
				$module['settings_full'] = execute_multi($sql,1);
				if ($simplify_settings) //CREATE A SIMPLIFIED ARRAY TO USE WITH SMARTY AND OTHER APEARENCE PARAMETERS
				{
					for ($i=0;count($module['settings_full']) > $i;$i++)
					{
					foreach ($module['settings_full'][$i] as $key => $value) 
					{
						if ($key == "name") 
						{
							$module['settings'][$value] = $module['settings_full'][$i]['value'];
						}
						//CHECK OUT FOR OPTIONS FIELDS
						if ($key == "options") 
						{
							$option = $module['settings_full'][$i]['options'];
							if ($option) 
							{
								$module['settings_full'][$i]['options_array'] = form_settings_array($option,"###",":::");
							}
						}
					}
					}
				}
			}
			$loaded_modules[$module['name']] = $module;
		}
		return $module;
	}
	else 
	{
		return 0;
	}
	}//END OF RETURN
}


function translate($table,$fields,$arg,$single=1) 
{ 
	global $sql;
	$sql->db_Select($table,$fields,$arg);
	if ($single == 1) 
	{  
	return execute_single($sql,1); 
	}//END OF IF
	else 
	{
		return execute_multi($sql,1);
	}
}//END OF FUNCTION translate

function load_prefs($conf=0,$cat="no")
{
	global $sql;
	$fields = ($conf == 0) ? "name,value" : "*";


if ($cat == "no") 
{  
$sql->db_Select("config",$fields,"ORDER BY orderby",$mode="no_where");
}//END OF IF
else 
{  
$sql->db_Select("config",$fields,"category = '$cat' ORDER BY orderby"); 
}//END OF ELSE

$ar = execute_multi($sql);
//Simplify the array
if ($conf == 0) 
{  
 $return = array();
 foreach ($ar as $key => $value) 
 {
	$return[$value['name']] = $value['value'];
 }//END OF FOREACH
return $return;
}//END OF IF
else 
{
	
	for ($i=0;count($ar) > $i;$i++)
	{
		$trans =  translate("config_lng","comment","name = '".$ar[$i]['name']."' AND code = '".DEFAULT_LANG_ADMIN."'");
		$ar[$i]['comment'] =$trans['comment'];
	}//END OF FOR
return $ar; 
}//END OF ELSE
}//END OF FUNCTION

function load_lang($lang)
{

global $sql;
$t = new textparse();
$sql->db_Select("languages","name,value","code = '$lang'");
for ($i=0;$sql->db_Rows() > $i;$i++)
{
	$g = $sql->db_Fetch();
	extract($g);
	$ar[] = array(
	'title' => $name,
	'trans' => $t->formtparev($value)
	);

}//END OF FOR
//Simplify the array
 $return = array();
 foreach ($ar as $key => $value) 
 {
$return[$value[title]] = $value[trans];

 }//END OF FOREACH
return $return;
}//END OF FUNCTION

function check_lang($lang) 
{ 
	global $sql;
	$sql->db_Select("countries","code","code = '$lang'");
	if ($sql->db_Rows() > 0) 
	 {  
	 	return $lang; 
	 }//END OF IF
	 else 
	  {  
	  	return DEFAULT_LANG; 
	  }//END OF ELSE 
}//END OF FUNCTION check_lang

function set_lang($lang) 
{ 
	$_SESSION['front_language'] = $lang;
//	setcookie('front_language', $lang, time()+3600*24*30, '/', '', 0); 
}//END OF FUNCTION set_lang

function get_countries($active,$trans="no") 
{ 
global $sql;
if ($active == 0) 
{  
 $sql->db_Select("countries","*");
}//END OF IF
elseif ($active != 0) 
{  
if ($active == 'Y') 
{
	$active = "= 'Y'";
}
elseif ($active == 'N')
{
	$active = "= 'N'";
}
elseif ($active == 2)
{
	$active = "IN ('Y','N')";
}
$sql->db_Select("countries","*","active $active"); 
}//END OF ELSE
if ($trans == "yes") 
{ 
  $sql->db_Select("countries","*","active $active AND code !='".DEFAULT_LANG."'"); 
  
}//END OF ELSEIF

	for ($i=0;$sql->db_Rows() > $i;$i++)
  	{
  	$r = $sql->db_Fetch();
  	extract($r);
  	foreach ($r as $key => $value) 
  	{
  	   	if (!preg_match("[0-9]",$key)) 
  	{
  		$ar[$i][$key] = $value;
  	}//END OF IF
  	}//END OF FOREACH 
  	}//END OF FOR  
  return $ar;
}//END OF FUNCTION get_extra_fields

function get_lang_name($code) 
{ 
  global $sql;
  $sql->db_Select("countries","*","code = '$code'");
  	for ($i=0;$sql->db_Rows() > $i;$i++)
  	{
  	$r = $sql->db_Fetch();
  	extract($r);
  	foreach ($r as $key => $value) 
  	{
  	   	if (!preg_match("[0-9]",$key)) 
  	{
  		$ar[$key] = $value;
  	}//END OF IF
  	}//END OF FOREACH 
  	}//END OF FOR
  	return $ar;
}//END OF FUNCTION get_language

function get_country_codes($active="Y") 
{ 
	$where = ($active == "Y") ? "active = 'Y'" : "active IN ('Y','N')";
	global $sql;
	$sql->db_Select("countries","*",$where);
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
		$r = $sql->db_Fetch();
		foreach ($r as $key => $value) 
		{
		   	if (!preg_match("[0-9]",$key)) 
		{
			$ar[$i][$key] = $value;
		}//END OF IF
		}//END OF FOREACH 
		}//END OF FOR
		return $ar;
}//END OF FUNCTION get_languages

function get_country($code) 
{ 
	 global $sql;
	 $sql->db_Select("countries","*","code = '$code'");
	 return execute_single($sql,1);
}//END OF FUNCTION get_country

function get_general_settings() 
{ 
	global $sql;
	$sql->db_Select("prefs","*");
	 $r = $sql->db_Fetch();
	 foreach ($r as $key => $value) 
	 {
	   	if (!preg_match("[0-9]",$key)) 
	{
		$ar[$key] = $value;
	}
	 }
	 return $ar;
}//END OF FUNCTION get_general_settings

function users_online() 
{ 
	global $sql;
	
$sql->db_Select("users_online","usertype, is_registered","is_registered = 'Y' AND usertype = 'A'");
$users_online['admins'] = $sql->db_Rows();

$sql->db_Select("users_online","usertype, is_registered","is_registered = 'A' AND usertype = 'F'");
$users_online['visitors'] = $sql->db_Rows();
return $users_online;
}//END OF FUNCTION users_online

function check_required_fields($required_fields,$arr)
{
	global $lang;
 for ($i=0;count($arr) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($arr[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR
 return $error_list;
}//END OF FUNCTION

function encrypt_uid($uid)
{
$CRYPT_SALT = 185; # any number ranging 1-255
$START_CHAR_CODE = 54; # 'd' letter
if ($m = 1) 
{
	return text_crypt($uid);
}
else 
{
	return text_decrypt($uid);
}
}

function form_settings_string($ar,$seperator1='###',$seperator2=':::')
{
$i=0;
	foreach ($ar as $k => $v) 
	{
		$a .= $k.$seperator2.$v;
		if ($i < count($ar)-1) {
			$a .= $seperator1;
		}
		$i++;
	}	
	return $a;
}

function form_settings_array($string,$seperator1='###',$seperator2=':::')
{

	$tmp = explode($seperator1,$string);
	$b = array();

	for ($i=0;count($tmp) > $i;$i++)
	{
		$t = explode($seperator2,$tmp[$i]);
		$b[$t[0]] = $t[1];

	}

	return $b;
}

function form_contest_settings($ar,$seperator1='###',$seperator2=':::')
{
$i=0;
	foreach ($ar as $k => $v) 
	{
		$a .= $k.$seperator2.$v;
		if ($i < count($ar)-1) {
			$a .= $seperator1;
		}
		$i++;
	}	
	return $a;
}

function save_unformated_settings($settings_array,$seperator1,$separator2,$filter)
{
	$settings_string = form_contest_settings($settings_array,"###",":::");

	$tmp = contest_settings($settings_string,"###",":::");
	foreach ($tmp as $k => $v) 
	{
		if (preg_match($filter,$k)) 
		{
			$tmp2[str_replace($filter,'',$k)] = $v;
		}
	}
	return  form_settings_string($tmp2,"###",":::");
}

function valid_email($email)
{
	if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {
  return 1;
}
else {
  return 0;
}
}

function check_image_resolutions($optionid,$photo_resolution)
{
	global $sql;
	$sql->db_Select("image_resolutions","*","option_id = $optionid");
	$res = execute_single($sql);
	if ($res['image_x'] <= $photo_resolution['image_x'] OR $res['image_y'] <= $photo_resolution['image_y']) 
	{
		$a['res_x'] = $res['image_x'];
		$a['res_y'] = $res['image_y'];
		$a['alowed'] = 1;
		return $a;
	}
	else 
	{
		$a['res_x'] = $res['image_x'];
		$a['res_y'] = $res['image_y'];
		$a['alowed'] = 2;
		return $a;
	}
}

function construct_tags_string($tags_array,$seperator)
{
	$i=0;
	$count = count($tags_array);
	foreach ($tags_array as $key => $value) 
	{
		$tags_string .= $value;
		if ($count > $i+1) 
		{
			$tags_string .= " $seperator ";
		}
		$i = $i+1;
	}
		
	return $tags_string;
}

function construct_tags_array($tags_string,$seperator)
{
	$tmp = explode($seperator,$tags_string);
	if (is_array($tmp)) 
	{
	for ($i=0;count($tmp) > $i;$i++)
	{
		$tmp[$i] = trim($tmp[$i]);
	}
	return $tmp;	
	}
	else {
		return false;
	}

}

function create_user_album_folders($uid,$user_folder,$albumid)
{
				$directory = $_SERVER['DOCUMENT_ROOT'].$user_folder."/album_$albumid";
				$directory_thumbs = $directory."/thumbs";
				$directory_main = $directory."/main";
				//Create the user personal folder
				if (!mkdir($directory,0777)) 
				{
					$error = true;
				}
					else 
				{
					mkdir($directory_thumbs,0755);
					mkdir($directory_main,0755);
				}
}

function get_users($id="no",$admin=1,$extra=0) //gets users details
{ 
	$p = new convert();
	$a = ($admin == 1) ? "admin = 1" : "admin = 0";
	$q = ($id != "no") ? "id = $id" : " $a ORDER BY user_join DESC";
	global $sql;

	$sql->db_Select("users","*",$q);
	if ($id == "no") 
	{  
	$a = execute_multi($sql,1);
	for ($i=0;count($a) > $i;$i++)
	{
		$a[$i]['pass'] = text_decrypt($a[$i]['pass']);
		if ($extra != 0) 
		{
			//First get user images
			$sql->db_Select("user_images","filesize","uid = ".$a[$i]['id']);
			$images = execute_multi($sql);
			$sum = array();
			for ($j=0;$sql->db_Rows() > $j;$j++)
			{
				$sum[] = $images[$j]['filesize'];
			}
			$a[$i]['total_filesize'] = convert_filesize(array_sum($sum));
			$a[$i]['total_images'] = $sql->db_Rows();
			unset($sum);
		}
	}//END OF FOR
	}//END OF IF
	else 
	{  
	$a = execute_single($sql,1);
	if (!empty($a)){
		$a['pass'] = text_decrypt($a['pass']);
	}
		if ($extra != 0) 
		{
			//First get user images
			$sql->db_Select("user_images","filesize","uid = ".$a['id']);
			$images = execute_multi($sql);
			for ($j=0;count($images) > $j;$j++)
			{
				$sum[] = $images[$j]['filesize'];
			}
			$a['total_filesize'] = convert_filesize(array_sum($sum));
			$a['total_images'] =count($images);
		}
	}//END OF ELSE
	return $a; 
}//END OF FUNCTION get_users

function simplify_efields($ar)
{

	for ($i=0;count($ar) > $i;$i++)
	{
		if (is_array($ar[$i])) 
		{
		foreach ($ar[$i] as $k => $v) 
		{
			$tmp[$ar[$i]['var_name']][$k] = $v;
		}
		}	
	
	}
return $tmp;
	
}

function get_image_types($module_id,$settings=0)
{
	global $sql;
	if ($settings['required']) {
		$q[] = "required = ".$settings['required'];
	}
	if ($settings['ids']) {
		$q[] = "id IN (".$settings['ids'].")";
	}
	if ($settings['title']) {
		$q[] = "title = '".$settings['title']."'";
	}
	if (is_array($q))
	{
		$query = "AND ".implode(" AND ",$q);
	}
	if ($settings['debug']) {
		echo "module_id = $module_id $query";
	}
	
	$sql->db_Select("image_types","*","module_id = $module_id $query");
	$res = execute_multi($sql);
	$tmp = array();
	if ($res) {
	foreach ($res as  $v) {
		$tmp[$v['title']] = $v;
		$tmp[$v['title']]['settings'] = form_settings_array($v['settings']);
		}
	}
		return $tmp;
}

function quick_contact($data,$required_fields=0,$options=0)
{
 $contact = array();
 $t = new textparse();
 foreach ($data as $key => $value) 
 {   
	 $contact[$key] = $value;
	 $mailmessage .= $key." : ".$t->formtpa($value)."<br>";
	 
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($contact) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($contact[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {  
 	$mailfrom = $t->formtpa($data['email']);
 	$from = $t->formtpa($data['name']);
    $mail = mailer($from,$mailfrom,$options['email_to'],$options['email_subject'],$mailmessage);
 	if ($mail == 1) 
 	{  
 		return 1;

 	}//END OF IF
 	elseif ($mail ==0) 
 	{  
		return 0;
 	}//END OF ELSE
 }//END OF IF NO ERRORS

}//END FUNCTION

function write_htaccess($string,$oldstring='')
{
	
	if ($oldstring) 
	{
		$file = file_get_contents($_SERVER['DOCUMENT_ROOT']."/.htaccess");

		if (strstr($file,$oldstring)) 
		{
			$f = fopen($_SERVER['DOCUMENT_ROOT']."/.htaccess","w");
			$contents = str_replace($oldstring,$string,$file);
			fwrite($f,$contents);
			fclose($f);
		}
		else 
		{
			$f = fopen($_SERVER['DOCUMENT_ROOT']."/.htaccess","a");
			fputs($f,"\n");
			fwrite($f,$string);
			fclose($f);
		}
	}
	else 
	{
	$f = fopen($_SERVER['DOCUMENT_ROOT']."/.htaccess","a");
	fputs($f,"\n");
	fputs($f,$string);
	fclose($f);
	}
}

function get_themes()
{
	global $sql;
	$sql->db_Select("themes","*");
	return execute_multi($sql,1);
}


function get_providers($lang,$uid=0,$type=0,$extra=0,$active=0)
{
	global $sql;
	
	if ($uid) 
	{
		$sql->db_Select("users","id","id = $uid AND user_class = 'P'");
		if ($sql->db_Rows()) 
		{
			$provider = execute_single($sql);
			$provider['user'] = get_users($provider['id'],0);
			if ($extra AND $type) 
			{
				$provider['items'] = get_provider_items($type,$uid);
			}
			return $provider;
		}
	}//END UID
	else 
	{
		$sql->db_Select("users","id","user_class = 'P'");
		if ($sql->db_Rows()) 
		{
			$providers = execute_multi($sql);
			for ($i=0;count($providers) > $i;$i++)
			{
			$providers[$i]['user'] = get_users($providers[$i]['id'],0);
				if ($extra AND $type) 
				{
					$providers[$i]['items'] = get_provider_items($type,$providers[$i]['id']);
				}
			}
			return $providers;
		}
	}//END ALL PROVIDERS
}//END FUNCTION


function get_provider_items($type,$lang,$active,$uid=0,$id=0,$itemid=0)
{
	global $sql;
	if ($uid) 
	{
		$sql->db_Select("provider_items","*","uid = $uid AND type = '$type'");
		if ($sql->db_Rows()) 
		{
			$items = execute_multi($sql);
			for ($i=0;count($items) > $i;$i++)
			{
				if ($type == 'gallery') 
				{
					$items[$i]['items'] = get_gallery($items[$i]['itemid'],$lang,$active,1);
				}
				elseif ($type == 'content')
				{
					$items[$i]['items'] = get_content($items[$i]['itemid'],$lang,$active,1);
				}
				elseif ($type == 'products')
				{
					$items[$i]['items'] = get_products($items[$i]['itemid'],$lang,$active,1);
				}
				elseif ($type == 'news')
				{
					$items[$i]['items'] = get_news($items[$i]['itemid'],$lang,$active,1);
				}
			}//END FOR
			return $items;
		}//END FOUND
		else 
		{
			return 0;
		}
	}//END UID
	if ($itemid) 
	{
		$sql->db_Select("provider_items","*","itemid = $itemid AND type = '$type'");
		if ($sql->db_Rows() > 0)
		{
			$provider = execute_single($sql);
			$provider['user'] = get_users($provider['uid'],0);
			return $provider;
		}
	}//END OF ITEMID
}

function get_provider_id($itemid,$type,$options=0)
{
	global $sql;

	$sql->db_Select("provider_items","*","type = '$type' AND itemid = $itemid");
	if ($sql->db_Rows()) 
	{
		$provider = execute_single($sql);
		if ($options['get_user']) 
		{
			$provider['user_data'] = get_users($provider['uid'],0);
		}
		if ($options['get_items']) 
		{
			$table = $options['table'];
			$provider['items'] = get_provider_items($options['table'],$options['lang'],$options['active'],$provider['uid'],0,0);
		}
		
		return $provider;
	}
}

function create_timestamp_from_date($day)
{
$date = explode("/",$day);
$now = time();
$hour = strftime("%H");
$minutes = strftime("%M");
$seconds = strftime("%S");
return  mktime($hour,$minutes,$seconds,$date[1],$date[0],$date[2]);
}

function get_all_countries() 
{ 
	global $sql;
	$sql->db_Select("country_codes","*","ORDER BY country",$mode="no_where"); 
	return execute_multi($sql,1);
}//END OF FUNCTION get_country_codes

function country_details($code) 
{ 
	global $sql;
	$sql->db_Select("country_codes","country,charset","code = '$code'");
	$r = $sql->db_Fetch(); 
	return $r;
}//END OF FUNCTION country_details

function get_newsletters($id="no")
{
	global $sql;
	$c = new convert();
	if ($id == "no") 
	{
		$sql->db_Select("newsletters","*");
		$newsletters = execute_multi($sql,1);
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
			$newsletters[$i]['date_added'] = $c->convert_date($newsletters[$i]['date_added']);
		}
	}
	else 
	{
		$sql->db_Select("newsletters","*","id=$id");
		$newsletters = execute_single($sql,1);
		$newsletters['date_added'] = $c->convert_date($newsletters['date_added']);
	}

	return $newsletters;
}

function get_maillist($gid,$active="no")
{
	global $sql;
	$t = new convert();
	$active = ("no") ? "0,1" : "1";
	if ($gid == "p")//pending subscribers 
	{
		$list = pending_subscribers();
		
	}
	else //everything else
	{
	$sql->db_Select("maillist","*","groupid IN ($gid) AND active IN ($active) ORDER BY date_added DESC");
	$list = execute_multi($sql,1);
	for ($i=0;$sql->db_Rows() > $i;$i++)
	{
		$list[$i]['loop'] = $i;
		$list[$i]['date_added'] = $t->convert_date($list[$i]['date_added']);
	}
	}//END OF EVERYTHING ELSE

	return $list;
}

function get_subscriber ($id)
{
	global $sql;
	$sql->db_Select("maillist","*","id=$id");
	return execute_single($sql,1);
}

function get_maillist_groups()
{
	global $sql;
	$sql1 = new db();
	$t = new convert();
	$sql->db_Select("maillist_groups","*");
	
	$groups = execute_multi($sql,1);
	for ($i=0;$sql->db_Rows() > $i;$i++)
	{
		$gid = $groups[$i]['id'];
		$sql1->db_Select("maillist","id","groupid = $gid");
		$groups[$i]['members'] = $sql1->db_Rows();
		$groups[$i]['date_added'] = $t->convert_date($groups[$i]['date_added']);		
	}

	return $groups;
}

function pending_subscribers()
{
	global $sql;
	$t = new convert();
	$sql->db_Select("maillist_pending","*");
	$list = execute_multi($sql,1);
	for ($i=0;$sql->db_Rows() > $i;$i++)
	{
		$list[$i]['loop'] = $i;
		$list[$i]['date_added'] = $t->convert_date($list[$i]['date_added']);
	}
	return $list;
}




function verification_mail($email,$name,$verification_url)
{
$mailer = new FreakMailer();
$t = new textparse();
//load the verification template
$template = process_newsletter_template($t->formtparev(VERIFICATION_EMAIL),$email,$name);
$template = str_replace("#VERIFY_URL#",$verification_url,$template);
// Set the subject
$mailer->Subject = process_newsletter_template(VERIFICATION_TITLE);
// Body
$mailer->Body = $template;
// Add an address to send to.
$mailer->AddAddress($email, $name);
$mailer->isHTML(true); 
if(!$mailer->Send())
{
  $ret = 0;
}
else
{
  $ret = 1;
}
$mailer->ClearAddresses();
$mailer->ClearAttachments(); 
$mailer->IsHTML(false); 
return $ret;
}


function process_text($body,$extra_find="",$extra_rep="")
{
	$t = new textparse();
	$find[] = "#SITE#";$rep[] = SITE_NAME;
	$find[] = "#URL#";$rep[] = URL;
	$find[] = "#COMPANY_NAME#";$rep[] = COMPANY_NAME;
	$find[] = "#COMPANY_EMAIL#";$rep[] = COMPANY_EMAIL;

if ($extra_find) 
{
	for ($i=0;count($extra_find) > $i;$i++)
	{
		$find[] = $extra_find[$i];$rep[] = $t->formtparev($extra_rep[$i]);
	}
}
	$body = str_replace($find,$rep,$body);
	return $body;
}

function process_newsletter_template($body,$extra_find="",$extra_rep="")
{
	$t = new textparse();
	$find[] = "#SITE#";$rep[] = SITE_NAME;
	$find[] = "#URL#";$rep[] = URL;

if ($extra_find) 
{
	for ($i=0;count($extra_find) > $i;$i++)
	{
		$find[] = $extra_find[$i];$rep[] = $t->formtparev($extra_rep[$i]);
	}
}
	$body = str_replace($find,$rep,$body);
	return $body;
}

function cron_tasks($settings)
{
	global $sql;
	if ($settings['itemid']) {//Get for a specific item
		$sql->db_Select("cron_tasks","*","itemid = ".$settings['itemid']);
		if ($sql->db_Rows()) {
			$item = execute_single($sql);
			return $item;
		}
	}
	
}

function auto_publish($settings)
{
	global $sql;
	
	if ($settings['action'] == 'activate')
	{
		$sql->db_Select('cron_tasks',"id,itemid","action = 'activate' AND module = '".$settings['module']."' AND date < ".time());
		if ($settings['debug']) { echo "SELECT id FROM cron_tasks WHERE module = '".$settings['module']."' AND date < ".time(); }
		if ($sql->db_Rows())//ACTIVATE THE ITEMS FOUND
		{
			$res = execute_multi($sql);
			for ($i=0; count($res) > $i;$i++)
			{
				$sql->db_Update($settings['module'],'active = 1 WHERE id = '.$res[$i]['itemid']);//ACTIVATE
				$sql->db_Delete('cron_tasks',"id = ".$res[$i]['id']);//DELETE CRON ENTRY
			}
			
		}
	}
}

function build_parent()
{
	global $exclude,$sql,$depth;
$file = $_SERVER['DOCUMENT_ROOT']."/.htaccess";
$start_line = binary_search_in_file($file,'#PRODUCTS START#') +1;
$end_line = binary_search_in_file($file,'#PRODUCTS END#');
for ($i=$start_line;$end_line > $i;$i++)
{
	$arr[] = $i;
}

cutline($file,$arr);

$nav_query = MYSQL_QUERY("SELECT categoryid,parentid,settings FROM `categories` ORDER BY `categoryid`");
$tree = "";                         // Clear the directory tree
$depth = 1;                         // Child level depth.
$top_level_on = 1;               // What top-level category are we on?
$exclude = ARRAY();               // Define the exclusion array
ARRAY_PUSH($exclude, 0);     // Put a starting value in it
 
WHILE ( $nav_row = MYSQL_FETCH_ARRAY($nav_query) )
{
	$settings = form_settings_array($nav_row['settings'],"###",":::");
     $goOn = 1;               // Resets variable to allow us to continue building out the tree.
     FOR($x = 0; $x < COUNT($exclude); $x++ )          // Check to see if the new item has been used
     {
          IF ( $exclude[$x] == $nav_row['categoryid'] )
          {
               $goOn = 0;
               BREAK;                    // Stop looking b/c we already found that it's in the exclusion list and we can't continue to process this node
          }
     }
     IF ( $goOn == 1 )
     {
$tree .= 'RewriteRule ^'.$settings['cat_alias'].'.html /product_category.php?cat='.$nav_row['categoryid'].'&theme='.$settings['theme'].'&area='.$settings['cat_alias']."\n";                   // Process the main tree node
$tree .= 'RewriteRule ^'.$settings['cat_alias'].'_([0-9]+).html /product_category.php?page=$1&cat='.$nav_row['categoryid'].'&theme='.$settings['theme'].'&area='.$settings['cat_alias']."\n";
if ($manufacturers_module = module_is_active("manufacturers",1,1)) 
{
$tree .= 'RewriteRule ^'.$settings['cat_alias'].'_([0-9a-zA-Z].*).html /product_category.php?manufacturerid=$1&cat='.$nav_row['categoryid'].'&theme='.$settings['theme'].'&area='.$settings['cat_alias']."\n";
$tree .= 'RewriteRule ^'.$settings['cat_alias'].'_([0-9a-zA-Z].*)_([0-9]+).html /product_category.php?manufacturer_id=$1&page=$2&cat='.$nav_row['categoryid'].'&theme='.$settings['theme'].'&area='.$settings['cat_alias']."\n";
}                   // Process the main tree node
          ARRAY_PUSH($exclude, $nav_row['categoryid']);          // Add to the exclusion list
          IF ( $nav_row['category_id'] < 18 )
          { $top_level_on = $nav_row['categoryid']; }
 
          $tree .= build_child($nav_row['categoryid'],$exclude);          // Start the recursive function of building the child tree
     }
}

append_text($file,array('start'=>$start_line,'tree'=>$tree));
return $tree;
}


 
FUNCTION build_child($oldID,$exclude)               // Recursive function to get all of the children...unlimited depth
{
     GLOBAL $exclude, $depth,$sql;               // Refer to the global array defined at the top of this script
     $child_query = MYSQL_QUERY("SELECT * FROM `categories` WHERE parentid=" . $oldID);
     WHILE ( $child = MYSQL_FETCH_ARRAY($child_query) )
     {
     	$settings = form_settings_array($child['settings'],"###",":::");
          IF ( $child['categoryid'] != $child['parentid'] )
          {
               $tempTree .= 'RewriteRule ^'.$settings['cat_alias'].'.html /product_category.php?cat='.$child['categoryid'].'&theme='.$settings['theme'].'&area='.$settings['cat_alias']."\n";
               $tempTree .= 'RewriteRule ^'.$settings['cat_alias'].'_([0-9]+).html /product_category.php?page=$1&cat='.$child['categoryid'].'&theme='.$settings['theme'].'&area='.$settings['cat_alias']."\n"; 
if ($manufacturers_module = module_is_active("manufacturers",1,1)) 
{
$tempTree .= 'RewriteRule ^'.$settings['cat_alias'].'_([0-9a-zA-Z].*).html /product_category.php?manufacturerid=$1&cat='.$child['categoryid'].'&theme='.$settings['theme'].'&area='.$settings['cat_alias']."\n";
$tempTree .= 'RewriteRule ^'.$settings['cat_alias'].'_([0-9a-zA-Z].*)_([0-9]+).html /product_category.php?manufacturer_id=$1&page=$2&cat='.$child['categoryid'].'&theme='.$settings['theme'].'&area='.$settings['cat_alias']."\n";
}
               $depth++;          // Incriment depth b/c we're building this child's child tree  (complicated yet???)
               $tempTree .= build_child($child['categoryid'],$exclude);          // Add to the temporary local tree
               $depth--;          // Decrement depth b/c we're done building the child's child tree.
               if (is_array($exclude)) {
               	ARRAY_PUSH($exclude, $child['categoryid'],$exclude);               // Add the item to the exclusion list
               }
          }
     }
 
     RETURN $tempTree;          // Return the entire child tree
}
 



function append_text($filename,$settings)
{
	
$lines = file($filename);
$first_line = $lines[$settings['start']];
if (is_array($lines)) {
	$i=0;
foreach ($lines as $k=>$v)
{
	
	if ($i == $settings['start']) {
		array_insert($lines,$settings['tree'],$i);
	}
	$i++;
}	
}
// Write to file
$file = fopen($filename, 'w');
fwrite($file, implode('', $lines));
fclose($file);
	
}

function cutline($filename,$lines_arr) {

$X = count($lines_arr); // Number of lines to remove

$lines = file($filename);
$first_line = $lines[$lines_arr[0]];
if (is_array($lines_arr)) {
foreach ($lines_arr as $k=>$v)
{
	unset($lines[$v]);
}
}

// Write to file
$file = fopen($filename, 'w');
fwrite($file, implode('', $lines));
fclose($file);

} 

function binary_search_in_file($filename, $search) {
$file = fopen($filename,"r+");
while (!feof($file)) { // Untill the end of the file
   $line = fgets($file);
   if (preg_match("/$search/i",$line)) { 
   break;
   }
   $i++;
   }
return  $i;   

}

function array_insert(&$array,$element,$position=null) {
  if (count($array) == 0) {
    $array[] = $element;
  }
  elseif (is_numeric($position) && $position < 0) {
    if((count($array)+position) < 0) {
      $array = array_insert($array,$element,0);
    }
    else {
      $array[count($array)+$position] = $element;
    }
  }
  elseif (is_numeric($position) && isset($array[$position])) {
    $part1 = array_slice($array,0,$position,true);
    $part2 = array_slice($array,$position,null,true);
    $array = array_merge($part1,array($position=>$element),$part2);
    foreach($array as $key=>$item) {
      if (is_null($item)) {
        unset($array[$key]);
      }
    }
  }
  elseif (is_null($position)) {
    $array[] = $element;
  }
  elseif (!isset($array[$position])) {
    $array[$position] = $element;
  }
  $array = array_merge($array);
  return $array;
}

function create_items_folders($settings)
{
	global $sql;
		mkdir($settings['root'],0755);
		$sql->db_Select("image_types","dir","module_id = ".$settings['module']);
		if ($sql->db_Rows()) {
			$f = execute_multi($sql);
			foreach ($f as $v)
			{
				if (!is_dir($settings['root']."/".$v['dir'])) 
				{
					mkdir($settings['root']."/".$v['dir'],0755);
				}
			}
		}
}

class Latin1UTF8 {
   
    private $latin1_to_utf8;
    private $utf8_to_latin1;
    public function __construct() {
        for($i=32; $i<=255; $i++) {
            $this->latin1_to_utf8[chr($i)] = utf8_encode(chr($i));
            $this->utf8_to_latin1[utf8_encode(chr($i))] = chr($i);
        }
    }
   
    public function mixed_to_latin1($text) {
        foreach( $this->utf8_to_latin1 as $key => $val ) {
            $text = str_replace($key, $val, $text);
        }
        return $text;
    }

    public function mixed_to_utf8($text) {
        return utf8_encode($this->mixed_to_latin1($text));
    }
} 


function toAscii($str, $replace=array(), $delimiter='-') {
	$trans = new Latin1UTF8();
	$str = $trans->mixed_to_utf8($str);

	if( !empty($replace) ) {
		$str = str_replace((array)$replace, ' ', $str);
	}

	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	return $clean;
}

class tags {
var $settings;
var $module;
var $orderby;
var $way;
var $limit;
var $itemid;

	function tags($settings)//INITIALIZE
	{
		$this->settings = $settings;	
		$this->itemid = $settings['itemid'];	
		$this->module = $settings['module'];	
		$this->orderby = ($this->settings['orderby']) ? $this->settings['orderby'] : 'tag';
		$this->way = ($this->settings['way']) ? $this->settings['way'] : 'ASC';
		$this->limit = ($this->settings['limit']) ? "LIMIT ".$this->settings['limit'] : "";	
		
	}
	
	
	
	function GetTagItemsByTagId($tagID)
	{
		global $sql;

		$sql->db_Select('tags INNER JOIN tags_items ON (tags.id=tags_items.id)','tag,tags.id,permalink,itemid',"tags.id = $tagID AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink,itemid FROM tags INNER JOIN tags_items ON (tags.id=tags_items.id) WHERE tags.id = $tagID AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			return execute_multi($sql);
		}
	}//END FUNCTION
	
		function getAllTags($settings=0) {
		global $sql;
		
		$fields = ($settings['fields']) ? $settings['fields'] : 'tag,tags.id,permalink';
		if ($settings['limit']) {
			$limit = " LIMIT ".$settings['limit'];
		}
		$orderBy = ($settings['orderby']) ? $settings['orderby'] : 'tag';
		$way = ($settings['way']) ? $settings['way'] : 'ASC';
		$sql->db_Select('tags',$fields,"module = '".$this->module['name']."'  GROUP BY tag ORDER BY $orderBy $way $limit");
		if ($settings['debug']) {
			echo "SELECT $fields FROM tags WHERE  module = '".$this->module['name']."'  GROUP BY tag ORDER BY $orderBy $way $limit <Br>";
		}
		if ($sql->db_Rows()) {
			return execute_multi($sql);
			
		}
		
	}//END FUNCTION
	
	function GetTagItemsByTag($tag)
	{
		global $sql;

		$sql->db_Select('tags INNER JOIN tags_items ON (tags.id=tags_items.id)','tag,tags.id,permalink,itemid',"tags.tag = '$tag' AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink,itemid FROM tags INNER JOIN tags_items ON (tags.id=tags_items.id) WHERE tags.tag = '$tag' AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			return execute_multi($sql);
		}
	}//END FUNCTION
	
	function GetTagItemsByItemid($itemid)
	{
		global $sql;

		$sql->db_Select('tags INNER JOIN tags_items ON (tags.id=tags_items.id)','tag,tags.id,permalink,itemid',"itemid = '$itemid' AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink,itemid FROM tags INNER JOIN tags_items ON (tags.id=tags_items.id) WHERE itemid = '$itemid' AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			$res = execute_multi($sql);
			return $res;
		}
	}//END FUNCTION

	function GetTags()
	{
		global $sql;

		if ($this->settings['count_items']) {
			$count = ",(SELECT count(itemid) FROM tags_items WHERE id = tags.id AND module = '".$this->module['name']."') as item_count";
		}
		$sql->db_Select('tags INNER JOIN tags_items ON (tags.id=tags_items.id)',"tag,tags.id,permalink,itemid $count ","module = '".$this->module['name']."' GROUP BY tag ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink,itemid $count FROM tags INNER JOIN tags_items ON (tags.id=tags_items.id) WHERE  module = '".$this->module['name']."' GROUP BY tag ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			if ($this->settings['TagCloud']) {
				$res = execute_multi($sql);
				return $this->CreateTagCloud($res);
			}
			else {
				return execute_multi($sql);
			}
			
		}
	}//END FUNCTION

	function GetMultipleTags($tags)
	{
		global $sql;
		if ($this->settings['SearchField'] == 'id') {
			$searchFor = implode(",",$tags);
		}
		elseif ($this->settings['SearchField'] == 'tag')
		{
			foreach ($tags as $v)
			{
				$tmp[] = "'$v'";//ADD QUOTED FOR TEXT SEARCH
			}
			$searchFor = implode(",",$tmp);
		}
		
		if ($this->settings['count_items']) {
			$count = ",(SELECT count(itemid) FROM tags_items WHERE id = tags.id AND module = '".$this->module['name']."') as item_count";
		}
        if (!$this->settings['simple']) {
        $join = "INNER JOIN tags_items ON (tags.id=tags_items.id)";
        $fields = ',itemid';
        }//END IF
		$sql->db_Select("tags $join","tag,tags.id,permalink $fields $count ","tags.".$this->settings['SearchField']." IN ($searchFor) AND module = '".$this->module['name']."' GROUP BY tag ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink $fields $count FROM tags $join WHERE  ".$this->settings['SearchField']." IN ($searchFor) AND module = '".$this->module['name']."' GROUP BY tag ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			if ($this->settings['TagCloud']) {
				$res = execute_multi($sql);
				return $this->CreateTagCloud($res);
			}
			else {
				return execute_multi($sql);
			}
			
		}
	}//END FUNCTION

	function CreateTagCloud($res)
	{
		if ($res) 
		{
	
			
			for ($i=0;count($res) > $i;$i++)
			{
				$tags[$res[$i]['tag']] = $res[$i]['item_count'];
			}
		//	echo "SELECT tag , COUNT(id) AS quantity FROM recipes_tags "
			$max_size = ($this->settings['max_size']) ? $this->settings['max_size'] : 250; // max font size in %
			$min_size = ($this->settings['min_size']) ? $this->settings['min_size'] : 100; // min font size in %
			
			$max_qty = max(array_values($tags));
			$min_qty = min(array_values($tags));
				// find the range of values
			$spread = $max_qty - $min_qty;
			if (0 == $spread) 
			{ // we don't want to divide by zero
		    	$spread = 1;
			}
		
			// determine the font-size increment
			// this is the increase per tag quantity (times used)
			$step = ($max_size - $min_size)/($spread);
			
			for ($i=0;count($res) > $i;$i++)
			{
				$size = $min_size + (($res[$i]['item_count'] - $min_qty) * $step);
				$res[$i]['size'] = $size;
			}
			}//END OF ROWS
		return $res;
	}//END FUNCTION
	
	function SaveTags($tags)
	{
		global $sql;
		//UPDATE TAGS
		if ($tags) 
		{
			if (!is_array($tags)) {
				$tags_array = $this->ConstructTagsArray($tags);
			}
			else { $tags_array = $tags; }

			//FIRST CHECK WHICH ONES EXIST
			$this->settings['SearchField'] = 'tag';
            $this->settings['simple'] = 1;//GET ALL TAGS WITHOUT JOINING THE TABLE ITEMS
            $this->settings['debug'] =0;
			$existing_tags = $this->GetMultipleTags($tags_array);

			if (is_array($tags_array)) {
				$tags_array = array_flip($tags_array);
				$tmp = $tags_array;
				if ($existing_tags) {
				foreach ($existing_tags as $v)
				{
					if (array_key_exists($v['tag'],$tags_array)) {
						$tmp[$v['tag']] = array();
						$tmp[$v['tag']]['order'] = $tags_array[$v['tag']];
						$tmp[$v['tag']]['id'] = $v['id'];
					}//END IF
				}//END FOREACH
				}//END EXISTING TAGS
				//NOW CHECK OUT FOR NEW ITEMS. REMAINING ITEMS FROM TAGS ARRAY

				if ($tmp) {
				    $i = 0;
                    $sql->db_Delete('tags_items',"id IN ((SELECT tags.id FROM tags WHERE module = '".$this->module['name']."' AND tags.id = tags_items.id)) AND
                    itemid = ".$this->itemid);
					foreach ($tmp as $k=>$v)
					{
						if (!is_array($v)) {
						  $v = array();
							$permalink = greeklish($k);
							$sql->db_Insert("tags (tag,module,permalink)","'$k','".$this->module['name']."','$permalink'");//ADD TAG
//							echo "INSERT INTO tags VALUES '','$k','".$this->module['name'],"','$permalink'";
							$n_id = $sql->last_insert_id;
                            $v['id'] = $n_id;
						}

						  //DROP OLD ONES
                         // echo "INSERT INTO tags_items VALUES ('".$v['id']. "','".$this->itemid."','$i')";
                          $sql->db_Insert('tags_items',"'".$v['id']. "','".$this->itemid."','$i'");

					$i++;
					}
				}//END IF
			}
		}//END IF
		
	}//END FUNCTION

	function ConstructTagsString($tags,$seperator=",")
	{
		if (is_array($tags)) {
		foreach ($tags as $v)
		{
			$arr[] = $v['tag'];
		}
		
		return implode($seperator,$arr);
		}

	}//END FUNCTION
	
	function ConstructTagsArray($tags_string,$seperator=",")
	{
			$tmp = explode($seperator,$tags_string);

        
		$tmp = array_unique($tmp);//REMOVE DUPLICATES
		if (is_array($tmp)) 
		{

        ksort($tmp);

		return $tmp;	
		}
		else {
			return false;
		}
	
	}//END FUNCTION
	

}//END CLASS

function MediaFiles($settings=0)
{
	global $sql;
	if ($settings['single']) 
	{
	   if ($settings['id']) {
            $q[] = "id = ".$settings['id'];
       }//END IF   
       if ($settings['title']) {
            $q[] = "title = '".$settings['title']."'";
       }//END IF
        if ($settings['type']) {
            $q[] = "type = '".$settings['type']."'";
       }//END IF
       if (is_array($q)) {
        $query = implode(" AND ",$q);
        $mode = "default";
       }//END IF
       else{ $mode = "no_where";}
		$sql->db_Select("media_files","*",$query,$mode);
		if ($sql->db_Rows()) 
		{
		  $res = execute_single($sql);
          $res['settings'] = form_settings_array($res['settings']);
		}
        //echo "SELECT * FROM media_files WHERE $query";
        return $res;
	}
	else {
	$sql->db_Select("media_files","*");
	if ($sql->db_Rows()) 
	{
		$res = execute_multi($sql);
        for ($i=0;count($res) > $i;$i++) {
            $res[$i]['settings'] = form_settings_array($res[$i]['settings']);
        }//END FOR
	}
    return $res;
	}   
}//END FUNCTION

    function FileSizeFormat($size)
    {
      $dims=array('&nbsp;','K','M','G','T','P');
      $dim=0;
    
      while( ($size/1024) > 0.95)
      {
        $size/=1024;
        $dim++;
      }
      return round($size,2).$dims[$dim]."B";
    }//END FUNCTION
    
      function format_titme($sec, $padHours = false) 
  {

    // holds formatted string
    $hms = "";

    // there are 3600 seconds in an hour, so if we
    // divide total seconds by 3600 and throw away
    // the remainder, we've got the number of hours
    $hours = intval(intval($sec) / 3600); 

    // add to $hms, with a leading 0 if asked for
    $hms .= ($padHours) 
          ? str_pad($hours, 2, "0", STR_PAD_LEFT). ':'
          : $hours. 'h:';
     
    // dividing the total seconds by 60 will give us
    // the number of minutes, but we're interested in 
    // minutes past the hour: to get that, we need to 
    // divide by 60 again and keep the remainder
    $minutes = intval(($sec / 60) % 60); 

    // then add to $hms (with a leading 0 if needed)
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). 'm:';

    // seconds are simple - just divide the total
    // seconds by 60 and keep the remainder
    $seconds = intval($sec % 60)."s"; 

    // add to $hms, again with a leading 0 if needed
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

    // done!
    return $hms;
    
  }
    function greeklish($str_i,$settings=0) {
  $str_i = trim($str_i);
  $g_c = array("α","ά","β","γ","δ","ε","έ","ζ","η","ή","θ","ι","ί","κ","λ","μ","ν","ξ","ο",
  "�?","π","�?","σ","ς","τ","υ","�?","φ","χ","�?","ω","�?","Α","Ά","Β","Γ","Δ","Ε","�?","Ζ","Η",
  "Ή","�?","Ι","�?","�?","Λ","�?","�?","�?","�?","�?","Π","Ρ","Σ","Τ","Υ","�?","Φ","Χ","Ψ","Ω","�?","!","@","#","$","%","^","&","*","(",")"," ",".",",",":","--","'");
  
  $e_c = array("a","a","v","g","d","e","e","z","i","i","th","i","i","k","l","m","n","ks","o",
  "o","p","r","s","s","t","u","u","f","h","y","w","w","A","A","V","G","D","E","E","Z","I",
  "I","TH","I","I","K","L","M","N","KS","O","O","P","R","S","T","Y","Y","F","H","Y","W","W","","","","","","","","","","","-","","","","","");
  
  for($i=0;$i<count($g_c);$i++){
      $str_i = str_replace($g_c[$i], $e_c[$i], $str_i);
  }
  
  if ($settings['AllLower']) {
  	return strtolower($str_i);
  }
  else {
  	return $str_i;
  }
}

function formatprice($price, $thousand_delim = ",", $decimal_delim = ".", $precision = "2")
{
	
//	$price = sprintf("%.$precision"."f", $price);
	$price = sprintf("%.02f", doubleval($price));
	$price = str_replace(".", $decimal_delim, $price);
	$pos = strpos($price, $decimal_delim);
	if (!$pos) $pos = strlen($price);
	for ($i = $pos -3; $i > 0; $i -= 3) {
		$price = substr($price, 0, $i).$thousand_delim.substr($price, $i);
	}
	$price = str_replace($decimal_delim."00",'',$price);
	return $price;
}


if (!function_exists('money_format'))
{
function money_format($format, $number) 
{ 
    $regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'. 
              '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/'; 
    if (setlocale(LC_MONETARY, 0) == 'C') { 
        setlocale(LC_MONETARY, ''); 
    } 
    $locale = localeconv(); 
    preg_match_all($regex, $format, $matches, PREG_SET_ORDER); 
    foreach ($matches as $fmatch) { 
        $value = floatval($number); 
        $flags = array( 
            'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ? 
                           $match[1] : ' ', 
            'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0, 
            'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ? 
                           $match[0] : '+', 
            'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0, 
            'isleft'    => preg_match('/\-/', $fmatch[1]) > 0 
        ); 
        $width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0; 
        $left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0; 
        $right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits']; 
        $conversion = $fmatch[5]; 

        $positive = true; 
        if ($value < 0) { 
            $positive = false; 
            $value  *= -1; 
        } 
        $letter = $positive ? 'p' : 'n'; 

        $prefix = $suffix = $cprefix = $csuffix = $signal = ''; 

        $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign']; 
        switch (true) { 
            case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+': 
                $prefix = $signal; 
                break; 
            case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+': 
                $suffix = $signal; 
                break; 
            case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+': 
                $cprefix = $signal; 
                break; 
            case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+': 
                $csuffix = $signal; 
                break; 
            case $flags['usesignal'] == '(': 
            case $locale["{$letter}_sign_posn"] == 0: 
                $prefix = '('; 
                $suffix = ')'; 
                break; 
        } 
        if (!$flags['nosimbol']) { 
            $currency = $cprefix . 
                        ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) . 
                        $csuffix; 
        } else { 
            $currency = ''; 
        } 
        $space  = $locale["{$letter}_sep_by_space"] ? ' ' : ''; 

        $value = number_format($value, $right, $locale['mon_decimal_point'], 
                 $flags['nogroup'] ? '' : $locale['mon_thousands_sep']); 
        $value = @explode($locale['mon_decimal_point'], $value); 

        $n = strlen($prefix) + strlen($currency) + strlen($value[0]); 
        if ($left > 0 && $left > $n) { 
            $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0]; 
        } 
        $value = implode($locale['mon_decimal_point'], $value); 
        if ($locale["{$letter}_cs_precedes"]) { 
            $value = $prefix . $currency . $space . $value . $suffix; 
        } else { 
            $value = $prefix . $value . $space . $currency . $suffix; 
        } 
        if ($width > 0) { 
            $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ? 
                     STR_PAD_RIGHT : STR_PAD_LEFT); 
        } 

        $format = str_replace($fmatch[0], $value, $format); 
    } 
    return $format; 
} 
}

function print_ar($ar) {
	echo "<pre>";
	print_r($ar);
	echo "</pre>";
}

function orderBy($data, $field)
  {
    $code = "return strnatcmp(\$a['$field'], \$b['$field']);";
    usort($data, create_function('$a,$b', $code));
    return $data;
  }

/**
 * A function that check if the string is JSON.
 * In true case, $jsonObj is touched by reference
 * as its value is json decoded.
 * In false case, wrong $jsonObj is unchanged.
 * @param string $jsonStr
 * @param stdClass $jsonObj
 * @param boolean $assoc
 * @param int $depth
 * @return boolean
 */
function isJSON($jsonStr,&$jsonObj = NULL, $assoc = false, $depth = 512)
{
	$jsonStr = ' '.$jsonStr.' '; // Bug Fix
	
	$jsonTmp = @json_decode($jsonStr,$assoc,$depth);
	if($jsonTmp !== NULL)
	{
		@$jsonObj = $jsonTmp;
		return true;
	}
	
	return false;
}
  
?>
