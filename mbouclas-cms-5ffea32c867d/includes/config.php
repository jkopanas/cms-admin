<?php
//Database definitions
define("MPREFIX","");
define("DBHOST","localhost");
define("DBUSER","root");
define("DBPASS","");
define("DB","baloons");
define("SKIN_PATH",$_SERVER['DOCUMENT_ROOT']."/skin");
define("SKIN_URL","/skin");
define("TEMPLATES_PATH",$_SERVER['DOCUMENT_ROOT']."/templates_c");
define("DEFAULT_SORT","sku");
define("USE_SMARTY_CAHCHING",0);

if (class_exists('Memcache',false)) {
//	define('USE_CACHE',1); //ENABLE CACHING
	define('TTL',time()+3600*24*30);
	$memcache = new Memcache;
    $memcache->connect("localhost",11211); # You might need to set "localhost" to "127.0.0.1"
}

#
# Select the sessions mechanism:
# 1 - PHP sessions data is stored on the file system
# 2 - PHP sessions data is stored on the MySQL database
# 3 - internal sessions mechanism is used (highly recommended)
$useSESSIONs_type = 3;
$SESSION_NAME = "xid";//Set the session name here
date_default_timezone_set('Europe/Athens');
/* ACTIVE PLUGINS */
$plugins = array(
	'shortcodes'=> array('name' => 'shortCodes','path'=>'shortcodes/index.php','area' => array("F") ),
	//'facebook'=> array('name' => 'facebook','path'=>'facebook/index.php', 'area' => array("F","A","X"))
);
/* END PLUGINS */
?>
