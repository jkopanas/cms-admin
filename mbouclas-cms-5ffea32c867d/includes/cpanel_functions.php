<?php
function get_user_favorites($uid)
{
	$sql = new db();
	$p = new convert();
	$sql->db_Select("user_favorites","*","uid = '".ID."'");

	if ($sql->db_Rows() > 0) 
	{
		$favorites = execute_multi($sql,0);
		for ($i=0;count($favorites) > $i;$i++)
		{
			$fav_date = $p->convert_date($favorites[$i]['date_added'],"long");
			$favorites[$i] = get_user_image(ID,$favorites[$i]['productid']);
			$favorites[$i]['fav_date'] = $fav_date;
			
		}
		return $favorites;
	}
	else 
	{
		return 0;	
	}
}

function add_user_history($id,$limit)
{
		$sql = new db();
		$sql->db_Select("user_history","*","uid = ".ID." ORDER BY date_added ASC");
		if ($sql->db_Rows() != $limit) 
		{
			$sql->db_Insert("user_history","'$id','".ID."','".time()."'");
		}
		else 
		{
			$res = execute_multi($sql,0);
			$sql->db_Delete("user_history","uid =".ID." AND productid = ".$res[0]['productid']." AND date_added = '".$res[0]['date_added']."'");
			$sql->db_Insert("user_history","'$id','".ID."','".time()."'");
		}
}

function get_user_history()
{
	$sql = new db();
	$p = new convert();
	$sql->db_Select("user_history","*","uid = ".ID." ORDER BY date_added DESC");

	if ($sql->db_Rows() > 0) 
	{
		$history = execute_multi($sql,0);
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
			$htr_date = $p->convert_date($history[$i]['date_added'],"long");
			$history[$i] = get_product($history[$i]['productid'],FRONT_LANG,1);
			$history[$i]['htr_date'] = $htr_date;
		}
		return $history;
	}
	else 
	{
		return 0;	
	}
}

function get_user_searches()
{
	$sql = new db();
	$t = new convert();
	$sql->db_Select("user_searches","id,title,date_added","uid = ".ID);
	$searches = execute_multi($sql,0);
	for ($i=0;$sql->db_Rows() > $i;$i++)
	{
		$searches[$i]['date_added'] = $t->convert_date($searches[$i]['date_added'],"long");		
	}
	return $searches;
}

function get_user_search($id)
{
	$sql = new db();
	$sql->db_Select("user_searches","*","id = $id AND uid = ".ID);
	if ($sql->db_Rows() > 0) 
	{
		$res = execute_single($sql,1);
		$t = explode("##",$res['posted_data']);
		for ($i=0;count($t) > $i;$i++)
		{
			$t1 = explode("::",$t[$i]);
			if ($t1[1] != "") {	$n[$t1[0]] = $t1[1];}
		}
		$res['posted_data'] = $n;
		return $res;
	}
	else 
	{
		return 0;
	}
}

function get_user_subscriptions($uid="no")
{
	$sql = new db();
	$sql2 = new db();
	$t = new convert();
	$uid = ($uid == "no") ? ID : $uid;
	$sql->db_Select("maillist","groupid,date_added","uid = ".$uid);

	if ($sql->db_Rows() > 0) 
	{
		$subs = execute_multi($sql,0);
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
			$subs[$i]['date_added'] = $t->convert_date($subs[$i]['date_added'],"long");
			//Get subscription names
			$sql->db_Select("maillist_groups","*","id =".$subs[$i]['groupid']);
			$s = execute_single($sql,0);
			$subs[$i]['subscription'] = $s['title'];
		}
		return $subs;
	}
	else 
	{
		return 0;
	}
}

function get_user_alerts($uid,$id="no")
{
	$sql = new db();
	$p = new convert();
	$id = ($id == "no") ? "" : "id = $id AND ";
	$sql->db_Select("user_alerts","*","$id uid=".$uid);
	if ($sql->db_Rows() > 0) 
	{
		$alerts = execute_multi($sql,1);
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
			$alerts[$i]['date_added'] = $p->convert_date($alerts[$i]['date_added'],"short");
			$alerts[$i]['last_activated'] = $p->convert_date($alerts[$i]['last_activated'],"short");
		}
		return $alerts;
	}
	else 
	{
		return 0;
	}
	
}

function get_user_notifications($uid,$id="no")
{
	$sql = new db();
	$sql2 = new db();
	$p = new convert();
	$id = ($id == "no") ? "" : "id = $id AND";
	$sql->db_Select("user_notifications","*","$id uid = $uid");
	if ($sql->db_Rows() > 0)
	{
	if ($id == "") 
	{
		$notifications = execute_multi($sql,0);
//		$notifications['count'] = $sql->db_Rows();
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
			$notifications[$i]['date_added'] = $p->convert_date($notifications[$i]['date_added'],"short");
			$sql2->db_Select("user_alerts","title","id =".$notifications[$i]['alertid']." AND uid = $uid");
			$t = execute_single($sql2,1);
			$notifications[$i]['alert'] = $t['title'];
		}
		return $notifications;
	}
	else 
	{
		$notifications = execute_single($sql,0);

		$notifications['date_added'] = $p->convert_date($notifications['date_added'],"short");
		$products = explode(",",$notifications['productids']);
		$sql2->db_Select("user_alerts","title","id =".$notifications['alertid']." AND uid = $uid");
		$t = execute_single($sql2,1);
		$notifications['alert'] = $t['title'];
		for ($i=0;count($products) > $i;$i++)
		{
			$product[$i][] = get_product($products[$i],FRONT_LANG,1);
		}
		$notifications['products'] = $product;
		return $notifications;
	}
	}//END OF ROWS FOUND
}
?>