<?php

class Benchmark
{
    private function getmicrotime()
    {
        list($micro_seconds, $seconds) = explode(" ", microtime());
        return ((float)$micro_seconds + (float)$seconds);   
    }
    public function StartBenchmark()
    {
        $this->StartBenchmarkTime = (float) $this->getmicrotime();
    }
    
    public function EndBenchmark()
    {
        $this->EndBenchmarkTime = (float) $this->getmicrotime();       
        return (float) ($this->EndBenchmarkTime-$this->StartBenchmarkTime);
    }
}

class Handlers 
{
    /**
     * Open a file, parse it and returns it content in as string.
     */
    static public function filereader($file)
    {
    	if(is_file($file)?filesize($file)>0:false)
    	{
            ob_start();
    		$fp = fopen($file,"r+");
    		$filedata=fread($fp, filesize($file));
    		fclose($fp);
    		ob_end_flush();
    		return (string) $filedata; 
    	}
        
        return "";
    }
    
    /**
     * Open a file and write to it.
     */
    static public function filewriter($filepath,$content)
    { 
        if(!is_file($_SERVER['DOCUMENT_ROOT'].$filepath))
        {   
            $matches = array();
            //Find directory
            preg_match('`^(.+)/([a-zA-Z0-9]+\.[a-z]+)$`i', $filepath, $matches);
            
            if(isset($matches[1]))
            {
          		$directory=$matches[1];
        
        		if ($directory?!is_dir($directory):false)
                {    
                    //Create directory if not exist
                    if (!mkdir($directory)){return FALSE;}
      		    }
            }              
        }
        
        $handle = fopen($filepath,'w+');
        if($handle)
        {
            fwrite($handle,$content);
            fclose($handle);
        }
        chmod($filepath, 0755);
    }
    
    /**
     * Scan an array of objects and returns a string in csv format.
     * @param $seperator string A group of characters used to seperate values. Each value is the object's property value.
     * @param $newline string A group of character used to seperate lines.
     * @param $array array The list of objects
     * @param $mapping array The list of object's properties to be export. 
     */
    static public function convert_objects_2_csv($seperator,$newline,$array,$mapping)
    {
        (string) $csv = "";
        (int) $insert_deals_length = count($array);

        //Create csv headers
        $csv = implode($seperator, $mapping).$newline;
 
        if(isset($array)&&isset($mapping))
        {
            foreach($array as $key => $row)
            {  
                foreach($mapping as $map_key => $map_value)
                {          
                    $csv .= $row->getProperty($map_value).$seperator;           
                }
                $csv = rtrim($csv,$seperator).$newline;       
            }
        }       
        return $csv;
    }
    
    /**
     * Scan an array of hashtables and returns a string in csv format.
     * @param $seperator string A group of characters used to seperate values.
     * @param $newline string A group of character used to seperate lines.
     * @param $array array The list of hashtables.
     * @param $mapping array The list of hashtable's keys. 
     */
    static public function convert_array_2_csv($seperator,$newline,$array,$mapping)
    {
        (string) $csv = "";
        (int) $insert_deals_length = count($array);
     
        $csv = implode($seperator, $mapping).$newline;

        if(isset($array)&&isset($mapping))
        {
            foreach($array as $key => $row)
            {
                foreach($mapping as $map_key => $map_value)
                {
                    $csv .= $row[$map_value].$seperator;          
                }
                
                $csv = rtrim($csv,$seperator).$newline;     
            }
        }
          
        return $csv;
    }

    /**
     * Filter an array of objects, accoridng to unique value of the given object's properties.
     * @param $objArray array The list with objects
     * @param $property string The property name to use as identifier for unique objects
     */
    static public function array_unique_object($objArray=array(), $property) {
        $propertylist = array();

        if(isset($objArray))
        {
            foreach($objArray as $key => $obj)
            {
                if (!$propertylist[$obj->$property]){
                    if(!is_null($obj->$property))
                    $propertylist[$obj->$property] = $obj->$property;
                }
            }
        }
        return $propertylist;  
    }
    
    /**
     * Convert a DateTime string to an array accordin to hierarchy format
     * @param $str string The DateTime.
     * @param $format array The hierarchy format.
     */    
    static public function convert_DateTimeToArray($str,$format=array("y","m","d","h","m","s"))
    {          
        preg_match_all('#[0-9]+#si',$str,$RegExpResults);
        return count($RegExpResults[0])==6?array_combine($format,$RegExpResults[0]):null;
    }
    
    /**
     * Conert a DateTime to an Unix int
     */
    static public function DateTImeArrayToInteger($DateTimeArray)
    {
        if($DateTimeArray)
        {return mktime($DateTimeArray["h"],$DateTimeArray["m"],$DateTimeArray["s"],$DateTimeArray["m"],$DateTimeArray["d"],$DateTimeArray["y"]);}
    }
    
}

/**
 * load_DATA_INFILE
 * Use as a template object for LOAD DATA INFILE MySQL Insert Method  
 * @package DataStore
 */
class load_DATA_INFILE
{
    /**
     * The .csv file name (absolute path name).     
     * @var string 
    */
    public $file_name;
    /**
     * The table name 
     * @var string  
     */
    public $table_name;
    /**
     * The characters responsibly for field seperation 
     * @var string
     */
    public $field_separate_char = "@##!@#";
    public $field_enclose_char;
    public $field_escape_char;
    /**
     * The characters responsibly for line seperation 
     * @var string
     */
    public $lines_separate_char = "\r\n";
    /**
     * True: the first line of csv file is used as a header
     * @var bool
     */
    public $use_csv_header;
    /**
     * The table columns
     * @var array 
     */
    public $arr_csv_columns;
    public $arr_csv_columns_flip;
    /**
     * The transformation of user variables
     * @var string 
     */
    public $use_dynamic_columns;        
}

/**
 * MySQLiDBHanlder_Exception
 * Handles MySQLi errors   
 * @package DataStore
 */
class MySQLiDBHanlder_Exception extends Exception
{
    function __construct($sql,$driver)
    {
        $errorCode = $driver->errno;
        $msg = $driver->error;
        echo "<b>SQL Query (Custom Info):</b>";
        echo $sql;
        echo "<br/>";
        parent::__construct($msg,$errorCode);
    }
}

/**
 * PDODBHandler_Exception
 * Handles PDO errors   
 * @package DataStore
 */
class PDODBHandler_Exception extends Exception
{
    function __construct($sql,$driver)
    {
        $error = $driver->errorInfo();
        $errorCode = $error[1];
        $msg = $error[2];
        echo "<b>SQL Query (Custom Info):</b>";
        echo $sql;
        echo "<br/>";
        parent::__construct($msg,$errorCode);
    }
}

interface DB_Driver
{
    /**
     * Prepares the SQL query, and returns a statement handle.       
     */
    public function db_PrepareStatement($query);  
    public function db_Select($table,$fields,$parametersValue=array(),$parametersType=array(),$template="",$mode="default");
    public function db_Select_multi($table,$fields,$parametersValue=array(),$parametersType=array(),$template="",$mode="default");
    public function db_Select_table($table,$fields);
    public function db_Query($query); 
    public function db_Create_Temp_Table($table,$fields);
    
    /**
     * Execute the Insert Query and return the insert id
     * @param $table string The table name
     * @param $arg string The insert values 
     */
    public function db_Insert($table,$arg);
    
    /**
     * Create a .csv file to be used in LOAD DATA INFILE insert technics, using object structured content.
     * @param $filepath string The absolute csv's file path.
     * @param $contentObjects array Each row is an Object, which will be used as content, and each object's method represent a coloumn field.
     * @param $mapping array The list with all the field names. It used to create csv headers, and to access object's method.
     */ 
    public function db_Create_DATA_INFILE($filepath,$contentObjects,$mapping);
    
    /**
     * Create a .csv file to be used in LOAD DATA INFILE insert technics, using array structured content.
     * @param $filepath string The absolute csv's file path.
     * @param $contentObjects array Each row is a Hashtable, which will be used as content, and each key represent a coloumn field.
     * @param $mapping array The list with all the field names. It used to create csv headers, and to access hashtable's key.
     */ 
    public function db_Create_DATA_INFILE_with_array($filepath,$contentObjects,$mapping);
    
    /**
     * Execute a LOAD DATA INFILE Query
     * @param $localMode bool True:execute a LOAD DATA LOCAL INFILE Query
     */
    public function db_LOAD_DATA_INFILE(load_DATA_INFILE $DATA_INFILE,$localMode = false);
    
    /**
     * Retrieves information about a specific table, and store it in array.
     * array[0]: string: Create table query which can be used to create temporary table or a clone table. 
     * array[1]: array: Columns names 
     * array[2]: array: Columns info
     */
    public function db_table_fields_info($table_name,$ignore_types=array(),$fields_list=array(),$fields_mode=false);
    
    /**
     * Returns as table information, table's column names.
     */
    public function db_table_fields_name($table_name);
    public function db_Update_Temp($table,$tmp_table,$connKey,$updateValues);
    public function db_Fetch(); 
    
}

class DB_Common_Driver implements DB_Driver
{
    
    public function db_PrepareStatement($query)
    { 
        if($this->formatQuery?count($this->_Parameters)>0:false)
        {
            (string) $formatQueryMethod = $this->formatQuery;
            $query = $this->$formatQueryMethod($query);
        }
        
        $this->DB_Statement = $this->DB_Driver->prepare($query); 
        
        if(!$this->DB_Statement&&$this->ShowErrors)
        {
            throw new $this->_DriverType_Exception($query,$this->DB_Driver);
        }          
    }
    
    public function db_Select($table, $fields, $parametersValue=array(),$parametersType=array(), $template="", $mode="default")
    {
        $this->db_PrepareStatement("SELECT ".$fields." FROM ".$table." WHERE ".$template);
        $this->db_BindParameters($parametersValue,$parametersType);
        $this->DB_Statement->execute();
    }
    
    public function db_Select_multi($table, $fields, $parametersValue=array(),$parametersType=array(), $template="", $mode="default")
    {
        $this->db_PrepareStatement("SELECT ".$fields." FROM ".$table." WHERE ".$template);        
    }
    
    public function db_Select_table($table, $fields)
    {        
        $this->db_PrepareStatement("SELECT ".$fields." FROM ".$table); 
        $this->DB_Statement->execute();       
    }
    
    public function db_Query($query)
    {                             
        $result = $this->Query_Results($query);
        
        if($result)
        {
            return $result;
        }
        else if($this->ShowErrors)
        {throw new $this->_DriverType_Exception($query,$this->DB_Driver);}
    }   
     
    public function db_Create_Temp_Table($table,$fields)
    {   
        $query = "CREATE TEMPORARY TABLE ".$table." (".$fields.") ENGINE=MyISAM DEFAULT CHARSET=utf8";
        if(!$this->DB_Driver->query($query)&&$this->ShowErrors)
        {
            throw new $this->_DriverType_Exception($query,$this->DB_Driver);
        } 
    }
        
    public function db_Insert($table, $arg){
        
        $query = "INSERT INTO ".$table." VALUES (".$arg.")";
        if(!$this->DB_Driver->query($query)&&$this->ShowErrors)
        {
            throw new $this->_DriverType_Exception($query,$this->DB_Driver);
        }
        
        return $this->getInsertID();
    }
    
    public function db_Insert_Normal($table, $arg){
        
        $query = "INSERT INTO ".$table." VALUES (".$arg.")";
        
        $this->DB_Statement = $this->DB_Driver->query($query);
        
        /*if(!$statement)
        {
            throw new $this->_DriverType_Exception($query,$this->DB_Driver);
        }*/
        
        return $this->getInsertID();
    }
        
    public function db_Create_DATA_INFILE($filepath,$contentObjects,$mapping)
    {      
        Handlers::filewriter($filepath,Handlers::convert_objects_2_csv("@##!@#","@##%@#",$contentObjects,$mapping));    
    }
    
    public function db_Create_DATA_INFILE_with_array($filepath,$contentObjects,$mapping)
    {      
        Handlers::filewriter($filepath,Handlers::convert_array_2_csv("@##!@#","@##%@#",$contentObjects,$mapping));    
    }

    public function db_LOAD_DATA_INFILE( load_DATA_INFILE $DATA_INFILE,$localMode = false)
    {$localMode=false;
        $query = "LOAD DATA ".($localMode?"LOCAL ":"")."INFILE '".@mysql_escape_string($DATA_INFILE->file_name).
        "' INTO TABLE `".$DATA_INFILE->table_name.
        "` FIELDS TERMINATED BY '".@mysql_escape_string($DATA_INFILE->field_separate_char).
        "' OPTIONALLY ENCLOSED BY '".@mysql_escape_string($DATA_INFILE->field_enclose_char).
        "' ESCAPED BY '".@mysql_escape_string($DATA_INFILE->field_escape_char).
        "' LINES TERMINATED BY '".@mysql_escape_string($DATA_INFILE->lines_separate_char).
        "' ".
        ($DATA_INFILE->use_csv_header ? " IGNORE 1 LINES " : "")
        ."(".implode(",", $DATA_INFILE->arr_csv_columns).")"
        .($DATA_INFILE->use_dynamic_columns?" SET ".$DATA_INFILE->use_dynamic_columns:"");
   
        if(!$this->DB_Driver->query($query)&&$this->ShowErrors)
        {
            throw new $this->_DriverType_Exception($query,$this->DB_Driver);
        }
    }
    
    public function db_table_fields_info($table_name,$ignore_types=array(),$fields_list=array(),$fields_mode=false)
    {
        $table_info = array();
        $table_fields = array();
        $table_info_create = array();
        
        $columns = $this->DB_Driver->query("SHOW COLUMNS FROM $table_name");
        while($column = $this->db_fetch_array($columns)){
            if($fields_list?isset($fields_list[$column['Field']])==$fields_mode:true)
            {
                $create_field_str = "".$column['Field'].""." ".
                $column['Type']." ".
                (!isset($ignore_types['Null'])?(isset($column['Null'])&&!isset($ignore_types['Null'])=="NO"?"NOT NULL":"NULL"):"")." ".
                (!isset($ignore_types['Key'])?(isset($column['Key'])&&!isset($ignore_types['Key'])=="PRI"?"PRIMARY KEY":""):"")." ".
                (!isset($ignore_types['Extra'])?(isset($column['Extra'])=="auto_increment"?"AUTO_INCREMENT":""):"");
                $column['Create_field_str'] = $create_field_str;
                $table_info_create[]=$create_field_str;
                $table_fields[] = $column['Field'];
                $table_info[] = $column;
            }
        }
        
        return array($table_info_create,$table_info,$table_fields);
    }
    
    public function db_table_fields_name($table_name)
    {
        $table_info = array();
        
        $columns = $this->DB_Driver->query("SHOW COLUMNS FROM $table_name");
        while($column = $this->db_fetch_array($columns)){
           
            $table_info[] = $column['Field'];
        }
        
        return $table_info;
    }
    
    public function db_Update_Temp($table,$tmp_table,$connKey,$updateValues)
    {
        $query = "UPDATE ".$table." a JOIN ".$tmp_table." b USING (".$connKey.")  SET ".$updateValues ;
        if(!$this->DB_Driver->query($query)&&$this->ShowErrors)
        {
            throw new $this->_DriverType_Exception($query,$this->DB_Driver);
        }
    }
    
    public function db_Fetch()
    {
        $this->mySQLrows = $this->db_StatementFetch();
    }
    
    
}


class DataStoreInstance
{    
    public function createInstance($DriverType,$mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb) 
    {        
        return new $DriverType($mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb);
    }
}

interface DataStoreCommands
{
    /**
     * Fetch all records from a table, and return them as a hashtable organized by the field value.
     * @param $set_row_key string  The field name from which for every row its value is set as key. 
     * @return array
     */
    public function get_table($table,$set_row_key="id");
    
    /**
     * Fetch all records from a table, and return them as a hashtable organized by the field value.
     * Using multiple fields in where statement.
     * @param $set_row_key string  The field name from which for every row its value is set as key.
     * @param $parametersType array The fields types used on where statement 
     * @return array
     */
    public function get_fields_multiwhere($table,$set_row_key="id",$parametersType,$whereStatement,$extra="");
    
    /**
     * Fetch all records from a table, and return them as a hashtable organized by the field value.
     * Using a specific field in where statement.
     * @param $set_row_key string  The field name from which for every row its value is set as key.
     * @param $parametersType array The fields types used on where statement 
     * @return array
     */
    public function get_fields($table,$set_row_key="id",$parametersType,$whereStatement,$extra="");
    
    /**
     * Update a table's record set using temporary table.
     * @param $table string The update table
     * @param $tmp_table string The temporary table
     * @param @connKey string The field name used to match records between update table & temporary table
     * @param @$fields array The table's field for update
     */    
    public function temp_update($table,$tmp_table,$connKey,$fields);    
    
    /**
     * Delete records from a table according to a list of values for a specific field
     * @param $keys array Keys is a list of values  
     * @param $keyfield string The field name use for value comparison
     */
    public function Delete_by_Key($table,$keys=array(),$keyfield="id",$keyType="i");
    
    public function RowCounter($rows);
}

class DataStore extends DB_Common_Driver implements DataStoreCommands
{
      
    private $_Row_Key;
	private $_Row_Key_Array;    
    private $_RowKeyValue;    
	private $_sort_query_value;
	private $_sort_query_type="ASC";
    
    /**
     * The fields which will be retrieve from a table.
     * @var string
     */
    public $_customFields;
    
    /**
     * KeyToValueArray Situtation defines when a field is used as index and another field as value on an array result from a SELECT Sql Query.
     * @var bool
     */
    public $_KeyToValueArray_Flag = false;
    
    /**
     * The table's field name where will be used to associate as value to a KeyToValueArray situation
     * @var string
     */
    public $_KeyToValueArray_Field;
    
    /**
     * True: A field has been defined as the index on the result array. To avoid overwritting rows since the field is not unique we create an extra auto-increment dimension.
     * @var bool
     */
    public $_multiRowKeys=true;
    
    private $Benchmark;
    
    public $_Parameters = array();
    
    public $ShowErrors = false; 
    
    public function _reset_properties()
    {
        $this->_multiRowKeys=true;
        $this->_customFields = "";
        $this->_KeyToValueArray_Field="";
        $this->_KeyToValueArray_Flag = false;     
    }
    
    public function __call($methodname,$args)
    {
        if(method_exists($this->Benchmark,$methodname))
        {
            return $this->Benchmark->$methodname($this);
        }
    }
  	
    public function get_table($table,$set_row_key="id")
    {
        (array) $return_array = array();
        
        $this->db_Select_table($table,$this->_customFields{0}?$this->_customFields:"*");
     
        $this->db_Fetch();

        $return_array = $this->get_Rows($set_row_key);
        
        $this->_reset_properties();
        return $return_array;
    }
    
    public function get_fields_multiwhere($table,$set_row_key="id",$parametersType,$whereStatement,$extra="")
    {
        (array) $return_array = array();
        $temp_return_array = array();
        $this->db_Select_multi($table,strlen($this->_customFields)>0?$this->_customFields:"*",$parametersType,$whereStatement,$extra);
                
        $parametersValueLength = count($whereStatement);
        
        for($bp=0;$bp<$parametersValueLength;$bp++)
        {
            $this->db_bind_param($bp,$whereStatement[$bp],$parametersType[$bp]);
           
            $this->DB_Statement->execute();
            $this->db_Fetch();
            
            $temp_return_array=$this->get_Rows($set_row_key);
            
            if(key($temp_return_array))
            {$return_array[key($temp_return_array)]=$temp_return_array[key($temp_return_array)];}           
        } 
        
        return $return_array;
    }
	public function get_fields($table,$set_row_key="id",$parametersType,$whereStatement,$extra="")
	{		
		$mode="default";
		if ($this->_sort_query_value)
		{
			if(!$extra){$mode="WHERE ";}			
			$extra=$extra." order by ".$this->_sort_query_value." ".$this->_sort_query_type." ";
		}
   
		if($extra)
		{$this->db_Select($table,strlen($this->_customFields)>0?$this->_customFields:"*",$parametersType,$whereStatement,$extra,$mode);}
		else
		{$this->db_Select($table,strlen($this->_customFields)>0?$this->_customFields:"*",$parametersType,$whereStatement);}
        
        $this->db_Fetch();
            
		$return_array=$this->get_Rows($set_row_key);
		$this->_sort_query_type="ASC";
		$this->_sort_query_value="";
       
		return $return_array;
	}
    
    //public function  
    		
    public function temp_update($table,$tmp_table,$connKey,$fields)
    {
        (string) $updateValues = "";
        
        $fieldsLength = count($fields);
        for($f=0;$f<$fieldsLength;$f++)
        {
            $updateValues .="a.".$fields[$f]." = b.".$fields[$f].",";
        }
        $updateValues = rtrim($updateValues,",");
        
        $this->db_Update_Temp($table,$tmp_table,$connKey,$updateValues);
    }
    
    public function Delete_by_Key($table,$keys=array(),$keyfield="id",$keyType="i")
    {
        $this->db_Delete_by_Key($table, $keys,$keyfield,$keyType);
    }      
    
    public function RowCounter($rows)
    {
        return $this->db_RowCounter($rows);
    }
    
    public function CloseConnection()
    {
        $this->db_CloseConnection();
    }
    
    public function CountFields($results)
    {
        return $this->db_CountFields($results);
    }
    
    public function FieldNameByOffset($results,$offset)
    {
        return $this->db_FieldNameByOffset($results,$offset);
    }
    
    public function Query_Results($query)
    {
        return $this->db_Query_Results($query);
    }
    
    public function FetchArray($columns)
    {
        return $this->db_fetch_array($columns);
    }
    
    public function get_DB_Statement()
    {
        return $this->DB_Statement;
    }
}

class MySQLiDBHanlder extends DataStore
{
    protected $DB_Statement;
    protected $ParamTypes;
    protected $formatQuery="_formatQuery";
    protected $_DriverType="MySQLiDBHanlder";
    protected $_DriverType_Exception="MySQLiDBHanlder_Exception";
    protected $QueryObject;
        
    public function __construct($mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb="")
    {
        $this->db_Connect($mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb);
        $this->Benchmark = new Benchmark();
    }
    
    protected function db_Connect($mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb)
    {
        $this->DB_Driver = new mysqli($mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb);
        $this->DB_Driver->set_charset("utf8");
        
        if ($this->DB_Driver->connect_error) {
            die('Connect Error (' . $this->DB_Driver->connect_errno . ') '
                    . $this->DB_Driver->connect_error);
        }  
    }
    
    protected function _formatQuery($query)
    {        
        return str_replace($this->_Parameters,array_fill(0,count($this->_Parameters),"?"),$query);
    }
    
    protected function getInsertID()
    {
        return $this->DB_Driver->insert_id;
    }
    
    protected function db_fetch_array($columns)
    {
        return $columns->fetch_array();
    }
    
    protected function db_bind_param($index,$type,$value)
    {
        $this->DB_Statement->bind_param($type,$value);
    }
    
    protected function db_BindParameters($parametersValue,$parametersType)
    {
        $parametersValueLength = count($parametersValue);       
        for($bp=0;$bp<$parametersValueLength;$bp++)
        {
            $this->DB_Statement->bind_param($parametersType[$bp],$parametersValue[$bp]);
        }
    }
    
    protected function db_StatementFetch()
    {
        $metadata=$this->DB_Statement->result_metadata();
        
        while ($field = $metadata->fetch_field()) 
        {$params[] = &$row[$field->name];}
        
        call_user_func_array(array($this->DB_Statement, 'bind_result'), $params);

        return $row;
    }
    
    private function clone_Results($results,$set_row_key)
    {
        $return_results = Array();
        foreach ($results as $k => $v) {
            $return_results[$k] = $v;                        
        }        
        $this->_RowKeyValue = $return_results[$set_row_key];
        
        return $return_results;
    }
    
    private function clone_single_Results($results,$set_row_key)
    {
        $return_value = $results[$this->_KeyToValueArray_Field];        
        $this->_RowKeyValue = $results[$set_row_key];
        
        return $return_value;
    }
    
    protected function get_Rows($set_row_key="")
    {
        $row_array = Array();
        $i=-1;
   
        $result_return_Method = $this->_KeyToValueArray_Flag?"clone_single_Results":"clone_Results";
              
        if($set_row_key)
        {
            if($this->_multiRowKeys)
            {                
                while($this->DB_Statement->fetch())
                {                       
                    $mySQLrows = $this->$result_return_Method($this->mySQLrows,$set_row_key);                   
                    $row_array[$this->_RowKeyValue][]=$mySQLrows;
                }               
            }
            else
            {
                while($this->DB_Statement->fetch())
                {               
                    $mySQLrows = $this->$result_return_Method($this->mySQLrows,$set_row_key);                   
                    $row_array[$this->_RowKeyValue]=$mySQLrows;                                      
                }
            }
        }
        else
        {
            while($this->DB_Statement->fetch())
            {$row_array[]=$this->$result_return_Method($this->mySQLrows,$set_row_key);}
        }
       
		return $row_array;
    }
    
    protected function db_Delete_by_Key($table, $keys=Array(),$keyfield="id",$keyType="i")
    {
        $this->db_PrepareStatement("DELETE FROM ".$table." WHERE ".$keyfield."=?");
        $keyLength = count($keys);
        for($deleteCounter=0;$deleteCounter<$keyLength;$deleteCounter++)
        {
            $this->DB_Statement->bind_param($keyType,$keys[$deleteCounter]);            
            $this->DB_Statement->execute();
        }
    }
    
    protected function db_RowCounter($rows)
    {
        return $rows->num_rows;
    }
    
    protected function db_CloseConnection()
    {
        $this->DB_Driver->close();
    }
    
    protected function db_CountFields($results)
    {
        return $results->field_count;
    }
    
    protected function db_FieldNameByOffset($results,$offset)
    {
        $field_info = $results->fetch_fields();
        
        return $field_info[$offset]->name;
    }
    
    protected function db_Query_Results($query)
    {
        
        return $this->QueryObject = $this->DB_Driver->query($query);
        //return mysqli_fetch_all($this->QueryObject);
    }
}

class PDODBHandler extends DataStore
{
    protected $DB_Statement;
    protected $ParamTypes;
    protected $formatQuery=null;
    protected $_DriverType="PDODBHandler";
    protected $_DriverType_Exception="PDODBHandler_Exception";
    protected $QueryObject;
    
    public function __construct($mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb="")
    {                
        $this->db_Connect($mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb);
        $this->Benchmark = new Benchmark();
    }
    
    public function db_Connect($mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb)
    {
        try {
            $this->DB_Driver = new PDO('mysql:host='.$mySQLserver.';dbname='.$mySQLdefaultdb,$mySQLuser,$mySQLpassword);
            $this->DB_Driver->exec("SET CHARACTER SET utf8");
        }
        catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
        
        $this->ParamTypes["i"]=PDO::PARAM_INT;
        $this->ParamTypes["s"]=PDO::PARAM_STR;
    }
    
    protected function getInsertID()
    {
        return $this->DB_Driver->lastInsertId();
    }
    
    protected function db_fetch_array($columns)
    {
        
        if($row = $this->QueryObject->fetch())        
        return $row;
    }
    
    protected function db_bind_param($index,$type,$value)
    {   
        $this->DB_Statement->bindValue($this->_Parameters[$index],$value,$this->ParamTypes[$type]);
    }
    
    protected function db_BindParameters($parametersValue,$parametersType)
    {        
        $parametersValueLength = count($parametersValue);       
        for($bp=0;$bp<$parametersValueLength;$bp++)
        {           
            $this->DB_Statement->bindValue($this->_Parameters[$bp],$parametersValue[$bp],$this->ParamTypes[$parametersType[$bp]]);
        }
    }
    
    protected function db_StatementFetch()
    {        
        $this->DB_Statement->setFetchMode(PDO::FETCH_ASSOC);
    }
    
    private function clone_Results($results,$set_row_key)
    {
        $return_results = Array();
        foreach ($results as $k => $v) {
            $return_results[$k] = $v;                        
        }
        $this->_RowKeyValue = $return_results[$set_row_key];
        
        return $return_results;
    }
    
    private function clone_single_Results($results,$set_row_key)
    { 
        $return_value = $results[$this->_KeyToValueArray_Field];        
        $this->_RowKeyValue = $results[$set_row_key];
        
        return $return_value;
    }
    
    protected function get_Rows($set_row_key="")
	{
        $row_array = Array();
        $i=-1;

        $result_return_Method = $this->_KeyToValueArray_Flag?"clone_single_Results":"clone_Results";
            
        if($set_row_key)
        {
            if($this->_multiRowKeys)
            {                
                while($row = $this->DB_Statement->fetch())
                {   
                    $mySQLrows = $this->$result_return_Method($row,$set_row_key);                  
                    $row_array[$this->_RowKeyValue][]=$row;
                }               
            }
            else
            {
                while($row = $this->DB_Statement->fetch())
                {               
                    $mySQLrows = $this->$result_return_Method($row,$set_row_key);                   
                    $row_array[$this->_RowKeyValue]=$mySQLrows;                                      
                }
            }
        }
        else
        {
            while($row = $this->DB_Statement->fetch())
            {$row_array[]=$this->$result_return_Method($row,$set_row_key);}
        }
             
		return $row_array;
	}
        
    protected function db_Delete_by_Key($table, $keys=Array(),$keyfield="id",$keyType="i")
    {
        $this->db_PrepareStatement("DELETE FROM ".$table." WHERE ".$keyfield."=:".$keyfield);
        $keyLength = count($keys);
        for($deleteCounter=0;$deleteCounter<$keyLength;$deleteCounter++)
        {
            $this->DB_Statement->bindParam(":".$keyfield,$this->ParamTypes[$keyType],$keys[$deleteCounter]);
            $this->DB_Statement->execute();
        }        
    }
    
    protected function db_RowCounter($rows)
    {
        return $this->QueryObject->rowCount();
    }
    
    protected function db_CloseConnection()
    {
        $this->DB_Driver = null;
    }
    
    protected function db_CountFields($results)
    {
        return $results->columnCount();
    }
    
    protected function db_FieldNameByOffset($results,$offset)
    {
        $metadata = $results->getColumnMeta($offset);
        
        return $metadata["name"];
    }
    
    protected function db_Query_Results($query)
    {
        //$this->QueryObject = $this->DB_Driver->query($query);
        $this->QueryObject = $this->DB_Driver->query($query);
        if($this->QueryObject)
        return $this->QueryObject;
        else
        return false;
        //array();//$this->QueryObject->fetchAll();
    }
}


?>