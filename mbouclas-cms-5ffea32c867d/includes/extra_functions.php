<?php
function create_ingredient_list($arr)
{
	 foreach ($arr as $key => $value) 
 	{   
	 if (strstr($key,"$")) 
	 	{
	 	$key = str_replace("$","-",$key);
	 	list($field,$id)=split("-",$key);
	 	$ingredients_list .= "$field::$value"; 	
	 	$ingredients_list .= "|"; 	
		}
 	}//END OF INGREDIENT LIST AND POST ARRAY
 $a = strlen($ingredients_list);
 $ingredients_list = substr($ingredients_list,0,$a-1);//strip last useless seperator of a string 
 return $ingredients_list;
}

function create_ingredient_list_array($arr)
{
	 foreach ($arr as $key => $value) 
 	{   
	 if (strstr($key,"$")) 
	 	{
	 	$key = str_replace("$","-",$key);
	 	list($field,$id)=split("-",$key);
	 	$ingredients_list[$id][$field] = $value; 	
	 	$ingredients_list[$id]["id"] = $id; 	
		}
 	}//END OF INGREDIENT LIST AND POST ARRAY
 return $ingredients_list;
}


function reverse_ingredient_list($ingredient_list)
{
	$arr = array();
		$t = explode("|",$ingredient_list);
		for ($j=0;count($t) > $j;$j++)
		{
			$t1 = explode("::",$t[$j]);
			$arr[$j][$t1[0]] = $t1[1];
			$arr[$j]["id"] = $j;
		}
		return $arr;
}

function edit_ingredients_list($ingredients)
{
$ar = explode("\n",$ingredients);
for ($i=0;count($ar) > $i;$i++)
{
	preg_match_all('/[A-Za-z](.*)/',$ar[$i],$matches);
	preg_match_all('/\\d+/',$ar[$i],$out);
	$a[$i]['ingredient'] = $matches[0][0];
	$a[$i]['id'] = $i;
	if (!strstr($out[0][0],$matches[0][0])) 
	{
		$a[$i]['quantity'] = $out[0][0];
	}
}

return $a;
}

?>