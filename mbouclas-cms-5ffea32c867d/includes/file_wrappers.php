<?php

#
# fopen() wrapper
#
function func_fopen($file, $perm = 'r', $is_root = false) {
	$file = func_allow_file($file, $is_root);
	if ($file === false)
		return false;
	return @fopen($file, $perm);
}

#
# fopen + fread wrapper
#
function func_file_get($file, $is_root = false) {
	$fp = func_fopen($file, 'rb', $is_root);

	if ($fp === false) return false;

	while (strlen($str = fread($fp, 8192)) > 0 )
		$data .= $str;

	fclose($fp);	
	return $data;
}

#
# readfile() wrapper
#
function func_readfile($file, $is_root = false) {
	$file = func_allow_file($file, $is_root);
	if ($file === false) return false;
	return readfile($file);
}

#
# Get tmpfile content
#
function func_temp_read($tmpfile, $delete = false) {
	if (empty($tmpfile))
	return false;

	$fp = @fopen($tmpfile,"rb");
	if(!$fp)
		return false;

	while (strlen($str = fread($fp, 4096)) > 0 )
		$data .= $str;
	fclose($fp);    
                
	if ($delete) {
		@unlink($tmpfile);
	}

	return $data;
}

#
# move_uploaded_file() wrapper
#
function func_move_uploaded_file($file) {
	global $HTTP_POST_FILES, $file_temp_dir;

	if(empty($file) || !isset($HTTP_POST_FILES[$file]))
		return false;

	$path = func_allow_file(tempnam($file_temp_dir,basename($HTTP_POST_FILES[$file]['name'])), true);
	if ($path === false)
		return false;

	if (move_uploaded_file($HTTP_POST_FILES[$file]['tmp_name'],$path))
		return $path;

	chmod($path, 0644);
	return false;
}

#
# file() wrapper
#
function func_file($file, $is_root = false) {
	$file = func_allow_file($file, $is_root);
	if ($file === false) return array();

	return file($file);
}

?>