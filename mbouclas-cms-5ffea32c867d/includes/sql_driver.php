<?php

class db{

	//	 global variables
	public  $mySQLserver;
	public  $mySQLuser;
	public  $mySQLpassword;
	public  $mySQLdefaultdb;
	public  $mySQLaccess;
	public  $mySQLresult;
	public  $mySQLrows;
	public  $mySQLerror;
    public  $DriverType = "PDODBHandler";
    private $DataStore;
    public  $last_insert_id;
    public  $queryCount = array();
    

	public function db_Connect($mySQLserver, $mySQLuser, $mySQLpassword, $mySQLdefaultdb){
		/*
		# Connect to mySQL server and select database
		#
		# - parameters #1:		string $mySQLserver, mySQL server
		# - parameters #2:		string $mySQLuser, mySQL username
		# - parameters #3:		string $mySQLpassword, mySQL password
		# - parameters #4:		string mySQLdefaultdb, mySQL default database
		# - return				error if encountered
		# - scope					public
		*/
        /*
		$this->mySQLserver = $mySQLserver;
		$this->mySQLuser = $mySQLuser;
		$this->mySQLpassword = $mySQLpassword;
		$this->mySQLdefaultdb = $mySQLdefaultdb;
		$temp = $this->mySQLerror;
		$this->mySQLerror = FALSE;
		$this->mySQL_access = @mysql_connect($this->mySQLserver, $this->mySQLuser, $this->mySQLpassword);
		@mysql_select_db($this->mySQLdefaultdb);
		$this->dbError("dbConnect/SelectDB");
		return $this->mySQLerror = $temp;
        */
        $DataStoreInstance = new DataStoreInstance();
        $this->DataStore = $DataStoreInstance->createInstance($this->DriverType,$mySQLserver,$mySQLuser,$mySQLpassword,$mySQLdefaultdb);
	}
    
    public function show_errors($show_errors=true)
    {
        $this->DataStore->ShowErrors = $show_errors;
    }
	public function db_Select($table, $fields="*", $arg="", $mode="default"){
		/*
		# Select with args
		#
		# - parameter #1:	string $table, table name
		# - parameter #2:	string $fields, table fields to be retrieved, default *
		# - parameter #3:	string $arg, query arguaments, default null
		# - parameter #4:	string $mode, arguament has WHERE or not, default=default (WHERE)
		# - return				affected rows
		# - scope					public
		*/
        
        /*
		global $dbq;
		$dbq++;

		$debug = 0;
		$debugtable = "forum_t";
		if($arg != "" && $mode=="default"){
			if($debug == TRUE && $debugtable == $table){ echo "SELECT ".$fields." FROM ".MPREFIX.$table." WHERE ".$arg."<br />"; }
			if($this->mySQLresult = @mysql_query("SELECT ".$fields." FROM ".MPREFIX.$table." WHERE ".$arg)){
				$this->dbError("dbQuery");
				return $this->db_Rows();
			}else{
				$this->dbError("db_Select (SELECT $fields FROM ".MPREFIX."$table WHERE $arg)");
				return FALSE;
			}
		}else if($arg != "" && $mode != "default"){
			if($debug == TRUE && $debugtable == $table){ echo "@@SELECT ".$fields." FROM ".MPREFIX.$table." ".$arg."<br />"; }
			if($this->mySQLresult = @mysql_query("SELECT ".$fields." FROM ".MPREFIX.$table." ".$arg)){
				$this->dbError("dbQuery");
				return $this->db_Rows();
			}else{
				$this->dbError("db_Select (SELECT $fields FROM ".MPREFIX."$table $arg)");
				return FALSE;
			}
		}else{
			if($debug == TRUE && $debugtable == $table){ echo "SELECT ".$fields." FROM ".MPREFIX.$table."<br />"; }
			if($this->mySQLresult = @mysql_query("SELECT ".$fields." FROM ".MPREFIX.$table)){
				$this->dbError("dbQuery");
				return $this->db_Rows();
			}else{
				$this->dbError("db_Select (SELECT $fields FROM ".MPREFIX."$table)");
				return FALSE;
			}		
		}
        */
        $debug = 0;
        if($arg != "" && $mode=="default"){						
            $query = "SELECT ".$fields." FROM ".MPREFIX.$table." WHERE ".$arg;            
		}else if($arg != "" && $mode != "default"){						
            $query = "SELECT ".$fields." FROM ".MPREFIX.$table." ".$arg;
		}else{						
            $query = "SELECT ".$fields." FROM ".MPREFIX.$table;		
		}
		
        if($debug == TRUE && $debugtable == $table){ echo $query."<br />"; }
        
        $this->mySQLresult = $this->DataStore->db_Query($query);
        $this->registerQuery('select');
        if($this->mySQLresult)
        {return $this->db_Rows();}
        else
        {return false;}
	}

	public function db_Insert($table, $arg){
		/*
		# Insert with args
		#
		# - parameter #1:	string $table, table name
		# - parameter #2:	string $arg, insert string
		# - return				sql identifier, or error if (error reporting = on, error occured, boolean)
		# - scope					public
		*/
		
//		echo "INSERT INTO ".MPREFIX.$table." VALUES (".$arg.")<br>";
		/*if($result = $this->mySQLresult = @mysql_query("INSERT INTO ".MPREFIX.$table." VALUES (".$arg.")" )){
			
			return $result;
		}else{
			$this->dbError("db_Insert ($query)");
			return FALSE;
		}*/
		$this->registerQuery('insert');
        if($this->last_insert_id = $this->DataStore->db_Insert_Normal($table, $arg))
        {
            return $this->mySQLresult = $this->DataStore->get_DB_Statement();
            
        }
        else
        {return false;}
	}

	public function db_Update($table, $arg){
		/*
		# Update with args
		#
		# - parameter #1:	string $table, table name
		# - parameter #2:	string $arg, update string
		# - return				sql identifier, or error if (error reporting = on, error occured, boolean)
		# - scope					public
		*/
		$debug = 0;
//		$debugtable = "download";
        /*
        if($result = $this->mySQLresult = @mysql_query("UPDATE ".MPREFIX.$table." SET ".$arg)){
			
			return $result;
		}else{
			$this->dbError("db_Update ($query)");
			return FALSE;
		}
        */
        $this->registerQuery('update');
        $query = "UPDATE ".MPREFIX.$table." SET ".$arg;
        
		if($debug == TRUE){ echo $query."<br />"; }		
        
        if($result = $this->mySQLresult = $this->DataStore->db_query($query))
        {return $result;}
        else
        {return false;} 
	}

	public function db_Fetch(){
		/*
		# Retrieve table row
		#
		# - parameters		none
		# - return				result array, or error if (error reporting = on, error occured, boolean)
		# - scope					public
		*/
        /*
		if($row = @mysql_fetch_array($this->mySQLresult)){
			while (list($key,$val) = each($row)) {
				$row[$key] = stripslashes($val);
			}
			$this->dbError("db_Fetch");
			return $row;
		}else{
			$this->dbError("db_Fetch");
			return FALSE;
		}*/
        //return $this->DataStore->
        
        if($row = $this->DataStore->FetchArray($this->mySQLresult))
        {
            while (list($key,$val) = each($row)) {
				$row[$key] = stripslashes($val);
			}
            
            return $row;
        }
        else
        {return false;}
	}
    

	public function db_Count($table, $fields="(*)", $arg=""){
		/*
		# Retrieve result count
		#
		# - parameter #1:	string $table, table name
		# - parameter #2:	string $fields, count fields, default (*)
		# - parameter #3:	string $arg, count string, default null
		# - return				result array, or error if (error reporting = on, error occured, boolean)
		# - scope					public
		*/
//		echo "SELECT COUNT".$fields." FROM ".MPREFIX.$table." ".$arg;

        $query = "SELECT COUNT".$fields." FROM ".MPREFIX.$table." ".$arg;
		/*if($this->mySQLresult = @mysql_query("SELECT COUNT".$fields." FROM ".MPREFIX.$table." ".$arg)){
			$rows = $this->mySQLrows = @mysql_fetch_array($this->mySQLresult);
			return $rows[0];
		}else{
			$this->dbError("dbCount ($query)");            
		}*/
        if($this->mySQLresult = $this->DataStore->db_query($query))
        {
            $rows = $this->DataStore->FetchArray($this->mySQLresult);
            
            return $rows[0];
        }
        else
        {return false;} 
	}

	public function db_Close(){
		/*
		# Close mySQL server connection
		#
		# - parameters		none
		# - return				null
		# - scope					public
		*/
		/*mysql_close();
		$this->dbError("dbClose");
        */
        $this->DataStore->CloseConnection();
	}

	public function db_Delete($table, $arg=""){
		/*
		# Delete with args
		#
		# - parameter #1:	string $table, table name
		# - parameter #2:	string $arg, delete string
		# - return				result array, or error if (error reporting = on, error occured, boolean)
		# - scope					public
		*/
		if($table == "forum_t" || $table == "poll"){
//			echo "DELETE FROM ".MPREFIX.$table." WHERE ".$arg."<br />";			// debug
		}
		
        /*
		if($arg == ""){
			if($result = $this->mySQLresult = @mysql_query("DELETE FROM ".MPREFIX.$table)){
				return $result;
			}else{
				$this->dbError("db_Delete ($query)");
				return FALSE;
			}
		}else{
			if($result = $this->mySQLresult = @mysql_query("DELETE FROM ".MPREFIX.$table." WHERE ".$arg)){
				return $result;
			}else{
				$this->dbError("db_Delete ($query)");
				return FALSE;
			}
		}
        */
        $this->registerQuery('delete');
        if($arg == "")
        {$query = "DELETE FROM ".MPREFIX.$table;}
        else
        {$query = "DELETE FROM ".MPREFIX.$table." WHERE ".$arg;}
        
        if($result = $this->mySQLresult = $this->DataStore->db_query($query))
        {return $result;}
        else
        {return false;}	
	}

	public function db_Rows(){
		/*
		# Return affected rows
		#
		# - parameters		none
		# - return				affected rows, or error if (error reporting = on, error occured, boolean)
		# - scope					public
		*/
		/*$rows = $this->mySQLrows = @mysql_num_rows($this->mySQLresult);
		return $rows;
		$this->dbError("db_Rows");
        */
        $rows = $this->mySQLrows = $this->mySQLresult?$this->DataStore->RowCounter($this->mySQLresult):0;
        
        return $rows;
	}

	public function dbError($from){
		/*
		# Return affected rows
		#
		# - parameter #1		string $from, routine that called this function
		# - return				error message on mySQL error
		# - scope					private
		*/
		if($error_message = @mysql_error()){
			if($this->mySQLerror == TRUE){
				echo "<b>mySQL Error!</b> Function: $from. [".@mysql_errno()." - $error_message]<br />";
				return $error_message;
			}
		}
	}

	public function db_SetErrorReporting($mode){
		$this->mySQLerror = $mode;
	}


	public function db_Select_gen($arg){
		/*if($this->mySQLresult = @mysql_query($arg)){
			$this->dbError("db_Select_gen");
			return $this->db_Rows();
		}else{
			$this->dbError("dbQuery ($query)");
			return FALSE;
		}
        */
          
        $this->mySQLresult = $this->DataStore->db_Query($arg);
               
        if($this->mySQLresult)
        {return $this->db_Rows();}
        else
        {return false;}	
	}


	public function db_Fieldname($offset){

		/*$result = @mysql_field_name($this->mySQLresult, $offset);
		return $result;
        */
        return $this->DataStore->FieldNameByOffset($this->mySQLresult,$offset);
	}

	public function db_Num_fields(){
		/*$result = @mysql_num_fields($this->mySQLresult);
		return $result;*/
        
        return $this->DataStore->CountFields($this->mySQLresult);
	}

	public function q($query) {
    	/*	if($result = $this->mySQLresult = @mysql_query($query)){
    
    			return $result;
    		}else{
    			$this->dbError("db_Insert ($query)");
    			return FALSE;
    		}
        */    
    	$this->registerQuery('generic');
        $this->mySQLresult = $this->DataStore->db_Query($query);
                
        if($this->mySQLresult)
        {return $this->mySQLresult;}
        else
        {return false;}		
    }
    
    private function registerQuery($type="select")
    {
    	$start = microtime(true);
    	$this->queryCount[$type][] = microtime(true) - $start;
    }
    
    public function countQueries()
    {
    	foreach ($this->queryCount as $k=>$v)
    	{
    		$ret[$k] = sizeof($v);
    	}
        return $ret;
    }

    public function getTime()
    {
    	foreach ($this->queryCount as $k=>$v)
    	{
    		$ret[$k] = array_sum($v);
    	}
        return $ret;
    }
    
    
    
    
}


?>