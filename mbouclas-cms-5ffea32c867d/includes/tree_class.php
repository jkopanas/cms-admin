<?php
//	database functions :: MySQL

function db_connect($host,$user,$pass) //create connection
{
	$r = mysql_connect($host,$user,$pass);
	if(preg_match('/^5\./',mysql_get_server_info($r)))db_query('SET SESSION sql_mode=0');
	return $r;
}

function db_select_db($name) //select database
{
	return mysql_select_db($name);
}

function db_query($s) //database query
{
	return mysql_query($s);
}

function db_fetch_row($q) //row fetching
{
	return mysql_fetch_row($q);
}

function db_insert_id()
{
	return mysql_insert_id();
}

function db_error() //database error message
{
	return mysql_error();
}

function fillTheCList($parent,$level,$table,$table_lng,$lang) //completely expand category tree
{
    $tables = "`$table`
  INNER JOIN `$table_lng` ON (`$table`.categoryid = `$table_lng`.categoryid)";
       $fields = "  `$table`.categoryid,
  `$table`.parentid,
  `$table`.categoryid_path,
  `$table`.order_by,
  `$table`.product_count,
  `$table`.date_added,
  `$table_lng`.category";     
$q = "$table.categoryid<>0 and parentid=$parent AND code = '$lang' ORDER BY order_by";
//echo "SELECT $fields FROM $tables WHERE $q<Br><Br>";
	$q = db_query("SELECT $fields FROM $tables WHERE $q") or die (db_error());
	$a = array(); //parents
	while ($row = db_fetch_row($q))
	{
		$row[5] = $level;
		$a[] = $row;
		//process subcategories
		$b = fillTheCList($row[0],$level+1,$table,$table_lng,$lang);
		//add $b[] to the end of $a[]
		for ($j=0; $j<count($b); $j++)
		{
			$a[] = $b[$j];
		}
	}
	return $a;

} //fillTheCList


?>