<?php
//$_COOKIE['boxes'] = '';

include("init.php");

$current_module = $loaded_modules['content'];
$smarty->assign("current_module",$current_module);

if ($_POST['var']) {
foreach ($_POST['var'] as $k=>$v) {
	$posted_data[] = "$k : $v";
}
$mailer = new PHPMailer();
$mailbody = implode("\n\r\<br>",$posted_data);

$mailer->IsHTML(true);
$mailer->From = $_POST['var']['email'];  // This HAS TO be your gmail address or it won't work!
$mailer->FromName = COMPANY_NAME." ".$lang['user']; // This is the from name in the email, you can put anything you like here
$mailer->Subject = 'New Question From '.SITE_NAME;
$mailer->Body = $mailbody;
$mailer->CharSet ="utf-8";
$mailer->AddAddress(COMPANY_EMAIL);
$mailer->Send();
$harvester = new userHarvester();
echo $harvester->writeUserToDB($_POST['var'],array('fields'=>'id','check'=>1));
exit();
}

$item_settings = array('fields'=>'*','thumb'=>1,'CatNav'=>1,'debug'=>0,'active'=>1,'cat_details'=>1,'main'=>1,'images'=>1,'efields'=>1,'parse'=>1 );
$item_settings['images_settings_limit'] = "0,11";
$item_settings['images_settings_available'] = "1";
$item = $Content->GetItem("10",$item_settings);
if (!$smarty->isCached('home.tpl',$url)) {
$l = new siteModules();
$layout = $l->pageBoxes('all',e_FILE,'front',array('getBoxes'=>'full','fields'=>'boxes,areas,id,settings','boxFields'=>'id,title,name,settings,fetchType,file,required_modules,template','init'=>1,'boxFilters'=>array('active'=>0),'debug'=>0));
$smarty->assign("layout",$layout['boxes']);
}//END BOXES

$smarty->assign('item',$item);
$smarty->assign("include_file","welcome.tpl");//assigned template variable include_file
$smarty->assign("area","home");//assigned template variable include_file
include(ABSPATH."/memcache.php");
$smarty->caching = USE_SMARTY_CAHCHING;

HookParent::getInstance()->doTriggerHookGlobally("HomePreFetch");
$smarty->display("home.tpl",$url);//Display the home.tpl template


?>