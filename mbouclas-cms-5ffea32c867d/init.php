<?php
define('ABSPATH', dirname(__FILE__));

include(ABSPATH."/includes/config.php");
    $url =md5($_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI']);
//    $memcache->delete($url);

if (class_exists('Memcache')) {
	$value = $memcache->get($url);
	if ($value) {
/*		echo $value;
		exit();*/
	}
}

session_start();
include(ABSPATH."/includes/dataStore.php");
include(ABSPATH."/includes/sql_driver.php");
include(ABSPATH."/includes/pagination.php");
include(ABSPATH."/includes/common.php");
include(ABSPATH."/includes/functions.php");
include(ABSPATH."/includes/smarty.php");
include(ABSPATH."/includes/phpmailer/class.phpmailer.php");
include(ABSPATH."/includes/pass_crypt.php");
include(ABSPATH."/includes/file_wrappers.php");
include(ABSPATH."/includes/mailer.php");
include(ABSPATH."/manage/security.php");


if (USE_SMARTY_CAHCHING == 1) {
	$smarty->caching =1;
	if ($smarty->isCached('home.tpl',$url)) {
		$smarty->display('home.tpl',$url);
		exit();
	}
}
$sql = new db();
$sql->db_Connect(DBHOST, DBUSER, DBPASS, DB);

/*mysql_query(' set character set utf8 ');
mysql_query("SET NAMES 'utf8'") or die('Query failed: ' . mysql_error());*/
################### BEGIN Cryptography Settings #####################################################
$CRYPT_SALT = 85; # any number ranging 1-255
$START_CHAR_CODE = 100; # 'd' letter
################### END Cryptography Settings #######################################################
spl_autoload_register('smartyAutoload');
spl_autoload_register("__autoload");
function __autoload($class_name)
{
	if (file_exists(ABSPATH.'/modules/'."$class_name.php")) {		
    	require_once ABSPATH.'/modules/'."$class_name.php";
	}
	//	throw new Exception("Unable to load ".ABSPATH.'/modules/'."$class_name.php");
}

$hooks = new hooks();

############################# BEGIN GLOBAL VARIABLE DEFINITIONS #####################################
$config["Sessions"]["session_length"] = 3600;
define("USER_TYPE","visitor");//for stats purposes

define("e_HTTP", "/");
$url_prefix=substr($_SERVER['PHP_SELF'],strlen(e_HTTP),strrpos($_SERVER['PHP_SELF'],"/")+1-strlen(e_HTTP));
$num_levels=substr_count($url_prefix,"/");

for($i=1;$i<=$num_levels;$i++)
{
	$link_prefix.="../";
}

define("e_SELF", "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']); //full address + filename
define("e_QUERY", eregi_replace("&|/?PHPSESSID.*", "", $_SERVER['QUERY_STRING']));//everything on the string after the ?
define('e_BASE',$link_prefix);
define("e_REQUEST_URI",$_SERVER['REQUEST_URI']);
define("e_FILE",basename($_SERVER['PHP_SELF']));
define("TIME_LOGIN",12);//HOURS TO STAY LOGED IN, IF WE NEED DAYS WE MULTIPLY HOURS BY DAYS. e.g. 24*2 (2 days)
//  echo e_REQUEST_URI;
############################# END GLOBAL VARIABLE DEFINITIONS ###################################

############################# BEGIN SITE PREFS ##################################################
$prefs = load_prefs();
foreach ($prefs as $key => $value) 
{ 
   	if (!preg_match("[0-9]",$key)) 
	{
		define(strtoupper($key),$value);
		$smarty->assign(strtoupper($key),$value);
	}//END OF IF
}//END OF FOR 
$encrypt_emails = ENCRYPT_EMAILS;
global $encrypt_emails;
############################# END SITE PREFS ####################################################

############################# BEGIN EMAILER SETTINGS ############################################
// Email Settings
$site['from_name'] = COMPANY_NAME; // from email name
$site['from_email'] = COMPANY_EMAIL; // from email address

// Just in case we need to relay to a different server, 
// provide an option to use external mail server.
$site['smtp_mode'] = 'disabled'; // enabled or disabled
$site['smtp_host'] = null;
$site['smtp_port'] = null;
$site['smtp_username'] = null; 
############################ END EMAILER SETTINGS ###############################################

############################# BEGIN LOAD LANG ###################################################
//check if language change
$Languages = new Languages();

if ($_GET['lang']) 
{ 
$Languages->setLanguage($_GET['lang'],'front_lang');
$front_language = $Languages->checkLanguage($_GET['lang'],'Y');//ONLY ACTIVE LANGUAGES
if (DEFAULT_LANG != $_GET['lang']) {
	define('TRANSLATE',$_GET['lang']);
}
}//END OF IF
if (empty($front_language)) 
{  
 	$lang = $Languages->LoadLanguage(DEFAULT_LANG,array('common','front'));
 	define("FRONT_LANG",DEFAULT_LANG);
 	$front_language = DEFAULT_LANG;
}//END OF IF
else 
{  
	$front_language = $Languages->checkLanguage($front_language);
	$lang = $Languages->LoadLanguage($front_language);
}//END OF ELSE
$webmaster = 0;
if ($webmaster) {
	if (strpos($HTTP_USER_AGENT,'Opera') !== false)
    $user_agent = 'opera';
elseif (strpos($HTTP_USER_AGENT,'MSIE') !== false)
    $user_agent = 'ie';
else
    $user_agent = 'ns';
	$smarty->assign("lang",$Languages->webmasterModeConvertLabels($lang));
	$smarty->assign("webmasterMode",$webmaster);
}

$front_language = strtolower($front_language);
define("FRONT_LANG",$front_language);
$smarty->assign("front_language",$front_language);//assigned template variable front_language
$smarty->assign("FRONT_LANG",$front_language);//assigned template variable front_language
$smarty->assign("lang",$lang);//assigned template variable lang
$country = $Languages->get_country(FRONT_LANG);

############################# END LOAD LANG ###################################################

init_session();//get the user data id a cookie or session is set


####################### IF THE POST VARIABLES ARE SUBMITED LOGIN #############################
if(IsSet($_POST['username']))
{
if (isset($_POST['return_to'])) { define("RETURN_TO","http://".$_SERVER['HTTP_HOST'].$_POST['return_to']); } else {
	define("RETURN_TO","http://".$_SERVER['HTTP_HOST']);
}

$usr = new userlogin($_POST['username'], $_POST['password'], $_POST['autologin']); 
//on success the class userlogin will refresh the page and init will be called again to get the user data
}
####################### END LOGIN #############################################################			

############################## IF logout IN QUERY STRING LOGOUT ###############################
$cookie_name = "user_".USER_TYPE;
if(e_QUERY == "logout")
{
 $ip=getenv(REMOTE_ADDR); 
$sql->db_Update("users","user_lastvisit = '".time()."' WHERE id = ".ID." AND uname = '".UNAME."'");
$sql->q("REPLACE INTO stats_login_history (login, date_time, usertype, action, status, ip) VALUES ('".UNAME."','".time()."','$login_type','logout','success','$ip')");
setcookie($cookie_name, '', 0, '/', '', 0);//reset user cookie
setcookie('webfxtab_tabPane1', '', 0, '/', '', 0);//reset general stats tab pane cookie cookie
$sql->db_Delete("user_cart","status = 0 AND uid = ".ID);
header("Location: ".URL);
exit;
}//END OF IF
############################## END LOGOUT ####################################################

$sort_fields = array(
	"sku" => $lang['sku'],
	"title"	=> $lang['title'],
	"category" => $lang['category'],
	"name" => $lang['location'],
	"date_added" => $lang['date_added']
);

if (TRACKING_STATISTICS == 1) 
{ 
include("includes/tracking_stats.php");
}//END OF IF

########################### BEGIN SMARTY ASSIGMENTS ###########################################
if (defined("LOGINMESSAGE")){$smarty->assign("LOGINMESSAGE",LOGINMESSAGE);}//END OF IF


############################# LOAD ACTIVE MODULES ###############################################
	 $loaded_modules = array();
	 global $loaded_modules,$site_modules,$loaded_modules_array;
	 $modules_list = load_modules(1,0);
############################# END LOAD ACTIVE MODULES ###########################################
########################### LOAD MODULES ###################################
foreach ($modules_list as $m)
{
	if ($m['startup'] == 1) 
	{
		if ($loaded_modules_array[] =  module_is_active($m['name'],1,1))
		{		
			$smarty->assign($m['name'],$loaded_modules[$m['name']]);
		}
	}
}

$Content = new Items(array('module'=>$loaded_modules['content'],'debug'=>0));
$menu = new menuEditor();
$smarty->assign('TopMenu',$menu->getMenu(1,array('returnItems'=>1)));
################################### CART STUFF ###############################################
//unset($_SESSION['cart']);
//GET TOTALS
if (is_array($_SESSION['cart']) AND !empty($_SESSION['cart'])) {
foreach ($_SESSION['cart'] as $k=>$v)
{
	$total_quantities[] = $v['quantity'];
	$total_prices[] = $v['total'];
}//END TOTALS
$e= new eshop();
$smarty->assign("total_quantities",array_sum($total_quantities));
$smarty->assign("total_price",$e->getVatPriceOf($total_prices));

} elseif (is_numeric(ID)) {
	
	$e= new eshop();
	$res=$e->doSessionToCart(ID);
	$smarty->assign("total_quantities",$res['total_quantity']);
	$smarty->assign("total_price",$e->getVatPriceOf($res['total_price']));
	
}
################################## END CART STUFF ##############################################
############################# END LOAD ACTIVE MODULES ###########################################
	if (preg_match('/home.local/',$_SERVER['HTTP_HOST'])) {
		$smarty->assign("LOCAL",1);
	}
$validation_arr = array( rand(1,10), rand(1,10));
$smarty->assign("validation",$validation_arr);
$smarty->assign("validation_sum",array_sum($validation_arr));
$smarty->assign("availableLanguages",$Languages->AvailableLanguages(array('active'=>'Y','findDefault'=>'default_lang','replaceURL'=>e_REQUEST_URI)));
$smarty->assign("FRONT_LANG",FRONT_LANG);//assigned template variable FRONT_LANG
$smarty->assign("QUERY_STRING",capture_links($_GET));//assigned template variable query_string
$smarty->assign("PHP_SELF",$_SERVER['PHP_SELF']);
$smarty->assign("e_REQUEST_URI",e_REQUEST_URI);
$smarty->assign("LanguageDetails",$country);
$smarty->assign("QUERY",e_QUERY);
$smarty->assign("SELF",e_SELF);
$smarty->assign("e_FILE",e_FILE);
########################### END SMARTY ASSIGMENTS ###########################################

/* PLUGINS */
include_once(ABSPATH."/plugins/index.php");
if (!empty($plugins)) {
	foreach ($plugins as $k=>$v) {
		if (in_array("F", $v['area']) ) {
			include_once(ABSPATH."/plugins/".$v['path']);
		}
	}
}

HookParent::getInstance()->doTriggerHook('global', "afterSystemLoad");
########################SMATY DEBUG###############################
$smarty->debugging = false;
$smarty->debug_tpl = 'test/debug.tpl';
########################SMATY DEBUG###############################
?>
