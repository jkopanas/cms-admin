<?php
define("ABSPATH", dirname(__FILE__));
define("ENCODED_TYPE", 0);
define("AJAX_URL", "http://mcms.net-tomorrow.com/ajax/loader.php?file=updater/update.php&action=fetchmcms&version=max&type=".(ENCODED_TYPE+1));
$jsonOutput = array(
	"error" 	=> true,
	"message"	=> "1000: Done!",
	"success"	=> false,
);

if($_POST['action'] == "fetchArchive")
{
	//---fetch Update
	//$remoteFP 	= @fopen(AJAX_URL, "rb");
	//$localFP 		= @fopen(ABSPATH.DIRECTORY_SEPARATOR."mcms.zip", "ab");
	$booleanSwitch 	= false;
	$bCount=0;
	/*********************/
	$url  = AJAX_URL;
	$path = ABSPATH.DIRECTORY_SEPARATOR."mcms.zip";
	$fp = fopen($path, 'wb');
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_FILE, $fp);
	$data = curl_exec($ch);
	curl_close($ch);
	fclose($fp);
	/*********************/
	/********************* /
	if($remoteFP && $localFP)
	{
		while (!feof($remoteFP))
		{
			$bCount += fwrite($localFP, fread($remoteFP, 512));
		}
	}
	else
	{
		$booleanSwitch = true;
	}
	
	@fclose($remoteFP);
	@fclose($localFP);
	/*********************/
	//---/fetch Update
	
	if(!is_resource(ABSPATH.DIRECTORY_SEPARATOR."mcms.zip") && $booleanSwitch)
	{
		$jsonOutput['message'] = "1001: Unable to fetch installation Files";
		
		$jsonOutput["success"] = !$jsonOutput['error']; //Boolean Trick
		echo json_encode($jsonOutput);
		exit();
	}
	
	//---Responding
	$jsonOutput['error'] 	= $booleanSwitch;
	$jsonOutput["success"] 	= !$jsonOutput['error']; //Boolean Trick
	echo json_encode($jsonOutput);
	exit();
}

if($_POST['action'] == "unzipArchive")
{
	//---Unzip
	$zip = new ZipArchive();
	$fCount=0;
	$booleanSwitch = false;
	
	if(($res=$zip->open(ABSPATH.DIRECTORY_SEPARATOR."mcms.zip")) === true)
	{
		for($i = 0; $i < $zip->numFiles; $i++)
		{
			if($zip->extractTo(ABSPATH.DIRECTORY_SEPARATOR,
				$zip->getNameIndex($i)
			))
			{
				$fCount++;
			}
		}
		$zip->close();
	}
	//---/Unzip
	
	if($fCount==0)
	{
		$jsonOutput['message'] 	= "1002: Unable to install Files, check user Rights";
		$jsonOutput['error'] 	= true;
		$jsonOutput["success"] 	= !$jsonOutput['error']; //Boolean Trick
		echo json_encode($jsonOutput);
		exit();
	}

	//---Responding
	$jsonOutput['error'] 	= false;
	$jsonOutput["success"] 	= !$jsonOutput['error']; //Boolean Trick
	echo json_encode($jsonOutput);
	exit();
}

if($_POST['action'] == "configMCMS")
{
	//---Config File
	$booleanSwitch 	= false;
	
	$configFile = "<?php
		//Database definitions
		define(\"MPREFIX\",\"\");
		define(\"DBHOST\",\"".$_POST['dbHost']."\");
		define(\"DBUSER\",\"".$_POST['dbUser']."\");
		define(\"DBPASS\",\"".$_POST['dbPass']."\");
		define(\"DB\",\"".$_POST['dbName']."\");
		define(\"SKIN_PATH\",::DOLLAR::_SERVER['DOCUMENT_ROOT'].\"/skin\");
		define(\"SKIN_URL\",\"/skin\");
		define(\"TEMPLATES_PATH\",::DOLLAR::_SERVER['DOCUMENT_ROOT'].\"/templates_c\");
		define(\"DEFAULT_SORT\",\"sku\");
		define(\"USE_SMARTY_CAHCHING\",0);
		
		if (class_exists('Memcache',false)) {
		//	define('USE_CACHE',1); //ENABLE CACHING
			define('TTL',time()+3600*24*30);
			::DOLLAR::memcache = new Memcache;
		    ::DOLLAR::memcache->connect(\"localhost\",11211); # You might need to set \"localhost\" to \"127.0.0.1\"
		}
		
		#
		# Select the sessions mechanism:
		# 1 - PHP sessions data is stored on the file system
		# 2 - PHP sessions data is stored on the MySQL database
		# 3 - internal sessions mechanism is used (highly recommended)
		::DOLLAR::useSESSIONs_type = 3;
		::DOLLAR::SESSION_NAME = \"xid\";//Set the session name here
		date_default_timezone_set('Europe/Athens');
		/* ACTIVE PLUGINS */
		::DOLLAR::plugins = array( );
		/* END PLUGINS */
		?>";
	// Bug fix - Method to avoid dummy variable invocation
	$configFile = str_replace("::DOLLAR::","$",$configFile);
	
	$booleanSwitch = (@file_put_contents("./includes/config.php",$configFile)>0? false: true);
	//---/Config File
	
	if($booleanSwitch)
	{
		$jsonOutput['message'] 	= "1003: Unable to save config File, check user Rights";
		$jsonOutput['error'] 	= true;
		$jsonOutput["success"] 	= !$jsonOutput['error']; //Boolean Trick
		echo json_encode($jsonOutput);
		exit();
	}

	//---Responding
	$jsonOutput['error'] 	= $booleanSwitch;
	$jsonOutput["success"] 	= !$jsonOutput['error']; //Boolean Trick
	echo json_encode($jsonOutput);
	exit();
}

if($_POST['action'] == "executeDatabase")
{
	//---DB Creation
	//include_once('init.php');
	//$sqlFile = ABSPATH.DIRECTORY_SEPARATOR."dbdump.sql";
	$sqlFile = ABSPATH.DIRECTORY_SEPARATOR."databaseDump.sql";
	
	$sql = file_get_contents($sqlFile);
	$booleanSwitch=true;
	
	$pdo = new PDO(
	    'mysql:host='.$_POST['dbHost'].';dbname='.$_POST['dbName'], 
	    $_POST['dbUser'], 
	    $_POST['dbPass']
	);
	$booleanSwitch = $booleanSwitch && ($pdo->prepare($sql)->execute()!==false?true:false);
	
	$enc = rand(1,255); # generate random salt.
	$result = "S".chr(100 + ($enc & 240) / 16).chr(100 + ($enc & 15)); # include salt in the result;
	$enc ^= 85;
	for ($i = 0; $i < strlen($_POST['adminPass']); $i++) 
	{
		$r = ord(substr($_POST['adminPass'], $i, 1)) ^ $enc++;
		if ($enc > 255)
		{
			$enc = 0;
		}
		$result .= chr(100 + ($r & 240) / 16).chr(100 + ($r & 15));
	}
	$_POST['adminPass'] = $result;
	// Boolean tricks - On Failure, as domino Everyone Fails. To Success everyone must secced.
	$pdo->prepare("INSERT INTO `users` (`uname`, `pass`, `user_sess`, `email`, `user_name`, `user_surname`, `user_folder`, `user_mobile`, `user_location`, `user_timezone`, `user_join`, `user_lastvisit`, `user_currentvisit`, `user_prefs`, `admin`, `user_login`, `user_class`, `user_perms`, `featured`, `settings`, `user_type`) VALUES
	('".$_POST['adminUName']."', '".$_POST['adminPass']."', NULL, ''".$_POST['adminEmail']."'', ''".$_POST['adminFName']."'', ''".$_POST['adminLName']."'', NULL, NULL, NULL, NULL, NULL, 1341481240, 1341481240, NULL, 1, 1, 'SA', NULL, NULL, NULL, NULL)")->execute();
	$pdo->prepare("UPDATE config set value = '".$_POST['mcmsHost']."/manage/' WHERE name = 'admin_url'")->execute();
	$pdo->prepare("UPDATE config set value = '".$_POST['mcmsHost']."/manage/' WHERE name = 'admin_url'")->execute();
	$pdo->prepare("UPDATE config set value = '".$_POST['mcmsHost']."/' WHERE name = 'url'")->execute();
	$pdo->prepare("UPDATE config set value = '".$_POST['mcmsHost']."/catalog' WHERE name = 'catalog_url'")->execute();
	$pdo->prepare("UPDATE config set value = '".$_POST['mcmsTitle']."' WHERE name = 'site_name'")->execute();
	$pdo->prepare("UPDATE config set value = '".ENCODED_TYPE."' WHERE name = 'encoded_type'")->execute();
	//---DB Creation
	
	if(!$booleanSwitch)
	{
		$jsonOutput['message'] 	= "1004: Unable to create Database, check DB credentials";
		$jsonOutput['error'] 	= true;
		$jsonOutput["success"]	 = !$jsonOutput['error']; //Boolean Trick
		echo json_encode($jsonOutput);
		exit();
	}

	//---Responding
	$jsonOutput['error'] 	= false;
	$jsonOutput["success"] 	= !$jsonOutput['error']; //Boolean Trick
	echo json_encode($jsonOutput);
	exit();
}

if(strlen($_POST['action'])>0)
{
	//---Responding
	$jsonOutput['message'] 	= "1005: Uknown Request";
	$jsonOutput['error'] 	= true;
	$jsonOutput["success"] 	= !$jsonOutput['error']; //Boolean Trick
	echo json_encode($jsonOutput);
	exit();
}

?>

<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>MCMS - Installer</title>
    <link rel="shortcut icon" href="favicon.ico">
	
    <link rel="stylesheet" href="http://mcms.net-tomorrow.com/skin/css/admin2/style.css" media="all" type="text/css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans" type="text/css">
	<link href="http://mcms.net-tomorrow.com/scripts/kendo/styles/kendo.common.min.css" rel="stylesheet">
	<link href="http://mcms.net-tomorrow.com/scripts/kendo/styles/kendo.metro.min.css" rel="stylesheet">
	<link href="http://mcms.net-tomorrow.com/scripts/multiSelect/chosen.css" rel="stylesheet">
	<script type="text/javascript" src="http://mcms.net-tomorrow.com/scripts/head.load.min.js"></script>
	<script type="text/javascript" src="http://mcms.net-tomorrow.com/scripts/jquery-1.7.1.min.js"></script>

	<script type="text/javascript">
		function showPreLoadingScreen(loadingText,onViewCallBack) {
			removePreLoadingScreen();
			document.write( " " +
				"<div id=\"preLoad\" style=\"width:100%;height:100%;background:#FFF;opacity:0.4;-moz-opacity:0.4;-khtml-opacity:0.4;filter:alpha(opacity=40);position:fixed;top:0;z-index: 10000;\">"+
				    "<div style=\"position:fixed;top: 50%;left: 50%;height:60px;opacity:1;-moz-opacity:1;-khtml-opacity:1;filter:alpha(opacity=100);margin-top: -30px;margin-left: -24px;\">"+
				    	"<img id=\"logo\" src=\"http://beta.net-tomorrow.com/images/site/logo-1.png\" alt=\"logo\" title=\logo\" style=\"position:absolute;top: -60px;left: -75px;width:200px;height:60px;opacity:1;filter:alpha(opacity=100);-moz-opacity:1;-khtml-opacity:1;\" >"+
				        "<img src=\"http://mcms.net-tomorrow.com/scripts/kendo/styles/Metro/loading-image.gif\" alt=\"Loading Image\" style=\"width:48px;\" />"+
				        "<br />"+loadingText+
				    "</div>"+
				"</div>" );
			try {
				return onViewCallBack();
			}
			catch(e) {
	
			}
			return $("#preLoad");
		}
		function removePreLoadingScreen(onRemoveCallback) {
			$("#preLoad")
				.css("filter","alpha(opacity=0)")
				.css("opacity","0")
				.css("-moz-opacity","0")
				.css("-khtml-opacity","0")
				.css("visibility","hidden")
				.css("display","none")
				.css("z-index","-1")
				.remove();
			try {
				return onRemoveCallback();
			}
			catch(e) {
	
			}
			return true;
		}
		showPreLoadingScreen("Loading...");
		
		var mcms;
		head.js('http://mcms.net-tomorrow.com/scripts/kendo/js/kendo.web.min.js');
		head.js('http://mcms.net-tomorrow.com/scripts/ckeditor/ckeditor.js');
		head.js('http://mcms.net-tomorrow.com/scripts/ckeditor/adapters/jquery.js');
		head.js('http://mcms.net-tomorrow.com/scripts/admin2/mcms.js');
		head.js('http://mcms.net-tomorrow.com/scripts/multiSelect/chosen.jquery.min.js');
		
		head.ready(function() {
			mcms =  $(document).mCms({ lang:$('input[name=loaded_language]').val(),module:$('input[name=module]').val()}).data().mcms;//Initialize
			$.lang = mcms.getLang();
		});
		head.ready(function(){
			var db = new mcms.dataStore({
			     method: "POST",
			     async:false,
			     url:"",
			     dataType: "json",
			     trigger:"loadComplete"
			});
			var validator = {};
			var installerController = {};
			var installerWindow = $("<div />").kendoWindow({
			    title: "Mcms Installer",
			    resizable: false,
			    modal: true,
			    draggable: false,
			    actions: ["Refresh", "Close"],
			    width: "600px",
			    height: "550px",
			    maxWidth: "700px",
			    maxHeight: "600px",
			    minHeight: "400px",
			    minWidth: "500px",
			    open: onOpen,
                close: onClose,
                refresh: onRefresh
			});
			var installerModal = installerWindow.data("kendoWindow");//.center().open();
			
			var installerViewModel = kendo.observable({
				contentVal: "",
				dbName: function(e) {
					console.log(e);
				},
				dbHost: "",
				dbUser: "",
				dbPass: "",
				adminFName: "",
				adminLName: "",
				adminEmail: "",
				adminUName: "",
				adminPass: "",
				mcmsHost: window.location.origin+"/",
				mcmsEmail: "",
				mcmsValidation: "",
				mcmsTitle: "",
				concatThis: function(e) {
					this.set(e.target.name,e.target.value);
				},
				loadVal : function(e) {
					for (x in this) {
						e.find("[name="+x+"]").val(this.get(x));
					}
				},
				init: function() {
					return this;
				},
				doNext: function() {
					installerController.doNext();
					return this;
				},
				doCancel: function(e) {
					onClose(e);
					return this;
				},
				doBack: function(e) {
					installerController.doRollBack(1);
					return this;
				}
			});
			
			var installerTemplate = kendo.template( $("#modalTemplate").html() );
			installerModal.content(installerTemplate([]));
			kendo.bind(installerWindow, installerViewModel);
			installerViewModel.init();
			installerController = progressBoxesFactory($("#progressTabs"),{
				size: 5,
				actions: [firstPage,secondPage,checkDB,mcmsCredentials,mcmsValidation,mcmsInstallation],
				messages: ["Welcome","Database credentials","MCMS credentials","MCMS validation","MCMS instalation"]
			});
			installerModal.center().open();
			validator = $("#main").kendoValidator().data("kendoValidator");
			
			function firstPage(e) {
				$(".installerModal").children("#main").animate({opacity: 0.0},250,function(event) {
					$(".installerModal")
						.children("#main")
						.html(kendo.template( $("#firstTemplate").html() )([]))
						.animate({opacity:1},250);
					$(".installerModal").children("#main").find("input").first().focus();
	        	});
				installerViewModel.loadVal($(".installerModal").children("#main"));
				return true;
			}
			function secondPage(e) {
				$(".installerModal").children("#main").animate({opacity: 0.0},550,function(event) {
					$(".installerModal")
						.children("#main")
						.html(kendo.template( $("#secondTemplate").html() )([]))
						.animate({opacity:1},250);
					$(".installerModal").children("#main").find("input").first().focus();
					installerViewModel.loadVal($(".installerModal").children("#main"));
	        	});
				return true;
			}
			function checkDB(e) {
				if (validator.validate() || e.isRollBack) {
					$(".installerModal").children("#main").animate({opacity: 0.0},550,function(event) {
						$(".installerModal")
							.children("#main")
							.html(kendo.template( $("#thirdTemplate").html() )([]))
							.animate({opacity:1},250);
						$(".installerModal").children("#main").find("input").first().focus();
						installerViewModel.loadVal($(".installerModal").children("#main"));
		        	});
		        	return true;
				}
	        	return false;
			}
			function mcmsCredentials(e) {
				if (validator.validate() || e.isRollBack) {
					$(".installerModal").children("#main").animate({opacity: 0.0},550,function(event) {
						$(".installerModal")
							.children("#main")
							.html(kendo.template( $("#fourthTemplate").html() )([]))
							.animate({opacity:1},250);
						$(".installerModal").children("#main").find("input").first().focus();
						installerViewModel.loadVal($(".installerModal").children("#main"));
		        	});
		        	return true;
				}
	        	return false;
			}
			function mcmsValidation(e) {
				if (validator.validate() || e.isRollBack) {
					$(".installerModal").children("#main").animate({opacity: 0.0},550,function(event) {
						$(".installerModal")
							.children("#main")
							.html(kendo.template( $("#fifthTemplate").html() )([]))
							.animate({opacity:1},250);
						$("#installationProcessIcon").html("<img src=\"http://mcms.net-tomorrow.com/images/ajax-loader.gif\" title=installing alt=installing />");
						$("#installationProcessText").html("installing");
		        	});
					$("#nextBtn").attr("disabled","disabled");
					$("#backBtn").attr("disabled","disabled");
					setTimeout(function() {
						installerController.doNext();
					}, 1500);
					
		        	return true;
				}
	        	return false;
			}
			function mcmsInstallation(e) {
				var postData = {
					action:"fetchArchive",
					dbName: installerViewModel.get('dbName'),
					dbHost: installerViewModel.get('dbHost'),
					dbUser: installerViewModel.get('dbUser'),
					dbPass: installerViewModel.get('dbPass'),
					adminFName: installerViewModel.get('adminFName'),
					adminLName: installerViewModel.get('adminLName'),
					adminEmail: installerViewModel.get('adminEmail'),
					mcmsHost: installerViewModel.get('mcmsHost'),
					mcmsEmail: installerViewModel.get('mcmsEmail'),
					mcmsValidation: installerViewModel.get('mcmsValidation'),
					mcmsTitle: installerViewModel.get('mcmsTitle')
				};
				db.data = postData;
				$(db).bind('fetchArchive',function(){
					console.log("fetchArchive");
					$("#installationProcessText").html("installing: Fetching Files");
					db.data = postData;
					if(db.records.success) {
						setTimeout(function() {
							db.data.action = "unzipArchive";
							db.trigger = "unzipArchive";
							$(db).loadStore(db);
						}, 800);
					}
					else {
						$("#installationProcessIcon").html("<img src=\"http://mcms.net-tomorrow.com/images/icons/box-error.png\" title=Error alt=Error />");
						$("#installationProcessText").html("Failure: "+ db.records.message);
						showConfirmationModal("Contact Support?\n<br/>Installation Failure: "+ db.records.message,function() {
							setTimeout(function() {
								installerModal.destroy(); 
								setTimeout(function() { window.location.href = "http://net-tomorrow.com";  }, 1000);
							}, 1500);
						});
					}
				});
				$(db).bind('unzipArchive',function(){
					console.log("unzipArchive");
					$("#installationProcessText").html("installing: Installing Files");
					db.data = postData;
					if(db.records.success) {
						setTimeout(function() {
							db.data.action = "configMCMS";
							db.trigger = "configMCMS";
							$(db).loadStore(db);
						}, 800);
					}
					else {
						$("#installationProcessIcon").html("<img src=\"http://mcms.net-tomorrow.com/images/icons/box-error.png\" title=Error alt=Error />");
						$("#installationProcessText").html("Failure: "+ db.records.message);
						showConfirmationModal("Contact Support?\n<br/>Installation Failure: "+ db.records.message,function() {
							setTimeout(function() {
								installerModal.destroy(); 
								setTimeout(function() { window.location.href = "http://net-tomorrow.com";  }, 1000);
							}, 1500);
						});
					}
				});
				$(db).bind('configMCMS',function(){
					console.log("configMCMS");
					$("#installationProcessText").html("installing: Configuring Files");
					if(db.records.success) {
						setTimeout(function() {
							db.data.action = "executeDatabase";
							db.trigger = "executeDatabase";
							$(db).loadStore(db);
						}, 800);
					}
					else {
						$("#installationProcessIcon").html("<img src=\"http://mcms.net-tomorrow.com/images/icons/box-error.png\" title=Error alt=Error />");
						$("#installationProcessText").html("Failure: "+ db.records.message);
						showConfirmationModal("Contact Support?\n<br/>Installation Failure: "+ db.records.message,function() {
							setTimeout(function() {
								installerModal.destroy(); 
								setTimeout(function() { window.location.href = "http://net-tomorrow.com";  }, 1000);
							}, 1500);
						});
					}
				});
				$(db).bind('executeDatabase',function(){
					console.log("executeDatabase");
					$("#installationProcessText").html("installing: Installing Database");
					if(db.records.success) {
						setTimeout(function() {
							db.data.action = "installationSuccess";
							db.trigger = "installationSuccess";
							$(db).loadStore(db);
						}, 800);
					}
					else {
						$("#installationProcessIcon").html("<img src=\"http://mcms.net-tomorrow.com/images/icons/box-error.png\" title=Error alt=Error />");
						$("#installationProcessText").html("Failure: "+ db.records.message);
						showConfirmationModal("Contact Support?\n<br/>Installation Failure: "+ db.records.message,function() {
							setTimeout(function() {
								installerModal.destroy(); 
								setTimeout(function() { window.location.href = "http://net-tomorrow.com";  }, 1000);
							}, 1500);
						});
					}
				});
				$(db).bind('installationError',function(){
					console.log("installationError");
					setTimeout(function() { installerController.doNext(); }, 2000);
				});
				$(db).bind('installationSuccess',function(){
					console.log("installationSuccess");
					$("#installationProcessIcon").html("<img src=\"http://mcms.net-tomorrow.com/images/icons/icon-tick.png\" title=Success alt=Success />");
					$("#installationProcessText").html("Done!");
					setTimeout(function() { 
						installerModal.destroy();
						setTimeout(function() { window.location.href = installerViewModel.mcmsHost; }, 1000);
					}, 1500);
				});
				db.trigger = "fetchArchive";
				$(db).loadStore(db);
			}
			function onClose(e) {
		    	e.preventDefault();
		    	showConfirmationModal("You are about to Cancel Instalation Process, continue?", function() {
		    		setTimeout(function() {
						installerModal.destroy(); 
						setTimeout(function() { window.location.href = "http://net-tomorrow.com";  }, 1000);
					}, 1);
		    	});
		    	return false;
		    }
			function onOpen(e) {
				
            }
			function onRefresh(e) {
				e.preventDefault();
				showPreLoadingScreen("Refreshing...");
            }
			function progressBoxesFactory(target,settings) {
				settings = (settings?settings:{});
				var progressBoxes = {
					index: 0,
					isRollBack: false,
					pBoxMsg: {},
					pBoxes: {},
					set: {
						size: (settings.size?settings.size:3),
						actions: (settings.actions?settings.actions:[]),
						messages: (settings.messages?settings.messages:function(){
							var tmpArr = [];
							for (i=0; i<this.size; i++) { tmpArr[i] = "Progress Index" };
							return tmpArr;
						}),
						activeClass: (settings.activeClass?settings.activeClass:"activePTab"),
						inActiveClass: (settings.inActiveClass?settings.inActiveClass:"inActivePTab"),
						mainWrapBefore: (settings.mainWrapBefore?settings.mainWrapBefore:"<ul>"),
						mainWrapAfter: (settings.mainWrapAfter?settings.mainWrapAfter:"</ul>"),
						innerWrapBefore: (settings.innerWrapBefore?settings.innerWrapBefore:"<li>"),
						innernWrapAfter: (settings.innernWrapAfter?settings.innernWrapAfter:"</li>"),
						onBeforeDraw: (settings.onBeforeDraw?settings.onBeforeDraw:function(){}),
						onAfterDraw: (settings.onAfterDraw?settings.onAfterDraw:function(){}),
						innerWidth: (settings.innerWidth?settings.innerWidth:70),
						innerHeight: (settings.innerHeight?settings.innerHeight:62),
						onNextCallback: (settings.onNextCallback?settings.onNextCallback:function(){}),
						onRollbackCallback: (settings.onRollbackCallback?settings.onRollbackCallback:function(){})
					},
					init: function() {
						target.append("<div class=\"pBoxMsg\" ></div> ");
						target.append("<div class=\"pBoxesWrapper\" ></div> ");
						pBoxMsg = target.children(".pBoxMsg");
						pBoxMsg.css("clear","both")
							.css("position","relative")
							.css("float","left")
							.css("width","100%")
							.css("height","20px")
							.css("display","inline-block");
						pBoxes = target.children(".pBoxesWrapper");
						pBoxes.css("clear","both")
							.css("position","relative")
							.css("float","left")
							.css("width","100%")
							.css("display","inline-block");
						var tmpHTML = "";
						tmpHTML += this.set.mainWrapBefore;
						for (i=0; i<this.set.size; i++) { tmpHTML += this.set.innerWrapBefore+"<span>"+(i+1)+"</span>"+this.set.innernWrapAfter };
						tmpHTML += this.set.mainWrapAfter;
						pBoxes.append(tmpHTML);
						pBoxes = pBoxes.children().first();
						pBoxes.addClass("pBoxes");
						pBoxes.css("width",(this.set.innerWidth * this.set.size)+"px")
							.css("height",this.set.innerHeight+"px")
							.css("clear","both")
							.css("position","relative")
							.css("float","left")
							.css("margin","auto")
							.css("display","inline-block");
						var tmpSuper = this;
						$.each(pBoxes.children(), function(i, val) {
							$(this).width(tmpSuper.set.innerWidth)
								.height(tmpSuper.set.innerHeight)
								.css("position","relative")
								.css("float","left")
								.css("display","inline-block")
								.addClass("pBox")
								.addClass(tmpSuper.set.inActiveClass);
					    });
						pBoxes.children().first().addClass("firstPBox").addClass("currentPBox");
						pBoxes.children().last().addClass("lastPBox");
						return this;
					},
					doNext: function() {
						var continueIndex=false;
						this.isRollBack=false;
						if(this.index<=this.set.size) {
							try {
								continueIndex = (this.set.actions[this.index](this)? true:false);
							}
							catch(e) {
							}
							try {
								this.set.onNextCallback(this);
							}
							catch(e) {
							}
						}
						if(continueIndex)
						{
							pBoxes
								.children(":nth-child("+(this.index+1)+")")
								.addClass("currentPBox");
							pBoxes
								.children(":nth-child("+this.index+")")
								.addClass(this.set.activeClass)
								.removeClass("currentPBox")
								.removeClass(this.set.inActiveClass);
							this.index++;
						}
						return this;
					},
					doRollBack: function(steps) {
						this.isRollBack=true;
						if((this.index-steps)>0) {
							try {
								this.set.actions[this.index-steps-1](this);
							}
							catch(e) {
							}
							try {
								this.set.onNextCallback(this);
							}
							catch(e) {
							}
							pBoxes
								.children(":nth-child("+(this.index-steps)+")")
								.addClass("currentPBox");
							pBoxes
								.children(":nth-child("+(this.index)+")")
								.addClass(this.set.activeClass)
								.removeClass("currentPBox")
								.removeClass(this.set.inActiveClass);
							this.index -= steps;
						}
						return this;
					}
				};
                return progressBoxes.init().doNext();
            }
			function showConfirmationModal(confirmationText,confirmationCallback) {
				var confirmationModal = $("<div />").kendoWindow({
				    title: "Confirm",
				    resizable: false,
				    modal: true
				});

				confirmationModal.data("kendoWindow")
					.content("<p class=\"confirmationMessage\" style=\"margin: 5px;\">" + confirmationText+" </p>"
							+"<center> <button class=\"confirmationConfirmBtn k-button\" >Yes</button>"
							+"<button class=\"confirmationCancelBtn k-button\">No</button></center>")
					.center().open();
				
				confirmationModal.find(".confirmationConfirmBtn,.confirmationCancelBtn")
					.click(function() {
					    if ($(this).hasClass("confirmationConfirmBtn")) {
					    	confirmationCallback();
					    }
				    	confirmationModal.data("kendoWindow").close().destroy();
				}).end();
			}
			$(".k-link").delegate(".k-refresh","click",function(e) { 
				showConfirmationModal("You are about to reload and re-enter your data. continue?",function() {
					location.reload(true);
				});
			});
		});
	</script>
	<style type="text/css">
		#progressTabs {
			position: relative;
			height: 100px;
			clear: both;
			float: left;
			margin: auto;
		}
		#main {
			position: relative;
			height: 300px;
			clear: both;
			float: left;
			padding: 15px;
			margin: auto;
		}
		#bottomBtnWrapper {
			position: relative;
			height: 25px;
			width: 240px;
			margin-left: 340px;
			float: left;
			margin-top: 50px;
		}
		#bottomBtnWrapper button {
			position: relative;
			float: left;
			margin: 10px;
		}
		.installerModalContent ul li {
			padding: 8px 20px;
			float: left;
			width: 485px;
			position: relative;
		}
		.installerModalContent ul li input {
			padding: 5px;
			float: right;
			position: relative;
		}
		.pBox span {
			position: relative;
			margin-left: 24px;
			margin-top: 17px;
			display: block;
			vertical-align: middle;
			text-align: center;
			width: 30px;
			height: 30px;
			font-size: 25px;
		}
		.pBox.firstPBox span {
			margin-left: 15px;
		}
		.pBox.inActivePTab {
			background: url('http://mcms.net-tomorrow.com/images/pBoxes/pBoxU.png') no-repeat;
		}
		.pBox.inActivePTab.firstPBox {
			background: url('http://mcms.net-tomorrow.com/images/pBoxes/pBoxLU.png') no-repeat;
		}
		.pBox.inActivePTab.lastPBox {
			background: url('http://mcms.net-tomorrow.com/images/pBoxes/pBoxRU.png') no-repeat;
		}
		.pBox.activePTab {
			background: url('http://mcms.net-tomorrow.com/images/pBoxes/pBoxC.png') no-repeat;
		}
		.pBox.activePTab.firstPBox {
			background: url('http://mcms.net-tomorrow.com/images/pBoxes/pBoxLC.png') no-repeat;
		}
		.pBox.activePTab.lastPBox {
			background: url('http://mcms.net-tomorrow.com/images/pBoxes/pBoxRC.png') no-repeat;
		}
		.pBox.currentPBox.firstPBox {
			background: url('http://mcms.net-tomorrow.com/images/pBoxes/pBoxLCUR.png') no-repeat;
		}
		.pBox.currentPBox.lastPBox {
			background: url('http://mcms.net-tomorrow.com/images/pBoxes/pBoxRCUR.png') no-repeat;
		}
		.pBox.currentPBox {
			background: url('http://mcms.net-tomorrow.com/images/pBoxes/pBoxCUR.png') no-repeat;
		}
	</style>
</head>
<body class="bg_c stD  fluid ">
	<input type="hidden" name="loaded_language" value="en">
	<input type="hidden" name="current_module" value="">
	<input type="hidden" name="current_module_name" value="">
	<input type="hidden" name="module" value="">
	<script id="modalTemplate" type="text/kendo-ui-template">
		<div class="installerModal" >
			<div id="progressTabs" > </div>
			<div id="main" data-bind="html: contentVal, events: { keyup: concatThis }" > </div>
			<div id="bottomBtnWrapper" >
				<button id="cancelBtn" class="k-button"data-value-update="keyup" data-bind="click: doCancel">Cancel</button>
				<button id="backBtn" class="btn btn_d" data-bind="click: doBack">Back</button>
				<button id="nextBtn" class="btn btn_d"data-value-update="keyup" data-bind="click: doNext">Next</button>
			</div>
		</div>
	</script>
	<script id="firstTemplate" type="text/kendo-ui-template">
		<div class="installerModalContent" >
			<h3 > Mind to check the followings before you start the installation process </h3>
			<ul >
				<li ><label ><span>You should have set the php-user rights to 775 for files & 664 for directories at the installing directory</span><label></li>
				<li ><label ><span>You possest your DataBase's credentials</span><label></li>
				<li ><label ><span>You possest a host url, an administration e-mail, and your provided Admin credentials</span><label></li>
				<li ><label ><span>You are an already registered customer of net-tomorrow</span><label></li>
				<li ><label ><span>You can't wait to optimize your web business! :)</span><label></li>
			</ul>
		</div>
	</script>
	<script id="secondTemplate" type="text/kendo-ui-template">
		<div class="installerModalContent" >
			<h3 >Database credentials</h3>
			<ul >
				<li ><label ><span>Database name</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: dbName" placeholder="eg eshop_db" name="dbName" required  validationMessage="The name of your Database" /><label></li>
				<li ><label ><span>Database Host</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: dbHost" placeholder="eg localhost" name="dbHost" required validationMessage="Database's host, should be localhost" /><label></li>
				<li ><label ><span>Root user</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: dbUser" placeholder="eg root" name="dbUser" required validationMessage="DB user-name" /><label></li>
				<li ><label ><span>Root Password</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: dbPass" placeholder="eg root" name="dbPass" /><label></li>
			</ul>
		</div>
	</script>
	<script id="thirdTemplate" type="text/kendo-ui-template">
		<div class="installerModalContent" >
			<h3 >MCMS credentials</h3>
			<ul >
				<li ><label ><span>First name</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: adminFName" placeholder="eg John" name="adminFName" required validationMessage="Webmaster's first name" /><label></li>
				<li ><label ><span>Last name</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: adminLName" placeholder="eg Smith" name="adminLName" required validationMessage="Webmaster's last name" /><label></li>
				<li ><label ><span>Admin's User name</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: adminUName" placeholder="eg admin" name="adminUName" required validationMessage="The User name is needed enter at the admin area" /><label></li>
				<li ><label ><span>Admin's Password</span><input type="password"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: adminPass" placeholder="eg p@$$w0rD77" name="adminPass" required validationMessage="The password is needed enter at the admin area" /><label></li>
				<li ><label ><span>Admin's email</span><input type="email" data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: adminEmail" placeholder="eg admin@website.com" name="adminEmail" required validationMessage="Enter the email of the webmaster or Administrator" /><label></li>
				<li ><label ><span>Mcms host (public URL)</span><input type="text" data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: mcmsHost" placeholder="eg www.myWebSite.com" name="mcmsHost" required pattern="(http[s]?:\/\/){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}((\/)|(\/[a-zA-Z0-9\.\/\-]+))*" validationMessage="Enter your business's URL eg http://www.myeshop.com/" /><label></li>
				<li ><label ><span>Mcms Title (public site title)</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: mcmsTitle" placeholder="eg My EShop" name="mcmsTitle" required validationMessage="Enter your business's name" /><label></li>
			</ul>
		</div>
	</script>
	<script id="fourthTemplate" type="text/kendo-ui-template">
		<div class="installerModalContent" >
			<h3 >MCMS validation</h3>
			<ul >
				<li ><label ><span>Registered Email</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: mcmsEmail"  placeholder="your customer registered email" name="mcmsEmail" required /><label></li>
				<li ><label ><span>Validation Key</span><input type="text"data-value-update="keyup" data-bind="events: { keyup: concatThis }, value: mcmsValidation"  placeholder="provided by net-tomorrow" name="mcmsValidation" required /><label></li>
			</ul>
		</div>
	</script>
	<script id="fifthTemplate" type="text/kendo-ui-template">
		<div class="installerModalContent" >
			<h3 >MCMS instalation</h3>
			<ul >
				<li ><label ><span>Status</span><div id="installationProcessIcon"></div><div id="installationProcessText"></div><label></li>
			</ul>
		</div>
	</script>
</body>
</html>