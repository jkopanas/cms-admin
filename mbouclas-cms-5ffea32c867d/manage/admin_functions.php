<?php
function getperms($arg, $ap = ADMINPERMS){
        if($ap == "0"){return TRUE;}
        if($ap == ""){return FALSE;}
        $ap = ".".$ap;
        if(preg_match("#\.".$arg."\.#", $ap)){
                return TRUE;
        } else {
                return FALSE;
        }
}//END OF FUNCTION

function read_images($path,$extentions)
{
	
if ($handle = opendir($path)) 
{ 
   while (false !== ($file = readdir($handle))) 
   {
   	for ($i = 0; $i < count($extentions); $i++)
{
if (eregi("\.". $extentions[$i] ."$", $file) && $file !="null.gif") 
{
	$filetime = (file_exists($path."/".$file)) ? filemtime($path."/".$file) : "";
  $files[] = array('file' => $file, 'time' => $filetime, 'path' =>$path."/".$file);
}//END OF IF
}//END OF FOR
   }//END OF WHILE
   closedir($handle); 
}//END OF IF 
if ($files) 
{  
rsort($files);
}//END OF IF
return $files;
}//END OF FUNCTION

function translate($table,$fields,$arg,$single=1) 
{ 
	$sql = new db();
	$sql->db_Select($table,$fields,$arg);
	if ($single == 1) 
	{  
	return execute_single($sql,1); 
	}//END OF IF
	else 
	{
		return execute_multi($sql,1);
	}
}//END OF FUNCTION translate

function add_extra_field_values($data,$productid,$lang) 
{ 
		$sql = new db();
		while(list($key,$val)=each($data)) 
		{
		if (strstr($key,"-")) 
		{
			list($field,$id)=split("-",$key);
			if ($val) 
			{  
			$query = "'$productid','$id','$val','$lang'";
			$sql->db_Insert("extra_field_values",$query);
			}//END OF IF not empty field
		}//END OF IF
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function upd_extra_field_values($data,$productid,$lang) 
{ 
		$sql = new db();
		$t = new textparse();
		foreach ($data as $key => $val)
		{
		if (strstr($key,"-")) 
		{
			$val = $t->formtpa($val);
			list($field,$id)=split("-",$key);
			$sql->db_Select("extra_field_values","productid","productid = '$productid' AND fieldid = '$id' AND code = '$lang'");
			if ($sql->db_Rows() > 0) 
			{ 
			$query = "value = '$val' WHERE productid = '$productid' AND fieldid = '$id' AND code = '$lang'";
			$sql->db_Update("extra_field_values",$query);		 
			}//END OF IF
			elseif ($sql->db_Rows() == 0 AND $val != "") 
			{  
			$sql->db_Insert("extra_field_values","'$productid','$id','$val,'$lang''"); 
			}//END OF ELSE
		}//END OF IF
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function add_link($source,$dest,$bydircetional) 
{ 
  $sql = new db();
  $sql->db_Select("product_links","*","product_source = '$source' AND product_dest = '$dest'");
  if ($sql->db_Rows() == 0) 
  {   
  $sql->db_Insert("product_links","'$source','$dest',''");
  if ($bydircetional == 1) 
  {  
  	$sql->db_Select("product_links","*","product_source = '$dest' AND product_dest = '$source'");
    if ($sql->db_Rows() == 0) 
  { 	
   $sql->db_Insert("product_links","'$dest','$source',''");
  }//END OF IF
  }//END OF IF
  }//END OF IF
}//END OF FUNCTION add_link

function category_modify($cat,$data) 
{ 
$sql = new db();
$sql2 = new db();
$t = new textparse();
$category = get_category($cat);
$b = format_category($category);
	$sql->db_Select("categories","categoryid_path,categoryid,category","categoryid_path LIKE '%".$category['categoryid']."%'");
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
		$r = $sql->db_Fetch();
		$tmp = explode("/",$r['category']);
		$tmp1 = explode("/",$r['categoryid_path']);
		//compare the string pieces to the original
		for ($j=0;count($tmp1) > $j;$j++)
		{
//			echo "category = '$title' WHERE categoryid = '".$r['categoryid']."'<br>";
			if ($tmp1[$j] == $cat) 
			{  
			 $finder = $j;
			}//END OF IF

			if ($tmp[$j] == $b[$finder]['cat']) 
			{  
			 	$tmp[$j] = $data['title'];
			 	$title1 = implode("/",$tmp);
			 	$sql2->db_Update("categories","category = '$title1' WHERE categoryid = '".$r['categoryid']."'");  
			}//END OF IF
		}//END OF FOR
		}//END OF FOR
}//END OF FUNCTION category_modify

function add_class($data) 
{ 
	$sql = new db();
	$t = new textparse();
	//insert the class to the database
	$sql->db_Insert("classes","'','".$t->formtpa($data['class'])."','".$t->formtpa($data['classtext'])."'"); 
	//process the class options
	$classid = mysql_insert_id();
	$tmp = explode("\n",$data['options']);
	for ($i=0;count($tmp) > $i;$i++)
	{
		$sql->db_Insert("class_options","'','".$classid."','".trim($t->formtpa($tmp[$i]))."','$i'");
	}//END OF FOR
}//END OF FUNCTION add_class

function update_class($classid) 
{ 
	$sql = new db();
	$t = new textparse();
	 
}//END OF FUNCTION update_class

function watermark($img,$watermark) 
{
$watermark = imagecreatefrompng($watermark);//create watermark canvas   
$watermark_width = imagesx($watermark);//watermark width  
$watermark_height = imagesy($watermark);//watermark height  
$image = imagecreatetruecolor($watermark_width, $watermark_height);//create image canvas  
$image = imagecreatefromjpeg($img);//new file  
$size = getimagesize($img);  
$dest_x = $size[0] - $watermark_width - 5;  
$dest_y = $size[1] - $watermark_height - 5;  
imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, 100);//merge the two images  
imagejpeg($image,$img,60);//save the new file  
imagedestroy($image);//empty the memory space from the new image  
imagedestroy($watermark);//empty the memory space from the watermark
}//END OF FUNCTION watermark

function resizeToFile ($sourcefile, $dest_x, $dest_y, $targetfile, $jpegqual,$proportions)
{
/*resizeToFile resizes a picture and writes it to the harddisk
*  
* $sourcefile = the filename of the picture that is going to be resized
* $dest_x   = X-Size of the target picture in pixels
* $dest_y   = Y-Size of the target picture in pixels
* $targetfile = The name under which the resized picture will be stored
* $jpegqual   = The Compression-Rate that is to be used 
*/

/* Get the dimensions of the source picture */
$picsize=getimagesize("$sourcefile");
$source_x  = $picsize[0];
$source_y  = $picsize[1];
$source_id = imageCreateFromJPEG("$sourcefile");

//constrain proportions algorithm
if ($proportions == 1) 
{  
$w = $dest_x;
$new_width = ($source_x * $dest_y) / $source_y;
if ($new_width > $w) {
$new_width = $w;
$dest_y = ($source_y * $new_width) / $source_x;
}
$dest_x = $new_width;
$dest_y = $dest_y;
}//END OF IF
/* Create a new image object (not neccessarily true colour) */
$target_id=imagecreatetruecolor($dest_x, $dest_y);
 /* Resize the original picture and copy it into the just created image
  object. Because of the lack of space I had to wrap the parameters to 
  several lines. I recommend putting them in one line in order keep your
  code clean and readable */
$target_pic=imagecopyresampled($target_id,$source_id,0,0,0,0,$dest_x,$dest_y,$source_x,$source_y);
/* Create a jpeg with the quality of "$jpegqual" out of the
  image object "$target_pic".
  This will be saved as $targetfile */
imagejpeg ($target_id,"$targetfile",$jpegqual);
return true;
}//END OF FUNCTION

function duplicate_language($code) 
{ 
	//create a duplicate of the default language using the new language code
	$sql = new db();
	$sql2 = new db();
	$sql->db_Select("languages","*","code = '".DEFAULT_LANG."'");
		for ($i=0;$sql->db_Rows() > $i;$i++)
	 	{
	 	$r = $sql->db_Fetch();
		$sql2->db_Insert("languages","'$code','".$r['descr']."','".$r['name']."','".$r['value']."','".$r['topic']."'");
	 	}//END OF FOR
	//now insert it into the countries table
	$r = country_details($code);
	$sql->db_Insert("countries","'$code','".$r['charset']."','N','".$r['country']."','".$r['locale']."',
					 '".$r['logo']."', '".$r['image']."'"); 
}//END OF FUNCTION duplicate_language

function get_all_countries() 
{ 
	$sql = new db();
	$sql->db_Select("country_codes","*","ORDER BY country",$mode="no_where"); 
	return execute($sql);
}//END OF FUNCTION get_country_codes

function country_details($code) 
{ 
	$sql = new db();
	$sql->db_Select("country_codes","country,charset","code = '$code'");
	$r = $sql->db_Fetch(); 
	return $r;
}//END OF FUNCTION country_details

?>