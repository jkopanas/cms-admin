<?php
include("init.php");
include("sql_dump.php");


//Start dumping database
if ($action == 'save')
{
	if (!is_array($tbls))
	{
		$smarty->assign("error",$lang['no_tables_found']);//assigned template variable error
	}
	
	//put tables in a list
	
	$tbl = array();
	
	$tbl[] = reset($tbls);
	
	if (count($tbls) > 1)
	{
		$a = true;
		while ($a != false)
		{
			$a = next($tbls);
			if ($a != false)
			{
				$tbl[] = $a;
			}
		}
	}
	

	//Go through the options
	
	if ($opt == 1)//Check what to dump (Schema, Data or both)
	{
		$sv_s = true;
		$sv_d = true;
	}
	else if ($opt == 2)
	{
		$sv_s = true;
		$sv_d = false;
		$fc   = "_struct";
	}
	else if ($opt == 3)
	{
		$sv_s = false;
		$sv_d = true;
		$fc   = "_data";
	}
	else
	{
		exit;
	}

	
	$fext = "." . $savmode;
	$fich = $dbbase . $fc . $fext;
	
	//Erase file or not if exists
	
	$dte = "";
	if ($ecraz != 1)
	{
		$dte = date("dMy_Hi")."_";
	}
	//compress or not
	$gz = "";
	if ($file_type == '1')
	{
		$gz .= ".gz";
	}
	
	$fcut = false;
	$ftbl = false;
	
	$f_nm = array();
	
	if($f_cut == 1)
	{
		$fcut = true;
		$fz_max = $fz_max;
		$nbf = 1;
		$f_size = 170;
	}
	if ($f_tbl == 1)
	{
		$ftbl = true;
	}
	else
	{
		if (!$fcut)
		{
			open_file("dump_".$dte.$dbbase.$fc.$fext.$gz);
		}
		else
		{
			open_file("dump_".$dte.$dbbase.$fc."_1".$fext.$gz);
		}
	}
	$nbf = 1;

	if ($fext == ".sql")
	{
		
		if ($ftbl)
		{
			while (list($i) = each($tbl))
			{
				
				$temp = sqldumptable($tbl[$i]);
				$sz_t = strlen($temp);
				if ($fcut)
				{
					open_file("dump_".$dte.$tbl[$i].$fc.".sql".$gz);
					$nbf = 0;
					$p_sql = split_sql_file($temp);
					while(list($j,$val) = each($p_sql))
					{
						if ((file_pos() + 6 + strlen($val)) < $fz_max)
						{
							write_file($val . ";");
						}
						else
						{
							close_file();
							$nbf++;
							open_file("dump_".$dte.$tbl[$i].$fc."_".$nbf.".sql".$gz);
							write_file($val . ";");
						}
					}
					close_file();
				}
				else
				{
					open_file("dump_".$dte.$tbl[$i].$fc.".sql".$gz);
					write_file($temp ."\n\n");
					close_file();
					$nbf = 1;
				}
				$tblsv = $tblsv . "<b>" . $tbl[$i] . "</b> , ";
			}
		}
		else
		{
			while (list($i) = each($tbl))
			{
				
				$temp = sqldumptable($tbl[$i]);
				$sz_t = strlen($temp);
				if ($fcut && ((file_pos() + $sz_t) > $fz_max))
				{
					$p_sql = split_sql_file($temp);
					while(list($j,$val) = each($p_sql))
					{
						if ((file_pos() + 6 + strlen($val)) < $fz_max)
						{
							write_file($val . ";");
						}
						else
						{
							close_file();
							$nbf++;
							open_file("dump_".$dte.$dbbase.$fc."_".$nbf.".sql".$gz);
							write_file($val . ";");
						}
					}
				}
				else
				{
					write_file($temp);
				}
				$tblsv = $tblsv . "<b>" . $tbl[$i] . "</b> , ";
			}
		}
	}
	else if ($fext == ".csv")
	{
		if ($ftbl)
		{
			while (list($i) = each($tbl))
			{
				
				$temp = csvdumptable($tbl[$i]);
				$sz_t = strlen($temp);
				if ($fcut)
				{
					open_file("dump_".$dte.$tbl[$i].$fc.".csv".$gz);
					$nbf = 0;
					$p_csv = split_csv_file($temp);
					while(list($j,$val) = each($p_csv))
					{
						if ((file_pos() + 6 + strlen($val)) < $fz_max)
						{
							write_file($val . "\n");
						}
						else
						{
							close_file();
							$nbf++;
							open_file("dump_".$dte.$tbl[$i].$fc."_".$nbf.".csv".$gz);
							write_file($val . "\n");
						}
					}
					close_file();
				}
				else
				{
					open_file("dump_".$dte.$tbl[$i].$fc.".csv".$gz);
					write_file($temp ."\n\n");
					close_file();
					$nbf = 1;
				}
				$tblsv = $tblsv . "<b>" . $tbl[$i] . "</b> , ";
			}
		}
		else
		{
			while (list($i) = each($tbl))
			{
				
				$temp = csvdumptable($tbl[$i]);
				$sz_t = strlen($temp);
				if ($fcut && ((file_pos() + $sz_t) > $fz_max))
				{
					$p_csv = split_sql_file($temp);
					while(list($j,$val) = each($p_csv))
					{
						if ((file_pos() + 6 + strlen($val)) < $fz_max)
						{
							write_file($val . "\n");
						}
						else
						{
							close_file();
							$nbf++;
							open_file("dump_".$dte.$dbbase.$fc."_".$nbf.".csv".$gz);
							write_file($val . "\n");
						}
					}
				}
				else
				{
					write_file($temp);
				}
				$tblsv = $tblsv . "<b>" . $tbl[$i] . "</b> , ";
			}
		}
	}
	mysql_close();
	if (!$ftbl)
	{
		close_file();
	}
	reset($f_nm);
for ($i=0;count($f_nm) > $i;$i++)
{
	$files[$i]['loc'] = str_replace($_SERVER['DOCUMENT_ROOT'],"",$f_nm[$i]);
	$files[$i]['name'] = str_replace($backup_folder."/","",$f_nm[$i]);
	$files[$i]['size'] = check_filesize($f_nm[$i]);
	$files[$i]['table_name'] = $tbls[$i];
	
}//END OF FOR
$tmp = explode(",",$tblsv);//END OF print_r;
unset($tmp[count($tmp)-1]);

$smarty->assign("file_count",count($files));//assigned template variable file_count
$smarty->assign("table_names",$tmp);//assigned template variable table_names
$smarty->assign("files",$files);//assigned template variable files
if (is_array($files)){$smarty->assign("db_done",1);}//assigned template variable db_done
}//END OF SAVING DB FILES
//SAVE IMAGES
elseif ($action == "images") 
{
include("../includes/pclzip.lib.php");
$zipfile = "images.zip";
chdir($_SERVER['DOCUMENT_ROOT']);
if (file_exists($_SERVER['DOCUMENT_ROOT'].BACKUP_FOLDER."/".$zipfile)) {unlink($_SERVER['DOCUMENT_ROOT'].BACKUP_FOLDER."/".$zipfile);}
$print_file = str_replace("/","",PRODUCT_IMAGES);
$archive = new PclZip($_SERVER['DOCUMENT_ROOT'].BACKUP_FOLDER."/".$zipfile);
$list = $archive->add("$print_file");
echo $_SERVER['DOCUMENT_ROOT'].PRODUCT_IMAGES;
//$list = $archive->add($_SERVER['DOCUMENT_ROOT'].PRODUCT_IMAGES);
for ($i=0;count($files) > $i;$i++)
{
$list = $archive->add($_SERVER['DOCUMENT_ROOT'].PRODUCT_IMAGES);
}
unset($archive);
$smarty->assign("image_archive",BACKUP_FOLDER."/".$zipfile);//assigned template variable image_archive
$smarty->assign("image_archive_size",check_filesize($_SERVER['DOCUMENT_ROOT'].BACKUP_FOLDER."/".$zipfile));//assigned template variable image_archive
$smarty->assign("images_done",1);
}//END OF ELSEIF

$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","admin/backup.tpl");
$smarty->display("admin/home.tpl");

?>