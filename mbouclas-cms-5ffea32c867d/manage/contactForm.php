<?php
include("init.php");
if ($_GET['action'] == 'placeItemOnMap') {
	print_r($_POST);
	exit();
}//END SAVE MAP


if (defined('CONTACT_FORM_FIELDS')) {
$fields = json_decode(CONTACT_FORM_FIELDS,true);
foreach ($fields as $v)
{
	foreach ($v['translations'] as $k=>$b)
	{
		if ($b)
		{
			$trans[$v['fieldName']][$k] = $lang[$b];
		}
	}
}
$map = new maps(array('module'=>array('name'=>'contactForm')));

$mapItem = $map->getMapItem($_GET['id'],array('debug'=>0,'getItem'=>0));

if ($mapItem)
{
	$mapItem['item'] = array('title'=>COMPANY_NAME);
	$smarty->assign('mapItem',json_encode($mapItem));
	$smarty->assign("item",$mapItem);
}

$smarty->assign("trans",$trans);
$smarty->assign('fields',$fields);
}

$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","admin/contactForm.tpl");
$smarty->display("admin/home.tpl");
?>