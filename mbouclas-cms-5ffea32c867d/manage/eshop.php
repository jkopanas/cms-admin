<?php
include("init.php");//load from manage!!!!


//---INITIATIONS-----
global $smarty;
define("MODULE_NAME", "eshop");

//---SECURITY SWITCH-----
if (!array_key_exists(MODULE_NAME,$loaded_modules)) {
	exit("No Module were loaded - Module Does not exist");
}
//---/SECURITY SWITCH-----

$current_module = $loaded_modules[MODULE_NAME];
// TODO
//print_r($current_module);

define("MODULE_FOLDER", "/manage/".$current_module['name'].".php");
define("MODULE_AJAX_FOLDER", "/ajax/eshop/index.php");
$smarty->assign("MODULE_NAME", MODULE_NAME);
$smarty->assign("MODULE_FOLDER", MODULE_FOLDER);
$smarty->assign("MODULE_AJAX_FOLDER", MODULE_AJAX_FOLDER);
//---/INITIATIONS-----

//$s = new siteModules();
//$layout = $s->pageBoxes(MODULE_NAME,"orders.php",'back',array('getBoxes'=>'full','boxFields'=>'id,title,name,settings'));
//$smarty->assign("layout",$layout);
$s = new siteModules();
if(isset($_REQUEST['fetch']) && (strlen($_REQUEST['fetch'])>0))
{
	$smarty->assign("submenu", $_REQUEST['fetch']);
	$boxView = $s->getBoxView($_REQUEST['fetch']);
	
	$smarty->assign("name",$boxView['name']);
	if(isset($boxView['settings']['jsIncludes']))
	{
		$smarty->assign("jsIncludes",$boxView['settings']['jsIncludes']);
	}
	if(isset($boxView['settings']['boxViewTemplate']))
	{
		$smarty->assign("boxViewTemplate",$boxView['settings']['boxViewTemplate']);
	}
	$fetcfingPage = $boxView['file'];
	//$fetcfingPage = $current_module['folder']."/admin/".trim($_REQUEST['fetch'].".tpl");
	// in case of $_POST the Page is asked to be fetched in AJAX
	//$smarty->display($fetcfingPage);
	//exit();
}
else
{
	$boxView = $s->getBoxView("index");
	
	$smarty->assign("name",$boxView['name']);
	if(isset($boxView['settings']['jsIncludes']))
	{
		$smarty->assign("jsIncludes",$boxView['settings']['jsIncludes']);
	}
	if(isset($boxView['settings']['boxViewTemplate']))
	{
		$smarty->assign("boxViewTemplate",$boxView['settings']['boxViewTemplate']);
	}
	$fetcfingPage = $boxView['file'];
	//$smarty->display($boxView['file']);
	//$fetcfingPage = $current_module['folder']."/admin/home.tpl";
}

//$smarty->assign("menu",$current_module['name']);
//$smarty->assign("breadcrumb","admin2/breadcrumbs/eshop.tpl");
$eshop = new eshop();
$settings = array();
$settings['simplify']=true;
$smarty->assign("orderStatusCodes",$eshop->orderStatusCodes($settings));
$smarty->assign("fetcfingPage",$fetcfingPage);
$smarty->assign("shown",1);
$smarty->assign("menu",$current_module['name']);
//$smarty->assign("sidebar","common/itemSidebar.tpl");
//$smarty->assign("sidebar",$current_module['folder']."/admin/menu.tpl");
$smarty->assign("submenu","add");
///
$smarty->assign("page_title",SITE_NAME." Administration | Eshop");
$smarty->assign("include_file",$current_module['folder']."/admin/index.tpl");

$smarty->display(CURRENT_SKIN."/home.tpl");
exit();

/*******************************************************************
* O => YOU ARE HERE
*
* ViewContrl -> View Controler; controler.js
* View -> the View of the box; tpl render
* ModContrl -> the modular_box controler; loader.php
* Module -> the modular_box; The php file of the box
*
* A View request is been answered, as the view asks for its data.
* Data are been processed by the modular box and returned to view
* witch should render the fetched json.
*
* 		ViewContrl			View		ModContrl			Module
*---------------------------------------------------------------------------
*-REQVIEW-->_I_				 |				 |				 |
* 			| |	Call View	 |				 |				 |
* 			| |---------------------------->_I_				 |
* 							 | Smarty Displ |O|Fetch view	 |
* 							_I_<------------|O|				 |
* 		Ajax Display		| |				 |				 |
* <-------------------------| |				 |				 |
* 							 |				 |				 |
* 							_I_				 |				 |
*  							| |Call loader	 |				 |
*  							| |------------>_I_				 |
*  							 |				| |Ajax-Load	 |
* 							 |				| |------------>_I_
* 							 |					Return Data	| | Fetch Data
* 							_I_<----------------------------| |
* 							| |Render Data
* 			Display Data	| |
* <-------------------------| |
*
* A Request is been handled, as the command should be proccesed
* and return its state.
*
* -REQ-CMD---Module+Comonent+RequestCMD---->___
* 											| |Ajax-Load
* 											| |------------>___
* 												Return Data	| | Process CMD
* 							_I_<----------------------------| | & Fetch Data
* 							| |Render Data
* 			Display Data	| |
* <-------------------------| |
*
*******************************************************************/

/**
 * <div class="menuWrapper dp25" id="adminSubMenuWrapper" >
	<div class="box_c" />
	<h3 class="box_c_heading cf">
		<span class="k-link k-state-selected">E-Shop Menu</span>
	</h3>
		{include file="modules/eshop/admin/menu.tpl"}
	</div>
</div>
 */
?>