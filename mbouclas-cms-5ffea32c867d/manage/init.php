<?php
session_start();
define('ABSPATH', str_replace("manage","",dirname(__FILE__)));
//echo ABSPATH;
include(ABSPATH."/includes/config.php");
include(ABSPATH."/includes/dataStore.php");
include(ABSPATH."/includes/sql_driver.php");
include(ABSPATH."/includes/phpmailer/class.phpmailer.php");
include(ABSPATH."/includes/functions.php");
include(ABSPATH."/includes/admin_functions.php");
include(ABSPATH."/includes/common.php");
include(ABSPATH."/includes/pagination.php");
include(ABSPATH."/includes/pass_crypt.php");
include(ABSPATH."/includes/file_wrappers.php");
include(ABSPATH."/includes/extra_functions.php");
include(ABSPATH."/manage/security.php");
include(ABSPATH."/includes/smarty.php");

define('CURRENT_SKIN','admin2');

################### BEGIN Cryptography Settings ########################################################
$CRYPT_SALT = 85; # any number ranging 1-255
$START_CHAR_CODE = 100; # 'd' letter
################### END Cryptography Settings ##########################################################

$sql = new db();
$sql->db_Connect(DBHOST,DBUSER,DBPASS,DB);


spl_autoload_register('smartyAutoload'); 
spl_autoload_register("__autoload");

function __autoload($class_name)
{
    require_once ABSPATH.'/modules/'."$class_name.php";
//    throw new Exception("Unable to load ".ABSPATH.'/modules/'."$class_name.php");
}
/*mysql_query(' set character set utf8 ');
mysql_query("SET NAMES 'utf8'") or die('Query failed: ' . mysql_error());*/

$config["Sessions"]["session_length"] = 5400;//1.5 Hours
define("USER_TYPE","admin");
############################# BEGIN GLOBAL VARIABLE DEFINITIONS #####################################
define("e_HTTP", "/manage/");
$url_prefix=substr($_SERVER['PHP_SELF'],strlen(e_HTTP),strrpos($_SERVER['PHP_SELF'],"/")+1-strlen(e_HTTP));
$num_levels=substr_count($url_prefix,"/");

for($i=1;$i<=$num_levels;$i++)
{
	$link_prefix.="../";
}

define("e_SELF", "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']); //full address + filename
define("e_QUERY", eregi_replace("&|/?PHPSESSID.*", "", $_SERVER['QUERY_STRING']));//everything on the string after the ?
define('e_BASE',$link_prefix);
define("e_FILE",basename($_SERVER['PHP_SELF']));
define("QUERY_STRING",capture_links($_GET));
define("TIME_LOGIN",12);//HOURS TO STAY LOGED IN, IF WE NEED DAYS WE MULTIPLY HOURS BY DAYS. e.g. 24*2 (2 days)
############################# END GLOBAL VARIABLE DEFINITIONS #####################################

############################# BEGIN LOAD SKIN ###################################################
/*$sql->db_Select("skins","id,skin_path,skin_url","used = 1");//Get active skin
$skin = execute_single($sql,0);*/


############################# END LOAD SKIN ###################################################

############################# BEGIN SITE PREFS ##################################################
$prefs = load_prefs();
foreach ($prefs as $key => $value) 
{ 
   	if (!preg_match("[0-9]",$key)) 
	{
		define(strtoupper($key),$value);
		$smarty->assign(strtoupper($key),$value);
		$constants[$key] = $value;
	}//END OF IF
}//END OF FOR 
$smarty->assign("SYSTEM_PREFS",json_encode($constants));
$encrypt_emails = ENCRYPT_EMAILS;
global $encrypt_emails;
############################# END SITE PREFS ####################################################

############################# BEGIN EMAILER SETTINGS ############################################
// Email Settings
$site['from_name'] = COMPANY_NAME; // from email name
$site['from_email'] = COMPANY_EMAIL; // from email address

// Just in case we need to relay to a different server, 
// provide an option to use external mail server.
$site['smtp_mode'] = 'disabled'; // enabled or disabled
$site['smtp_host'] = null;
$site['smtp_port'] = null;
$site['smtp_username'] = null; 

############################ END EMAILER SETTINGS ###############################################

############################# BEGIN LOAD LANG ###################################################
//check if language change
$Languages = new Languages();

if ($_GET['sl']) 
{  
$Languages->setLanguage($_GET['sl'],'back_lang');
$back_language = $Languages->checkLanguage($_GET['sl']);
}//END OF IF
if (empty($back_language)) 
{  
 	$lang = $Languages->LoadLanguage(DEFAULT_LANG_ADMIN);
 	define("BACK_LANG",DEFAULT_LANG_ADMIN);
 	$back_language = DEFAULT_LANG_ADMIN;
}//END OF IF
else 
{  
	$back_language = $Languages->checkLanguage($back_language);
	$lang = $Languages->LoadLanguage($back_language);
}//END OF ELSE
define("BACK_LANG",$back_language);
$smarty->assign("BACK_LANG",$back_language);//assigned template variable front_language
$smarty->assign("lang",$lang);//assigned template variable lang
$country = $Languages->get_country(DEFAULT_LANG_ADMIN);
setlocale (LC_ALL, $country['locale']);
############################# END LOAD LANG ###################################################
init_session();//get the user data id a cookie or session is set
####################### IF THE POST VARIABLES ARE SUBMITED LOGIN #############################

if(IsSet($_POST['uname']))
{
$smarty->assign("redir",e_QUERY ? e_SELF."?".e_QUERY : e_SELF);
$usr = new userlogin($_POST['uname'], $_POST['pass'], $_POST['autologin']); 
//on success the class userlogin will refresh the page and init will be called again to get the user data
}
####################### END LOGIN #############################################################			

############################## IF logout IN QUERY STRING LOGOUT ###############################
$cookie_name = "user_".USER_TYPE;if(e_QUERY == "logout")
{
 $ip=getenv(REMOTE_ADDR); 
$sql->db_Update("users","user_lastvisit = '".time()."' WHERE id = ".ID." AND uname = '".UNAME."'");
$sql->q("REPLACE INTO stats_login_history (login, date_time, usertype, action, status, ip) VALUES ('".UNAME."','".time()."','$login_type','logout','success','$ip')");
setcookie($cookie_name, '', 0, '/', '', 0);//reset user cookie
setcookie('webfxtab_tabPane1', '', 0, '/', '', 0);//reset general stats tab pane cookie cookie
header("Location: index.php");
exit;
}//END OF IF
############################## END LOGOUT ####################################################



############################# LOAD ACTIVE MODULES ###############################################
	 $loaded_modules = array();
	 global $loaded_modules,$site_modules,$loaded_modules_array;
	 $modules_list = load_modules(1,0);
############################# END LOAD ACTIVE MODULES ###########################################
########################### LOAD MODULES ###################################
foreach ($modules_list as $m)
{
	if ($m['startup'] == 1) 
	{
		if ($loaded_modules_array[] =  module_is_active($m['name'],1,1))
		{		
			$smarty->assign($m['name'],$loaded_modules[$m['name']]);
			if (strstr($_SERVER['PHP_SELF'],$m['name']))
			{
				define('CURRENT_MODULE',$m['name']);
			}
			elseif ($_GET['module'])//AJAX
			{
				define('CURRENT_MODULE',$_GET['module']);
			}


			
		}
	}
}
if (!defined('CURRENT_MODULE')) { define('CURRENT_MODULE','content'); }
$smarty->assign("loaded_modules",$loaded_modules);
######################### END LOAD AVAILABLE MODULES #########################################


############################# BEGIN WYSIWYG EDITOR ##############################################
//require_once(ABSPATH."manage/tools/wysiwygPro/wproAjax.class.php");
for ($i=0;count($active_modules) > $i;$i++)
{
	$sql->db_Select("modules_settings","value,name,options","name = 'images_path' AND module_id = ".$active_modules[$i]['id']);
	if ($sql->db_Rows() > 0)
	{
		$tmp = execute_single($sql);

		$editor_settings['images_folders'][$i]['images_dir'] = $_SERVER['DOCUMENT_ROOT'].$tmp['value'];
		$editor_settings['images_folders'][$i]['images_url'] = $tmp['value'];
		$editor_settings['images_folders'][$i]['type'] = $tmp['options'];
		$editor_settings['images_folders'][$i]['name'] = $active_modules[$i]['name'];
	}

}
		$editor_settings['document_folders']['document_dir'] = $_SERVER['DOCUMENT_ROOT']."/docs";
		$editor_settings['document_folders']['document_url'] = "/docs";
############################# END WYSIWYG EDITOR ################################################
auto_publish(array('action' => 'activate','module'=>'content','debug'=>0));


if (array_key_exists(CURRENT_MODULE,$loaded_modules))
{
	$Content = new Items(array('module'=>$loaded_modules[CURRENT_MODULE],'debug'=>0));
}
$smarty->assign("CURRENT_MODULE", $loaded_modules[CURRENT_MODULE] );
$smarty->assign("MODULE_FOLDER", $loaded_modules[CURRENT_MODULE]['folder']."/admin");
$smarty->assign("root_categories", $Content->ItemTreeCategories(0));
########################### BEGIN SMARTY ASSIGMENTS ###########################################
$all_countries = $Languages->AvailableLanguages(array('findDefault'=>'default_lang_admin'));

if (defined("LOGINMESSAGE")){$smarty->assign("LOGINMESSAGE",LOGINMESSAGE);}//END OF IF
//$smarty->assign("users_online",users_online());//assigned template variable users_online
$smarty->assign("sort_fields",$sort_fields);//assigned template variable sort_fields
$smarty->assign("country_details",$country);//assigned template variable country_details
$smarty->assign("PHP_SELF",$_SERVER['PHP_SELF']);
$smarty->assign("QUERY",e_QUERY);
$smarty->assign("e_FILE",e_FILE);
$smarty->assign("QUERY_STRING",QUERY_STRING);
$smarty->assign("countries", $all_countries);//assigned template variable countries

########################### END SMARTY ASSIGMENTS ###########################################
include(ABSPATH."/plugins/index.php");

/* PLUGINS */
if (!empty($plugins)) {
	foreach ($plugins as $k=>$v) {
		if ( $v['area'] AND in_array("A", $v['area']) ) {
			include_once(ABSPATH."/plugins/".$v['path']);
		}
	}
}


########################SMATY DEBUG###############################
$smarty->debugging = false;
$smarty->debug_tpl = 'test/debug.tpl';
########################SMATY DEBUG###############################
?>