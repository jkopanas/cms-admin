<?php
include("init.php");

$settings = new SettingsManager();
if (empty($_GET)) {
	$smarty->assign("Modules",$settings->getAvailableModules());
}
if ($_GET['id']) {
	$smarty->assign("Module",$settings->loadModuleDetails($_GET['id']));
	$smarty->assign("Settings",$settings->loadModuleSettings($_GET['id'],array('group'=>'category')));
}

$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","admin/settings_manager.tpl");
$smarty->display("admin/home.tpl");

?>