
var dataSource =  {
	pageSize: 20,
     serverPaging: true,
    serverFiltering: true,
    serverSorting: true,

	    error: function(e) {
        // handle event
        console.log(e)
    },
    schema : {
    data : function(data) {
    	statusCodes = data.status_codes;
     	return data.orders;
     },
     total: function (data) { 
		return data.list.total
    }	
    },
  transport: {
read: {
    // the remote service url
    url: "/ajax/eshop.php?action=search",

    dataType: "json",
	type : 'post',
    data: {
        secondaryData: { orderby : 'eshop_orders.id',results_per_page:20,way:'desc'},'return':'json'
    }
}
}	
};
var schema = {
    model: {
    fields: {
        id: { type: "number" },
        email: { type: "string" },
    }
}
 };
var columns = [
      {
          field: "id",
          title: "#ID",
          template : '<a href="\\#">${ id }</a>'
      },
      {
      	field:'email',
      	title:'email'
      },
      {
      	field:'status',
      	title:'Status',
      	template : function(e) { return statusCodes[e.status];}
      }
      ];
var grid = mcms.grid('#grid',{ ref:'name',dataSource:dataSource,schema:schema, columns:columns});