<?php
class CategoriesActions
{
	var $settings;
	var $module;
	var $tables;
	
	function CategoriesActions($settings)
	{
		$this->module = $settings['module'];
		$this->settings = $settings;
		//SETUP TABLES
		$this->tables = array();
		if ($this->module['name'] == 'content') {
			$this->tables['category'] = 'content_categories';
		}//END IF
		elseif ($this->module['name'] == 'products')
		{
			$this->tables['category'] = 'products_categories';
		}
	}//END FUNCTION	

function AddFlatCategory($table,$data,$settings=0)
{
	global $sql;
	$t = new textparse();
	$Cat = new Items(array('module'=>$this->module['name'],'debug'=>0));
	$new_category = $t->formtpa($data['title']);
	$new_cat = $Cat->ItemCategory($new_parentid,array('table'=>$table));
	$new_description = $t->formtpa($data['description']);
	$new_date_added = time();
	$new_orderby = $data['orderby'];
	$new_active = $data['active'];
	$new_alias = $t->formtpa(str_replace(" ","_",$data['alias']));
	$image = $data['image'];
	//Check out for settings
	$tmp = array();
	foreach ($data as $k => $v)
	{
		if (strstr($k,"settings_")) 
		{
			list($dump,$field)=split("settings_",$k);
	//		echo "$field --- $v<br>";
	if ($v) 
	{
		$tmp[$field] = $v;
	}
			
		}
		
	}
	if ($tmp) 
	{
		$category_settings = json_encode($tmp);
	}
		
		$sql->db_Insert($table,"'','$new_category','$new_description','$image','$new_orderby','$new_date_added','$new_active','$new_alias','$category_settings'");
		$new_categoryid = $sql->last_insert_id;
		if ($data['debug']) {
			echo "INSERT INTO $table VALUES ('','$new_category','$new_parentid','','$new_description','$new_alias','$new_orderby','$new_date_added','$category_settings')<br>";
		}
	
	if ($settings['return'] == 'cat') {
		return $new_categoryid;
	}
	}//END FUNCTION

	function ModifyFlatCategory($table,$data,$settings=0)
	{
		global $sql;
		$old_cat = $data['category'];
		$t = new textparse();
	//Check out for settings
	$tmp = array();
	foreach ($data as $k => $v)
	{
		if (strstr($k,"settings_")) 
		{
			list($dump,$field)=split("settings_",$k);
	//		echo "$field --- $v<br>";
			$tmp[$field] = $v;
		}
	}
	if ($tmp) 
	{
		$category_settings = json_encode($tmp);
	}
	$category = $t->formtpa($data['title']);
	$description = $t->formtpa($data['description']);
	$image = $t->formtpa($data['image']);
	$alias = $t->formtpa(str_replace(" ","_",$data['alias']));
	//now modify the details of the given category
	$sql->db_Update($table,"alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', 
	orderby = '".$data['orderby']."', image = '".$image."' WHERE categoryid = ".$data['cat']);
	if ($settings['debug']) {
		echo "UPDATE $table SET  alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', categoryid_path = '$new_catid_path', 
	order_by = '".$data['orderby']."' WHERE categoryid = ".$data['cat'];
	}
	
	}//END FUNCTION
	
	function AddTreeCategory($table,$data,$settings=0)
	{
		global $sql;	
		HookParent::getInstance()->doTriggerHookGlobally("beforeCategoryAdd",$data);
		$t = new textparse();
		$Cat = new Items(array('module'=>$this->module,'debug'=>0));
		$new_category = $t->formtpa($data['title']);
		$new_parentid = $data['parent_catid'];
		$new_cat = $Cat->ItemCategory($new_parentid,array('table'=>$table,'debug'=>0));
		$new_description = $t->formtpa($data['desc']);
		$new_date_added = time();
		$new_orderby = $data['orderby'];
		$new_alias = $t->formtpa(str_replace(" ","_",$data['alias']));
		//Check out for settings
	$tmp = array();

	foreach ($data as $k => $v)
	{

		if (strstr($k,"settings_")) 
		{
			list($dump,$field)=split("settings_",$k);
	//		echo "$field --- $v<br>";
	if ($v!="") 
	{
		$tmp[$field] = $v;
	}
			
		}
	}
	if ($tmp) 
	{
		$category_settings = json_encode($tmp);
	}
		
		$sql->db_Insert($table,"'','$new_category','$new_parentid','','$new_description','$new_alias','$new_orderby','$new_date_added','$category_settings'");
		$new_categoryid = $sql->last_insert_id;
		$new_categoryid_path = ($new_cat == 0) ? $new_categoryid : $new_cat['categoryid_path']."/".$new_categoryid;
		$sql->db_Update($table,"categoryid_path = '$new_categoryid_path' WHERE categoryid = '$new_categoryid'");
		
		if ($settings['debug']) {
			echo "INSERT INTO $table VALUES ('','$new_category','$new_parentid','','$new_description','$new_alias','$new_orderby','$new_date_added','$category_settings')<br>";
		}
	$data['categoryid'] = $new_categoryid;
	HookParent::getInstance()->doTriggerHookGlobally("afterCategoryAdd",$data);
	if ($settings['return'] == 'cat') {
		return $new_categoryid;
	}
	
	}//END FUNCTION
	
	function ModifyTreeCategory($table,$data,$settings=0)
        {
                global $sql;
          HookParent::getInstance()->doTriggerHookGlobally("beforeCategoryUpdate",$data);
                $old_cat = $data['category'];
                $t = new textparse();
        $parentid = $data['parent_catid'];
        if ($parentid != 0) 
        {  
                $Cat = new Items(array('module'=>$this->module,'debug'=>0));
                $parent_details = $Cat->ItemCategory($parentid,array('table'=>$table,'debug'=>0));
                $new_catid_path  = $parent_details['categoryid_path']."/".$data['cat'];
        }//END OF IF
        else 
        {
        $new_catid_path = $data['cat'];        
        }
        
        //Check out for settings
        $tmp = array();
        foreach ($data as $k => $v)
        {
                if (strstr($k,"settings_")) 
                {
                        list($dump,$field)=split("settings_",$k);
        //                echo "$field --- $v<br>";
                        $tmp[$field] = $v;
                }
                
                
        }
        if ($tmp) 
        {
                $category_settings = json_encode($tmp);
        }
        $category = $t->formtpa($data['title']);
        $description = $t->formtpa($data['description']);
        $alias = $t->formtpa(str_replace(" ","_",$data['alias']));
        //now modify the details of the given category
        $sql->db_Update($table,"parentid = '$parentid', alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', categoryid_path = '$new_catid_path',order_by = '".$data['orderby']."' WHERE categoryid = ".$data['cat']);
        echo mysql_error();
        if ($settings['debug']) {
                echo "UPDATE $table SET parentid = '$parentid', alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', categoryid_path = '$new_catid_path', 
        order_by = '".$data['orderby']."' WHERE categoryid = ".$data['cat'];
        }
        //rearange subcategory paths
        $items = new Items(array('module'=>$this->module));
        $children = $items->ItemTreeCategories($data['cat'],array('table'=>$table));
        for ($i=0;count($children) > $i;$i++)
        {
        $child_id = $children[$i]['categoryid'];
        $new_cat_path = $new_catid_path."/$child_id";
        $sql->db_Update($table,"categoryid_path = '$new_cat_path' WHERE categoryid = '$child_id'");
        }//END OF FOR
        if (class_exists('Memcache',false)) {
        $cache = new memCacheExt();
        $cache->deleteItem($data['cat'],$this->module['name']);
        }
			HookParent::getInstance()->doTriggerHookGlobally("afterCategoryUpdate",$data);
			
        }//END FUNCTION
	
	
	function AddCommonTreeCategory($tableID,$data,$settings=0)
	{
		global $sql;
		$t = new Parse();
		$Cat = new Items(array('module'=>$this->module,'debug'=>0));
		$new_category = $t->toDB($data['title']);
		$new_parentid = $data['parent_catid'];
		$new_cat = $Cat->commonCategoryTree($tableID,$new_parentid,array('table'=>$table,'debug'=>0));

		$new_description = $t->toDB($data['desc']);
		$new_date_added = time();
		$new_orderby = $data['orderby'];
		$new_alias = $t->toDB(str_replace(" ","_",$data['alias']));
		//Check out for settings
	$tmp = array();
	foreach ($data as $k => $v)
	{
		if (strstr($k,"settings_")) 
		{
			list($dump,$field)=split("settings_",$k);
	//		echo "$field --- $v<br>";
	if ($v) 
	{
		$tmp[$field] = $v;
	}
			
		}
	}
	if ($tmp) 
	{
		$category_settings = json_encode($tmp);
	}
		
		$sql->db_Insert("common_categories_tree","'',$tableID,'".$this->module['name']."','$new_category','$new_parentid','','$new_description','$new_alias','$new_orderby','$new_date_added','$category_settings'");
		$new_categoryid = $sql->last_insert_id;
		$new_categoryid_path = ($new_cat == 0) ? $new_categoryid : $new_cat['categoryid_path']."/".$new_categoryid;
		$sql->db_Update("common_categories_tree","categoryid_path = '$new_categoryid_path' WHERE categoryid = '$new_categoryid' AND tableID = $tableID");
		if ($settings['debug']) {
			echo "INSERT INTO common_categories_tree VALUES ('',$tableID,'".$this->module['name']."','$new_category','$new_parentid','','$new_description','$new_alias','$new_orderby','$new_date_added','$category_settings')<br>";
		}
	
	if ($settings['return'] == 'cat') {
		return $new_categoryid;
	}
	
	}//END FUNCTION
	
	function ModifyCommonTreeCategory($tableID,$data,$settings=0)
	{
		global $sql;
		$old_cat = $data['category'];
		$t = new Parse();
	$parentid = $data['parent_catid'];
	if ($parentid != 0) 
	{  
		$Cat = new Items(array('module'=>$this->module,'debug'=>0));
		$parent_details = $Cat->commonCategoryTree($tableID,$parentid,array('table'=>$table,'debug'=>0));
		$new_catid_path  = $parent_details['categoryid_path']."/".$data['cat'];
	}//END OF IF
	else 
	{
	$new_catid_path = $data['cat'];	
	}
	
	//Check out for settings
	$tmp = array();
	foreach ($data as $k => $v)
	{
		if (strstr($k,"settings_")) 
		{
			list($dump,$field)=split("settings_",$k);
	//		echo "$field --- $v<br>";
			$tmp[$field] = $v;
		}
		
		
	}
	if ($tmp) 
	{
		$category_settings = json_encode($tmp);
	}
	$category = $t->toDB($data['title']);
	$description = $t->toDB($data['description']);
	$alias = $t->toDB(str_replace(" ","_",$data['alias']));
	//now modify the details of the given category
	$sql->db_Update("common_categories_tree","parentid = '$parentid', alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', categoryid_path = '$new_catid_path',order_by = '".$data['orderby']."' 
	WHERE tableID = $tableID AND categoryid = ".$data['cat']);
	echo mysql_error();
	if ($settings['debug']) {
		echo "UPDATE common_categories_tree SET parentid = '$parentid', alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', categoryid_path = '$new_catid_path', 
	order_by = '".$data['orderby']."' WHERE tableID = $tableID AND  categoryid = ".$data['cat'];
	}
	//rearange subcategory paths
	$items = new Items(array('module'=>$this->module));
	$children = $items->CommonCategoriesTree($tableID,$data['cat'],array('table'=>$table));
	for ($i=0;count($children) > $i;$i++)
	{
	$child_id = $children[$i]['categoryid'];
	$new_cat_path = $new_catid_path."/$child_id";
	$sql->db_Update("common_categories_tree","categoryid_path = '$new_cat_path' WHERE tableID = $tableID AND categoryid = '$child_id'");
	}//END OF FOR
	
		if (class_exists('Memcache',false)) {
		$cache = new memCacheExt();
		$cache->deleteItem($data['cat'],$this->module['name']);
		}
	}//END FUNCTION
	
}//END CLASS

?>