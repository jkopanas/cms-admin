<?php

class ExtraFields extends Items
{
    function GetItemExtraFields($itemid, $settings = 0)
    {
        global $sql;
        global $Languages;
        if ($settings['active']) {
            $active_q = "AND active = '" . $settings['active'] . "'";
        }
        
        $fields = ($settings['fields']) ? $settings['fields'] :
            "extra_fields.fieldid,extra_fields.field,extra_fields.type,extra_fields.var_name,extra_fields.settings,extra_field_values.value";
        $sql->db_Select("extra_fields INNER JOIN extra_field_values ON (extra_fields.fieldid = extra_field_values.fieldid)",
            $fields, "itemid = $itemid AND module = '" . $this->module['name'] . "' $active_q");
        
        if($settings['debug'])
        {
            echo("SELECT $fields FROM extra_fields INNER JOIN extra_field_values ON (extra_fields.fieldid = extra_field_values.fieldid) WHERE itemid = $itemid AND module = '".$this->module['name']."' AND active = 'Y'<br>");
        }
        if ($sql->db_Rows()) {
            $fields = execute_multi($sql,1);
            //print_r($fields);
            foreach ($fields as $k=>$v)
            {
            	$data = json_decode($fields[$k]['settings'],true);
                $fields[$k]['settings']=$data;
                
                
                if (defined('TRANSLATE')) {
            		$translations = $Languages->itemTranslations($fields[$k]['fieldid'],TRANSLATE,$this->module['name'],array('debug'=>0,'groupby'=>'field','table'=>'extra_fields'));
        			if ($translations) {
            			foreach ($translations as $key=>$val)
            			{
                            if ($val['translation']) {
                                if(strstr($key,'settings'))
                                {
                                    $value=explode('_',$key);   
                                    $fields[$k]['settings'][$value[1]]=$val['translation'];
                                }
                                else
                                {
                                    $fields[$k][$key] = $val['translation'];    
                                }
                            	
                            }
            			}
        			}
                }
                
                
                if($fields[$k]['type']=='multiselect')
                {

                    $fields[$k]['value']=$data['data'][$fields[$k]['value']];
                }
            }
            //print_r($fields);
            if ($settings['grouped'])
            {               
                if($settings['grouped']['title']){
                    $args[] = "title='".$settings['grouped']['title']."'"; 
                }
                if($settings['grouped']['id']){
                    $args[] = "groupid='".$settings['grouped']['id']."'";
                }
                
                $args[] = "module='".$this->module['name']."'";
                
                $mode='default';
                
                if(count($args)>1)
                    $args = implode(" AND ",$args);
                elseif(count($args)==1)
                    $args = $args[0];
                else
                    $mode='no_where';
                
                $sql->db_Select('extra_fields_groups_items INNER JOIN extra_fields_groups ON extra_fields_groups.id=extra_fields_groups_items.groupid','*',$args.' ORDER BY extra_fields_groups.orderby,extra_fields_groups_items.orderby',$mode);
                if($settings['debug'])
                {
                    echo 'SELECT * FROM extra_fields_groups_items INNER JOIN extra_fields_groups ON extra_fields_groups.id=extra_fields_groups_items.groupid WHERE '.$args.' ORDER BY extra_fields_groups.orderby,extra_fields_groups_items.orderby';
                }
                if($sql->db_Rows())
                {
                    $data = execute_multi($sql);
                    
//                    print_r($data);
                    
                    if (defined('TRANSLATE')) {
                        foreach($data as $key=>$val)
                        {
                    			
                    			$translations = $Languages->itemTranslations($data[$key]['groupid'],TRANSLATE,$data[$key]['module'],array('debug'=>0,'groupby'=>'field','table'=>'extra_fields_groups'));
                    			if ($translations) {
                    			foreach ($translations as $k=>$v)
                    			{
                    				if ($v['translation']) {
                    					$data[$key][$k] = $v['translation'];
                    				}
                    			}
                    			}
                  		}//END TRANSLATING
                        //print_r($group);
                        //print_r($item);
                    }
                    
                    foreach($data as $k=>$v)
                    {
                        //$groups[$data[$k]['groupid']][]=$data[$k]['itemid'];
                        foreach($fields as $key=>$val)
                        {
                            if($fields[$key]['fieldid']==$data[$k]['itemid'])
                            {   
                                $groups[$data[$k]['groupid']]['title']=$data[$k]['title'];
                                $groups[$data[$k]['groupid']]['description']=$data[$k]['description'];
                                $groups[$data[$k]['groupid']]['settings']=json_decode($data[$k]['settings'],true);
                                $groups[$data[$k]['groupid']]['data'][]=$fields[$key];
                            }
                        }
                    }
                }
                $fields = $groups;
            }
            else if ($settings['return_simple']) { //RETURN A SIMPLE ARRAY FOR COMPARISON
                foreach ($fields as $v) {
                    $tmp[$v['fieldid']] = $v['value'];
                    if (defined('FRONT_LANG') and (FRONT_LANG != DEFAULT_LANG)) { //TRANSLATE CONTENT
                        global $Languages;
                        $translations = $Languages->itemTranslations($v['fieldid'], FRONT_LANG, $this->
                            module['name'], array('debug' => 0, 'groupby' => 'field', 'table' =>
                            'extra_field_values'));
                        if ($translations) {
                            foreach ($translations as $k => $val) {
                                if ($val['translation']) {
                                    $tmp[$v['fieldid']] = $val['translation'];
                                }
                            }
                        }
                    } //END TRANSLATE
                } //END FOREACH
                unset($fields);
                $fields = $tmp;
            } //END IF
            else {
                if (defined('FRONT_LANG') and (FRONT_LANG != DEFAULT_LANG)) { //TRANSLATE CONTENT
                    for ($i = 0; count($fields) > $i; $i++) {
                        global $Languages;
                        $translations = $Languages->itemTranslations($fields[$i]['fieldid'], FRONT_LANG,
                            $this->module['name'], array('debug' => 0, 'groupby' => 'field'));
                        if ($translations) {
                            foreach ($translations as $k => $val) {
                                if ($val['translation']) {
                                    $fields[$i][$k] = $val['translation'];
                                }
                            }
                        }
                    } //END LOOP
                } //END TRANSLATE
            }
            return $fields;
        } //END IF ROWS
    } //END FUNCTION

    function ExtraField($fieldid, $settings = 0)
    {
        global $sql;
        $fields = ($settings['fields']) ? $settings['fields'] : 'extra_fields.*';
        if ($settings['catids']) {
            $q[] = "catid IN (" . implode(',', $settings['catids']) . ")";
            $tables[] = "INNER JOIN extra_field_categories ON (extra_fields.fieldid=extra_field_categories.fieldid)";
            $fields .= ",extra_field_categories.orderby";
        } //END IF
        if ($settings['catid_list']) {
            $fields .= ",(SELECT GROUP_CONCAT(catid ORDER BY orderby SEPARATOR ',') FROM extra_field_categories WHERE fieldid = extra_fields.fieldid ) as catids";
        } //END IF
        $q[] = "fieldid = $fieldid";
        if (is_array($q)) {
            $query = implode(" AND ", $q);
        } //END IF
        if (is_array($tables)) {
            $joins = implode(" ", $tables);
        } //END IF
        if ($settings['debug']) {
            echo "SELECT $fields FROM extra_fields WHERE $query <br>";
        }
        $sql->db_Select("extra_fields", $fields, "$query");
        if ($sql->db_Rows()) {
            $field = execute_single($sql);

            $field['settings'] = json_decode($field['settings'],true);            
            if ($settings['catid_list']) {
                $catids = explode(",", $field['catids']);
                unset($field['catids']);
                for ($i = 0; count($catids) > $i; $i++) {
                    $field['catids'][$catids[$i]] = $i; //CATID AS KEY, ORDER BY AS VALUE
                }


            } //END RETURN CATEGORIES
            return $field;
        } //END IF
    } //END FUNCTION

    function GetExtraFields($settings = 0)
    {
        global $sql;

        $fields = ($settings['fields']) ? $settings['fields'] : 'extra_fields.*';
        if ($settings['active']) {
            $q[] = "active = '" . $settings['active'] . "'";
        }
        if ($settings['catid']) {
            $q[] = "catid = " . $settings['catid'];
            $tables[] = "INNER JOIN extra_field_categories ON (extra_fields.fieldid=extra_field_categories.fieldid)";
            $fields .= ",extra_field_categories.orderby";
        } //END IF
        if ($settings['catid_list']) {
            $fields .= ",(SELECT GROUP_CONCAT(catid ORDER BY orderby SEPARATOR ',') FROM extra_field_categories WHERE fieldid = extra_fields.fieldid ) as catids";
        } //END IF
		if ($settings['groupid']) {
            $q[] = "groupid = " . $settings['groupid'];
            $tables[] = "INNER JOIN  extra_fields_groups_items ON (extra_fields.fieldid=extra_fields_groups_items.itemid)";
        } //END IF
        
        if ($settings['catids']) {
            $q[] = "catid IN (" . implode(',', $settings['catids']) . ")";
            $tables[] = "INNER JOIN extra_field_categories ON (extra_fields.fieldid=extra_field_categories.fieldid)";
            $fields .= ",extra_field_categories.orderby";
            if ($settings['catid_list']) {
                $fields .= ",(SELECT GROUP_CONCAT(catid ORDER BY orderby SEPARATOR ',') FROM extra_field_categories WHERE fieldid = extra_fields.fieldid ) as catids";
            } //END IF
        } //END IF
        $q[] = "extra_fields.module = '" . $this->module['name'] . "'";
        
    		
    if ($settings['searchFields']) {
		
		foreach ($settings['searchFields'] as $k=>$v) {
			
			if ($v['type'] == 'LIKE') {
				 $q[] = "extra_fields.".$k." LIKE '%".$v["val"]."%'";
			}
			elseif ($v['type'] == 'NOTLIKE') { 
				$q[] = "extra_fields.".$k." NOT LIKE '%".$v["val"]."%'";
			}
			elseif ($v['type'] == 'EQ') {
				$q[] = "extra_fields.".$k." = '".$v["val"]."'";
			}
			elseif ($v['type'] == 'NE') { 
				$q[] = "extra_fields.".$k." != '".$v["val"]."'";
			}
		}

	}
        
        
        if (is_array($q)) {
            $query = implode(" AND ", $q);
        } //END IF
        if (is_array($tables)) {
            $joins = implode(" ", $tables);
        } //END IF
        $orderby = ($settings['orderby']) ? $settings['orderby'] :
            "extra_fields.fieldid";
        $way = ($settings['way']) ? $settings['way'] : "ASC";
        $mode = ($query) ? "default" : "no_where";
        
        $current_page = ($settings['page']) ? $settings['page'] : 1;
        $results_per_page = ($settings['results_per_page']) ? $settings['results_per_page'] : 0;

        $start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
		if ($settings['results_per_page']) {
			$limitQ = "LIMIT $start , $results_per_page";
		}
		
        
     		if($settings['countItems'])
            {
                $sql->db_Select("extra_fields $joins", $fields, $query ." GROUP BY fieldid ORDER BY $orderby $way", $mode);
            	return $sql->mySQLrows;
            }
        
        $sql->db_Select("extra_fields $joins", $fields, $query .
            " GROUP BY fieldid ORDER BY $orderby $way $limitQ", $mode);
        if ($settings['debug']) {
            echo "SELECT $fields FROM extra_fields $joins WHERE $query GROUP BY fieldid ORDER BY $orderby $way LIMIT $start , $results_per_page";
        } //END IF
        
        if ($sql->db_Rows()) {
            $res = execute_multi($sql);
            for ($i = 0; count($res) > $i; $i++) {
                $res[$i]['settings'] = json_decode($res[$i]['settings'],true);
                if ($settings['match_values'] and $settings['itemid']) { //FILL UP FIELDS WITH ITEM VALUES
                    $Itemfields = $this->GetItemExtraFields($settings['itemid'], array('fields' =>
                        'extra_fields.fieldid,extra_field_values.value', 'return_simple' => 1,'debug'=>0));
                    //$v['value'] = $Itemfields[$v['fieldid']];
                    if (is_array($Itemfields) and array_key_exists($res[$i]['fieldid'], $Itemfields)) {
                        $res[$i]['value'] = $Itemfields[$res[$i]['fieldid']];
                    } //END IF
                } //END IF
            } //END FOR
           
            
            return $res;
        } //END IF

    } //END FUNCTION

    function getItemGroups($id)
    {
        global $sql;
        $sql->db_Select('extra_fields_groups_items','groupid,orderby',"itemid='".$id."'");
        //echo 'SELECT '.'groupid,orderby'.' FROM '.'extra_fields_groups_items'.' WHERE '."itemid='".$posted_data['fieldid']."'";
        if($sql->db_Rows())
        {
            $data=execute_multi($sql);
            foreach($data as $item)
            {
                $prev[$item['groupid']]=$item['orderby'];
            }
            //unset($data);
        }
        
        return $prev;
    }
    
    function ModifyExtraField($posted_data, $settings = 0)
    {
        global $sql;
        
        $parse = new Parse();        
        
        foreach ($posted_data as $k => $v) {
            if (strstr($k, "efieldKey_")) {
                $keys[str_replace("efieldKey_", "", $k)] = $v;
            }
        }
        
        $groups=$posted_data['group_list'];

        foreach ($posted_data as $k => $v) {
            if (strstr($k, "efieldVal_")) {
                $vals[str_replace("efieldVal_", "", $k)] = $v;
            }
        }

        if($keys)
        foreach ($keys as $k => $v) {
            $data_settings[$v] = $parse->toDB($vals[$k]);
        }

        foreach ($posted_data as $k => $v) {
            if (strstr($k, "settings_")) {
                $tmp[str_replace("settings_", "", $k)] = $parse->toDB($v);
            }
        }
        $tmp['data'] = $data_settings;
        if (is_array($tmp)) {
            $settings_query = ",settings = '" . json_encode($tmp) . "'";
        } //END IF
       
        
        //set groups
        if($groups)
        {
            $sql->db_Select('extra_fields_groups_items',',groupid,orderby',"itemid='".$posted_data['fieldid']."'");
            if($sql->db_Rows())
            {
                $data=execute_multi($sql);
                foreach($data as $item)
                {
                    $prev[$item['groupid']]=$item['orderby'];
                }
                //unset($data);
            }
            
            $sql->db_Delete("extra_fields_groups_items","itemid='".$posted_data['fieldid']."'");
            
            foreach ($groups as $item)
            {
                if($prev){
                if(key_exists($item,$prev)) $order = $prev[$item];
                else $order=0;
                }
                else $order=0;
                $sql->db_insert("extra_fields_groups_items","'".$posted_data['fieldid']."','".$item."','".$order."'");
            }
        }


        $query = "field = '" . $posted_data['field'] . "',var_name = '" . $posted_data['var_name'] .
            "',type = '" . $posted_data['type'] . "',active = '" . $posted_data['active'] .
            "',image = '" . $posted_data['image'] . "' $settings_query";
        $sql->db_Update('extra_fields', $query . " WHERE fieldid =  " . $posted_data['fieldid'] .
            " AND module = '" . $this->module['name'] . "'");
        if ($settings['debug']) {
            echo ("UPDATE extra_fields SET $query WHERE fieldid =  " . $posted_data['fieldid'] .
                " AND module = '" . $this->module['name'] . "'");
        } //END IF

        ############ UPDATE CATEGORIES ####
        $sql->db_Select("extra_field_categories", "catid,orderby", "module = '" . $this->module['name'] . "' AND fieldid = " . $posted_data['fieldid']);
        // echo "SELECT catid,orderby FROM extra_field_categories WHERE module = '".$this->module['name']."' AND fieldid = ".$posted_data['fieldid'];
        if ($sql->db_Rows()) {
            $old_ids = execute_multi($sql);
            foreach ($old_ids as $v) {
                $simple_ids[$v['catid']] = $v['orderby'];
                $old_ids_flat[] = $v['catid'];
            }
        }
        $counter = count($simple_ids);
        
        if ($posted_data['categoryids'] == '0')
        {
        $sql->db_Delete("extra_field_categories","fieldid = ".$posted_data['fieldid']);//CLEAN UP
        }
        elseif  ($posted_data['categoryids'] == '%') {//APPLY TO ALL CATEGORIES
        $sql->db_Delete("extra_field_categories","fieldid = ".$posted_data['fieldid']."AND module = '".$this->module['name']."'");//CLEAN UP
        // 	GET ALL CATEGORY IDS 
        $sql->db_Select($this->tables['category'],"categoryid");
        $res = execute_multi($sql);
        foreach ($res as $v)
        {
        $counter++;
        $sql->db_Insert("extra_field_categories",$v['categoryid'].",".$posted_data['fieldid'].",'".$this->module['name']."',$counter");
        }
        }
        else {//WE HAVE INDIVIDUAL CATEGORIES
        // 	$sql->db_Delete("extra_field_categories","fieldid = ".$posted_data['fieldid']);//CLEAN UP
        $existing = array();
        foreach ($posted_data['categories_list'] as $k=>$v)
        {
        if (@!array_key_exists($v,$simple_ids)) {
        	$counter++;
        	$sql->db_Insert("extra_field_categories","$v,".$posted_data['fieldid'].",'".$this->module['name']."',$counter");
        }
        else {
        $existing[] = $v;
        }
        
        }//END FOREACH
        if (is_array($old_ids) AND is_array($existing)) {
        $to_delete = array_diff($old_ids_flat,$existing);
        if (is_array($to_delete)) {
        //		echo "fieldid = ".$posted_data['fieldid']." AND catid IN (".implode(',',$to_delete).") AND module = '".$this->module['name']."'";
        	$sql->db_Delete("extra_field_categories","fieldid = ".$posted_data['fieldid']." AND catid IN (".implode(',',$to_delete).") AND module = '".$this->module['name']."'");
        }
        }
        }//END SELECTION
        
        return array('fieldid' => $posted_data['fieldid'], 'field' => $posted_data['field'] ,'var_name' => $posted_data['var_name'],'type' => $posted_data['type'],'active' => $posted_data['active'] );
    } //END FUNCTION

    function AddExtraField($posted_data, $settings = 0)
    {
        global $sql;
        
        $parse = new Parse();/*
        foreach($posted_data as $k=>$v)
        {
            $posted_data[$k]=$parse->toDB($v);
        }        
        */
        foreach ($posted_data as $k => $v) {
            if (strstr($k, "efieldKey_")) {
                $keys[str_replace("efieldKey_", "", $k)] = $v;
            }
        }

        $groups=$posted_data['group_list'];
        
        foreach ($posted_data as $k => $v) {
            if (strstr($k, "efieldVal_")) {
                $vals[str_replace("efieldVal_", "", $k)] = $v;
            }
        }
        if ($keys)
            foreach ($keys as $k => $v) {
                $data_settings[$v] = $vals[$k];
            }

        foreach ($posted_data as $k => $v) {
            if (strstr($k, "settings_")) {
                $tmp[str_replace("settings_", "", $k)] =  $parse->toDB($v);
            }
        }
        $tmp['data'] = $data_settings;
        if (is_array($tmp)) {
            $settings_query = json_encode($tmp);
        } //END IF
        
        

        $query = "'" . $this->module['name'] . "','" . $posted_data['active'] . "','" .
            $posted_data['type'] . "','" . $posted_data['var_name'] . "','" . $settings_query .
            "',
 '" . $posted_data['field'] . "'";
        $sql->db_Insert('extra_fields (module,active,type,var_name,settings,field)', $query);
        if ($settings['debug']) {
            echo ("INSERT INTO extra_fields VALUES ($query)");
        } //END IF
        //echo ("INSERT INTO extra_fields VALUES ($query)");
        $new_id = $sql->last_insert_id;
        
        
        if($groups)
        {
            foreach ($groups as $item)
            {
                $sql->db_insert("extra_fields_groups_items","'".$new_id."','".$item."','0'");
            }
        }
        
        ############ UPDATE CATEGORIES ####
        if ($posted_data['categoryids'] == '0')
        {
        $sql->db_Delete("extra_field_categories","fieldid = ".$posted_data['fieldid']);//CLEAN UP
        }
        elseif  ($posted_data['categoryids'] == '%') {//APPLY TO ALL CATEGORIES
        $sql->db_Delete("extra_field_categories","fieldid = ".$posted_data['fieldid']);//CLEAN UP
        // 	GET ALL CATEGORY IDS 
        $sql->db_Select($this->tables['category'],"categoryid");
        $res = execute_multi($sql);
        
        foreach ($res as $v)
        {
        $counter++;
        $sql->db_Insert("extra_field_categories",$v['categoryid'].",".$new_id.",'".$this->module['name']."',$counter");
        //echo 'INSERT INTO extra_field_categories('.$v['categoryid'].",".$new_id.",'".$this->module['name']."',$counter".') <br />';
        }
        }
        else {//WE HAVE INDIVIDUAL CATEGORIES
            foreach ($posted_data['categories_list'] as $k=>$v)
        	{
        		$counter++;
        		$sql->db_Insert("extra_field_categories","$v,".$new_id.",'".$this->module['name']."',$counter");
        		echo 'INSERT INTO extra_field_categories('."$v,".$new_id.",'".$this->module['name']."',$counter".') <br />';
        	}
        }//END SELECTION
        
        return array('action'=> 'insert','fieldid' => $new_id, 'field' => $posted_data['field'] ,'var_name' => $posted_data['var_name'],'type' => $posted_data['type'],'active' => $posted_data['active'] );
       
        return $new_id;
    } //END FUNCTION
    
    function getExtraFieldID($var_name)
    {
        global $sql;
        $sql->db_Select("extra_fields","fieldid","var_name='$var_name'");
        $data = execute_single($sql);
        return $data["fieldid"];
    }
    
    function getExtraFieldValuesByItemID($itemid)
    {
        global $sql;
        $sql->db_Select("extra_field_values","fieldid,value","itemid='$itemid'");
        $data = execute_multi($sql);
        
        if($data)
        foreach($data as $k=>$v)
        {
            
            $efield = $this->ExtraField($v['fieldid']);            
            $data[$k]["fieldname"]= $efield['field'];
            if($efield['type']=="multiselect")
                $data[$k]["fieldvalue"]=$efield['settings']['data'][$v['value']];
            else
              $data[$k]["fieldvalue"]=$v['value'];
        }
        return $data;
    }
    
    
    function searchGroups($settings)
    {
        $this->tables['extra_fields_groups'] = array('table'=>'extra_fields_groups','key'=>'id');
        $this->tables['extra_fields_groups_items'] = array('table'=>'extra_fields_groups_items');
        global $sql;
        
        $fields = (!$settings['fields']) ? "*" : $settings['fields'];
        $orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
    	$way = ($settings['way']) ? "  ".$settings['way'] : "";
    	$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
    	$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
    	if (is_array($settings['searchFields'])) {
    		foreach ($settings['searchFields'] as $k => $v) {
    			$q[] = "$k = '$v'";
    		}
    	}//END SETUP SEARCH FIELDS
    	if(!$settings['searchFields']['archived']==1)
    	{
    		$settings['searchFields']['archived']=0;
    	}
    	if (is_array($settings['searchFieldsLike'])) {
    		foreach ($settings['searchFieldsLike'] as $k => $v) {
    			$q[] = "$k LIKE '%$v%'";
    		}
    	}//END SETUP SEARCH FIELDS WITH LIKE
    	
    	if (is_array($settings['greaterThan'])) {
    		foreach ($settings['greaterThan'] as $k => $v) {
    			$q[] = $k." > ".$v;
    		}
    	}//END SETUP SEARCH FIELDS WITH LIKE
    	
    	if (is_array($settings['lessThan'])) {
    		foreach ($settings['lessThan'] as $k => $v) {
    			$q[] = $k." < ".$v;
    		}
    	}
    	
        if (is_array($settings['missingFields'])) {
    		foreach ($settings['missingFields'] as $k => $v) {
    			$q[] = "$k != '$v'";
    		}
    	}//END SETUP SEARCH FIELDS
    	 
        if($q)
        {
            $query=implode(" AND ",$q);
        }

        
        if (!$query) 
		{
			$query_mode='no_where';
		}
		else {
			$query_mode = 'default';
		}
        
        if($settings['itemCount']==1)
        {
            $sql->db_Select("extra_fields_groups_items GROUP by groupid","groupid,count(itemid) as itemscount");
            if($sql->db_Rows())
            {
                $itemscount=execute_multi($sql);
                foreach ($itemscount as $item)
                {
                    $itemCount[$item["groupid"]]=$item['itemscount'];
                }
            }
            //print_r($itemCount);
        }
        
		if ($settings['return'] == 'multi') //Return all results, NO PAGINATION
		{
			$sql->db_Select($this->tables['extra_fields_groups']['table'],$fields,"$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT ".$fields." FROM ".$this->tables['extra_fields_groups']['table']." WHERE $query $orderby $way $limit<br>";
			}
			if ($sql->db_Rows()) 
			{
				$res = execute_multi($sql);				
			}
            
            if($settings['itemCount']==1)
            {
                $sql->db_Select("extra_fields_groups_items GROUP by groupid","groupid,count(itemid) as itemscount");
                
                if($sql->db_Rows())
                {
                
                    $itemscount=execute_multi($sql);
                    
                    foreach ($itemscount as $item)
                    {
                        $itemCount[$item["groupid"]]=$item['itemscount'];
                    }
                }
                //print_r($itemCount);
                for($i=0;$i<count($res);$i++)
                {
                    if($itemCount[$res[$i]['id']]!="")
                    $res[$i]['count']=$itemCount[$res[$i]['id']];
                    else
                    $res[$i]['count']=0;
                }
            }
		}//END ALL RESULTS
		elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
		{
			$sql->db_Select($this->tables['extra_fields_groups']['table'],'id',"$query $orderby $way",$query_mode);//GET THE TOTAL COUNT
			
				if ($settings['debug']) 
				{
					echo "SELECT id FROM ".$this->tables['extra_fields_groups']['table']." WHERE $query $orderby $way<br>";
				}
			if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
			{
				$total = $sql->db_Rows();
				$current_page = ($settings['page']) ? $settings['page'] : 1;
				$results_per_page =  $settings['results_per_page'];
				if (isset($settings['start'])) 
				{
					$start = $settings['start'];
				}
				else {
					$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
				}
				$limit = "LIMIT $start,".$results_per_page;
				$sql->db_Select($this->tables['extra_fields_groups']['table'],$fields,"$query $orderby $way $limit",$query_mode);
				if ($settings['debug']) {
					echo "<br>SELECT $fields FROM ".$this->tables['extra_fields_groups']['table']." WHERE $query $orderby $way $limit";
				}
				$res = execute_multi($sql,1);
				paginate_results($current_page,$results_per_page,$total);
				
			}//END FOUND RESULTS
		}//END PAGINATION        
    	elseif ($settings['return'] == 'single') // RETURN PAGINATED RESULTS
    	{
			$sql->db_Select($this->tables['extra_fields_groups']['table'],$fields,"$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT ".$fields." FROM ".$this->tables['extra_fields_groups']['table']." WHERE $query $orderby $way $limit<br>";
			}
			if ($sql->db_Rows()) 
			{
				$res = execute_single($sql);	
			}
			
			
			if ($settings['getUserDetails']) {
						$res['userDetails'] = $this->users->userDetails($res['uid'],array('fields'=>'id,uname'));
			}//END USER DETAILS
    				
    	}
    	elseif ($settings['return'] == 'count') // RETURN COUNT RESULTS
		{
			$sql->db_Select($this->tables['extra_fields_groups']['table'],"count(".$this->tables['extra_fields_groups']['table'].".id) as total","$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT count(".$this->tables['extra_fields_groups']['table'].".id) as total FROM ".$this->tables['extra_fields_groups']['table']." WHERE $query $orderby $way $limit<br>";
			}
			$res = execute_single($sql);
			return $res['total'];
		}
        return $res;
    }
    
    function getGroupItems($groupID,$fullItems=0)
    {
        global $sql;
        $i=0;
        $sql->db_Select('extra_fields_groups_items',"itemid,orderby","groupid='$groupID' ORDER BY orderby ASC");
        
        if($sql->db_Rows()>0)
        {
            $items = execute_multi($sql);
          
            if($fullItems==0)
            {               
                foreach($items as $item)
                {
                    $res[$item['itemid']]=$item['orderby'];
                }
            }
            else
            {
                $i=0;
                foreach($items as $item)
                {
                    $res[$i]=$this->ExtraField($item['itemid']);
                    $i++;
                }
            }
        }
        else
        {
            if(!is_array($res)) $res = array();
        }
        
        return $res;
    }
    
    function getGroup($groupID)
    {
        global $sql;
        $sql->db_Select('extra_fields_groups',"*","id='$groupID'");
        $group = execute_single($sql);
        $group["items"] = $this->getGroupItems($groupID);
        $group['settings']=json_decode($group['settings'],true);
        return $group;
    }
    
    function newGroup($settings)
    {
        global $sql;
        
        foreach($settings as $k=>$v)
        {
        	if ($k == 'themes') {
                $data['themes'][] = $v;
            }
        }

        $data = json_encode($data);
        
        $sql->db_Insert("extra_fields_groups (title,description,settings,module)","'".$settings['title']."','".$settings['description']."','$data','".$settings['module']."'");
        $newid=$sql->last_insert_id;
        $i=0;
        foreach($settings['items'] as $k=>$v)
        {    
            $sql->db_insert("extra_fields_groups_items","'$v','$newid','".$i++."'");
        }
    	
        return array('count' => $i , 'title' => $settings['title'], 'description' => $settings['description'],"id" => $newid );
        
    }
    
    function updateGroup($settings)
    {
        global $sql;

        foreach($settings as $k=>$v)
        {
            if ($k == 'themes') {
                $data['themes'][] = $v;
            }
        }

        $data = json_encode($data);
        
        $sql->db_Update("extra_fields_groups","title='".$settings['title']."',description='".$settings['description']."',settings='$data' , module='".$settings['module']."' WHERE id='".$settings['groupid']."'");
        $sql->db_Delete("extra_fields_groups_items","groupid='".$settings['groupid']."'");
		
        $i=0;
      	if (!empty($settings['items'])) {  
        	foreach($settings['items'] as $k=>$v)
        	{       	
        	  $sql->db_insert("extra_fields_groups_items","'$v','".$settings['groupid']."','".$i++."'");	  
        	}
      	}  
		return array('count' => $i , 'title' => $settings['title'], 'description' => $settings['description'],'id' => $settings['groupid'] );
    }
    
    function removeGroup($settings)
    {
        global $sql;
        
        $sql->db_Delete("extra_fields_groups","id='".$settings['groupid']."'");
        $sql->db_Delete("extra_fields_groups_items","groupid='".$settings['groupid']."'");
        
        echo "reload";
    }
    
    function getEFieldsGroups($settings)
    {
        global $sql;
        
        if($settings['theme'])
        {
            $themeID=$settings['theme'];
            $args = <<< EOF
            settings REGEXP '.*"themes";a:[0-9]+:{[^{]*s:[0-9]+:"$themeID".*}' ORDER BY orderby ASC
EOF;
         
        
            $sql->db_Select("extra_fields_groups","*",$args);
            
            
            $groups = execute_multi($sql);
            for($i=0;$i<count($groups);$i++)
            {
                $groups[$i]['settings']=json_decode($groups[$i]['settings'],true);
                $groups[$i]["items"] = $this->getGroupItems($groups[$i]['id'],1);
            }
        }
        //$query="SELECT * FROM extra_fields_groups WHERE settings REGEXP '.*\"themes\";a:[0-9]+:{[^{]*s:[0-9]+:\"25\".*}'";
        return $groups;
    }//END FUNCTION
    
    
    function filterItemsEfields($itemIDs,$module=0,$settings=0)
    {
        /*
        	itemIDs set to zero will bring all results
        	if we need to bring zero values to overwrite at some point we use $settings['defZero']
        */
    	global $sql;
        global $Languages;
    	$module = ($module) ? $module :$this->module;
    	$tables = "( SELECT v.value, v.fieldid, f.var_name, f.field, f.type";
    	if ($settings['grouped']) {
    		$tables .= ",g.groupid,g.orderby";
    	}
    	if (defined('TRANSLATE')) {
    		$tables .= " ,(  SELECT translation FROM translations WHERE itemid = v.fieldid AND code = 'gr' AND module = 'listings' AND translations.table = 'extra_field_values' AND 
    		(settings LIKE '%\"field\":\"itemid\"%' AND settings LIKE CONCAT('%\"value\":',v.itemid,'%')) ) as translation";
		}
    	if ($settings['count']) {
    		$tables .= " ,COUNT(*) AS qty ";
    	}
    	$tables .= "FROM extra_field_values AS v
    				INNER JOIN extra_fields AS f ON f.fieldid=v.fieldid";
    	if ($settings['grouped']) {
    		$fields[] = "x.groupid,x.orderby ";
    		$tables .= " INNER JOIN extra_fields_groups_items AS g ON g.itemid=v.fieldid ";
    	}
    	$tables .= "WHERE f.module = '$module'";
    	if ($itemIDs) {
    		$tables .= " AND v.itemid IN ($itemIDs)";
    	}
    	$fields[] = "x.fieldid, x.var_name, x.field, x.type ";
    	if ($settings['searchable']) {
    		$tables .= " AND f.settings LIKE '%\"search\":\"1\"%' ";
    	}
    	if ($settings['values']) {
    		$defVal = (!$settings['defZero']) ? 'x.qty' : '0';
    		if (defined('TRANSLATE')) {
    			$fields[] = "IF(x.translation IS NULL,
							  CAST(GROUP_CONCAT(CONCAT(x.value,':::',$defVal) SEPARATOR '###') AS CHAR) ,
							  CAST(GROUP_CONCAT(CONCAT(x.value,':::',$defVal,':::',x.translation) SEPARATOR '###') AS CHAR)
							  ) a";
    		}
    		else {
    			$fields[] = "CAST(GROUP_CONCAT(CONCAT(x.value,':::',$defVal) SEPARATOR '###') AS CHAR) a";
    		}
    	}
    	if ($settings['count']) {
    		$fields[] = "COUNT(*) AS total";
    	}

    	$qFields = implode(',',$fields);
   		$tables .= "GROUP BY v.value, v.fieldid ) as x";
    	$sql->db_Select($tables,$qFields,"GROUP BY x.fieldid ORDER BY x.groupid ASC,x.orderby ASC",$mode="no_where");
    	if ($settings['debug']) {
    		echo "SELECT $qFields FROM $tables GROUP BY x.fieldid<br><br>";
    		echo mysql_error();
    	}
//    	echo "SELECT $qFields FROM $tables GROUP BY x.fieldid<br><br>";
    	if ($sql->db_Rows()) {
    		$r = execute_multi($sql);
    		if ($settings['mergeWithItems']) {
    			foreach ($r as $v){
    				$fieldIDs[] = $v['fieldid'];
    			}
    			$table = "(SELECT v.value,v.fieldid, COUNT(*) AS qty FROM extra_field_values AS v 
				INNER JOIN extra_fields AS f ON f.fieldid=v.fieldid
				WHERE 
				v.fieldid IN (".implode(',',$fieldIDs).") AND v.itemid IN (".$settings['mergeWithItems'].") ";
    			if ($settings['searchable']) {
    				$table .= "AND f.settings LIKE '%\"search\":\"1\"%'";
    			}
 				$table .= "GROUP BY v.value, v.fieldid) x";
 				$field = "x.fieldid,CAST(GROUP_CONCAT(CONCAT(x.value,':::',x.qty) SEPARATOR '###') AS CHAR) vals";
    			$sql->db_Select($table,$field,"GROUP BY x.fieldid",$mode="no_where");
    			if ($sql->db_Rows()) {
    				$tmp = execute_multi($sql);
    				foreach ($tmp as $v) {
    					$found[$v['fieldid']] = form_settings_array($v['vals']);
    				}
    			}
    		}
            foreach ($r as $k=> $v)
    		{

    			$r[$k]['values'] = explode('###',$v['a']);

                if (defined('TRANSLATE')) {
					$translations = $Languages->itemTranslations($r[$k]['fieldid'],TRANSLATE,$module,array('debug'=>0,'groupby'=>'field','table'=>'extra_fields'));
                    if ($translations) {
            			foreach ($translations as $key=>$val)
            			{
            				if ($val['translation']) {
            					$r[$k][$key] = $val['translation'];
            				}
            			}
                    }
				}
                foreach ($r[$k]['values'] as $key=>$t)
    			{
    				list($val,$total,$trans)=split(":::",$t);
    				$r[$k]['values'][$key] = array('value'=>$val,'total'=>$total,'translation'=>$trans);
    				if (is_array($found[$v['fieldid']]) AND array_key_exists($r[$k]['values'][$key]['value'],$found[$v['fieldid']])) {

    					$r[$k]['values'][$key]['total'] = $found[$v['fieldid']][$r[$k]['values'][$key]['value']];

    				}
//   					echo $r[$k]['fieldid']." --- ".$r[$k]['values'][$key]['value']."<br>";
/*    				if (defined('TRANSLATE')) {
    					$translations = $Languages->itemTranslations($r[$key]['fieldid'],TRANSLATE,$module,array('debug'=>0,'groupby'=>'field','table'=>'extra_field_values'));
    					if ($translations) {
    						foreach ($translations as $key1=>$val)
    						{
	            				if ($val['translation']) {
	            					$r[$key][$key1] = $val['translation'];
	            				}
    						}//END LOOP
    					}//END TRANSLATE
					}//END TRANSLATIONS*/
    			}
    			
    		}
	
    		if ($settings['grouped']) {
	    		foreach ($r as $v)
	    		{
	    			$fields[$v['fieldid']] = $v;
	    			$allFields[] = $v['fieldid'];
	    		}
	    		$sql->db_Select("extra_fields_groups_items INNER JOIN extra_fields_groups ON (extra_fields_groups.id=extra_fields_groups_items.groupid)","title,id","itemid IN (".implode(',',$allFields).") AND module = '$module' GROUP BY id");
	    		$g = execute_multi($sql);
                
                 if (defined('TRANSLATE')) {
                        foreach($g as $key=>$val)
                        {
                    			global $Languages;
                    			$translations = $Languages->itemTranslations($g[$key]['id'],TRANSLATE,$module,array('debug'=>0,'groupby'=>'field','table'=>'extra_fields_groups'));
                    			if ($translations) {
                    			foreach ($translations as $k=>$v)
                    			{
                    				if ($v['translation']) {
                    					$g[$key][$k] = $v['translation'];
                    				}
                    			}
                    			}
                  		}//END TRANSLATING
                        //print_r($group);
                        //print_r($item);
                    }
                    
                
	    		foreach ($g as $v)
	    		{
	    			$groups[$v['id']] = $v;
	    		}
	    		foreach ($r as $v)
	    		{
	    			if (array_key_exists($v['groupid'],$groups)) {
	    				$groups[$v['groupid']]['data'][] = $v;
	    			}
	    		}
	    		return $groups;
			}//END GROUPS    	
			return $r;	
    	}//END ROWS
    	
    }//END FUNCTION
} //END CLASS

?>
