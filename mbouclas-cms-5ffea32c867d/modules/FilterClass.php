<?php

class FilterClass {
	
	
	public $operators = array( "eq" => "=", "neq" => "!=", "lt" => "<", "lte" => "<=", "gt" => ">", "gte" => ">=" , "startswith" => " LIKE ':::%'" ,"endswith" => "LIKE '%:::'" ,"contains" => "LIKE '%:::%'" );

	
	
	private $CurFilter = array();
	
	private $fields = array("*");
	
	private $tables = array();
	
	private $where = array();
	
	private $limit = array( 'start' => 0 , 'end' => 10);
	
	public function __construct(Array $settings) {
		
		
		
		if (empty($settings['filter'])) { return; }
		
		/*
		$tables= array(	"users" => array(	"pk" => "id" ,"dep" => array( "users_audience"=> "uid", "users_audience_list_members"=>"uid", "users_filter_query"=>"uid" )	),
									"users_audience" => array(	"pk" => "id", "dep" => array("user_audience_details"=>"user_id","users_audience_lists" => "uid","users_audience_list_members"=>"audienceid","users_audience_charge"=>"user_id")	),
									"user_audience_details" => array("pk" => "id" ) ,
									"users_audience_lists" => array("pk" => "id", "dep" => array("users_audience_list_members"=>"listid") ),
									"users_audience_list_members",
									"users_audience_charge" => array("pk" => "id"),
									"users_filter_query" => array("pk" => "id")
		);
		
		$fields = array( 
									"users" => array ( "uname" ),
									"users_audience" => array( "name","surname","phone","mobile","email" )
		);
	
		
		$filter['filters']= array('field' => 'listid', 'table' => 'users_audience_list_members'  );
			*/
		
		$this->limit = (!empty($settings['limit'])) ? $settings['limit'] : $this->limit;
		$this->fields = (!empty($settings['fields'])) ? $settings['fields'] : $this->fields;
		$this->tables = (!empty($settings['tables'])) ? $settings['tables'] : $this->tables;
		

		foreach ( $settings['filter']['filters'] as $key => $value ) {
			if (!is_array($value['filters'])) {
				$this->CurFilter[$value['operator']][]=array ("table"=> $value['table'], "field" =>  $value['field'], "value" => $value['value']);
			} else {
				foreach ($value['filters'] as $k => $v) {
					$this->CurFilter[$v['operator']][]=array ("table"=> $v['table'], "field" =>  $v['field'], "value" => $v['value']);
				}
			}
		}

				
		foreach ($this->CurFilter as $key => $value) {
			$this->buildwhere($key);
		}
	
	}
	
	
	public function buildwhere($op) {
		
		foreach ($this->CurFilter[$op] as $key => $value) {
			$pos = strpos($this->operators[$op], ':::');
		
			 if ( $pos === false ) {
       				$this->where[$value['table']][] = "`".$value['table']."`.".$value['field'].$this->operators[$op]."'".$value['value']."'";
    		} else {			
    				$replace = str_replace(':::', $value['value'] ,$this->operators[$op],$replace);
					$this->where[$value['table']][] = "`".$value['table']."`.".$value['field']. " ".$replace;
			 }
		}
		
	}
	
	
	public function buildquery() {	
		
		foreach ($this->where as $key => $value ) {
				$q[$key] =implode(" and ", $value); 
		}
		
		
		
	
		foreach ($q as $key => $value ) {
			$res = $this->qexec($key,$value);
		}

		return $res;
	}
	
	private function buildlimit() {
		return " limit ".$this->limit[start].",".$this->limit['end'];
	}
	
	
	private function qexec($key,$value) {
		
			global $sql;
	
			$sql->db_Select($key,implode(",",$this->fields),$value.$this->buildlimit());
			$res=execute_multi($sql);	
		
			$sql->db_Select($key,"count(*) as c ",$value);
			$number=execute_single($sql);	

			return array("data" => $res, 'number' => $number['c']);
	}
	
	
}