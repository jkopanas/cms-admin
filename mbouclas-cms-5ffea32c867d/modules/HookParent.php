<?php

/**
 * Examples of Use
 * 
 * INSERT INTO `modules` (`name`, `folder`, `active`, `startup`, `main_file`, `menu_title_admin`, `orderby`, `is_item`, `on_menu`) 
 * VALUES 
 * 		('hookParent', 'modules/hookParent', '1', '1', 'hookParent', 'HookParent', '0', '0', '0')
 * 
 *	function testFunck()
 * 	{
 * 		echo "DONE..!!!!";
 * 		return true;
 * 	}
 * 	HookParent::getInstance()->doBindHook("module", "testEvent", "testFunck");
 * 	
 * 	HookParent::getInstance()->doTriggerHook("module", "testEvent");
 * 
 */


/**
 * The Modular Hook System of MCMS
 * @author 
 *
 */
class HookParent
{
	private $hookAnchors; // Hook Anchors - hooks System
	private $hookObservers; // Observables
	private $hookObservables; // Observers
	private $resourcePool; // Resources Pool
	private $errorLog;
	/**
	 * The Singleton Instance Pattern
	 * @var HookParent
	 */
	private static $_INSTANCE = NULL;
	
	/**
	 * Builds The Instance of Hooks modular system
	 */
	function __construct()
	{
		$this->init();
	}
	
	/**
	 * Builds The Instance of Hooks modular system
	 */
	function init()
	{
		// Singleton Pattern
		if(self::$_INSTANCE === null)
		{
			// Building the global scope as it 
			// is vital to be used, for global 
			// or Module-free bindings
			$this->hookAnchors 					= array();
			$this->hookAnchors['global'] 		= array();
			$this->hookObservers 				= array();
			$this->hookObservers['global'] 		= array();
			$this->hookObservables 				= array();
			$this->hookObservables['global'] 	= array();
			$this->resourcePool 				= array();
			$this->resourcePool['global'] 		= array();
			$this->errorLog 					= array();
			
			self::$_INSTANCE = &$this;
		}
	}
	
	/**
	 * Caches a Resource that wishes to be accesable 
	 * by tagging a scope.
	 * 
	 * @param string $module
	 * @param string $resourceKey
	 * @param unknown_type $resource
	 * @return HookParent
	 */
	public function doCacheResource($module,$resourceKey,&$resource)
	{
		$this->setScope($module);
		$this->resourcePool[$module][$resourceKey] = &$resource;
		return $this;
	}
	
	/**
	* Caches a Resource that wishes to be accesable Globally.
	*
	* @param string $resourceKey
	* @param unknown_type $resource
	* @return HookParent
	*/
	public function doCacheResourceGlobally($resourceKey,&$resource)
	{
		$this->doCacheResource('global',$resourceKey,&$resource);
		return $this;
	}
	
	/**
	* Fetches a Cached Resource that is accesable by tagging a scope.
	*
	* @param string $module
	* @param string $resourceKey
	* @return unknown_type
	*/
	public function doFetchResource($module,$resourceKey)
	{
		if(isset($this->resourcePool[$module][$resourceKey]))
		{
			return $this->resourcePool[$module][$resourceKey];
		}
		return false;
	}
	
	/**
	 * Fetches a Cached Resource that is accesable Globally.
	 *
	 * @param string $resourceKey
	 * @param unknown_type $resource
	 * @return HookParent
	 */
	public function doFetchResourceGlobally($resourceKey)
	{
		return $this->doFetchResource('global',$resourceKey);
	}
	
	/**
	* Removes a Resource that was accesable by a scope.
	*
	* @param string $module
	* @param string $resourceKey
	* @return HookParent
	*/
	public function doUnCacheResource($module,$resourceKey)
	{
		unset($this->resourcePool[$module][$resourceKey]);
		return $this;
	}
	
	/**
	 * Removes a Resource that was accesable Globally.
	 *
	 * @param string $resourceKey
	 * @return HookParent
	 */
	public function doUnCacheResourceGlobally($resourceKey)
	{
		$this->doCacheResource('global',$resourceKey);
		return $this;
	}
	
	/**
	* Registers a Hook Key, so it may be executed
	* dynamicaly as a trigger on an event. 
	* It Is bind to a module scope, so it may trigger
	* inside a sandbox of permisions, 
	* otherwise it should be registered as global.
	* 
	* @param string $hookModule
	* @param string $hookKey
	* @param string $hookVal
	* @return HookParent
	*/
	public function doBindHook($hookModule,$hookKey,$hookVal=false)
	{
		$this->setScope($hookModule);
		$this->hookAnchors[$hookModule][$hookKey] = ($hookVal ? $hookVal : $hookKey);
		return $this;
	}
	
	/**
	 * registers a hook Globally.
	 * @see HookParent::doBindHook($hookModule,$hookKey,$hookVal)
	 * @param string $hookKey
	 * @param string $hookVal
	 * @return HookParent
	 */
	public function doBindHookGlobally($hookKey,$hookVal=false)
	{
		return $this->doBindHook('global', $hookKey,$hookVal);
	}
	
	/**
	 * Unbind a request from its Scope
	 * @param string $hookModule
	 * @param string $hookKey
	 * @return HookParent
	 */
	public function doUnBindHook($hookModule,$hookKey)
	{
		unset($this->hookAnchors[$hookModule][$hookKey]);
		return $this;
	}
	
	/**
	 * Globally Unbind a request
	 * @param string $hookKey
	 */
	public function doUnBindHookGlobally($hookKey)
	{
		return $this->doUnBindHook('global', $hookKey);
	}
	
	/**
	 * Execute the trigger accordingly to the given Scope
	 * @param string $hookModule
	 * @param string $hookKey
	 * @param unknown_type $args
	 * @return boolean|unkown
	 */
	public function doTriggerHook($hookModule,$hookKey,&$args=0)
	{
		$return=null;
		try
		{
			$return = @call_user_func($this->hookAnchors[$hookModule][$hookKey],$args);
			//$return = $this->hookAnchors[$hookModule][$hookKey]($args);
		}
		catch (Exception $e)
		{
			array_push($this->errorLog, $e);
		}
		return $return;
	}
	
	/**
	 * Execute this trigger in the Global Scope
	 * @param string $hookKey
	 * @param unknown_type $args
	 * @return boolean|unkown
	 * @return HookParent
	 */
	public function doTriggerHookGlobally($hookKey,&$args=0)
	{
		return $this->doTriggerHook("global", $hookKey, $args);
	}
	
	/**
	 * Generates an empty Scope, with no binds
	 * @param string $hookModule
	 */
	public function setScope($hookModule)
	{
		if(!isset($this->hookAnchors[$hookModule]))
		{
			$this->hookAnchors[$hookModule] = array();
		}
		if(!isset($this->resourcePool[$hookModule]))
		{
			$this->resourcePool[$hookModule] = array();
		}
		return $this;
	}
	
	/**
	 * Returns The Last error of the Hook System
	 * @return mixed|boolean       print_ar($this->AvailPlugin);
       exit;
	 */
	public function getLastError()
	{
		if(count($this->errorLog)>0)
		{
			return array_pop($this->errorLog);
		}
		return false;
	}
	
	/**
	 * Returns the Instance of the running Singleton
	 * @return HookParent
	 */
	public static function getInstance()
	{
		if(self::$_INSTANCE === null)
		{
			// Force Singleton's initialization
			new HookParent();
		}
		return self::$_INSTANCE;
	}
}