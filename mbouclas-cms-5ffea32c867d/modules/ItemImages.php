<?php
class ItemImages
{
	var $itemid;
	var $module;
	var $settings;
	
	function ItemImages($settings)
	{
		$this->itemid = $settings['itemid'];
		$this->module = $settings['module'];
		$this->settings = $settings;
		
	}//END FUNCTION
	
	function GetItemDetailedImages($itemid,$type,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : '*';
		$limit_q = ($settings['images_settings_limit']) ? " LIMIT ".$settings['images_settings_limit'] : "";
        
		if (isset($type) AND ($type OR $type == 0)) {
        $type = " AND type = ".$type;
        }//END IF
        
	if ($settings['images_settings_available']  OR $settings['images_settings_available'] == 0) 
	{
		if ($settings['images_settings_available'] == 1) 
			{
				$q[] = " available = 1";
			}
			elseif (isset($settings['images_settings_available']) AND $settings['images_settings_available'] == 0)
			{
				$q[] = " available = 0";
			}
			else 
			{
				$q[] = " available IN (1,0) ";
			}
		}
        
		$sql->db_Select('item_images',$fields,"module = '".$this->module['name']."' AND itemid = ".$itemid.$type." and ".implode(" and ", $q)."  ORDER BY orderby ".$limit_q);
        
//		echo "SELECT $fields FROM item_images WHERE module = '".$this->module['name']."' AND itemid = ".$itemid.$type." ORDER BY orderby <br>";
		if ($sql->db_Rows()) {
			$images = execute_multi($sql);

			//GET ALL COPIES IN ONE QUERY INSTEAD OF MANY
			foreach ($images as $v) {
				$all_ids[] = $v['id'];
			}//END FOREACH
			$sql->db_Select('item_images_aditional','*',"imageid IN (".implode(',',$all_ids).")");
			if ($sql->db_Rows())
			{
				$copies = execute_multi($sql);
				foreach ($copies as $v) {//REARANGE IDS TO COMPARE WITH PARENT
					$tmp[$v['imageid']][] = $v;
				}//END FOREACH
				for ($i = 0; count($images) > $i; $i++) {
				$images[$i] =$this->ArrangeCopies($tmp[$images[$i]['id']],$images[$i]);
				}//END FOR
			}

          if (!isset($type) ) {//WE HAVE NO TYPE, RETURN ALL IMAGES BY CATEGORY
          //GET IMAGE CATEGORIES TO MATCH
          $sql->db_Select("item_images_categories","id,title","module = '".$this->module['name']."'");
          $categories = execute_multi($sql);
          foreach ($categories as $v) {
            $cats[$v['id']] = $v;
          }
          $cats[0]['title'] = 'thumbs';//Default to thumbs 
          $tmp = array();
            foreach ($images as $v) {//GROUP IMAGES
                if ($v['type'] == 0) {//THUMB, do not use multi array
                $tmp[$cats[$v['type']]['title']] = $v;
                }//END IF
                else//GALLERY. USE MULTI ARRAY
                {
                    $tmp[$cats[$v['type']]['title']][] = $v;
                }
                
            }//END FOREACH
            unset($images);
            $images = $tmp;
            
          }//END IF          
			return $images;
		}//END IF
	}//END FUNCTION
	
	function ArrangeCopies($copies,$arr)
	{
		if ($copies)
		{
		foreach ($copies as $v)
		{
			$arr['image_full'][$v['image_type']] = $v;
			$arr[$v['image_type']] = $v['image_url'];
		}
		}
		return $arr;
	}//END FUNCTION
	
	function GetItemThumb($itemid,$settings=0)
	{
		global $sql;
		$type = ($settings['type']) ? " AND type = ".$settings['type'] : " AND type = 0";
		$sql->db_Select('item_images','id',"module = '".$this->module['name']."' AND itemid = ".$itemid.$type);
//        echo "SELECT id FROM item_images WHERE module = '".$this->module['name']."' AND itemid = ".$itemid.$type."<Br>";
		if ($sql->db_Rows())
		{
			$thumb = execute_single($sql);

			//NOW GET THE COPIES
			$sql->db_Select('item_images_aditional','*',"imageid = ".$thumb['id']);
//			echo "SELECT * FROM item_images_aditional WHERE imageid = ".$thumb['id']."<br>";
			if ($sql->db_Rows())
			{
				$copies = execute_multi($sql);
				//SIMPLIFY THE COPIES
				foreach ($copies as $v)
				{
					$ret[$v['image_type']] = $v['image_url'];
					$full_details[$v['image_type']] = $v;
				}
				$thumb['image_full'] = $full_details;
			}//END ROWS

			if ($settings['GetThumb'])
			{
			return $ret;
			}
			else {
				return $thumb;
			}
		}//END ROWS
	}//END FUNCTION
	
	function ImageCategories($id=0)
	{
		global $sql;
		if ($id) {
			$sql->db_Select("item_images_categories","*","id = $id AND module = '".$this->module['name']."'");
			$category = execute_single($sql);
            $category['settings'] = json_decode($category['settings'],true);
            return $category;
		}
		else {
			$sql->db_Select("item_images_categories","*","module = '".$this->module['name']."' ORDER BY id");
			$categories = execute_multi($sql);
			if ($categories){
            for ($i=0;count($categories) >$i;$i++) {
            $categories[$i]['settings'] = json_decode($categories[$i]['settings'],true);
            }//END FOR
			}
            return $categories;
		}
	}//END FUNCTION
	
    function sdir( $path='.', $mask='*', $nocache=0 ){ 
        static $dir = array(); // cache result in memory 
        if ( !isset($dir[$path]) || $nocache) { 
            $dir[$path] = scandir($path); 
        } 
        foreach ($dir[$path] as $i=>$entry) { 
            if ($entry!='.' && $entry!='..' && fnmatch($mask, $entry) ) { 
                $sdir[] = $entry; 
            } 
        } 
        return ($sdir); 
    }//END FUNCTION
    
function scanDirectories($rootDir, $allowext, $allData=array()) {
    $dirContent = scandir($rootDir);
    foreach($dirContent as $key => $content) {
        $path = $rootDir.'/'.$content;
        $ext = substr($content, strrpos($content, '.') + 1);
        
        if(in_array($ext, $allowext)) {
            if(is_file($path) && is_readable($path)) {
                $allData[] = $path;
            }elseif(is_dir($path) && is_readable($path)) {
                // recursive callback to open new directory
                $allData = scanDirectories($path, $allData);
            }
        }
    }
    return $allData;
}

}//END CLASS
?>