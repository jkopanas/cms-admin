<?php
class ItemMedia
{
	var $itemid;
	var $module;
	var $settings;
	
	function ItemMedia($settings)
	{
		$this->itemid = $settings['itemid'];
		$this->module = $settings['module'];
		$this->settings = $settings;
		
	}//END FUNCTION
    
    function ItemDocuments() {
    global $sql;
    $settings = $this->settings;
    $fields = ($settings['fields']) ? $settings['fields'] : "*";
    $q[] = "module = '".$this->module['name']."'";
    if ($settings['itemid']) {
        $q[] = "itemid = ".$this->itemid;
    }//END IF
    if ($settings['available']) {
        $q[] = "available = ".$this->settings['available'];
    }//END IF
    $query = implode(' AND ',$q);
    $orderby = ($settings['orderby']) ? $settings['orderby'] : 'id';
    $way = ($settings['way']) ? $settings['way'] : 'ASC';
        $sql->db_Select("item_documents",$fields,$query." ORDER BY $orderby $way");
        if ($settings['debug']) {
            echo "SELECT $fields FROM item_documents WHERE $query ORDER BY  $orderby $way";
        }//END IF
        if ($sql->db_Rows()) {
            $res = execute_multi($sql);
            for ($i=0;count($res) > $i;$i++) {
            $res[$i]['settings'] = form_settings_array($res[$i]['settings']);
            $res[$i]['FileSize'] = FileSizeFormat($res[$i]['size']);
            }//END FOR
        }//END IF
        return $res;
    }//END FUNCTION

    function GetDocument($id) {
    global $sql;
        $fields = ($settings['fields']) ? $settings['fields'] : "*";
    	$q[] = "module = '".$this->module['name']."'";
        $q[] = "id = ".$this->settings['id'];
    	if ($settings['available']) {
        	$q[] = "available = ".$this->settings['available'];
	    }//END IF
	    $sql->db_Select("item_documents",$fields,"id = $id AND module = '".$this->settings['module']['name']."'");
	    if ($settings['debug']) {
	    echo "SELECT $fields FROM item_documents WHERE id = $id AND module = '".$this->settings['module']['name']."'<br>";
	    }
	    if ($sql->db_Rows()) {
	    	$res = execute_single($sql);
	    	$res['settings'] = form_settings_array($res['settings']);
            $res['FileSize'] = FileSizeFormat($res['size']);
	    }//END ROWS
    return $res;
    }//END FUNCTIONS
    
	function DeleteItem($id)
	{
	 	global $sql;
	 	switch ($this->settings['media'])
	 	{
	 		case 'document':$item = $this->GetDocument($id); $table = 'item_documents';
	 		break;
	 		case 'video': $item = $this->GetVideo($id); $table = 'item_videos';
	 		break;
	 	}
	 	$sql->db_Delete($table,"id = $id");
	 	@unlink($_SERVER['DOCUMENT_ROOT'].$item['file_url']);
	 	@unlink($_SERVER['DOCUMENT_ROOT'].$item['thumb']);
	}//END FUNCTION
    
	
    function ItemVideos() {
    global $sql;
    $settings = $this->settings;
    $fields = ($settings['fields']) ? $settings['fields'] : "*";
    $q[] = "module = '".$this->module['name']."'";
    if ($settings['itemid']) {
        $q[] = "itemid = ".$this->itemid;
    }//END IF
    if ($settings['available']) {
        $q[] = "available = ".$this->settings['available'];
    }//END IF
    $query = implode(' AND ',$q);
    $orderby = ($settings['orderby']) ? $settings['orderby'] : 'id';
    $way = ($settings['way']) ? $settings['way'] : 'ASC';
        $sql->db_Select("item_videos",$fields,$query." ORDER BY $orderby $way");
        if ($settings['debug']) {
            echo "SELECT $fields FROM item_videos WHERE $query ORDER BY  $orderby $way";
        }//END IF
        if ($sql->db_Rows()) {
            $res = execute_multi($sql);
            for ($i=0;count($res) > $i;$i++) {
            $res[$i]['settings'] = form_settings_array($res[$i]['settings']);
            $res[$i]['FileSize'] = ($res[$i]['size'] > 0) ? FileSizeFormat($res[$i]['size']) : '';
            }//END FOR
        }//END IF
        return $res;
    }//END FUNCTION
	
    function GetVideo($id) {
    global $sql;
        $fields = ($settings['fields']) ? $settings['fields'] : "*";
    	$q[] = "module = '".$this->module['name']."'";
        $q[] = "id = ".$this->settings['id'];
    	if ($settings['available']) {
        	$q[] = "available = ".$this->settings['available'];
	    }//END IF
	    $sql->db_Select("item_videos",$fields,"id = $id AND module = '".$this->settings['module']['name']."'");
	    if ($settings['debug']) {
	    echo "SELECT $fields FROM item_videos WHERE id = $id AND module = '".$this->settings['module']['name']."'<br>";
	    }
	    if ($sql->db_Rows()) {
	    	$res = execute_single($sql);
	    	$res['settings'] = form_settings_array($res['settings']);
            $res['FileSize'] = FileSizeFormat($res['size']);
	    }//END ROWS
    return $res;
    }//END FUNCTIONS
    
     function GetSingleItemid($itemid) {
    global $sql;
        $fields = ($settings['fields']) ? $settings['fields'] : "*";
    	$q[] = "module = '".$this->module['name']."'";
        $q[] = "id = ".$this->settings['id'];
    	if ($settings['available']) {
        	$q[] = "available = ".$this->settings['available'];
	    }//END IF
	    $sql->db_Select("item_videos",$fields,"itemid = $itemid AND module = '".$this->settings['module']['name']."'");
	    if ($settings['debug']) {
	    echo "SELECT $fields FROM item_videos WHERE itemid = $itemid AND module = '".$this->settings['module']['name']."'<br>";
	    }
	    if ($sql->db_Rows()) {
	    	$res = execute_single($sql);
	    	$res['settings'] = form_settings_array($res['settings']);
            $res['FileSize'] = FileSizeFormat($res['size']);
	    }//END ROWS
    return $res;
    }//END FUNCTIONS
    
    
 }//END CLASS
 

?>