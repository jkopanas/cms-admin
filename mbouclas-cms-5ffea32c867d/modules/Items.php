<?php

/**
 * Items
 * 
 * @package MCMS
 * @author Michael Bouclas
 * @copyright 2010
 * @version $Id$
 * @access public
 */
class Items
{
	public $settings;
	public $module;
	public $tables;
	public $itemid;
	
	public function Items($settings)
	{
		$this->itemid = $settings['itemid'];
		$this->module = $settings['module'];
		$this->settings = $settings;
		 
		//SETUP TABLES
		$this->tables = array();
		$this->tables['related'] = 'item_links';
		$this->tables['featured'] = 'item_featured';
		if ($this->module['name'] == 'content') {
			$this->tables['item'] = 'content';
			$this->tables['category'] = 'content_categories';
			$this->tables['pages'] = 'content_page_categories';
		}//END IF
		elseif ($this->module['name'] == 'products')
		{
			$this->tables['item'] = 'products';
			$this->tables['category'] = 'products_categories';
			$this->tables['pages'] = 'products_page_categories';
            $this->tables['related'] = 'item_links';			
		}//END IF
		elseif ($this->module['name'] == 'd_items')
		{
			$this->tables['category'] = 'smart_categories';
			$this->tables['pages'] = 'smart_page_categories';
            $this->tables['related'] = 'item_links';			
		}//END IF
        elseif ($this->module['name'] == 'deals')
		{
			$this->tables['category'] = 'deals_categories_current';
			$this->tables['pages'] = 'deals_page_categories';
            $this->tables['related'] = 'item_links';			
		}//END IF
		elseif ($this->module['name'] == 'courses')
		{
			$this->tables['item'] = 'courses';
			$this->tables['category'] = 'courses_categories';
			$this->tables['pages'] = 'courses_page_categories';
            $this->tables['related'] = 'item_links';			
		}//END IF
		elseif ($this->module['name'] == 'recipes')
		{
			$this->tables['item'] = 'recipes';
			$this->tables['category'] = 'recipes_categories';
			$this->tables['pages'] = 'recipes_page_categories';
            $this->tables['related'] = 'item_links';			
		}//END IF
		elseif ($this->module['name'] == 'listings') {
			$this->tables['item'] = 'listings';
			$this->tables['category'] = 'listings_categories';
			$this->tables['pages'] = 'listings_page_categories';
		}//END IF
		elseif ($this->module['name'] == 'locations') {
			$this->tables['item'] = 'locations';
			$this->tables['category'] = 'locations_categories';
			$this->tables['pages'] = 'locations_page_categories';
		}//END IF
	}//END FUNCTION


function GetItem($itemid,$settings=0)
{
	global $sql,$loaded_modules;
	
	HookParent::getInstance()->doTriggerHook($this->module['name'], "beforeItemFetch");
	
	if (is_array($settings))
	{
	$settings = array_merge($this->settings,$settings);
	}
	if ($settings['active'] AND ($settings['active'] == 1 OR $settings['active'] == 0) )
	{
		$active = " AND active = ".$settings['active'];
		$image_active = $settings['active'];
	}
	$parse = (isset($settings['parse'])) ? $settings['parse'] : 1;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$q = "id = ".$itemid.$active;

	$sql->db_Select($this->tables['item'],$fields,$q);
	if ($settings['debug']) {
		echo "SELECT $fields FROM ".$this->tables['item']." WHERE $q<br>";
	}
	if ($sql->db_Rows()) {
		$item = execute_single($sql,$parse);
		$p = HookParent::getInstance()->doTriggerHook($this->module['name'], "afterItemFetch",$item);
		if ($p) {
			$item = $p;
		}
		$this->itemid = $item['id'];
		if (defined('TRANSLATE')) {
			global $Languages;
			$translations = $Languages->itemTranslations($item['id'],TRANSLATE,$this->module['name'],array('debug'=>0,'groupby'=>'field','table'=>$this->module['name']));
			if ($translations) {
			foreach ($translations as $k=>$v)
			{
				if ($v['translation']) {
					$item[$k] = $v['translation'];
				}
			}
			}
		}//END TRANSLATING
		
        if (!$settings['JustItems'])
        {
		$item['category'] = $this->GetItemCategory($settings);
        if ($settings['main']) 
        { 
            $item['main_category'] = $item['category'];
        }
        else
        {
		if ($item['category'])
		{
		foreach ($item['category'] as $v)
		{
			if ($v['main'] == 1)
			{
				$item['main_category'] = $v;
			}
		}
		}
        }//END MULTIPLE CATEGORIES
        $ItemCat = ($item['main_category']['categoryid']) ? $item['main_category']['categoryid'] : $item['main_category']['catid'];
        }//END CATEGORY
		$item['settings'] = json_decode(base64_decode($item['settings']),true);//UTF8 Compatibility
		if ($settings['get_provider']) {
			$item['user'] = get_provider_id($item['id'],$this->module['name'],array('get_user'=>$settings['get_user']));
		}
		if ($settings['efields']) {
			$ExtraFields = new ExtraFields(array('module'=>$this->module,'debug'=>0));
			$item['efields'] = simplify_efields($ExtraFields->GetItemExtraFields($item['id']));
		}
		if ($settings['comments']) {
			$item['comments'] = get_users_comments(array('id'=>$item['id'],'module'=>$this->module['name'],'get_user'=>1,'debug'=>0));
		}
		if ($settings['votes']) {
			$item['votes'] = get_item_votes(array('id'=>$item['id'],'module'=>$this->module['name']));
			$item['votes']['sum'] = $item['votes']['votes_plus'] - $item['votes']['votes_minus'];
		}
		if ($settings['CatNav']) {
			$item['nav'] = $this->CatNav($ItemCat);
		}
		if ($settings['tags']) {
			$tags = new tags(array('module'=>$this->module,'debug'=>0,'orderby'=>'orderby'));
			$item['tags'] = $tags->GetTagItemsByItemid($item['id']);
            $item['tags_string']  = $tags->ConstructTagsString($item['tags']);
		}
		if ($settings['GetCommonCategories']) {
			if (!is_array($settings['GetCommonCategories'])) {
				$settings['GetCommonCategories'] = $this->ListCommonCategories($this->module['name']);
			}
			$returnType = ($settings['returnSimple']) ? $settings['returnSimple'] : '0';
			foreach ($settings['GetCommonCategories'] as $v)
			{
				$item[str_replace($this->module['name']."_",'',$v['table_name'])] = $this->CommonCategoriesFlatItems($v['table_name'],$this->module['name'],0,array('itemid'=>$item['id'],'returnSimple'=>$returnType));
			}//END LOOP TABLES

		}
		if ($settings['GetCommonCategoriesTree']) {
			$returnType = ($settings['returnSimple']) ? $settings['returnSimple'] : '0';
			$allTreeCategories = $this->ListCommonCategories($this->module['name'],array('type'=>'tree','fields'=>'id,table_name'));
			if ($allTreeCategories) {
			foreach ($allTreeCategories as $v)
			{
				$item[$v['table_name']] = $this->CommonCategoriesTreeItems($v['id'],$this->module['name'],0,array('itemid'=>$item['id'],'returnSimple'=>$returnType,'return'=>$settings['return'],'debug'=>0));
			}//END LOOP TABLES
		}
		}
		if ($loaded_modules['eshop']) {
			$eshop = new eshop();
			$item['eshop'] = $eshop->getProduct($item['id'],$this->module['name']);
		}
		
		if ($settings['GetRecipeDetails']) {
			//NOW GET THE RECIPE DETAILS	
			$item = array_merge($item,$this->GetRecipeDetails($item['id'],$settings));//SENDS TO THE EXTEND CLASS RECIPES
		}
		if ($settings['GetProductDetails']) {
			//NOW GET THE PRODUCT DETAILS
		}
		if ($settings['d_items']) {
			$d_items = new SmartCategories(array('module'=>$this->module,'debug'=>0));	
			$item['smartcategory'] = $d_items->getPageItems($item['id'],$settings);
		}
		//IMAGES STUFF
		if ($settings['thumb'] OR $settings['images'] OR $settings['gallery'])
		{
			$Images = new ItemImages($settings);
		}
		if ($settings['thumb'])
		{
			$item['image_full'] = $Images->GetItemThumb($item['id'],array('GetThumb'=>1));
		}
		if ($settings['images']) {
			$type = (!$settings['type']) ? $loaded_modules[$settings['modules']]['settings']['default_image_type'] : $settings['type'];
			$item['detailed_images'] = $Images->GetItemDetailedImages($item['id'],$type,$settings);
		}

		$p = HookParent::getInstance()->doTriggerHook($this->module['name'], "beforeItemReturn",$item);
		if ($p) {
			$item = $p;
		}
		return $item;
	}//END ROWS
}//END FUNCTION

function GetItemCategory($settings)
{
	global $sql;
	$id = $this->itemid;
	$main = ($settings['main']) ? " AND main = 1 " : ""; 
	$sql->db_Select($this->tables['pages'],"catid,main","itemid = $id $main");
//	echo "SELECT catid FROM ".$this->tables['pages']." WHERE itemid = $id<br>";
	if ($main)
	{
		$cat = execute_single($sql);
		if ($settings['cat_details']) {
			return  $this->ItemCategory($cat['catid'],array('table'=>$this->tables['category'],'settings'=>$settings['form_settings'],'debug'=>0));
		}
		else {
			return $cat;
		}
	}
	else {
		if ($settings['cat_details']) {
		$cat = execute_multi($sql);
		for ($i=0;count($cat) >$i;$i++)
		{
			$a[$i] = $this->ItemCategory($cat[$i]['catid'],array('table'=>$this->tables['category'],'settings'=>$settings['form_settings'],'debug'=>0));
			if ($cat[$i]['main'] == 1) {
				$a[$i]['main'] = 1;
			}
		}
		return $a;
	}
	else {
		return execute_multi($sql);
	}
	}

}//END FUNCTION

function ItemCategory($cat,$settings=0)
{
	  global $sql;
	  $fields = ($settings['fields']) ? $settings['fields'] : "*";
      if ($settings['num_subs']) {
      $fields .= ",@categoryid := categoryid,
(
SELECT count(categoryid) FROM products_categories WHERE 
(products_categories.categoryid_path LIKE CONCAT('%',@categoryid,'/%') OR products_categories.categoryid_path LIKE CONCAT('%/',@categoryid,'%'))
) as num_subs ";
      }//END IF
  $sql->db_Select($this->tables['category'],"$fields",$this->tables['category'].".categoryid = $cat");
  if ($settings['debug'] == 1) {
  	echo "SELECT $fields FROM ".$this->tables['category']." WHERE ".$settings['table'].".categoryid = $cat<Br>";
  }
	if ($sql->db_Rows()) 
	{
    	$a = execute_single($sql,1);
 		$a['settings'] = json_decode($a['settings'],true);
 		if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
			global $Languages;
			$translations = $Languages->itemTranslations($a['categoryid'],FRONT_LANG,$this->module['name'],array('debug'=>0,'groupby'=>'field','table'=>$this->module['name'].'_categories'));
			if ($translations) {
			foreach ($translations as $k=>$v)
			{
				if ($v['translation']) {
					$a[$k] = $v['translation'];
				}
			}
			}
		}//END TRANSLATE CATEGORIES
	}
 return $a;
}//END FUNCTION

function ItemTreeCategories($cat=0,$settings=0)
{
	global $sql;
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$active = $settings['active'];
	$table = $this->tables['category'];
	$table_cat = $this->tables['pages'];
	$settings_q = ($settings['lookup']) ? " AND settings LIKE '%".$settings['lookup']."%'" : "";
	$orderby = ($settings['orderby']) ? $settings['orderby'] : "order_by";
	$way = ($settings['way']) ? $settings['way'] : "ASC";
    if ($settings['num_items'] OR $settings['num_subs'])
    {
        $fields .= ",@categoryid := categoryid,@categoryid_path := categoryid_path";
    }
	if ($settings['num_items']) {
	   $main_q = ($settings['main_category_items']) ? " AND main = 1 " : "";
		$fields .= ",(SELECT count(itemid) FROM ".$this->tables['pages']." INNER JOIN ".$this->tables['category']." ON (".$this->tables['category'].".categoryid=".$this->tables['pages'].".catid) WHERE catid = categoryid $main_q AND (".$this->tables['category'].".categoryid_path LIKE CONCAT('%',@categoryid,'/%') OR ".$this->tables['category'].".categoryid_path = @categoryid 
OR ".$this->tables['category'].".categoryid_path LIKE CONCAT('%/',@categoryid,'%'))) as num_items";
	}
       if ($settings['num_subs']) {
      $fields .= ",
(
SELECT count(categoryid) FROM ".$this->tables['category']." WHERE 
(".$this->tables['category'].".categoryid_path LIKE CONCAT(@categoryid_path,'/%') )
) as num_sub ";
      }//END IF
//	echo "SELECT $fields FROM ".$this->tables['category']." WHERE parentid = $cat $settings_q ORDER BY $orderby $way $limit_q<br>";
	$sql->db_Select($this->tables['category'],$fields,"parentid = $cat $settings_q ORDER BY $orderby $way $limit_q");
	if ($settings['debug']) {
		echo "SELECT $fields FROM ".$this->tables['category']." WHERE parentid = $cat $settings_q ORDER BY $orderby $way $limit_q";
	}
	if ($sql->db_Rows()) {
		$categories = execute_multi($sql,1);
	 for ($i=0;count($categories) > $i;$i++)
	 {
		 $categoryid = $categories[$i]['categoryid'];
		 
		if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
			global $Languages;
			$translations = $Languages->itemTranslations($categoryid,FRONT_LANG,$this->module['name'],array('debug'=>0,'groupby'=>'field'));
			if ($translations) {
			foreach ($translations as $k=>$v)
			{
				if ($v['translation']) {
					$categories[$i][$k] = $v['translation'];
				}
			}
			}
		}//END TRANSLATE CATEGORIES
		 if ($settings['get_subs']) 
		 {
		 	$categories[$i]['subcategories'] = $this->ItemTreeCategories($categoryid,$settings);	 	
		 }
 
		 //get settings 
		 $categories[$i]['settings_array'] = json_decode($categories[$i]['settings'],true);
		// print_ar($categories);

	 }//END OF FOR
	
	 return $categories;
	}//END ROWS
 
}//END FUNCTION


function CommonCategoriesTree ($tableID,$cat=0,$settings=0) {
global $sql;

	$tablePages = 'common_categories_tree_items';
	$table = 'common_categories_tree';
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$active = $settings['active'];
	$table = $table;
	$settings_q = ($settings['lookup']) ? " AND settings LIKE '%".$settings['lookup']."%'" : "";
	$orderby = ($settings['orderby']) ? $settings['orderby'] : "order_by";
	$way = ($settings['way']) ? $settings['way'] : "ASC";
    if ($settings['num_items'] OR $settings['num_subs'])
    {
        $fields .= ",@categoryid := categoryid";
    }
	if ($settings['num_items']) {
		if ($settings['filteredItems']) {
			$filteredItems = " AND itemid IN (".$settings['filteredItems'].")";
		}
	   $main_q = ($settings['main_category_items']) ? " AND main = 1 " : "";
		$fields .= ",(SELECT count(*) FROM ".$tablePages." INNER JOIN ".$table." ON (".$table.".categoryid=".$tablePages.".catid) WHERE catid  = categoryid $main_q $filteredItems AND (".$table.".categoryid_path LIKE CONCAT('%',@categoryid,'/%') OR ".$table.".categoryid_path = @categoryid 
OR ".$table.".categoryid_path LIKE CONCAT('%/',@categoryid,'%'))) as num_items";
	}
       if ($settings['num_subs']) {
      $fields .= ",
(
SELECT count(*) FROM ".$table." WHERE 
(".$table.".categoryid_path LIKE CONCAT('%',@categoryid,'/%') )
) as num_sub ";
      }//END IF
//	echo "SELECT $fields FROM ".$table." WHERE parentid = $cat $settings_q ORDER BY $orderby $way $limit_q<br><br><br>";
	$sql->db_Select($table,$fields,"parentid = $cat  AND tableID = $tableID $settings_q ORDER BY $orderby $way $limit_q");
	if ($settings['debug']) {
		echo "SELECT $fields FROM ".$table." WHERE parentid = $cat AND tableID = $tableID $settings_q ORDER BY $orderby $way $limit_q<br><br>";
	}
	if ($sql->db_Rows()) {
		$categories = execute_multi($sql,1);
		print_ar($categories);
	 for ($i=0;count($categories) > $i;$i++)
	 {
		 $categoryid = $categories[$i]['categoryid'];
		if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
			global $Languages;
			$translations = $Languages->itemTranslations($categoryid,FRONT_LANG,$this->module['name'],array('debug'=>0,'groupby'=>'field'));
			if ($translations) {
			foreach ($translations as $k=>$v)
			{
				if ($v['translation']) {
					$categories[$i][$k] = $v['translation'];
				}
			}
			}
		}//END TRANSLATE CATEGORIES
		 
		 if ($settings['get_subs']) 
		 {
		 	$categories[$i]['subcategories'] = $this->ItemTreeCategories($categoryid,array($settings));
		 }
 
		 //get settings 
		 $categories[$i]['settings_array'] = json_decode($categories[$i]['settings'],true);
		 
	 }//END OF FOR
	 return $categories;
	}//END ROWS

}//END FUNCTION

function commonTreeCatNav($tableID,$cat,$settings=0) 
{ 
	global $sql;
	$sql->db_Select("common_categories_tree","categoryid_path","categoryid = $cat") ;
	if ($settings['debug']) {
		echo "SELECT categoryid_path FROM common_categories_tree WHERE categoryid = $cat<br>";
	}
	if ($sql->db_Rows()) {
		$r =execute_single($sql);
		$tmp = explode("/",$r['categoryid_path']);
		
		if (is_array($tmp) and count($tmp) > 0) 
		{
			$sql->db_Select("common_categories_tree","category,categoryid,alias","tableID=$tableID AND categoryid IN (".implode(",",$tmp).")");
			if ($sql->db_Rows()) {
				$ar = execute_multi($sql);
			
//			echo "SELECT category FROM common_categories_tree WHERE tableID=$tableID AND categoryid IN (".implode(",",$tmp).")";

if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
	$table = $this->ListCommonCategories($this->module['name'],array('type'=>'tree','debug'),$tableID);
	global $Languages;
	 for ($i=0;count($ar) > $i;$i++)
	 {
		 $categoryid = $ar[$i]['categoryid'];
		
			
			$translations = $Languages->itemTranslations($categoryid,FRONT_LANG,$this->module['name'],array('debug'=>0,'groupby'=>'field','table'=>$table['table_name']));
			if ($translations) {
			foreach ($translations as $k=>$v)
			{
				if ($v['translation']) {
					$ar[$i][$k] = $v['translation'];
				}
			}
			}
		}//END TRANSLATE CATEGORIES
	 }
	 }//END ROWS
	return $ar;
	}//END OF IF subproducts_categories found
	}
}//END OF FUNCTION cat_nav

function commonCategoryTree($tableID,$cat,$settings=0)
{
	  global $sql;
      if ($settings['num_subs']) {
      $fields = ",@categoryid := categoryid,
(
SELECT count(categoryid) FROM common_categories_tree WHERE 
(common_categories_tree.categoryid_path LIKE CONCAT('%',@categoryid,'/%') OR common_categories_tree.categoryid_path LIKE CONCAT('%/',@categoryid,'%'))
) as num_subs ";
      }//END IF
  $sql->db_Select("common_categories_tree","* $fields","common_categories_tree.categoryid = $cat");
  if ($settings['debug'] == 1) {
  	echo "SELECT * $fields FROM common_categories_tree WHERE common_categories_tree.categoryid = $cat<Br>";
  }
	if ($sql->db_Rows()) 
	{
    	$a = execute_single($sql,1);
 		$a['settings'] = json_decode($a['settings'],true);
 		if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
			global $Languages;
			$translations = $Languages->itemTranslations($a['categoryid'],FRONT_LANG,$this->module['name'],array('debug'=>0,'groupby'=>'field'));
			if ($translations) {
			foreach ($translations as $k=>$v)
			{
				if ($v['translation']) {
					$a[$k] = $v['translation'];
				}
			}
			}
		}//END TRANSLATE CATEGORIES
	}
 return $a;
}//END FUNCTION

function AllCommonTreeCategories($tableID,$parentid=0)
{
global $sql;
$sql->db_Select("common_categories_tree","categoryid,category,categoryid_path,parentid","tableID = $tableID");
//echo "SELECT categoryid,category,categoryid_path FROM common_categories_tree WHERE tableID = $tableID";
$all_cats = execute_multi($sql);
//print_r($all_cats);
//MAP AN ARRAY WITH THE NAMES OF CATEGORIES
if(is_array($all_cats) AND !empty($all_cats))
{
foreach ($all_cats as $k=>$v) {
	$names[$v[categoryid]]=$v[category];
	
}
}//END IF

//LOOP CATEGORIES TO BUILD PATH
$g = array();
for ($i=0;count($all_cats) > $i;$i++)
{
	$tmp = explode("/",$all_cats[$i]['categoryid_path']);
	
	for ($j=0;count($tmp)>$j;$j++)
	{
		$z[] = $tmp[$j];
	}

	$tmp = $this->ApplyCategoryNames($tmp,$all_cats[$i]['categoryid'],$names);
//	print_r($tmp);
	$parentid = $all_cats[$i]['parentid'];
	$levelNo = substr_count($tmp[key($tmp)],"/");
	//$levels[$levelNo][$parentid][key($tmp)]=$tmp[key($tmp)];
	//natcasesort($tmp);
	$levelInfo[key($tmp)]['parentid']=$parentid;
	$levelInfo[key($tmp)]['levelNo']=$levelNo;
	$a[key($tmp)] = $tmp[key($tmp)];
	
}//END FOR
 //echo count($a);
natcasesort($a);

foreach($a as $aKey => $aValue)
{
	$parentid = $levelInfo[$aKey]['parentid'];
	$levelNo = $levelInfo[$aKey]['levelNo'];
	$levels[$levelNo][$parentid][$aKey]=$a[$aKey];
}
foreach($levels[$levelNo][$parentid] as $tKey => $tValue)
{
	$temp = $levels[$levelNo][$parentid];
	natcasesort($temp);
	$levels[$levelNo][$parentid]=$temp;
}
	$levelValue = $levels[0]; 
	$nextlevelKey = $levelKey+1;
	//print_r($levelValue);
	foreach($levelValue as $parentKey => $parentValue)
	{
		foreach($parentValue as $catKey => $value)
		{

		
			if($levels[$nextlevelKey][$catKey])
			{
				
				$this->levelScanner($levels,$levels[$nextlevelKey][$catKey],$nextlevelKey,$catKey);
							 
			}
		}
	}

natcasesort($a);
//DO THIS FOR BACKWARDS COMPATIBILITY
$tmp = array();
foreach ($a as $k=>$v)
{
	$tmp[$k]['categoryid'] = $k;
	$tmp[$k]['category'] = $v;
}
return $tmp;

}//END FUNCTION

function CommonCategoriesFlat ($table,$settings=0)
{
	global $sql;
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$active = $settings['active'];

	if ($settings['num_items']) {
	   $fields .= ",@categoryid := categoryid";
		$fields .= ",(SELECT count(itemid) FROM common_categories_flat_items INNER JOIN common_categories_flat ON (common_categories_flat.categoryid=common_categories_flat_items.catid) 
		INNER JOIN ".$this->tables['item']." ON (common_categories_flat_items.itemid=".$this->tables['item'].".id)
		WHERE catid = @categoryid AND ".$this->tables['item'].".active = $active ) as num_items";
	}	 
	$sql->db_Select("common_categories_flat",$fields,"table_name = '$table' AND module = '".$this->module['name']."' ORDER BY orderby $limit_q");
	if ($settings['debug']) {
		echo "SELECT $fields FROM common_categories_flat WHERE table_name = '$table' AND module = '".$this->module['name']."' ORDER BY orderby $limit_q";
	}
	if ($sql->db_Rows()) {
		$categories = execute_multi($sql,1);
	
	 for ($i=0;count($categories) > $i;$i++)
	 {
		 $categoryid = $categories[$i]['categoryid'];
		
		 
		 //get settings 
		 if ($settings['settings']) {
		 	 $categories[$i]['settings_array'] = json_decode($categories[$i]['settings'],true);
		 }
		
		 
	 }//END OF FOR
	 return $categories;
	}//END ROWS
	
	
}//END FUNCTION

function CommonCategoriesFlatItems ($table,$module,$catid=0,$settings=0)
{
	global $sql;
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$active = $settings['active'];
	if ($settings['active']) {
		$q[] = "active = ".$settings['active'];
		$table_q = " INNER JOIN $module ON ($module.id=common_categories_flat_items.itemid)";
	}
	if ($catid) {
		$q[] = "catid = $catid";
	}
	if ($settings['itemid']){
		$q[] = "itemid = ".$settings['itemid'];
	}
	$q[] = "module = '$module'";
	$q[] = "table_name = '$table'";
	$query = implode(" AND ",$q);
		$sql->db_Select("common_categories_flat_items INNER JOIN common_categories_flat ON (common_categories_flat.categoryid=common_categories_flat_items.catid)",$fields,"$query $limit_q");
		if ($settings['debug']) {
			echo "SELECT $fields FROM common_categories_flat_items INNER JOIN common_categories_flat ON (common_categories_flat.categoryid=common_categories_flat_items.catid) WHERE $query $limit_q<Br>";
		}
		if ($sql->db_Rows()) {
			$ids = execute_multi($sql);
			if ($settings['returnSimple']) {
				foreach ($ids as $v) {
					$res[] = $v['catid'];
				}//END FOREACH
			}//END IF
			else {
				$res = $ids;
			}//END RETURN
		}//END ROWS
	
	return $res;
	
}//END FUNCTION

function CommonCategoriesTreeItems ($tableID,$module,$catid=0,$settings=0)
{
	global $sql;
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$limit_q = ($settings['results_per_page']) ? " LIMIT ".$settings['results_per_page'] : "";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : " ORDER BY common_categories_tree_items.orderby";
	$way = ($settings['way']) ? "  ".$settings['way'] : "";
	
	$active = $settings['active'];
	if ($settings['active']) {
		$q[] = "active = ".$settings['active'];
		$table_q = " INNER JOIN $module ON ($module.id=common_categories_tree_items.itemid)";
	}

	if ($settings['itemid']){
		$q[] = "itemid = ".$settings['itemid'];
	}
	if ($catid) {
		
	
	if ($settings['searchInSubcategories']) {
		# Search also in all subcategories
		$sql->db_Select("common_categories_tree","categoryid_path","categoryid = '$catid' AND tableID = $tableID AND module = '$module'");
	//	echo "SELECT categoryid_path FROM ".
		$r = execute_single($sql);
		$categoryid_path = $r['categoryid_path'];
		$sql->db_Select("common_categories_tree","categoryid","tableID = $tableID AND module = '$module' AND (categoryid = $catid OR categoryid_path LIKE '$categoryid_path/%')");
//		echo "<Br><Br>SELECT categoryid FROM common_categories_tree WHERE tableID = $tableID AND module = '$module' AND (categoryid = $catid OR categoryid_path LIKE '$categoryid_path/%')<br><br>";
		$q[] = " categoryid IN (SELECT categoryid FROM common_categories_tree WHERE tableID = $tableID AND module = '$module' AND (categoryid = $catid OR categoryid_path LIKE '$categoryid_path/%'))";		

	}
	else {
		$q[] = "catid = $catid";
	}
	}//END CATID 
	$q[] = "module = '$module'";
	$q[] = "common_categories_tree_items.tableID = '$tableID'";
	$query = implode(" AND ",$q);
	
	if ($settings['return'] == 'paginated') {
		$sql->db_Select("common_categories_tree_items INNER JOIN common_categories_tree ON (common_categories_tree.categoryid=common_categories_tree_items.catid) $table_q","count(itemid) as total","$query  $orderby");
		
/*		echo "SELECT count(itemid) as total FROM common_categories_tree_items INNER JOIN common_categories_tree ON (common_categories_tree.categoryid=common_categories_tree_items.catid) $table_q
		WHERE $query  ORDER BY $orderby";	*/
		if ($sql->db_Rows()) {
			$a = execute_single($sql);
			$total = $a['total'];
			$current_page = ($settings['page']) ? $settings['page'] : 1;
			$results_per_page =  $settings['results_per_page'];
			if (isset($settings['start'])) 
			{
				$start = $settings['start'];
			}
			else {
				$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
			}
			$limit = "LIMIT $start,".$results_per_page;
			$sql->db_Select("common_categories_tree_items INNER JOIN common_categories_tree ON (common_categories_tree.categoryid=common_categories_tree_items.catid) $table_q","$fields","$query $orderby $way $limit");
			
			if ($settings['debug']) {
				echo "SELECT $fields FROM common_categories_tree_items INNER JOIN common_categories_tree ON (common_categories_tree.categoryid=common_categories_tree_items.catid)  $table_q WHERE $query $orderby $way $limit <Br>";
			}
			$tmp = execute_multi($sql,1);
			paginate_results($current_page,$results_per_page,$total);
			foreach ($tmp as $v) {
				$res[] = $this->GetItem($v['itemid'],$settings['itemSettings']);
			}
			if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
	$table = $this->ListCommonCategories($this->module['name'],array('type'=>'tree','debug'),$tableID);
	global $Languages;
	 for ($i=0;count($res) > $i;$i++)
	 {
		 $categoryid = $res[$i]['categoryid'];
			
			$translations = $Languages->itemTranslations($categoryid,FRONT_LANG,$this->module['name'],array('debug'=>0,'groupby'=>'field','table'=>$table['table_name']));
			if ($translations) {
			foreach ($translations as $k=>$v)
			{
				if ($v['translation']) {
					$res[$i][$k] = $v['translation'];
				}
			}
			}
		}//END TRANSLATE CATEGORIES
	 }
		}//END ROWS
	}//END PAGINATED
	else {
	$sql->db_Select("common_categories_tree_items INNER JOIN common_categories_tree ON (common_categories_tree.categoryid=common_categories_tree_items.catid) $table_q",$fields,"$query $orderby $way $limit_q");

		if ($settings['debug']) {
			echo "SELECT $fields FROM common_categories_tree_items INNER JOIN common_categories_tree ON (common_categories_tree.categoryid=common_categories_tree_items.catid)  $table_q WHERE $query $orderby $way $limit_q<Br>";
		}
		if ($sql->db_Rows()) {
			if ($settings['return'] == 'multi') {
				
				$ids = execute_multi($sql);
				$res = $ids;
				if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
				$table = $this->ListCommonCategories($this->module['name'],array('type'=>'tree','debug'),$tableID);
					global $Languages;
					 for ($i=0;count($res) > $i;$i++)
					 {
						 $categoryid = $res[$i]['categoryid'];
							
							$translations = $Languages->itemTranslations($categoryid,FRONT_LANG,$this->module['name'],array('debug'=>0,'groupby'=>'field','table'=>$table['table_name']));
							if ($translations) {
							foreach ($translations as $k=>$v)
							{
								if ($v['translation']) {
									$res[$i][$k] = $v['translation'];
								}
							}
							}
						}//END TRANSLATE CATEGORIES
					 }
			}
			else {
			if ($settings['returnSimple'] == 1) {
				$res = execute_single($sql);
				$res['settings'] = json_decode($res['settings']);
			}//END IF
			else {
				$ids = execute_multi($sql);
				
				if ($settings['returnSimple'] == 'byKey')
				{
					foreach ($ids as $k=>$v)
					{
						$tmp[$v['categoryid']] = $v;
					}
					$ids = $tmp;
				}
				else {
					foreach ($ids as $k=>$v)
					{
						$ids[$k]['settings'] = json_decode($ids[$k]['settings'],true);
					}
				}
				$res = $ids;
			}//END RETURN
			
				if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
				$table = $this->ListCommonCategories($this->module['name'],array('type'=>'tree','debug'),$tableID);
				global $Languages;
						
						$translations = $Languages->itemTranslations($res['categoryid'],FRONT_LANG,$this->module['name'],array('debug'=>0,'groupby'=>'field','table'=>$table['table_name']));
						if ($translations) {
						foreach ($translations as $k=>$v)
						{
							if ($v['translation']) {
								$res[$k] = $v['translation'];
							}
						}
						}
				 }//END TRANSLATE CATEGORIES
			
			}//END SINGLE
		}//END ROWS
	}//END NON PAGINATED

	return $res;
	
}//END FUNCTION


function ListCommonCategories($module,$settings=0,$id=0)
{
	global $sql;
	$type = ($settings['type']) ? $settings['type'] : 'flat';
	$fields = ($settings['fields']) ? $settings['fields'] : '*';
	if ($id) {
		$q = "AND id = $id";
	}
	$sql->db_Select("common_categories_tables",$fields,"module = '$module' AND type = '$type' $q");
	if ($sql->db_Rows()) {
		if ($id) {
			return execute_single($sql);
		}
		else {
			return execute_multi($sql);
		}
		
	}
	
}//END FUNCTION

function AllItemsTreeCategories($parentid=0)
{
global $sql;
$sql->db_Select($this->tables['category'],"categoryid,category,categoryid_path,parentid");
//echo "SELECT categoryid,category,categoryid_path FROM ".$this->tables['category'];
$all_cats = execute_multi($sql);
//print_r($all_cats);
//MAP AN ARRAY WITH THE NAMES OF CATEGORIES
if(is_array($all_cats) AND !empty($all_cats))
{
foreach ($all_cats as $k=>$v) {
	$names[$v[categoryid]]=$v[category];
	
}
}//END IF

//LOOP CATEGORIES TO BUILD PATH
$g = array();
for ($i=0;count($all_cats) > $i;$i++)
{
	$tmp = explode("/",$all_cats[$i]['categoryid_path']);
	
	for ($j=0;count($tmp)>$j;$j++)
	{
		$z[] = $tmp[$j];
	}

	$tmp = $this->ApplyCategoryNames($tmp,$all_cats[$i]['categoryid'],$names);
//	print_r($tmp);
	$parentid = $all_cats[$i]['parentid'];
	$levelNo = substr_count($tmp[key($tmp)],"/");
	//$levels[$levelNo][$parentid][key($tmp)]=$tmp[key($tmp)];
	//natcasesort($tmp);
	$levelInfo[key($tmp)]['parentid']=$parentid;
	$levelInfo[key($tmp)]['levelNo']=$levelNo;
	$a[key($tmp)] = $tmp[key($tmp)];
	
}//END FOR
 //echo count($a);
natcasesort($a);

foreach($a as $aKey => $aValue)
{
	$parentid = $levelInfo[$aKey]['parentid'];
	$levelNo = $levelInfo[$aKey]['levelNo'];
	$levels[$levelNo][$parentid][$aKey]=$a[$aKey];
}
foreach($levels[$levelNo][$parentid] as $tKey => $tValue)
{
	$temp = $levels[$levelNo][$parentid];
	natcasesort($temp);
	$levels[$levelNo][$parentid]=$temp;
}
	$levelValue = $levels[0]; 
	$nextlevelKey = $levelKey+1;
	//print_r($levelValue);
	foreach($levelValue as $parentKey => $parentValue)
	{
		foreach($parentValue as $catKey => $value)
		{

		
			if($levels[$nextlevelKey][$catKey])
			{
				
				$this->levelScanner($levels,$levels[$nextlevelKey][$catKey],$nextlevelKey,$catKey);
							 
			}
		}
	}

natcasesort($a);
//DO THIS FOR BACKWARDS COMPATIBILITY
$tmp = array();
foreach ($a as $k=>$v)
{
	$tmp[$k]['categoryid'] = $k;
	$tmp[$k]['category'] = $v;
}
return $tmp;

}//END FUNCTION

function levelScanner($levels,$arr,$levelKey,$catKey)
{
	$nextlevelKey = $levelKey+1;
	foreach($arr as $arrkey => $arrvalue)
	{
//		echo $arrvalue."<br>";
		//print_r($tempValue);
		
		if($levels[$nextlevelKey][$arrkey])
		{
			$temp[]= $arrkey;
				
		}
		//echo $arrkey;
		
	}
	if($temp)
	{
		for($i=0;$i<count($temp);$i++)
		{
			$tempKey = $temp[$i];
			$this->levelScanner($levels,$levels[$nextlevelKey][$tempKey],$nextlevelKey,$tempKey);
		}
	}
	
	//unset($levels[$levelKey]);
	//unset($levels[$levelKey][$catKey]);
}//END FUNCTION

function ApplyCategoryNames($catid,$id,$names)
{
	if (is_array($catid)) {
		foreach ($catid as $v) {
			$tmp[] = $names[$v];
		}
		return array($id => implode(" / ",$tmp));
	}
	else {
		return array($id =>$names[$catid]);
	}
	
}//END FUNCTION

function ItemFlatCategories($settings)
{
	global $sql;
	
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$active = $settings['active'];
	$table = $settings['table_page'];
	$table_cat = $settings['table'];
		 
	$sql->db_Select($settings['table'],$fields,"ORDER BY orderby $limit_q",$mode="no_where");
	if ($settings['debug']) {
		echo "SELECT $fields FROM $table_cat ORDER BY orderby $limit_q";
	}
	if ($sql->db_Rows()) {
		$categories = execute_multi($sql,1);
	
	 for ($i=0;count($categories) > $i;$i++)
	 {
		 $categoryid = $categories[$i]['categoryid'];
		
	 
		 if ($settings['num_items']) {

		 $sql->db_Select("$table
		 INNER JOIN content ON ($table.itemid=content.id)
		 INNER JOIN $table_cat ON ($table.catid=$table_cat.categoryid)",
		 "$table.itemid","content.active IN ($active) ");
			if ($settings['debug']) {
				echo "<br>SELECT $table.itemid FROM $table
		 INNER JOIN content ON ($table.itemid=content.id)
		 AND content.active IN ($active)";				
			}
		 $categories[$i]['num_items'] = $sql->db_Rows();
		}//END NUM ITEMS
		 
		 //get settings 
		 if ($settings['settings']) {
		 	 $categories[$i]['settings_array'] = json_decode($categories[$i]['settings'],true);
		 }
		
		 
	 }//END OF FOR
	 return $categories;
	}//END ROWS
}//END FUNCTION

function FlatCategoryItems($settings)
{
	global $sql;
	//Check for the item first
	$sql->db_Select($settings['page_table'],'catid','itemid = '.$settings['itemid']);
	if ($sql->db_Rows()) {
		if ($settings['multi']) {
			$cat = execute_multi($sql);
			for ($i=0;count($cat)>$i;$i++)
			{
				$a[] = $this->ItemFlatCategories($cat[$i]['catid'],array('table'=>$settings['table'],'debug'=>0));
			}
			return $a;
		}
		else {
			$cat = execute_single($sql);
			return $this->ItemFlatCategories($cat['catid'],array('table'=>$settings['table'],'debug'=>0));
		}

	}
}//END FUNCTION

function AllItemCategories($settings=0)
{
	global $sql;
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$table = ($settings['table']) ? $settings['table'] : $this->tables['category'];
	$sql->db_Select($table,$fields, "ORDER BY categoryid_path $limit_q",$mode="no_where");
	if ($sql->db_Rows() > 0) 
	{  
		$categories = execute_multi($sql);
		for ($i=0;count($categories) > $i;$i++)
		{
			$categories[$i]['category'] = $this->construct_content_category($categories[$i]['categoryid_path'],$table);
		}//END OF FOR
		sort($categories);
	return $categories;	
	}//END OF IF
}//END OF FUNCTION



function construct_content_category($path,$table) 
{ 
	global $sql;
	
	if (strstr($path,"/")) 
	{  
	$tmp = explode("/",$path);
	$ids = implode(",",$tmp);
	}//END OF IF
	else 
	{  
	$ids = $path; 
	}//END OF ELSE
$sql->db_Select($table,"category","categoryid IN ($ids)");
if ($sql->db_Rows()) {
	$a = execute_multi($sql);
	for ($i=0;count($a) > $i;$i++)
	{
		$ar[] = $a[$i]['category'];
	}
	 if (count($ar) > 0) 
	 {  
	 	$return = implode("/",$ar); 
	 }//END OF IF
	 else 
	 {  
	 	$return = $r['category']; 
	 }//END OF ELSE

	 return $return;
}//END ROWS
}//END FUNCTION

function ContstructCategory($path,$table) 
{ 
	global $sql;
	if (strstr($path,"/")) 
	{  
	$tmp = explode("/",$path);
	$ids = implode(",",$tmp);
	}//END OF IF
	else 
	{  
	$ids = $path; 
	}//END OF ELSE
$sql->db_Select($table,"category","categoryid IN ($ids)");
if ($sql->db_Rows()) {
	$a = execute_multi($sql);
	for ($i=0;count($a) > $i;$i++)
	{
		$ar[] = $a[$i]['category'];
	}
	 if (count($ar) > 0) 
	 {  
	 	$return = implode("/",$ar); 
	 }//END OF IF
	 else 
	 {  
	 	$return = $r['category']; 
	 }//END OF ELSE

	 return $return;
}//END ROWS
}//END FUNCTION

function CatNav($cat,$settings=0) 
{ 
	global $sql;
	$sql->db_Select($this->tables['category'],"categoryid_path","categoryid = $cat") ;
	if ($settings['debug']) {
		echo "SELECT categoryid_path FROM ".$this->tables['category']." WHERE categoryid = $cat<br>";
	}
	if ($sql->db_Rows()) {
		$r =execute_single($sql);
		$tmp = explode("/",$r['categoryid_path']);
		
		if (is_array($tmp) and count($tmp) > 0) 
		{
	 	for ($i=0;count($tmp) > $i;$i++)
		{
//			echo "SELECT $fields FROM $tables WHERE $table.categoryid = ".$tmp[$i]." AND code = '$lang'<br>";
			$ar[] = $this->ItemCategory($tmp[$i]);
		}//END OF FOR
	return $ar;
	}//END OF IF subproducts_categories found
	}
}//END OF FUNCTION cat_nav

function SaveItem($data,$id=0,$settings=0)
{
global $sql;
$t = new Parse();

if ($settings['complexFields']) {
	foreach ($data as $k=>$v) {
		if ($v['module']) {//OTHER MODULES
			$rest[$v['module']][$k] = $v;
		}
		elseif ($v['table']) {//SECONDARY STUFF LIKE CATEGORIES
			$aditional[$v['table']][$k] = $v;
		}
		elseif (strstr($k,"settings_")) 
		{
			list($field,$code)=split("settings_",$k);
			$s[$code] = $v['value'];
		}//END OF IF
		else{//ACTUAL ITEM
			$tmp[$k] = $v['value'];
		}
	}
	$data = $tmp;
}//END COMPLEX

foreach ($data as $k=>$v){
	$v = $t->toDB($v);
	$updateQ[] = "$k = '$v'";
	

	
}//END DATA

if ($s) {
	$data['settings'] = base64_encode(json_encode($s));
	$updateQ[] = "settings = '".$data['settings']."', date_modified = '".time()."'";
}

if ($id) {//UPDATE
	$restSettings['checkFirst'] = 1;
	$query = implode(",",$updateQ)." WHERE id = $id";
	
	$sql->db_Update($this->tables['item'],$query);
//	$settings['debug'] = 1;
	if ($settings['debug']) {
		echo "UPDATE ".$this->tables['item']." SET $query<br>";
		echo $sql->mySQLerror;
	}
}//END UPDATE
else {
	foreach ($data as $k=>$v) {
		$insertQ[] = "'".$t->toDB($v)."'";
		$fields[] = $k;
	}
		$insertQ[] = "'".time()."'";
		$fields[] = 'date_added';
	if ($settings['debug']) {
		echo "INSERT INTO ".$this->tables['item']." (".implode(',',$fields).") VALUES (".implode(",",$insertQ).")<Br>";
	}
	$sql->db_Insert($this->tables['item']." (".implode(',',$fields).")",implode(",",$insertQ));
	$id = $sql->last_insert_id;
}//END ADD


	if ($aditional['categories']['categoryid']) {//MAIN CATEGORY
		$sql->db_Delete($this->tables['pages'],"itemid = $id AND main = 1");
		$sql->db_Insert($this->tables['pages'],$aditional['categories']['categoryid']['value'].",$id,1");
	}	
	if ($aditional['categories']['categoryIDs']) {//ADITIONA CATEGORIES
		$sql->db_Delete($this->tables['pages'],"itemid = $id AND main = 0");
		foreach ($aditional['categories']['categoryIDs']['value'] as $v) {
			$sql->db_Insert($this->tables['pages'],$v.",$id,0");
		}
	}
	
	if ($rest) {
		foreach ($rest as $k=>$v) {
			call_user_func(array( $this, "saveItem_$k"),$id,$v,$restSettings);
		}
	}

	if ($aditional['commonCategories']) {
		foreach ($aditional['commonCategories'] as $key=>$v) {
			if (strstr($key,'commonCategoryTree-')) {
				list($useless, $tableID) = split("commonCategoryTree-",$key);
				
				if (is_array($val)) {
					foreach ($val as $l)
					{
						$commonCategoriesTree[$tableID][] = $l;
					}
				}
				else {
					$commonCategoriesTree[$tableID] = $val;
				}
				
			
				
			}//END COMMON TREE CATEGORIES
			if (strstr($key,'commonCategoryFlat-')) {
				list($useless, $tableID) = split("commonCategoryFlat-",$key);
				if (is_array($val)) {
					foreach ($val as $l)
					{
						$commonCategoriesFlat[$tableID][] = $l;
					}
				}
				else {
					$commonCategoriesFlat[$tableID] = $val;
				}
				
			}//END COMMON TREE CATEGORIES	
		}
	
		if ($commonCategoriesTree AND $tableID) {
			foreach ($commonCategoriesTree as $k=>$v) {
				$sql->db_Delete("common_categories_tree_items","itemid = $id AND tableID = $k");
				$inserts = array();
				if (is_array($v)) {
					$i=0;
					foreach ($v as $l)
					{
						$inserts[] = "('$l','$id','$i','$k')";
						$i++;
					}
					$sql->q('INSERT INTO common_categories_tree_items VALUES '.implode(',',$inserts));
		//			echo 'INSERT INTO common_categories_tree_items VALUES '.implode(',',$inserts)."<br>";
					
				}
				else {
					$sql->db_Insert("common_categories_tree_items","'$v','$id','','$k'");
				}
				
			}
		}
		if ($commonCategoriesFlat) {
			foreach ($commonCategoriesFlat as $k=>$v) {
				$sql->db_Delete("common_categories_tree_items","itemid = $id AND tableID = $tableID");
			$sql->db_Insert("common_categories_tree_items","'$v','$id','$tableID'");
			}
		}
	}//END COMMON CATEGORIES	
	return $id;
}//END FUNCTION

public function saveItem_eshop($itemid,$data,$settings=0) {
//	print_r($itemid);
	$eshop = new eshop();
	$eData['itemid'] = $itemid;
	foreach ($data as $k=>$v) {
		$eData[$k] = $v['value'];
	}
	$eData['module'] = $this->module['name'];
	$eshop->itemActions($eData,array('action'=>'modifyItem','debug'=>0,'checkFirst'=>$settings['checkFirst']));
}//END FUNCTION

public function saveItem_tags($itemid,$data,$settings=0) {
	$tags = new tags(array('module'=>$this->module,'debug'=>0,'itemid'=>$itemid));
	$tags->SaveTags($data['tags']['value']);
}//END FUNCTION

function AddItem($data)
{
	global $sql,$loaded_modules;
	$required_fields = array('title');
	//STEP 1: grab POST variables into a formated array
 $posted_data = array();
 foreach ($data as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
 	$t = new textparse();
//Database insertions
//Add the basic content details
$categoryid = $data['categoryid'];
$orderby = $data['orderby'];
$allow_comments = $data['allow_comments'];
$active = ($data['AutoActivate']) ? 0 : $data['active'];
$orderby = $data['orderby'];
$description = $t->formtpa($data['description']);
$description_long = $t->formtpa($data['description_long']);
$permalink = $t->formtpa($data['permalink']);
$main_cat = $data['main-cat'];
$langCode = $data['code'];
$date_added = time();
$uid = ID;
$title = $t->formtpa($data['title']);
//SETTINGS
foreach ($data as $key => $val)
{
if (strstr($key,"settings_")) 
{
list($field,$code)=split("settings_",$key);
$s[$code] = $val;
}//END OF IF
}//END OF FOREACH
if (is_array($s)) {
	$settings = json_encode($s);
}
if ($this->module['name'] == 'products')
{
	$sku = $data['sku'];
	$sku_insert = "'$sku',";
}
//print_r($data);//END OF print_r
$sql->db_Insert($this->tables['item'],"'','$title','$description','$description_long','$uid','$date_added',$date_added,'$active',0,'$orderby','$settings','$permalink','$langCode'");
//echo "INSERT INTO ".$this->tables['item']." VALUES ('','$title','$description','$description_long',$uid,'$date_added',$date_added,'$active',$allow_comments,0,'$orderby','$settings','$permalink')<br>";
$id = $sql->last_insert_id;

if ($loaded_modules['eshop']) {
	$eshop = new eshop();
	$eData['itemid'] = $id;
	$eData['price'] = $data['price'];
	$eData['list_price'] = $data['list_price'];
	$eData['module'] = $this->module['name'];
	$eshop->itemActions($eData,array('action'=>'modifyItem','debug'=>0));
}

//CRON JOBS
if ($data['date_added'] > time()) {
	$sql->db_Update('content',"date_added = ".$data['date_added']." WHERE id = $id");
	//First chck for existing cron jobs
	$sql->db_Select("cron_tasks","id","itemid = $id AND module = '".$this->module['name']."' AND action = 'activate'");
	if ($sql->db_Rows()) {
		$sql->db_Update("cron_tasks","date = '".$data['date_added']."' WHERE itemid = $id AND module = '".$this->module['name']."' AND action = 'activate'");
	}
	else {//new entry
		$sql->db_Insert('cron_tasks',"'','$id','".$this->module['name']."','activate','".$data['date_added']."'");
		
	}
}
else {//Auto publish off
	//First chck for existing cron jobs
	$sql->db_Select("cron_tasks","id","itemid = $id AND module = '".$this->module['name']."' AND action = 'activate'");
	if ($sql->db_Rows()) {
		$sql->db_Delete("cron_tasks","id","itemid = $id AND module = '".$this->module['name']."' AND action = 'activate'");
	}
}

//SECONDARY UPDATES

if (is_array($data['cat'])) 
{
foreach ($data['cat'] as $v)
{
	$main = ($v == $main_cat) ? 1 : 0;
	$sql->db_Insert($this->tables['pages'],"$v,$id,$main");	
}
}


//Providers
if ($data['providerid'] AND $data['providerid'] != '%') 
{
	$sql->db_Select("provider_items","id","itemid = $id AND type = '".$this->module['name']."'");
	if ($sql->db_Rows()) 
	{
		$sql->db_Update("provider_items","uid = ".$data['providerid']." WHERE itemid = $id AND type = '".$this->module['name']."' ");
	}
	else 
	{
		$sql->db_Insert("provider_items","'','".$data['providerid']."','$id','".$this->module['name']."','".time()."'");
//		echo "'','".$data['provider']."','$id','".time()."'";
	}
}

//add extra fields
$this->AddExtraFieldValues($data,$data['id']);

if ($this->module['settings']['user_content_tags'])
{
//ADD TAGS
$tags = new tags(array('module'=>$loaded_modules['content'],'debug'=>0,'itemid'=>$id));
$tags->SaveTags($data['tags']);
}

return $id;
 }//END OF NO ERRORS
 else {
 	return $error_list;
 }

}//END FUNCTION

function ModifyItem($data)
{
	
global $sql,$loaded_modules;
$t = new textparse();

	$id = $data['id'];
	$required_fields = array('title');
//print_r($data);
//exit();
	//STEP 1: grab POST variables into a formated array
 $posted_data = array();

 foreach ($data as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR
 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
//Database insertions
//Add the basic content details
$orderby = $data['orderby'];
$allow_comments = $data['allow_comments'];
$active = ($data['AutoActivate']) ? 0 : $data['active'];
$orderby = $data['orderby'];
$description = $t->formtpa($data['description']);
$description_long = $t->formtpa($data['description_long']);
$main_cat = $data['main-cat'];
$uid = ID;
$title = $t->formtpa($data['title']);
$permalink = $t->formtpa($data['permalink']);

$date_modified = time();
if ($data['date_added'] > time()) {
	$date_added_q = ",date_added='".$data['date_added']."'";
	//First chck for existing cron jobs
	$sql->db_Select("cron_tasks","id","itemid = $id AND module = '".$this->module['name']."' AND action = 'activate'");
	if ($sql->db_Rows()) {
		$sql->db_Update("cron_tasks","date = '".$data['date_added']."' WHERE itemid = $id AND module = '".$this->module['name']."' AND action = 'activate'");
	}
	else {//new entry
		$sql->db_Insert('cron_tasks',"'','$id','".$this->module['name']."','activate','".$data['date_added']."'");
		
	}
}
else {//Auto publish off
	//First chck for existing cron jobs
	$sql->db_Select("cron_tasks","id","itemid = $id AND module = '".$this->module['name']."' AND action = 'activate'");
	if ($sql->db_Rows()) {
		$sql->db_Delete("cron_tasks","id","itemid = $id AND module = '".$this->module['name']."' AND action = 'activate'");
	}
}
//SETTINGS
foreach ($data as $key => $val)
{
if (strstr($key,"settings_")) 
{
list($field,$code)=split("settings_",$key);
$s[$code] = $val;
}//END OF IF
if (strstr($key,'commonCategoryTree-')) {
	list($useless, $tableID) = split("commonCategoryTree-",$key);
	
	if (is_array($val)) {
		foreach ($val as $l)
		{
			$commonCategoriesTree[$tableID][] = $l;
		}
	}
	else {
		$commonCategoriesTree[$tableID] = $val;
	}
	

	
}//END COMMON TREE CATEGORIES
if (strstr($key,'commonCategoryFlat-')) {
	list($useless, $tableID) = split("commonCategoryFlat-",$key);
	if (is_array($val)) {
		foreach ($val as $l)
		{
			$commonCategoriesFlat[$tableID][] = $l;
		}
	}
	else {
		$commonCategoriesFlat[$tableID] = $val;
	}
	
}//END COMMON TREE CATEGORIES
}//END OF FOREACH
$old = $this->GetItem($id,array('fields'=>'settings,id'));

if (is_array($s)) {
	$settings = array_merge($old['settings'],$s);
	$settings = json_encode($settings);
}
if ($loaded_modules['eshop']) {
	$eshop = new eshop();
	$eData['itemid'] = $id;
	$eData['price'] = $data['price'];
	$eData['list_price'] = $data['list_price'];
	$eData['module'] = $this->module['name'];
	$eshop->itemActions($eData,array('action'=>'modifyItem','debug'=>0,'checkFirst'=>1));
}
//print_r($data);//END OF print_r
$query = "active = '$active', orderby = '$orderby', date_modified= '$date_modified'$date_added_q,title = '$title',description = '$description',
description_long = '$description_long',settings ='$settings', permalink = '$permalink' WHERE id = $id";
$sql->db_Update($this->tables['item'],$query);
//echo "UPDATE ".$this->tables['item']." SET $query";

//content CATEGORIES
if (is_array($data['cat'])) 
{
$sql->db_Delete($this->tables['pages'],"itemid = $id");
foreach ($data['cat'] as $v)
{
	$main = ($v == $main_cat) ? 1 : 0;
	$sql->db_Insert($this->tables['pages'],"$v,$id,$main");	
}
}

//Providers
if ($data['providerid'] AND $data['providerid'] != '%') 
{
	$sql->db_Select("provider_items","id","itemid = $id AND type = '".$this->module['name']."'");
	if ($sql->db_Rows()) 
	{
		$sql->db_Update("provider_items","uid = ".$data['providerid']." WHERE itemid = $id AND type = '".$this->module['name']."' ");
	}
	else 
	{
		$sql->db_Insert("provider_items","'','".$data['providerid']."','$id','".$this->module['name']."','".time()."'");
//		echo "'','".$data['provider']."','$id','".time()."'";
	}
}
if ($data['smartcat']) {
	$d_items = new SmartCategories(array('module'=>$this->module,'debug'=>0));	
	$sql->db_Delete('smart_page_categories',"itemid = $id AND module = '".$this->module['name']."'");
	foreach ($data['smartcat'] as $v)
	{
		$main = ($v == $main_cat) ? 1 : 0;
		$sql->db_Insert('smart_page_categories',"$v,$id,$main,'".$this->module['name']."'");	
	}
}

if ($commonCategoriesTree AND $tableID) {
	foreach ($commonCategoriesTree as $k=>$v) {
		$sql->db_Delete("common_categories_tree_items","itemid = $id AND tableID = $k");
		$inserts = array();
		if (is_array($v)) {
			$i=0;
			foreach ($v as $l)
			{
				$inserts[] = "('$l','$id','$i','$k')";
				$i++;
			}
			$sql->q('INSERT INTO common_categories_tree_items VALUES '.implode(',',$inserts));
//			echo 'INSERT INTO common_categories_tree_items VALUES '.implode(',',$inserts)."<br>";
			
		}
		else {
			$sql->db_Insert("common_categories_tree_items","'$v','$id','','$k'");
		}
		
	}
}
if ($commonCategoriesFlat) {
	foreach ($commonCategoriesFlat as $k=>$v) {
		$sql->db_Delete("common_categories_tree_items","itemid = $id AND tableID = $tableID");
		$sql->db_Insert("common_categories_tree_items","'$v','$id','$tableID'");
	}
}

//add extra fields
$this->UpdateExtraFieldsValues($data,$data['id']);


//UPDATE TAGS
$tags = new tags(array('module'=>$this->module,'debug'=>0,'itemid'=>$id));
$tags->SaveTags($data['tags']);

if ($this->module['name'] == 'recipes')//SAVE RECIPE INGREDIENTS
{
	$this->SaveRecipeIngredients($id,$data,array());
	
}//END SAVE INGREDIENTS
if (is_numeric(USE_CACHE)) {
	$cache = new memCacheExt();
	$cache->deleteItem($id,$this->module['name']);
}

return "modified";
 }//END OF NO ERRORS

 else {
 	return $error_list;
 }
}//END FUNCTION

function UpdateExtraFieldsValues($data,$itemid) 
{ 
		global $sql;
		$t = new textparse();

		foreach ($data as $key => $v)
		{
			$id = $v['id'];
			$val = $v['value'];
			$sql->db_Select("extra_field_values","itemid","itemid = '$itemid' AND fieldid = '$id'");
			if ($sql->db_Rows() > 0) 
			{ 
			$query = "value = '$val' WHERE itemid = '$itemid' AND fieldid = '$id'";
//			echo $query;
			$sql->db_Update("extra_field_values",$query);	
			}//END OF IF
			else 
			{ 
			if ($val) 
			 {  
			$sql->db_Insert("extra_field_values","'$itemid','$id','$val'"); 
			
			}//END OF IF 
			}//END OF ELSE
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function AddExtraFieldValues($data,$itemid) 
{ 
		global $sql;
		
		while(list($key,$val)=each($data)) 
		{
		if (strstr($key,"efields-")) 
		{
			list($field,$id)=split("-",$key);
			if ($val) 
			{  
			$query = "'$itemid','$id','$val'";
			$sql->db_Insert("extra_field_values",$query);
			}//END OF IF not empty field
		}//END OF IF
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function LatestItems($settings)
{

	 $settings = array_merge($this->settings,$settings);
	 $settings['exclude'] = ($settings['exclude']) ? $settings['exclude'] : '';//compatibility fix
	 $settings['availability'] = ($settings['active']) ? $settings['active'] : 1;//compatibility fix
	 $settings['sort'] = ($settings['sort']) ? $settings['sort'] : $this->module['settings']['default_sort'];
	 $settings['sort_direction'] = ($settings['sort_direction']) ? $settings['sort_direction'] : $this->module['settings']['default_sort_direction'];
	 $settings['results_per_page'] = ($settings['results_per_page']) ? $settings['results_per_page'] : $this->module['settings']['items_per_page'];	

	 $res = $this->ItemSearch($settings,$this->module,$settings['page'],$settings['debug']);
	 if ($settings['just_results']) {
	 	return $res['results'];
	 }
	 else {
	 	return $res;
	 }	
}//END FUNCTION

function ItemSearch($posted_data,$settings,$page=0,$query=0)
{
	global $sql,$smarty;
	$search_condition = array();
	$results['data'] = $posted_data;
	$included_tables = array();
	$posted_data['sort'] = ($posted_data['sort']) ? $posted_data['sort'] : $this->module['settings']['default_sort'];
	$posted_data['sort_direction'] = ($posted_data['sort_direction']) ? $posted_data['sort_direction'] : $this->module['settings']['default_sort_direction'];
	$posted_data['results_per_page'] = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $this->module['settings']['items_per_page'];
	############### SETUP QUERY ##############################
$search_tables[] = $this->tables['item'];
if ($posted_data['included_table']) {
	foreach ($posted_data['included_table'] as $k=>$v) {
		$search_tables[] = $v;
		$included_tables[] = $k;
	}
}
############# SKU ######################
if ($posted_data['id'] != "") 
{  
 $search_condition[] = " ".$this->tables['item'].".id = '".$posted_data['id']."'";
}//END OF IF
################## CATEGORIES ####################
if ($posted_data['categories'] AND is_array($posted_data['categories'])) {
	foreach ($posted_data['categories'] as $v) {
		$m[] = $v['categoryid'];
	}
	 $search_condition[] = " ".$this->tables['pages']." IN (".implode(",",$m).")";
	$search_tables[] = "INNER JOIN ".$this->tables['pages']." ON (".$this->tables['item'].".id = ".$this->tables['pages'].".itemid)";
	$included_tables[] = $this->tables['item'];
}//END MULTIPLE CATEGORIES
//echo $posted_data['categoryid']."\n";
if ($posted_data['categoryid'] != "%" AND $posted_data['categoryid']) 
{
if (!empty($posted_data["search_in_subcategories"])) 
{
	
# Search also in all subcategories
	$sql->db_Select($this->tables['category'],"categoryid_path","categoryid = '".$posted_data["categoryid"]."'");
//	echo "SELECT categoryid_path FROM ".
	$r = $sql->db_Fetch();
	$categoryid_path = $r['categoryid_path'];
	$sql->db_Select($this->tables['category'],"categoryid","categoryid = '".$posted_data["categoryid"]."' OR categoryid_path LIKE '$categoryid_path/%'");
//	echo "<Br><Br>SELECT categoryid FROM".$this->tables['category']." WHERE categoryid = '".$posted_data["categoryid"]."' OR categoryid_path LIKE '$categoryid_path/%'<br><br>";
	$categoryids_tmp = execute_multi($sql,0);	
	
	if (is_array($categoryids_tmp) && !empty($categoryids_tmp)) 
	{
		foreach ($categoryids_tmp as $k=>$v) 
		{
		$categoryids[] = $v["categoryid"];
		}//END OF FOREACH
        $main_q = ($posted_data['main_category_items']) ? " AND main = 1 " : "";
		$search_condition[] = $this->tables['pages'].".catid $category_sign IN (".implode(",", $categoryids).") $main_q ";
	}//END OF IF
}//END OF IF
else //Search in top category only
  {  
 $search_condition[] = $this->tables['pages'].".catid = ".$posted_data['categoryid'];
  }//END OF ELSE  
$search_tables[] = "INNER JOIN ".$this->tables['pages']." ON (".$this->tables['item'].".id = ".$this->tables['pages'].".itemid)";
$included_tables[] = $this->tables['pages'];
}//END OF IF
##################### PROVIDER ###########3
if ($posted_data['provider_id'] != "%" AND $posted_data['provider_id']) 
{
$search_tables[] = "INNER JOIN provider_items ON (".$this->tables['item'].".id = provider_items.itemid)";
$search_condition[] = " provider_items.type = '".$this->module['name']."' AND provider_items.uid =".$posted_data['provider_id'];
$included_tables[] = 'provider_items';
}
if ($posted_data['user'] != "%" AND $posted_data['user']) 
{
//First get the userid
$sql->db_Select("users","id","uname = '".$posted_data['user']."'");
$a =execute_single($sql);
$search_tables[] = "INNER JOIN provider_items ON (".$this->tables['item']." = provider_items.itemid)";
$search_condition[] = " provider_items.type = '".$this->module['name']."' AND provider_items.uid =".$a['id'];
$included_tables[] = 'provider_items';	

}
if ($posted_data['searchFields']) {
	foreach ($posted_data['searchFields'] as $k=>$v) {
		if ($v['type'] == 'LIKE') {
			 $condition[] = $this->tables['item'].".$k LIKE '%".$v["val"]."%'";
		}
		elseif ($v['type'] == 'NOTLIKE') { 
			$condition[] = $this->tables['item'].".$k NOT LIKE '%".$v["val"]."%'";
		}
		elseif ($v['type'] == 'EQ') { 
			$condition[] = $this->tables['item'].".$k = '".$v["val"]."'";
		}
		elseif ($v['type'] == 'NE') { 
			$condition[] = $this->tables['item'].".$k != '".$v["val"]."'";
		}
	}
	if ($condition) {
		$search_condition[] = implode(" AND ", $condition);
	}
}
############## PATTERN SEARCH ################
if (!empty($posted_data["substring"])) 
{
$posted_data['substring'] = trim($posted_data['substring']);
if ($posted_data['search_main'] != "") 
{  
 $condition[] = $this->tables['item'].".title LIKE '%".$posted_data["substring"]."%'";
}//END OF IF
if ($posted_data['search_short'] != "") 
{  
 $condition[] = $this->tables['item'].".description LIKE '%".$posted_data["substring"]."%'";
}//END OF IF
if ($posted_data['search_long'] != "") 
{  
 $condition[] = $this->tables['item'].".description_long LIKE '%".$posted_data["substring"]."%'";
}//END OF IF

if (!empty($condition))
{ 
	$search_condition[] = "(".implode(" OR ", $condition).")";
}//END OF IF
}//END OF PATERN SEARCH

if ($posted_data['date-from']) {
$search_condition[]	= $this->tables['item'].".date_added > ".$posted_data['date-from'];
}
if ($posted_data['date-to']) {
$search_condition[]	= $this->tables['item'].".date_added < ".$posted_data['date-to'];
}


############################# EXTRA FIELD VALUE ############################
	if ($posted_data['efield'] AND $posted_data['efieldValue']) 
	{
			$search_condition[] = " id IN (SELECT itemid FROM extra_field_values INNER JOIN extra_fields WHERE var_name = '".$posted_data['efield']."' AND module = '".$this->module['name']."' AND value = '".$posted_data['efieldValue']."')";
	}//ENS SINGLE FIELD
##################### EXCLUDE ITEMS #######################
if ($posted_data['exclude']) {
	$search_condition[] = " ".$this->tables['item'].".id NOT IN (".$posted_data['exclude'].")";
}

if ($posted_data['advanced'] != '%') {
	if ($posted_data['advanced'] == 'provider') {//GET ALL PRODUCTS WITH NO REFERENCE IN THE PROVIDER TABLE
		$search_tables[] = "LEFT JOIN eshop_provider_products ON (products.id = eshop_provider_products.itemid)";
		$search_condition[] = " eshop_provider_products.itemid IS NULL  ";
		$included_tables[] = 'eshop_provider_products';	
	}
	elseif ($posted_data['advanced'] == 'price')
	{
		$search_tables[] = "LEFT JOIN eshop_item_details ON (products.id = eshop_item_details.itemid)";
		//$search_condition[] = " eshop_item_details.itemid IS  NOT NULL ";
		$included_tables[] = 'eshop_item_details';		
	}
	elseif ($posted_data['advanced'] == 'manufacturer')
	{
		$search_tables[] = "LEFT JOIN products_manufacturers ON (products.id = products_manufacturers.productid)";
		$search_condition[] = " products_manufacturers.productid IS NULL ";
		$included_tables[] = 'products_manufacturers';		
	}
	elseif ($posted_data['advanced'] == 'category')
	{
		$search_tables[] = "LEFT JOIN ".$this->tables['pages']." ON (".$this->tables['item'].".id = ".$this->tables['pages'].".itemid)";
		$search_condition[] = $this->tables['pages'].".itemid IS NULL  ";
		$included_tables[] = $this->tables['pages'];		
	}
	elseif ($posted_data['advanced'] == 'related')
	{
		$search_tables[] = "LEFT JOIN item_links ON (".$this->tables['item'].".id = item_links.content_source)";
		$search_condition[] = " item_links.content_source IS NULL";
		$included_tables[] = 'item_links';	
	
	}
	elseif ($posted_data['advanced'] == 'image')
	{
        $search_tables[] = "LEFT JOIN item_images ON (".$this->tables['item'].".id = item_images.itemid)";
		$search_condition[] = "item_images.itemid IS NULL  ";
		$included_tables[] = "item_images";		
	}
	elseif ($posted_data['advanced'] == 'tags')
	{
        $search_tables[] = "LEFT JOIN tags_items ON (".$this->tables['item'].".id = tags_items.itemid)";
		$search_condition[] = "tags_items.itemid IS NULL  ";
		$included_tables[] = "tags_items";		
	}
}
/* HOOKS */
if ($settings['hooks']) {
	foreach ($settings['hooks'] as $k=>$v)
	{
		$search_condition[] = $v;
	}
}//END HOOKS
if ($settings['tablesHooks']) {
	foreach ($settings['tablesHooks'] as $k=>$v)
	{
		$search_tables[] = $v;
		$included_tables[] = $k;
	}
}
###################### SORTING ###################	
$sort = ($posted_data['sort']) ? $posted_data['sort'] : $settings['settings']['default_content_sort'];
//End check
$direction = ($posted_data['sort_direction']) ? $posted_data['sort_direction'] : $settings['settings']['default_sort_direction'];
$selected = ($posted_data['sort']) ? $sort : $settings['settings']['default_content_sort'];
//print_r($posted_data);
if ($sort == ("date_added")) { $selected = $this->tables['item'].".date_added";}
elseif ($sort == "title") 
{
	$selected = $this->tables['item'].'.title';
}
elseif ($sort == 'catid')
{
		if (!in_array($this->tables['pages'],$included_tables)) 
	{
		$search_tables[] = "INNER JOIN ".$this->tables['pages']." ON (".$this->tables['item'].".id = ".$this->tables['pages'].".itemid)";
		$included_tables[] = $this->tables['pages'];
	}
}
elseif ($sort == 'price')
{
	if (!in_array($this->tables['pages'],$included_tables)) 
	{
		$search_tables[] = "INNER JOIN ".$this->tables['pages']." ON (".$this->tables['item'].".id = ".$this->tables['pages'].".itemid)";
		$included_tables[] = $this->tables['pages'];
	}
	
}

if ($posted_data['availability'] OR $posted_data['availability'] == 0) 
{
	if ($posted_data['availability'] == 1) 
	{
		$search_condition[] = " active = 1";
		$active = 1;
	}
	elseif (isset($posted_data['availability']) AND $posted_data['availability'] == 0)
	{
		$search_condition[] = " active = 0";
		$active = 0;
	}
	else 
	{
		$search_condition[] = " active IN (1,0) ";
		$active = "no";
	}
}

if ($posted_data['lang']) {
	$search_condition[] = " code = '".$posted_data['lang']."'";
}//END LANG

$fields = $this->tables['item'].".id";
if ($settings['fieldsHooks']) {
	foreach ($settings['fieldsHooks'] as $v) {
		$fields .= ",$v";
	}
}
$search_tables = implode(" ",$search_tables);
/*print_r($search_condition);
echo "<Br>-----------<Br>";*/
$search_condition = implode(" AND ",$search_condition);
$extras = " 
GROUP BY ".$this->tables['item'].".id
ORDER BY $selected $direction";
//echo "$selected --> $direction";
//EXECUTE THE QUERY AND GATHER THE ID'S
if ($posted_data['justIds']) { //WILL RETURN ONLY THE IDS OF THE SEARCH
	$sql->db_Select($search_tables,$this->tables['item'].".id",$search_condition);
	if ($query == 2) {
		echo "SELECT ".$this->tables['item'].".id FROM $search_tables WHERE $search_condition<br><br>";
	}
	if ($sql->db_Rows()) {
		$a = execute_multi($sql);
		
		foreach ($a as $v)
		{
			$ret[] = $v['id'];
		}
	}
	return $ret;
}
else {
	$sql->db_Select($search_tables,"COUNT(*) as total",$search_condition);
}
#################### PAGINATE ###################
$current_page = ($posted_data['page']) ? $posted_data['page'] : 1;
$results_per_page = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $settings['settings']['items_per_page'];
if (isset($posted_data['start'])) 
{
	$start = $posted_data['start'];
}
else {
	$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
}
//echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page."<br>";
$tot = execute_single($sql);
$total = $tot['total'];

if ($query == 1) {
	$smarty->assign("query", "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page);
}
elseif ($query == 2)
{
	echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page."<br>";
	echo "SELECT $fields FROM $search_tables WHERE $search_condition $extras LIMIT $start,".$results_per_page."\n<br>";
}
//echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page;
if ($total) 
{
$results['data']['total'] = $total;
$sql->db_Select($search_tables,$fields,"$search_condition $extras LIMIT $start,".$results_per_page);
$search_results = execute_multi($sql,1);
paginate_results($current_page,$results_per_page,$total,$posted_data['suffix']);
if ($posted_data['return_ids'])
{ 
	
	return array('total'=>$total,'results'=>$search_results,'data'=>$posted_data);
}

$posted_data['module'] = $this->module;
for ($i=0;count($search_results) > $i;$i++)
{
	$content[$i] = $this->GetItem($search_results[$i]['id'],array_merge($settings,$posted_data));
}//END OF FOR

$results['results'] = $content;

}//END OF IF

return $results;
}//END FUNCTION

function FeaturedContent($cat,$settings=0)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$limit = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
	$query[] = "module = '".$this->module['name']."'";
	$query[] = "categoryid = $cat";
	if ($settings['type']) {
		$query[] =  "type = '".$settings['type']."'";
	}
	
	$way = ($settings['way']) ? "  ".$settings['way'] : "";
	if (is_array($query)) {
		$q = implode(" AND ",$query);
	}
	$q .= $orderby;
	$q .= $way;
	$q .= $limit;
	$mode = (is_array($query)) ? "default" : 'no_where';
	$sql->db_Select($this->tables['featured'],$fields,$q,$mode);
	if ($settings['debug']) {
		echo "SELECT $fields FROM ".$this->tables['featured']." WHERE $q";
	}
	if ($sql->db_Rows()) {
	$res = execute_multi($sql);
	if ($settings['get_results']) {
		$active = ($settings['active']) ? $settings['active'] : 1;
		$provider = ($settings['provider']) ? $settings['provider'] : 0;
		$get_user = ($settings['get_user']) ? $settings['get_user'] : 0;
		$debug_items = ($settings['debug_items']) ? $settings['debug_items'] : 0;
		$thumb = ($settings['thumb']) ? $settings['thumb'] : 0;
		$efields = ($settings['efields']) ? $settings['efields'] : 0;
		$tags = ($settings['tags']) ? $settings['tags'] : 0;
		$votes = ($settings['votes']) ? $settings['votes'] : 0;
		$comments = ($settings['comments']) ? $settings['comments'] : 0;
		$gallery = ($settings['gallery']) ? $settings['gallery'] : 0;
		$images = ($settings['images']) ? $settings['images'] : 0;
		for ($i=0;count($res) > $i;$i++)
		{
			if ($settings['get_cat']) {
				$res[$i]['cat'] = $this->ItemCategory($res[$i]['categoryid'],$settings);
			}
			
			$settings['fields'] = '*';//reset
			if ($res[$i]['type'] == 'cat') {
				$res[$i]['item'] = $this->ItemCategory($res[$i]['itemid'],array('table'=>'content_categories','settings'=>1));
			}
			elseif ($res[$i]['type'] == 'itm')
			{
				$res[$i]['item'] = $this->GetItem($res[$i]['itemid'],$settings);
			}
		}
	}//END RESULTS
	if ($settings['return'] == 'single') {
		return $res[0];
	}
	}//END ROWS
	return $res;
}//END FUNCTION

function RelatedContent($source,$settings=0)
{
	global $sql;
    
    $fields = ($settings['fields']) ? $settings['fields'] : "*";
	$limit = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
	$query[] = "module = '".$this->module['name']."'";
	$query[] = "content_source = $source";
	$query[] = ($settings['module_dest']) ? "module_dest = '".$settings['module_dest']."'" : "module_dest = '".$this->module['name']."'";
	$way = ($settings['way']) ? "  ".$settings['way'] : "";
	if (is_array($query)) {
		$q = implode(" AND ",$query);
	}
	$q .= $orderby;
	$q .= $way;
	$q .= $limit;
	$mode = (is_array($query)) ? "default" : 'no_where';
	$sql->db_Select($this->tables['related'],$fields,$q,$mode);
	if ($settings['debug']) {
		echo "SELECT $fields FROM ".$this->tables['related']." WHERE $q";
	} 

	if ($sql->db_Rows()) {
	$res = execute_multi($sql);
	if ($settings['get_results']) {

		for ($i=0;count($res) > $i;$i++)
		{
			$settings['fields'] = '*';//reset
			$res[$i]['item'] = $this->GetItem($res[$i]['content_dest'],$settings);
		}
	}//END RESULTS
	if ($settings['return'] == 'single') {
		return $res[0];
	}
	}//END ROWS
	return $res;
}//END FUNCTION
function DeleteContent($ids,$settings=0)
{
	global $sql;
	if ($this->module['name'] == 'content')
	{
		$tables[] = array('table' => 'content','field' => 'id');
		$tables[] = array('table' => 'content_page_categories','field' => 'itemid');
	}
	elseif ($this->module['name'] == 'products')
	{
		$tables[] = array('table' => 'products','field' => 'id');
		$tables[] = array('table' => 'products_page_categories','field' => 'itemid');
		$tables[] = array('table' => 'products_classes_options','field' => 'itemid');
		$tables[] = array('table' => 'products_classes','field' => 'itemid');
	}
	elseif ($this->module['name'] == 'listings')
	{
		$tables[] = array('table' => 'listings','field' => 'id');
		$tables[] = array('table' => 'listings_page_categories','field' => 'itemid');
		$tables[] = array('table' => 'listings_classes_options','field' => 'itemid');
		$tables[] = array('table' => 'listings_classes','field' => 'itemid');
	}
	
	$tables[] = array('table' => 'tags','field' => 'itemid');
	
	$tables[] = array('table' => 'item_links','field' => 'content_source','module'=>$this->module['name']);
	$tables[] = array('table' => 'item_images_aditional','field' => 'itemid','module'=>$this->module['name']);
	$tables[] = array('table' => 'item_images','field' => 'itemid','module'=>$this->module['name']);
	$tables[] = array('table' => 'item_documents','field' => 'itemid','module'=>$this->module['name']);
	$tables[] = array('table' => 'item_featured','field' => 'itemid','module'=>$this->module['name']);
	$tables[] = array('table' => 'extra_field_values','field' => 'itemid','module'=>$this->module['name']);
	$tables[] = array('table' => 'tags_items','field' => 'itemid','module'=>$this->module['name']);

	$idSet = implode(",",$ids);
	foreach ($tables as $v)
	{
		if ($v['module']) {
			$q = " AND module = '".$v['module']."'";
		}
		$sql->db_Delete($v['table'],$v['field']." IN ($idSet)");
//		echo "DELETE FROM ".$v['table']." WHERE ".$v['field']." IN ($idSet);<br>";
	}
	//Remove Images
	$MediaFilesSettings = MediaFiles(); 
	foreach ($ids as $v)
	{
		foreach ($MediaFilesSettings as $p) {
			$ItemDir = $p['folder']."/".$this->module['name']."_".$v;
			$BaseDir = $_SERVER['DOCUMENT_ROOT'].$ItemDir;
			if (is_dir($BaseDir))
				recursive_remove_directory($BaseDir);
			}
		}
}//END FUNCTION

function NextPrevItems($item,$settings=0)
{
	global $sql;
	$current_category = $item['category'];
	
	$posted_data = ($settings['posted_data']) ? $settings['posted_data'] : array('categoryid'=>$cat,'availability'=>1,'thumb'=>0,'main'=>1);
	$posted_data['sort'] = ($current_category['settings']['orderby']) ? $current_category['settings']['orderby'] : $this->module['settings']['default_sort'];
	
	if ($current_category['settings']['orderby'] AND !$posted_data['sort'] AND !$posted_data['sort_direction']) {
		$posted_data['sort'] = $current_category['settings']['orderby'];
		$posted_data['sort_direction'] = $current_category['settings']['way'];
	}
	elseif (!$posted_data['sort'] AND !$posted_data['sort_direction'] AND !$current_category['orderby'])
	{
		$posted_data['sort'] = $current_module['settings']['default_content_sort'];
		$posted_data['sort_direction'] = $current_module['settings']['default_sort_direction'];
	}
	if ($current_category['settings']['results_per_page'] AND !$posted_data['results_per_page']) {
		$posted_data['results_per_page'] = $current_category['settings']['results_per_page'];
	}
	else {
		$posted_data['results_per_page'] = ($current_category['results_per_page']) ? $current_category['results_per_page'] : $current_module['settings']['items_per_page'];	
	}

	//GET ALL ITEMS
	$sql->db_Select("content INNER JOIN content_page_categories ON (content.id=content_page_categories.itemid)","id",
	"catid = ".$current_category['categoryid']." ORDER BY ".$posted_data['sort']." ".$posted_data['sort_direction']);

	$all_items = execute_multi($sql);
	
	$i=0;
	foreach ($all_items as $key => $val)
	{
		$tmp[] = $val['id'];
		if ($val['id'] == $item['id']) 
		{
			$current_id = $i;
		}
		$i++;
	}
	$prevID = (in_array($tmp[$current_id-1],$tmp)) ? $tmp[$current_id-1] : $tmp[count($tmp)-1];
	$nextID = (in_array($tmp[$current_id+1],$tmp)) ? $tmp[$current_id+1] : $tmp[0];
	
	$ret['prev'] = $this->GetItem($prevID);
	$ret['next'] = $this->GetItem($nextID);
	//Get number of pages to find out the page of this item
	if (is_array($tmp) AND $posted_data['results_per_page']) {
		$chunks = array_chunk($tmp, $posted_data['results_per_page']);
		for ($i=0;count($chunks) > $i;$i++)
		{
			if (in_array($item['id'],$chunks[$i])) 
			{
				$ret['current_page'] = $i+1;
			}
		}
	}//END CHUNCKS

	
	return $ret;
}//END FUNCTION

function recurseCategories($ar=0,$start=0,$Catsettings=0)
{
	global $sql;
	$ar = $this->ItemTreeCategories($start,$Catsettings);
	if ($ar) {
		foreach ($ar as $k=>$v)
		{
			if ($v['num_sub']) {
				$ar[$k]['subs'] = $this->recurseCategories($ar,$v['categoryid'],$Catsettings);
			}
		}
	}
	return $ar;
}//END FUNCTION

function recurseCommonCategories($tableID,$ar=0,$start=0,$Catsettings=0)
{
	global $sql;
	$ar = $this->CommonCategoriesTree($tableID,$start,$Catsettings);
	for ($i=0;count($ar) >$i;$i++)
	{
		if ($ar[$i]['num_sub']) {
			$ar[$i]['subs'] = $this->recurseCommonCategories($tableID,$ar,$ar[$i]['categoryid'],$Catsettings);
							  
		}
	}
	return $ar;
}//END FUNCTION


function applyCategoryFilters($module,$implodedItemIds,$settings=0)
{
	global $sql;

	
	if ($settings['commonCategories']) {
		$posted_data['commonCategoryID'] = ($posted_data['commonCategoryID']) ? $posted_data['commonCategoryID'] : 0;
		//GET COMMON CATEGORIES BASED ON THE IDS
		$sql->db_Select("common_categories_tree INNER JOIN common_categories_tree_items ON (common_categories_tree_items.catid=common_categories_tree.categoryid)
		INNER JOIN common_categories_tables ON (common_categories_tree_items.tableID=common_categories_tables.id)","table_name,common_categories_tree.tableID","common_categories_tree.module = '$module' AND itemid IN (".$implodedItemIds.") GROUP BY categoryid ORDER BY order_by");
		$commonCategories = execute_multi($sql);
		
		if ($commonCategories) {
			$commonCategories = array_unique($commonCategories);
			foreach ($commonCategories as $v)
			{
				$commonCategoriesTables[$v['table_name']] = $v;
				$commonCategoriesTables[$v['table_name']]['results'] = ($settings['recursive']) ? $this->recurseCommonCategories($v['tableID'],$posted_data['commonCategoryID'],0,array('num_subs'=>1,'num_items'=>1,'debug'=>0,'filteredItems'=>$implodedItemIds)) :
				$this->CommonCategoriesTree($v['tableID'],$posted_data['commonCategoryID'],array('debug'=>0,'num_items'=>1,'num_subs'=>1,'filteredItems'=>$implodedItemIds));
			}
		}
		$ret['commonCategories'] = $commonCategoriesTables;
	}//END COMMON CATEGORIES
	
	if ($settings['efields']) {
		
	}//END EXTRA FIELDS
	return $ret;
}//END FUNCTION

}//END CLASS
?>