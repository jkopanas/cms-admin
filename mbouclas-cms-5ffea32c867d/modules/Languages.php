<?php
class Languages
{
	var $itemid;
	var $module;
	var $settings;


	
	
function getAllCountries($lang,$active=0) 
{ 
	global $sql;
	$sql->db_Select("country_codes","*","lang = '$lang' ORDER BY country"); 
	return execute_multi($sql,1);
}//END OF FUNCTION get_country_codes

function AvailableLanguages($settings=0) 
{ 
	global $sql;
	if ($settings['active']) {
		$q = "active = '".$settings['active']."'";
		$mode = 'default';
	}
	else {
		$mode="no_where";
	}

	$sql->db_Select("countries","*",$q,$mode);
	if ($sql->db_Rows()) {
		$countryCodes = execute_multi($sql);
		$res = $countryCodes;
		if ($settings['findDefault'] OR $settings['excludeDefault']) {
			$default = $this->getDefaultLanguage($settings['findDefault']);
			for ($i=0;count($res) > $i;$i++)
			{
				if ($res[$i]['code'] == $default) {
					if ($settings['findDefault']) {
						$res[$i]['default'] = 1;
					}
					if ($settings['excludeDefault']) {
						unset($res[$i]);
						sort($res);
					}
				}
				if ($settings['replaceURL']) {
					if ($settings['replaceURL'] == '/') {
						$res[$i]['thisURL'] = $settings['replaceURL'].$res[$i]['code']."/index.html";
					}
					else {
						if ( preg_match('(/[a-z]{2}/)',$settings['replaceURL']))
						{
							$res[$i]['thisURL'] =  preg_replace('(/[a-z]{2}/)','/'.$res[$i]['code'].'/',$settings['replaceURL']);
						}
						else {
							$res[$i]['thisURL'] = $res[$i]['code'].$settings['replaceURL'];
						}
						
					}
				}
			}
		}//END DEFAULT LANGUAGE
	return $res;
	}//END ROWS

}//END OF FUNCTION get_country_codes
	


function get_country($code) 
{ 
	 global $sql;
	 $sql->db_Select("countries","*","code = '$code'");
	 return execute_single($sql,1);
}//END OF FUNCTION get_country

function getTranslations($settings=0) 
{
	global $sql;
	
	$number = (!$settings['results_per_page']) ? PAGINATION_TOP_ADMIN : $settings['results_per_page'];
	$orderby = ($settings['orderby']) ? $settings['orderby'] : ' name ';
	$way = ($settings['way']) ? $settings['way'] : ' asc ';
	if ($settings['topic']) {
		$q[] = $settings['topic'];
	}
	if ($settings['code']) {
		$q[] = "code = '".$settings['code']."'";
	}
	if ($settings['filter']) {
		$filter = $settings['filter'];
		$q[] = "(name LIKE '%$filter%' OR descr LIKE '%$filter%' or value LIKE '%$filter%')";
	}
	
	$query = implode(' AND ',$q);
	$sql->db_Select("languages","*","$query");
	$total = $sql->db_Rows();
	$start = ($settings['page']) ? ($settings['page']*$number)-$number : 0;
	$sql->db_Select("languages","*","$query order by $orderby $way LIMIT $start,".$number); 
	if ($settings['debug']) {
		echo "SELECT * FROM languages WHERE $query order by $orderby $way LIMIT $start,".$number;
	}
	
	$res = execute_multi($sql);
	
	
	$options = array(
							"total" => $total,
							"number" => $number
             			);
	
	return array('data' => $res , "option" => $options );
}//END OF FUNCTION get_translations

function LoadLanguage($code)
{
	global $memcache;
	$url =md5($_SERVER['HTTP_HOST'].'_languages');
if (is_numeric(USE_CACHE)) {
//    $memcache->delete($url);
	$value = $memcache->get($url);
	if ($value) {
		return $value;
	}
}
	
global $sql;
$sql->db_Select("languages","name,value","code = '$code'");

$res = execute_multi($sql,1);
foreach ($res as $k=>$v)
{
	$lang[$v['name']] = $v['value'];
}
if (is_numeric(USE_CACHE)) {
	$memcache->set($url,$lang,0,TTL);
}
return $lang;


}//END OF FUNCTION

function addTranslation($settings) {
	global $sql;
	$t = new textparse();
	$all_countries = $this->AvailableLanguages();
	$name = $t->formtpa($settings['Varname']);
	$topic = $t->formtpa($settings['topic']);
	for ($i=0;count($all_countries) > $i;$i++)//Loop all available countries
	{
		//Add translation for individual countries
		$code = $all_countries[$i]['code'];
		$descv = "descr-".$code;
		$valuev = "value-".$code;
		$descr = $t->formtpa($settings[$descv]);		
		$value = $t->formtpa($settings[$valuev]);
		$sql->db_Insert("languages","'$code' , '$descr' , '$name' , '$value' , '$topic'");
		
	}//END OF FOR
		if (is_numeric(USE_CACHE)) {
			$memcache->delete(md5('languages'));
		}
	return 1;
	
	
}//END OF FUNCTION

function loadSingleVariable($name,$code=0) {
	global $sql;
	$q = "name = '$name'";
	if ($code) {
		$q .= " AND code = '$code'";
	}
	$sql->db_Select("languages","*",$q);
	if ($code) {
		return execute_single($sql,1);
	}
	else {
		$res = execute_multi($sql,1);
		return $res;
	}
	
}//END FUNCTION

function updateSingleTranslation($name,$data)
{
	global $sql,$memcache;
	$p = new Parse();
	$t = new textparse();
	foreach ($data as $k=>$v)
	{
		list($field,$code)=split("-",$k);
		$sql->db_Update("languages","$field = '".$t->formtpa($v)."' WHERE name = '$name' AND code = '$code'");
	}
		if (is_numeric(USE_CACHE)) {
			$memcache->delete(md5('languages'));
		}
	
}//END FUNCTION


function addNewLanguage($code)
{
	global $sql;
	$sql->db_Select("languages","*","code = '".DEFAULT_LANG."'");
	$defaultLANG = execute_multi($sql);
	foreach ($defaultLANG as $v)
	{
		$q[] = "('$code','".$v['descr']."','".$v['name']."','".$v['value']."','".$v['topic']."')";
	}
	$query =  "INSERT INTO languages (`code`, `descr`, `name`, `value`, `topic`) VALUES ".implode(",",$q);
	//mysql_query($query);
	$r = $this->CountryDetails($code);
	$sql->db_Insert("countries","'$code','".$r['charset']."','N','".$r['country']."','".$r['locale']."',
					 '".$r['logo']."', '".$r['image']."','$alias','$settings'"); 
	
	return array( "country" => $r['country'] , "code" => $code );
}//END FUNCTION

function CountryDetails($code) 
{ 
	global $sql;
	$sql->db_Select("country_codes","country,charset","code = '$code'");
	return execute_single($sql);
}//END OF FUNCTION country_details

function setLanguage($lang,$cookie) 
{ 
	$_SESSION[$cookie] = $lang;
}//END OF FUNCTION set_lang

function checkLanguage($lang,$active=0) 
{
	global $sql;
	if ($active) {
		$q = " AND active = '$active'";		
	}
	$sql->db_Select("countries","code","code = '$lang' $q");
	if ($sql->db_Rows()) {
		return $lang;
	}
	else {
		return DEFAULT_LANG;
	}
	
}//END FUNCTION

function getDefaultLanguage($area='default_lang',$settings=0)
{
	global $sql;
	
	$sql->db_Select("config","value","name = '$area'");
	$res = execute_single($sql);
	if ($settings['return'] == 'array') {
		if ($settings['languageDetails']) {
			$res['details'] = $this->get_country($res['value']);
		}
		return $res;
	}
	else {
		return $res['value'];
	}
	
}//END FUNCTION

function itemTranslations($itemid,$code,$module=0,$settings=0)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	if ($settings['field']) {
		$q[] = "field = '".$settings['field']."'";
	}
	if ($settings['table']) {
		$q[] = "translations.table = '".$settings['table']."'";
	}
	if ($module) {
		$q[] = "module = '$module'";
	}
	if ($settings['extraField']) {
		$q[] =	"(settings LIKE '%\"field\":\"".$settings['extraField']['field']."\"%' AND (settings LIKE '%\"value\":".$settings['extraField']['value']."%' OR settings LIKE '%\"value\":\"".$settings['extraField']['value']."\"%'))";
	}
	if ($q) {
		$query = " AND ".implode(" AND ",$q);
	}
	$sql->db_Select("translations",$fields,"itemid = $itemid AND code = '$code' $query");
	if ($settings['debug']) {
		echo "SELECT $fields FROM translations WHERE itemid = $itemid AND code = '$code'  $query<br>";
	}
	if ($settings['field']) {
		$res = execute_single($sql,1);
	}
	else {
		if ($sql->db_Rows()) {
			$res = execute_multi($sql,1);
		if ($settings['groupby'] == 'field') 
		{
			foreach ($res as $v)
			{
				$ret[$v['field']] = $v;
			}
		}
		if ($settings['groupby'] == 'table' ) {
			foreach ($res as $v)
			{
				$ret[$v['table']][] = $v;
			}
		}//END GROUP
		}//END ROW
	}//END ALL TRANSLATIONS
	if (!$ret) {
		return $res;
	}
	else {
		return $ret;
	}
	
}//END FUNCTION

function moveTranslationData($table,$originalLang,$destLang)
{
	global $sql;
	$sql->q("DROP TABLE IF EXISTS `tempTranslations`;");
	$sql->q("CREATE TEMPORARY TABLE IF NOT EXISTS tempTranslations LIKE translations;");
	$sql->q("INSERT INTO tempTranslations  SELECT * FROM translations;");
	$sql->q("DELETE FROM translations WHERE code = '$destLang';");
	foreach ($table as $k=>$v) {
	foreach ($v['fields'] as $field) {
					$fields = array();
					$fields[] = "'$originalLang'";//code
					$fields[] = "'$k'";//table
					$fields[] = $k.".".$v['fieldid'];//itemid
					$fields[] = "$k.$field";//Translation
					$fields[] = $v['module'];//Translation
					$fields[] = "'$field'";//Translation
					$fieldsQuery = implode(",",$fields);
					$sql->q("INSERT INTO translations (code,`table`,itemid,translation,module,field)  
					(SELECT $fieldsQuery
 					FROM $k ".$v['join']." GROUP BY ".$v['fieldid'].");");
					$sql->q("UPDATE translations,tempTranslations SET translations.date_modified = UNIX_TIMESTAMP(), translations.date_added = tempTranslations.date_added,translations.active=tempTranslations.active,translations.settings=tempTranslations.settings WHERE translations.id=tempTranslations.id");
//					echo "INSERT INTO translations (code,`table`,itemid,translation,module,field)  
//					(SELECT $fieldsQuery
// 					FROM $k ".$v['join']." GROUP BY ".$v['fieldid'].");<Br><br>";
//					END MOVE ORIGINAL DATA INTO TEMP TRANSLATION. NOW MOVE TRANSLATIONS TO ORIGNAL DATA
					$sql->q("UPDATE $k,tempTranslations SET $k.$field = tempTranslations.translation WHERE $k.".$v['fieldid']."= tempTranslations.itemid
					AND tempTranslations.table = '$k' AND tempTranslations.code = '$destLang' AND tempTranslations.field = '$field';");
/*					echo "UPDATE $k,tempTranslations SET $k.$field = tempTranslations.translation WHERE $k.".$v['fieldid']."= tempTranslations.itemid
					AND tempTranslations.table = '$k' AND tempTranslations.code = '$destLang' AND tempTranslations.field = '$field';<Br><Br>";*/
				}//END LOOP
				
	}//END LOOP
	
}//END FUNCTION

function getTablesToTranslate()
{
	global $sql;

	$sql->db_Select("translations_tables","*");
	$tmp = execute_multi($sql);
	foreach ($tmp as $v) {
		$res[$v['table']] = $v;
		$res[$v['table']]['fields'] = explode(",",$res[$v['table']]['fields']);
		if ($res[$v['table']]['module'] != 'module') {
			$res[$v['table']]['module'] = "'".$res[$v['table']]['module']."'";
		}
	}

	return $res;
}//END FUNCTION

function webmasterModeLabels($user_agent, $label, $value)
{
    static $disabled = array(
        'lbl_site_title',
        'txt_site_title',
        'lbl_site_path',
        'lbl_close_storefront',
        'lbl_open_storefront_warning',
        'lbl_search',
        'txt_minicart_total_note'
    );

    // check for exceptions
    if (in_array($label, $disabled) || preg_match('/txt_subtitle.*/i', $label))
        return $value;

    return '<span id="Var-' . $label . '" rel="Var-'.$label.'" class="webmasterMode">' . $value . '</span>';
}//END FUNCTION

function webmasterModeConvertLabels (&$lang)
{
    global $user_agent;
    global $smarty;

    if (is_array($lang)) {

        $lang_copy = array();

        foreach ($lang as $name => $val) {

            $lang_copy[$name] = addcslashes($val, "\0..\37\\");

            $lang_copy[$name] = htmlspecialchars($lang_copy[$name],ENT_QUOTES);

            $lang[$name] = $this->webmasterModeLabels($user_agent,$name,$val);

        }

        $smarty->assign('webmaster_lng', $lang_copy);

    }

}

public function Create_json_translations() {
	global $sql;
	$sql->q("SELECT name,value,code FROM languages");
	$translations=execute_multi($sql);
	
  foreach ($translations as $key=>$value) {		 	  
			 $transl[$value['code']][$value['name']] = $value['value'];

				 
		$new_transl=json_encode($transl[$value['code']]); 
		$file = ABSPATH."/scripts/lang/lang.".$value['code'].".json";
	    $fp = null;
	    $fp = fopen($file, "w");
	       if (!$fp) {
	         return false;
	       }
	     
	   fwrite($fp, $new_transl);   
	   fclose($fp);			 				 
   } 
	
	
} //end function



}//END CLASS

?>