<?php

class Products extends Items {
	
    function GetProduct($itemid,$settings=0) {
    	$settings['debug'] = 2;
		$product = $this->GetItem($itemid,$settings);
		return $product;
    }//END FUNCTIONS
    
    function ProductSearch($posted_data,$settings,$page=0,$query=0)
    {
    	$posted_data['return_ids']=1;//THIS WILL RETURN IDS ONLY FOR POST PROCCESSING
    	$items = $this->ItemSearch($posted_data,$this->module,$page,0);
    	if ($items['results'])
    	{
    		foreach ($items['results'] as $v)
    		{
    			$results[] = $this->GetItem($v['id'],$settings);
    		}//END FOREACH
    		return $results;
    	}//END ITEMS
    }//END FUNCTION
    
    
}//END CLASS

?>