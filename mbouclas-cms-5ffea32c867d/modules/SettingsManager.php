<?php

class SettingsManager {
	
	public function getSetting($id=0,$settings =array()) {
		global $sql;
		
		$fields = ($settings['fields']) ? $settings['fields'] : '*';
		
		if ($id) {
			$q[] = "id = '$id'";
		}
		
		if ($q) {
			$query = implode(' AND ',$q);
			$mode = 'default';
		} else { $mode = "no_where"; }
		
		$sql->db_Select("modules_settings_manager",$fields,"$query ");
		if ($settings['debug']) {
			echo "SELECT $fields FROM modules_settings_manager WHERE $query ";
		}
		
		 return ($sql->db_Rows()) ? execute_single($sql) : array();
		
	}
	
	public function UpdateSettingManager ($id,$data) {
	global $sql;
	$t = new Parse();
		//	print_ar($data);
	foreach ($data['data'] as $k=>$v){
			if ( "select_type" != $k && "default" != $k  ) {
				$v = $t->toDB($v);
				$updateQ[] = "$k = '$v'";
			}
	}//END DATA

	if ($data['settings']) {
		$tmp = array();
		foreach($data['settings']['values'] as $key => $value ) {
			array_push($tmp, array('value' => $value, 'translation' => $data['settings']['translation'][$key], 'default'=> "1"));
		}
		
		$data['settings'] = json_encode(array('values' => $tmp));
		$updateQ[] = "options = '".$data['settings']."'";
		
	} else {
		
		$updateQ[] = "options = ''";
		
	}

	if ($id) {//UPDATE	
		$query = implode(",",$updateQ)." WHERE id = $id";
		$sql->db_Update("modules_settings_manager",$query);
	}
	
	$data['data']['id'] = $id;
	$data['data']['options'] = json_decode($data['settings']);
	return $data['data'];
}
	
	
	public function getSettings($module=0,$file=0,$settings=0) {
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : '*';
		$orderby = ($settings['orderby']) ? $settings['orderby'] : 'orderby';
		$way = ($settings['way']) ? $settings['way'] : 'ASC';
		
		if ($module) {
			$q[] = "module = '$module'";
		}
		if ($file) {
			$q[] = "file = '$file'";
		}

		if ($q) {
			$query = implode(' AND ',$q);
			$mode = 'default';
		}
		else { $mode = "no_where"; }
		
		$sql->db_Select("modules_settings_manager",$fields,"$query ORDER BY $orderby $way",$mode);
		if ($settings['debug']) {
			echo "SELECT $fields FROM modules_settings_manager WHERE $query ORDER BY $orderby $way";
		}
		if ($sql->db_Rows()) {
			$res = execute_multi($sql);
			foreach ($res as $k=>$v) {
				$res[$k]['options'] = json_decode($v['options'],true);
			}
		}
		return $res;
	}//END FUNCTION
	
	function loadModuleSettings($id,$settings=0)
	{
		global $sql;
		$sql->db_Select("modules_settings","name,description,id,type,comments,options,category","module_id = $id");
		$res = execute_multi($sql);
		if ($settings['group'] == 'category') {
			foreach ($res as $v) {
				$ret[$v['category']][] = $v;
			}//END LOOP
			$res = $ret;
		}//END GROUP BY
		return $res;
	}//END FUNCTION
	
	function getAvailableModules()
	{
		global $sql;
		$sql->db_Select("modules_settings INNER JOIN modules ON (modules.id=modules_settings.module_id)","module_id,modules.name","GROUP BY module_id",$mode="no_where");
		return execute_multi($sql);
	}
	
	function loadModuleDetails($id)
	{
		global $sql;
		$sql->db_Select("modules","*","id = $id");
		return execute_single($sql);
	}
	
	function settingsCategories($id)
	{
		global $sql;
		$sql->db_Select("modules","category","id = $id GROUP BY category");
		return execute_multi($sql);
	}
	
	
}//END CLASS

?>