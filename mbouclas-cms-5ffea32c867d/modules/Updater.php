<?php
require_once(ABSPATH."/modules/updater/updater.core.class.php");
require_once(ABSPATH."/modules/updater/updater.plugins.class.php");
require_once(ABSPATH."/modules/updater/updater.traceworker.class.php");
/**
 * The Updater Module. It is Used to Update Vital files of MCMS,
 * and trace or Upgrade the current version to a forthcoming.
 * @author
 *
 */
class Updater
{
	private $sqlDriver;
	private $module;
	private $traceWorker;
	private $coreUpdater;
	private $pluginUpdater;
	private $isUpdating;
	private $isTracing;
	private $currentUpdatingState;
	private $updatingStates;
	private $currentTracingState;
	private $tracingStates;
	private $updateToken;
	private $errorStack;
	private $logStack;
	private $tokenHashPtr;
	private $updaterSettings;

	/**
	 * The Facade pattern of Updating Procedure.
	 * @param array $settings
	 */
	function __construct(&$settings=array())
	{
		$this->init($settings);
	}

	/**
	 * The Facade pattern of Updating Procedure.
	 * @param array $settings
	 */
	function init(&$settings=array())
	{
		global $sql, $loaded_modules;
		// TODO - Settings Pagination - Unzip & Copy
		$this->sqlDriver 						= &$sql;
		$this->module 							= &$loaded_modules['updater'];
		try
		{
			$this->coreUpdater 					= new CoreUpdater();
			$this->pluginUpdater 				= new PluginsUpdater();
			$this->traceWorker 					= new TraceWorkerUpdater();
		}
		catch (Exception $e)
		{
			
		}
		
		
		$this->setUpdaterMode($settings); // Initiate/Update Updater Mode & State
		$this->errorStack						= array();
		$this->logStack							= array();
		$this->updaterSettings					= array();
		
		$this->updaterSettings['isUpdating']	= &$this->isUpdating;
		$this->updaterSettings['isTracing']		= &$this->isTracing;
		$this->updaterSettings['archivePath'] 	= ($this->isTracing? 
					ABSPATH."/updatesrepository/".$this->tokenHashPtr."/" :
					ABSPATH."/templates_c/".$this->tokenHashPtr."/" );
		
		$this->updaterSettings['dbtable'] 		= "updater";
		$this->updaterSettings['version'] 		= $this->getCurrentVersion($settings);
		
		$this->doBuildUpdaterFolder($this->updaterSettings['archivePath'],$settings);
		$this->doUnHibernate($settings);
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterUpdaterCostruction",$settings);
	}

	/**
	 * Continue Process accordingly to the Settigns token Array
	 * @param array $settings
	 * @return Updater
	 */
	public function doNext(&$settings=array())
	{
		$this->doFuseSettings($settings);
		if($this->isTracing)
		{
			return $this->doContinueTracing($settings);
		}
		if($this->isUpdating)
		{
			return $this->doContinueUpdating($settings);
		}
		return $this;
	}

	/**
	 * Continue Process of tracing & build the update Package
	 * accordingly to the Settigns token Array
	 * @param array $settings
	 * @return Updater
	 */
	public function doContinueTracing(&$settings=array())
	{
		$this->doTriggerHook("beforeContinueTracing",$settings);
		$this->doFuseSettings($settings);
		$copiesArr = array();
		if(!is_array($settings['foldercopies']))
		{
			$settings['foldercopies'] = array();
		}
		if(!is_array($settings['filecopies']))
		{
			$settings['filecopies'] = array();
		}
		foreach($settings['copies'] as &$filePTR)
		{
			$copiesArr[] = ($filePTR['path'] = ABSPATH.$filePTR['path']);
			$isDir = ($filePTR['type'] == "folder" ? true : is_dir($filePTR['path']) );
			
			if($isDir)
			{
				$filePTR['path'] 			= str_replace(ABSPATH,'',$filePTR['path']);
				$settings['foldercopies'][] = $filePTR;
			}
			else
			{
				$filePTR['path'] 			= str_replace(ABSPATH,'',$filePTR['path']);
				$settings['filecopies'][] 	= $filePTR;
			}
		}
		$settings['filecopies'][] = array(
				"file" 		=> "manifest.json",
				"path" 		=> "manifest.json",
				"modified" 	=> microtime(),
				"type" 		=> "file"
		);
		$settings['filecopies'][] = array(
				"file" 		=> "dbdump.sql",
				"path" 		=> "dbdump.sql",
				"modified" 	=> microtime(),
				"type" 		=> "file"
		);
		$settings['filecopies'][] = array(
				"file" 		=> "changelog.txt",
				"path" 		=> "changelog.txt",
				"modified" 	=> microtime(),
				"type" 		=> "file"
		);
		$this->doBuildManifest($settings);
		$this->doBuildSQLFile($settings);
		$this->doBuildChangelogFile($settings);

		$settings['manifestfile'] 	= str_replace(ABSPATH,'',$this->updaterSettings['archivePath']."manifest.json");
		$settings['sqlfile'] 		= str_replace(ABSPATH,'',$this->updaterSettings['archivePath']."dbdump.sql");
		$settings['changelogfile']	= str_replace(ABSPATH,'',$this->updaterSettings['archivePath']."changelog.txt");

		$settings['copies'] 		= $copiesArr;
		$settings['copies'][]		= $this->updaterSettings['archivePath']."manifest.json";
		$settings['copies'][] 		= $this->updaterSettings['archivePath']."dbdump.sql";
		$settings['copies'][] 		= $this->updaterSettings['archivePath']."changelog.txt";
		
		$this->doBuildArchive($settings);
		
		// Build Encoded Dir
		$tmpArr = array(
				"manifest.json",
				"dbdump.sql",
				"changelog.txt",
				($settings['title']? $settings['title'] : $this->getCurrentVersion()) . ".zip"
		);
		$this->coreUpdater->copyFilesTraversive($tmpArr, $this->updaterSettings['archivePath'], $this->updaterSettings['archivePath'].'encoded/');
		
		$this->doRegisterUpdate(&$settings);
		
		$settings['status'] = 1;
		if(count($this->errorStack)>0)
		{
			$settings['status'] = -1;
		}
		$settings['error'] = &$this->errorStack;

		$this->doFuseSettings($settings);
		$this->doHibernate($settings);
		$this->doTriggerHook("afterContinueTracing",$settings);
		return $this;
	}

	/**
	 * Continue Process of updating the Current MCMS
	 * accordingly to the Settigns token Array.
	 * @param array $settings
	 * @return Updater
	 */
	public function doContinueUpdating(&$settings=array())
	{
		$this->doTriggerHook("beforeContinueUpdating",$settings);
		$this->doFuseSettings($settings);
		
		switch ($settings['nextAct'])
		{
			case 'updateStarted':
				//$settings['token'] = ($settings['token'] && strlen($settings['token'])>0? $settings['token'] : $this->getNewGeneratedToken($settings));
				//$settings['token'] = (preg_match_all('/([0-9]+).([0-9]+).([0-9]+)/',$settings['token'],$dummy) ? $this->getNewGeneratedToken($settings) : $settings['token'] );
				$settings['next'] = 'fetchFiles';
				$settings['status'] = 1;
				if(count($this->errorStack)>0)
				{
					$settings['status'] = -1;
				}
				$settings['error'] = &$this->errorStack;
				
				break;
			case 'fetchFiles':
				$this->doFetchUpdate($settings);
				$settings['next'] = 'unzipFiles';
				break;
			case 'unzipFiles':
				$this->doUnArchiveReq($settings);
				$this->doUnHibernateByManifest($settings);
				$settings['next'] = 'createRestorePoint';
				break;
			case 'createRestorePoint':
				$this->doCreateRestorePoint($settings);
				$settings['next'] = 'copyFiles';
				break;
			case 'copyFiles':
				$this->doCreateFolders($settings);
				$this->doCopyFiles($settings);
				$settings['next'] = 'updateSql';
				break;
			case 'updateSql':
				$this->doExecuteSQLFile($settings);
				$settings['next'] = 'cleanFiles';
				break;
			case 'cleanFiles':
				$this->doClearTempraryFiles($settings);
				$settings['next'] = '';
				break;
			case 'rollbackfiles':
				$settings['status'] = 1;
				if(count($this->errorStack)>0)
				{
					$settings['status'] = -1;
				}
				$settings['error'] = &$this->errorStack;
			case 'rollbacksql':
				$settings['status'] = 1;
				if(count($this->errorStack)>0)
				{
					$settings['status'] = -1;
				}
				$settings['error'] = &$this->errorStack;
				break;
			default:
				$this->doTriggerHook($settings['nextAct'],$settings);
				break;
		}
		
		$settings['lastAct'] = $settings['nextAct'];
		$settings['nextAct'] = 0;
		unset($settings['nextAct']);
		unset($this->updaterSettings['nextAct']);
		$this->doFuseSettings($settings);
		$this->doHibernate($settings);
		$this->doTriggerHook("afterContinueUpdating",$settings);
		return $this;
	}

	/**
	 * Hibernate the Curent Process, so it may restart
	 * @param array $settings
	 * @return Updater
	 */
	public function doHibernate(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeHibernate");
		
		$settings['time']		= microtime();
		$settings['mode'] 		= ( $settings['mode'] 		? $settings['mode'] 	: ($this->isTracing ? 'tracing' : 'updating') );
		$settings['token'] 		= ( $settings['token'] 		? $settings['token'] 	: $this->tokenHashPtr );
		$settings['progress'] 	= ( $settings['progress'] 	? $settings['progress'] : 0 );
		$settings['total'] 		= ( $settings['total'] 		? $settings['total'] 	: 0 );
		$settings['index'] 		= ( $settings['index'] 		? $settings['index'] 	: 0 );
		$settings['lastAct']	= ( $settings['lastAct'] 	? $settings['lastAct'] 	: 0 );
		$settings['hooks']		= ( $settings['hooks']		? $settings['hooks']	: array());
		
		$fp = @fopen($this->updaterSettings['archivePath'].$this->tokenHashPtr.".json", 'w');
		@fwrite($fp, json_encode($settings));
		@fclose($fp);

		$this->doTriggerHook("afterHibernate");
		return $this;
	}

	/**
	 * UnHibernate Updater's state accordingly with the given token
	 * @param array $settings
	 * @return Updater
	 */
	public function doUnHibernate(&$settings=array())
	{
		if(file_exists($this->updaterSettings['archivePath'].$settings['token'].".json"))
		{
			$this->doTraverseSettingsArray( // Call Function
				json_decode( // First desirilize JSON token
					file_get_contents( // Read JSON File Token
						$this->updaterSettings['archivePath']
						.$settings['token']
						.".json"
					), TRUE
				), $settings // Manipulate Settings Token
			);
		}
		return $this; // Return updater with loded settings
	}

	/**
	 * Traverse Array, Usefull to UnHibernate Uploader settings
	 * @param array $srcArr
	 * @param array $arrPTR
	 * @return multitype:
	 */
	private function doTraverseSettingsArray($srcArr,&$arrPTR)
	{
		// Bug Fix - null arrays
		if(!is_array($arrPTR))
		{
			$arrPTR = array();
		}
		foreach ((new RecursiveArrayIterator($srcArr)) as $k => $v)
		{
			if(is_array($v))
			{
				$this->doTraverseSettingsArray($v,$arrPTR[$k]);
			}
			elseif(isJSON($v,$v,true))
			{
				$this->doTraverseSettingsArray($v,$arrPTR[$k]);
			}
			else
			{
				$arrPTR[$k] = $v;
			}
		}
		return $arrPTR;
	}

	/**
	 * Merge Settings, so a settings tokken may achieved
	 * @param array $settings
	 * @return Updater
	 */
	private function doFuseSettings(&$settings)
	{
		if(!is_array($settings))
		{
			$settings = array();
		}
		$this->doTraverseSettingsArray($settings, $this->updaterSettings);
		$settings = $this->updaterSettings;
		
		return $this;
	}

	/**
	 * Set mode of updater by initial settings
	 * @param unknown_type $settings
	 * @return Updater
	 */
	public function setUpdaterMode(&$settings=array())
	{
		$this->traceWorker->setTraceWorkerStates($this->tracingStates);
		$this->coreUpdater->setUpdatingStates($this->updatingStates);

		if(strlen($settings['mode'])>0)
		{
			$this->isUpdating 	= (strcasecmp($settings['mode'], "update") == 0 ? true : false );
			$this->isTracing 	= !$this->isUpdating; // Boolean trick :)
			$settings['token']	= ($settings['token']? $settings['token'] : $this->getNewGeneratedToken($settings));
			$this->tokenHashPtr = &$settings['token'];
			return $this;
		}

		$this->isTracing 	= (preg_match_all('/([0-9]+).([0-9]+).([0-9]+)/',$settings['token'],$dummy) ? true : false );
		$this->isUpdating 	= !$this->isTracing; // Boolean trick :)
		$this->tokenHashPtr = &$settings['token'];
		return $this;
	}

	/**
	 * Execute Archive Command.
	 * It makes an archive from the generated JSON file List
	 * @param string $jsonToken
	 * @return boolean
	 */
	public function doBuildArchive(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeBuildArchive",$settings);
		
		$tmpArr = array(
			"token" 			=> $this->tokenHashPtr,
			"archiveNameSTR" 	=> ($settings['title']? $settings['title'] : $this->getCurrentVersion()) . ".zip",
			"dirPTR" 			=> $this->updaterSettings['archivePath'],
			"filesArr" 			=> $settings['copies']
		);
		
		if($this->traceWorker->generateArchive($tmpArr))
		{
			unset($settings['copies']);
			$this->doTriggerHook("afterBuildArchive",$settings);
			return $tmpArr;
		}
		unset($settings['copies']);
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterBuildArchive",$settings);
		return false;
	}

	/**
	 * Execute UnZip command
	 * @param file|string $archiveFP
	 * @return Updater
	 */
	public function doUnArchiveReq(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeUnArchiveReq",$settings);
		
		$tmpArr = array(
			"token" 			=> $this->tokenHashPtr,
			"archiveNameSTR" 	=> $settings['token'].".zip",
			"dirPTR" 			=> $this->updaterSettings['archivePath'],
		);
		$settings['status'] = 1;
		
		if($this->coreUpdater->unZipFiles($tmpArr)>0)
		{
			$settings['status'] = 1;
			$settings['error'] = &$this->errorStack;
			$this->doTriggerHook("afterBuildArchive",$settings);
			return $tmpArr;
		}
		$settings['status'] = -1;
		@array_push($this->errorStack, "3002:UnZip Process Failed");
		$settings['error'] = &$this->errorStack;
		
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterUnArchiveReq",$settings);
		return $this;
	}
	
	/**
	 * Create//Copy UnZiped files to Update MCMS
	 * @param array $settings
	 * @return Updater
	 */
	public function doCopyFiles(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeCopyFiles",$settings);
		
		$tmpArr = array(
			"token" 		=> $this->tokenHashPtr,
			"filesArr"		=> $settings['filecopies'],
			"destFilePath" 	=> ABSPATH."/",
			"srcFilePath" 	=> $this->updaterSettings['archivePath'].'tmp/',
		);
		
		$settings['status'] = 1;
		if($this->coreUpdater->copyFiles($tmpArr))
		{
			$settings['status'] = 1;
			$settings['error'] = &$this->errorStack;
			$this->doTriggerHook("afterCopyFiles",$settings);
			return $this;
		}
		
		$settings['status'] = -1;
		@array_push($this->errorStack, "4002:Copy Files Process Failed");
		$settings['error'] = &$this->errorStack;
		
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterCopyFiles",$settings);
		return $this;
	}
	
	/**
	 * Create A restore Point Before Update transaction
	 * @param array $settings
	 * @return Updater
	 */
	public function doCreateRestorePoint($settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeCreateRestorePoint",$settings);
		mkdir($this->updaterSettings['archivePath'].'backup/');
		$tmpArr = array(
					"token" 		=> $this->tokenHashPtr,
					"filesArr"		=> $settings['filecopies'],
					"destFilePath" 	=> $this->updaterSettings['archivePath'].'backup/',
					"srcFilePath" 	=> ABSPATH."/",
		);
		
		$settings['status'] = 1;
		if(!$this->coreUpdater->copyFiles($tmpArr))
		{
			$settings['status'] = -1;
			@array_push($this->errorStack, "7002:Restore Point Creation Failed");
			$settings['error'] = &$this->errorStack;
			$this->doTriggerHook("afterCreateRestorePoint",$settings);
			return $this;
		}
		
		$this->doMakeSQLDump($settings);
		
		$settings['status'] = 1;
		$settings['error'] = &$this->errorStack;
		
		
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterCreateRestorePoint",$settings);
		return $this;
	}
	
	/**
	 * Execute A rollBack Process to files & Dirs
	 * @param array $settings
	 */
	public function doExecuteRollbackFiles(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeExecuteRollbackFiles",$settings);
		
		$tmpArr = array(
				"token" 		=> $this->tokenHashPtr,
				"filesArr"		=> $settings['filecopies'],
				"destFilePath" 	=> ABSPATH."/",
				"srcFilePath" 	=> $this->updaterSettings['archivePath'].'backup/',
		);
		
		$settings['status'] = 1;
		if($this->coreUpdater->copyFiles($tmpArr))
		{
			$settings['status'] = 1;
			$settings['error'] = &$this->errorStack;
			$this->doTriggerHook("afterExecuteRollbackFiles",$settings);
			return $this;
		}
		
		$settings['status'] = -1;
		@array_push($this->errorStack, "4052:RollBack Copy Files Process Failed");
		$settings['error'] = &$this->errorStack;
		
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterExecuteRollbackFiles",$settings);
		return $this;
	}
	
	/**
	 * Make Directories as it's maped at Manifest
	 * @param array $settings
	 * @return Updater
	 */
	public function doCreateFolders(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeCreateFolders",$settings);
		
		if(is_array($settings['foldercopies']))
		{
			foreach($settings['foldercopies'] as $k => $v)
			{
				if(!is_dir(ABSPATH."/".$v))
				{
					@mkdir(ABSPATH."/".$v,0755,true);
				}
			}
		}
		
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterCreateFolders",$settings);
		return $this;
	}
	
	/**
	 * Fetches the archive by $settings['version'] & $settings['token']
	 * @param array $settings 
	 * @return Updater
	 */
	public function doFetchUpdate(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeFetchUpdate",$settings);
		
		$coreToken = array();
		$coreToken['path'] 		= "/templates_c/".$settings['token']."/";
		$coreToken['callUrl'] 	= FETCH_UPDATE_URL; // (	$settings['url']? $settings['url']	: FETCH_UPDATE_URL // DB Table Config - FETCH_UPDATE_URL );
		$coreToken['callUrl'] 	.= $settings['version'];
		
		$this->doTriggerHook("beforeMakeTempDir",$settings);
		$coreToken['filepath'] 	= $this->coreUpdater->makeTempDir($coreToken).$settings['token'].".zip";
		$this->doTriggerHook("afterMakeTempDir",$settings);
		$coreToken['bCounter']	= $this->coreUpdater->fetchRemoteFile($coreToken);
		
		if($coreToken['bCounter']<=0)
		{
			array_push($this->errorStack, "3001: No File was Fetched & Written");
		}
		
		$settings['status'] = 1;
		if(count($this->errorStack)>0)
		{
			$settings['status'] = -1;
		}
		$settings['error'] = &$this->errorStack;
		
		$this->doFuseSettings($settings);
		$this->doHibernate($settings);
		
		$this->doTriggerHook("afterFetchUpdate",$settings);
		return $this;
	}
	
	/**
	 * Execute the SQL of dbdump.sql file of fetched Update Archive
	 * @param array $settings
	 * @return Updater
	 */
	public function doExecuteSQLFile(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeExecuteSQLFile",$settings);
		
		$coreToken = array();
		$coreToken['path'] 		= ABSPATH."/templates_c/".$settings['token']."/tmp/dbdump.sql";
		
		if(($coreToken['sql'] 	= file_get_contents($coreToken['path'])))
		{
			$this->coreUpdater->executeSQL($coreToken);
			
			$this->sqlDriver->db_Update('config',"value='".$settings['version']."' WHERE `name`='version_number'");
			$this->sqlDriver->db_Update('updater',"version='".$settings['version']."' WHERE `module`='core'");
			
			$settings['status'] = 1;
		}
		else
		{
			array_push($this->errorStack, "3003: Couldn't read SQL File");
			$settings['status'] = -1;
		}
		
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterExecuteSQLFile",$settings);
		return $this;
	}
	
	/**
	 * Execute RollBack SQL of MCMS
	 * @param array $settings
	 * @return Updater
	 */
	public function doExecuteRollBackSQL(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeExecuteRollBackSQL",$settings);
	
		$coreToken = array();
		$coreToken['path'] 		= $this->updaterSettings['archivePath']."backupdb.sql";
	
		if(($coreToken['sql'] 	= file_get_contents($coreToken['path'])))
		{
			$this->coreUpdater->executeSQL($coreToken);
			
			$this->sqlDriver->q("UPDATE `config` SET `value`='".$settings['version']."' WHERE (`name`='version_number')");
			$this->sqlDriver->q("UPDATE `updater` SET `version`='".self::getMCMSVersion()." WHERE (`module`='core')");
				
			$settings['status'] = 1;
		}
		else
		{
			array_push($this->errorStack, "3503:RollBack Couldn't read SQL File");
			$settings['status'] = -1;
		}
	
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterExecuteRollBackSQL",$settings);
		return $this;
	}
	
	/**
	 * Clear the Temporary directory created for this Update
	 * @param array $settings
	 * @return Updater
	 */
	public function doClearTempraryFiles(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeClearTempraryFiles",$settings);
		$coreToken = array();
		
		$coreToken['filepath'] 	= ABSPATH."/templates_c/".$settings['token']."/";
		
		if($this->coreUpdater->deleteFiles($coreToken))
		{
			$settings['status'] = 1;
		}
		else
		{
			array_push($this->errorStack, "3003: Couldn't Clear up Temporary Directory");
			$settings['status'] = -1;
		}
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterClearTempraryFiles",$settings);
		return $this;
	}
	
	/**
	 * Returns the Current Version of this MCMS
	 * @param array $settings
	 * @return string
	 */
	public function getCurrentVersion(&$settings=array())
	{
		// VERSION_NUMBER - DB-Table Config 
		if(strlen($this->updaterSettings['version'])>0)
		{
			return $this->updaterSettings['version'];
		}
		elseif(isset($settings['version']))
		{
			$this->sqlDriver->db_Update(
					"updater", 
					" version = '".$settings['version']
			."' Where module = 'core' "
			);
		}
		$this->sqlDriver->db_Select("updater","version"," module = 'core' ");
		$tmpArr = execute_single($this->sqlDriver);

		return $tmpArr['version'];
	}
	
	/**
	 * Register Built update to DB repositories
	 * @param array $settings
	 * @return Updater
	 */
	public function doRegisterUpdate(&$settings)
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeRegisterUpdate",$settings);
		
		$settings['version'] 		= ($settings['version']? $settings['version'] : $this->getCurrentVersion($settings));
		$settings['versioncheck'] 	= Updater::getVersioncheck($settings['version']);
		$settings['changeLog'] 		= ($settings['changeLog']? $settings['changeLog'] : "");
		
		if(!isset($settings['tokens']))
		{
			$settings['tokens'] = array('fetchFiles','unzipFiles','createRestorePoint','copyFiles','updateSql','cleanFiles');
			if(isset($settings['hooks']))
			{
				$settings['hooks'] = (is_array($settings['hooks'])? $settings['hooks'] : explode(",", $settings['hooks']));
			}
			else
			{
				$settings['hooks'] = array();
			}
			$settings['tokens'] = array_merge($settings['tokens'],$settings['hooks']);
		}
		
		$this->sqlDriver->db_Insert("updates_repository (version,title,tokens,changelog,versioncheck,filepath,encoded_filepath,type) ", 
				" '".$settings['version']
				."','".($settings['title']? $settings['title'] : $this->getCurrentVersion())
				."','".json_encode($settings['tokens'])
				."','".$settings['changelog']
				."','".$settings['versioncheck']
				."','".str_replace(ABSPATH,'',$this->updaterSettings['archivePath'].($settings['title']? $settings['title'] : $this->getCurrentVersion()) . ".zip")
				."','".str_replace(ABSPATH,'',$this->updaterSettings['archivePath']."encoded/".($settings['title']? $settings['title'] : $this->getCurrentVersion()) . ".zip")
				."','".'0'."'"
		);
		
		$this->doTriggerHook("afterRegisterUpdate",$settings);
		return $this;
	}
	
	/**
	 * Build Manifest By Settings, and return it as text String
	 * @param array $settings
	 * @return string
	 */
	public function doBuildManifest(&$settings=array())
	{
		$manifest 				= array();
		$manifest['validate'] 	= array();
		$tmpArr 				= array();
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeBuildManifest",$settings);

		//---CORE----------------------
		$this->sqlDriver->db_Select("updater","*"," module = 'core'");
		$tmpArr = (($tmpArr = execute_single($this->sqlDriver))? $tmpArr : array());
		$manifest['validate']['core'] 						= array();
		$manifest['validate']['core']['version'] 			= $this->getCurrentVersion();
		$manifest['validate']['core']['dependencies'] 		= $tmpArr['dependencies'];
		//---DATABASE------------------
		$this->sqlDriver->db_Select("updater","*"," module = 'db'");
		$tmpArr = (($tmpArr = execute_single($this->sqlDriver))? $tmpArr : array());
		$manifest['validate']['db'] 						= array();
		$manifest['validate']['db']['version'] 				= $tmpArr['version'];
		$manifest['validate']['db']['dependencies'] 		= $tmpArr['dependencies'];
		//---CORE-MODULES--------------
		$this->sqlDriver->db_Select("updater","*" ," NOT (module = 'core') AND NOT (module = 'db') AND isPlugin = 0 ");
		$tmpArr = (($tmpArr = execute_multi($this->sqlDriver))? $tmpArr : array());
		$manifest['validate']['modules'] = array();
		foreach($tmpArr as $aArr)
		{
			$manifest['validate']['modules'][$aArr['module']] 					= array();
			$manifest['validate']['modules'][$aArr['module']]['version'] 		= $aArr['version'];
			$manifest['validate']['modules'][$aArr['module']]['dependencies'] 	= $aArr['dependencies'];
			$manifest['validate']['modules'][$aArr['module']]['path'] 			= $aArr['path'];
		}
		//---MCMS-PLUGINS-----------------------
		$this->sqlDriver->db_Select("updater","*" ," isPlugin = 1 ");
		$tmpArr = (($tmpArr = execute_multi($this->sqlDriver))? $tmpArr : array());
		$manifest['validate']['plugins'] = array();
		foreach($tmpArr as $aArr)
		{
			$manifest['validate']['plugins'][$aArr['module']] 					= array();
			$manifest['validate']['plugins'][$aArr['module']]['version'] 		= $aArr['version'];
			$manifest['validate']['plugins'][$aArr['module']]['dependencies'] 	= $aArr['dependencies'];
			$manifest['validate']['plugins'][$aArr['module']]['path'] 			= $aArr['path'];
		}

		$manifest['execute']['hooks']			= ($settings['hooks'] 			? $settings['hooks'] 			: array());
		$manifest['execute']['foldercopies']	= ($settings['foldercopies']	? $settings['foldercopies'] 	: array());
		$manifest['execute']['filecopies']		= ($settings['filecopies'] 		? $settings['filecopies']		: array());
		$manifest['execute']['deletions']		= ($settings['deletions'] 		? $settings['deletions'] 		: array());
		$manifest['execute']['sql']				= ($settings['sql'] 			? $settings['sql'] 				: array());

		$manifest['callback']['url']			= ($settings['callbackurl'] 	? $settings['callbackurl'] 		: "");
		$manifest['callback']['hooks']			= ($settings['callbackhooks'] 	? $settings['callbackhooks'] 	: array());

		$jsonManifest = json_encode($manifest);

		file_put_contents($this->updaterSettings['archivePath']."manifest.json", $jsonManifest, FILE_APPEND | LOCK_EX);

		$this->doTriggerHook("afterBuildManifest",$settings);
		return $jsonManifest;
	}
	
	/**
	 * Manipulate Updater's Settings By Reading 
	 * the UpdateManifest. Force Hibernation.
	 * @param array $settings
	 * @return Updater
	 */
	public function doUnHibernateByManifest(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeUnHibernateByManifest",$settings);
		
		$tmpSettings = array();
		if(file_exists($this->updaterSettings['archivePath'].'tmp/'."manifest.json"))
		{
			$this->doTraverseSettingsArray( // Call Function
				json_decode( // First deserilize JSON token
					file_get_contents( // Read JSON Manifest
						$this->updaterSettings['archivePath'].'tmp/'."manifest.json"
					), TRUE
				), $tmpSettings // Manipulate tmpSettings Token
			);
			$settings['version'] 		= (isset($tmpSettings['validate']['core']['version'])	? $tmpSettings['validate']['core']['version'] 	: $this->getCurrentVersion($settings));
			$settings['hooks'] 			= (isset($tmpSettings['execute']['hooks'])				? $tmpSettings['execute']['hooks'] 				: array());
			$settings['foldercopies'] 	= (isset($tmpSettings['execute']['foldercopies'])		? $tmpSettings['execute']['foldercopies'] 		: array());
			$settings['filecopies'] 	= (isset($tmpSettings['execute']['filecopies'])			? $tmpSettings['execute']['filecopies'] 		: array());
			$settings['deletions'] 		= (isset($tmpSettings['execute']['deletions'])			? $tmpSettings['execute']['deletions'] 			: array());
			$settings['sql'] 			= (isset($tmpSettings['execute']['sql'])				? $tmpSettings['execute']['sql'] 				: 0);
			$settings['callbackurl'] 	= (isset($tmpSettings['callback']['url'])				? $tmpSettings['callback']['url'] 				: '');
			$settings['callbackhooks'] 	= (isset($tmpSettings['callback']['hooks'])				? $tmpSettings['callback']['hooks'] 			: array());			
		}
		
		$tmpArr = array();
		foreach ($settings['foldercopies'] as $vArr) 
		{
			$tmpArr[] = $vArr['path'];
		}
		$settings['foldercopies'] = $tmpArr;
		$tmpArr = array();
		foreach ($settings['filecopies'] as $vArr)
		{
			$tmpArr[] = $vArr['path'];
		}
		$settings['filecopies'] = array_merge($tmpArr,$settings['foldercopies']);
		
		$settings['status'] = 1;
		if(count($tmpSettings)==0)
		{
			$settings['status'] = -1;
			@array_push($this->errorStack, "6007:Unable to Read Manifest");
		}
		$settings['error'] = &$this->errorStack;
		
		$this->doFuseSettings($settings);
		$this->doHibernate($settings);
		$this->doTriggerHook("afterUnHibernateByManifest",$settings);
		return $this; // Return updater with loded settings
	}
	
	/**
	 * Builds The SQl Dump File. If $settings['sqlTables'] is used then
	 * traceWorker is dumping the actual DB.
	 * $settings['sql'] is the custom used SQL that is concatinated
	 * with every other dump of traceWorker
	 * @param array $settings
	 * @return string
	 */
	function doBuildSQLFile(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeBuildSQLFile",$settings);

		$sqlFile = "";
		if(isset($settings['sql']))
		{
			$sqlFile .= $settings['sql'];
			$sqlFile .= "\r\n\r\n";
		}
		if(isset($settings['sqlTables']) && !is_array($settings['sqlTables']))
		{
			$settings['sqlTables'] = explode(",", $settings['sqlTables']);
		}
		if(isset($settings['sqlTables']) && count($settings['sqlTables'])>0)
		{
			$tmpArr = array();
			for($i=0; $i<count($settings['sqlTables']); $i++)
			{
				$tmpArr[$i] 				= array();
				$tmpArr[$i]['createtable']	= 0;
				$tmpArr[$i]['droptable']	= 0;
				$tmpArr[$i]['name']			= $settings['sqlTables'][$i];
			}
			$sqlFile .= $this->traceWorker->generateSQLDump($tmpArr);
		}

		file_put_contents($this->updaterSettings['archivePath']."dbdump.sql", $sqlFile, FILE_APPEND | LOCK_EX);

		$this->doTriggerHook("afterBuildSQLFile",$settings);
		return $sqlFile;
	}

	/**
	 * Builds The Changelog File.
	 * @param array $settings
	 * @return string
	 */
	function doBuildChangelogFile(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeBuildChangelogFile",$settings);

		$changelog = "";
		if(strlen($settings['changelog']))
		{
			$changelog .= $settings['changelog'];
		}

		file_put_contents($this->updaterSettings['archivePath']."changelog.txt", $changelog, FILE_APPEND | LOCK_EX);

		$this->doTriggerHook("afterBuildChangelogFile",$settings);
		return $sqlFile;
	}

	
	/**
	 * Check if directory extsts and returns 1
	 * otherwise it recrusivly creates the direcoty and returns 0.
	 * -1 is return on failure
	 * @param string $dir
	 * @param array $settings
	 * @return number
	 */
	public function doBuildUpdaterFolder($dir,&$settings=array())
	{
		$this->doFuseSettings($settings);
		if(@file_exists($dir))
		{
			return 1;
		}
		if(@mkdir($dir,0775,true))
		{
			return 0;
		}
		return -1;
	}

	/**
	 * Build Dependencies of the current Core MCMS, and return it as array of Dependencies
	 * @param array $settings
	 * @return array
	 */
	public function doBuildCoreDependencies(&$settings=array())
	{
		$this->doTriggerHook("beforeBuildCoreDependencie",$settings);
		// TODO
		$this->doTriggerHook("afterBuildCoreDependencie",$settings);
		return array();
	}

	/**
	 * Build Dependencies of the current DB MCMS, and return it as array of Dependencies
	 * @param array $settings
	 * @return array
	 */
	public function doBuildDBDependencies(&$settings=array())
	{
		$this->doTriggerHook("beforeBuildDBDependencies",$settings);
		// TODO
		$this->doTriggerHook("afterBuildDBDependencies",$settings);
		return array();
	}

	/**
	 * Build Dependencies of the current Modules MCMS, and return it as array of Dependencies
	 * @param array $settings
	 * @return array
	 */
	public function doBuildModuleDependencies(&$settings=array())
	{
		$this->doTriggerHook("beforeBuildModuleDependencies",$settings);
		// TODO
		$this->doTriggerHook("afterBuildModuleDependencies",$settings);
		return array();
	}

	/**
	 * Build Dependencies of the given Plugin MCMS, and return it as array of Dependencies
	 * @param array $settings
	 * @return array
	 */
	public function doBuildPluginDependencies(&$settings=array())
	{
		$this->doTriggerHook("beforeBuildPluginDependencies",$settings);
		// TODO
		$this->doTriggerHook("afterBuildPluginDependencies",$settings);
		return array();
	}

	/**
	 * Check Dependencies & version completion with the Comperable Core arrayToken.
	 * @param array $comperable
	 * @return boolean
	 */
	public function doValidateCore(&$comperable)
	{
		// TODO - Compare & Validate
		$this->doTriggerHook("beforeValidateCore",$settings);
		$comperable['name'];
		$comperable['version'];
		$comperable['dependencies'];
		$comperable['path'];
		$comperable['isPlugin'];
		$comperable['lastupdate'];
		$this->doTriggerHook("afterValidateCore",$settings);
		return true;
	}

	/**
	 * Check Dependencies & version completion with the
	 * Comperable MCMS Database arrayToken.
	 * @param array $comperable
	 * @return boolean
	 */
	public function doValidateDatabase(&$comperable)
	{
		// TODO - Compare & Validate
		$this->doTriggerHook("beforeValidateDatabase",$settings);
		$comperable['name'];
		$comperable['version'];
		$comperable['dependencies'];
		$comperable['path'];
		$comperable['isPlugin'];
		$comperable['lastupdate'];
		$this->doTriggerHook("afterValidateDatabase",$settings);
		return true;
	}

	/**
	 * Check Dependencies & version completion with the Comperable Module arrayToken.
	 * @param array $comperable
	 * @return boolean
	 */
	public function doValidateModules(&$comperable)
	{
		// TODO - Compare & Validate
		$this->doTriggerHook("beforeValidateModules",$settings);
		$comperable['name'];
		$comperable['version'];
		$comperable['dependencies'];
		$comperable['path'];
		$comperable['isPlugin'];
		$comperable['lastupdate'];
		$this->doTriggerHook("afterValidateModules",$settings);
		return true;
	}

	/**
	 * Check Dependencies & version completion with the Comperable Plugins arrayToken.
	 * @param array $comperable
	 * @return boolean
	 */
	public function doValidatePlugins(&$comperable)
	{
		// TODO - Compare & Validate
		$this->doTriggerHook("beforeValidatePlugins",$settings);
		$comperable['name'];
		$comperable['version'];
		$comperable['dependencies'];
		$comperable['path'];
		$comperable['isPlugin'];
		$comperable['lastupdate'];
		$this->doTriggerHook("afterValidatePlugins",$settings);
		return true;
	}

	/**
	 * Makes a Mysql Dump. So it may Secure the validity of
	 * the update transaction of the Database.
	 * @param array $settings
	 * @return boolean
	 */
	public function doMakeSQLDump(&$settings=array())
	{
		$this->doTriggerHook("beforeMakeSQLDump",$settings);
		try 
		{
			$this->sqlDriver->db_Select_gen('SHOW TABLES');
			$arr = execute_multi($this->sqlDriver);
			$tmpTablesArr = array();
			
			$tmpKey = array_keys($arr[0]);
			$tmpKey = $tmpKey[0];
			
			$i=0;
			foreach($arr as $vArr)
			{
				$tmpTablesArr[$i]['name'] 			= $vArr[$tmpKey];
				$tmpTablesArr[$i]['droptable'] 		= true;
				$tmpTablesArr[$i]['createtable'] 	= true;
				$i++;
			}
			
			$tmpArr['tables']	= $tmpTablesArr;
			
			file_put_contents(
				$this->updaterSettings['archivePath']."backupdb.sql", 
				$this->coreUpdater->generateSQLDump($tmpArr)
			);
		}
		catch (Exception $e)
		{
			
		}
		$this->doTriggerHook("afterMakeSQLDump",$settings);
		return true;
	}
	
	/**
	 * Moves the Updater to the next stage by indexing
	 * the already given UpdatingToken.
	 * @param array $settings
	 * @return boolean
	 */
	public function doMoveToNextStage(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeMoveToNextStage",$settings);
		$statesMap = array();
		if($this->isTracing)
		{
			$this->traceWorker->setTraceWorkerStates($statesMap);
		}
		elseif($this->isUpdating)
		{
			$this->coreUpdater->setUpdatingStates($statesMap);
		}
		
		if($statesMap[$settings['lastAct']])
		{
			$settings['nextAct'] = $statesMap[$statesMap[$settings['lastAct']]+1];
		}
		
		$this->doFuseSettings($settings);
		$this->doTriggerHook("afterMoveToNextStage",$settings);
		return true;
	}

	/**
	 * A TraceUpdating state indicator
	 * @return number|string
	 */
	public function getTracingState()
	{
		if(isset($this->tracingStates[$this->currentTracingState]))
		{
			return $this->tracingStates[$this->currentTracingState];
		}
		return 0;
	}

	/**
	 * An Updating state indicator
	 * @param array $settings
	 * @return number|string
	 */
	public function getUpdatingState(&$settings=array())
	{
		if(isset($this->updatingStates[$this->currentUpdatingState]))
		{
			return $this->updatingStates[$this->currentUpdatingState];
		}
		return 0;
	}

	/**
	 * A Booleas switch to check the Updating procedure state
	 * @param array $settings
	 * @return boolean
	 */
	public function isUpdating(&$settings=array())
	{
		return $this->isUpdating;
	}

	/**
	 * A Booleas switch to check the traceUpdating procedure state
	 * @param array $settings
	 * @return boolean
	 */
	public function isTracing(&$settings=array())
	{
		return $this->isTracing;
	}

	/**
	 * A token that is used to indicate Updating Stages.
	 * @param array $settings
	 * @return string
	 */
	public function getUpdatingToken(&$settings=array())
	{
		$this->doFuseSettings($settings);
		if($this->isUpdating)
		{
			return $this->tokenHashPtr;
		}
		return "";
	}

	/**
	 * A token that is used to indicate Tracing Stages.
	 * @param array $settings
	 * @return string
	 */
	public function getTracingToken(&$settings=array())
	{
		$this->doFuseSettings($settings);
		if($this->isTracing)
		{
			return $this->tokenHashPtr;
		}
		return "";
	}

	/**
	 * Generate New Token.
	 * MD5( Current version + TimeStamp )
	 * @param array $settings
	 * @return string
	 */
	public function getNewGeneratedToken(&$settings=array())
	{
		$this->doFuseSettings($settings);
		$this->doTriggerHook("beforeGenerateToken",$settings);
		$token = '';
		if($this->isTracing)
		{
			$token = $this->getCurrentVersion($settings);
		}
		else
		{
			$token = md5(''.$settings['version'].microtime().'');
		}
		$this->doTriggerHook("afterGenerateToken",$settings);
		return $token;
	}

	/**
	 * Sets the Current Updater token, So Updater may indicate its state.
	 * true is returned as the token is found. Otherwise a new creation should be demanded.
	 * @param string $token
	 * @param array $settings
	 * @return boolean
	 */
	public function setUpdaterToken($token,&$settings=array())
	{
		if(strlen($this->tokenHashPtr)>0 && $this->tokenHashPtr == $token)
		{
			return true;
		}

		$this->tokenHashPtr = $this->getNewGeneratedToken();

		return false;
	}

	/**
	 * Log the Current Activity
	 * @param unknown_type $settings
	 * @return boolean
	 */
	public function doLogState(&$settings=array())
	{
		array_push($this->logStack, $settings['nextAct']);
		return true;
	}

	/**
	 * Fetched the Last Log activities
	 * @param array $settings
	 * @return multitype:
	 */
	public function getLastLog(&$settings=array())
	{
		return array_pop($this->logStack);
	}

	/**
	 * Get Error State
	 * @param array $settings
	 * @return multitype:
	 */
	public function getErrorState(&$settings=array())
	{
		return $this->errorStack;
	}

	/**
	 * Pops the last error
	 * @param array $settings
	 * @return multitype:
	 */
	public function getLastError()
	{
		return array_pop($this->errorStack);
	}

	/**
	 * Use the HookSystem of MCMS as it
	 * trigers Events of Updater
	 * @param string $hookKey
	 * @param array $args
	 * @return Updater
	 */
	public function doTriggerHook($hookKey,&$args=array())
	{
		try
		{
			HookParent::getInstance()->doTriggerHook("updater", $hookKey,$args);
		}
		catch (Exception $e)
		{
				
		}
		return $this;
	}
	
	/**
	 * Fuse settings & return the requested value
	 * @param String $key
	 * @param array $settngs
	 * @return unknown
	 */
	public function getSettings($key,&$settings=array())
	{
		$this->doFuseSettings($settings);
		return ($this->updaterSettings[$key]?$this->updaterSettings[$key]: $settings[$key]);
	}
	
	/**
	 * Calculate the VersionCheck ID of the given version String
	 * @param string $version
	 * @return int
	 */
	public static function getVersioncheck($version)
	{
		$return = 0;
		$tmpArr = explode(".", $version);
		for($i=(count($tmpArr)-1),$j=0; $i>=0; $j++,$i--)
		{
			$return += $tmpArr[$j]*pow(100, $i);
		}
		return $return;
	}
	
	/**
	 * Gets current MCMS Version
	 * @return string
	 */
	public static function getMCMSVersion()
	{
		global $sql;
		// VERSION_NUMBER
		$sql->db_Select("updater","version"," module = 'core' ");
		$tmpArr = $sql->db_Fetch();
		return $tmpArr['version'];
	}
	
	/**
	 * Generate New String as an update Pointer
	 * by its Version.
	 * @return string
	 */
	public static function generateTokenHashStr()
	{
		return md5(''.self::getMCMSVersion().microtime().'');;
	}
	
	/**
	 * Persorm a version comperation as it results
	 * the versions that the client lucks
	 * @param unknown_type $settings
	 */
	public static function doVersionCheck(&$settings)
	{
		global $sql;
		
		$sql->db_Select("updates_repository","version,title,tokens,changelog as changeLog", 
			"type = 0 AND "
			." versioncheck > ".Updater::getVersioncheck($settings['version']));
		$tmpArr = execute_multi($sql);
		
		foreach($tmpArr as &$vArr)
		{
			$vArr['tokens'] = @json_decode($vArr['tokens'],true);
		}
		
		$settings['versionresults'] = array(
				'total' 	=> ($tmpArr? count($tmpArr) : 0),
				'versions' 	=> ($tmpArr? $tmpArr : array()),
				'rollBack' 	=> array(
						'tokens'=>array(
								'rollbackfiles', 'rollbacksql'
						)
				)
		);
		return $settings;
	}

}

?>
