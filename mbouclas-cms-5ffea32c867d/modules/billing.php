<?php
class billing
{
    public $users;

    function billing()
    {
        $this->tables['billing'] = array('table' => 'billing', 'key' => 'id');
        $this->tables['billingTransactions'] = array('table' => 'billingTransactions','key' => 'id');
        
        $this->users = new user();
    }

    function getUserCredits($uid)
    {
        global $sql;
        $sql->db_Select("billing", "credits", "uid='$uid' LIMIT 1");
        $rows = $sql->db_Rows();
        if ($rows == 1) {
            $res = execute_single($sql);
            return $res['credits'];
        } else {
            return 0;
        }
    }
    
    function getTransMaxAmount()
    {
        global $sql;
        $sql->db_Select("billingTransactions","MAX(creditsChange) as max");
        $res = execute_single($sql);
        return $res["max"];
    }
    
    function getUserBillingID($uid)
    {
        global $sql;
        $sql->db_Select("billing", "id", "uid='$uid' LIMIT 1");
        $rows = $sql->db_Rows();
        if ($rows == 1) {
            $res = execute_single($sql);
            return $res['id'];
        } else {
            return 0;
        }
    }

    function addUserCredits($uid, $credits)
    {
        global $sql;
        $now = time();

        $sql->db_Select("billing", "id,credits", "uid=$uid");
        $prevCr = $sql->db_Fetch();

        if ($sql->db_Rows() == 1) {
            echo "OLD USER";
            $sql->db_Update("billing", "credits=credits+$credits, lastTransaction=$now WHERE uid=$uid");
            $this->newTransaction($prevCr['credits'], $prevCr['id'], $credits);
            
            return true;
        } else {
            //echo "NEW USER";
            $sql->db_Insert("billing (uid,credits,lastTransaction)", "$uid,$credits," . time());
            $this->newTransaction(0, $sql->last_insert_id, $credits);
            
            //echo "</br>INSERT INTO "."billing (uid,credits,lastTransaction) VALUES"." ($uid,$credits," . time().")";
            return false;
        }
    }

    function newTransaction($prevCr, $billingID, $credits)
    {
        global $sql;
        echo "</br>adding transaction with </br>"."'$billingID','$credits','$prevCr','";
        $sql->db_Insert("billingTransactions (billingID,creditsChange,prevCredits,transDate)", "'$billingID','$credits','$prevCr','" .time() . "'");
    }

    function searchTransactions($settings)
    {
        
        //print_r($settings);
        global $sql;
        //$users->tables["users"]["table"]
        $fields = (!$settings['fields']) ? "*" : $settings['fields'];
        $orderby = ($settings['orderby']) ? " ORDER BY " . $settings['orderby'] : "";
        $way = ($settings['way']) ? "  " . $settings['way'] : "";
        $limit = ($settings['results_per_page']) ? "LIMIT " . $settings['results_per_page'] : "";
        $settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
        $tables['billing'] = " INNER JOIN billing ON (".$this->tables['billing']['table'].".id=".$this->tables['billingTransactions']['table'].".billingID)";
        $tables['users'] = " INNER JOIN users ON (users.id=".$this->tables['billing']['table'].".uid)";
        
        if (is_array($settings['searchFields'])) {
            foreach ($settings['searchFields'] as $k => $v) {
                $q[] = "$k = '$v'";
            }
        } //END SETUP SEARCH FIELDS

        
        

        if (is_array($settings['searchFieldsLike'])) {
            foreach ($settings['searchFieldsLike'] as $k => $v) {
                if ($k == 'user_name' OR $k == 'user_surname') {
        			$q[] = "$k LIKE '%$v%'";
        		}
        		else {
       			$q[] = "$k LIKE '%$v%'";
        		}
            }
        } //END SETUP SEARCH FIELDS WITH LIKE

        if (is_array($settings['greaterThan'])) {
            foreach ($settings['greaterThan'] as $k => $v) {
                $q[] = $k . " > " . $v;
            }
        } //END SETUP SEARCH FIELDS WITH LIKE

        if (is_array($settings['lessThan'])) {
            foreach ($settings['lessThan'] as $k => $v) {
                $q[] = $k . " < " . $v;
            }
        }

        if (is_array($settings['missingFields'])) {
            foreach ($settings['missingFields'] as $k => $v) {
                $q[] = "$k != '$v'";
            }
        } //END SETUP SEARCH FIELDS
        
        if (is_array($settings['searchFieldBETWEEN'])) {
    	foreach ($settings['searchFieldBETWEEN'] as $k => $v) {
   			$q[] = "(".$this->tables['billingTransactions']['table'].".$k BETWEEN $v)";
    	}
    }//END SETUP SEARCH FIELDS WITH BETWEEN

        if ($q) {
            $query = implode(" AND ", $q);
        }


        if (!$query) {
            $query_mode = 'no_where';
        } else {
            $query_mode = 'default';
        }

        if($tables) $this->tables['billingTransactions']['table'].=implode(" ",$tables);
        
        
        if ($settings['return'] == 'multi') //Return all results, NO PAGINATION
        {
            $sql->db_Select($this->tables['billingTransactions']['table'], $fields, "$query $orderby $way $limit",
                $query_mode);
            if ($settings['debug']) {
                echo "SELECT " . $fields . " FROM " . $this->tables['billingTransactions']['table'] .
                    " WHERE $query $orderby $way $limit<br>";
            }
            if ($sql->db_Rows()) {
                $res = execute_multi($sql);
            }
        } //END ALL RESULTS
        elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
        {
            $sql->db_Select($this->tables['billingTransactions']['table'], $fields, "$query $orderby $way",
                $query_mode); //GET THE TOTAL COUNT

            if ($settings['debug']) {
                echo "SELECT ".$fields." FROM " . $this->tables['billingTransactions']['table'] . " WHERE $query $orderby $way<br>";
            }
            if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
                {
                $total = $sql->db_Rows();
                $current_page = ($settings['page']) ? $settings['page'] : 1;
                $results_per_page = $settings['results_per_page'];
                if (isset($settings['start'])) {
                    $start = $settings['start'];
                } else {
                    $start = ($current_page) ? ($current_page * $results_per_page) - $results_per_page :
                        0;
                }
                $limit = "LIMIT $start," . $results_per_page;
                $sql->db_Select($this->tables['billingTransactions']['table'], $fields, "$query $orderby $way $limit",
                    $query_mode);
                    
                    
                if ($settings['debug']) {
                    echo "<br>SELECT $fields FROM " . $this->tables['billingTransactions']['table'] . " WHERE $query $orderby $way $limit";
                }
                $res = execute_multi($sql, 1);
                
                paginate_results($current_page, $results_per_page, $total);

            } //END FOUND RESULTS
        } //END PAGINATION
        elseif ($settings['return'] == 'single') // RETURN PAGINATED RESULTS
        {
            $sql->db_Select($this->tables['billingTransactions']['table'], $fields, "$query $orderby $way $limit",
                $query_mode);
            if ($settings['debug']) {
                echo "SELECT " . $fields . " FROM " . $this->tables['billingTransactions']['table'] .
                    " WHERE $query $orderby $way $limit<br>";
            }
            if ($sql->db_Rows()) {
                $res = execute_single($sql);
            }


            if ($settings['getUserDetails']) {
                $res['userDetails'] = $this->users->userDetails($res['uid'], array('fields' =>
                    'id,uname'));
            } //END USER DETAILS

        } elseif ($settings['return'] == 'count') // RETURN COUNT RESULTS
        {
            $sql->db_Select($this->tables['billingTransactions']['table'], "count(" . $this->tables['billingTransactions']['table'] .
                ".id) as total", "$query $orderby $way $limit", $query_mode);
            if ($settings['debug']) {
                echo "SELECT count(" . $this->tables['billingTransactions']['table'] . ".id) as total FROM " .
                    $this->tables['billingTransactions']['table'] . " WHERE $query $orderby $way $limit<br>";
            }
            $res = execute_single($sql);
            return $res['total'];
        }
        
        
      
        
         
        return $res;
        
    }
    
    function getUserTransactions($uid,$settings=null)
    {
        if($settings==null)
        {
            $settings = array("fields"=>"billing.credits,billingTransactions.id,billingTransactions.transDate,users.uname,billingTransactions.creditsChange,billingTransactions.prevCredits",
            "debug"=>1,
            'searchFields'=>array('users.id'=>$uid),
            'orderby'=>$this->tables['billingTransactions']['table'].'.transDate',
            'way'=>'desc',
            'return'=>'multi'
            );
        }
        else
        {
            $settings['searchFieldsLIKE']['users.unane'] = $uid;
        }
         
        return $this->searchTransactions($settings);
    }
}

?>