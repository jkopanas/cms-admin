<?php
include("../../../manage/init.php");//load from manage!!!!

$billing = new billing();


$smarty->assign("data",$posted_data);
$smarty->assign("maxPrice",ceil($billing->getTransMaxAmount()));

$data = $billing->searchTransactions(array("fields"=>"billing.credits,billingTransactions.id,billingTransactions.transDate,users.uname,billingTransactions.creditsChange,billingTransactions.prevCredits",
"debug"=>0,
'orderby'=>'billingTransactions.id',
'way'=>'desc',
'return'=>'paginated',
'results_per_page'=>10,'page'=>1,
'missingFields'=>$missing_data));

$smarty->assign("items",$data);

$smarty->assign("menu",$users_module['name']);
$smarty->assign("submenu","main");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/billing/admin/main.tpl");
$smarty->display("admin/home.tpl");

?>