<?php
function chmod_R($path, $filemode) { 
   if (!is_dir($path))
      return chmod($path, $filemode);
   $dh = opendir($path);
   while ($file = readdir($dh)) {
       if($file != '.' && $file != '..') {
           $fullpath = $path.'/'.$file;
           if(!is_dir($fullpath)) {
             if (!chmod($fullpath, $filemode))
                return FALSE;
           } else {
             if (!chmod_R($fullpath, $filemode))
                return FALSE;
           }
       }
   }

   closedir($dh);
   
   if(chmod($path, $filemode))
     return TRUE;
   else 
     return FALSE;
} 
chown($_SERVER['DOCUMENT_ROOT']."/images/gallery/products/category_260", $user_name);
chmod_R($_SERVER['DOCUMENT_ROOT']."/images/gallery",0777);

?>