<?php
class HandleUploadedItem
{
	var $base_dir = "";
	var $file = "";
	var $settings=0;
	var $module=0;
	var $return = "";
	var $type = "";
	var $tables = "";
	
	//INITIALIZE CLASS
	function HandleUploadedItem($settings) {
      $this->settings = $settings;
      $this->base_dir = $settings['upload_dir'];
      $this->file = $settings['filename'];
      $this->module = $settings['module'];
	  if ($settings['module']['name'] == 'content') {
	  	$this->tables['images'] = 'item_images';
	  	$this->tables['images_aditional'] = 'item_images_aditional';
	  	$this->type = 'content';
	  }
	  elseif ($settings['module']['name'] == 'products') {
	  	$this->tables['images'] = 'item_images';
	  	$this->tables['images_aditional'] = 'item_images_aditional';
	  	$this->type = 'products';
	  }	
	  elseif ($settings['module']['name'] == 'listings') {
	  	$this->tables['images'] = 'item_images';
	  	$this->tables['images_aditional'] = 'item_images_aditional';
	  	$this->type = 'listings';
	  }	
	  elseif ($settings['module']['name'] == 'quiz') {
	  	$this->tables['images'] = 'item_images';
	  	$this->tables['images_aditional'] = 'item_images_aditional';
	  	$this->type = 'quiz';
	  }	
	  elseif ($settings['module']['name'] == 'recipes') {
	  	$this->tables['images'] = 'item_images';
	  	$this->tables['images_aditional'] = 'item_images_aditional';
	  	$this->type = 'recipes';
	  }	    
      if ($settings['file_type'] == 'Thumb') {
      	if ($settings['ExistingThumb']) {//A THUMB EXISTS - DELETE OLD ONE FIRST
      		//$this->RemoveFromDb(array('itemid'=>$settings['ExistingThumb']));
            $this->DeleteOldThumb($settings['itemid']);
      	}
      	if ($this->module['settings']['keep_original_image']) {
			if (!is_dir($settings['upload_dir']."/originals")) {
				mkdir($settings['upload_dir']."/originals",0755);
			}
				copy($settings['upload_dir'].$settings['filename'],$settings['upload_dir']."/originals/".$settings['filename']);
		}
   		$this->CreateImageSet();
      	$this->InsertImagesToDb();//ADD IT TO DB
		$this->CleanUp();
      	
      }//END THUMB
      if ($settings['file_type'] == 'ReplaceCopy') {
   		$this->ReplaceImageCopy();
		$this->CleanUp();
      	
      }//END THUMB
      if ($settings['file_type'] == 'images') {
      	if ($this->module['settings']['keep_original_image']) {
			if (!is_dir($settings['upload_dir']."/originals")) {
				mkdir($settings['upload_dir']."/originals",0755);
			}
				copy($settings['upload_dir'].$settings['filename'],$settings['upload_dir']."/originals/".$settings['filename']);
		}
      	$this->CreateImageSet();
      	$this->InsertImagesToDb();//ADD IT TO DB
      	$this->CleanUp();
      }
      if ($settings['file_type'] == 'SimpleUpload') {

        if ($settings['media'] == 'image') {
            $this->CreateImageSet();
            $this->CleanUp();
        }//END IMAGE
        elseif ($settings['media'] == 'document')
        {
           $this->CheckDocument($settings['upload_dir'].$settings['filename']);
        }//END DOCUMENT
        
        
      }//END SIMPLE UPLOAD
      if ($settings['file_type'] == 'SaveMedia') {

        if ($settings['media'] == 'document') {
            $this->CheckDocument($settings['upload_dir'].$settings['filename']);
            $this->SaveDocument();
            
        }//END IMAGE
        elseif ($settings['media'] == 'video')
        {
           $this->CheckDocument($settings['upload_dir'].$settings['filename']);
           $this->SaveVideo();
        }//END DOCUMENT
        elseif ($settings['media'] == 'audio')
        {
            
        }
        
      }//END SIMPLE UPLOAD   
     
   }//END FUNCTION
   
   function CleanUp()
   {
   		unlink($this->settings['upload_dir'].$this->settings['filename']);
   }//END FUNCTION

   function CreateDirs($dir)
   {
			if ($dir AND !is_dir($this->base_dir."/".$dir)) {
				mkdir($this->base_dir."/".$dir,0755);
		}
   }//END FUNCTION

   function CheckDocument($file) {
        $this->return['mime'] = mime_content_type($file);
        $this->return['filesizePlain'] = filesize($file);
        $this->return['filesize'] = FileSizeFormat($this->return['filesizePlain']);
        $this->return['name'] = $this->settings['filename'];
        $this->return['CleanDir'] = str_replace($_SERVER['DOCUMENT_ROOT'],'',$this->settings['upload_dir']);
        
   }//END FUNCTION
   

    
   function ReturnDetails()
   {
   		return $this->return;
   }//END FUNCTION 
   
   function SaveDocument() {
    global $sql;
    $type = explode('/',$this->return['mime']);
    $q = array("''",$this->settings['itemid'],"'".$this->module['name']."'","'".$this->settings['filename']."'","'".$this->return['CleanDir']."'","'".$this->return['CleanDir']."/".$this->settings['filename']."'","'".$this->settings['filename']."'","''",0,0,"'".$type[1]."'","'".$this->return['mime']."'",$this->return['filesizePlain'],time(),"''");
    $query = implode(',',$q);
    //error_message($query);
    $sql->db_Insert('item_documents',$query);
    
   }//END FUNCTION
   
   function SaveVideo()
   {
	   	global $sql;
	   	if ($this->settings['filename']) {
	    $type = explode('/',$this->return['mime']);
	    $extension = "ffmpeg";
		$extension_soname = $extension . "." . PHP_SHLIB_SUFFIX;
		$extension_fullname = PHP_EXTENSION_DIR . "/" . $extension_soname;	   		
	   	}//END UPLOAD BY FILE

		
	    if (extension_loaded($extension) AND file_exists($this->settings['upload_dir'].$this->settings['filename'])) 
		{
			
		$mov = new ffmpeg_movie($this->settings['upload_dir'].$this->settings['filename']);
		$info['width'] = $mov->getFrameWidth();
		$info['height'] = $mov->getFrameHeight();
		$info['duration'] = $mov->getDuration();
		$info['framerate'] = $mov->getFrameRate();
		$info['filename'] = $mov->getFileName();
		$info['video_codec'] = $mov->getVideoCodec();
		$info['audio_codec'] = $mov->getAudioCodec();
		$info['audio_chanels'] = $mov->getAudioChannels();
		$info['bit_rate'] = $mov->getBitRate();
		
		//Screenshot
		$cap = $mov->getFrame(rand(1,$mov->getFrameCount()));
		$img = $cap->toGDImage(); 
		$thumb = $this->return['CleanDir'].$this->settings['filename'].".jpg";
		imagejpeg($img, $_SERVER['DOCUMENT_ROOT'].$thumb);
//		error_message($thumb);
	}
		if ($this->settings['data']['duration']) {//FROM EMBED
			$info['duration'] = $this->settings['data']['duration'];
		}
		if ($this->settings['data']['image']) {//FROM EMBED
			$this_media = MediaFiles(array('type'=>$this->settings['media'],'single'=>1));
			$thumb = $this_media['folder']."/".$this->module['name']."_".$this->settings['itemid']."/".time().".jpg";
			
			if (!is_dir($this_media['folder']."/".$this->module['name']."_".$this->settings['itemid'])) {
				mkdir($_SERVER['DOCUMENT_ROOT'].$this_media['folder']."/".$this->module['name']."_".$this->settings['itemid'],0755);
			}
			$this->SaveRemoteThumb($thumb,$this->settings['data']['image']);
		}
		$details = form_settings_string($info);
		$title = ($this->settings['data']['title']) ? $this->settings['data']['title'] : $this->settings['filename'];
		$embed = ($this->settings['data']['embed']) ? $this->settings['data']['embed'] : '';
		$file_url = (!$this->settings['data']['embed']) ? $this->return['CleanDir'].$this->settings['filename'] : '';
	    $q = array("''",$this->settings['itemid'],"'".$this->module['name']."'","'".$this->settings['filename']."'","'".$this->return['CleanDir']."'","'".$file_url."'","'$embed'","'$thumb'","'$title'","'$details'",0,0,"'".$type[1]."'","'".$this->return['mime']."'","'".$this->return['filesizePlain']."'",time(),"''");
	    $query = implode(',',$q);
//	    echo $query;
	    //error_message($query);
	    $sql->db_Insert('item_videos',$query);
	    return $sql->last_insert_id;
   }//END FUNCTION
   
   function SaveRemoteThumb($fullpath,$remote_file)
   {
   		$ch = curl_init();
		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $remote_file);
		    curl_setopt($ch, CURLOPT_HEADER, 0);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		
		// grab URL and pass it to the browser
		$rawdata=curl_exec($ch);
		// close cURL resource, and free up system resources
		curl_close($ch);
		if(file_exists($_SERVER['DOCUMENT_ROOT'].$fullpath)){
		        unlink($_SERVER['DOCUMENT_ROOT'].$fullpath);
	    }
	    $fp = fopen($_SERVER['DOCUMENT_ROOT'].$fullpath,'x');
	    fwrite($fp, $rawdata);
	    fclose($fp);
   }//END FUNCTION
   
   function CreateImageSet()
   {
   	global $sql;
   	    if ($this->settings['CustomImageSet'] AND is_array($this->settings['ImageSet'])) {//CUSTOM SET. GET THE IMAGE TYPES DATA
   			$ImageTypes = get_image_types($this->module['id'],array('ids'=>implode(",",$this->settings['ImageSet']),'debug'=>0));	
   		}
   		elseif ($this->settings['CustomImageSet'] AND !is_array($this->settings['ImageSet'])) {//FAILSAFE
   			$ImageTypes = get_image_types($this->module['id'],array('required'=>1));
   		}
   		elseif (!$this->settings['CustomImageSet'] AND !is_array($this->settings['ImageSet'])) {//DEFAULT ACTION, CREATE ALL COPIES
   			$ImageTypes = get_image_types($this->module['id']);	
   		}
   		elseif (!$this->settings['CustomImageSet'] AND is_array($this->settings['ImageSet'])) {//PROBABLY CAME FROM A READY MADE ARRAY
   			$ImageTypes = $this->settings['ImageSet'];	
   		}
//error_message($this->module['id'],$_SERVER['DOCUMENT_ROOT']."/media_files/images/quiz_1/log.txt");
//error_message($ImageTypes,$_SERVER['DOCUMENT_ROOT']."/media_files/images/quiz_1/log.txt");
   		foreach ($ImageTypes as $v)
   		{
   			$this->CreateDirs($v['dir']);//CREATE DIRECTORY IF NOT THERE
   			if ($v['settings']['resize']) {
   				$Image = $this->CreateImge($v);//Create Resampled image
   				if ($Image) {
   					$this->return[$v['title']] = $Image;
   				}
   				
   			}//END IF REIZE
   		}
   		
   }//END FUNCTION
   
   function ReplaceImageCopy()
   {
   	global $sql;

   	//CHECK OUT FOR EXISTING COPY IN THE DB
   	$ImageType = get_image_types($this->module['id'],array('title'=>$this->settings['ImageCopy']));
   	$Image = $this->CreateImge($ImageType[$this->settings['ImageCopy']]);
   	if ($Image) {
   		$this->return[$this->settings['ImageCopy']] = $Image;
   		$this->return[$this->settings['ImageCopy']]['imageid'] = $this->settings['ImageID'];
   	}
   	else { $this->return[$this->settings['ImageCopy']] = 'error'; return false; }
	$image = $Image['name'];
	$image_path = $Image['CleanDir'];
	
	$image_details = $Image['size'];	
	$image_x = $image_details[0];
	$image_y = $image_details[1];
	$date_added = time();
	$image_path = str_replace("//","/",$image_path);
	$image_url = $Image['CleanDir']."/".$Image['name'];//WINDOWS ONLY
	$image_path = $Image['CleanDir'];//WINDOWS ONLY

   	//DELETE OLD FILE
   	$sql->db_Select($this->tables['images_aditional'],'image_url',"itemid = ".$this->settings['itemid']." AND imageid = ".$this->settings['ImageID']." AND image_type = '".$this->settings['ImageCopy']."'");
   	if ($sql->db_Rows()) {
   		$old_file = execute_single($sql);
   		unlink($_SERVER['DOCUMENT_ROOT'].$old_file['image_url']);
   			$query = "image = ',".$Image['name']."',image_path='$image_path',image_url ='$image_url',date_added = '$date_added',image_x ='$image_x',image_y ='$image_y' WHERE itemid = ".$this->settings[		'itemid']." AND imageid = ".$this->settings['ImageID']." AND image_type = '".$this->settings['ImageCopy']."'";
   		$sql->db_Update($this->tables['images_aditional'],$query);
   	}
   	else {//NEW ENTRY
   		$original_file = $this->settings['filename'];
//   		echo $this->settings['ImageID'].",".$this->settings['itemid'].",'$original_file','$image_path','$image_url','$date_added','$image_x','$image_y','".$this->settings['ImageCopy']."'";
   		$sql->db_Insert($this->tables['images_aditional'],$this->settings['ImageID'].",".$this->settings['itemid'].",'$original_file','$image_path','$image_url','$date_added','$image_x','$image_y','".$this->settings['ImageCopy']."'");
   	}
   }//END FUNCTION

   function InsertImagesToDb()
   {
   	global $sql;
   		$id = $this->settings['itemid'];
   		$original_file = $this->settings['filename'];
   		$type = $this->settings['type'];
   		$sql->db_Insert($this->tables['images'],"'','".$this->module['name']."','$id','$original_file','$alt','$title','$details','$available','$i','$type'");
//   		echo "INSERT INTO ".$this->tables['images']."'','$id','$original_file','$alt','$title','$details','$available','$i','$type'";
		$n_id = $sql->last_insert_id;
		
		foreach ($this->ReturnDetails() as $k => $v)
		{
			//Insert to aditional tables
			$image = $v['name'];
			$image_path = $v['CleanDir'];
			
//				fwrite($f,"BEFORE : ".$image_path."\n\r");
			$image_details = $v['size'];	
			$image_x = $image_details[0];
			$image_y = $image_details[1];
			$date_added = time();
			$image_path = str_replace("//","/",$image_path);
			$image_url = $v['CleanDir']."/".$v['name'];//WINDOWS ONLY
			$image_path = $v['CleanDir'];//WINDOWS ONLY
				
//				fwrite($f,"AFTER : ".$image_path."\n\r");
			$sql->db_Insert($this->tables['images_aditional'],"'$n_id',$id,'$image','$image_path','$image_url','$date_added','$image_x','$image_y','$k'");
//			echo "'$n_id','$image','$image_path','$image_x','$image_y','$k'<BR>";
			
			$this->return[$k]['id'] = $sql->last_insert_id;
			$this->return[$k]['imageid'] = $n_id;
		}
		
		if ($this->module['settings']['keep_original_image']) {
			$image_path = str_replace($_SERVER['DOCUMENT_ROOT'],'',$this->settings['upload_dir'])."/originals";
			$image_url = str_replace($_SERVER['DOCUMENT_ROOT'],'',$this->settings['upload_dir'])."/originals/".$original_file;
			$image_size = getimagesize($_SERVER['DOCUMENT_ROOT'].$image_path."/".$original_file);
			$image_x = $image_size[0];
			$image_y = $image_size[1];
			$sql->db_Insert($this->tables['images_aditional'],"'$n_id',$id,'$original_file','$image_path','$image_url','$date_added','$image_x','$image_y','originals'");
		}
   }//END FUNCTION

   function CropImage($src,$dest)
   {
   
   	if (!$settings['quality']) {
	$jpeg_quality = 100;
	}
   		$ImageType = get_image_types($this->module['id'],array('title'=>$this->settings['ImageType'],'debug'=>0));
   		$ImageType = $ImageType[$this->settings['ImageType']];

   		$targ_w = $ImageType['width'];
   		$targ_h = $ImageType['height'];
		$img_r = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'].$src['image_url']);
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
		  imagecopyresampled($dst_r,$img_r,0,0,$this->settings['coords']['x'],$this->settings['coords']['y'],
		    $targ_w,$targ_h,$this->settings['coords']['w'],$this->settings['coords']['h']);
		
		if (imagejpeg($dst_r, $_SERVER['DOCUMENT_ROOT'].$dest['image_url'], $jpeg_quality))
		{return true;}
		
		
   }//END FUNCTION
   
   function DeleteImages($imageIDs)
   {
   	global $sql;
   	foreach ($imageIDs as $v)
   	{
   		$this->RemoveFromDb(array('itemid'=>$v));
   	}
   }//END FUNCTION
   
   function DeleteOldThumb($itemid)
   {
    global $sql;
    $q = "DELETE item_images,item_images_aditional FROM item_images,item_images_aditional 
          WHERE (item_images.id=item_images_aditional.imageid) AND item_images.itemid = $itemid AND type = 0";
    $sql->q($q);
   }//END FUNCTION
   
 	function RemoveFromDb($settings)
 	{
 		global $sql;
 		if ($settings['itemid']) {//GO BY IMAGE ID
 		$id = $settings['itemid'];
 		$sql->db_Select($this->tables['images_aditional'],"image_url","imageid = $id");
 		//echo "SELECT image_url FROM ".$this->tables['images_aditional']." WHERE imageid = $id<br><br><br>";
		 if ($sql->db_Rows()) {
		 	$a = execute_multi($sql);
		 	foreach ($a as $v)
		 	{
		 		@unlink($_SERVER['DOCUMENT_ROOT'].$v['image_url']);
		 	}
		 }//END ROWS
		 
		 $sql->db_Delete($this->tables['images'],"id = ".$id);
		 $sql->db_Delete($this->tables['images_aditional'],"imageid = ".$id);
 		}//END DELETE BY ID
		elseif ($settings['imageid'])//DELETE A COPY OF THE IMAGE
		{
			$id = $settings['imageid'];
			$sql->db_Select($this->tables['images_aditional'],"image_url","id = $id");
			if ($sql->db_Rows()) {
				$image = execute_single($sql);
				$sql->db_Delete($this->tables['images_aditional'],"id = ".$id);
				@unlink($_SERVER['DOCUMENT_ROOT'].$image['image_url']);
			}//END ROWS
			
		}//END DELETE COPY OF IMAGE
 	}//END FUNCTION
 	
	function CreateImge($settings)
	{
		$srcFile = $this->base_dir."/".$this->file;
		$dir = $this->base_dir."/".$settings['dir'];
		$imagesize = getimagesize($srcFile);
		$image['name'] = $settings['prefix'].$this->file;
		$image['dir'] = $settings['dir'];
		$image['CleanDir'] = str_replace($_SERVER['DOCUMENT_ROOT'],"",$this->base_dir).$settings['dir'];
		list($srcW, $srcH, $srcType, $html_attr) = $imagesize;
		$ext = substr($this->file, -3);
		$ext = strtolower($ext);
		if($ext == 'jpg') {
			$srcImage = @imagecreatefromjpeg($srcFile);
		} elseif($ext == 'gif') {
			$srcImage = @imagecreatefromgif($srcFile);
		} elseif($ext == 'png') {
			$srcImage = @imagecreatefrompng($srcFile);
		}
			$fix = ($this->settings['ImageFix']) ? $this->settings['ImageFix'] : $settings['settings']['fix'];
error_message($fix,$_SERVER['DOCUMENT_ROOT']."/material/error_".$settings['width']."x".$settings['height'].".txt");
if ($fix == 'ImageBox') {
	$thumb = $this->thumbnail_box($srcImage, $settings['width'], $settings['height']);
}//END IMAGEBOX
elseif ($fix == 'ByWidth') {
	$thumb = $this->resizeByWidth($srcImage,$settings['width'],$settings['height'],$imagesize);
}//END RESIZE BY WIDTH
elseif ($fix == 'ByHeight') {
	
}//END RESIZE BY HEIGHT
elseif ($fix == 'ByWidthAndHeight') {
	
}//END RESIZE BY BOTH WIDTH AND HEIGHT

if ($thumb) {//If it is to be resized
imagedestroy($srcImage);

if(is_null($thumb)) {
    /* image creation or copying failed */
    header('HTTP/1.1 500 Internal Server Error');
    exit();
}
if (!$settings['quality']) {
	$settings['quality'] = 100;
}
$newFile = $dir."/".$settings['prefix'].$this->file;
imagejpeg($thumb, $newFile, $settings['quality']);
$image['size'] = getimagesize($newFile);
return $image;
}
else {//Check out original image
	return false;
	
	
}

	
	}//END FUNCTION
function resizeByWidth($srcImage,$width,$heightm,$imagesize)
{
		list($srcW, $srcH, $srcType, $html_attr) = $imagesize;

		if(!$srcImage) return false;
		//$srcW = imagesx($srcImage);
		//$srcH = imagesy($srcImage);
		$factor = $width / $srcW;

		$newH = (int) round($srcH * $factor);
		$newW = (int) round($srcW * $factor);

		$newImage = imagecreatetruecolor($newW, $newH);
		
		imagecopyresampled($newImage,$srcImage,0,0,0,0,$newW,$newH,$srcW,$srcH);
		return $newImage;
}//END FUNCTION

function thumbnail_box($src_img, $w, $h) {
	$lowend = 0.8;
    $highend = 1.25;
    //create the image, of the required size
    $new = imagecreatetruecolor($w, $h);
    if($new === false) {
        //creation failed -- probably not enough memory
        return null;
    }


    //Fill the image with a light grey color
    //(this will be visible in the padding around the image,
    //if the aspect ratios of the image and the thumbnail do not match)
    //Replace this with any color you want, or comment it out for black.
    //I used grey for testing =)
    $fill = imagecolorallocate($new, 255, 255, 255);
    imagefill($new, 0, 0, $fill);

			$src_w = imageSX($src_img);
            $src_h = imageSY($src_img);

            $scaleX = (float)$w / $src_w;
            $scaleY = (float)$h / $src_h;
            $scale = min($scaleX, $scaleY);

            $dstW = $w;
            $dstH = $h;
            $dstX = $dstY = 0;

            $scaleR = $scaleX / $scaleY;
            if($scaleR < $lowend || $scaleR > $highend)
            {
                $dstW = (int)($scale * $src_w + 0.5);
                $dstH = (int)($scale * $src_h + 0.5);

                // Keep pic centered in frame.
                $dstX = (int)(0.5 * ($w - $dstW));
                $dstY = (int)(0.5 * ($h - $dstH));
            }
            
            imagecopyresampled(
                $new, $src_img, $dstX, $dstY, 0, 0, 
                $dstW, $dstH, $src_w, $src_h); 
    //copy successful
    return $new;
}
}//END CLASS
/**
 * USED TO SAVE ITEMS TO DB AFTER AN UPLOAD USING THE JAVA TOOL
 *
 * @param array $settings
 */
function handleUploadedFiles($settings)
{
	global $sql;
	$lang = $settings['lang'];
	$module = $settings['module'];
	$filename = $settings['filename'];
	$itemid = $settings['itemid'];
	//First see what type of module we're dealing with
	if ($settings['module']['name'] == 'products') 
	{
		$item = get_products($itemid,$lang);
		$type = 'products';
		$table = 'item_images';
		$table_aditional = 'item_images_aditional';
	}
	elseif ($settings['module']['name'] == 'content')
	{
		$item = get_content(array('active'=>2,'id'=>$itemid,'get_provider'=> 1));
		$type = 'content';
		$table = 'item_images';
		$table_aditional = 'item_images_aditional';
	}
	elseif ($settings['module']['name'] == 'recipes')
	{
		$item = get_content($itemid,$lang);
		$type = 'recipes';
		$table = 'item_images';
		$table_aditional = 'item_images_aditional';
	}
	elseif ($settings['module']['name'] == 'news')
	{
		$item = get_news($itemid,$lang);
		$type = 'news';
		$table = 'item_images';
		$table_aditional = 'item_images_aditional';
	}
	elseif ($settings['module']['name'] == 'locations')
	{
		$item = get_locations($itemid,$lang);
		$type = 'location';
		$table = 'item_images';
		$table_aditional = 'item_images_aditional';
	}
	elseif ($settings['module']['name'] == 'd_items')
	{
		$item = get_d_items($itemid,$lang);
		$type = 'd_items';
		$table = 'item_images';
		$table_aditional = 'item_images_aditional';
	}

// At the moment the file is in the tmp directory, we need to see if there are copies to be made and then if the original is to be kept copy it to the right folder.
$category_folder = $module['settings']['images_path']."/category_".$item['catid'];
############## CHECK IF THIS CATEGORY DIRECTORY EXISTS #######################
if (!is_dir($_SERVER['DOCUMENT_ROOT'].$category_folder)) {
mkdir($_SERVER['DOCUMENT_ROOT'].$category_folder,0777);	
}
$item_folder = $category_folder."/".$type."_$itemid";
//create_folders($category_folder,$folder);
//Copy new image to the products folder
$new_file = ABSPATH.$item_folder."/".$filename;
$uploadfile = $settings['upload_dir'].$settings['filename'];

##################### CHECK FILE TYPES #######################
if ($settings['file_type'] == 'image') 
{
############ CHECK TO SEE IF WE NEED AN IMAGE SET #################
if ($settings['ImageSet']) 
{
######### START THE IMAGE SET CLASS ###################
if (copy($uploadfile,$new_file))
{
//	error_message($new_file);
$a = new CreateImageSet();
$dir = $_SERVER['DOCUMENT_ROOT']."/$item_folder";
$db_info = array('table' => $table,'table_aditional' => $table_aditional,'query' => 'itemid = '.$itemid,'itemid' => $itemid,'fields' => 'original_file');
$settings['image_types'] = get_image_types($module['id']);
$b = $a->Images($dir,$settings,$module,$db_info,$settings['type'],1);
}
}//END IMAGE SET
######################### NO IMAGE SET, JUST COPY IT #########################
else 
{
create_item_folders(array('directory_cat' => $category_folder, 'directory' =>$item_folder));
if (copy($uploadfile,$new_file))
{
	######################## INSERT TO DB!!!!!!! #################
}


}
}//END IMAGE TYPE
elseif ($settings['file_type'] == 'docs')
{
	
}//END DOCS TYPE
elseif ($settings['file_type'] == 'thumb')
{
############ CHECK TO SEE IF WE NEED AN IMAGE SET #################
	
if ($settings['ImageSet']) 
{
	mkdir($_SERVER['DOCUMENT_ROOT'].$item_folder,0777);
if (copy($uploadfile,$new_file))
{
######### START THE IMAGE SET CLASS ###################
$a = new CreateImageSet();
$dir = $_SERVER['DOCUMENT_ROOT']."/$item_folder";
$db_info = array('table' => $table,'table_aditional' => $table_aditional,'query' => 'itemid = '.$itemid,'itemid' => $itemid,'fields' => 'original_file');
$settings['image_types'] = get_image_types($module['id']);
$b = $a->Images($dir,$settings,$module,$db_info,0,$settings['imageFix']);//IMAGE TYPE IS SET TO 0 BECAUSE IT IS A THUMB
}
}//END IMAGE SET
######################### NO IMAGE SET, JUST COPY IT #########################
else 
{
create_item_folders(array('directory_cat' => $category_folder, 'directory' =>$item_folder));
if (copy($uploadfile,$new_file))
{
	######################## INSERT TO DB!!!!!!! #################
}


}	
}//END THUMB TYPE
elseif ($settings['file_type'] == 'video')
{
	//First copy the video file from the temp to the destination
	$destination_dir = ABSPATH.$item_folder;
	$destination = $destination_dir."/".$settings['filename'];
	copy($settings['upload_dir']."/".$settings['filename'],$destination);
	
	if (file_exists($destination)) 
	{
		
	$mov = new ffmpeg_movie($destination);
	$info['width'] = $mov->getFrameWidth();
	$info['height'] = $mov->getFrameHeight();
	$info['duration'] = $mov->getDuration();
	$info['framerate'] = $mov->getFrameRate();
	$info['filename'] = $mov->getFileName();
	$info['video_codec'] = $mov->getVideoCodec();
	$info['audio_codec'] = $mov->getAudioCodec();
	$info['audio_chanels'] = $mov->getAudioChannels();
	$info['bit_rate'] = $mov->getBitRate();
	
	}
	else {
		
	}

		$ext = substr($settings['filename'], -4);//extention + the dot
		$clear_file = str_replace($ext,'',$settings['filename']);	


//DONE WITH FILE ACTIONS, INSERT TO DB
$info_str = form_settings_string($info,"###",":::");
$date_added = time();

$sql->db_Insert("videos","'',".$module['id'].",".$itemid.",'".$settings['filename']."','$clear_file','$thumb','$item_folder','$info_str','','$date_added','','$clear_file - Description','',0");
$new_id = $sql->last_insert_id;
//RENAME THE VIDEO TO AVOID ISSUES
$new_filename = $new_id.$ext;


	if ($module['settings']['num_screen_caps'] > 0) //CREATE SCREEN CAPS
	{

		
		for ($i=0;$module['settings']['num_screen_caps'] > $i;$i++)
		{
			$cap = $mov->getFrame(rand(1,$mov->getFrameCount()));
		$img = $cap->toGDImage(); 
		imagejpeg($img, $destination_dir."/$new_id"."_screen_cap_".$i.".jpg");
		$screen_caps[] = $item_folder."/$new_id"."_screen_cap_".$i.".jpg";
		}
		$screen_caps_str = implode(":::",$screen_caps);
		$thumb = $screen_caps[0];//First cap is the thumb
		$q .= ", thumb = '$thumb' ";
	}//END SCREEN CAPS
	if ($screen_caps_str) {
		$q .= ", screen_caps = '$screen_caps_str'";
	}
$sql->db_Update("videos","filename = '$new_filename' $q WHERE id = $new_id");

//RETURN THIS TO RENAME THE FILE AFTER THE FFMPEG IS DONE. AT THIS POINT THE FILE IS LOCKED AND CANNOT BE RENAMED
return array('id' => $new_id, 'old_file' =>$destination, 'new_file' => $destination_dir."/$new_filename");
}//END VIDEO TYPE
elseif ($settings['file_type'] == 'video_aditional')
{
	//First copy the video file from the temp to the destination
	$destination_dir = ABSPATH.$item_folder;
	$destination = $destination_dir."/".$settings['filename'];
	copy($settings['upload_dir']."/".$settings['filename'],$destination);
	
	
	if (file_exists($destination)) 
	{
		
	$mov = new ffmpeg_movie($destination);
	$info['width'] = $mov->getFrameWidth();
	$info['height'] = $mov->getFrameHeight();
	$info['duration'] = $mov->getDuration();
	$info['framerate'] = $mov->getFrameRate();
	$info['filename'] = $mov->getFileName();
	$info['video_codec'] = $mov->getVideoCodec();
	$info['audio_codec'] = $mov->getAudioCodec();
	$info['audio_chanels'] = $mov->getAudioChannels();
	$info['bit_rate'] = $mov->getBitRate();
	
	}
	else {
		
	}
//Get the aditional format info
 $format = get_video_formats($settings['module']['id'],$settings['format_id']);
 
		$ext = substr($settings['filename'], -4);//extention + the dot
		$clear_file = str_replace($ext,'',$settings['filename']);
		$clear_file = $clear_file.' '.$format['suffix'];
		
//DONE WITH FILE ACTIONS, INSERT TO DB
$info_str = form_settings_string($info,"###",":::");
$date_added = time();

$sql->db_Insert("videos_aditional","'',".$settings['video_id'].",".$settings['format_id'].",".$module['id'].",".$itemid.",'".$settings['filename']."','$clear_file','$item_folder','$info_str','$date_added','',0");
//RENAME THE VIDEO TO AVOID ISSUES
$new_filename = $settings['video_id'].$format['suffix'].$ext;

$sql->db_Update("videos_aditional","filename = '$new_filename'  WHERE video_id = ".$settings['video_id']." AND format_id = ".$settings['format_id']);
return array('id' => $new_id, 'old_file' =>$destination, 'new_file' => $destination_dir."/$new_filename",'info' =>$info);
}
##################### END FILE TYPES #########################


}//END FUNCTION

  function seconds_to_hours($sec, $padHours = false) 
  {

    // holds formatted string
    $hms = "";

    // there are 3600 seconds in an hour, so if we
    // divide total seconds by 3600 and throw away
    // the remainder, we've got the number of hours
    $hours = intval(intval($sec) / 3600); 

    // add to $hms, with a leading 0 if asked for
    $hms .= ($padHours) 
          ? str_pad($hours, 2, "0", STR_PAD_LEFT). ':'
          : $hours. 'h:';
     
    // dividing the total seconds by 60 will give us
    // the number of minutes, but we're interested in 
    // minutes past the hour: to get that, we need to 
    // divide by 60 again and keep the remainder
    $minutes = intval(($sec / 60) % 60); 

    // then add to $hms (with a leading 0 if needed)
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). 'm:';

    // seconds are simple - just divide the total
    // seconds by 60 and keep the remainder
    $seconds = intval($sec % 60)."s"; 

    // add to $hms, again with a leading 0 if needed
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

    // done!
    return $hms;
    
  }
  
function error_message($message,$file=0)
{
	if (!$file) {$file = $_SERVER['DOCUMENT_ROOT']."/modules/log1234.txt";}
	$f = fopen($file,"w");
	if (is_array($message)) 
	{
		$message = var_export($message,true);
	}
	
	fwrite($f,$message);
	fclose($f);
}
class CreateImageSet
{
	
	var $base_dir = "";
	var $thumbDir = "";
	var $tPrefix = "";
	var $settings=0;
	var $fix=0;
	var $module_settings=0;
	
//INITIALIZE CLASS
	function Images($dir, $settings,$module_settings,$db_info,$type,$search_mode=0)
	{
	if(substr($dir, -1) == "/") $dir = substr($dir, 0, -1);
	$this->base_dir = $dir;
	$this->fix = $search_mode;
	$this->settings = $settings['image_types'];
	$this->options = $settings;
	$this->module_settings = $module_settings;
	$images = $this->create_image_set($settings['upload_dir'],$settings['filename']);
	$this->InsertToDb(ID,$images,$db_info['itemid'],$module_settings,$db_info,$type);

	return $images;
	}
	
function create_image_set($dir,$filename)
{
//Here is our image located on the tmp directory
$image = $dir.$filename;
if (!is_dir($this->base_dir)) 
{
	mkdir($this->base_dir,0777);
}
			
				foreach ($this->settings as $k =>$v) 
				{
//					echo $this->base_dir."/".$v['dir']."/".$image." ---- ".$v['width']."<br>";
					//We need to go dir by dir on this one
					$target = $this->base_dir."/".$v['dir']."/".$v['prefix'].$filename;
					
					$tmp[$k] = $this->checkFile($target,$v['width'],$v['height'],$this->fix);

						if (!is_dir($this->base_dir."/".$v['dir'])) 
						{
							mkdir($this->base_dir."/".$v['dir'],0777);
						}
						
						if ($this->module_settings['settings']['keep_original_image'] AND !$v['dir']) 
						{
							if (!is_dir($this->base_dir."/originals")) 
							{
								mkdir($this->base_dir."/originals",0777);
							}
							copy($image,$this->base_dir."/originals/$filename");
								
						}
						if ($v['settings']) {
							$v['settings'] = form_settings_array($v['settings'],"###",":::");
						}
						if ($v['settings']['resize']) {
							$tmp[$k] = $this->createImge($filename,$this->base_dir."/".$v['dir'],$v['width'],$v['height'],$v['prefix'],$v['quality'],$v['settings']);	
						}

						
						
					if ($tmp[$k]) 
					{
					$images[$k]['path'] = $target;
					$images[$k]['image'] = $v['prefix'].$filename;
					
					}
				//	print_r($ar);

				}
		
			
			return $images;

}

function InsertToDb($uid,$images,$id,$module_settings,$db_info,$type)
{
	global $sql;
	
	if ($images) 
	{
//		error_message("SELECT ".$db_info['fields']." FROM ".$db_info['table']." WHERE ".$db_info['query'],$_SERVER['DOCUMENT_ROOT']."/modules/error.txt");
		


			$original_file = $images['main']['image'];

			$sql->db_Insert($db_info['table'],"'','$id','$original_file','$alt','$title','$details','$available','$i','$type'");
			$n_id = $sql->last_insert_id;
//			$f = fopen("log_upload.txt","w");
			foreach ($images as $k => $v)
			{
				//Insert to aditional tables
				$image = $v['image'];
				$image_path = $v['path'];
				
//				fwrite($f,"BEFORE : ".$image_path."\n\r");
				$image_details = getimagesize($image_path);	
				$image_x = $image_details[0];
				$image_y = $image_details[1];
				$date_added = time();
				$image_path = str_replace("//","/",$image_path);
				$image_url = str_replace($_SERVER['DOCUMENT_ROOT'],"",$image_path);//WINDOWS ONLY
				$image_path = str_replace("\\","/",$image_path);//WINDOWS ONLY
				
//				fwrite($f,"AFTER : ".$image_path."\n\r");
				$sql->db_Insert($db_info['table_aditional'],"'$n_id',$id,'$image','$image_path','$image_url','$date_added','$image_x','$image_y','$k'");
//				echo "'$n_id','$image','$image_path','$image_x','$image_y','$k'<BR>";
				
			}
//			fclose($f);

	}
	
}

	function read_dir_images($dir)
	{
		$files = false;
		if($resDir = opendir($dir))
		{
			// check all files in $dir - add images to array 'files'
			$cpos = 0;
			while($file = readdir($resDir))
			{
				if($file[0] != "_" && $file != "." && $file != ".." && !is_dir($dir . "/" . $file))
				{
					$ext = substr($file, -3);
					$ext = strtolower($ext);
					if($ext == 'jpg' OR $ext == 'gif' OR $ext == 'png' OR $ext == 'jpeg') {
						$files[$cpos] = $file;
						$cpos++;
					}
				}
			}
			closedir($resDir);
		}
		if($files) sort($files);
		return $files;
	}
	
	function checkFile($imageFile,$maxW=0,$maxH=0,$fix=0)
	{//USED TO 
		if(file_exists($imageFile)) {

			list($srcW, $srcH, $srcType, $html_attr) = getimagesize($imageFile);
			
			if($fix == 1) {
				if($srcW > $maxW) {//Check by width. This will enable resize only if the srcW is bigger than expected
//					echo "here 1";
					return true;
				}
			} elseif($fix == 2) {//check by height. This will enable resize only if the srcH is bigger than expected
				if($srcH > $maxH) {
//					echo "here 2";
					return true;
				}
			} elseif($fix == 3) {//check both width and height. This will enable resize only if BOTH srcW AND srcH are bigger than expected
				if($srcH > $maxH AND $srcW > $maxW) {
//					echo "here 3";
					return true;
				}
			}
			elseif($fix == 4) {//check both width and height. This will enable resize only if either srcW OR srcH are bigger than expected
				if($srcH > $maxH OR $srcW > $maxW) {
//					echo "heres 4";
					return true;
				}
			}
//			echo "thumb of $imageFile exists<br>";
			return false;
		} else {
//			echo "thumb of $image doesn't exist!!!<br>";
			return false;
		}
	}

	function createImge($image,$dir,$width,$height,$prefix=0,$quality=100)
	{
		$srcFile = $this->base_dir."/".$image;
		list($srcW, $srcH, $srcType, $html_attr) = getimagesize($srcFile);
		$ext = substr($image, -3);
		$ext = strtolower($ext);
		if($ext == 'jpg') {
			$srcImage = @imagecreatefromjpeg($srcFile);
		} elseif($ext == 'gif') {
			$srcImage = @imagecreatefromgif($srcFile);
		} elseif($ext == 'png') {
			$srcImage = @imagecreatefrompng($srcFile);
		}
		
$thumb = $this->thumbnail_box($srcImage, $width, $height);
imagedestroy($srcImage);

if(is_null($thumb)) {
    /* image creation or copying failed */
    header('HTTP/1.1 500 Internal Server Error');
    exit();
}
$newFile = $dir."/".$prefix.$image;
imagejpeg($thumb, $newFile, $quality);
		
	return true;
	}

function thumbnail_box($img, $box_w, $box_h) {
    //create the image, of the required size
    $new = imagecreatetruecolor($box_w, $box_h);
    if($new === false) {
        //creation failed -- probably not enough memory
        return null;
    }


    //Fill the image with a light grey color
    //(this will be visible in the padding around the image,
    //if the aspect ratios of the image and the thumbnail do not match)
    //Replace this with any color you want, or comment it out for black.
    //I used grey for testing =)
    $fill = imagecolorallocate($new, 255, 255, 255);
    imagefill($new, 0, 0, $fill);

    //compute resize ratio
    $hratio = $box_h / imagesy($img);
    $wratio = $box_w / imagesx($img);
    $ratio = min($hratio, $wratio);

    //if the source is smaller than the thumbnail size, 
    //don't resize -- add a margin instead
    //(that is, dont magnify images)
    if($ratio > 1.0)
        $ratio = 1.0;
        
        echo imagesx($img)." --- ".imagesy($img). " | "."$box_w --- $box_h ---- $ratio\n";
    //compute sizes
    $sy = floor(imagesy($img) * $ratio);
    $sx = floor(imagesx($img) * $ratio);

    //compute margins
    //Using these margins centers the image in the thumbnail.
    //If you always want the image to the top left, 
    //set both of these to 0
    $m_y = floor(($box_h - $sy) / 2);
    $m_x = floor(($box_w - $sx) / 2);

    //Copy the image data, and resample
    //
    //If you want a fast and ugly thumbnail,
    //replace imagecopyresampled with imagecopyresized
    if(!imagecopyresampled($new, $img,
        $m_x, $m_y, //dest x, y (margins)
        0, 0, //src x, y (0,0 means top left)
        $sx, $sy,//dest w, h (resample to this size (computed above)
        imagesx($img), imagesy($img)) //src w, h (the full size of the original)
    ) {
        //copy failed
        imagedestroy($new);
        return null;
    }
    //copy successful
    return $new;
}
	
	function rebuildThumbs()
	{
		@set_time_limit(300);
		$imageList = $this->getImageNames();
		foreach($imageList as $image) {
			$this->createThumb($image);
		}
	}
	
	
}//END OF CLASS


function create_item_folders($settings)
{
				//Create the user personal folder
				if (!is_dir($settings['directory_cat'])) 
				{
					if (mkdir($settings['directory_cat']))
					{
					mkdir($settings['directory'],0777);
					return true;
					}
					else 
					{
						echo "can't write";
					}
				}
				else 
				{
					mkdir($settings['directory'],0777);
					return true;
				}
}

function get_item_videos($module_id,$itemid=0,$id=0,$settings=0)
{
	global $sql;
	
	if ($itemid) //This should return all videos of a given item
	{
		if ($settings['orderby']) {
			$q .= "ORDER BY ".$settings['orderby'];
		}
		if ($settings['way']) 
		{
			$q .= $settings['way'];
		}
		$sql->db_Select("videos","*","module_id = $module_id AND itemid = $itemid $q");
		if ($sql->db_Rows()) 
		{
			$res = execute_multi($sql);
			$video_formats = get_video_formats($settings['module_id']);
			for ($i=0;count($res) > $i;$i++)
			{
				$tmp = $res[$i]['info'];
				unset($res[$i]['info']);
				$res[$i]['info'] = form_settings_array($tmp,"###",":::");
				if ($res[$i]['settings']) 
				{
					$tmp1 = $res[$i]['settings'];
					unset($res[$i]['settings']);
					$res[$i]['settings'] = form_settings_array($tmp1,"###",":::");
				}
				if ($res[$i]['info']['duration']) 
				{
					$res[$i]['info']['duration'] = seconds_to_hours($res[$i]['info']['duration']);
				}
				if ($res[$i]['screen_caps']) 
				{
					$tmp = $res[$i]['screen_caps'];
					unset($res[$i]['screen_caps']);
					$res[$i]['screen_caps'] = explode(":::",$tmp);
				}
				if ($settings['get_formats']) 
				{
					$res[$i]['formats'] = get_item_videos_aditional($res[$i]['id']);
					if ($settings['formats_cover_gaps']) //This will loop through the aditional formats of this video and padd with an empty array the missing ones
					{
							if (count($res[$i]['formats']) != count($video_formats)) 
							{
								if (is_array($res[$i]['formats'])) 
								{
									$tmp = array();//We will hold the available formats here
									$tmp1 = array();//We will hold the video formats here
									$tmp2 = array();//We will hold the missing formats here
									foreach ($res[$i]['formats'] as $f)
									{
										$tmp[] = $f['format_id'];
									}
									foreach ($video_formats as $vf) {
										$tmp1[] = $vf['id'];
									}
									$tmp2 = array_diff($tmp1,$tmp);
									$tmp3 = array_merge($tmp2,array());
				
									
									foreach ($tmp3 as $m)
									{
										//loop through the video formats to compare
										for ($j=0;count($video_formats) > $j;$j++)
										{
											if ($video_formats[$j]['id'] == $m) 
											{
												$res[$i]['formats'][] = $video_formats[$j];
											}
										}
									}

								}
								else 
								{
									$res[$i]['formats'] = $video_formats;
								}

							}
					}
				}
			}//END FOR
			return $res;
		}
	}
	if ($id) //This should return a single entry
	{
		$sql->db_Select("videos","*","id = $id");
		if ($sql->db_Rows()) 
		{
				$res = execute_single($sql);

				$tmp = $res['info'];
				unset($res['info']);
				$res['info'] = form_settings_array($tmp,"###",":::");
				if ($res['settings']) 
				{
					$tmp1 = $res['settings'];
					unset($res['settings']);
					$res['settings'] = form_settings_array($tmp1,"###",":::");
				}
				if ($res['info']['duration']) 
				{
					$res['info']['duration'] = seconds_to_hours($res['info']['duration']);
				}
				if ($res['screen_caps']) 
				{
					$tmp = $res['screen_caps'];
					unset($res['screen_caps']);
					$res['screen_caps'] = explode(":::",$tmp);
				}
		}
		return $res;
	}
	
}


function get_item_videos_aditional($video_id,$format_id=0)
{
	global $sql;

	if ($video_id AND !$format_id) //This should return all videos of a given item
	{
		$sql->db_Select("videos_aditional","*","video_id = $video_id");
		if ($sql->db_Rows()) 
		{
			$res = execute_multi($sql);
			for ($i=0;count($res) > $i;$i++)
			{
				$tmp = $res[$i]['info'];
				unset($res[$i]['info']);
				$res[$i]['info'] = form_settings_array($tmp,"###",":::");
				if ($res[$i]['settings']) 
				{
					$tmp1 = $res[$i]['settings'];
					unset($res[$i]['settings']);
					$res[$i]['settings'] = form_settings_array($tmp1,"###",":::");
				}
				if ($res[$i]['info']['duration']) 
				{
					$res[$i]['info']['duration'] = seconds_to_hours($res[$i]['info']['duration']);
				}
			}//END FOR
			return $res;
		}

	}
	
	if ($video_id AND $format_id) //This should return a single entry
	{
		$sql->db_Select("videos_aditional","*","video_id = $video_id AND format_id = $format_id");
		if ($sql->db_Rows()) 
		{
				$res = execute_single($sql);

				$tmp = $res['info'];
				unset($res['info']);
				$res['info'] = form_settings_array($tmp,"###",":::");
				if ($res['settings']) 
				{
					$tmp1 = $res['settings'];
					unset($res['settings']);
					$res['settings'] = form_settings_array($tmp1,"###",":::");
				}
				if ($res['info']['duration']) 
				{
					$res['info']['duration'] = seconds_to_hours($res['info']['duration']);
				}
				return $res;
		}

	}
		
}

function get_video_formats($module_id,$settings=0)
{
	global $sql;
	
	if ($settings['id']) 
	{
		$sql->db_Select("video_formats","*","module_id = $module_id AND id = ".$settings['id']);
		if ($sql->db_Rows()) 
		{
			return execute_single($sql);
		}
	}
	else {
	$sql->db_Select("video_formats","*","module_id = $module_id");
	if ($sql->db_Rows()) 
	{
		return execute_multi($sql);
	}
	}
}

?>