<?php
include("../../../manage/init.php");//load from manage!!!!
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ##########################################
if ($_GET['cat']) {
	$current_module = $loaded_modules[CURRENT_MODULE];
    $smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("content_module",$current_module);
	$posted_data = array();//setup a a clean array
	$cat = $_GET['cat'];
	if (is_numeric($cat)) {
	$cat = ($cat) ? $cat : 0;
	}
	else {//REVERSE LOOKUP
		$cat = str_replace("content/","",$cat);
		$sql->db_Select("content_categories","categoryid","alias = '$cat'");
		if ($sql->db_Rows()) {
			$a = execute_single($sql);
			$cat = $a['categoryid'];
		}
	}
	$current_category = $Content->ItemCategory($cat,array('table'=>'content_categories','settings'=>1));
	$posted_data = array('categoryid'=>$cat,'debug'=>0,'availability'=>2);
	$posted_data['sort'] = ($settings['sort']) ? $settings['sort'] : $current_module['settings']['default_sort'];
	$posted_data['sort_direction'] = ($settings['sort_direction']) ? $settings['sort_direction'] : $current_module['settings']['default_sort_direction'];
	$posted_data['results_per_page'] = ($settings['results_per_page']) ? $settings['results_per_page'] : $current_module['settings']['items_per_page'];	
	$posted_data['page'] = $page;
	$posted_data['cat_details'] = 1;
    $posted_data['search_in_subcategories'] = 1;
	$items = $Content->ItemSearch($posted_data,$current_module,$page,0);
	$smarty->assign("cat",$current_category);
	$smarty->assign('nav',$Content->CatNav($current_category['categoryid'],array('debug'=>0,'table'=>'content_categories')));
	$smarty->assign("items",$items['results']);
    $smarty->assign("SavedData",form_settings_string($posted_data));
}


$smarty->assign("menu",$content_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("catid",$cat);
$smarty->assign("parentid",$parentid);
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/content/admin/category_content.tpl");
$smarty->display("admin/home.tpl");

?>