<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("content",$loaded_modules)) {
	$current_module = $loaded_modules['content'];
	$smarty->assign("MODULE_FOLDER",$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$Content = new Items(array('module'=>$current_module,'debug'=>0));
}
$cat = ($_GET['cat']) ? $_GET['cat'] : 0;
##################### ADD NEW CATEGORY ######################3
if ($_POST['action'] == "addcat") 
{
//ADD THE CATEGORY
$ClsCat = new CategoriesActions(array('module'=>$current_module,'debug'=>0));
$cat = $ClsCat->AddCommonTreeCategory($_POST['tableID'],$_POST,array('return'=>'cat','file'=>'content_category','debug'=>0));
header("Location: commonCategoriesTree.php?tableID=".$_POST['tableID']."&cat=$cat");
exit();
}//END OF NEW CATEGORY

if ($_POST['action'] == "modify") 
{
$ClsCat = new CategoriesActions(array('module'=>$current_module,'debug'=>0));
$data = $_POST;
$data['category'] = $category;
$data['cat'] = $cat;
$ClsCat->ModifyCommonTreeCategory($data['tableID'],$data,array('debug'=>0));
header("Location: commonCategoriesTree.php?action=edit&tableID=".$data['tableID']."&cat=".$cat);
 exit();
}//END MODIFY CATEGORY
if ($_GET['tableID']) {
	$smarty->assign("table",$Content->ListCommonCategories($current_module['name'],array('type'=>'tree'),$_GET['tableID']));
}
if ($_GET['action'] == 'edit') {
		$smarty->assign("category",$category);
		$smarty->assign("action","modify");
		$smarty->assign("category",$Content->commonCategoryTree($_GET['tableID'],$_GET['cat']));
		$smarty->assign("edit",1);
}
elseif ($_GET['action'] == "delete")
{
	 $sql->db_Select("common_categories_tree","categoryid","categoryid_path LIKE '%$cat/%'");
	 if ($sql->db_Rows() > 0) {
	 	$tmp = execute_multi($sql,0);
	 	for ($i=0;count($tmp) > $i;$i++)
	 	{
	 	$sql->db_Delete("common_categories_tree","tableID = ".$_GET['tableID']." AND categoryid=".$tmp[$i]['categoryid']);
	 	$sql->db_Delete("common_categories_tree_items","catid=".$tmp[$i]['categoryid']);
	 	
//	 	echo "DELETE FROM categories WHERE categoryid=".$tmp[$i]['categoryid'].";<br>";
	 	}
	 }
//	 echo "DELETE FROM categories WHERE categoryid=".$cat.";<br>";
	 $sql->db_Delete("common_categories_tree","categoryid= $cat");
	 $sql->db_Delete("common_categories_tree_items","catid=$cat");
	  header("Location: commonCategoriesTree.php?tableID=".$_GET['tableID']);
	 exit();
}

if ($_GET['mode'] == 'translate') {
	$smarty->assign("mode",$_GET['mode']);
	$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
	for ($i=0;count($availableLanguages) > $i;$i++)
	{
		if ($availableLanguages[$i]['code'])
		{
		$availableLanguages[$i]['item'] = $Languages->itemTranslations($category['categoryid'],$availableLanguages[$i]['code'],$current_module['name'],
		array('groupby'=>'field','debug'=>0,'table'=>$current_module['name']."_categories"));
		}
	}
	$smarty->assign("availableLanguages",$availableLanguages);//WE WANT FRONT END ONLY
	$smarty->assign("currentLanguage",$availableLanguages[0]);//WE WANT FRONT END ONLY
	$smarty->assign("defaultLanguage",$Languages->getDefaultLanguage('default_lang',array('return'=>'array','languageDetails'=>1)));//WE WANT FRONT END ONLY
	$smarty->assign("table",$Content->ListCommonCategories($current_module['name'],array('type'=>'tree'),$_GET['tableID']));
}


if (!$_GET['tableID']) {
	$TreeCategories = $Content->ListCommonCategories($current_module['name'],array('type'=>'tree'));
	$smarty->assign("categories",$TreeCategories);
}//END LIST TABLES
else {//TABLE
	$smarty->assign("currentTable",$Content->CommonCategoriesTree($_GET['tableID'],$cat,array('debug'=>0,'num_items'=>1,'num_subs'=>1)));	
	$smarty->assign("nav",$Content->commonTreeCatNav($_GET['tableID'],$cat));//assigned template variable a
	$smarty->assign("tableID",$_GET['tableID']);//assigned template variable a
	
	$all_categories = $Content->AllCommonTreeCategories($_GET['tableID']);
	foreach ($all_categories as $v)
	{
		$simplified_categories[] = $v['categoryid'];
	}
$smarty->assign("allcategories",$all_categories);
$smarty->assign("simple_categories",$simplified_categories);
if (!$_GET['action']) {
	$smarty->assign("currentCategory",$Content->commonCategoryTree($_GET['tableID'],$_GET['cat']));
}

}//END TABLE

$smarty->assign("themes",get_themes());
$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","commonCategoriesTree");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']);
$smarty->assign("include_file","admin/commonCategoriesTree.tpl");
$smarty->display("admin/home.tpl");

?>