<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("content",$loaded_modules)) {
	$current_module = $loaded_modules['content'];
	$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$module_path = URL."/".$current_module['folder']."/admin";

$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];

//root categories
if (!$cat) 
{  
 	$cat = 0;
}//END OF IF
	
$category = (empty($cat)) ? 0 : content_flat_category($cat,array('table'=>'content_cooking_methods'));
################# QUICK UPDATE #####################
if ($_POST['mode'] == "quick_update") 
{
		foreach ($_POST as $key => $val)
		{
			if (strstr($key,"-")) 
			{
				list($field,$code)=split("-",$key);
				$sql->db_Update("content_cooking_methods","$field = '$val' WHERE categoryid = $code");
			}
		}
header("Location: ".e_SELF);
exit();						
}
##################### ADD NEW CATEGORY ######################3
if ($_POST['action'] == "addcat") 
{
//ADD THE CATEGORY
content_add_flat_category('content_cooking_methods',$_POST,array('return'=>'cat','file'=>'cooking_methods'));
header("Location: ".e_SELF);
exit();
}//END OF NEW CATEGORY
##################### MODIFY EXISTING CATEGORY ######################3
if ($_POST['action'] == "modify") 
{
$data = $_POST;
$data['category'] = $category;
$data['cat'] = $cat;
content_modify_flat_category('content_cooking_methods',$data,array('debug'=>0));
header("Location: ".e_SELF."?action=edit&cat=".$cat);
 exit();
}//END MODIFY CATEGORY

if ($_GET['action'] == "edit") 
{
	
	$smarty->assign("category",$category);
	$smarty->assign("edit",1);
	$smarty->assign("action","modify");
	$smarty->assign("more_categories",content_flat_categories(array('table'=>'content_cooking_methods')));
}
elseif ($_GET['action'] == "delete")
{
	 $sql->db_Delete("content_cooking_methods","categoryid= $cat");
	  header("Location: ".e_SELF);
	 exit();
}
$smarty->assign("allcategories",content_flat_categories(array('table'=>'content_cooking_methods')));
$smarty->assign("cat",content_flat_categories(array('table'=>'content_cooking_methods','table_page'=>'content_page_cooking_methods','num_items'=>1,'debug'=>0)));//assigned template variable cat

}//END MODULE IS ACTIVE
else {
	exit();
}


$smarty->assign("themes",get_themes());
$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","cooking_methods");
$smarty->assign("area","Cooking Methods");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']." | Cooking Methods");
$smarty->assign("include_file",$current_module['folder']."/admin/categories_flat.tpl");
$smarty->display("admin/home.tpl");

?>