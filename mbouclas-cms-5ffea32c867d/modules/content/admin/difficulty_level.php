<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("content",$loaded_modules)) {
	$current_module = $loaded_modules['content'];
	$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$module_path = URL."/".$current_module['folder']."/admin";

$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];

//root categories
if (!$cat) 
{  
 	$cat = 0;
}//END OF IF
	
$category = (empty($cat)) ? 0 : content_flat_category($cat,array('table'=>'content_difficulty_level'));
################# QUICK UPDATE #####################
if ($_POST['mode'] == "quick_update") 
{
		foreach ($_POST as $key => $val)
		{
			if (strstr($key,"-")) 
			{
				list($field,$code)=split("-",$key);
				$sql->db_Update("content_difficulty_level","$field = '$val' WHERE categoryid = $code");
			}
		}
header("Location: ".e_SELF);
exit();						
}
##################### ADD NEW CATEGORY ######################3
if ($_POST['action'] == "addcat") 
{
//ADD THE CATEGORY
content_add_flat_category('content_difficulty_level',$_POST,array('return'=>'cat','file'=>'difficulty_level'));
header("Location: ".e_SELF);
exit();
}//END OF NEW CATEGORY
##################### MODIFY EXISTING CATEGORY ######################3
if ($_POST['action'] == "modify") 
{
$data = $_POST;
$data['category'] = $category;
$data['cat'] = $cat;
content_modify_flat_category('content_difficulty_level',$data,array('debug'=>0));
header("Location: ".e_SELF."?action=edit&cat=".$cat);
 exit();
}//END MODIFY CATEGORY

if ($_GET['action'] == "edit") 
{
	
	$smarty->assign("category",$category);
	$smarty->assign("edit",1);
	$smarty->assign("action","modify");
	$smarty->assign("more_categories",content_flat_categories(array('table'=>'content_difficulty_level')));
}
elseif ($_GET['action'] == "delete")
{
	 $sql->db_Delete("content_difficulty_level","categoryid= $cat");
	  header("Location: ".e_SELF);
	 exit();
}
$smarty->assign("allcategories",content_flat_categories(array('table'=>'content_difficulty_level')));
$smarty->assign("cat",content_flat_categories(array('table'=>'content_difficulty_level','table_page'=>'content_page_difficulty_level','num_items'=>1,'debug'=>0)));//assigned template variable cat

}//END MODULE IS ACTIVE
else {
	exit();
}


$smarty->assign("themes",get_themes());
$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","difficulty_level");
$smarty->assign("area","Difficulty Levels");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']." | Cooking Methods");
$smarty->assign("include_file",$current_module['folder']."/admin/categories_flat.tpl");
$smarty->display("admin/home.tpl");

?>