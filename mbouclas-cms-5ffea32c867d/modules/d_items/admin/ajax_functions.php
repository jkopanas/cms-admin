<?php
include(ABSPATH."/includes/xajax/xajax.inc.php");
$xajax = new xajax("","xajax_",'utf-8');
global $smarty;

$xajax->registerFunction("delete_bookmark");
$xajax->registerFunction("add_bookmark");
$xajax->registerFunction("set_d_items");
$xajax->registerFunction("set_featured");
$xajax->registerFunction("set_related");
$xajax->registerFunction("set_trivia_featured");
$xajax->registerFunction("deleteRelated");
$xajax->registerFunction("deleteFeatured");
$xajax->registerFunction("autocomplete");
$xajax->registerFunction("get_search_categories");
$xajax->registerFunction("set_extra_fields");


function autocomplete($input,$target)
{
	global $sql,$smarty;
	$objResponse = new xajaxResponse();
	$len = strlen($input);
		if ($len)
	{
		$sql->db_Select("item_ingredients","distinct(base)","base like '$input%' ORDER BY base");
		$suggest = execute_multi($sql,0);
		
		$smarty->assign("auto",$suggest);
		$smarty->assign("target","$target");
		$message = $smarty->fetch("modules/d_items/admin/autocomplete_list.tpl");
		
		
	}
	else {
		$message = "No results";
	}
	$objResponse->addAssign("inr_display","innerHTML",$message);
	$objResponse->addScript("setTimeout(\"document.getElementById('inr_display').innerHTML=''\",3500);");
	return $objResponse;
}

function delete_bookmark($id,$res_div)
{
	global $smarty,$lang;
	$sql = new db();
	$sql->db_Delete("bookmarks","itemid = '$id' AND uid = ".ID);
	$objResponse = new xajaxResponse();
    $objResponse->addAssign($res_div,"innerHTML","");
    return $objResponse;
}//END FUNCTION

function deleteRelated($dest,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
	$sql->db_Delete("d_items_links","d_items_source = ".$id." AND d_items_dest = ".$dest);
	$smarty->assign("links",get_related_d_items($id,DEFAULT_LANG));//assigned template variable links
	$message = $smarty->fetch("modules/d_items/admin/related_d_items_table.tpl");
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function add_bookmark($aFormValues,$res_div)
{
	global $smarty,$lang;
	$sql = new db();
	foreach ($aFormValues['ids'] as $key => $val)
	{
		$date_added = time();
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("bookmarks","'$key','$date_added', '".ID."'");			
		}
	}//END OF WHILE
	
	$objResponse = new xajaxResponse();
    $objResponse->addAssign($res_div,"innerHTML","");
    return $objResponse;
}//END FUNCTION

function set_d_items1($aFormValues,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
if (!$id) {
$id = $aFormValues['id'];	
}
	foreach ($aFormValues['ids'] as $key => $val)
	{
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("d_items_links",$id.",".$key.",'$orderby'");
			if ($aFormValues['bidirectional-'.$key] == 1) 
			{
				$sql->db_Insert("d_items_links",$key.",".$id.",'$orderby'");
			}
		}
	}//END OF WHILE

	$smarty->assign("links",get_upselling($id));//assigned template variable links
	$message = $smarty->fetch("modules/d_items/admin/related_d_items_table.tpl");
    $objResponse->addAssign("cat_results","innerHTML",'');
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function set_featured($aFormValues,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
	$cat = $aFormValues['category'];
	foreach ($aFormValues['ids'] as $key => $val)
	{
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("d_items_featured","'".$key."','$cat','','cat'"); 
			if ($aFormValues['bidirectional-'.$key] == 1) 
			{
				$d_items = get_d_items($key,DEFAULT_LANG);
				$bi_id = $d_items['catid'];

				$sql->db_Insert("d_items_featured","'".$key."','$bi_id','','cat'"); 
			}

		}
	}//END OF WHILE
	
	$smarty->assign("links",get_featured_d_items($cat,"cat",DEFAULT_LANG));//assigned template variable links
	$message = $smarty->fetch("modules/d_items/admin/related_d_items_table.tpl");
    $objResponse->addAssign("cat_results","innerHTML",'');
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function set_related($aFormValues,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
if (!$id) {
$id = $aFormValues['itemid'];	
}
	foreach ($aFormValues['ids'] as $key => $val)
	{
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("d_items_links",$id.",".$key.",'$orderby'");
			if ($aFormValues['bidirectional-'.$key] == 1) 
			{
				$sql->db_Insert("d_items_links",$key.",".$id.",'$orderby'");
			}
		}
	}//END OF WHILE

	$smarty->assign("links",get_related_d_items($id,DEFAULT_LANG));//assigned template variable links
	$message = $smarty->fetch("modules/d_items/admin/related_d_items_table.tpl");
    $objResponse->addAssign("cat_results","innerHTML",'');
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function set_trivia_featured($aFormValues,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
	foreach ($aFormValues['ids'] as $key => $val)
	{
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("d_items_trivia","'".$id."','$key'"); 

		}
	}//END OF WHILE
$trivia = get_trivia($id,$letter="a",DEFAULT_LANG);
	$smarty->assign("links",$trivia['related']);//assigned template variable links
	$message = $smarty->fetch("modules/d_items/admin/related_d_items_table.tpl");
    $objResponse->addAssign("cat_results","innerHTML",'');
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function deleteFeatured($dest,$set_div)
{
	global $smarty,$lang,$sql,$id,$cat;
	$objResponse = new xajaxResponse();
	$cat = ($cat) ? $cat : 0;
	$sql->db_Delete("d_items_featured","itemid = ".$dest." AND categoryid = ".$cat);
	$smarty->assign("links",get_featured_d_items($cat,"cat",DEFAULT_LANG));//assigned template variable links
	$message = $smarty->fetch("modules/d_items/admin/related_d_items_table.tpl");
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function get_search_categories($aFormValues,$res_div,$mode)
{
	global $smarty,$lang,$id,$sql,$in_form;

	$search_tables = "d_items ";
$fields = "d_items.id,d_items.active";

if ($mode == "cat") 
{
	$sql->db_Select("d_items INNER JOIN d_items_page_categories ON (d_items.id=d_items_page_categories.itemid)","d_items.id","catid = '".$aFormValues['cat']."' AND main ='Y'");
//$message_q = $sql->db_Rows();
$tmp = execute_multi($sql);
	for ($i=0;count($tmp) > $i;$i++)
	{
	
	$d_items[$i] = get_d_items($tmp[$i]['id'],DEFAULT_LANG);	
	}//END OF FOR

}//EDN OF CATEGORY SEARCH
elseif ($mode == "quick")
{
$aFormValues['substring'] = trim($aFormValues['substring']);


 $condition[] = "d_items_lng.title LIKE '%".$aFormValues["substring"]."%'";

 $condition[] = "d_items_lng.description LIKE '%".$aFormValues["substring"]."%'";

 $condition[] = "d_items_lng.full_description LIKE '%".$aFormValues["substring"]."%'";
 if (!empty($condition))
{ 
	$search_condition .= "(".implode(" OR ", $condition).")"." AND ";
	$fields .= " ,d_items_lng.title ";
$search_tables .= "INNER JOIN d_items_lng ON (d_items.id=d_items_lng.itemid)";
}//END OF IF
$search_condition .= "
d_items.active IN (0,1) 
GROUP BY d_items.id
ORDER BY date_added DESC";
$sql->db_Select($search_tables,$fields,"$search_condition");
$d_items = execute_multi($sql,0);
}//END OF KEYWORD SEARCH
if (count($d_items) > 0) 
{
	$smarty->assign("d_items",$d_items);
	$smarty->assign("query",$message_q);
	if ($in_form) {
		$smarty->assign("in_form",$in_form);
	}
	$message = $smarty->fetch("modules/d_items/admin/pop_search_results.tpl");
}
else {
	$message = $lang['txt_no_d_items'];
}
	$objResponse = new xajaxResponse();
    $objResponse->addAssign($res_div,"innerHTML","");
    $objResponse->addAssign($res_div,"innerHTML",$message);
    return $objResponse;
}

function set_extra_fields($catid,$resdiv,$cid)
{
	global $sql,$smarty;
	$smarty->assign("extra_fields",get_extra_fields_d_items("Y",DEFAULT_LANG,$cid,$catid));
	$message = $smarty->fetch("common/extra_fields_modify.tpl");

	$objResponse = new xajaxResponse();
    $objResponse->addAssign($resdiv,"innerHTML","");
    $objResponse->addAssign($resdiv,"innerHTML",$message);
    return $objResponse;
}

			if ($maps_module) 
			{
			include($_SERVER['DOCUMENT_ROOT']."/".$maps_module['folder']."/admin/ajax_functions_inc.php");
			}

$xajax->processRequests();
$smarty->assign("ajax_requests",$xajax->getJavascript(URL)); 
?>