<?php
include("../../../manage/init.php");//load from manage!!!!
if ($d_items_module = module_is_active("d_items",1,1,0)) 
{
	$module_path = URL."/".$d_items_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",URL."/".$d_items_module['folder']."/admin");

$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];
$category = (empty($cat)) ? 0 : get_d_items_category($cat,DEFAULT_LANG);
$parentid = ($_GET['action']) ? $category['parentid'] : $cat;

$active = "0,1";
$categoryid = $category['categoryid'];

 $sql->db_Select("d_items_page_categories
 INNER JOIN d_items ON (d_items_page_categories.itemid=d_items.id)
 INNER JOIN d_items_categories ON (d_items_page_categories.catid=d_items_categories.categoryid)",
 "d_items_page_categories.itemid","(d_items_categories.categoryid_path LIKE '%$categoryid/%' 
 OR `d_items_categories`.categoryid_path LIKE '%/$categoryid' OR d_items_categories.categoryid_path = '$categoryid') 
 AND d_items.active IN ($active) ");

// echo "SELECT d_items_page_categories.itemid FROM d_items_page_categories
// INNER JOIN d_items ON (d_items_page_categories.itemid=d_items.id)
// INNER JOIN d_items_categories ON (d_items_page_categories.catid=d_items_categories.categoryid) WHERE d_items_page_categories.itemid","(d_items_categories.categoryid_path LIKE '%$categoryid/%' 
// OR `d_items_categories`.categoryid_path LIKE '%/$categoryid' OR d_items_categories.categoryid_path = '$categoryid') 
// AND d_items.active IN ($active)";
 
$tmp = execute_multi($sql);
 $lang = DEFAULT_LANG;
 for ($i=0;count($tmp) > $i;$i++)
 {
 	$d_items[] = get_d_items($tmp[$i]['itemid'],$lang);
 }
$smarty->assign("items",$d_items);

}

$smarty->assign("allcategories",get_all_d_items_categories(DEFAULT_LANG));//assigned template variable allcategories
$smarty->assign("MODULE_SETTINGS",$d_items_module['settings']);
$smarty->assign("menu",$d_items_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("catid",$cat);
$smarty->assign("parentid",$parentid);
$smarty->assign("cat",get_d_items_categories($cat,DEFAULT_LANG,0));//assigned template variable cat
$smarty->assign("nav",d_items_cat_nav($cat,DEFAULT_LANG));//assigned template variable a
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("current_category",get_d_items_category($cat,DEFAULT_LANG));//assigned template variable current_cat
$smarty->assign("include_file","modules/d_items/admin/category_items.tpl");
$smarty->display("admin/home.tpl");

?>