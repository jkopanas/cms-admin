<?php
include("../../../manage/init.php");//load from manage!!!!
if ($d_items_module = module_is_active("d_items",1,1)) 
{
	$smarty->assign("latest_items",get_latest_d_items($d_items_module['settings']['latest_items_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",URL."/".$d_items_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$d_items_module['settings']);
	$smarty->assign("allcategories",get_all_d_items_categories(DEFAULT_LANG));//assigned template variable allcategories
}

$smarty->assign("menu",$d_items_module['name']);
$smarty->assign("submenu","main");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/d_items/admin/main.tpl");
$smarty->display("admin/home.tpl");

?>