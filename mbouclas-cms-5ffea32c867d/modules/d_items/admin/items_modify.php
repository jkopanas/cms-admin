<?php
include("../../../manage/init.php");//load from manage!!!!

if ($d_items_module = module_is_active("d_items",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$d_items_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
	
	
########################## UPDATE d_items ##################################################################################
if ($_POST['action'] == "update") 
{
	$required_fields = array('d_items_title');

	//STEP 1: grab POST variables into a formated array
 $posted_data = array();
 foreach ($_POST as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
//Database insertions
//Add the basic d_items details
$categoryid = $_POST['categoryid'];
$extra_categories = $_POST['categoryids'];
$sku = $_POST['sku'];
$active = $_POST['active'];
$orderby = $_POST['orderby'];
$description = $t->formtpa($_POST['short_desc']);
$long_description = $t->formtpa($_POST['long_desc']);
$meta_desc = $t->formtpa($_POST['meta_desc']);
$meta_keywords = $t->formtpa($_POST['meta_keywords']);
$title = $t->formtpa($_POST['d_items_title']);
$orderby = $_POST['orderby'];
//print_r($_POST);//END OF print_r
$query = "sku = '$sku', active = '$active', orderby = $orderby WHERE id = $id";
$sql->db_Update("d_items",$query);
//echo $query."<br><br>";
$query = "title = '$title',description = '$description',full_description = '$long_description', meta_desc = '$meta_desc', 
meta_keywords = '$meta_keywords' WHERE itemid = $id AND code = '".DEFAULT_LANG."'";
//echo $query."<br><br>";
$sql->db_Update("d_items_lng",$query);

//SECONDARY UPDATES

//d_items CATEGORIES
$sql->db_Select("d_items_page_categories","catid","itemid = $id AND catid = $categoryid AND main = 'Y'");

if ($sql->db_Rows() > 0) //UPDATE
{
	$sql->db_Update("d_items_page_categories","catid = $categoryid WHERE itemid = $id AND main = 'Y'");
}
else 
{
	$sql->db_Insert("d_items_page_categories","'$categoryid','$itemid','Y'");
}


//Add main category
$sql->db_Update("d_items_page_categories","catid = '$categoryid' WHERE main = 'Y' AND itemid = '$id'");
//See if there are any extra categories to insert



if ($extra_categories) 
{  
	//first drop the old ones
$sql->db_Delete("d_items_page_categories","itemid = '$id' AND main = 'N'");
 for ($i=0;count($extra_categories) > $i;$i++)
 {
 	if ($extra_categories[$i] != "")
 	{
 		$sql->db_Insert("d_items_page_categories","'".$extra_categories[$i]."','$id','N'");
 	}
 	
 }//END OF FOR
}//END OF IF

//add extra fields
upd_d_items_extra_field_values($_POST,$id,DEFAULT_LANG);
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
upd_d_items_extra_field_values($_POST,$id,$countries[$i]['code']);
}//END OF FOR
}//END OF IF

//UPDATE TAGS
$tags = construct_tags_array($_POST['tags'],",");
//Clear old tags
$sql->db_Delete("d_items_tags","itemid = $id");
//insert new tags
for ($i=0;count($tags) > $i;$i++)
{
	$tag = $tags[$i];
	$sql->db_Insert("d_items_tags","'','$id','$tag'");
}

############# CHECK OUT FOR ESHOP MODULE #######################################
	if ($eshop_module = module_is_active("eshop",1,0)) 
	{
		$price = $_POST['price'];
		$list_price = $_POST['list_price'];
		
$sql->db_Select("eshop_item_details","id","itemid = $id");
if ($sql->db_Rows() > 0) 
{
	$sql->db_Update('eshop_item_details',"price = '$price',list_price = '$list_price' WHERE itemid = $id");
}
else 
{
		$sql->db_Insert("eshop_item_details","'','$id','$price','$list_price','$quantity','$membership','$distribution','$weight',
		'$avail','$sales_stats','$views_stats','$del_stats','$shipping_freight','$free_shipping','$discount_avail','$min_amount',
		'$low_avail_limit','$free_tax','item'");
}
		
	}
	
//Locations STUFF
	if (module_is_active($locations_module = "locations",1,0)) 
	{
		$locid = $_POST['locid'];
		$item_loc = get_item_location($id,"d_items_page_locations","catid");

		if ($item_loc) 
		{
			$sql->db_Update("d_items_page_locations","catid = '$locid' WHERE itemid ='$id'");
		}
		else 
		{
			$sql->db_Insert("d_items_page_locations","'$locid','$id'");
		}
		
	}//END OF MODULE

$smarty->assign("updated",1);
 }//END OF NO ERRORS
 $smarty->assign("error_list",$error_list);//assigned template variable error_list<BR>
}

	
################################################## LOAD d_items ############################################################
	$d_items = get_d_items($id,DEFAULT_LANG,0,1);

	
	if (!empty($d_items)) 
	{  
	$sql->db_Select("d_items_page_categories","catid","itemid = $id AND main = 'N'");
	$all_d_items_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_d_items_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_d_items_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_d_items",get_latest_d_items($d_items_module['settings']['latest_d_items'],DEFAULT_LANG,"no",$d_items['catid']));
	$extra_fields = get_extra_fields_d_items("Y",DEFAULT_LANG,$id,$d_items['catid']);
	$smarty->assign("extra_fields",$extra_fields);
	
	$smarty->assign("id",$id);
	$smarty->assign("tags",get_d_items_tags($id,","));
	$smarty->assign("nav",d_items_cat_nav($d_items['catid'],DEFAULT_LANG));

	
	
			//maps for real estate
	if ($maps_module = module_is_active("maps",1,1)) 
	{
//		echo "sdsd";
		//Maps are active
		$smarty->assign("maps_module",$maps_module);
	}

// create a new instance of wysiwygPro:
$editor = new wysiwygPro();

// configure your directories:

// Note: folders used by the file browser must be made writable otherwise uploading and file editing will not work.

// full directory path for your images folder:
$editor->imageDir = $_SERVER['DOCUMENT_ROOT'].'/images/';
// URL of your images folder:
$editor->imageURL = '/images';

// full directory path of your documents folder for storing PDF and Word files etc:
$editor->documentDir = $_SERVER['DOCUMENT_ROOT'].'/docs/';
// url of your documents folder:
$editor->documentURL = '/docs/';

// full directory path of your media folder for storing video files:
$editor->mediaDir = $_SERVER['DOCUMENT_ROOT'].'/docs/';
// url of your media folder:
$editor->mediaURL = '/docs';

// File editing permissions:
$editor->editImages = true;
$editor->renameFiles = true;
$editor->renameFolders = true;
$editor->deleteFiles = true;
$editor->deleteFolders = true;
$editor->copyFiles = true;
$editor->copyFolders = true;
$editor->moveFiles = true;
$editor->moveFolders = true;
$editor->upload = true;
$editor->overwrite = true;
$editor->createFolders = true;

$smarty->assign("editor_js",$editor->fetchFileBrowserJS('OpenFileBrowser'));

	$smarty->assign("item",$d_items);
	$smarty->assign("d_items_module",$d_items_module);
	$smarty->assign("action","update");
	}//END OF d_items found
	else 
	{  
		header("Location: search.php"); 
		exit();
	}//END OF ELSE

################################################ END OF LOAD d_items #########################################
}

$smarty->assign("image_types",get_image_types(3));

$smarty->assign("USE_AJAX",$d_items_module['folder']."/admin/ajax_functions.tpl");
$smarty->assign("menu",$d_items_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","modify");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/d_items/admin/items_modify.tpl");
$smarty->display("admin/home.tpl");



?>