<?php
include("../../../manage/init.php");//load from manage!!!!

if ($d_items_module = module_is_active("d_items",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$d_items_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
	
	

	
################################################## LOAD d_items ############################################################
	$d_items = get_d_items($id,DEFAULT_LANG);

	if (!empty($d_items)) 
	{  
	$sql->db_Select("d_items_page_categories","catid","itemid = $id");
	$all_d_items_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_d_items_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_d_items_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_d_items",get_latest_d_items($d_items_module['settings']['latest_d_items'],DEFAULT_LANG,"no",$d_items['catid']));
	$smarty->assign("extra_fields",get_d_items_extra_fields("Y",DEFAULT_LANG,$id));

	$smarty->assign("id",$id);
	$smarty->assign("tags",get_d_items_tags($id,","));
	$smarty->assign("nav",d_items_cat_nav($d_items['catid'],DEFAULT_LANG));

		if ($manufacturers_module = module_is_active("manufacturers",1,1)) 
	{
		$smarty->assign("manufacturers_list",get_manufacturers(DEFAULT_LANG));
		$smarty->assign("manufacturers_module",$manufacturers_module);
		$item['mid'] = get_item_manufacturer($id);
	}
	############# END OF  MANUFACTURERS MODULE #####################################
	
	############# CHECK OUT FOR ESHOP MODULE #######################################
	
	if ($eshop_module = module_is_active("eshop",1,1)) 
	{
		$d_items['details'] = eshop_item_details($id);
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
		
	}

		if ($manufacturers_module = module_is_active("manufacturers",1,1)) 
	{
		$smarty->assign("manufacturers_list",get_manufacturers(DEFAULT_LANG));
		$smarty->assign("manufacturers_module",$manufacturers_module);
		$item['mid'] = get_item_manufacturer($id);
	}
	############# END OF  MANUFACTURERS MODULE #####################################
	
	############# CHECK OUT FOR ESHOP MODULE #######################################
	
	if ($eshop_module = module_is_active("eshop",1,1)) 
	{
		$d_items['details'] = eshop_item_details($id);
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
		
	}
	
			//maps for real estate
	if ($maps_module = module_is_active("maps",1,1)) 
	{
		if ($_GET['mapid']) 
		{
			$smarty->assign("map",get_maps($_GET['mapid'],"0,1"));
			$smarty->assign("mapid",$_GET['mapid']);
			$smarty->assign("point",get_map_items(0,$id,$_GET['mapid'],'item'));
		}
		
		//Maps are active
		$smarty->assign("maps",get_maps(0,'0,1',"item"));
		$smarty->assign("maps_module",$maps_module);
		
	
	}

	$smarty->assign("d_items",$d_items);
	$smarty->assign("d_items_module",$d_items_module);
	$smarty->assign("action","update");
	}//END OF d_items found
	else 
	{  
		header("Location: search.php"); 
		exit();
	}//END OF ELSE
################################################ END OF LOAD d_items #########################################

			####################### LOAD AJAX  ###########################
			include($_SERVER['DOCUMENT_ROOT']."/".$d_items_module['folder']."/admin/ajax_functions.php");
			###################### END OF AJAX #######################################	
			
}



$smarty->assign("MAPS",1);
$smarty->assign("related",get_related_d_items($id,DEFAULT_LANG));
$smarty->assign("USE_AJAX","modules/d_items/admin/d_items_ajax.tpl");
$smarty->assign("menu",$d_items_module['name']);
$smarty->assign("submenu","maps");//USED ON SUBMENUS
$smarty->assign("section","maps");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/d_items/admin/d_items_modify_map.tpl");
$smarty->display("admin/home.tpl");

?>