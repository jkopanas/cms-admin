<?php
include("../../../manage/init.php");//load from manage!!!!

if ($d_items_module = module_is_active("d_items",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$d_items_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

	if ($_POST['action'] == "modify") 
	{
		foreach ($_POST as $k => $v) 
		{
			$sql->db_Update("eshop_item_details","$k = '$v' WHERE itemid = $id");
		}
	}
	
################################################## LOAD item ############################################################
	$item = get_d_items($id,DEFAULT_LANG);

	if (!empty($item)) 
	{  
	$sql->db_Select("item_categories","catid","itemid = $id AND main = 'N'");
	$all_item_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_item_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_d_items_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_d_items",get_latest_d_items($d_items_module['settings']['latest_d_items'],DEFAULT_LANG,"no",$item['catid']));
	$smarty->assign("extra_fields",get_d_items_extra_fields("Y",DEFAULT_LANG,$id));

	$smarty->assign("id",$id);
	$smarty->assign("nav",d_items_cat_nav($item['catid'],DEFAULT_LANG));
	
	if ($eshop_module = module_is_active("eshop",1,1)) 
	{
		$item['details'] = eshop_item_details($id);
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
		
	}
	

	$smarty->assign("d_items",$item);
	$smarty->assign("d_items_module",$d_items_module);
	$smarty->assign("action","update");
	$smarty->assign("image_categories",d_items_images_categories());
	################################################ END OF LOAD item #########################################
	}//END OF item

}

$smarty->assign("menu",$d_items_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","eshop_options");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/eshop/admin/item_details.tpl");
$smarty->display("admin/home.tpl");

?>