<?php
include("../../../manage/init.php");//load from manage!!!!

if ($d_items_module = module_is_active("d_items",1,1)) 
{
	$module_path = URL."/".$d_items_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",$module_path);
$t = new textparse();
$page = $_POST['page'];
################################################ MASS ACTIVATE/DEACTIVATE ###################################
if ($_POST['mode'] == "deactivate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("d_items","active = 0 WHERE id = $key");				
			}
		}//END OF WHILE

	if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
	
}

if ($_POST['mode'] == "activate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("d_items","active = 1 WHERE id = $key");				
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
}
################################################ END MASS ACTIVATE/DEACTIVATE ###############################

################################################ MASS DELETE ###############################
if ($_POST['mode'] == "delete") 
{
	
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			if ($val == 1)//eliminates the check all/none box 
			{
	$d_items_details = get_d_items($id,DEFAULT_LANG);
	$categoryid = $d_items_details['catid'];  
//delete the d_items
$sql->db_Delete("d_items","id = $id"); 
//delete translations
$sql->db_Delete("d_items_lng","itemid = $id"); 
//delete d_items links
$sql->db_Delete("d_items_links","d_items_source = $id"); 
$sql->db_Delete("d_items_links","d_items_dest = $id"); 
//delete from any category
$sql->db_Delete("d_items_page_categories","itemid = $id"); 
//delete from any category
$sql->db_Delete("d_items_locations","itemid = $id"); 
//delete from images
$sql->db_Delete("d_items_images","itemid = $id"); 
$sql->db_Delete("d_items_images_aditional","itemid = $id"); 
//delete from featured d_items
$sql->db_Delete("featured_d_items","itemid = $id"); 
//delete from extra fields
$sql->db_Delete("d_items_extra_field_values","itemid = $id"); 
//delete from classes
$sql->db_Delete("d_items_class_d_items","itemid = $id"); 
//delete from bookmars
$sql->db_Delete("bookmars","itemid = $id");
//Delete from feeds
$sql->db_Delete("feeds","itemid = $id");
//Subtract from d_items count
$sql->db_Update("d_items_categories","d_items_count = d_items_count-1 WHERE categoryid = '$categoryid'");
//delete recipe features
//ALL DONE			
$d_items_dir = $_SERVER['DOCUMENT_ROOT'].$d_items_module['settings']['images_path']."/category_$categoryid/item_$id/";
//echo $d_items_dir."<Br>";
recursive_remove_directory($d_items_dir);
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
			exit();
		}
		else {
				header("Location:".$module_path."/$page");
				exit();
			}	
}

################################################ END MASS DELETE ###############################

############################################### MASS FIRST ACTIVATION ##############################3
if ($_POST['mode'] == "first_activate") 
{
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "d_items_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("d_items_lng","title = '$val' WHERE itemid = $code");
							
						}
						elseif ($field == "categoryid")
						{
							$sql->db_Update("d_items_page_categories","catid = $val WHERE itemid = $code AND main = 'Y'");
						}
						else 
						{
							$sql->db_Update("d_items","$field = '$val' WHERE id = $code");
							
						}
						
					}
		}
	
	foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
					$sql->db_Update("d_items","active = 0 WHERE id = $code");
			}
		}
		//clear up
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS EDIT ##############################3
if ($_POST['mode'] == "mass_edit") 
{
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "d_items_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("d_items_lng","title = '$val' WHERE itemid = $code");
							
						}
						elseif ($field == "categoryid")
						{
								//item CATEGORIES
								$sql->db_Select("d_items_page_categories","catid","itemid = $code AND main = 'Y'");

								if ($sql->db_Rows() > 0) //UPDATE
								{
									$sql->db_Update("d_items_page_categories","catid = $val WHERE itemid = $code AND main = 'Y'");
								}
								else 
								{
									$sql->db_Insert("d_items_page_categories","'$val','$code','Y'");
								}
						}
						else 
						{
							$sql->db_Update("d_items","$field = '$val' WHERE id = $code");
							
						}
						
					}
					
					if (strstr($key,"eshop_")) 
					{
						list($vh,$ch)=split("eshop_",$key);
						{
							
							list($ph,$oh)=split("-",$ch);
//							echo "$ph - $oh --- $val<br>";
							$sql->db_Select("eshop_item_details","id","itemid = $oh");
							if ($sql->db_Rows() > 0) 
							{
							$sql->db_Update('eshop_item_details',"$ph = '$val' WHERE itemid = $oh");
							}
							else 
							{
									$sql->db_Insert("eshop_item_details","'','$oh','$price','$list_price','$quantity','$membership','$distribution','$weight','$avail','$sales_stats','$views_stats','$del_stats','$shipping_freight','$free_shipping','$discount_avail','$min_amount','$low_avail_limit','$free_tax','item'");
									$sql->db_Update('eshop_item_details',"$ph = '$val' WHERE itemid = $oh");
							}
					}
		}//ESHOP
		
					if (strstr($key,"locations_")) 
					{
						
						list($vh,$ch)=split("locations_",$key);
						{
							
						list($ph,$oh)=split("-",$ch);
						$sql->db_Select("d_items_locations","itemid","itemid = $oh");
//						echo "SELECT itemid FROM d_items_locations WHERE itemid = $oh<br>";
						if ($sql->db_Rows()) 
						{
							$sql->db_Update("d_items_locations","id = '$val' WHERE itemid ='$oh'");
						}
						else 
						{
							$sql->db_Insert("d_items_locations","'$val','$oh'");
						}
						}
					}//LOCATIONS
		}
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS DELETE IMAGES ##############################3
if ($_POST['mode'] == "delete_images") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
						
				list($field,$code)=split("-",$key);
				$sql->db_Select("d_items_images","image,thumb,itemid","id = $key");
 				$a = execute_single($sql);
				$sql->db_Select("d_items_images_aditional","*","imageid = $key");
				$b = execute_multi($sql);
				foreach ($b as $v)
				{
					@unlink($v['image_path']); 
				}
				
 				
 				$sql->db_Delete("d_items_images","id = $key");
 				$sql->db_Delete("d_items_images_aditional","imageid = $key");
//echo $key."<br>";
					}
		}
	
		header("Location:".$module_path."/$page");
		exit();
}
############################################### MASS ACTIVATE IMAGES ##############################3
if ($_POST['mode'] == "activate_all_images") 
{
	$id = $_POST['id'];
	$sql->db_Update("d_items_images","available = 1 WHERE itemid = $id AND type = ".$_POST['type']);
//	echo "available = 1 WHERE itemid = $id AND type = ".$_POST['type'];
	header("Location:".$module_path."/$page");
	exit();
}

}//END OF LOAD MODULE
?>