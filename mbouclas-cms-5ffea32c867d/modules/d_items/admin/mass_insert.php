<?php
include("../../../manage/init.php");//load from manage!!!!

if ($d_items_module = module_is_active("d_items",1,1)) 
{
	$cat = $_GET['cat'];

	$smarty->assign("MODULE_FOLDER",URL."/".$d_items_module['folder']."/admin");
	$smarty->assign("action",$_GET['action']);
		$sql->db_Select("d_items","COUNT(id) AS counter","active = 2");
		$smarty->assign("inactive",execute_single($sql));
			
	if ($_GET['cat']) 
	{
		$smarty->assign("cat",$cat);
		$smarty->assign("category",get_d_items_category($cat,DEFAULT_LANG,1));
		$smarty->assign("nav",d_items_cat_nav($cat,DEFAULT_LANG_ADMIN));//assigned template variable a
	}
	else 
	{		
		$smarty->assign("allcategories",get_all_d_items_categories(DEFAULT_LANG));//assigned template variable allcategories
	}
	
	
	if ($_GET['action']) 
	{
		//Check for mass uploaded d_items
		$sql->db_Select("d_items","id","active = 2");
		if ($sql->db_Rows() > 0) 
		{
			$tmp_uploads = execute_multi($sql);
			for ($i=0;count($tmp_uploads) > $i;$i++)
			{
				$d_items[$i] = get_d_items($tmp_uploads[$i]['id'],DEFAULT_LANG);
			}
			$smarty->assign("items",$d_items);
		}//END OF UPLOADED d_items FOUND
	}//END OF REVIEW UPLOADED d_items
}//END OF MODULE




$smarty->assign("menu",$d_items_module['name']);
$smarty->assign("submenu","mass_insert");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/d_items/admin/mass_insert.tpl");
$smarty->display("admin/home.tpl");

?>