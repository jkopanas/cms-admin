<?php
class deals {
	public $sql;
	public $tables;
    
	public function deals()
	{
		global $sql;
		$this->sql = $sql;
        $this->tables = array();
        $this->tables['deals_items'] = 'deals_items';
        $this->tables['deals_categories'] = 'deals_items_current';
        $this->tables['deals_page_categories'] = 'deals_page_categories';
        
	}//END METHOD
	
	public function getDeal($id,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $fields['settings'] : "*";
		$sql->db_Select($this->tables['deals_items'],$fields,"id='$id'");
		$data = execute_single($sql);
        
        if($settings['getShop']==1)
        {
            $data['shop']=$this->getShop($data['shopid']);
        }
        if($settings['getCategory']==1)
        {
            $data['category']=$this->getCategory($data['categoryid']);
        }
        return $data;
	}//END METHOD
	
    public function getShop($id,$settings=0)
    {
        global $sql;
		$fields = ($settings['fields']) ? $fields['settings'] : "*";
		$sql->db_Select('deals_shops',$fields,"id='$id'");
		$data = execute_single($sql);
        return $data;
    }
    
    public function getCategory($id,$settings=0)
    {
        global $sql;
		$fields = ($settings['fields']) ? $fields['settings'] : "*";
		$sql->db_Select('deals_categories',$fields,"categoryid='$id'");
		$data = execute_single($sql);
        return $data;
    }
    
    
	public function searchDeals($settings)
	{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
	$way = ($settings['way']) ? "  ".$settings['way'] : "";
	$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
	$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
	if (is_array($settings['searchFields'])) {
		foreach ($settings['searchFields'] as $k => $v) {
			$q[] = "$k = '$v'";
		}
	}//END SETUP SEARCH FIELDS
	if (is_array($settings['searchFieldsIN'])) {
		foreach ($settings['searchFieldsIN'] as $k => $v) {
			if (is_array($v)) {
				$v = implode(",",$v);
			}
			$q[] = "$k  IN ($v)";
		}
	}//END SETUP SEARCH FIELDS WITH IN
	if (is_array($settings['searchFieldsINMulti'])) {
		foreach ($settings['searchFieldsINMulti'] as $v) {
			foreach ($v as $k=>$b) {
				$q[] = "$k  IN ($b)";
			}
			
		}
	}//END SETUP SEARCH FIELDS WITH IN
    if (is_array($settings['searchFieldsLike'])) {
    	foreach ($settings['searchFieldsLike'] as $k => $v) {
   			$q[] = "$k LIKE '%$v%'";
    	}
    }//END SETUP SEARCH FIELDS WITH LIKE
    
    if (is_array($settings['searchFieldsNotLike'])) {
    	foreach ($settings['searchFieldsNotLike'] as $k => $v) {
   			$q[] = "$k NOT LIKE '%$v%'";
    	}
    }//END SETUP SEARCH FIELDS WITH NOT LIKE  

    $filters = $settings["filters"];	

		if (isset($filters['missing'])) 
		{
			$q[] = $filters['missing']." IS NULL ";//UNTESTED
		}
		if (isset($filters['custom_field'])) 
		{
			$q[] = $filters['custom_field']." LIKE '%".$filters['custom_value']."%'";
		}
		if ($filters['user_class_deny']) 
		{
			$tmp = explode(",",$filters['user_class_deny']);
			if (count($tmp) == 1) 
			{
				$q[] = "user_class != '".$filters['user_class_deny']."'";
			}
			else 
			{
				foreach ($tmp as $v)
				{
					$q[] = "user_class != ".$v."";	
				}
			}	
		}
		if ($q) {
			$query = implode(" AND ",$q);
		}
				if (!$query) 
				{
					$query_mode='no_where';
				}
				else {
					$query_mode = 'default';
				}

			if ($settings['return'] == 'multi') //Return all results, NO PAGINATION
			{
				$sql->db_Select($this->tables['deals_items'],$fields,"$query $orderby $way",$query_mode);
				if ($settings['debug']) {
					echo "SELECT $fields FROM ".$this->tables['deals_items']." WHERE $query $orderby $way<br>";
				}
				if ($sql->db_Rows()) 
				{
					$res = execute_multi($sql);
					if ($sql->db_Rows()) {
					foreach ($res as $k=> $v) {
//						$res[] = $this->userDetails($v['id'],$settings);
					}
					}
					return $res;
				}
				
			}//END ALL RESULTS
			elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
			{
				$sql->db_Select($this->tables['deals_items'],'count(id) as total',"$query",$query_mode);//GET THE TOTAL COUNT
				
					if ($settings['debug']) 
					{
						echo "SELECT count(id) as total FROM ".$this->tables['deals_items']." WHERE $query $orderby $way<br>";
					}
				if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
				{
					$a = execute_single($sql);
					$total = $a['total'];
					$current_page = ($settings['page']) ? $settings['page'] : 1;
					$results_per_page =  $settings['results_per_page'];
					if (isset($settings['start'])) 
					{
						$start = $settings['start'];
					}
					else {
						$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
					}
					$limit = "LIMIT $start,".$results_per_page;
					$sql->db_Select($this->tables['deals_items'],"$fields","$query $orderby $way $limit",$query_mode);
					if ($settings['debug']) {
						echo "<br>SELECT $fields FROM ".$this->tables['deals_items']." WHERE $query $orderby $way $limit";
					}
					$tmp = execute_multi($sql,1);
					paginate_results($current_page,$results_per_page,$total);
                    if($settings['countSubDeals'])
                    {
                        
                        //$ids=implode(',',$ids);
                        
                        $sql->db_Select($this->tables['deals_items'],'parent,count(id) as count',"parent!='0' GROUP BY parent");
                        $count=execute_multi($sql);
                        
                        foreach($count as $v)
                        {
                            $temp[$v['parent']]=$v['count'];
                        }
                        
                        foreach ($tmp as $k=>$v) {
                            
							$tmp[$k]['subdeals'] = $temp[$v['id']];
						}
                    }
					if ($tmp) {
						foreach ($tmp as $v) {
							$tmp['settings'] = json_decode($tmp['settings']);
                            $ids[] = $tmp['id'];
						}
					}
                    
                    
					$res = $tmp;
					return $res;
					
				}//END FOUND RESULTS
			}//END PAGINATION
		elseif ($settings['return'] == 'count') // RETURN COUNT RESULTS
		{
			$sql->db_Select($this->tables['deals_items'],"count(".$this->tables['deals_items'].".id) as total","$query  $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT count(".$this->tables['deals_items'].".id) as total FROM ".$this->tables['deals_items']." WHERE $query  $limit<br>";
			}
			$res = execute_single($sql);
			return $res['total'];
		}//END COUNT
		elseif ($settings['return'] == 'single')
		{
				$sql->db_Select($this->tables['deals_items'],$fields,"$query $orderby $way $limit",$query_mode);
				if ($settings['debug']) {
					echo "SELECT $fields FROM ".$this->tables['deals_items']." WHERE $query $orderby $way $limit<br>";
				}
				if ($sql->db_Rows()) 
				{
					$tmp = execute_single($sql);
					foreach ($tmp as $v) {
						$res = $this->userDetails($v['id'],$settings);
					}
					return $res;
				}
		}//END SINGLE
	}//END METHOD
	
	public function readHistory()
	{
		
	}//END METHOD
	
	public function dealCategories($settings=0)
	{
		global $sql;
		
		$fields = (!$settings['fields']) ? "*" : $settings['fields'];
		$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
		$active = $settings['active'];
		$table = $settings['table_page'];
		$table_cat = $settings['table'];
		if ($settings['count_items']) {
			$fields .= ",@categoryid := categoryid";
			foreach ($settings['count_items'] as $k=>$v)
			{
				$counters[] = "(SELECT count($k) FROM deals_items as c WHERE categoryid = @categoryid) as $v";
			}
			$fields .= ",".implode(",",$counters);
		}//END COUNTERS
		$sql->db_Select('deals_categories',$fields,"ORDER BY orderby $limit_q",$mode="no_where");
		if ($settings['debug']) {
			echo "SELECT $fields FROM deals_categories ORDER BY orderby $limit_q";
		}
		if ($sql->db_Rows()) {
			$categories = execute_multi($sql,1);
		
		 for ($i=0;count($categories) > $i;$i++)
		 {
			 $categoryid = $categories[$i]['categoryid'];
			
			 //get settings 
			 if ($settings['settings']) {
			 	 $categories[$i]['settings'] = json_decode($categories[$i]['settings']);
			 }
			 
		 }//END OF FOR
		 return $categories;
		}//END ROWS
	}//END METHOD
	
	public function dealCategory($categoryid,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $fields['settings'] : "*";
		if ($settings['count_items']) {
			foreach ($settings['count_items'] as $k=>$v)
			{
				$counters[] = "(SELECT count($k) FROM deals_items as c WHERE categoryid = $categoryid) as $v";
			}
			$fields .= ",".implode(",",$counters);
		}//END COUNTERS
		if ($settings['debug']) {
			echo "SELECT $fields FROM deals_categories WHERE categoryid = $categoryid<br>";
		}
		$sql->db_Select('deals_categories',$fields,'categoryid = '.$categoryid);
		if ($sql->db_Rows()) {
			$cat = execute_single($sql);
			$cat['settings'] = json_decode($cat['settings']);
			return $cat;
		}//END ROWS
	}//END METHOD
	
	public function setupSearchData($data)
	{
		$posted_data = $data['data'];
		$secondaryData = $data['secondaryData'];
		$searchFilters = $data['secondaryData']['filters'];

		$secondaryData['searchFieldsLike'] = $data['likeData'];
		if ($secondaryData['custom_field'] AND $secondaryData['custom_value']) {
			$searchFilters['custom_field'] = $secondaryData['custom_field'];
			$searchFilters['custom_value'] = $secondaryData['custom_value'];
		}
		if ($secondaryData['missing']) {
			$searchFilters['missing'] = $secondaryData['missing'];
		}

		if ($posted_data) 
		{
		foreach ($posted_data as $k=>$v) {
			if (is_array($v)) {
				$secondaryData['searchFieldsIN'][$k] = $v;
				unset($posted_data[$k]);
			}
		}
		}
		
		if ($secondaryData['dateFrom']) {
			$tmp = explode("/",$secondaryData['dateFrom']);
			$secondaryData['searchFieldsGT']['date_added'] = mktime(23,59,0,$tmp[1],$tmp[0],$tmp[2]);
		}
		
		if ($secondaryData['dateTo']) {
			$tmp = explode("/",$secondaryData['dateTo']);
			$secondaryData['searchFieldsLT']['date_added'] = mktime(23,59,0,$tmp[1],$tmp[0],$tmp[2]);
		}

		$secondaryData['searchFields'] = $posted_data;
		$secondaryData['debug'] = ($secondaryData['debug']) ? $secondaryData['debug'] : 0;
		$secondaryData['page'] = ($secondaryData['page']) ? $secondaryData['page'] : 1;
		$secondaryData['filters'] = $searchFilters;
		$secondaryData['debug'] = ($secondaryData['debug']) ? $secondaryData['debug'] : 0;
		$secondaryData['return'] = ($secondaryData['return']) ? $secondaryData['return'] : 'paginated';
		$secondaryData['getTotalAmount'] = ($secondaryData['getTotalAmount']) ? $secondaryData['getTotalAmount'] : 0;
		$secondaryData['getDetails'] = ($secondaryData['getDetails']) ? $secondaryData['getDetails'] : 0;
	
		return $secondaryData;
	}//END FUNCTION
	
	public function dealShops($id=0,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		if ($settings['count_items']) {
			if ($settings['count_items_ids']) {
				$addQ = " AND id IN (".$settings['count_items_ids'].")";
			}
			foreach ($settings['count_items'] as $k=>$v)
			{
				$counters[] = "@shopid :=id,(SELECT count($k) as c FROM deals_items WHERE shopid = @shopid $addQ) as $v";
			}
			$fields .= ",".implode(",",$counters);
		}//END COUNTERS
		
		if ($id) {
			if (is_array($id)) {
				$q[] = "id IN (".implode(",",$id).")";
			}
			else {
				$q[] = "id = $id";
			}
			
		}

		
		if ($q) {
			$query = implode(" AND ",$q);
		}
		if ($settings['groupBy']) {
			$query .= " GROUP BY ".$settings['groupBy'];
		}
		$mode = (!$query) ? 'default' : "no_where";
		$sql->db_Select("deals_shops",$fields,$query,$mode);
		if ($settings['debug']) {
			echo "SELECT $fields FROM deals_shops WHERE $query<br><br>";
			echo mysql_error();
		}
		if ($sql->db_Rows()) {
			if ($id) {
				$res = execute_single($sql);
			}
			else {
				$res = execute_multi($sql);	
			}
			
		}
		return $res;
		
	}//END METHOD
	
	public function recalculateItems($fields,$table,$joinField,$ids)
	{
		global $sql;
		
		$sql->db_Select("deals_items INNER JOIN $table ON (deals_items.shopid=$table.$joinField)","count(deals_items.id) as count,$fields","deals_items.id IN ($ids) GROUP BY $table.$joinField");
//		echo "SELECT count(deals_items.id) as count,$fields FROM deals_items INNER JOIN $table ON (deals_items.shopid=$table.$joinField) WHERE deals_items.id IN ($ids) GROUP BY $table.$joinField";
		if ($sql->db_Rows()) {
			$res = execute_multi($sql);
			return $res;
		}
	}//END METHOD
	
	public function groupLocations($settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		if ($settings['count_items']) {
			if ($settings['count_items_ids']) {
				$addQ = " AND id IN (".$settings['count_items_ids'].")";
			}
			foreach ($settings['count_items'] as $k=>$v)
			{
				$counters[] = "@shopid :=id,(SELECT count($k) as c FROM ".$this->tables['deals_items']." WHERE shopid = @shopid $addQ) as $v";
			}
			$fields .= ",".implode(",",$counters);
		}//END COUNTERS
		
		$sql->db_Select($this->tables['deals_items'],$fields,"GROUP BY locationText",$mode="no_where");
		if ($sql->db_Rows()) {
			$res = execute_multi($sql);
			
		}//END ROWS
		return $res;
	}//END METHOD
    
    public function createDealPri($settings)
    {
        global $sql;
        
        $fields=array();
        $values=array();
        
        
        $donothing=array('zoomLevel','category','alias','MapTypeId','mapItem','commonCategoryTree-','FormAction','main-cat','parent_catid');
        $donothing = array_flip($donothing);
        
        foreach($settings as $k=>$v)
        {
            
            if(($k=="cat")&&(is_array($v)))
            {
                $categories = $v;
            }
            elseif($k=="startDate")
            {
                $fields[]=$k;
                $values[]=strtotime($v);
            }
            elseif($k=="endDate")
            {
                $fields[]=$k;
                $values[]=strtotime($v);
            }
            elseif($k=="geocoderAddress")
            {
                $fields[]='locationText';
                $values[]=$v;
            }
            elseif($k=="description")
            {
                $fields[]=$k;
                $values[]=addslashes($v);
            }
            elseif(array_key_exists($k,$donothing))
            {
                //do nothing
            }
            else
            {
                $fields[]=$k;
                $values[]=$v;
            }
            
        }
        
        $fields = implode(",",$fields);
        $values = "'".implode("','",$values)."'";
        
        //create primary deal
        $sql->db_Insert('deals_items_primary'.' ('.$fields.')',$values);
        $lastid = $sql->last_insert_id;
        
        
        //actions after insert
        
        
        if(is_array($categories))
        {
            $sql->db_Delete($this->tables['deals_page_categories'],"itemid = $lastid");
            foreach ($categories as $val)
            {
                
            	$main = ($val == $settings['main-cat']) ? 1 : 0;
            	$sql->db_Insert($this->tables['deals_page_categories'],"'$val','$lastid','$main'");	
                //echo "INSERT INTO ".'deals_page_categories'." VALUES ("."'$val','$lastid','$main'".")".'<br/>';
            }
        }
        
        //echo "INSERT INTO ".'deals_items_primary'.' ('.$fields.')'." VALUES (".$values.")";
        //$this->archiveDeal($id);
    }
    
    public function getCategories($settings=0)
    {
        global $sql;
        
        //$sql->db_Select("deals_categories_current","*","","no_where");
        
        
        
        
        if ($settings['count_items']) {
            $table = "deals_categories_current INNER JOIN deals_page_categories ON deals_categories_current.categoryid = deals_page_categories.catid";
            $fields = "deals_categories_current.*,COUNT(deals_page_categories.itemid) as num_items";
            $group = "GROUP BY deals_page_categories.catid";
            $sql->db_Select($table,$fields,$group,"no_where");
            
            $categories = execute_multi($sql);
		}//END COUNTERS
        else
        {
            $sql->db_Select("deals_categories_current","*","","no_where");
            $categories = execute_multi($sql);
        }
        
        return $categories;
    }
    
    public function getDealsForCategories($settings)
    {
        global $sql;
        
        
        if( is_int($settings['category']))
        {
            $where = "catid='".$settings['category']."'";
        }
        else if(is_array($settings['category']))
        {
            $categories = implode(",",$settings['category']);
            $where = "catid IN ($categories)";
        }
        
        if($settings['extra_args'])
        {
            $where= $where." ".$settings['extra_args'];
        }
        if($settings['group'])
        {
            $where= $where." GROUP BY ".$settings['group'];
        }
        
        $sql->db_Select("deals_page_categories","itemid as id",$where);
        //echo "SELECT itemid as id FROM deals_page_categories WHERE ".$where;
        
        $items = execute_multi($sql);
        foreach($items as $v)
        {
            $data[]=$v['id'];
        }
        if($settings['fullItems'])
        {
            $data = $this->searchDeals(array('searchFieldsIN'=>array('id'=>$items)));
            $items = $data;
        }
        
        return $data;
    }
    
    public function archiveDeal($dealid)
    {
        global $sql;
        
        $sql->db_Update('deals_items',"archived='1' WHERE id='$dealid'");
        
    }
	
}//END CLASS

?>