<?php
include("../../../manage/init.php");//load from manage!!!!


$current_module = $loaded_modules['deals'];
$deals = new deals();

if($_POST)
{
    $deals->createDealPri($_POST);
}

$id = $_GET['id'];
$deal = $deals->getDeal($id,array('getShop'=>0,'getCategory'=>0));


if ($loaded_modules['maps']) {
    $map = new maps(array('module'=>$current_module));
    $mapItem = $map->getMapItem($id,array('debug'=>0,'getItem'=>0));
    if ($mapItem)
    {
     	$mapItem['item'] = $deal;
    	$smarty->assign('mapItemEncoded',json_encode($mapItem));
    	$smarty->assign("mapItem",$mapItem);
    }

}//END MAPS

$all_categories = $Content->AllItemsTreeCategories();

$smarty->assign("allcategories",$all_categories);

$smarty->assign('item',$deal);
$smarty->assign("include_file","modules/deals/admin/deal_form.tpl");
$smarty->display("admin/home.tpl");

?>
