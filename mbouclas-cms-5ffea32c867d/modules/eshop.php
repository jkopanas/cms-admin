<?php
/**
 * The Class of the Module Eshop
 *
 */
class eshop {

	public $tables;
	public $module;
	public $ordersTotal;
	public $tax;
	public $lastAffectedID;
	public $paymentMethodsArr;
	public $shippingMethodsArr;

	/**
	 * PHP's Magic-Constructor
	 */
	function __construct()
	{
		$this->init();
	}

	/**
	 * Constructor of the Eshop module-class
	 *
	 */
	function init()
	{
		global $loaded_modules;
		$this->tables['cart'] = array('table'=>'eshop_cart','key'=>'id','userKey'=>'uid');
		$this->tables['cc_rates'] = array('table'=>'eshop_cc_rates','key'=>'id');
		$this->tables['cc_rates_exclussions'] = array('table'=>'eshop_cc_rates_exclussions','key'=>'itemid');
		$this->tables['classes'] = array('table'=>'eshop_classes','key'=>'id');
		$this->tables['classes_exclussions'] = array('table'=>'eshop_classes_exclussions','key'=>'class_id');
		$this->tables['classes_options'] = array('table'=>'eshop_classes_options','key'=>'otpionid');
		$this->tables['discounts'] = array('table'=>'eshop_discounts','key'=>'id');
		$this->tables['discounts_products'] = array('table'=>'eshop_discounts_products','key'=>'id');
		$this->tables['order_status_codes'] = array('table'=>'eshop_order_status_codes','key'=>'id');
		$this->tables['orders'] = array('table'=>'eshop_orders','key'=>'id');
		$this->tables['payment_methods'] = array('table'=>'eshop_payment_methods','key'=>'id');
		$this->tables['payment_processors'] = array('table'=>'eshop_payment_processors','key'=>'id');
		$this->tables['items'] = array('table'=>'eshop_item_details','key'=>'id');
		$this->tables['item_details'] = array('table'=>'eshop_item_details','key'=>'id');
		$this->tables['provider_data'] = array('table'=>'eshop_provider_data','key'=>'id');
		$this->tables['provider_items'] = array('table'=>'eshop_provider_items','key'=>'id');
		$this->tables['shipping'] = array('table'=>'eshop_shipping','key'=>'id');
		$this->tables['shipping_rates'] = array('table'=>'eshop_shipping_rates','key'=>'id');
		$this->tables['tax'] = array('table'=>'eshop_tax','key'=>'id');
		$this->lastAffectedID = 0;
		$this->module = $loaded_modules['eshop'];
		$tmp = $this->getTax(0,0,array('default'=>1));
		$this->tax = $tmp['taxrate'];
	}//END FUNCTION

	/**
	 * Gets the status codes of the orders
	 *
	 * @param array $settings
	 * @return array
	 */
	function orderStatusCodes($settings=0)
	{
		global $sql;

		$fields = (!$settings['fields']) ? "*" : $settings['fields'];
		$sql->db_Select($this->tables['order_status_codes']['table'],$fields);
		$res = execute_multi($sql);
		if ($settings['simplify']) {
			for ($i=0;count($res) > $i;$i++)
			{
				$codes[$res[$i]['id']] = $res[$i]['title'];
			}
			return $codes;
		}
		return $res;
	}//END FUNCTION

	/**
	 * adds or subtracts VAT from price
	 * @deprecated
	 * @param float $price_without_vat
	 * @param float $vat
	 * @param string $mode
	 * @return float
	 */
	function vatPrice($price_without_vat,$vat="default",$mode='plus')
	{
		return $this->getVatPriceOf($price_without_vat, $vat,$mode);
	}//END FUNCTION

	/**
	 * adds or subtracts VAT from price
	 *
	 * @param float $price_without_vat
	 * @param float $vat
	 * @param string $mode
	 * @return float
	 */
	function getVatPriceOf($price_without_vat,$vat="default",$mode='plus')
	{
		$vat = ($vat == 'default') ? $this->module['settings']['vat'] : $vat;
		if (is_array($price_without_vat)) {
			$price_without_vat = array_sum($price_without_vat);

		}

		if ($mode == 'plus') {
			$price_with_vat = $price_without_vat + ($vat*($price_without_vat/100)); // work out the amount of vat
		}
		else {
			$price_with_vat = $price_without_vat - ($vat*($price_without_vat/100)); // work out the amount of vat
		}
		return $price_with_vat;
	}//END FUNCTION


	/**
	 * @deprecated
	 * @param unknown_type $id
	 * @param unknown_type $uid
	 * @return Ambigous <unknown, number>
	 */
	function removeFromCart($id,$uid)
	{
		return $this->doRemoveFromCart($id, $uid);
	}

	/**
	 * deletes an item from user cart
	 *
	 * @param int $id
	 * @param int $uid
	 * @return unknown
	 */
	function doRemoveFromCart($id = 0 ,$uid,$filters)
	{
		global $sql;
		if ($id) {
			$q[] = "id=".$id;
		}
		
		if (is_array($filters)) {
			
			foreach($filters as $key => $value ) {
				$q[] = $key." = '".$value."'";
			}
			
		}
		
		$q[] = "status = 0 AND uid =".$uid;

		if ($sql->db_Delete($this->tables['cart']['table'],implode("and ",$q)))
		{
			return 1;
		}

	}//END FUNCTION

	/**
	 * adds item to user cart
	 *
	 * @param int $uid
	 * @param int $item
	 * @param array $options
	 * @param string $module
	 */
	function addToCart($uid,$item,$options,$module="products")
	{
		global $sql;

		$status = 0;//Active cart object. When order complete set it to 1
		$order_id = 0;//Active cart object. When order complete set it to the order number
		$quantity = ($options['quantity']) ? $options['quantity'] : 1;
		
		$item['id'];
		$item['sku'];
		$item['title'];
		$item['price'];
		
		// First check if the options aggree with the cart
		$sql->db_Select($this->tables['cart']['table'],"id","status = 0 AND uid = $uid");
		
		if ($sql->db_Rows())
		{
			$res = execute_single($sql);
			$res['quantity'] += $quantity;
			$res['price'] += ($price *$quantity);
			$res['options'] = json_decode($res['options']);
			
			if(isset($res['options']['products'][$item['id']]))
			{
				$res['options']['products'][$item['id']]['quantity'] = $quantity;
			}
			else
			{
				$res['options']['products'][$item['id']] = array(
						"id" 		=> $item['id'],
						"sku" 		=> $item['sku'],
						"title"		=> $item['title'],
						"price" 	=> $item['price'],
						"quantity"	=> $quantity
				);
				array_push($res['options']['productids'], $item['id']);
			}
			
			$res['options'] = json_encode($res['options']);
			
			$query = "";
			foreach ($res as $key => $val)
			{
				$query .= " $key = '$val'  ,";
			}
			
			$query = substr($query, 0, -1);
			$query .= " WHERE id = ".$res['id'];
			$sql->db_Update($this->tables['cart']['table'],$query);
			//Found update quantities
		}
		else //NOT FOUND INSERT IT
		{
			$products = array();
			$products['products'] = array();
			$products['productids'] = array();
			
			array_push($products['productids'], $item['id']);
			
			$products['products'][$item['id']] = array(
					"id" 		=> $item['id'],
					"sku" 		=> $item['sku'],
					"title"		=> $item['title'],
					"price" 	=> $item['price'],
					"quantity"	=> $quantity,
			);
			$insertToken = array();
			$insertToken['module'] = $module;
			$insertToken['uid'] = $uid;
			$insertToken['itemid'] = $item['id'];
			$insertToken['optionid'] = 0;
			$insertToken['quantity'] = $quantity;
			$insertToken['price'] = $item['price'];
			$insertToken['status'] = 0;
			$insertToken['order_id'] = 0;
			$insertToken['options'] = $this->getDefaultCartOptions($products);
			$insertToken['extra'] = json_encode(array());
			$insertToken['date_added'] = time();
			
			$sql->db_Insert($this->tables['cart']['table'],
					"'','".$insertToken['module']
					."','".$insertToken['uid']
					."','".$insertToken['itemid']
					."','".$insertToken['optionid']
					."','".$insertToken['quantity']
					."','".$insertToken['price']
					."','".$insertToken['status']
					."','".$insertToken['order_id']
					."','".$insertToken['options']
					."','".$insertToken['extra']
					."','".$insertToken['date_added']."'");
		}
	}//END FUNCTION

	/**
	 * Empties the users cart
	 *
	 * @param unknown_type $uid
	 */
	function emptyCart($uid)
	{
		$sql = new db();
		$sql->db_delete($this->tables['cart']['table'],"uid = $uid AND status = 0");
	}//END FUNCTION

	/**
	 * @deprecated
	 * @param unknown_type $itemid
	 * @param unknown_type $settings
	 */
	function discounts($itemid=0,$settings=0)
	{
		return $this->getDiscount($itemid,$settings);
	}

	/**
	 * Get discounts of a product
	 * @param unknown_type $itemid
	 * @param unknown_type $settings
	 * @return Ambigous <multitype:, number, string, unknown, mixed>
	 */
	function getDiscount($itemid,$settings=0)
	{
		return $this->getDiscounts($itemid,$settings);
	}

	/**
	 * grabs discoutns
	 *
	 * @param int $itemid
	 * @param array $settings
	 * @return array
	 */
	function getDiscounts($itemid=0,$settings=0)
	{
		global $sql;
		$fields = (!$settings['fields']) ? "*" : $settings['fields'];
		//Check if a product discount is available.
		if ($itemid)
		{
			$sql->db_Select($this->tables['discounts_products'],$fields,"itemid = $itemid");
			if ($sql->db_Rows()) //FOUND A VALID DISCOUNT
			{
				$tmp = execute_single($sql);
				$sql->db_Select($this->tables['discounts']['table'],$fields,"id = ".$tmp['id']);
				return execute_single($sql);
			}
			else
			{
				$sql->db_Select($this->tables['discounts']['table'],$fields,"type = 0");
				return execute_multi($sql);
			}
		}
		else
		{
			$sql->db_Select($this->tables['discounts']['table'],$fields,"type = 0");
			return execute_multi($sql);
		}
	}//END FUNCTION


	function searchOrders($settings)
	{
		global $sql,$loaded_modules;
		$users = new user();
		$products = new Items(array('module'=>$loaded_modules['products']));
		$fields = ($settings['fields']) ? $settings['fields'] : $this->tables['orders']['table'].".*";
		$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
		$way = ($settings['way']) ? "  ".$settings['way'] : "";
		$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
		$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
		if (is_array($settings['searchFields'])) {
			foreach ($settings['searchFields'] as $k => $v) {
				if (is_array($v)) {
					$q[] = $this->tables['orders']['table'].".$k IN (".implode(",",$v).")";
				}
				else {
					$q[] = $this->tables['orders']['table'].".$k = '$v'";
				}
			}
		}//END SETUP SEARCH FIELDS

		if (is_array($settings['searchFieldsIN'])) {
			foreach ($settings['searchFieldsINs'] as $k => $v) {
				$q[] = $this->tables['orders']['table'].".$k = IN ($v)";
			}
		}//END SETUP SEARCH FIELDS WITH IN
		if (is_array($settings['searchFieldsLike'])) {
			foreach ($settings['searchFieldsLike'] as $k => $v) {
				if ($k == 'user_name' OR $k == 'user_surname') {
					$tables['users'] = " INNER JOIN users ON (users.id=".$this->tables['orders']['table'].".uid)";
					$q[] = "$k LIKE '%$v%'";
				}
				else {
					$q[] = $this->tables['orders']['table'].".$k LIKE '%$v%'";
				}
			}
		}

		if (is_array($settings['searchFieldsNotLike'])) {
			foreach ($settings['searchFieldsNotLike'] as $k => $v) {
				$q[] = $this->tables['orders']['table'].".$k NOT LIKE '%$v%'";
			}
		}//END SETUP SEARCH FIELDS WITH NOT LIKE

		if (is_array($settings['searchFieldsGT'])) {
			foreach ($settings['searchFieldsGT'] as $k => $v) {
				$q[] = $this->tables['orders']['table'].".$k > $v";
			}
		}//END SETUP SEARCH FIELDS WITH NOT LIKE

		if (is_array($settings['searchFieldsLT'])) {
			foreach ($settings['searchFieldsLT'] as $k => $v) {
				$q[] = $this->tables['orders']['table'].".$k < $v";
			}
		}//END SETUP SEARCH FIELDS WITH NOT LIKE

		if (is_array($settings['searchFieldBETWEEN'])) {
			foreach ($settings['searchFieldBETWEEN'] as $k => $v) {
				$q[] = "(".$this->tables['orders']['table'].".$k BETWEEN $v)";
			}
		}//END SETUP SEARCH FIELDS WITH NOT LIKE

		$filters = $settings["filters"];

		if (isset($filters['missing']))
		{
			$q[] = $filters['missing']." IS NULL ";
		}
		if (isset($filters['custom_field']))
		{

			if ($filters['custom_field'] == 'uname') {
				$tables['users'] = " INNER JOIN users ON (users.id=".$this->tables['orders']['table'].".uid)";
			}//END UNAME
			$q[] = $this->tables['orders']['table'].".".$filters['custom_field']." LIKE '%".$filters['custom_value']."%'";
		}
		if ($settings['orderby'] == 'uname') {
			if (!$tables['users']) {
				$tables['users'] = " INNER JOIN users ON (users.id=".$this->tables['orders']['table'].".uid)";
			}
		}

		if ($q) {
			$query = implode(" AND ",$q);
		}

		if (!$query)
		{
			$query_mode='no_where';
		}
		else {
			$query_mode = 'default';
		}

		$tables = (is_array($tables)) ? $this->tables['orders']['table']." ".implode(" ",$tables) : $this->tables['orders']['table'];

		if ($settings['return'] == 'multi' OR $settings['return'] == 'single') //Return all results, NO PAGINATION
		{
			$sql->db_Select($tables,$fields,"$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT $fields FROM $tables WHERE $query $orderby $way $limit<br>";
			}
			$res = execute_multi($sql);
		}//END ALL RESULTS
		elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
		{

			$sql->db_Select($tables,$this->tables['orders']['table'].'.id,amount',"$query $orderby $way",$query_mode);//GET THE TOTAL COUNT
			if ($settings['debug'])
			{
				//echo "SELECT ".$this->tables['orders']['table'].".id,amount FROM $tables WHERE $query $orderby $way $limit<br>";
			}
			if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
			{
				$total = $sql->db_Rows();
					
				if ($settings['getTotalAmount']) {
					$tmp = execute_multi($sql);
					$this->ordersTotal = $this->orderSum($tmp);
				}

				$current_page = ($settings['page']) ? $settings['page'] : 1;
				$results_per_page =  $settings['results_per_page'];
				if (isset($settings['start']))
				{
					$start = $settings['start'];
				}
				else {
					$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
				}
				$limit = "LIMIT $start,".$results_per_page;
				$sql->db_Select($tables,$fields,"$query $orderby $way $limit",$query_mode);
				if ($settings['debug']) {
					//echo "<br>SELECT $fields FROM $tables WHERE $query $orderby $way $limit";
				}
				$res = execute_multi($sql,1);
				//			echo $total."asdfs fsf ";
				paginate_results($current_page,$results_per_page,$total);
			}//END FOUND RESULTS
		}//END PAGINATION
		elseif ($settings['return'] == 'count') // RETURN COUNT RESULTS
		{
			$sql->db_Select($tables,"count(".$this->tables['orders']['table'].".id) as total","$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				//echo "SELECT count(".$this->tables['orders']['table'].".id) as total FROM $tables WHERE $query $orderby $way $limit<br>";
			}
			$res = execute_single($sql);

			return $res['total'];
		}

		if (count($res) > 0)
		{
			for ($i=0;count($res) > $i;$i++)
			{
				$res[$i]['payment_method'] = $this->paymentMethods($res[$i]['payment_method']);
				$res[$i]['details'] = json_decode($res[$i]['details'],true);
				$res[$i]['user'] = $users->userDetails($res[$i]['uid']);
				
				if ($settings['getItemDetails']) {
					for ($i=0;count($res) > $i;$i++)
					{
						$sql->db_Select($this->tables['cart']['table']." INNER JOIN ".$products->tables['item']." ON (".$products->tables['item'].".id=".$this->tables['cart'].".itemid)",$this->tables['cart']['table'].".*,".$products->tables['item'].".title,(".$this->tables['cart']['table'].".quantity * ".$this->tables['cart']['table'].".price) as total","order_id = ".$res[$i]['id']);
						if ($sql->db_Rows()) {
							$res[$i]['products'] = execute_multi($sql);
						}
						foreach ($res[$i]['products'] as $v)
						{
							$res[$i]['prices'][] = $v['total'];
						}
					}
				}//END GET ITEM
					
				if ($settings['getUserDetails']) {
					$res[$i]['userDetails'] = $users->userDetails($res[$i]['uid'],array('fields'=>'id,uname,user_name,user_surname'));
				}
			}//END LOOP ROWS
		}//END ROWS

		if ($settings['return'] == 'single') {
			return $res[0];
		}
		else {
			if ($settings['getTotalAmount']) {

				$this->ordersTotal = $this->orderSum($res);
			}
			return $res;
		}

	}//END FUNCTION

	function orderSum($orders)
	{
		if ($orders) {
			foreach ($orders as $v)
			{
				$a[] = $v['amount'];
			}
		}
		return array_sum($a);
	}//END FUNCTION


	function paymentMethods($id=0,$settings=0)
	{
		global $sql;

		if ($id)
		{
			if ($active) {
				$active_q = " AND active = $active";
			}
			$fields = ($settings['fields']) ? $settings['fields'] : "*";
			$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
			$way = ($settings['way']) ? "  ".$settings['way'] : "";

			$sql->db_Select($this->tables['payment_methods']['table'],$fields,"id = $id $active_q ORDER BY orderby");
			if ($sql->db_Rows())
			{
				$s = execute_single($sql,1);
				$s['settings'] = json_decode($s['settings'],true);

				/*		if ($filters)
				 {
				$c = explode(":::",$s['shipping_methods']);
				$s['shipping'] = eshop_shipping_methods(0,0,$c);
				}*/
				return $s;

			}
		}
		############ TODO ###############
		else
		{
			$fields = ($settings['fields']) ? $settings['fields'] : 'eshop_payment_methods.*';
			if ($active) {
				$active_q = "active = $active";
			}
			$mode = ($active) ? "default" : "no_where";

			$sql->db_Select("eshop_payment_methods",$fields,"$active_q ORDER BY orderby",$mode);

			if ($sql->db_Rows())
			{
				$s = execute_multi($sql,1);
				foreach ($s as $i=>$v){
					$s[$i]['settings'] = json_decode($s[$i]['settings'],true);
					if ($settings['shipping_methods']) {
						//GET SHIIPPING METHODS FOR THIS PAYMENT METHOD
						$a = json_decode($s[$i]['shipping_methods']);
						$sql->db_Select("eshop_shipping","parent,id,shipping,base_cost,settings","id IN (".implode(',',$a).") ORDER BY orderby");
//							echo "SELECT parent,id,shipping,base_cost,settings FROM eshop_shipping WHERE id IN (".implode(',',$a).") ORDER BY orderby<br>";
						if ($sql->db_Rows()) {
							$tmp = execute_multi($sql);
							foreach ($tmp as $l=>$x)
							{
								$s[$i]['shipping'][$x['parent']]['methods'][$l] = $x;
								$s[$i]['shipping'][$x['parent']]['methods'][$l]['settings'] = json_decode($s[$i]['shipping'][$x['parent']]['methods'][$l]['settings'],true);
							}
						}
					}
				}
				return $s;
			}
		}
	}//END FUNCTION

	/**
	 * Sets the Data array as ts about to be queried by searchOrders()
	 * @param unknown_type $data
	 * @return Ambigous <number, unknown>
	 */
	function setupSearchData($data)
	{
		$posted_data = $data['data'];
		$secondaryData = $data['secondaryData'];
		$likeData = $data['likeData'];

		if ($secondaryData['custom_field'] AND $secondaryData['custom_value']) {
			$searchFilters['custom_field'] = $secondaryData['custom_field'];
			$searchFilters['custom_value'] = $secondaryData['custom_value'];
		}
		if ($secondaryData['missing']) {
			$searchFilters['missing'] = $secondaryData['missing'];
		}
		$secondaryData['searchFields'] = $posted_data;
		$secondaryData['filters'] = $searchFilters;
		$secondaryData['return'] = 'paginated';
		$secondaryData['page'] = ($_GET['page']) ? $_GET['page'] : 1;

		$posted_data = $data['data'];
		$secondaryData = $data['secondaryData'];
		$secondaryData['searchFieldsLike'] = $data['likeData'];

		if ($secondaryData['custom_field'] AND $secondaryData['custom_value']) {
			$searchFilters['custom_field'] = $secondaryData['custom_field'];
			$searchFilters['custom_value'] = $secondaryData['custom_value'];
		}
		if ($secondaryData['missing']) {
			$searchFilters['missing'] = $secondaryData['missing'];
		}

		if ($secondaryData['rangeActive']) {
			$secondaryData['searchFieldBETWEEN']['amount'] = $secondaryData['amountFrom']." AND ".$secondaryData['amountTo'];
		}

		if ($secondaryData['dateFrom']) {
			$tmp = explode("/",$secondaryData['dateFrom']);
			$secondaryData['searchFieldsGT']['date_added'] = mktime(23,59,0,$tmp[1],$tmp[0],$tmp[2]);
		}

		if ($secondaryData['dateTo']) {
			$tmp = explode("/",$secondaryData['dateTo']);
			$secondaryData['searchFieldsLT']['date_added'] = mktime(23,59,0,$tmp[1],$tmp[0],$tmp[2]);
		}
		$secondaryData['searchFields'] = $posted_data;
		$secondaryData['filters'] = $searchFilters;
		$secondaryData['debug'] = ($secondaryData['debug']) ? $secondaryData['debug'] : 0;
		$secondaryData['return'] = ($secondaryData['return']) ? $secondaryData['return'] : 'paginated';
		$secondaryData['getTotalAmount'] = ($secondaryData['getTotalAmount']) ? $secondaryData['getTotalAmount'] : 0;
		$secondaryData['getUserDetails'] = ($secondaryData['getUserDetails']) ? $secondaryData['getUserDetails'] : 0;
		$secondaryData['fields'] = ($secondaryData['fields']) ? $secondaryData['fields'] : '*';
		$secondaryData['page'] = ($secondaryData['page']) ? $secondaryData['page'] : 1;
		return $secondaryData;
	}//END FUNCTION

	/**
	 *
	 * Get A Product Item by joining & fetchig its details
	 * @param unknown_type $id
	 * @param unknown_type $type
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getProductDetails($id,$type=0)
	{
		global $sql;
		$q="";
		$res = array();

		if($type)
		{
			$q = " AND type = '$type'";
		}

		$sql->db_Select(
		$this->tables['items']['table']
		." INNER JOIN ".$this->tables['item_details']['table']
		." ON ( ".$this->tables['item_details']['table'].".".$this->tables['item_details']['key']." = ".$this->tables['items']['table'].".".$this->tables['items']['key']." ) ",
		$this->tables['items']['table'].".*, ".$this->tables['item_details'].".* ",
		$this->tables['items']['key']." = ".$id.$q
		);
		if ($sql->db_Rows()) {
			$res = execute_single($sql);
		}
		return $res;
	}

	/**
	 *
	 * Get A Product Item
	 * @param unknown_type $id
	 * @param unknown_type $settings
	 * @return Ambigous <multitype:, number, unknown, string, mixed>
	 */
	function getProduct($id,$module,$settings=0)
	{
		global $sql,$loaded_modules;
		$res = array();
		
		//$fields = ($settings['fields']) ? $settings['fields'] : $this->tables['items']['table'].".*";
		$fields = ($settings['fields']) ? $settings['fields'] : $this->tables['item_details']['table'].".*";
		//$sql->db_Select($this->tables['items']['table'],$fields,$this->tables['items']['key']." = ".$id );
		$sql->db_Select($this->tables['item_details']['table'],$fields," itemid = $id AND module = '$module' ");
		if ($settings['debug']) {
			// echo "SELECT $fields  FROM  ".$this->tables['item_details']['table']." WHERE ".$this->tables['item_details']['key']." = ".$id ;
		}
		if ($sql->db_Rows()) {
			$res = execute_single($sql);
		}
		return $res;
	} //END FUNCTION

	/**
	 *
	 * Adds a new Item
	 * @param unknown_type $data
	 * @param unknown_type $settings
	 */
	function doAddItem($data,$settings=0)
	{
		$settings['action'] = 'addItem';
		return $this->itemActions($data,$settings);
	}

	/**
	 *
	 * Adds a new Item
	 * @param unknown_type $data
	 * @param unknown_type $settings
	 */
	function doModifyItem($data,$settings=0)
	{
		$settings['action'] = 'modifyItem';
		$settings['checkFirst'] = true;
		return $this->itemActions($data,$settings);
	}

	/**
	 * add an item to the eshop item details table
	 * Waring:  Better use "doFunctions"
	 * @param array $data
	 * @param array $settings
	 */
	function itemActions($data,$settings=0)
	{
		global $sql;

		if ($settings['action'] == 'addItem')
		{
			foreach ($data as $k => $v)
			{
				$keys[] = $k;
				$values[] = "'$v'";
			}

			$sql->db_Insert($this->tables['item_details']['table']." (".implode(",",$keys).")",implode(",",$values));
			if ($settings['debug']) {
				echo "INSERT INTO ".$this->tables['item_details']['table']." (".implode(",",$keys).") VALUES (".implode(",",$values).")";
			}
		}//END ADD ITEM
		elseif ($settings['action'] == 'modifyItem')
		{
			if ($settings['checkFirst']) {
				$product = $this->getProduct($data['itemid'],$data['module']);
				if ($product) {
					foreach ($data as $k=>$v)
					{
						$updateQuery[] = "$k = '$v'";
					}
					$sql->db_Update($this->tables['item_details']['table'],implode(",",$updateQuery)." WHERE itemid = ".$data['itemid']." AND module = '".$data['module']."'");
					if ($settings['debug']) {
						echo "UPDATE ".$this->tables['item_details']['table']." SET ".implode(",",$updateQuery)." WHERE itemid = ".$data['itemid']." AND module = '".$data['module']."'";
					}
				}//END EXISTS
				else {
					$this->itemActions($data,array('action'=>'addItem'));
				}
			}
		}//END MODIFY
	}//END FUNCTION

	/**
	 * Gets the tax of a product or in general
	 *
	 * @param unknown_type $id
	 * @param unknown_type $code
	 * @param unknown_type $settings
	 */
	function getTax($id=0,$code=0,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : $this->tables['tax']['table'].".*";
		if ($id || $code || $settings['default']) {
			if ($id) {
				$q[] = "id = $id";
			}
			if ($code) {
				$q[] = "code = '$code'";
			}
			if ($settings['default']) {
				$q[] = $this->tables['tax']['table'].".default = 1";
			}
			$query = implode(" AND ",$q);
			//		echo "SELECT $fields FROM ".$this->tables['tax']['table']." WHERE $query";
			$sql->db_Select($this->tables['tax']['table'],$fields,$query);
			$res = execute_single($sql);

		}//END SINGLE
		else {
			$sql->db_Select($this->tables['tax']['table'],$fields,$query);
			$res = execute_multi($sql);
		}//END MULTI

		return $res;
	}//END FUNCTION

	/**
	 *Gets orders by User ID
	 * @param string $uname
	 * @param boolean $archive
	 * @param boolean $paginated
	 * @param int $page
	 * @param int $perPage
	 * @return array
	 */
	function getOrdersByUID($id,$archive=false,$paginated=false,$page=0,$perPage=0,$sort_field="date_added",$sort_dir='DESC',$fetchStats=false)
	{
		$settings = array();
		if($archive)
		{
			$settings['filters']['archive'] = 1;
		}
		if($paginated)
		{
			$settings['return'] = "paginated";
			$settings['page'] = $page;
			$settings['limit'] = $perPage;
		}
		$settings['filters']['custom_field'] = 'uid';
		$settings['filters']['custom_value'] = $id;
		$settings['filters']['sort_field'] = $sort_field;
		$settings['filters']['sort_direction'] = $sort_dir;
		
		if(!$fetchStats)
		{
			return $this->getOrders($settings);
		}
		return $this->getOrdersStats($settings);
	}

	/**
	 * Gets orders by User name
	 * @param string $uname
	 * @param boolean $archive
	 * @param boolean $paginated
	 * @param int $page
	 * @param int $perPage
	 * @return array
	 */
	function getOrdersByUname($uname,$archive=false,$paginated=false,$page=0,$perPage=0,$sort_field="date_added",$sort_dir='DESC',$fetchStats=false)
	{
		$settings = array();
		if($archive)
		{
			$settings['filters']['archive'] = 1;
		}
		if($paginated)
		{
			$settings['return'] = "paginated";
			$settings['page'] = $page;
			$settings['limit'] = $perPage;
		}
		$settings['filters']['custom_field'] = 'uname';
		$settings['filters']['custom_value'] = $uname;
		$settings['filters']['sort_field'] = $sort_field;
		$settings['filters']['sort_direction'] = $sort_dir;
		if(!$fetchStats)
		{
			return $this->getOrders($settings);
		}
		return $this->getOrdersStats($settings);
	}
	
	/**
	* Gets orders by User mail
	* @param string $umail
	* @param boolean $archive
	* @param boolean $paginated
	* @param int $page
	* @param int $perPage
	* @return array
	*/
	function getOrdersByUserMail($umail,$archive=false,$paginated=false,$page=0,$perPage=0,$sort_field="date_added",$sort_dir='DESC',$fetchStats=false)
	{
		$settings = array();
		if($archive)
		{
			$settings['filters']['archive'] = 1;
		}
		if($paginated)
		{
			$settings['return'] = "paginated";
			$settings['page'] = $page;
			$settings['limit'] = $perPage;
		}
		$settings['filters']['custom_field'] = 'email';
		$settings['filters']['custom_value'] = $umail;
		$settings['filters']['sort_field'] = $sort_field;
		$settings['filters']['sort_direction'] = $sort_dir;
		
		if(!$fetchStats)
		{
			return $this->getOrders($settings);
		}
		return $this->getOrdersStats($settings);
	}
	
	/**
	* Gets orders by Their Status
	* @param int $status
	* @param boolean $archive
	* @param boolean $paginated
	* @param int $page
	* @param int $perPage
	* @return array
	*/
	function getOrdersByStatus($status,$archive=false,$paginated=false,$page=0,$perPage=0,$sort_field="date_added",$sort_dir='DESC')
	{
		$settings = array();
		if($archive)
		{
			$settings['filters']['archive'] = 1;
		}
		if($paginated)
		{
			$settings['return'] = "paginated";
			$settings['page'] = $page;
			$settings['limit'] = $perPage;
		}
		$settings['filters']['status'] = $status;
		$settings['filters']['sort_field'] = $sort_field;
		$settings['filters']['sort_direction'] = $sort_dir;
		return $this->getOrders($settings);
	}

	/**
	 *
	 * gets prders by their payment method
	 * @param unknown_type $payment_method
	 * @param unknown_type $archive
	 * @param unknown_type $paginated
	 * @param unknown_type $page
	 * @param unknown_type $perPage
	 * @return Ambigous <Ambigous, unknown, number, string, boolean>
	 */
	function getOrdersByPaymentMethod($payment_method,$archive=false,$paginated=false,$page=0,$perPage=0,$sort_field="date_added",$sort_dir='DESC')
	{
		$settings = array();
		if($archive)
		{
			$settings['filters']['archive'] = 1;
		}
		if($paginated)
		{
			$settings['return'] = "paginated";
			$settings['page'] = $page;
			$settings['limit'] = $perPage;
		}
		$settings['filters']['custom_field'] = 'payment_method';
		$settings['filters']['custom_value'] = $payment_method;
		$settings['filters']['sort_field'] = $sort_field;
		$settings['filters']['sort_direction'] = $sort_dir;
		return $this->getOrders($settings);
	}
	
	/**
	*
	* gets prders by their shipping
	* @param unknown_type $payment_method
	* @param unknown_type $archive
	* @param unknown_type $paginated
	* @param unknown_type $page
	* @param unknown_type $perPage
	* @return Ambigous <Ambigous, unknown, number, string, boolean>
	*/
	function getOrdersByShippingMethod($shipping_method,$archive=false,$paginated=false,$page=0,$perPage=0,$sort_field="date_added",$sort_dir='DESC')
	{
		$settings = array();
		if($archive)
		{
			$settings['filters']['archive'] = 1;
		}
		if($paginated)
		{
			$settings['return'] = "paginated";
			$settings['page'] = $page;
			$settings['limit'] = $perPage;
		}
		$settings['filters']['custom_field'] = 'shipping_method';
		$settings['filters']['custom_value'] = $shipping_method;
		$settings['filters']['sort_field'] = $sort_field;
		$settings['filters']['sort_direction'] = $sort_dir;
		return $this->getOrders($settings);
	}
	
	/**
	*
	* gets orders by Custom Filtering
	* @param unknown_type $customFilters
	* @param unknown_type $settings
	* @return Ambigous <Ambigous, unknown, number, string, boolean>
	*/
	function getOrdersByCustomFilters($customFilters,$settings,$logic="AND",$fetchStats=false)
	{
		$logic = " ".$logic." ";
		$qArr = array();
		foreach ($customFilters as $vArr)
		{
			switch($vArr['operator'])
			{
				case "startswith":
					$qArr[] = " " .$vArr['field'] ." LIKE '".$vArr['value'] ."%' ";
					break;
				case "contains":
				case "substringof":
					$qArr[] = " " .$vArr['field'] ." LIKE '%".$vArr['value'] ."%' ";
					break;
				case "endswith":
					$qArr[] = " " .$vArr['field'] ." LIKE '%".$vArr['value'] ."' ";
					break;
				case "gte":
				case ">=":
				case "isgreaterthanorequalto":
				case "greaterthanequal":
				case "ge":
					$qArr[] = " " .$vArr['field'] ." >= ".(is_numeric($vArr['value'])? $vArr['value'] : "'".$vArr['value']."'")." ";
					break;
				case "gt":
				case ">":
				case "isgreaterthan":
				case "greaterthan":
				case "greater":
					$qArr[] = " " .$vArr['field'] ." > ".(is_numeric($vArr['value'])? $vArr['value'] : "'".$vArr['value']."'")." ";
					break;
				case "lte":
				case "<=":
				case "islessthanorequalto":
				case "lessthanequal":
				case "le":
					$qArr[] = " " .$vArr['field'] ." <= ".(is_numeric($vArr['value'])? $vArr['value'] : "'".$vArr['value']."'")." ";
					break;
				case "lt":
				case "<":
				case "islessthan":
				case "lessthan":
				case "less":
					$qArr[] = " " .$vArr['field'] ." < ".(is_numeric($vArr['value'])? $vArr['value'] : "'".$vArr['value']."'")." ";
					break;
				case "neq":
				case "!=": 
				case "isnotequalto": 
				case "notequals": 
				case "notequalto": 
				case "notequal": 
				case "ne":
					$qArr[] = " " .$vArr['field'] ." != ".(is_numeric($vArr['value'])? $vArr['value'] : "'".$vArr['value']."'")." ";
					break;
				case "eq":
				case "==":
				case "isequalto":
				case "equals":
				case "equalto":
				case "equal":
				default:
					$qArr[] = " " .$vArr['field'] ." = ".(is_numeric($vArr['value'])? $vArr['value'] : "'".$vArr['value']."'")." ";
					break;
			}
		}
		$settings['custom_filters'] = implode($logic, $qArr);
		if(!$fetchStats)
		{
			return $this->getOrders($settings);
		}
		return $this->getOrdersStats($settings);
	}
	
	/**
	 *
	 * Gets A specific order
	 * @param unknown_type $id
	 * @return Ambigous <unknown, number, string, boolean>
	 */
	function getOrder($id)
	{
		$settings = array();
		$settings['filters']['id'] = $id;
		return $this->getOrders($settings);
	}

	/**
	 *
	 * The Orders data Factory
	 * @param unknown_type $settings
	 * @return Ambigous <>|Ambigous <number, string, boolean, unknown>
	 */
	function getOrders($settings)
	{
		global $sql,$loaded_modules;
		$orders = array();
		$q_ar = array();
		$customFilters = $settings['custom_filters'] ? $settings['custom_filters'] : false;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$tables = "eshop_orders ";
		$filters = $settings['filters'];
		$sort_direction = ($filters['sort_direction'])? $filters['sort_direction'] :'DESC';
		$sort_field = ($filters['sort_field']) ? $filters['sort_field'] : 'id';
		if($filters['id'])
		{
			$settings['return'] == 'single';
			$q_ar[] = " id = ".$filters['id']." ";
		}
		if (isset($filters['archive'])) {
			$q_ar[] = " archive = ".$filters['archive'];
		}
		if (isset($filters['missing']))
		{
			$q_ar[] = $filters['missing']." IS NULL ";
		}
		if (isset($filters['custom_field']))
		{
			if ($filters['custom_field'] == 'uname') {
				$sql->db_Select("users","id","uname = '".$filters['custom_value']."'");
				$tmp = execute_single($sql);
				$filters['custom_value'] = $tmp['id'];
				$filters['custom_field'] = 'uid';
			}
			// Evangelos, Edit by
			$q_ar[] = " " . $filters['custom_field']. ( is_numeric($filters['custom_value'])? " = ".$filters['custom_value']." " : " LIKE '%".$filters['custom_value']."%' " );
		}
		if (isset($filters['status'])) {
			$q_ar[] = " status = ".$filters['status'];
		}
		if (isset($filters['uid'])) {
			$q_ar[] = " uid = ".$filters['uid']." ";
		}
		if (isset($filters['payment_method']))
		{
			$q_ar[] = " payment_method = ".$filters['payment_method']." ";
		}
		if (isset($filters['shipping_method']))
		{
			$q_ar[] = " shipping_method = ".$filters['shipping_method']." ";
		}
		if ($sort_field == 'uname')
		{
			$tables .= " INNER JOIN users ON (users.id=eshop_orders.uid) ";
			$fields = "eshop_orders.*";
		}
		if (count($q_ar) > 1)
		{
			$q = implode(" AND ",$q_ar);
		}
		else {
			$q = $q_ar[0];
		}
		if($customFilters)
		{
			if(strlen($q)>0)
			{
				$q .= " AND ";
			}
			$q .= " ( ".$customFilters." ) ";
		}
		
		$q .= " ORDER BY $sort_field $sort_direction ";

		if (count($q_ar) == 0)
		{
			$query_mode='no_where';
		}
		else {
			$query_mode = 'default';
		}
		
		if ($settings['return'] == 'multi' OR $settings['return'] == 'single') //Return all results, NO PAGINATION
		{
			if ($settings['limit'])
			{
				$q .= "LIMIT ".$settings['limit'];
			}
			$sql->db_Select("eshop_orders",$fields,$q,$query_mode);
			if ($sql->db_Rows())
			{
				$orders = execute_multi($sql);
			}
		}//END ALL RESULTS
		elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
		{
			$sql->db_Select($tables,$fields,$q,$query_mode);//GET THE TOTAL COUNT
			if ($settings['debug'])
			{
				echo "<br>SELECT $fields FROM $tables WHERE $q<br>";
			}
			if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
			{
				$total = $sql->db_Rows();
				$current_page = ($settings['page']) ? $settings['page'] : 1;
				$results_per_page =  $settings['limit'];
				if (isset($settings['start']))
				{
					$start = $settings['start'];
				}
				else {
					$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
				}
				$q .= " LIMIT $start,".$results_per_page;
				$sql->db_Select($tables,$fields,$q,$query_mode);
				$search_results = execute_multi($sql,1);
				if ($settings['debug'])
				{
					echo "<br>SELECT $fields FROM eshop_orders WHERE $q<br>";
				}
				//paginate_results($current_page,$results_per_page,$total);

				$orders = $search_results;

			}//END FOUND RESULTS

		}//END PAGINATION

		//optimazation trick
		$isLotteryModuleActivated = array_key_exists('lottery',$loaded_modules);
		$isProductModuleActivated = array_key_exists('products',$loaded_modules);

		
		for ($i=0; count($orders) > $i; $i++)
		{
			$tmp = false;
			//$orders[$i]['shipping_method'] = is_array($tmp = $this->getShippingMethods(($orders[$i]['shipping_method'])))? $tmp : $orders[$i]['shipping_method'];
			$orders[$i]['shipping_method'] = (is_array($tmp = $this->getShippingMethods("",1,array($orders[$i]['shipping_method']),"single")) || strlen($tmp) >1 ? $tmp : 0 );
			$orders[$i]['payment_method'] = $this->getPaymentMethods(($orders[$i]['payment_method']));
			
			
			// TODO
			
			$tmpArr = array();
			if(!isJSON($orders[$i]['details']))
			{
				$orders[$i]['details'] = $tmpArr;
				
				$details = array(
						"productids" => $orders[$i]['details']['productids'],
						"invoice_type" => $orders[$i]['details']['invoice_type'],
						"company_name" => $orders[$i]['details']['company_name'],
						"proffession" => $orders[$i]['details']['proffession'],
						"address" => ($orders[$i]['details']['address']? $orders[$i]['details']['address'] : $orders[$i]['details']['order_address'] ),
						"postcode" => $orders[$i]['details']['postcode'],
						"afm" => $orders[$i]['details']['afm'],
						"doy" => $orders[$i]['details']['doy'],
						"phone" => ($orders[$i]['details']['phone']? $orders[$i]['details']['phone'] : $orders[$i]['details']['user_phone'] ),
						"payment_method" => $orders[$i]['details']['payment_method'],
						"shipping" => $orders[$i]['details']['shipping'],
						"order_name" => ($orders[$i]['details']['order_name']? $orders[$i]['details']['order_name'] : $orders[$i]['details']['user_name'] ),
						"order_surname" => ($orders[$i]['details']['order_surname']? $orders[$i]['details']['order_surname'] : $orders[$i]['details']['user_surname'] ),
						"order_phone" => ($orders[$i]['details']['order_phone']? $orders[$i]['details']['order_phone'] : $orders[$i]['details']['user_phone'] ),
						"order_mobile" => ($orders[$i]['details']['order_mobile']? $orders[$i]['details']['order_mobile'] : $orders[$i]['details']['user_mobile'] ),
						"order_address" => ($orders[$i]['details']['order_address']? $orders[$i]['details']['order_address'] : $orders[$i]['details']['address'] ),
						"order_municipality" => $orders[$i]['details']['order_municipality'],
						"order_city" => $orders[$i]['details']['order_city'],
						"order_postcode" => $orders[$i]['details']['order_postcode'],
						"Period" => $orders[$i]['details']['Period'],
						"ExtraCharge" => $orders[$i]['details']['ExtraCharge'],
				);
				$orders[$i]['details'] = $this->getDefaultCartOptions($details);
				$sql->db_Update("eshop_orders"," details = \"". json_encode($orders[$i]['details']) ."\" WHERE id = '".$orders[$i]['id']."' ");
			}
			else 
			{
				$orders[$i]['details'] = json_decode($orders[$i]['details'],true);
				
			}
			$user = new user();
			$orders[$i]['user'] = $user->getUserByID($orders[$i]['uid']);
			
			if ($isLotteryModuleActivated)
			{
				$details = array();
				$details = $orders[$i]['details'];
				$tmp = explode("---",$details['tickets']);
				$a = array();
				// Evangelos, Edit by
				foreach ($tmp as $v)
				{
					$a = explode("|",$v);
					$items[$orders[$i]['id']]['groupid'] = $a[0];
					$items[$orders[$i]['id']]['status'] = $orders[$i]['status'];
					$items[$orders[$i]['id']]['t'][$a[1]]['tickets'] = explode(",",$a[2]);
					if ($settings['get_tickets']) {
						$items[$orders[$i]['id']]['t'][$a[1]]['item'] =  get_products($a[1],$settings['lang'],1);
						$orders[$i]['items'] = $items;
					}
					$orders[$i]['num_tickets'] += count($items[$orders[$i]['id']]['t'][$a[1]]['tickets']);
				}
			}//END LOTTERY
			if ($isProductModuleActivated)
			{
				if ($settings['get_products']) {
					for ($i=0;count($orders) > $i;$i++)
					{
						//echo "SELECT eshop_cart.*,products_lng.title FROM eshop_cart INNER JOIN products_lng ON (products_lng.productid=eshop_cart.productid) WHERE order_id = ".$orders[$i]['id']."<br>";
						$sql->db_Select("eshop_cart INNER JOIN products ON (products.id=eshop_cart.itemid)","eshop_cart.*,products.title as prodtitle,(eshop_cart.quantity * eshop_cart.price) as total","order_id = ".$orders[$i]['id']);
						//echo "eshop_cart INNER JOIN products_lng ON (products_lng.productid=eshop_cart.productid)","eshop_cart.*,products_lng.title as prodtitle,(eshop_cart.quantity * eshop_cart.price) as total","order_id = ".$orders[$i]['id'];
						
						if ($sql->db_Rows()) {
							$orders[$i]['products'] = execute_multi($sql);
							foreach ($orders[$i]['products'] as $v)
							{
								$orders[$i]['prices'][] = $v['total'];
							}
						}
					}
				}
			}
		}

		if ($settings['return'] == 'single') {
			return $orders[0];
		}
		else {
			return $orders;
		}
	}
	
	/**
	*
	* The Orders Stats data Factory, count & Sum
	* @param unknown_type $settings
	* @return Ambigous <>|Ambigous <number, string, boolean, unknown>
	*/
	function getOrdersStats($settings)
	{
		global $sql,$loaded_modules;
		$orders = array();
		$q_ar = array();
		$customFilters = $settings['custom_filters'] ? $settings['custom_filters'] : false;
		$fields = ($settings['fields']) ? $settings['fields'] : " count(id) as count, sum(amount) as sum ";
		$tables = "eshop_orders ";
		$filters = $settings['filters'];
		if (isset($filters['archive'])) {
			$q_ar[] = " archive = ".$filters['archive'];
		}
		if (isset($filters['custom_field']))
		{
			if ($filters['custom_field'] == 'uname') {
				$sql->db_Select("users","id","uname = '".$filters['custom_value']."'");
				$tmp = execute_single($sql);
				$filters['custom_value'] = $tmp['id'];
				$filters['custom_field'] = 'uid';
			}
			// Evangelos, Edit by
			$q_ar[] = $filters['custom_field']. ( is_numeric($filters['custom_value'])? " = ".$filters['custom_value']." " : " LIKE '%".$filters['custom_value']."%'" );
		}
		if (isset($filters['status'])) {
			$q_ar[] = " status = ".$filters['status'];
		}
		if (isset($filters['uid'])) {
			$q_ar[] = " uid = ".$filters['uid'];
		}
		if (isset($filters['payment_method'])) {
			$q_ar[] = " payment_method = ".$filters['payment_method'];
		}
		if (isset($filters['shipping_method'])) {
			$q_ar[] = " shipping_method = ".$filters['shipping_method'];
		}
		if (count($q_ar) > 1) {
			$q = implode(" AND ",$q_ar);
		}
		else {
			$q = $q_ar[0];
		}
		if($customFilters)
		{
			if(strlen($q)>0)
			{
				$q .= " AND ";
			}
			$q .= " ( ".$customFilters." ) ";
		}
		if (count($q_ar) == 0)
		{
			$query_mode='no_where';
		}
		else 
		{
			$query_mode = 'default';
		}
		
		$sql->db_Select("eshop_orders",$fields,$q,$query_mode);
		
		if ($sql->db_Rows())
		{
			$orders = execute_single($sql);
		}
		
		return $orders ? $orders : array("count"=>0,"sum"=>0,);
	}
	
	
	/**
	 * @deprecated
	 * @param unknown_type $settings
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function eshop_order_status_codes($settings=0)
	{
		return $this->getOrdersStatusCodes($settings);

	}

	/**
	 * fetches * from eshop_order_status_codes
	 * @param unknown_type $settings
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getOrdersStatusCodes($settings=0)
	{
		global $sql;
		$sql->db_Select("eshop_order_status_codes","*");
		return execute_multi($sql);
	}

	/**
	 *
	 * Fetches the payment Methods of eshop
	 * @param unknown_type $id
	 * @param unknown_type $active
	 * @param unknown_type $filters
	 * @return Ambigous <multitype:NULL , NULL, multitype:, multitype:Ambigous <> , number, unknown, string, mixed>
	 */
	function getPaymentMethods($id=0,$active=1,$filters=0)
	{
		global $sql;
		
		//---Cache optimazation
		$doCache = $filters['cache']? true : false;
		if(is_array($this->paymentMethodArr) && $doCache)
		{
			if($id)
			{
				return $this->paymentMethodArr[$id];
			}
			return $this->paymentMethodArr;
		}
		if(!is_array($this->paymentMethodArr) && $doCache)
		{
			$tmpArr = $this->getPaymentMethods(0,0);
			foreach ($tmpArr as $arr)
			{
				$this->paymentMethodsArr[$arr['id']] = $arr;
			}
		}
		//--cahche
		
		$s = array();
		//	echo "parent = '$id' ORDER BY orderby";
		if ($id)
		{
			if ($active) {
				$active_q = " AND active = $active";
			}
			$sql->db_Select("eshop_payment_methods","*","id = $id $active_q ORDER BY orderby");
			if ($sql->db_Rows() > 0)
			{
				$s = execute_single($sql,1);
				//$s['settings'] = form_settings_array($s['settings'],"###",":::");
				if ($filters)
				{
					//$c = explode(":::",$s['shipping_methods']);
					$c = json_decode($s['shipping_methods']);
					$s['shipping'] = array();
					$s['shipping'] = $this->getShippingMethods(0,1,$c);
				}
			}
		}
		else
		{
			if ($active) {
				$active_q = "active = $active";
			}
			$mode = ($active) ? "default" : "no_where";

			$sql->db_Select("eshop_payment_methods","*","$active_q ORDER BY orderby",$mode);

			if ($sql->db_Rows() > 0)
			{
				$s = execute_multi($sql,1);
				for ($i=0;count($s) > $i;$i++)
				{
					//$s[$i]['settings'] = form_settings_array($s[$i]['settings'],"###",":::");
					$s[$i]['settings'] = json_decode($s[$i]['settings'],true);
					if ($filters['shipping_methods']) 
					{
						$c = json_decode($s[$i]['shipping_methods']);
						$s[$i]['shipping'] = array();
						$s[$i]['shipping'] = $this->getShippingMethods(0,1,$c);
						
						/********************************* /
						//GET SHIIPPING METHODS FOR THIS PAYMENT METHOD
						$shipping_methods = str_replace(":::",",",$s[$i]['shipping_methods']);
						$sql->db_Select("eshop_shipping","*","id IN ($shipping_methods)");
						if ($sql->db_Rows()) {
							//$tmp = array();
							//$tmp = execute_multi($sql);
							$s[$i]['shipping'] = execute_multi($sql);
							foreach ($tmp as $v)
							{
								$s[$i]['shipping'][$v['parent']]['methods'][] = $v;
							}
							foreach ($s[$i]['shipping'] as $k=>$v) {
								$sql->db_Select("eshop_shipping","*","id = $k");
								$t = execute_single($sql);
								$t['settings'] = form_settings_array($t['settings'],'###',':::');
								$s[$i]['shipping'][$k]['parent_array'] = $t;
							}
						}//END ROWS
						/*********************************/
					}//END SHIPPING METHODS
					if ($filters['processor']) {
						//PAYMENT PROCESSOR
						$s[$i]['processor'] = $this->getPaymentProcessors($s[$i]['settings']['payment_processor']);
					}
				}//END LOOP
			}
		}
		return $s;
	}

	/**
	 *
	 * Fetches the payment processors of eshop
	 * @param unknown_type $id
	 * @return Ambigous <NULL, multitype:>
	 */
	function getPaymentProcessors($id=0)
	{
		global $sql;
		$s = null;
		if ($id)
		{
			$sql->db_Select("eshop_payment_processors","*","id = $id");
			$s = execute_single($sql,1);
			$s['settings'] = form_settings_array($s['settings'],"###",":::");
			$s['required_fields'] =explode(":::",$s['required_fields']);
		}
		else
		{
			$sql->db_Select("eshop_payment_processors","*");
			$s = execute_multi($sql,1);
			for ($i=0;count($s) > $i;$i++)
			{
				$tmp = form_settings_array($s[$i]['settings'],"###",":::");
				if(count($tmp)>0 && strlen($tmp[0]) >0 )
				{
					$s[$i]['settings'] = $tmp;
				}
				$tmp = array();
				$tmp = explode(":::",$s[$i]['required_fields']);
				if(count($tmp)>0 && strlen($tmp[0]) >0 )
				{
					$s[$i]['required_fields'] = $tmp;
				}
			}
		}
		return $s;
	}

	/**
	 *
	 * @deprecated
	 * @param unknown_type $id
	 * @param unknown_type $active
	 * @param unknown_type $filter
	 */
	function shippingMethods($id=0,$active=0,$filter=0)
	{
		$this->getShippingMethods($id,$active,$filter);
	}

	/**
	 * Returns the shipping methods in array or 0 instead
	 *
	 * @param int $id
	 * @param boolean $active
	 * @param String $filter
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getShippingMethods($id=0,$active=1,$filter=0,$method="multi")
	{
		global $sql;
		$q = array();
		$query = "";
		//$q[] = "1 = 1";
		//$q[] = " active = $active";
		if ($id != "" ) {
			$q[] = " parent = ".$id." ";
		}
		if (is_array($filter))
		{
			$q[] = "id IN(". (is_array($filter) ? implode(",",$filter) : $filter ).") ";
		}
		
		if ($active) 
		{ 
			$q[] = " active = $active"; 
		}
		//if ($id) { $q[] = " parent = '$id' "; }
		
		if (count($q)>0) {
			$query = implode(" AND ",$q);
		}
		else {
			$query = $q[0];
		}
		$sql->db_Select("eshop_shipping","*", $query);
		
		if($method=="multi")
		{
			$a = execute_multi($sql,1);
			foreach ($a as $k=>$v) {
				$a[$k]['settings'] = json_decode($v['settings'],true);
			}
			return $a;
		}
		return execute_single($sql,1);
	}//END FUNCTION
	
	
	/**
	* Returns the shipping methods of a payment Method
	*
	* @param int $id
	* @param boolean $active
	* @param String $filter
	* @return Ambigous <number, unknown, string, mixed>
	*/
	function getShippingMethodsByPaymentMethods($id,$active=1,$filter=0)
	{
		global $sql;
		
		$sql->db_Select("eshop_payment_methods","shipping_methods","id = ".$id);
		if ($sql->db_Rows() > 0)
		{
			$s = execute_single($sql,1);
			$c = explode(":::",$s['shipping_methods']);
			return $this->getShippingMethods(0,1,$c);
		}
		return array();
	}//END FUNCTION

	/**
	 *
	 * Archive an Order by ID
	 * @param int|array $id
	 * @return boolean
	 */
	function doArchiveOrder($id)
	{
		global $sql;
		if(is_array($id))
		{
			if($sql->db_Update("eshop_orders","archive = 1 WHERE id IN (".implode('", "',array_map('mysql_escape_string', $id)).")"))
			{
				return true;
			}
			return false;
		}

		if($sql->db_Update("eshop_orders","archive = 1 WHERE id = '".$id."'"))
		{
			return true;
		}
		return false;
	}

	/**
	 *
	 * Updates order's status by ID
	 * @param int $id
	 * @param int $status
	 * @return boolean
	 */
	function doUpdateOrderStatus($id, $status)
	{
		global $sql;

		if($sql->db_Update("eshop_orders","status = ".$status." WHERE id = '".$id."'"))
		{
			return true;
		}
		return false;
	}

	/**
	 *
	 * Archive an Order by ID
	 * @param int|array $id
	 * @return boolean
	 */
	function doDeleteOrder($id)
	{
		global $sql;
		if(is_array($id))
		{
			if($sql->db_Delete("lottery_tickets","orderid IN (".implode('", "',array_map('mysql_escape_string', $id)).")")
			&& $sql->db_Delete("eshop_orders","id IN (".implode('", "',array_map('mysql_escape_string', $id)).")"))
			{
				return true;
			}
			return false;
		}

		if($sql->db_Update("eshop_orders","archive = 1 WHERE id = '".$id."'"))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * Gets User ProductCart
	 * 
	 * $settings['status'] > 0 -> to fetch archived productCarts
	 * 
	 * @param unknown_type $uid
	 * @param unknown_type $settings
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getCartProductOptions($uid,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : $this->tables['cart']['table'].".*";
		$status = ($settings['status']) ? $settings['status'] : 0;

		$sql->db_Select($this->tables['cart']['table'],$fields,$this->tables['cart']['userKey']." = $uid AND status = $status");
		
		if ($sql->db_Rows()>0) {
			return execute_single($sql);
		}
		return execute_multi($sql);
	}

	/**
	 *
	 * @deprecated
	 * @param unknown_type $uid
	 * @param unknown_type $product
	 * @param unknown_type $options
	 * @param unknown_type $lang
	 * @return number
	 */
	function add_to_cart($uid,$product,$options,$lang)
	{
		return $this->doAddToCart($uid, $product, $options, $lang);
		/********
		 * //	$price = str_replace(".",",",$price);
		$status = 0;//Active cart object. When order complete set it to 1
		$order_id = 0;//Active cart object. When order complete set it to the order number
		$quantity = ($options['quantity']) ? $options['quantity'] : 1;
		//Analyze options
		$cart_options = $this->doAnalyzeCartoptions($options,$lang);

		########################### CALCULATE PRICE #################################################
		$price = ($product['eshop']['price'] * $quantity);//Calculate for the base quantity
		//ADITIONAL
		for ($i=0;count($tmp_opt) > $i;$i++)
		{
			if ($tmp_opt[$i]['modifier_type'] == '%')
			{
				$price = $price + ($tmp_opt[$i]['price_modifier'] * $price)/100;
			}
			elseif ($tmp_opt[$i]['modifier_type'] == '$')
			{
				$price = $price + $tmp_opt[$i]['price_modifier'];
			}
		}
		######################### CHECK CART FOR THIS PRODUCT ########################################
		// First check if the options aggree with the cart
		$sql->db_Select("eshop_cart","productid,quantity,price","status = 0 AND uid = $uid AND productid = ".$product['id']." AND options = '$cart_options'");
		if ($sql->db_Rows())
		{
			$res = execute_single($sql);
			$quantity = $res['quantity'] + 1;
			$price = $price + $res['price'];
			//Found update quantities
			$sql->db_Update("eshop_cart","quantity = '$quantity', price = '$price' WHERE status = 0 AND uid = $uid AND productid = ".$product['id']." AND options = '$cart_options'");
		}
		else //NOT FOUND INSERT IT
		{
			$time = time();
			$sql->db_Insert("eshop_cart","'$uid','".$product['id']."','$quantity','$price','$status','$order_id','$cart_options','$cart_details','$time'");
		}
		 */
	}
	
	
	function doSessionToCart ($uid) {
		global $sql;
		
		$sql->db_Select("eshop_cart","itemid,quantity,price","status = 0 and order_id=0 AND uid = $uid ");
		
		if ($sql->db_Rows())
		{
			
			$row=execute_multi($sql);
			$price = array();
			$quantity=0;
			foreach($row as $key => $value ) {
				
				$_SESSION['cart'][$value['itemid']]['quantity'] = $value['quantity'];
				$_SESSION['cart'][$value['itemid']]['price'] = $value['price'];
				$_SESSION['cart'][$value['itemid']]['total'] = $value['price']*$value['quantity'];
				$_SESSION['cart'][$value['itemid']]['formated_total'] = formatprice($_SESSION['cart'][$value['itemid']]['total']);
				$_SESSION['cart'][$value['itemid']]['saved'] = 1;
				$price[]= $_SESSION['cart'][$value['itemid']]['total'];
				$quantity= $value['quantity'] + $quantity;
			}
			

			return array('total_price'=> $price,"total_quantity" => $quantity);
		} else {
			return array();
		}
		
	}
	
	

	/**
	 *
	 * @param unknown_type $uid
	 * @param unknown_type $product
	 * @param unknown_type $options
	 * @param unknown_type $lang
	 * @return boolean
	 */
	function doAddToCart($uid,$product,$options,$lang)
	{
		global $sql;


		//	$price = str_replace(".",",",$price);
		$status = 0;//Active cart object. When order complete set it to 1
		$order_id = 0;//Active cart object. When order complete set it to the order number
		$quantity = ($options['quantity']) ? $options['quantity'] : 1;
		//Analyze options
		$cart_options = $this->getCartOptions($options,$lang);

		########################### CALCULATE PRICE #################################################
		$price = ($product['eshop']['price'] * $quantity);//Calculate for the base quantity
		//ADITIONAL
		for ($i=0;count($tmp_opt) > $i;$i++)
		{
			if ($tmp_opt[$i]['modifier_type'] == '%')
			{
				$price = $price + ($tmp_opt[$i]['price_modifier'] * $price)/100;
			}
			elseif ($tmp_opt[$i]['modifier_type'] == '$')
			{
				$price = $price + $tmp_opt[$i]['price_modifier'];
			}
		}
		
		######################### CHECK CART FOR THIS PRODUCT ########################################
		// First check if the options aggree with the cart
		$sql->db_Select("eshop_cart","itemid,quantity,price","status = 0 AND uid = $uid AND itemid = ".$product['id']." AND options = '".json_encode($cart_options,true)."'");

		if ($sql->db_Rows())
		{
			$res = execute_single($sql);
			//$quantity = $res['quantity'] + $quantity;
			//$price = $price + $res['price'];
			//Found update quantities
			$sql->db_Update("eshop_cart","quantity = '$quantity', price = '$price' WHERE status = 0 AND uid = $uid AND itemid = ".$product['id']." AND options = '".json_encode($cart_options,true)."'");
		}
		else //NOT FOUND INSERT IT
		{
			$time = time();
			$sql->db_Insert("eshop_cart","'','','$uid','".$product['id']."','','$quantity','$price','$status','$order_id','".json_encode($cart_options,true)."','$cart_details','$time'");
		//	print_ar($sql);
		}
		return true;
	}

	/**
	 * Converts A cart instance into an Order.
	 * Mind the options parametre
	 * @param int $cartID
	 * @param unknown_type $lang
	 * @param array $options
	 * @return boolean
	 */
	function doConvertCartToOrder($cartID,$lang,$options)
	{
		global $sql;
		$eCartArr = array();
		$userArr = array();

		if ($sql->db_Select("eshop_cart","id,uid,optionid,quantity,price,options"," cartid = $cartID AND options = '$cart_options'"))
		{
			$eCartArr = execute_single($sql);
			if ($sql->db_Select("users","id,email,user_name,user_surname,user_phone,user_mobile"," id = ".$eCartArr['uid']." "))
			{
				//$cart_options = $this->getCartOptions($eCartArr['options'],$lang);
				$cart_options = json_decode($eCartArr['options']);
				$userArr = execute_single($sql);
				// TODO - Confirm the validity of these options
				$price = 0;
				$price += ($cart_options['ExtraCharge']? $cart_options['ExtraCharge'] : 0);
				$price = $this->getVatPriceOf($eCartArr['price']);
				$paymentMethod = ($cart_options['payment_method']? $cart_options['payment_method'] : 0);
				$shippingMethod = ($cart_options['shipping']? $cart_options['shipping'] : 0);
				
				//$details = form_settings_string(array(),"###",":::");
				
				
				
				$details = array(
					"productids" => $cart_options['productids'],
					"invoice_type" => $cart_options['invoice_type'],
					"company_name" => $cart_options['company_name'],
					"proffession" => $cart_options['proffession'],
					"address" => ($cart_options['address']? $cart_options['address'] : $cart_options['order_address'] ),
					"postcode" => $cart_options['postcode'],
					"afm" => $cart_options['afm'],
					"doy" => $cart_options['doy'],
					"phone" => ($cart_options['phone']? $cart_options['phone'] : $userArr['user_phone'] ),
					"payment_method" => $cart_options['payment_method'],
					"shipping" => $cart_options['shipping'],
					"order_name" => ($cart_options['order_name']? $cart_options['order_name'] : $userArr['user_name'] ),
					"order_surname" => ($cart_options['order_surname']? $cart_options['order_surname'] : $userArr['user_surname'] ),
					"order_phone" => ($cart_options['order_phone']? $cart_options['order_phone'] : $userArr['user_phone'] ),
					"order_mobile" => ($cart_options['order_mobile']? $cart_options['order_mobile'] : $userArr['user_mobile'] ),
					"order_address" => ($cart_options['order_address']? $cart_options['order_address'] : $cart_options['address'] ),
					"order_municipality" => $cart_options['order_municipality'],
					"order_city" => $cart_options['order_city'],
					"order_postcode" => $cart_options['order_postcode'],
					"Period" => $cart_options['Period'],
					"ExtraCharge" => $cart_options['ExtraCharge'],
				);
				
				$time = time();
				$sql->db_Insert("eshop_orders","'".$userArr['id']."','".$userArr['email']."','1','$price','".$paymentMethod."','".$shippingMethod."',' ','".$time."','".json_encode($details)."','0','".$_SERVER['REMOTE_ADDR']."'");
				$this->lastAffectedID = $sql->last_insert_id;
				$sql->db_Update("eshop_cart"," order_id = '$sql->last_insert_id', price = '$price' WHERE id = ".$eCartArr['id']);
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 * @deprecated
	 * @param unknown_type $options
	 * @param unknown_type $lang
	 * @return Ambigous <string, unknown>
	 */
	function analyze_cart_options($options,$lang)
	{
		return $this->getCartOptions($options, $lang);
	}

	/**
	 *
	 * @param unknown_type $options
	 * @param unknown_type $lang
	 * @return Ambigous <string, unknown>|multitype:
	 */
	function getCartOptions($options,$lang)
	{
		foreach ($options as $key => $value)
		{

			if (strstr($key,'class_options')) //Search for options
			{
				if (strstr($key,'-'))
				{
					list($dump,$fieldid)=split("-",$key);

					$option[$fieldid] = $value;
					$tmp_opt[] = get_gallery_class_options($fieldid,$lang,$value);

				}
			}
		}

		if (is_array($option)) {
			return  form_settings_string($option,"###",":::");
		}
		return array();
	}
	
	/**
	* Gets the Default cart options as it is crated
	* 
	* $cart_options['products'][product_id] = array(
	* 		"id" => product_id,
	* 		"sku" => product_sku,
	* 		"title" => product_title,
	* 		"quantity" => quantity,
	* );
	* 
	* @param unknown_type $options
	* @return array
	*/
	function getDefaultCartOptions($cart_options)
	{
		global $sql;
		$products = array();
		
		if(isset($cart_options['products']))
		{
			$products = $cart_options['products'];
		}
		elseif(isset($cart_options['productids']))
		{
			
			$sql->db_Select("products"," id,sku,title, 1 as quantity ", "id IN(".preg_replace('/\|(\d{1})/', '', $cart_options['productids']).") ");
			$tmpArr = execute_multi($sql,1);
			for($i=0; $i<count($tmpArr); $i++)
			{
				$products[($tmpArr[$i]['id']? $tmpArr[$i]['id'] : 0)] = array(
						"id" => ($tmpArr[$i]['id']? $tmpArr[$i]['id'] : 0),
						"sku" => ($tmpArr[$i]['sku']? $tmpArr[$i]['sku'] : 0),
						"title" => ($tmpArr[$i]['title']? $tmpArr[$i]['title'] : ''),
						"price" => ($tmpArr[$i]['price']? $tmpArr[$i]['price'] : 0),
						"quantity" => ($tmpArr[$i]['quantity']? $tmpArr[$i]['quantity'] : 0),
				);
			}
			$cart_options['productids'] = preg_replace('/\|(\d{1})/', '', $cart_options['productids']);
		}
		else 
		{
			$products[0] = array(
				"id" => null,
				"sku" => null,
				"title" => null,
				"quantity" => null,
			);
		}
		
		return array(
			"productids" => $cart_options['productids'],
			"products" => $products,
			"invoice_type" => $cart_options['invoice_type'],
			"company_name" => $cart_options['company_name'],
			"proffession" => $cart_options['proffession'],
			"address" => ($cart_options['address']? $cart_options['address'] : $cart_options['order_address'] ),
			"postcode" => $cart_options['postcode'],
			"afm" => $cart_options['afm'],
			"doy" => $cart_options['doy'],
			"phone" => ($cart_options['phone']? $cart_options['phone'] : $userArr['user_phone'] ),
			"payment_method" => $cart_options['payment_method'],
			"shipping" => $cart_options['shipping'],
			"order_name" => ($cart_options['order_name']? $cart_options['order_name'] : $userArr['user_name'] ),
			"order_surname" => ($cart_options['order_surname']? $cart_options['order_surname'] : $userArr['user_surname'] ),
			"order_phone" => ($cart_options['order_phone']? $cart_options['order_phone'] : $userArr['user_phone'] ),
			"order_mobile" => ($cart_options['order_mobile']? $cart_options['order_mobile'] : $userArr['user_mobile'] ),
			"order_address" => ($cart_options['order_address']? $cart_options['order_address'] : $cart_options['address'] ),
			"order_municipality" => $cart_options['order_municipality'],
			"order_city" => $cart_options['order_city'],
			"order_postcode" => $cart_options['order_postcode'],
			"Period" => $cart_options['Period'],
			"ExtraCharge" => $cart_options['ExtraCharge'],
		);
	}
	
	/**
	 * Fetches the last affected id, either by updating or Inserting a new record to DB
	 * @return number
	 */
	function getLastAffectedID()
	{
		return $this->lastAffectedID;
	}
	
	/**
	 * Magic Equation Solver... FTW!
	 * 
	 * Based in Postfix & RPN Calculation Algorithm,
	 * -It parses an InFix equation ( Human readable ).
	 * -assigns values to variables. 
	 * -Solves the Equation as it's formated to RPN
	 * -returns just the Mathematical result
	 * 
	 * $arr = array();
	 * $arr['test'] = 5;
	 * 
	 * $equ = "(( test + 1 ) * 3) ^ 2)/3";
	 * $equArr = array();
	 * $equArr[] = "(";
	 * $equArr[] = "test";
	 * $equArr[] = "+";
	 * $equArr[] = "test";
	 * $equArr[] = ")";
	 * $equArr[] = "*";
	 * $equArr[] = "3";
	 * $eshop->getCostByEquation($arr, $equ);
	 * $eshop->getCostByEquation($arr, $equArr);
	 * 
	 * @param array $equationVarsArr
	 * @param string $equation
	 * @return real
	 */
	function  getCostByEquation($equationVarsArr, $equation)
	{
		$equationArr = (is_array($equation)? $equation : $this->getEquationdeserialized($equation) );
		// assign variables
		for($i=0; $i<count($equationArr); $i++)
		{
			if($this->isOperator($equationArr[$i]))
			{
				continue;
			}
			if(is_numeric($equationArr[$i]))
			{
				continue;
			}
			$equationArr[$i] = $equationVarsArr[$equationArr[$i]];
		}
		// Formats the array to Postfix Format, so it is able to be calculated
		$equationArr = $this->getRPNFormat($equationArr);
		return $this->getPostfixValue($equationArr);
	}
	
	/**
	 * Formats the Array and returns it 
	 * according to the RPN algorithm
	 * 
	 * (1+2)-3 --> 1 2 + 3 -
	 * 
	 * @param array $numArr
	 * @return multitype:
	 */
	function getRPNFormat(&$numArr)
	{
		$postfixArr = array();
		$postfixOpArr = array();
		$postfixNumArr  = array();
		
		// Parse to Convert Infix to Postfix
		while(count($numArr)>0)
		{
			$tmpVal = $this->doPopFomStack($numArr);
			if($this->isOperatorNumerical($tmpVal))
			{
				$postfixOpArr[] = $tmpVal;
				continue;
			}
			if(is_numeric($tmpVal))
			{
				$postfixNumArr[] = $tmpVal;
				continue;
			}
			if($this->isLeftBracket($tmpVal))
			{
				$tmp = $this->getRPNFormat($numArr);
				if(is_array($tmp))
				{
					$postfixArr = array_merge($postfixArr, $tmp);
				}
				else
				{
					$postfixArr[] = $tmp;
				}
				continue;
			}
			if($this->isRightBracket($tmpVal))
			{
				break;
			}
		}
		// Frst Operation within parnthesis,
		// next Operations out of parenthesis
		$postfixArr = array_merge($postfixArr, $postfixNumArr);
		// operations out of parenthesis
		$postfixArr = array_merge($postfixArr, $postfixOpArr);
		return $postfixArr;
	}
	
	/**
	 * Receives a RPn formated array 
	 * and calculates its value. 
	 * according to Postfix Algorithm
	 * 
	 * @param array $numArr
	 * @return real
	 */
	function getPostfixValue(&$numArr)
	{
		while(count($numArr)>1)
		{
			$firstNum = $this->doPopFomStack($numArr);
			$seconNum = $this->doPopFomStack($numArr);
			$operant = "";
			for($i=0; $i<count($numArr); $i++)
			{
				if($this->isOperatorNumerical($numArr[$i]))
				{
					$operant = $numArr[$i];
					unset($numArr[$i]);
					break;
				}
			}
			$val = $this->getCalculatedValue($firstNum,$seconNum,$operant);
			$this->doPushToStack($numArr, $val);
		}
		return $numArr[0];
	}
	
	/**
	 * Deserializes a human redable equation to an array
	 * 
	 * @param string $equationStr
	 * @return array:string unknown
	 */
	function getEquationDeserialized(&$equationStr)
	{
		$tmpArr = array();
		$tmpStr = "";
		$equationStr = trim($equationStr);
		
		for($i=0; $i<strlen($equationStr); $i++)
		{
			// Filter White sace, and brake the variable, or operator
			if($equationStr[$i] == " ")
			{
				if(strlen($tmpStr)>0)
				{
					$tmpArr[] = $tmpStr;
					$tmpStr = "";
				}
				continue;
			}
			if($this->isOperator($equationStr[$i]))
			{
				// save Variable before save operator
				if(strlen($tmpStr)>0)
				{
					$tmpArr[] = $tmpStr;
					$tmpStr = "";
				}
				$tmpArr[] = $equationStr[$i];
				continue;
			}
			$tmpStr .= $equationStr[$i];
		}
		if(strlen($equationStr)>0)
		{
			$tmpArr[] = $tmpStr;
		}
		return $tmpArr;
	}
	
	function doPopFomStack(&$tmpArr)
	{
		return array_shift($tmpArr);
	}
	
	function doSwapElements(&$var1, &$var2)
	{
		$tmpVar = $var1;
		$var1 = $var2;
		$var2 = $tmpVar;
	}
	
	function doSwapStackFirstElements(&$tmpArr)
	{
		$tmpVar = $tmpArr[0];
		$tmpArr[0] = $tmpArr[1];
		$tmpArr[1] = $tmpVar;
		return $tmpArr;
	}
	
	function doPushToStack(&$tmpArr, $val)
	{
		return array_unshift($tmpArr, $val);
	}
	
	function getCalculatedValue(&$arg1, &$arg2, &$oper)
	{
		switch ($oper) {
			case '+':
				return (real) $arg1 + $arg2;
			case '-':
				return (real) $arg1 - $arg2;
			case '*':
				return (real) $arg1 * $arg2;
			case '/':
				return (real) $arg1 / $arg2;
			case '^':
				return (real) pow($arg1, $arg2);
		}
		return "".$arg1.$oper.$arg2;
	}
	
	function isOperatorNumerical($str)
	{
		return(($str=="^" || $str=="+" || $str=="-" || $str=="*" || $str=="/" )? true : false);
	}
		
	function isOperator($str)
	{
		return(($str=="^" || $str=="+" || $str=="-" || $str=="*" || $str=="/" || $str=="(" || $str==")" || $str=="[" || $str=="]" || $str=="{" || $str=="}")? true : false);
	}
	
	function isBracket($str)
	{
		return(($str=="(" || $str==")" || $str=="[" || $str=="]" || $str=="{" || $str=="}")? true : false);
	}
	
	function isLeftBracket($str)
	{
		return(($str=="(" || $str=="[" || $str=="{" )? true : false);
	}
	
	function isRightBracket($str)
	{
		return(( $str==")" || $str=="]" || $str=="}")? true : false);
	}
	
	//END
}//END CLASS
?>