<?php
include("../../../manage/init.php");//load from manage!!!!


if ($loaded_modules['eshop']) 
{
	$eshop = new eshop();
	$orders = $eshop->searchOrders(array("debug"=>0,'results_per_page'=>20,'return'=>'paginated','page'=>1,'getTotalAmount'=>1,'searchFields'=>$searchFields,'missingFields'=>$missing_data,'getUserDetails'=>1,'searchFieldsGT'=>$searchFieldsGT,
	'searchFieldBETWEEN'=>$searchFieldsBetween,'searchFieldsLike'=>$searchFieldsLike,'orderby'=>'amount','way'=>'ASC','filters'=>$filters));
	$smarty->assign("orders",$orders);


	
$sql->db_Select("eshop_orders","MAX(amount) as maxPrice");
$a = execute_single($sql);
		$smarty->assign("maxPrice",ceil($a['maxPrice']));
	$smarty->assign("payment_methods",$eshop->paymentMethods(0,array('fields'=>'id,title')));
	$smarty->assign("status_codes",$eshop->orderStatusCodes(array('simplify'=>1)));
	$smarty->assign("ordersSum",$eshop->ordersTotal);
}//END MODULE


$smarty->assign("menu",$loaded_modules['eshop']['name']);
$smarty->assign("submenu","main");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/eshop/admin/main.tpl");
$smarty->display("admin/home.tpl");
$smarty->clear_all_assign();
?>