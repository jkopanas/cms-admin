<?php 
include($_SERVER['DOCUMENT_ROOT']."/init.php");

$orderid = $_SESSION['order'];
$sql->db_Select("eshop_orders","*","id = $orderid AND status = 1");
if ($sql->db_Rows()) {
$order = execute_single($sql);
$order_details = form_settings_array($order['details'],'###',":::");
$sql->db_Update("eshop_orders","status = 2,date_added = '".time()."' WHERE id = $orderid");//UPDATE STATUS AND TIME TO MATCH REALITY. AN ORDER MIGHT HAVE STARTED AT A PREVIOUS DATE.
$sql->db_Update("eshop_cart","status = 2 WHERE order_id = $orderid");
if (is_array($_SESSION['cart']) AND !empty($_SESSION['cart'])) {
$shipping_method = eshop_shipping($order['shipping_method']);
$shipping_method['parent'] = eshop_shipping($shipping_method['parent']);
$smarty->assign("total_price_no_vat",array_sum($total_prices));
$smarty->assign("vat_price",($loaded_modules['eshop']['settings']['vat']*(array_sum($total_prices)/100)));
$smarty->assign("items",$_SESSION['cart']);
$smarty->assign("orderid",$orderid);
$smarty->assign("payment_method",eshop_payment_methods($order['payment_method']));
$smarty->assign("shipping_method",$shipping_method);
$smarty->assign("details",$order_details);
}//END CHECK
else {
header("Location: index.html");
exit();
}
	//SEND EMAIL TO USER
$mailer = new PHPMailer();
$mailbody = $smarty->fetch("modules/users/email_order.tpl");
$mailer->IsHTML(true);
$mailer->From = COMPANY_EMAIL;  // This HAS TO be your gmail address or it won't work!
$mailer->FromName = COMPANY_NAME; // This is the from name in the email, you can put anything you like here
$mailer->Subject = $loaded_modules['eshop']['settings']['order_mail_subject'];
$mailer->Body = $mailbody;
$mailer->CharSet ="utf-8";
$mailer->AddAddress($order['email']);
$mailer->AddBCC('mbouclas@gmail.com');
$mailer->Send();
//SEND EMAIL TO CUSTOMER
$mailer = new PHPMailer();
$mailbody = $smarty->fetch("modules/users/email_order_admin.tpl");
$mailer->IsHTML(true);
$mailer->From = COMPANY_EMAIL;  // This HAS TO be your gmail address or it won't work!
$mailer->FromName = COMPANY_NAME; // This is the from name in the email, you can put anything you like here
$mailer->Subject = $loaded_modules['eshop']['settings']['order_mail_subject_admin'];
$mailer->Body = $mailbody;
$mailer->CharSet ="utf-8";
$mailer->AddAddress(COMPANY_EMAIL);
$mailer->AddBCC('mbouclas@gmail.com');
$mailer->Send();

//SEND SMS
if ($order_details['order_mobile']) {
$mailer = new PHPMailer();
$smarty->assign("mobile",$order_details['order_mobile']);
$smarty->assign("order",$order);
$mailbody = $smarty->fetch("modules/users/sms_body.tpl");
$mailer->IsHTML(false);
$mailer->From = COMPANY_EMAIL;  // This HAS TO be your gmail address or it won't work!
$mailer->FromName = COMPANY_NAME; // This is the from name in the email, you can put anything you like here
$mailer->Subject = "";
$mailer->Body = $mailbody;
$mailer->CharSet ="utf-8";
$mailer->AddAddress('sms@messaging.clickatell.com');
$mailer->AddBCC('mbouclas@gmail.com');
$mailer->Send();
}

//CLEAR SESSION VARIABLES
unset($_SESSION['cart']);
header("Location: transaction_complete.html");
exit();
//	echo "[OK]";
}//END ROWS
else {
header("Location: index.html");
exit();
}

?>