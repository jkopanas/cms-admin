<?php
include(ABSPATH."/includes/xajax/xajax.inc.php");
$xajax = new xajax("","xajax_",'utf-8');
global $smarty;

$xajax->registerFunction("delete_bookmark");
$xajax->registerFunction("add_bookmark");
$xajax->registerFunction("set_gallery");
$xajax->registerFunction("set_featured");
$xajax->registerFunction("set_related");
$xajax->registerFunction("set_trivia_featured");
$xajax->registerFunction("deleteRelated");
$xajax->registerFunction("deleteFeatured");
$xajax->registerFunction("autocomplete");
$xajax->registerFunction("get_search_categories");
$xajax->registerFunction("set_extra_fields");


function autocomplete($input,$target)
{
	global $sql,$smarty;
	$objResponse = new xajaxResponse();
	$len = strlen($input);
		if ($len)
	{
		$sql->db_Select("gallery_ingredients","distinct(base)","base like '$input%' ORDER BY base");
		$suggest = execute_multi($sql,0);
		
		$smarty->assign("auto",$suggest);
		$smarty->assign("target","$target");
		$message = $smarty->fetch("modules/gallery/admin/autocomplete_list.tpl");
		
		
	}
	else {
		$message = "No results";
	}
	$objResponse->addAssign("inr_display","innerHTML",$message);
	$objResponse->addScript("setTimeout(\"document.getElementById('inr_display').innerHTML=''\",3500);");
	return $objResponse;
}

function delete_bookmark($id,$res_div)
{
	global $smarty,$lang;
	$sql = new db();
	$sql->db_Delete("bookmarks","galleryid = '$id' AND uid = ".ID);
	$objResponse = new xajaxResponse();
    $objResponse->addAssign($res_div,"innerHTML","");
    return $objResponse;
}//END FUNCTION

function deleteRelated($dest,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
	$sql->db_Delete("gallery_links","gallery_source = ".$id." AND gallery_dest = ".$dest);
	$smarty->assign("links",get_related_gallery($id,DEFAULT_LANG));//assigned template variable links
	$message = $smarty->fetch("modules/gallery/admin/related_gallery_table.tpl");
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function add_bookmark($aFormValues,$res_div)
{
	global $smarty,$lang;
	$sql = new db();
	foreach ($aFormValues['ids'] as $key => $val)
	{
		$date_added = time();
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("bookmarks","'$key','$date_added', '".ID."'");			
		}
	}//END OF WHILE
	
	$objResponse = new xajaxResponse();
    $objResponse->addAssign($res_div,"innerHTML","");
    return $objResponse;
}//END FUNCTION

function set_gallery1($aFormValues,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
if (!$id) {
$id = $aFormValues['id'];	
}
	foreach ($aFormValues['ids'] as $key => $val)
	{
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("gallery_links",$id.",".$key.",'$orderby'");
			if ($aFormValues['bidirectional-'.$key] == 1) 
			{
				$sql->db_Insert("gallery_links",$key.",".$id.",'$orderby'");
			}
		}
	}//END OF WHILE

	$smarty->assign("links",get_upselling($id));//assigned template variable links
	$message = $smarty->fetch("modules/gallery/admin/related_gallery_table.tpl");
    $objResponse->addAssign("cat_results","innerHTML",'');
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function set_featured($aFormValues,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
	$cat = $aFormValues['category'];
	foreach ($aFormValues['ids'] as $key => $val)
	{
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("gallery_featured","'".$key."','$cat','','cat'"); 
			if ($aFormValues['bidirectional-'.$key] == 1) 
			{
				$gallery = get_gallery($key,DEFAULT_LANG);
				$bi_id = $gallery['catid'];

				$sql->db_Insert("gallery_featured","'".$key."','$bi_id','','cat'"); 
			}

		}
	}//END OF WHILE
	
	$smarty->assign("links",get_featured_gallery($cat,"cat",DEFAULT_LANG));//assigned template variable links
	$message = $smarty->fetch("modules/gallery/admin/related_gallery_table.tpl");
    $objResponse->addAssign("cat_results","innerHTML",'');
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function set_related($aFormValues,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
if (!$id) {
$id = $aFormValues['galleryid'];	
}
	foreach ($aFormValues['ids'] as $key => $val)
	{
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("gallery_links",$id.",".$key.",'$orderby'");
			if ($aFormValues['bidirectional-'.$key] == 1) 
			{
				$sql->db_Insert("gallery_links",$key.",".$id.",'$orderby'");
			}
		}
	}//END OF WHILE

	$smarty->assign("links",get_related_gallery($id,DEFAULT_LANG));//assigned template variable links
	$message = $smarty->fetch("modules/gallery/admin/related_gallery_table.tpl");
    $objResponse->addAssign("cat_results","innerHTML",'');
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function set_trivia_featured($aFormValues,$set_div)
{
	global $smarty,$lang,$sql,$id;
	$objResponse = new xajaxResponse();
	foreach ($aFormValues['ids'] as $key => $val)
	{
		if ($val == 1)//eliminates the check all/none box 
		{
			$sql->db_Insert("gallery_trivia","'".$id."','$key'"); 

		}
	}//END OF WHILE
$trivia = get_trivia($id,$letter="a",DEFAULT_LANG);
	$smarty->assign("links",$trivia['related']);//assigned template variable links
	$message = $smarty->fetch("modules/gallery/admin/related_gallery_table.tpl");
    $objResponse->addAssign("cat_results","innerHTML",'');
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function deleteFeatured($dest,$set_div)
{
	global $smarty,$lang,$sql,$id,$cat;
	$objResponse = new xajaxResponse();
	$cat = ($cat) ? $cat : 0;
	$sql->db_Delete("gallery_featured","galleryid = ".$dest." AND categoryid = ".$cat);
	$smarty->assign("links",get_featured_gallery($cat,"cat",DEFAULT_LANG));//assigned template variable links
	$message = $smarty->fetch("modules/gallery/admin/related_gallery_table.tpl");
    $objResponse->addAssign($set_div,"innerHTML",$message);
    return $objResponse;
}

function get_search_categories($aFormValues,$res_div,$mode)
{
	global $smarty,$lang,$id,$sql,$in_form;

	$search_tables = "gallery ";
$fields = "gallery.id,gallery.active";

if ($mode == "cat") 
{
	$sql->db_Select("gallery INNER JOIN gallery_page_categories ON (gallery.id=gallery_page_categories.galleryid)","gallery.id","catid = '".$aFormValues['cat']."' AND main ='Y'");
//$message_q = $sql->db_Rows();
$tmp = execute_multi($sql);
	for ($i=0;count($tmp) > $i;$i++)
	{
	
	$gallery[$i] = get_gallery($tmp[$i]['id'],DEFAULT_LANG);	
	}//END OF FOR

}//EDN OF CATEGORY SEARCH
elseif ($mode == "quick")
{
$aFormValues['substring'] = trim($aFormValues['substring']);


 $condition[] = "gallery_lng.title LIKE '%".$aFormValues["substring"]."%'";

 $condition[] = "gallery_lng.description LIKE '%".$aFormValues["substring"]."%'";

 $condition[] = "gallery_lng.full_description LIKE '%".$aFormValues["substring"]."%'";
 if (!empty($condition))
{ 
	$search_condition .= "(".implode(" OR ", $condition).")"." AND ";
	$fields .= " ,gallery_lng.title ";
$search_tables .= "INNER JOIN gallery_lng ON (gallery.id=gallery_lng.galleryid)";
}//END OF IF
$search_condition .= "
gallery.active IN (0,1) 
GROUP BY gallery.id
ORDER BY date_added DESC";
$sql->db_Select($search_tables,$fields,"$search_condition");
$gallery = execute_multi($sql,0);
}//END OF KEYWORD SEARCH
if (count($gallery) > 0) 
{
	$smarty->assign("gallery",$gallery);
	$smarty->assign("query",$message_q);
	if ($in_form) {
		$smarty->assign("in_form",$in_form);
	}
	$message = $smarty->fetch("modules/gallery/admin/pop_search_results.tpl");
}
else {
	$message = $lang['txt_no_gallery'];
}
	$objResponse = new xajaxResponse();
    $objResponse->addAssign($res_div,"innerHTML","");
    $objResponse->addAssign($res_div,"innerHTML",$message);
    return $objResponse;
}

function set_extra_fields($catid,$resdiv,$cid)
{
	global $sql,$smarty;
	$smarty->assign("extra_fields",get_extra_fields_gallery("Y",DEFAULT_LANG,$cid,$catid));
	$message = $smarty->fetch("common/extra_fields_modify.tpl");

	$objResponse = new xajaxResponse();
    $objResponse->addAssign($resdiv,"innerHTML","");
    $objResponse->addAssign($resdiv,"innerHTML",$message);
    return $objResponse;
}

$xajax->processRequests();
$smarty->assign("ajax_requests",$xajax->getJavascript(URL)); 
?>