<?php
include(ABSPATH."/modules/gallery/func.php");

$cat = ($_POST['cat']) ? $_POST['cat'] : $_GET['cat'];
$cat =check_cat($cat);
//Check if the sort field passed is valid
$var = array_flip ($sort_fields);
$result = in_array($_GET['sort'], $var, TRUE);
$sort = ($result == 1) ? $_GET['sort'] : DEFAULT_SORT;
//End check
$direction = ($_GET['sort_direction'] == 0) ? 0 : 1;
$selected = ($_GET['sort']) ? $sort : DEFAULT_SORT;
$order_by = ($_GET['sort_direction'] == 1) ? "desc" : "asc";

$featured = get_featured($cat,"cat",FRONT_LANG,1);//Featured gallery
$subcategories = get_cat($cat,FRONT_LANG);//Any possible subcategories
$nav = cat_nav($cat,FRONT_LANG);// Category navigation menu
$gallery = get_gallery($cat,$selected,$order_by,FRONT_LANG,"cat",1);//gallery found under the category
for ($i=0;count($gallery) > $i;$i++)
{
	$tmp = filter_exta_fields(1,$gallery[$i]['id'],FRONT_LANG);
	$gallery[$i]['price'] = $tmp['value'];//add the price to the gallery
}//END OF FOR


?>