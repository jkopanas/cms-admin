<?php
include("../../../manage/init.php");//load from manage!!!!
if ($gallery_module = module_is_active("gallery",1,1,0)) 
{
	$module_path = URL."/".$gallery_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",URL."/".$gallery_module['folder']."/admin");

$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];
$category = (empty($cat)) ? 0 : get_gallery_category($cat,DEFAULT_LANG);
$parentid = ($_GET['action']) ? $category['parentid'] : $cat;

$active = "0,1";
$categoryid = $category['categoryid'];

 $sql->db_Select("gallery_page_categories
 INNER JOIN gallery ON (gallery_page_categories.galleryid=gallery.id)
 INNER JOIN gallery_categories ON (gallery_page_categories.catid=gallery_categories.categoryid)",
 "gallery_page_categories.galleryid","(gallery_categories.categoryid_path LIKE '%$categoryid/%' 
 OR `gallery_categories`.categoryid_path LIKE '%/$categoryid' OR gallery_categories.categoryid_path = '$categoryid') 
 AND gallery_page_categories.main = 'Y' AND gallery.active IN ($active) ORDER BY id DESC ");

 
$tmp = execute_multi($sql);
 $lang = DEFAULT_LANG;

 for ($i=0;count($tmp) > $i;$i++)
 {
 	$gallery[] = get_gallery($tmp[$i]['galleryid'],$lang);
 }
$smarty->assign("gallery",$gallery);

}

$smarty->assign("allcategories",get_all_gallery_categories(DEFAULT_LANG));//assigned template variable allcategories
$smarty->assign("MODULE_SETTINGS",$gallery_module['settings']);
$smarty->assign("menu",$gallery_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("catid",$cat);
$smarty->assign("parentid",$parentid);
$smarty->assign("cat",get_gallery_categories($cat,DEFAULT_LANG,0));//assigned template variable cat
$smarty->assign("nav",gallery_cat_nav($cat,DEFAULT_LANG));//assigned template variable a
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("current_category",get_gallery_category($cat,DEFAULT_LANG));//assigned template variable current_cat
$smarty->assign("include_file","modules/gallery/admin/category_gallery.tpl");
$smarty->display("admin/home.tpl");

?>