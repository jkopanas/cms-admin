<?php
include($_SERVER['DOCUMENT_ROOT']."/manage/init.php");
include("../functions.php");
$sql = new db();
$t = new textparse();
$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];
$cat = (empty($cat)) ? 0 : $cat;
$category = (empty($cat)) ? 0 : get_category($cat,DEFAULT_LANG);

if ($_POST['mode'] == "modify") 
{
$error_list = array();
if ($_POST['title'] == "") 
{  
 $error = 1;
 $error_list['category'] = $lang['error_category']; 
}//END OF IF
if ($error != 1) 
{

	$new_category = $t->formtpa($_POST['title']);
	$new_parentid = $cat;
	$new_meta_descr = $t->formtpa($_POST['meta_desc']);
	$new_meta_keyw = $t->formtpa($_POST['meta_keyw']);
	$new_description = $t->formtpa($_POST['desc']);
	$new_date_added = time();
	$new_orderby = $_POST['orderby'];
	$sql->db_Insert("gallery_categories","'','$new_parentid','','$new_orderby','','$new_date_added'");
	$new_categoryid = mysql_insert_id();
	$new_categoryid_path = ($category == 0) ? $new_categoryid : $category['categoryid_path']."/".$new_categoryid;
	$sql->db_Insert("gallery_categories_lng","'".DEFAULT_LANG."','$new_categoryid','$new_category','$new_description',
	'$new_meta_descr','$new_meta_keyw','$image'");
########### REPLICATE DATA FOR TRANSLATIONS ###############################
$countries = get_countries("Y",$trans="yes");
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert("gallery_categories_lng","'".$countries[$i]['code']."','$new_categoryid','$new_category','$new_description',
'$new_meta_descr','$new_meta_keyw','$image'");
}//END OF FOR
}//END OF IF

if ($_POST['image_folder'] == "temp") 
{  
 $newfile = "i_".$_POST['cat'].".jpg";
 $source = $_SERVER['DOCUMENT_ROOT'].PRODUCT_IMAGES."/icons/".$newfile;
 copy(IMAGES_PATH."/temp/".$_POST['newthumb'],$source);

 unlink(IMAGES_PATH."/temp/".$_POST['newthumb']);
 $newfile = PRODUCT_IMAGES."/icons/".$newfile; 
}//END OF IF
elseif ($_POST['image_folder'] == "products") 
{ 
  $newfile = PRODUCT_IMAGES."/icons/".$_POST['newthumb']; 
}//END OF ELSEIF
if ($newfile) 
{  
	$sql->db_Update("gallery_categories_lng","image = '$newfile' WHERE albumid = $new_categoryid AND code = '".DEFAULT_LANG."'"); 
}//END OF IF

	if ($sql->db_Update("gallery_categories","albumid_path = '$new_categoryid_path' WHERE albumid = '$new_categoryid'")) 
	{  
	 header("Location: gallery_modify.php?cat=$new_categoryid");
	 exit();
	}//END OF IF;
}//NO errors add category
}//END OF IF POST MODIFY
$smarty->assign("error_list",$error_list);//assigned template variable error_list


editor("description","description");
$smarty->assign("pop_edit",1);//assigned template variable pop_edit
$smarty->assign("imanager",1);//assigned template variable imanager
$smarty->assign("nav",gallery_cat_nav($cat,DEFAULT_LANG_ADMIN));//assigned template variable a
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("action_title",$lang['add']." ".$lang['photo_gallery']);//assigned template variable action_title
$smarty->assign("category",$cat);//assigned template variable category
$smarty->assign("include_file","modules/gallery/admin/gallery_add.tpl");
$smarty->display("admin/home.tpl");

?>