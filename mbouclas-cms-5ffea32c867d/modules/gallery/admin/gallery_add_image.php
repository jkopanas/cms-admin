<?php
include($_SERVER['DOCUMENT_ROOT']."/manage/init.php");
include("../functions.php");
$sql = new db();
$t = new textparse();
if ($_GET['cat'] == 0) {
	$cat = 0;
}
else {
	$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];
}

$a = load_module_prefs("gallery_settings","gallery_settings_lng");



############ DELETE IMAGE #########################
if ($_GET['m'] == "del") 
{
	$sql->db_Delete("gallery_images","id = ".$_GET['img']);
	$sql->db_Delete("gallery_images_lng","imageid = ".$_GET['img']);
	header("Location: $PHP_SELF?cat=".$_GET['cat']);
}
################ UPDATE IMAGES ##########################
if ($_POST['mode'] == "modify") 
{  
foreach ($_POST as $key => $val)
{
			if (strstr($key,"$")) 
		{
			$key = str_replace("$","-",$key);
			list($field,$newid)=split("-",$key);
			$val = $t->formtpa($val);
			$query = "$field = '$val' WHERE id = $newid";
			$sql->db_Update("gallery_images",$query);
			$sql->db_Update("gallery_images_lng","$field = '$val' WHERE imageid = $newid");
		}//END OF IF
}
// header("Location: product_modify_img.php?id=$id");
// exit();
}//END OF IF UPDATE EXISTING IMAGES



$smarty->assign("menu","gallery");
$smarty->assign("submenu","add_images");
$smarty->assign("pop_edit",1);//assigned template variable pop_edit
$smarty->assign("catid",$cat);//assigned template variable imanager

$smarty->assign("allcategories",get_albums(DEFAULT_LANG_ADMIN));//assigned template variable allcategories
$smarty->assign("images",get_album_photos($cat,DEFAULT_LANG_ADMIN,"no","no"));
$smarty->assign("album",get_album($cat,DEFAULT_LANG_ADMIN));//assigned template variable cat
$smarty->assign("cat",get_album_subs($cat,DEFAULT_LANG_ADMIN));//assigned template variable cat
$smarty->assign("featured",get_featured($cat,"cat",DEFAULT_LANG_ADMIN));//assigned template variable featured
$smarty->assign("nav",gallery_cat_nav($cat,DEFAULT_LANG_ADMIN));//assigned template variable a
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("category",$cat);//assigned template variable category
$smarty->assign("current_category",$current_category);//assigned template variable current_cat
$smarty->assign("include_file","modules/gallery/admin/uploader.tpl");
$smarty->display("admin/home.tpl");

?>