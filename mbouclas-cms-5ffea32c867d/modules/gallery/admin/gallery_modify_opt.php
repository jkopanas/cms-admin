<?php
include("../../../manage/init.php");//load from manage!!!!

if ($gallery_module = module_is_active("gallery",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$gallery_module['folder']."/admin");
	
$t = new textparse();
$id = (!empty($_GET['id'])) ? $_GET['id'] : $_POST['id'];
$cid = (!empty($_GET['cid'])) ? $_GET['cid'] : $_POST['cid'];

if ($_POST['action'] == "new_class") 
{
	$tmp = array();
	foreach ($_POST as $key => $val)
	{
		if ($key == "new_list") 
		{
			foreach ($val as $k => $v)
			{
				foreach ($v as $a => $b)
				{
//					echo "INSERT INTO gallery_claass_options VALUES ('',$cid,$k --- $b<br>";
					$tmp[$a][$k] = $b;
//					echo "Key : $key --- VAL : $val -- V : $v --- k : $k --- a : $a --- b : $b<br>";
				}
			}
		}


	}
	//Insert into db
		$orderby = $t->formtpa($_POST['orderby']);
		$active = $t->formtpa($_POST['active']);
		$class_title = $t->formtpa($_POST['class']);
		$class_text = $t->formtpa($_POST['classtext']);
		//Class First
		$sql->db_Insert("gallery_classes","'','$id','$orderby','$active'");
		$cid = mysql_insert_id();
		$sql->db_Insert("gallery_class_lng","'".DEFAULT_LANG."',$cid,'$class_title','$class_text'");
		if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
		{  
			$countries = get_countries(2,$trans="yes");
			for ($i=0;count($countries) > $i;$i++)
			{
				$sql->db_Insert("gallery_class_lng","'".$countries[$i]['code']."',$cid,'$class_title','$class_text'");
			}
		}
	for ($i=0;count($tmp) > $i;$i++)
	{
		
		$pos = $tmp[$i]['pos'];
		$title = $t->formtpa($tmp[$i]['title']);
		$price_modifier = $tmp[$i]['price_modifier'];
		$modifier_type = $tmp[$i]['modifier_type'];
		
		//OPTIONS
		$sql->db_Insert("gallery_class_options","'',$cid,'$pos','$price_modifier','$modifier_type'");
		$o_id = mysql_insert_id();
		$sql->db_Insert("gallery_class_options_lng","'".DEFAULT_LANG."',$o_id,'$title'");
		if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
		{  
			$countries = get_countries(2,$trans="yes");
			for ($j=0;count($countries) > $j;$j++)
			{
				$sql->db_Insert("gallery_claass_options_lng","'".$countries[$j]['code']."',$o_id,'$title'");
			}//END OF FOR

		}//END OF IF
	}
	header("Location: gallery_modify_opt.php?id=$id&cid=$cid");
	exit();
}

	if ($eshop_module = module_is_active("eshop",1,1)) 
	{
		$gallery['details'] = eshop_product_details($id,'image');
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
		
	}

$allclasses = get_gallery_classes("no","include",DEFAULT_LANG,$id); 




$smarty->assign("global_classes",eshop_classes(0,$gallery_module['id'],1));
$smarty->assign("more_gallery",get_latest_gallery($gallery_module['settings']['latest_gallery'],DEFAULT_LANG,"no",$gallery['catid']));
$smarty->assign("image_categories",gallery_images_categories());
$smarty->assign("menu",$gallery_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","options");//USED ON SUBMENUS
$smarty->assign("id",$id);
$smarty->assign("cid",$cid);
}//END MODULE
$gallery = get_gallery($id,DEFAULT_LANG);
$links = get_related_gallery($id,DEFAULT_LANG);
$smarty->assign("all_classes",$allclasses);//assigned template variable all_classes
$smarty->assign("classes",get_class_gallery($id,"single",DEFAULT_LANG));//assigned template variable classes
$smarty->assign("links",$links);//assigned template variable links
$smarty->assign("action_title", $gallery['title']);//assigned template variable action_title
$smarty->assign("gallery",$gallery);//assigned template variable gallery
$smarty->assign("mode","modify");//assigned template variable mode
$smarty->assign("form_destination",$form_destination['gallery_modify']);//assigned template variable form_destination
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/gallery/admin/gallery_options.tpl");
$smarty->display("admin/home.tpl");

?>