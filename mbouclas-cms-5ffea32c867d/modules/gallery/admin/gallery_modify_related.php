<?php
include("../../../manage/init.php");//load from manage!!!!

if ($gallery_module = module_is_active("gallery",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$gallery_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
	
	

	
################################################## LOAD gallery ############################################################
	$gallery = get_gallery($id,DEFAULT_LANG);

	if (!empty($gallery)) 
	{  
	$sql->db_Select("gallery_page_categories","catid","galleryid = $id");
	$all_gallery_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_gallery_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_gallery_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_gallery",get_latest_gallery($gallery_module['settings']['latest_gallery'],DEFAULT_LANG,"no",$gallery['catid']));
	$smarty->assign("extra_fields",get_gallery_extra_fields("Y",DEFAULT_LANG,$id));

	$smarty->assign("id",$id);
	$smarty->assign("tags",get_gallery_tags($id,","));
	$smarty->assign("nav",gallery_cat_nav($gallery['catid'],DEFAULT_LANG));

		if ($manufacturers_module = module_is_active("manufacturers",1,1)) 
	{
		$smarty->assign("manufacturers_list",get_manufacturers(DEFAULT_LANG));
		$smarty->assign("manufacturers_module",$manufacturers_module);
		$gallery['mid'] = get_gallery_manufacturer($id);
	}
	############# END OF  MANUFACTURERS MODULE #####################################
	
	############# CHECK OUT FOR ESHOP MODULE #######################################
	
	if ($eshop_module = module_is_active("eshop",1,1)) 
	{
		$gallery['details'] = eshop_gallery_details($id);
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
		
	}

		if ($manufacturers_module = module_is_active("manufacturers",1,1)) 
	{
		$smarty->assign("manufacturers_list",get_manufacturers(DEFAULT_LANG));
		$smarty->assign("manufacturers_module",$manufacturers_module);
		$gallery['mid'] = get_gallery_manufacturer($id);
	}
	############# END OF  MANUFACTURERS MODULE #####################################
	
	############# CHECK OUT FOR ESHOP MODULE #######################################
	
	if ($eshop_module = module_is_active("eshop",1,1)) 
	{
		$gallery['details'] = eshop_gallery_details($id);
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
		
	}

	$smarty->assign("gallery",$gallery);
	$smarty->assign("gallery_module",$gallery_module);
	$smarty->assign("action","update");
	}//END OF gallery found
	else 
	{  
		header("Location: search.php"); 
		exit();
	}//END OF ELSE
################################################ END OF LOAD gallery #########################################

			####################### LOAD AJAX  ###########################
			include($_SERVER['DOCUMENT_ROOT']."/".$gallery_module['folder']."/admin/ajax_functions.php");
			###################### END OF AJAX #######################################	
			
}
$smarty->assign("related",get_related_gallery($id,DEFAULT_LANG));
$smarty->assign("USE_AJAX","modules/gallery/admin/gallery_ajax.tpl");
$smarty->assign("menu",$gallery_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","modify");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/gallery/admin/gallery_modify_related.tpl");
$smarty->display("admin/home.tpl");

?>