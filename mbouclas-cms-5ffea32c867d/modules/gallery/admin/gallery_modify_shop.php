<?php
include("../../../manage/init.php");//load from manage!!!!

if ($gallery_module = module_is_active("gallery",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$gallery_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

	if ($_POST['action'] == "modify") 
	{
		foreach ($_POST as $k => $v) 
		{
			$sql->db_Update("eshop_gallery_details","$k = '$v' WHERE galleryid = $id");
		}
	}
	
################################################## LOAD gallery ############################################################
	$gallery = get_gallery($id,DEFAULT_LANG);

	if (!empty($gallery)) 
	{  
	$sql->db_Select("gallery_categories","catid","galleryid = $id AND main = 'N'");
	$all_gallery_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_gallery_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_gallery_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_gallery",get_latest_gallery($gallery_module['settings']['latest_gallery'],DEFAULT_LANG,"no",$gallery['catid']));
	$smarty->assign("extra_fields",get_gallery_extra_fields("Y",DEFAULT_LANG,$id));

	$smarty->assign("id",$id);
	$smarty->assign("nav",gallery_cat_nav($gallery['catid'],DEFAULT_LANG));
	
	if ($eshop_module = module_is_active("eshop",1,1)) 
	{
		$gallery['details'] = eshop_product_details($id);
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
		
	}
	

	$smarty->assign("gallery",$gallery);
	$smarty->assign("gallery_module",$gallery_module);
	$smarty->assign("action","update");
	$smarty->assign("image_categories",gallery_images_categories());
	################################################ END OF LOAD gallery #########################################
	}//END OF gallery

}

$smarty->assign("menu",$gallery_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","eshop_options");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/eshop/admin/product_details.tpl");
$smarty->display("admin/home.tpl");

?>