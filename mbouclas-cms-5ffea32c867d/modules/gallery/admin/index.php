<?php
include("../../../manage/init.php");//load from manage!!!!

if ($gallery_module = module_is_active("gallery",1,1)) 
{
	$smarty->assign("latest_gallery",get_latest_gallery($gallery_module['settings']['latest_gallery_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",URL."/".$gallery_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$gallery_module['settings']);
	$smarty->assign("allcategories",get_all_gallery_categories(DEFAULT_LANG));//assigned template variable allcategories
//Take a sum of all products
$sql->db_Select("gallery","COUNT(*) as total_items");
$total_items = execute_single($sql);
$smarty->assign("total_items",$total_items['total_items']);
}


$smarty->assign("menu",$gallery_module['name']);
$smarty->assign("submenu","main");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/gallery/admin/main.tpl");
$smarty->display("admin/home.tpl");

?>