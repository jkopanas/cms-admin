<?php
include("../../../manage/init.php");//load from manage!!!!

if ($gallery_module = module_is_active("gallery",1,1)) 
{

	$smarty->assign("MODULE_FOLDER",URL."/".$gallery_module['folder']."/admin");
	$smarty->assign("allcategories",get_all_gallery_categories(DEFAULT_LANG));
	$smarty->assign("action",$_GET['action']);
	
	
	if ($_GET['action']) 
	{
		//Check for mass uploaded gallery
		$sql->db_Select("gallery","id","active = 2");
		if ($sql->db_Rows() > 0) 
		{
			$tmp_uploads = execute_multi($sql);
			for ($i=0;count($tmp_uploads) > $i;$i++)
			{
				$gallery[$i] = get_gallery($tmp_uploads[$i]['id'],DEFAULT_LANG);
			}
			$smarty->assign("gallery",$gallery);
		}//END OF UPLOADED gallery FOUND
	}//END OF REVIEW UPLOADED gallery
}//END OF MODULE



$smarty->assign("menu",$gallery_module['name']);
$smarty->assign("submenu","mass_insert");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/gallery/admin/mass_insert.tpl");
$smarty->display("admin/home.tpl");

?>