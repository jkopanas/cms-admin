<?php
session_start();
//make sure that it is a GET/POST data and not SESSION
$check_search = ($_POST['search']) ? $_POST['search'] : $_GET['search'];
if (empty($check_search))//IF SESSION data and not GET/POST it is a new search and should be cleared 
{  
unset($_SESSION['search_data']); 
unset($posted_data);
}//END OF IF

include("../../../manage/init.php");//load from manage!!!!
if ($gallery_module = module_is_active("gallery",1,1,1)) 
{

####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ##########################################
//gallery_r($_SESSION);
	$module_path = URL."/".$gallery_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",URL."/".$gallery_module['folder']."/admin");
	
//	gallery_r($_SESSION);
//user is posting variables for a new search
if ($eshop_module = module_is_active("eshop",1,1)) {
	$smarty->assign("eshop_module",$eshop_module);
}

	
	
$smarty->assign("extra_fields",get_gallery_extra_fields("Y",DEFAULT_LANG));//assigned template variable extra_field
	

}


if ($_POST['search']) 
{  
 $posted_data = array();//clear array
 $_SESSION['search_data'] = "";//clear the session to add the new data
 foreach ($_POST as $key => $value) //build the array that will hold the search data
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 $_SESSION['search_data'] = $posted_data;//setup the session
 $smarty->assign("search",1);
}//END OF IF
else//Pagination or refresh
{  
$posted_data = array();//setup a a clean array
$posted_data = $_SESSION['search_data'];//assign data to it
if ($_GET['page']) {$posted_data['page'] = $page;}
}

if ($_GET['tag']) 
{
 $posted_data = $_GET;//clear array
 $_SESSION['search_data'] = "";//clear the session to add the new data
  $_SESSION['search_data'] = $posted_data;//setup the session
 $smarty->assign("search",1);
}

//$posted_data = ($_POST) ? $_POST : $_GET;
if ($posted_data) {
	$res = gallery_search($posted_data,$gallery_module,$db_info,DEFAULT_LANG,0,0,1);
}

$smarty->assign("data",$res['data']);
$smarty->assign("results",$res['results']);

$smarty->assign("allcategories",get_all_gallery_categories(DEFAULT_LANG));
//gallery_r(get_gallery(65,DEFAULT_LANG));
$smarty->assign("MODULE_SETTINGS",$gallery_module['settings']);
$smarty->assign("USE_AJAX","modules/gallery/admin/gallery_ajax.tpl");
$smarty->assign("menu",$gallery_module['name']);
$smarty->assign("submenu","search");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/gallery/search_form.tpl");
$smarty->display("admin/home.tpl");

?>