<?php
function check_gallery_cat($cat) 
{ 
	$sql = new db();
	$sql->db_Select("gallery_categories","categoryid","categoryid = $cat");
	if ($sql->db_Rows() >0) 
	 {  
	 	return $cat; 
	 }//END OF IF 
	 else 
	 {  
	 header("Location: index.php");
	 exit();
	 }//END OF ELSE
}//END OF FUNCTION check_cat

function check_gallery($id) 
{ 
	$sql = new db();
	$sql->db_Select("gallery","id","id = $id AND active = 1");
	$r = $sql->db_Fetch();
	if ($sql->db_Rows() > 0)
	{
		return $r['id'];
	}
	else 
	 {  
	 	header("Location: index.php");
	 	exit(); 
	 }//END OF ELSE 
}//END OF FUNCTION check_gallery

function check_gallery_image($imageid) 
{ 
	global $sql;
	$sql->db_Select("gallery_images","*","id = $imageid AND available = 1");
	if ($sql->db_Rows() > 0)
	{
	 	return execute_single($sql,0);
	}
	else 
	 {  
		return 0;
	 }//END OF ELSE 
}//END OF FUNCTION check_image

function add_gallery_extra_field_values($data,$galleryid,$lang) 
{ 
		global $sql;
		while(list($key,$val)=each($data)) 
		{
		if (strstr($key,"-")) 
		{
			list($field,$id)=split("-",$key);
			if ($val) 
			{  
			$query = "'$galleryid','$id','$val','$lang'";
			$sql->db_Insert("gallery_extra_field_values",$query);
			}//END OF IF not empty field
		}//END OF IF
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function upd_gallery_extra_field_values($data,$galleryid,$lang) 
{ 
		global $sql;
		$t = new textparse();
		foreach ($data as $key => $val)
		{
		if (strstr($key,"-")) 
		{

			$val = $t->formtpa($val);
			list($field,$id)=split("-",$key);
			$sql->db_Select("gallery_extra_field_values","galleryid","galleryid = '$galleryid' AND fieldid = '$id' AND code = '$lang'");
			if ($sql->db_Rows() > 0) 
			{ 
			$query = "value = '$val' WHERE galleryid = '$galleryid' AND fieldid = '$id' AND code = '$lang'";
			
			$sql->db_Update("gallery_extra_field_values",$query);	
			}//END OF IF
			else 
			{ 
			if ($val) 
			 {  
			$sql->db_Insert("gallery_extra_field_values","'$galleryid','$id','$val','$lang'"); 
			}//END OF IF 
			}//END OF ELSE
		}//END OF IF
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function get_extra_fields_gallery($active,$lang,$galleryid=0,$catid=0) 
{ 
global $sql;
$fields = "gallery_extra_fields.fieldid, gallery_extra_fields.active,gallery_extra_fields.var_name, gallery_extra_fields.type, gallery_extra_fields_lng.field, gallery_extra_fields_lng.value";
$tables = "gallery_extra_fields INNER JOIN gallery_extra_fields_lng ON (gallery_extra_fields.fieldid = gallery_extra_fields_lng.fieldid)";
if ($catid) 
{
	//Check if exists at all
	$sql->db_Select("gallery_extra_field_categories","catid","catid = $catid");
	if ($sql->db_Rows() > 0)
	{
		$tables .="INNER JOIN gallery_extra_field_categories ON (gallery_extra_field_categories.fieldid=gallery_extra_fields.fieldid)";
		$cat = " AND catid = $catid GROUP BY fieldid";
	}

}
if ($active == 0) 
{  
 $sql->db_Select($tables,$fields,"code = '$lang' $cat ORDER BY gallery_extra_fields.type DESC");
}//END OF IF
else 
{  
$sql->db_Select($tables,$fields,"active = '$active' AND code = '$lang' $cat"); 
}//END OF ELSE
//echo "SELECT $fields FROM $tables WHERE code = '$lang' ORDER BY gallery_extra_fields.type DESC";
if ($sql->db_Rows() > 0)
 {

  $fields = execute_multi($sql,1);
  if ($galleryid) 
  {
  	  for ($i=0;count($fields) > $i;$i++)
 	 {
 	 	$sql->db_Select("gallery_extra_field_values","*","code = '$lang' AND galleryid = $galleryid AND fieldid = ".$fields[$i]['fieldid']);
// 	 	echo "SELECT * FROM gallery_extra_field_values WHERE code = '$lang' AND galleryid = $galleryid AND fieldid = ".$fields[$i]['fieldid'];
 	 	$values = execute_single($sql);
  		$fields[$i]['values'] = $values;
 	 }

  }
  //GET AVAILABLE CATEGORIES
     for ($i=0;count($fields) > $i;$i++)
 	 {
			$fields[$i]['categories'] = get_gallery_extra_field_categories($fields[$i]['fieldid']);
 	 }
  	return $fields;
  	}
  
}//END OF FUNCTION get_extra_fields

function get_gallery_extra_field_categories($id)
{
	global $sql;
	 	$sql->db_Select("gallery_extra_field_categories","catid","fieldid = ".$id);
 	 	if ($sql->db_Rows() > 0) 
 	 	{
 	 		return  execute_multi($sql);
 	 	}
}

function get_gallery_extra_fields($id,$lang,$hidden=0) 
{ 
	global $sql;
	$t = new textparse();
	 	//LOAD ANY POSSIBLE EXTRA FIELDS
	$sql->db_Select("gallery_extra_fields INNER JOIN gallery_extra_field_values ON (gallery_extra_fields.fieldid = gallery_extra_field_values.fieldid)",
	"gallery_extra_fields.fieldid,gallery_extra_fields.type,gallery_extra_fields.var_name","galleryid = $id AND code = '$lang' AND active = 'Y'");
	for ($i=0;$sql->db_Rows() > $i;$i++)
	{
		$r = $sql->db_Fetch();
if ($r['type'] != "hidden") //do no display hidden fields
 {  
$fields[] = filter_gallery_exta_fields($r['fieldid'],$id,$lang);
 }//END OF IF $r['type'];
		

	}//END OF FOR
	return $fields;
}//END OF FUNCTION get_gallery_extra_fields

function gallery_extra_fields($id,$lang) 
{ 
	$sql = new db();
	$t = new textparse();
	 	//LOAD ANY POSSIBLE EXTRA FIELDS
	$sql->db_Select("gallery_extra_fields INNER JOIN gallery_extra_field_values ON (gallery_extra_fields.fieldid = gallery_extra_field_values.fieldid)",
	"gallery_extra_fields.fieldid","id = $id AND active = 'Y'");
//	echo "SELECT gallery_extra_fields.fieldid FROM gallery_extra_fields INNER JOIN gallery_extra_field_values ON (gallery_extra_fields.fieldid = gallery_extra_field_values.fieldid) WHERE galleryid = $id AND active = 'Y'";
	for ($i=0;$sql->db_Rows() > $i;$i++)
	{
		$r = $sql->db_Fetch();
		$fields[$i] = filter_gallery_exta_fields($r['fieldid'],$id,$lang);
	}//END OF FOR
	return $fields;
}//END OF FUNCTION get_gallery_extra_fields

function get_gallery_extra_field_values($galleryid,$lang) 
{ 
  $sql = new db();
  $sql->db_Select("gallery_extra_field_values","galleryid,value","galleryid = $galleryid AND code = '$lang'");
  return execute_multi($sql,1);
}//END OF FUNCTION get_extra_field_values

function filter_gallery_exta_fields($id,$galleryid,$lang) 
{ 
	$sql = new db();
	$fields = "gallery_extra_fields.fieldid, gallery_extra_fields.type,gallery_extra_fields.var_name, gallery_extra_field_values.value, gallery_extra_field_values.galleryid, gallery_extra_fields_lng.field";
	$tables = "gallery_extra_fields INNER JOIN gallery_extra_field_values ON (gallery_extra_fields.fieldid = gallery_extra_field_values.fieldid)
  			   INNER JOIN gallery_extra_fields_lng ON (gallery_extra_fields.fieldid = gallery_extra_fields_lng.fieldid)";
	$q = "gallery_extra_fields.fieldid = $id AND gallery_extra_fields_lng.code = '$lang' AND galleryid = '$galleryid'";
	$sql->db_Select($tables,$fields,$q);
//echo "SELECT $fields FROM $tables WHERE $q<br><br>";
	return execute_single($sql,1);
}//END OF FUNCTION filter_exta_fields

function get_gallery($id,$lang,$active="no",$efields=0,$type=0,$module_settings=0,$options=0) 
{ 
	global $loaded_modules,$sql;
  $active = ($active == "no") ? "" : "AND gallery.active = $active";
  $t = new convert();
  $fields = "gallery.uid,gallery.sku, gallery.orderby, gallery.active, gallery.date_added, gallery.id, gallery_lng.title, gallery_lng.description,
   gallery_lng.full_description, gallery_lng.meta_desc, gallery_lng.meta_keywords";
  $tables = "gallery 
INNER JOIN gallery_lng ON (gallery.id=gallery_lng.galleryid)";
  $q = "gallery.id = $id AND gallery_lng.code = '$lang' $active";
//echo "SELECT $fields FROM $tables WHERE $q<br><br>";
  $sql->db_Select($tables,$fields,$q);
if ($sql->db_Rows() > 0) 
{  
	$gallery = execute_single($sql,1);
	$tmp_cat = get_gallery_cat($id);//gallery category
	$gallery['catid'] = $tmp_cat['catid'];//Backward compatibility
	$gallery['cat'] = get_gallery_category($gallery['catid'],$lang);
	$gallery['category'] = $gallery['cat']['category'];//Backward compatibility
	$gallery['nav_cat'] =  gallery_cat_nav($gallery['catid'],FRONT_LANG);
	$gallery['extra_fields'] = gallery_extra_fields($id,$lang);
	$gallery['user'] = get_users($gallery['uid']);
	if ($efields) 
	{
		$gallery['efields'] = simplify_efields(get_gallery_extra_fields($id,$lang));
	}
	if ($module_settings['use_gallery_tags']) 
	{
		$gallery['tags'] = get_gallery_tags($id,",");
		echo "ASd|";
	}
	$gallery['tags'] = get_gallery_tags($id,",");
	if (in_array("eshop",$loaded_modules)) 
	{
		$gallery['eshop'] = eshop_product_details($id,'image');
		$gallery['global_classes'] = eshop_classes(0,$module_settings['id'],$options,'title');
	}
	if (in_array("locations",$loaded_modules)) 
	{
		$gallery['locations'] = locations_gallery_location($id);
	}
//Get global classes if any

		$thumb = get_gallery_detailed_images($id,0,0);
		$gallery['image'] = $thumb[0]['thumb'];//gallery Thumb (backward compatibility
		$gallery['image_full'] = $thumb[0];//gallery Thumb (backward compatibility
	return $gallery;
}//END OF FOUND gallery
else 
{  
return 0; 
}//END OF ELSE
}//END OF FUNCTION get_gallery

function gallery_cat_nav($cat,$lang,$table="",$fields="",$tables="") 
{ 
	global $sql;
	$table_lng = ($table == "") ? "gallery_categories_lng" : $table."_lng";
	$table = ($table == "") ? "gallery_categories" : $table;
	$sql->db_Select("$table","categoryid_path","categoryid = $cat") ;
	$r = $sql->db_Fetch();
	$tmp = ($sql->db_Rows() > 0) ? explode("/",$r['categoryid_path']) : 0;


	if (is_array($tmp) and count($tmp) > 0) 
	{
		//found subcategory
		$fields = ($field == "") ? "$table_lng.category, $table_lng.image, $table.categoryid" : $fields;
		$tables = ($tables == "") ? "$table INNER JOIN $table_lng ON ($table.categoryid = $table_lng.categoryid)" : $tables;
		
		
	 	for ($i=0;count($tmp) > $i;$i++)
		{
//			echo "SELECT $fields FROM $tables WHERE $table.categoryid = ".$tmp[$i]." AND code = '$lang'";
		$sql->db_Select($tables,$fields,"$table.categoryid = ".$tmp[$i]." AND code = '$lang'");
		$ar[] = execute_single($sql,1);
		}//END OF FOR
	return $ar;
	}//END OF IF subgallery_categories found
	else 
	{  
	return 0; 
	}//END OF ELSE	
}//END OF FUNCTION cat_nav

function gallery_extra_fields_translations($id) 
{ 
	global $smarty,$sql;
$sql->db_Select("gallery_extra_fields","field","fieldid = $id");
$r = $sql->db_Fetch();
$field_name = $r['field'];
$countries = get_countries("Y",$trans="yes");
$class_lng = array();
$translations = array();

for ($i=0,$j=0,$d=0;count($countries) > $i;$i++)
{
	$sql->db_Select("gallery_extra_fields_lng","*","code = '".$countries[$i]['code']."' AND fieldid = $id");
	if ($sql->db_Rows() == 0) 
	{  
	 	$field_lng[$j]['code'] =  $countries[$i]['code'];
	 	$field_lng[$j]['country'] =  $countries[$i]['country'];
	 	$j++;
	}//END OF IF
	else 
	{  
	//see for possible translations
	 	$r = $sql->db_Fetch();
	 	$country_details = get_lang_name($r['code']);
	 	foreach ($r as $key => $value) 
	 	{
	 	   	if (!preg_match("[0-9]",$key)) 
	 	{
	 		$translations[$d][$key] = $value;
	 	}//END OF IF
	 	}//END OF FOREACH 
	 	$translations[$d]['country'] = $country_details['country'];
	 	$d++;
	}//END OF ELSE	
}//END OF FOR
$smarty->assign("field_name", $field_name);//assigned template variable extra_fields 
$smarty->assign("field_id", $id);//assigned template variable extra_fields 
$smarty->assign("trans",$translations);//assigned template variable trans  
$smarty->assign("field_lng",$field_lng);//assigned template variable gallery_lng  
}//END OF FUNCTION extra_fields_translations

function get_gallery_pages_images($id,$limit=0,$type="images",$multi=0) 
{ 
	global $sql;

	$type = ($multi) ? " AND type IN ($type)" : " AND type = '$type'";
	$sql->db_Select("gallery_images","*","available = 1 AND galleryid = $id $type ORDER BY orderby,date_added");
//	echo "SELECT * FROM gallery_images WHERE available = 1 AND galleryid = $id $type ORDER BY orderby<br>";
	if ($sql->db_Rows() > 0) 
	{
		if ($limit) 
		{
			
			return execute_paginated($sql,1,$limit);
		}
		else 
		{
			return execute_multi($sql,1);
		}
	}
	 	
}//END OF FUNCTION get_detailed_images

function get_gallery_cat($id) 
{ 
	$sql = new db();
	$sql->db_Select("gallery_page_categories","catid","galleryid = $id AND main ='Y'");
//	echo "<Br>SELECT catid FROM gallery_page_categories WHERE galleryid = $id AND main ='Y'<br>";
	return execute_single($sql,0);
}//END OF FUNCTION get_gallery_cat

function get_gallery_category($cat,$lang,$settings=0) 
{ 
  $sql = new db();
  $fields  ="gallery_categories_lng.category, gallery_categories_lng.image, gallery_categories.settings ,gallery_categories.categoryid, gallery_categories_lng.description,
  gallery_categories_lng.meta_descr, gallery_categories_lng.meta_keywords, gallery_categories.order_by, gallery_categories.parentid,gallery_categories.categoryid_path";
  $tables = "gallery_categories INNER JOIN gallery_categories_lng ON (gallery_categories.categoryid = gallery_categories_lng.categoryid)";
  $sql->db_Select($tables,$fields,"gallery_categories.categoryid = $cat AND code = '$lang'");
//  echo "SELECT $fields FROM $tables WHERE gallery_categories.categoryid = $cat AND code = '$lang'<Br>";
if ($sql->db_Rows() > 0) 
{
	 $a = execute_single($sql,1);
	 if ($settings) 
	 {
	 		
	 		$a['settings'] = form_settings_array($a['settings'],"###",":::");
	 }
}
 return $a;
}//END OF FUNCTION get_category

function format_gallery_category($data) 
{ 
  $tmp = explode("/",$data['category']);
  for ($i=0;count($tmp) > $i;$i++)
  {
  	$ar[$i]['cat'] = $tmp[$i];
  }//END OF FOR
  $tmp = explode("/",$data['categoryid_path']);
  for ($i=0;count($tmp) > $i;$i++)
  {
  	$ar[$i]['catid'] = $tmp[$i];
  }//END OF FOR
  return $ar;
}//END OF FUNCTION format_category

function get_gallery_pages($cat,$sort="default",$way="default",$lang,$type="cat",$active="no",$page=false,$limit="no",$efields=0,$start=0,$main='Y') 
{
	$active_gallery = $active; 
	$active = ($active == "no") ? "0,1" : "1"; 
	global $sql;
	$t = new textparse();
	if (!$page) {
		$not_paginated = 1;
	}
	$way = ($way != "default") ? $way : "DESC";
	$sort = ($sort != "default") ? $sort : "date_added";
	$page = ($page == false) ? CURRENT_PAGE : $page;
		if ($start) 
	{
		$limit_q = " LIMIT $start,$limit";
	}
	$table = ($type == "cat") ? "gallery_categories" : "gallery_locations";
	$tables = "gallery
 INNER JOIN gallery_page_categories ON (gallery.id=gallery_page_categories.galleryid)
 INNER JOIN gallery_categories_lng ON (gallery_page_categories.catid=gallery_categories_lng.categoryid)
 INNER JOIN gallery_lng ON (gallery.id=gallery_lng.galleryid)";
	$field = "gallery_page_categories.catid";
	$q = "($field = $cat) AND
  (active IN ($active)) AND 
  (gallery_lng.code = '".$lang."') AND 
  (gallery_categories_lng.code = '".$lang."') AND
  gallery_page_categories.main = '$main' 
   ORDER BY $sort $way $limit_q";
	$sql->db_Select($tables,"gallery.id",$q);

//	echo "SELECT gallery.id FROM $tables WHERE $q<br><BR>";
	if ($sql->db_Rows() >0) 
	{ 
		if ($not_paginated) 
		{
			$tmp = execute_multi($sql); 
		}
		else {
			$tmp = execute_paginated($sql,0,$limit,0,$page); 
		}
		
if (condition) {
	
}
		for ($i=0;count($tmp) > $i;$i++)
		{
			$gallery[] = get_gallery($tmp[$i]['id'],$lang,$active_gallery,$efields);
		}//END OF FOR
	}//END OF IF rows found
	if ($efields) 
	{
		for ($i=0;count($gallery) > $i;$i++)
		{
			$efields_val = get_gallery_extra_fields($gallery[$i]['id'],$lang);
			if ($efields_val) 
			{
				$gallery[$i]['efields'] = simplify_efields($efields_val);
			}
		}
	}
	
	 return $gallery;
}//END OF FUNCTION get_gallery

function get_gallery_categories($cat,$lang,$active=1,$get_subs=0,$limit=0) 
{ 
		$active = ($active == 1) ? "1" : "0,1";
global $sql;
		$limit_q = ($limit) ? " LIMIT $limit" : "";
		$fields = "gallery_categories_lng.category, gallery_categories_lng.description, gallery_categories_lng.meta_descr, 
		gallery_categories_lng.meta_keywords, gallery_categories_lng.image, gallery_categories.categoryid, gallery_categories.parentid, 
		gallery_categories.categoryid_path, gallery_categories.order_by,  gallery_categories.gallery_count, gallery_categories.date_added, gallery_categories.settings"; 
 $tables = "gallery_categories INNER JOIN gallery_categories_lng ON (gallery_categories.categoryid = gallery_categories_lng.categoryid)";
 $sql->db_Select($tables,"$fields","parentid = $cat AND code = '$lang' ORDER BY order_by $limit_q");

// echo "SELECT $fields FROM $tables WHERE parentid = $cat AND code = '$lang' ORDER BY order_by<br><br>";
 if ($sql->db_Rows() > 0) 
 {  
 $categories = execute_multi($sql,1);
 
 for ($i=0;count($categories) > $i;$i++)
 {
 $categoryid = $categories[$i]['categoryid'];

 if ($get_subs) 
 {
 	$categories[$i]['subcategories'] = get_gallery_categories($categoryid,$lang,$active);
 }
 
 $sql->db_Select("gallery_page_categories
 INNER JOIN gallery ON (gallery_page_categories.galleryid=gallery.id)
 INNER JOIN gallery_categories ON (gallery_page_categories.catid=gallery_categories.categoryid)",
 "gallery_page_categories.galleryid","(gallery_categories.categoryid_path LIKE '%$categoryid/%' 
 OR `gallery_categories`.categoryid_path LIKE '%/$categoryid' OR gallery_categories.categoryid_path = '$categoryid') 
 AND gallery.active IN ($active) ");

 $categories[$i]['num_gallery'] = $sql->db_Rows();

 
 $sql->db_Select("gallery_categories","categoryid","parentid = $categoryid");

 $categories[$i]['num_sub'] = $sql->db_Rows();
 
 //get settings 
 for ($j=0;count($categories) > $j;$j++)
 {
 	$categories[$j]['settings_array'] = form_settings_array($categories[$j]['settings'],"###",":::");
 }
 }//END OF FOR
return $categories;
 }//END OF IF
 else 
 {  
 return 0; 
 }//END OF ELSE
}//END OF FUNCTION get_categ

function get_all_gallery_categories($lang)
{
	$sql = new db();
	$fields = "gallery_categories_lng.category, gallery_categories_lng.image, gallery_categories.categoryid, gallery_categories.parentid,
	gallery_categories.categoryid_path";
	$tables = "gallery_categories INNER JOIN gallery_categories_lng ON (gallery_categories.categoryid = gallery_categories_lng.categoryid)";
	$sql->db_Select($tables,$fields, "code = '$lang' ORDER BY categoryid_path");
//	echo "SELECT $fields FROM $tables WHERE code = '$lang' ORDER BY categoryid_path";
	if ($sql->db_Rows() > 0) 
	{  
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
		$r = $sql->db_Fetch();
		foreach ($r as $key => $value) 
		{
		   	if (!preg_match("[0-9]",$key)) 
		{
			$ar[$i][$key] = $value;
		}//END OF IF
		}//END OF FOREACH 
		$ar[$i]['category'] = construct_gallery_category($r['categoryid_path'],$lang);
		}//END OF FOR
		sort($ar);
	return $ar;	
	}//END OF IF
}//END OF FUNCTION

function construct_gallery_category($path,$lang) 
{ 
	$sql = new db();
	if (strstr($path,"/")) 
	{  
	$tmp = explode("/",$path);
	$ids = implode(",",$tmp);
	}//END OF IF
	else 
	{  
	$ids = $path; 
	}//END OF ELSE
$sql->db_Select("gallery_categories_lng","category","categoryid IN ($ids) AND code = '$lang'");
for ($i=0;$sql->db_Rows() > $i;$i++)
	 {
	 	$r = $sql->db_Fetch();
	 	$ar[] = $r['category'];
	 }//END OF FOR	 
	 if (count($ar) > 0) 
	 {  
	 	$return = implode("/",$ar); 
	 }//END OF IF
	 else 
	 {  
	 	$return = $r['category']; 
	 }//END OF ELSE

	 return $return;
}//END OF FUNCTION construct_category

function get_gallery_images($id,$limit=0,$type="images",$multi=0,$active=1) 
{ 
	global $sql;

	$type = ($multi) ? " AND type IN ($type)" : " AND type = '$type'";
	$sql->db_Select("gallery_images","*","available = 1 AND galleryid = $id $type ORDER BY orderby,date_added");
//	echo "SELECT * FROM gallery_images WHERE available = 1 AND galleryid = $id $type ORDER BY orderby<br>";
	if ($sql->db_Rows() > 0) 
	{
		if ($limit) 
		{
			
			return execute_paginated($sql,1,$limit);
		}
		else 
		{
			return execute_multi($sql,1);
		}
	}
	 	
}//END OF FUNCTION get_detailed_images

function get_gallery_image($id)
{
	global $sql;
	
	$sql->db_Select("gallery_images","*","id = $id");
	return execute_single($sql,1);
}

function get_gallery_detailed_images($galleryid,$type="images",$active=0,$imageid=0,$thumbid=0) 
{ 
  $sql = new db();

  $imageid_sql = ($imageid) ? " AND imageid = $imageid" : "";
  $type = ($type ==0) ? "AND type = 0" : "AND type IN (0,$type)";
  $active = ($active == 0) ? "0,1" : $active;
  $query = "itemid = $galleryid AND available IN ($active) $imageid_sql $type  ORDER BY orderby";
  $fields = "gallery_images.*";
  $tables = "gallery_images";

  $sql->db_Select($tables,$fields,$query);
  if ($sql->db_Rows()) 
  {
  	$res = execute_multi($sql);
  	$tmp = array();
  	$i=0;
  	//Now start looping to create the array. Certain things will be done for backward compatibility
  	for ($i=0;count($res) > $i;$i++)
  	{
  		//First of all get the rest of the image types
  		$sql->db_Select("gallery_images_aditional","*","imageid =".$res[$i]['id']);
  		$tmp = execute_multi($sql);
  		
//  		echo $tmp['image_type']."<br>";
//  		echo "SELECT * FROM gallery_images_aditional WHERE imageid =".$ar['id']."<br>";
		foreach ($tmp as $ar) 
		{
			$res[$i][$ar['image_type']] = $ar;

  			$res[$i][$ar['image_type']] = str_replace($_SERVER['DOCUMENT_ROOT'],"",$ar['image_url']);

		}
  		
  	}
  }


  	return $res;
}//END OF FUNCTION get_detailed_images

function get_all_gallery_detailed_images($galleryid,$type="image",$active=0) 
{ 
  $sql = new db();
  $type = ($type == 1) ? "" : "AND type = '$type'";
  $active = ($active == 0) ? "0,1" : $active;
  $sql->db_Select("gallery_images","*","galleryid = $galleryid AND available IN ($active) $type  ORDER BY type,orderby");
  	return execute_multi($sql,1);
}//END OF FUNCTION get_detailed_images

function get_gallery_next_prev_images($id,$galleryid) 
{ 
	 $sql = new db();
	 $sql->db_Select("gallery_images","id","available = 1 AND galleryid = 1");
}//END OF FUNCTION get_next_prev_images

function get_related_gallery($id,$lang,$active="no") 
{ 
	$sql = new db();
	$active = ($active == "no") ? "0,1" : $active;
	$tables = "gallery INNER JOIN gallery_links ON (gallery.id = gallery_links.gallery_source)";
	$sql->db_Select($tables,"*","gallery_source = $id AND gallery.active IN ($active) ORDER BY gallery_links.orderby");
	if ($sql->db_Rows() > 0)
	{
		$tmp = execute_multi($sql);
		for ($i=0;count($tmp) > $i;$i++)
		{
			$ar[] = get_gallery($tmp[$i]['gallery_dest'],$lang);
		}
	}
//echo "SELECT * FROM $tables WHERE gallery_source = $id AND gallery.active IN ($active) ORDER BY orderby<br><BR>";
	
  return $ar;
}//END OF FUNCTION get_upselling

function get_featured_gallery($cat,$type,$lang,$active="no",$limit=0,$orderby=0) 
{ 
	$sql = new db();
	$cat = ($cat) ? $cat : 0;
	$active = ($active == "no") ? "0,1" : $active;
	$limit = ($limit) ? " LIMIT $limit" : "";
	$orderby = ($orderby) ? $orderby : "gallery_order";
	$tables = "gallery INNER JOIN gallery_featured ON (gallery.id = gallery_featured.galleryid)";
	$sql->db_Select($tables,"galleryid,gallery_order","categoryid = $cat AND type = '$type' AND gallery.active IN 
	($active) ORDER BY $orderby $limit");
//	echo "categoryid = $cat AND type = '$type' AND gallery.active IN ($active) ORDER BY $orderby $limit<br>";
	if ($sql->db_Rows() > 0) 
	{  
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
		$r = $sql->db_Fetch();
		$p = get_gallery($r['galleryid'],$lang);
		if ($p !=0) 
		{  
		$ar[$i] = $p;
		$ar[$i]['gallery_order'] = $r['gallery_order'];
		}//END OF IF
		}//END OF FOR
	return $ar;
	}//END OF IF
}//END OF FUNCTION get_featured

function get_latest_gallery($limit,$lang,$active="no",$cat=0,$efields=0)
{
	global $sql;
	$active_sql = ($active == "no") ? "0,1" : $active;
	if ($cat)//latest from a specific category
	{
		$tables = "gallery INNER JOIN gallery_page_categories ON (gallery.id = gallery_page_categories.galleryid)";
		$sql->db_Select($tables,"gallery.id","catid = $cat AND gallery.active IN ($active_sql) AND
		gallery_page_categories.main = 'Y' ORDER BY date_added DESC LIMIT $limit");
//		echo "SELECT gallery.id FROM gallery INNER JOIN gallery_page_categories ON (gallery.id = gallery_page_categories.galleryid) WHERE catid = $cat AND gallery.active IN ($active_sql) AND
//		gallery_page_categories = 'Y' ORDER BY date_added DESC LIMIT $limit";
	}
	else //Latest from ALL categories 
	{
		$sql->db_Select("gallery","id","gallery.active IN ($active_sql) ORDER BY date_added DESC LIMIT $limit");
//		echo "SELECT id from gallery WHERE gallery.active IN ($active_sql) ORDER BY date_added DESC LIMIT $limit<br>";
	}

	if ($sql->db_Rows()) 
	{
		$tmp = execute_multi($sql);
		for ($i=0;count($tmp) > $i;$i++)
		{
			$gallery[$i] = get_gallery($tmp[$i]['id'],$lang,$active,$efields);
		}
		return $gallery;
	}
	else {return 0;}
}

function gallery_cat_translations($id) 
{ 
	global $smarty,$sql;
$countries = get_countries("Y",$trans="yes");
$class_lng = array();
$translations = array();

for ($i=0,$j=0,$d=0;count($countries) > $i;$i++)
{
	$sql->db_Select("gallery_categories_lng","*","code = '".$countries[$i]['code']."' AND categoryid = $id");
	if ($sql->db_Rows() == 0) 
	{  
	 	$cat_lng[$j]['code'] =  $countries[$i]['code'];
	 	$cat_lng[$j]['country'] =  $countries[$i]['country'];
	 	$j++;
	}//END OF IF
	else 
	{  
	//see for possible translations
	 	$r = $sql->db_Fetch();
	 	$cat_details = get_lang_name($r['code']);
	 	foreach ($r as $key => $value) 
	 	{
	 	   	if (!preg_match("[0-9]",$key)) 
	 	{
	 		$translations[$d][$key] = $value;
	 	}//END OF IF
	 	}//END OF FOREACH 
	 	$translations[$d]['country'] = $cat_details['country'];
	 	$d++;
	}//END OF ELSE	
}//END OF FOR
$smarty->assign("trans",$translations);//assigned template variable trans  
$smarty->assign("cat_lng",$cat_lng);//assigned template variable gallery_lng  
}//END OF FUNCTION extra_fields_translations

function get_cat_gallery($cat,$lang,$active,$num=false) 
{ 
	$active = ($active = 1) ? "1" : "0,1";
	 $sql = new db();
	 $fields = "gallery.image, gallery.date_added, gallery.id, gallery_lng.title, gallery_page_categories.catid, gallery_lng.category";
	 $tables = "gallery INNER JOIN gallery_lng ON (gallery.id = gallery_lng.galleryid)
	  INNER JOIN gallery_page_categories ON (gallery.id = gallery_page_categories.galleryid)
	   INNER JOIN gallery_categories_lng ON (gallery_page_categories.catid = gallery_categories_lng.categoryid)";
	 $q = "(gallery_lng.code = '$lang') AND (gallery_categories_lng.code = '$lang') AND gallery_page_categories.catid = $cat AND gallery.active IN ($active)
	 ORDER BY date_added DESC";

	 $sql->db_Select($tables,$fields,$q);
	 if ($sql->db_Rows() > 0) 
	 {  
	 $a = ($num == false) ? execute_multi($sql,1) : execute_paginated($sql,1,$num);
	 }//END OF IF
	 return $a;
}//END OF FUNCTION get_cat_gallery

function get_category_gallery_pages($cat,$table)
{
$tree_parser = new tree_parser;

$tree_parser->load_table($table);
$cat_tree=$tree_parser->select_root_cat($cat,$table);
foreach ($cat_tree as $key => $value)
{
//	echo "$key :: $value<br>";
}
return $cat_tree;
}

function get_gallery_tags($id,$seperator,$mode=0)
{
	global $sql;
	
	$sql->db_Select("gallery_tags","tag","galleryid = $id");
	$tags = execute_multi($sql,1);

	//simplify the array
	for ($i=0;count($tags) > $i;$i++)
	{
		$t[] = $tags[$i]['tag'];
	}
	if ($mode) 
	{
		return $t;
	}
	else 
	{
		return construct_tags_string($t,$seperator);
	}
}

function create_gallery_tag_cloud($limit=0,$gallery=0)
{
	global $sql;
	if ($gallery) 
	{
		
	
	if (is_array($gallery)) 
	{
		foreach ($gallery as $k => $v)
		{
			$gallery_ids_array[] = $v['id'];
		}
		$gallery_ids_q = "galleryid IN (".implode(",",$gallery_ids_array).")";

			$orderby_q = ($limit) ? " ORDER BY quantity DESC LIMIT $limit" : " ORDER BY tag ASC";

		$sql->db_Select("gallery_tags","id,tag , COUNT(id) AS quantity","$gallery_ids_q GROUP BY tag $orderby_q");
//		echo "SELECT tag , COUNT(id) AS quantity FROM gallery_tags WHERE $gallery_ids_q GROUP BY tag ORDER BY tag ASC";
	}
	else 
	{
		$orderby_q = ($limit) ? " ORDER BY quantity DESC LIMIT $limit" : " ORDER BY tag ASC";

		$sql->db_Select("gallery_tags","id,tag , COUNT(id) AS quantity","galleryid = $gallery GROUP BY tag $orderby_q");
	}
	}
	else 
	{
		$sql->db_Select("gallery_tags","id,tag , COUNT(id) AS quantity","GROUP BY tag ORDER BY tag ASC",$mode="no_where");
	}
	
	if ($sql->db_Rows()) 
	{

	$tmp = execute_multi($sql);
	
	for ($i=0;count($tmp) > $i;$i++)
	{
		$tags[$tmp[$i]['tag']] = $tmp[$i]['quantity'];
	}
//	echo "SELECT tag , COUNT(id) AS quantity FROM gallery_tags "
	$max_size = 250; // max font size in %
	$min_size = 100; // min font size in %
	
	$max_qty = max(array_values($tags));
	$min_qty = min(array_values($tags));
	
		// find the range of values
	$spread = $max_qty - $min_qty;
	if (0 == $spread) 
	{ // we don't want to divide by zero
    	$spread = 1;
	}
	
	// determine the font-size increment
	// this is the increase per tag quantity (times used)
	$step = ($max_size - $min_size)/($spread);
	
	for ($i=0;count($tmp) > $i;$i++)
	{
		$size = $min_size + (($tmp[$i]['quantity'] - $min_qty) * $step);
		$tmp[$i]['size'] = $size;
	}
	}//END OF ROWS
	return $tmp;
}

function create_gallery_folders($catfolder,$folder)
{
				$directory = $_SERVER['DOCUMENT_ROOT'].$folder;
				$directory_cat = $_SERVER['DOCUMENT_ROOT'].$catfolder;
				$directory_thumbs = $directory."/thumbs";
				$directory_main = $directory."/main";
				//Create the user personal folder
				if (!is_dir($directory_cat)) 
				{
					if (mkdir($directory_cat))
					{
					mkdir($directory,0755);
					mkdir($directory_thumbs,0755);
					mkdir($directory_main,0755);
					return true;
					}
					else 
					{
						echo "can't write";
					}
				}
				else 
				{
					mkdir($directory,0755);
					mkdir($directory_thumbs,0755);
					mkdir($directory_main,0755);
					return true;
				}
}

function get_folder_gallery_images($uid,$folder,$id,$module_settings,$type=0)
{
	global $sql;
	
	
	$thumb_size = $module_settings['settings']['gallery_images_thumb_width'];
	// create a new Thumbs-Object(str "imagepath", str thumb_prefix, int width, int height [, int fix ])
// last (4th) parameter is optional and desides whether the size of all thumbs is
// limited/fixed by width, height or both (0 = both, 1 = width, 2 = height)
$destination_folder = $_SERVER['DOCUMENT_ROOT'].$folder."/";
$cThumbs = new Thumbs($destination_folder, $module_settings['settings']['thumb_prefix'], $thumb_size, $thumb_size, 0);

// get the array of all found images and thumbs
$mix = $cThumbs->getImages();

if ($mix !=0) 
{
//get user images from DB
$sql->db_Select("gallery_images","image","galleryid = '".$id."'");
if ($sql->db_Rows() > 0) 
{
	$res = execute_multi($sql,0);
for ($i=0;count($res) > $i;$i++)
{
	$tmp1[] = $_SERVER['DOCUMENT_ROOT'].$res[$i]['image'];//copy array for comparison
	$tmp[] = $res[$i]['image'];
}
}

//This should compare the DB to the folder. If there is the image count is different OR if the count is the same but the files are different
//Go ahead and write to the db, else return nothing
if (count($mix[0]) != count($tmp) OR array_diff_assoc($tmp1,$mix[0])) //Check if the user folder has changed and update the db
{

//$f = fopen("log.txt","w");
	//start adding new images
	for ($i=0;count($mix[0]) > $i;$i++)
	{
		$image_details = getimagesize($mix[0][$i]);
		
		$image = str_replace($_SERVER['DOCUMENT_ROOT'],'',$mix[0][$i]);
		$date_added = time();
		$image_x = $image_details[0];
		$image_y = $image_details[1];
		$image_path = $mix[0][$i];
		$thumb = str_replace($_SERVER['DOCUMENT_ROOT'],'',$mix[1][$i]);
		$filesize = filesize($image_path);
		$comments = 0;
		$views = 0;
		if (is_array($tmp)) 
		{
		if (!in_array($image,$tmp)) 
		{
		$sql->db_Insert("gallery_images","'','$id','$image','$image_path','$thumb','$alt','$title','0','$i','$date_added',
		'$image_x','$image_y','$type'");
		return mysql_insert_id();
		}	
		}
		else 
		{
		$sql->db_Insert("gallery_images","'','$id','$image','$image_path','$thumb','$alt','$title','0','$i','$date_added',
		'$image_x','$image_y','$type'");
		return mysql_insert_id();
		}
		$q = "INSERT INTO gallery_images VALUES ('','$id','$image','$image_path','$thumb','$alt','$title','0','$i','$date_added',
		'$image_x','$image_y','$type');\n\r";
//		fwrite($f,$q);
	}
//	fclose($f);
}

}
else {
	return 0;
}
}


class gallery_ImageSet
{
	var $base_dir = "";
	var $thumbDir = "";
	var $tPrefix = "";
	var $settings=0;
	var $fix=0;
	var $module_settings=0;
	
//INITIALIZE CLASS
	function Images($dir, $settings,$module_settings,$db_info,$type=0,$search_mode=0)
	{
		require_once($_SERVER['DOCUMENT_ROOT']."/includes/colors.inc.php");
		if(substr($dir, -1) == "/") $dir = substr($dir, 0, -1);
	$this->base_dir = $dir;
	$this->fix = $search_mode;
	$this->settings = $settings;
	$this->module_settings = $module_settings;
	$images = $this->create_image_set($dir);
	$this->InsertToDb(ID,$images,$db_info['itemid'],$module_settings,$db_info,$type);
	
	return $images;
	}
	
function create_image_set()
{
//First analyze the settings array to find out the directories to be processed
$imageList = array();
$imageList = $this->read_dir_images($this->base_dir);
$cpos =0;

		if ($imageList) 
		{
			foreach($imageList as $image) 
			{

//				$images[$cpos]['main']['path'] = $this->base_dir."/".$image;
//				$images[$cpos]['main']['image'] = $image;
				foreach ($this->settings as $k =>$v) 
				{
//					echo $this->base_dir."/".$v['dir']."/".$image." ---- ".$v['width']."<br>";
					//We need to go dir by dir on this one
					$target = $this->base_dir."/".$v['dir']."/".$v['prefix'].$image;
					$tmp[$k] = $this->checkFile($target,$v['width'],$v['height'],$this->fix);
					if (!$tmp[$k]) 
					{
						if (!is_dir($this->base_dir."/".$v['dir'])) 
						{
							mkdir($this->base_dir."/".$v['dir'],0755);
						}

						
						if ($this->module_settings['settings']['keep_original_image'] AND !$v['dir']) 
						{
							if (!is_dir($this->base_dir."/originals")) 
							{
								mkdir($this->base_dir."/originals",0755);
							}
							copy($this->base_dir."/$image",$this->base_dir."/originals/$image");
								
						}
$tmp[$k] = $this->createImge($image,$this->base_dir."/".$v['dir'],$v['width'],$v['height'],$v['prefix']);
						
					}
					if ($tmp[$k]) 
					{
					$images[$cpos][$k]['path'] = $target;
					$images[$cpos][$k]['image'] = $v['prefix'].$image;
					
					}
				//	gallery_r($ar);

				}
				$cpos++;
			}
			
			return $images;
		}
		else 
		{
			return 0;
		}
}

function InsertToDb($uid,$images,$id,$module_settings,$db_info,$type=0)
{
	global $sql;

	if ($images) 
	{
		$sql->db_Select($db_info['table'],$db_info['fields'],$db_info['query']);
		if ($sql->db_Rows() > 0) 
		{
			$res = execute_multi($sql);
			for ($i=0;count($res) > $i;$i++)
			{
				$tmp[] = $res[$i]['original_file'];
			}
	   }

		if (count($images) != count($tmp)  ) //Check if the user folder has changed and update the db
		{
		for ($i=0;count($images) > $i;$i++)//LOOP THROUGH FOLDER IMAGES
		{
			$original_file = $images[$i]['main']['image'];
		if (@!in_array($original_file,$tmp)) 
		{
			$sql->db_Insert($db_info['table'],"'','$id','$original_file','$alt','$title','$details','$available','$i','$type'");
			$n_id = mysql_insert_id();
//			$f = fopen("log_upload.txt","w");
			foreach ($images[$i] as $k => $v)
			{
				//Insert to aditional tables
				$image = $v['image'];
				$image_path = $v['path'];
				
//				fwrite($f,"BEFORE : ".$image_path."\n\r");
				$image_details = getimagesize($image_path);	
				$image_x = $image_details[0];
				$image_y = $image_details[1];
				$date_added = time();
				$image_path = str_replace("//","/",$image_path);
				$image_url = str_replace($_SERVER['DOCUMENT_ROOT'],"",$image_path);//WINDOWS ONLY
				$image_path = str_replace("\\","/",$image_path);//WINDOWS ONLY
				
//				fwrite($f,"AFTER : ".$image_path."\n\r");
				$sql->db_Insert($db_info['table_aditional'],"'$n_id',$id,'$image','$image_path','$image_url','$date_added','$image_x','$image_y','$k'");
//				echo "'$n_id','$image','$image_path','$image_x','$image_y','$k'<BR>";
				if ($this->settings['get_image_colors'] AND !$prefix) 
				{
					
					unset($colors);
					$colors = get_image_colors($image_path);
					for ($y = 0; $this->settings['image_colors_number'] > $y; $y++)
					{
						$sql->db_Insert("gallery_images_colors","'','$n_id','$image','".$colors_key[$y]."','".$colors[$colors_key[$y]]."'");
					}
				}
			}
//			fclose($f);
		}
		}//END OF LOOPING THROUGH IMAGES
		}
	}
	
}

	function read_dir_images($dir)
	{
		$files = false;
		if($resDir = opendir($dir))
		{
			// check all files in $dir - add images to array 'files'
			$cpos = 0;
			while($file = readdir($resDir))
			{
				if($file[0] != "_" && $file != "." && $file != ".." && !is_dir($dir . "/" . $file))
				{
					$ext = substr($file, -3);
					$ext = strtolower($ext);
					if($ext == 'jpg' OR $ext == 'gif' OR $ext == 'png' OR $ext == 'jpeg') {
						$files[$cpos] = $file;
						$cpos++;
					}
				}
			}
			closedir($resDir);
		}
		if($files) sort($files);
		return $files;
	}
	
	function checkFile($imageFile,$maxW=0,$maxH=0,$fix=0)
	{
		
		if(file_exists($imageFile)) {

			list($srcW, $srcH, $srcType, $html_attr) = getimagesize($imageFile);
			if($fix == 1) {
				if($maxW!= $srcW) {
					return false;
				}
			} elseif($fix == 2) {
				if($maxH!= $srcH) {
					return false;
				}
			} else {
				if($srcH > $maxH OR ($srcW != $maxW && $srcH != $maxH)) {
					return false;
				}
			}
//			echo "thumb of $imageFile exists<br>";
			return true;
		} else {
			//echo "thumb of $image doesn't exist!!!<br>";
			return false;
		}
	}

	function createImge($image,$dir,$width,$height,$prefix=0)
	{
		global $sql;
		$srcFile = $this->base_dir."/".$image;
		list($srcW, $srcH, $srcType, $html_attr) = getimagesize($srcFile);
		$ext = substr($image, -3);
		$ext = strtolower($ext);
		if($ext == 'jpg') {
			$srcImage = @imagecreatefromjpeg($srcFile);
		} elseif($ext == 'gif') {
			$srcImage = @imagecreatefromgif($srcFile);
		} elseif($ext == 'png') {
			$srcImage = @imagecreatefrompng($srcFile);
		}
		if(!$srcImage) return false;
		//$srcW = imagesx($srcImage);
		//$srcH = imagesy($srcImage);
		if($this->fix == 0) {
			if($srcW / $width > $srcH / $height) {
				$factor = $width / $srcW;
			} else {
				$factor = $width / $srcH;
			}
		} elseif($this->fix == 1) {
			$factor = $width / $srcW;
		} elseif($this->fix == 2) {
			$factor = $height / $srcH;
		}
		$newH = (int) round($srcH * $factor);
		$newW = (int) round($srcW * $factor);

		$newImage = imagecreatetruecolor($newW, $newH);
		$watermark = $_SERVER['DOCUMENT_ROOT'].$this->module_settings['watermark'];
		
		imagecopyresampled($newImage,$srcImage,0,0,0,0,$newW,$newH,$srcW,$srcH);
		$newFile = $dir."/".$prefix.$image;
		$watermark       =   $_SERVER['DOCUMENT_ROOT']."/images/watermark.png"; // original photo
		imagejpeg($newImage, $newFile, "85");
		if ($watermark AND !$prefix)//Only watermark main image 
		{
			$target = $dir."/"."marked_".$image;
			$this->watermark($newFile,$watermark);
		}
		
		return true;
	}

function get_image_colors($img)
{
$ex=new GetMostCommonColors();
$ex->image=$img;
$colors=$ex->Get_Color();
$colors_key=array_keys($colors);

return $colors_key;
}
	
function watermark($img,$watermark,$target=0,$PosX=transparentWatermarkOnCenter,$PosY=transparentWatermarkOnMiddle) 
{
$watermark = imagecreatefrompng($watermark);//create watermark canvas   
$watermark_width = imagesx($watermark);//watermark width  
$watermark_height = imagesy($watermark);//watermark height  

$image = imagecreatetruecolor($watermark_width, $watermark_height);//create image canvas  
$image = imagecreatefromjpeg($img);//new file  
$size = getimagesize($img);  
$imageWidth = $size[0];
$imageHeight = $size[1];

		//set position of logo
		switch ($PosX) {
			case transparentWatermarkOnLeft: 
			$leftStamp=0;
			break;
			case transparentWatermarkOnCenter:
			$leftStamp=($imageWidth - $watermark_width)/2;
			break;
			case transparentWatermarkOnRight:
			$leftStamp=$imageWidth - $watermark_width;
			break;
			default :
			$leftStamp=0;
		}
		switch ($PosY) {
			case transparentWatermarkOnTop:
			$topStamp=0;
			break;
			case transparentWatermarkOnMiddle:
			$topStamp=($imageHeight - $watermark_height)/2;
			break;
			case transparentWatermarkOnBottom:
			$topStamp=$imageHeight - $watermark_height;
			break;
			default:
			$topStamp=0;
		}

$dest_x =  $leftStamp;  
$dest_y =  $topStamp;  
imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, 100);//merge the two images 
$target = ($target ==0) ? $img : $target;
imagejpeg($image,$target,100);//save the new file  
imagedestroy($image);//empty the memory space from the new image  
imagedestroy($watermark);//empty the memory space from the watermark
}//END OF FUNCTION watermark
	
	function rebuildThumbs()
	{
		@set_time_limit(300);
		$imageList = $this->getImageNames();
		foreach($imageList as $image) {
			$this->createThumb($image);
		}
	}
	
	
}//END OF CLASS

function gallery_images_categories($id=0)
{
	global $sql;
	
	$sql->db_Select("gallery_images_categories","*","ORDER BY id",$mode="no_where");
	return execute_multi($sql);
}

function get_gallery_classes($classid="no",$type="include",$lang,$galleryid) 
{ 
	if ($type == "include") 
	{   
		$query = "galleryid = $galleryid AND code = '$lang' ORDER BY orderby";
	}//END OF IF
	elseif ($type == "exclude") 
	{ 
	  $query = "galleryid = $galleryid AND gallery_classes.classid NOT IN ($classid) AND code = '$lang' ORDER BY orderby";
	  $classid = "no";
	}//END OF ELSEIF
	elseif ($type == "single") 
	{ 
	  $query = "galleryid = $galleryid AND gallery_classes.classid = $classid AND code = '$lang' ORDER BY orderby"; 
	}//END OF ELSEIF

	$fields = "gallery_classes.orderby,gallery_classes.classid, gallery_classes.active, gallery_class_lng.classtext, gallery_class_lng.class";
	$tables = "gallery_classes INNER JOIN gallery_class_lng ON (gallery_classes.classid = gallery_class_lng.classid)";
	 $sql = new db();
//echo "SELECT $fields FROM $tables WHERE $query<br>";
	 $sql->db_Select($tables,$fields,$query);
	 if ($sql->db_Rows() > 0) 
	 {  
	 if ($classid != "no") 
	 {  	 
	 	$ar = execute_single($sql,1);
		//add class options
		
		$ar['options'] = get_gallery_class_options($ar['classid'],$lang);
	 }//END OF IF
	 else
	 { 
		$ar = execute_multi($sql,1);
		for ($i=0;count($ar) > $i;$i++)
		{
			$ar[$i]['options'] = get_gallery_class_options($ar[$i]['classid'],$lang);
		}//END OF FOR
	}//END OF ELSE
	 }
	 return $ar;
}//END OF FUNCTION get_classes

function get_gallery_class_options($classid=0,$lang,$optionid="no",$galleryid=0) 
{ 
	 global $sql;
	 $fields = "gallery_class_options_lng.option_name, gallery_class_options.*";
	 $tables = "gallery_class_options INNER JOIN gallery_class_options_lng ON (gallery_class_options.optionid = gallery_class_options_lng.optionid)";
	 $q = ($optionid == "no") ? "classid = $classid AND code = '$lang' ORDER BY orderby" : "gallery_class_options.optionid = $optionid AND code = '$lang'";
//	 echo "SELECT $fields FROM $tables WHERE $q<br>";
//echo $classid."--- $optionid --- $galleryid <br>";
	 $sql->db_Select($tables,$fields,$q);

	 if ($optionid == "no") 
	 {
	 	$tmp = execute_multi($sql,1);
	 	if ($galleryid) 
	 	{
	 		for ($i=0;count($tmp) > $i;$i++)
	 		{
	 			$tmp[$i]['class'] = get_gallery_classes($tmp[$i]['classid'],'include',$lang,$galleryid);
	 		}
	 	}
	 	return $tmp;
	 }
	 else {
	 	
	 	$tmp = execute_single($sql,1);
	 	if ($galleryid) 
	 	{
//	 		echo "<BR>---- ".$tmp['classid']." $lang,$galleryid -----<br>";
	 		$tmp['class'] = get_gallery_classes($tmp['classid'],'single',$lang,$galleryid);
//	 		gallery_r($tmp['class']);echo "<br><br>";
	 	}

	 	return $tmp;
	 }
	 
}//END OF FUNCTION get_class_options

function get_class_gallery($id,$type="include",$lang) 
{ 
	global $sql;
	$sql->db_Select("gallery_class_gallery","classid,orderby","galleryid = $id ORDER BY orderby");
	if ($sql->db_Rows() > 0) 
	{  
	for ($i=0;$sql->db_Rows() > $i;$i++)
		{
		$r = $sql->db_Fetch();
		$ar[$i] = get_classes($r['classid'],$type,$lang);		
		$ar[$i]['orderby'] =$r['orderby'];
		}//END OF FOR
	}//END OF IF
	return $ar;
}//END OF FUNCTION get_class_gallery

function class_gallery_translations($id) 
{ 
	global $smarty,$sql;
$countries = get_countries("Y",$trans="yes");
$class_lng = array();
$translations = array();

for ($i=0,$j=0,$d=0;count($countries) > $i;$i++)
{
	$sql->db_Select("gallery_class_lng","*","code = '".$countries[$i]['code']."' AND classid = $id");
	if ($sql->db_Rows() == 0) 
	{  
	 	$class_lng[$j]['code'] =  $countries[$i]['code'];
	 	$class_lng[$j]['country'] =  $countries[$i]['country'];
	 	$j++;
	}//END OF IF
	else 
	{  
	//see for possible translations
	 	$r = $sql->db_Fetch();
	 	$country_details = get_lang_name($r['code']);
	 	foreach ($r as $key => $value) 
	 	{
	 	   	if (!preg_match("[0-9]",$key)) 
	 	{
	 		$translations[$d][$key] = $value;
	 	}//END OF IF
	 	}//END OF FOREACH 
	 	$translations[$d]['country'] = $country_details['country'];
	 	$d++;
	}//END OF ELSE	
}//END OF FOR 

$smarty->assign("trans",$translations);//assigned template variable trans  
$smarty->assign("class_lng",$class_lng);//assigned template variable gallery_lng
}//END OF FUNCTION class_translations

function gallery_get_tags($gallery,$limit=0)
{
	global $sql;
	foreach ($gallery as $k => $v)
	{
		$tags[] = get_gallery_tags($v['id'],",");
	}
	for ($i=0;count($tags) > $i;$i++)
	{
		foreach ($tags[$i] as $v)
		{
			if ($v) {
			$tmp[] = $v;	
			}
			
		}
	}
	return array_merge(array(),array_unique($tmp));
}


function gallery_search($posted_data,$settings,$db_info,$lang=0,$limit=0,$page=0,$query=0)
{
	global $sql,$smarty;
//print_r($posted_data);
	$results['data'] = $posted_data;
	$included_tables = array();

############### SETUP QUERY ##############################
$search_tables = "gallery ";
############# SKU ######################
if ($posted_data['sku'] != "") 
{  
 $search_condition .= " gallery.sku = '".$posted_data['sku']."' AND ";
}//END OF IF
################## CATEGORIES ####################
if ($posted_data['categoryid'] != "%" AND $posted_data['categoryid']) 
{
if (!empty($posted_data["search_in_subcategories"])) 
{
	
# Search also in all subcategories
	$sql->db_Select("gallery_categories","categoryid_path","categoryid = '".$posted_data["categoryid"]."'");
	$r = $sql->db_Fetch();
	$categoryid_path = $r['categoryid_path'];
	$sql->db_Select("gallery_categories","categoryid","categoryid = '".$posted_data["categoryid"]."' OR categoryid_path LIKE '$categoryid_path/%'");
	$categoryids_tmp = execute_multi($sql,0);	
	
	if (is_array($categoryids_tmp) && !empty($categoryids_tmp)) 
	{
		foreach ($categoryids_tmp as $k=>$v) 
		{
		$categoryids[] = $v["categoryid"];
		}//END OF FOREACH

		$search_condition .= "gallery_page_categories.catid $category_sign IN (".implode(",", $categoryids).") AND ";
	}//END OF IF
}//END OF IF
else //Search in top category only
  {  
 $search_condition .= "gallery_page_categories.catid = ".$posted_data['categoryid']." AND ";
  }//END OF ELSE  
  
$search_tables .= "INNER JOIN gallery_page_categories ON (gallery.id = gallery_page_categories.galleryid)";
$search_condition .= " (main = 'Y') AND";
$included_tables[] = 'gallery_page_categories';
}//END OF IF

############## PATTERN SEARCH ################
if (!empty($posted_data["substring"])) 
{
$search_tables .= "INNER JOIN gallery_lng ON (gallery.id = gallery_lng.galleryid)";
$included_tables[] = 'gallery_lng';
$posted_data['substring'] = trim($posted_data['substring']);

if ($posted_data['search_main'] != "") 
{  
 $condition[] = "gallery_lng.title LIKE '%".$posted_data["substring"]."%'";
}//END OF IF
if ($posted_data['search_short'] != "") 
{  
 $condition[] = "gallery_lng.description LIKE '%".$posted_data["substring"]."%'";
}//END OF IF
if ($posted_data['search_long'] != "") 
{  
 $condition[] = "gallery_lng.full_description LIKE '%".$posted_data["substring"]."%'";
}//END OF IF

############### KEYWORDS AS TAGS ##################
if ($posted_data['search_tags']) 
{
	$keywords = explode(" ",$posted_data['substring']);
	foreach ($keywords as $v)
	{
		$tag[] = "'".$v."'";
	}
	$this_join = ($posted_data['search_main'] OR $posted_data['search_short'] OR $posted_data['search_long']) ? "LEFT OUTER JOIN" : "INNER JOIN";
	$search_tables .= " $this_join gallery_tags ON (gallery.id = gallery_tags.galleryid) ";
	$condition[] = " gallery_tags.tag IN (".implode(",",$tag).") ";
	$included_tables[] = 'gallery_tags';
}

 foreach ($posted_data as $key => $value) 
 {   
	if (strstr($key,"-")) 
	{
		$condition[] = "gallery_extra_field_values.value LIKE '%".$posted_data['substring']."%'";
	}//END OF IF
 }//END OF FOREACH

if (!empty($condition))
{ 
	if ($posted_data['efields']) 
	{
		$search_tables .= "LEFT OUTER JOIN gallery_extra_field_values ON (gallery.id = gallery_extra_field_values.galleryid)";
		$included_tables[] = 'gallery_extra_fields';
		$included_tables[] = 'gallery_extra_field_values';
	}
	$search_condition .= "(".implode(" OR ", $condition).")"." AND (gallery_lng.code = '".$lang."') AND ";
}//END OF IF
}//END OF PATERN SEARCH

#################### ONE COLOR ########################
if ($posted_data['color_hex']) 
{
	$color = strtoupper($posted_data['color_hex']);
	$search_condition .= " gallery_images_colors.hex = '$color' AND ";
	$search_tables .= " INNER JOIN gallery_images_colors ON (gallery.id = gallery_images_colors.galleryid) ";
	$included_tables[] = 'gallery_images_colors';
}

#################### COLORS ##########################
if ($posted_data['search_color']) 
{
	foreach ($posted_data['search_color'] as $v) 
	{
		$m[] = "'$v'";
	}
	$search_condition .= " gallery_images_colors.base_color_hex IN (".implode(",",$m).") AND gallery_images_colors.weight = 0 AND ";
	$search_tables .= " INNER JOIN gallery_images_colors ON (gallery.id = gallery_images_colors.galleryid) ";
	$included_tables[] = 'gallery_images_colors';
}

################### SINGLE TAG ######################
if ($posted_data['tag']) 
{
	$tag =gallery_lookup_tag($posted_data['tag']);
	$search_tables .= "INNER JOIN gallery_tags ON (gallery.id = gallery_tags.galleryid)";
	$search_condition .= " gallery_tags.tag = '".$tag['tag']."' AND";
	$results['data']['substring'] = $tag['tag'];
	$results['data']['search_tags'] = 1;
	$included_tables[] = 'gallery_tags';
}
########### MULTIPLE SELECTED TAGS ###################
if ($posted_data['tags']) 
{
	foreach ($posted_data['tags'] as $v)
	{
		unset($tmp);
		$tmp = gallery_lookup_tag($v);
		$tag[] = "'".$tmp['tag']."'";
		$data_tag[] = $tmp['tag'];
	}
	
	$search_tables .= "INNER JOIN gallery_tags ON (gallery.id = gallery_tags.galleryid)";
	$search_condition .= " gallery_tags.tag IN (".implode(",",$tag).") AND";
	$results['data']['substring'] = implode(" ",$data_tag);
	$results['data']['search_tags'] = 1;
	$included_tables[] = 'gallery_tags';
}


//Extra fields Radio buttons search
if ($posted_data)
{
 foreach ($posted_data as $key => $value) 
 {   
	if (strstr($key,"$"))
	{
		$key = str_replace("$","-",$key);
		list($field,$code)=split("-",$key);
		$condition_radio[] = "(gallery_extra_field_values.value = '$value' AND gallery_extra_field_values.fieldid = '$code')";
		$included_tables[] = 'gallery_extra_fields';
		$included_tables[] = 'gallery_extra_field_values';
	}
 }//END OF FOREACH
}
if (!empty($condition_radio))
{ 
	$search_condition .= "(".implode(" OR ", $condition_radio).")"." AND ";
}//END OF IF

###################### SORTING ###################	
$sort = ($posted_data['sort']) ? $posted_data['sort'] : $settings['settings']['default_gallery_sort'];
//End check
$direction = ($posted_data['sort_direction']) ? $posted_data['sort_direction'] : $settings['settings']['default_sort_direction'];
$selected = ($posted_data['sort']) ? $sort : $settings['settings']['default_gallery_sort'];

if ($sort == ("date_added")) { $selected = "gallery.date_added";}
elseif ($sort == "price") 
{
	if (!in_array('eshop_product_details',$included_tables)) 
	{
		$search_tables .= "INNER JOIN eshop_product_details ON (gallery.id = eshop_product_details.productid)";
	}
	$selected = 'price';
}
elseif ($sort == "title") 
{
	if (!in_array('gallery_lng',$included_tables)) 
	{
		$search_tables .= "INNER JOIN gallery_lng ON (gallery.id = gallery_lng.galleryid)";
	}
	$selected = 'gallery_lng.title';
}

if ($posted_data['availability'] OR $posted_data['availability'] == 0) 
{
	if ($posted_data['availability'] == 1) 
	{
		$search_condition .= " active = 1";
	}
	elseif ($posted_data['availability'] == 0)
	{
		$search_condition .= " active = 0";
	}
	else 
	{
		$search_condition .= " active IN (1,0) ";
	}
}

$search_condition .= " 
GROUP BY gallery.id
ORDER BY $selected $direction";

$fields = "gallery.id";

//EXECUTE THE QUERY AND GATHER THE ID'S
$sql->db_Select($search_tables,$fields,"$search_condition");
#################### PAGINATE ###################
$current_page = ($posted_data['page']) ? $posted_data['page'] : 1;
$results_per_page = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $settings['settings']['items_per_page'];
$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;

$total = $sql->db_Rows();
if ($query) {
	$smarty->assign("query", "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page);
}

//echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page;
if ($total) 
{
//echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start, $results_per_page";
$sql->db_Select($search_tables,$fields,"$search_condition LIMIT $start,".$results_per_page);
$search_results = execute_multi($sql,1);

paginate_results($current_page,$results_per_page,$total);

for ($i=0;count($search_results) > $i;$i++)
{
	$gallery[$i] = get_gallery($search_results[$i]['id'],DEFAULT_LANG);
}//END OF FOR

$results['results'] = $gallery;


}//END OF IF
return $results;
}

function gallery_lookup_tag($tagid)
{
	global $sql;
	
	$sql->db_Select("gallery_tags","*","id = $tagid");
	if ($sql->db_Rows()) 
	{
		return execute_single($sql,1);
	}
}
?>