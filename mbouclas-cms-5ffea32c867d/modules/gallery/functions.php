<?php

function get_user_images($uid,$folder,$albumid,$lang,$thumb_size=0,$public=0)
{
	global $sql;
	$thumb_size = ($thumb_size == 0) ? GALLERY_THUMB_SIZE : $thumb_size;
	// create a new Thumbs-Object(str "imagepath", str thumb_prefix, int width, int height [, int fix ])
// last (4th) parameter is optional and desides whether the size of all thumbs is
// limited/fixed by width, height or both (0 = both, 1 = width, 2 = height)
include($_SERVER['DOCUMENT_ROOT']."/modules/gallery/class_thumbs.php");
$albumid = (!$albumid) ? 0 : $albumid;
$cThumbs = new Thumbs($_SERVER['DOCUMENT_ROOT']."$folder/album_$albumid", "t1_", $thumb_size, $thumb_size, 0);

// get the array of all found images and thumbs
$mix = $cThumbs->getImages();

if ($mix !=0) 
{
//get user images from DB
$sql->db_Select("gallery_images","image","uid = '".$uid."' AND albumid = '$albumid'");

if ($sql->db_Rows() > 0) 
{
	$res = execute_multi($sql,0);
for ($i=0;count($res) > $i;$i++)
{
	$tmp[] = $res[$i]['image'];
}
}

if (count($mix[0]) != count($tmp)) //Check if the user folder has changed and update the db
{

	//start adding new images
	for ($i=0;count($mix[0]) > $i;$i++)
	{
		$image_details = getimagesize($mix[0][$i]);
		
		$image = str_replace($_SERVER['DOCUMENT_ROOT']."$folder/album_$albumid/",'',$mix[0][$i]);
		$date_added = time();
		$image_x = $image_details[0];
		$image_y = $image_details[1];
		$image_path = $mix[0][$i];
		$thumb = str_replace($_SERVER['DOCUMENT_ROOT']."$folder/album_$albumid/",'',$mix[1][$i]);
		$filesize = filesize($image_path);
		$comments = 0;
		$views = 0;
		$available = 1;
		$orderby = $i;
		if (is_array($tmp)) 
		{
		if (!in_array($image,$tmp)) 
		{
			
		$sql->db_Insert("gallery_images","'','$albumid','$image','$image_path','$thumb',$available,$orderby,'$date_added','$image_x','$image_y','$views',
		'$uid'");

		$imageid = mysql_insert_id();
		$sql->db_Insert("gallery_images_lng","'$imageid','$lang','$image','$image','$image'");
		}	
		}
		else 
		{
		$sql->db_Insert("gallery_images","'','$albumid','$image','$image_path','$thumb','$available','$orderby','$date_added','$image_x','$image_y','$views',
		'$uid'");
//				echo "INSERT INTO gallery_images VALUES ('','$albumid','$image','$image_path','$thumb','$available','$orderby','$date_added','$image_x','$image_y','$views',
//		'$uid')<br>";
		$imageid = mysql_insert_id();
		$sql->db_Insert("gallery_images_lng","'$imageid','$lang','$image','$image','$image'");
		
		}
	}
}
}
else {
	return 0;
}
}

function get_albums($lang)
{
	$sql = new db();
	$fields = "gallery_categories_lng.album, gallery_categories_lng.image,gallery_categories_lng.description ,gallery_categories.albumid, gallery_categories.parentid, gallery_categories.main,
	gallery_categories.albumid_path";
	$tables = "gallery_categories INNER JOIN gallery_categories_lng ON (gallery_categories.albumid = gallery_categories_lng.albumid)";
	$sql->db_Select($tables,$fields, "code = '$lang' ORDER BY albumid_path");
	if ($sql->db_Rows() > 0) 
	{  
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
		$r = $sql->db_Fetch();
		foreach ($r as $key => $value) 
		{
		   	if (!preg_match("[0-9]",$key)) 
		{
			$ar[$i][$key] = $value;
		}//END OF IF
		}//END OF FOREACH 
		$ar[$i]['album'] = construct_album($r['albumid_path'],$lang);
		}//END OF FOR
		sort($ar);
	return $ar;	
	}//END OF IF
}//END OF FUNCTION

function construct_album($path,$lang) 
{ 
	$sql = new db();
	if (strstr($path,"/")) 
	{  
	$tmp = explode("/",$path);
	$ids = implode(",",$tmp);
	}//END OF IF
	else 
	{  
	$ids = $path; 
	}//END OF ELSE
$sql->db_Select("gallery_categories_lng","album","albumid IN ($ids) AND code = '$lang'");
for ($i=0;$sql->db_Rows() > $i;$i++)
	 {
	 	$r = $sql->db_Fetch();
	 	$ar[] = $r['album'];
	 }//END OF FOR	 
	 if (count($ar) > 0) 
	 {  
	 	$return = implode("/",$ar); 
	 }//END OF IF
	 else 
	 {  
	 	$return = $r['album']; 
	 }//END OF ELSE

	 return $return;
}//END OF FUNCTION construct_album

function get_album_subs($cat,$lang,$active=1) 
{ 
		$active = ($active == 1) ? "1" : "0,1";
 $sql = new db();
 $fields = "gallery_categories_lng.album, gallery_categories_lng.description, gallery_categories_lng.meta_descr, gallery_categories_lng.meta_keywords,
  gallery_categories_lng.image, gallery_categories.albumid, gallery_categories.parentid, gallery_categories.albumid_path, gallery_categories.order_by,
  gallery_categories.product_count, gallery_categories.date_added"; 
 $tables = "gallery_categories INNER JOIN gallery_categories_lng ON (gallery_categories.albumid = gallery_categories_lng.albumid)";
 $sql->db_Select($tables,"$fields","parentid = $cat AND code = '$lang' ORDER BY order_by");

 if ($sql->db_Rows() > 0) 
 {  
 $gallery_categories = execute_multi($sql,1,"date_added","short");
 for ($i=0;count($gallery_categories) > $i;$i++)
 {
 $albumid = $gallery_categories[$i]['albumid'];
 
 $sql->db_Select("gallery_images
  INNER JOIN gallery_categories ON (gallery_images.albumid = gallery_categories.albumid)",
 "gallery_images.id","(gallery_categories.albumid_path LIKE '%$albumid/%' 
 OR `gallery_categories`.albumid_path LIKE '%/$albumid') OR `gallery_categories`.albumid_path = '$albumid' 
 AND `gallery_images`.available IN ($active)");

 $gallery_categories[$i]['num_images'] = $sql->db_Rows();
 $sql->db_Select("gallery_categories","albumid","parentid = $albumid");
 $gallery_categories[$i]['num_sub'] = $sql->db_Rows();
 }//END OF FOR
return $gallery_categories;
 }//END OF IF
 else 
 {  
 return 0; 
 }//END OF ELSE
}//END OF FUNCTION get_gallery_categories

function get_album($cat,$lang) 
{ 
  $sql = new db();
  $fields  ="gallery_categories_lng.album, gallery_categories_lng.image, gallery_categories.albumid, gallery_categories_lng.description,
  gallery_categories_lng.meta_descr, gallery_categories_lng.meta_keywords, gallery_categories.order_by,gallery_categories.main, gallery_categories.parentid,gallery_categories.albumid_path";
  $tables = "gallery_categories INNER JOIN gallery_categories_lng ON (gallery_categories.albumid = gallery_categories_lng.albumid)";
  $sql->db_Select($tables,$fields,"gallery_categories.albumid = $cat AND code = '$lang'");
  return execute_single($sql,1);
}//END OF FUNCTION get_album

function format_album($data) 
{ 
  $tmp = explode("/",$data['album']);
  for ($i=0;count($tmp) > $i;$i++)
  {
  	$ar[$i]['cat'] = $tmp[$i];
  }//END OF FOR
  $tmp = explode("/",$data['albumid_path']);
  for ($i=0;count($tmp) > $i;$i++)
  {
  	$ar[$i]['catid'] = $tmp[$i];
  }//END OF FOR
  return $ar;
}//END OF FUNCTION format_album

function album_nav($cat,$lang) 
{ 
	$sql = new db();
	$sql->db_Select("gallery_categories","albumid_path","albumid = $cat") ;
	$r = $sql->db_Fetch();
	$tmp = ($sql->db_Rows() > 0) ? explode("/",$r['albumid_path']) : 0;
	if (is_array($tmp) and count($tmp) > 0) 
	{
		//found subalbum
		$fields = "gallery_categories_lng.album, gallery_categories_lng.image, gallery_categories.albumid";
		$tables = "gallery_categories INNER JOIN gallery_categories_lng ON (gallery_categories.albumid = gallery_categories_lng.albumid)";
	 	for ($i=0;count($tmp) > $i;$i++)
		{
		$sql->db_Select($tables,$fields,"gallery_categories.albumid = ".$tmp[$i]." AND code = '$lang'");
		$ar[] = execute_single($sql,1);
		}//END OF FOR
	return $ar;
	}//END OF IF subgallery_categories found
	else 
	{  
	return 0; 
	}//END OF ELSE	
}//END OF FUNCTION cat_nav

function get_album_photos($albumid,$lang,$active="no",$number="no",$type="image",$page=false)
{
	$sql = new db();
	global $smarty;
	$page = ($page == false) ? CURRENT_PAGE : $page;
	$active_photo = $active; 
	$active = ($active == "no") ? "0,1" : "1"; 
	$root = ($albumid == 0) ? 1 : 0;
	if ($albumid !=0) {
	$tables = "`gallery_images`
  INNER JOIN `gallery_categories` ON (`gallery_images`.albumid = `gallery_categories`.albumid)";	
	$fields = "`gallery_images`.id";
	$q = "(`gallery_categories`.albumid = $albumid) AND (available IN ($active))ORDER BY gallery_images.orderby";
	}
	else {
		$tables = "gallery_images";
		$fields = "id";
		$q = "albumid = $albumid AND available IN ($active) ORDER BY gallery_images.orderby";
	}
//echo "SELECT $fields FROM $tables WHERE $q";
	$sql->db_Select($tables,$fields,$q);

	if ($sql->db_Rows() >0) 
	{ 
		if ($number == "no") 
		{
			$tmp = execute_multi($sql,0);
		}
		else 
		{
			$tmp = execute_paginated($sql,0,$number,0,"date_added"); 	
		}
		for ($i=0;count($tmp) > $i;$i++)
		{
			$photos[] = get_photo($tmp[$i]['id'],$lang,$root,$active_photo);
		}//END OF FOR
	}//END OF IF rows found

	return $photos;
}//END OF GET ALBUM PHOTOS

function get_photo($id,$lang,$root=0,$active="no")
{
	$sql = new db();
	$active = ($active == "no") ? "0,1" : "1"; 
	if ($root == 0) 
	{
	$fields = "`gallery_images`.id,  `gallery_images`.albumid,  `gallery_images`.image,  `gallery_images`.image_path,
  `gallery_images`.thumb,  `gallery_images`.available,  `gallery_images`.orderby,  `gallery_images`.date_added,
  `gallery_images`.image_x,  `gallery_images`.image_y,  `gallery_images`.views,
  `gallery_images_lng`.img_title,  `gallery_images_lng`.img_alt,  `gallery_images_lng`.img_desc,
  `gallery_categories_lng`.description,  `gallery_categories_lng`.meta_descr,  `gallery_categories_lng`.meta_keywords,
  `gallery_categories_lng`.album";
	$tables = "`gallery_images`
  INNER JOIN `gallery_categories` ON (`gallery_images`.albumid = `gallery_categories`.albumid)
  INNER JOIN `gallery_images_lng` ON (`gallery_images`.id = `gallery_images_lng`.imageid)
  INNER JOIN `gallery_categories_lng` ON (`gallery_categories`.albumid = `gallery_categories_lng`.albumid)";
	$q = "(`gallery_images`.id = $id) AND (`gallery_images_lng`.code = '$lang') AND 
  (`gallery_categories_lng`.code = '$lang') AND available IN ($active)";
	}
	else 
	{
	$fields = "`gallery_images`.id,  `gallery_images`.albumid,  `gallery_images`.image,  `gallery_images`.image_path,
  `gallery_images`.thumb,  `gallery_images`.available,  `gallery_images`.orderby,  `gallery_images`.date_added,
  `gallery_images`.image_x,  `gallery_images`.image_y,  `gallery_images`.views,
  `gallery_images_lng`.img_title,  `gallery_images_lng`.img_alt,  `gallery_images_lng`.img_desc";
	$tables = "`gallery_images`
  INNER JOIN `gallery_images_lng` ON (`gallery_images`.id = `gallery_images_lng`.imageid)";
	$q = "(`gallery_images`.id = $id) AND (`gallery_images_lng`.code = '$lang') AND available IN ($active)";		
	}

  $sql->db_Select($tables,$fields,$q);
//  echo "SELECT $fields FROM $tables WHERE $q<br>";
  return execute_single($sql,1,"date_added");
}

function gallery_cat_nav($cat,$lang) 
{ 
	$sql = new db();
	$sql->db_Select("gallery_categories","albumid_path","albumid = $cat") ;
	$r = $sql->db_Fetch();
	$tmp = ($sql->db_Rows() > 0) ? explode("/",$r['albumid_path']) : 0;
	if (is_array($tmp) and count($tmp) > 0) 
	{
		//found subcategory
		$fields = "gallery_categories_lng.album, gallery_categories.albumid";
		$tables = "gallery_categories
  INNER JOIN gallery_categories_lng ON (gallery_categories.albumid = gallery_categories_lng.albumid)";
	 	for ($i=0;count($tmp) > $i;$i++)
		{
		$sql->db_Select($tables,$fields,"gallery_categories.albumid = ".$tmp[$i]." AND code = '$lang'");
		$ar[] = execute_single($sql,1);
		}//END OF FOR
	return $ar;
	}//END OF IF subcategories found
	else 
	{  
	return 0; 
	}//END OF ELSE	
}//END OF FUNCTION cat_nav

function album_translations($id) 
{ 
	global $smarty,$sql;
$countries = get_countries("Y",$trans="yes");
$class_lng = array();
$translations = array();

for ($i=0,$j=0,$d=0;count($countries) > $i;$i++)
{
	$sql->db_Select("gallery_categories_lng","*","code = '".$countries[$i]['code']."' AND albumid = $id");
	if ($sql->db_Rows() == 0) 
	{  
	 	$cat_lng[$j]['code'] =  $countries[$i]['code'];
	 	$cat_lng[$j]['country'] =  $countries[$i]['country'];
	 	$j++;
	}//END OF IF
	else 
	{  
	//see for possible translations
	 	$r = $sql->db_Fetch();
	 	$cat_details = get_lang_name($r['code']);
	 	foreach ($r as $key => $value) 
	 	{
	 	   	if (!preg_match("[0-9]",$key)) 
	 	{
	 		$translations[$d][$key] = $value;
	 	}//END OF IF
	 	}//END OF FOREACH 
	 	$translations[$d]['country'] = $cat_details['country'];
	 	$d++;
	}//END OF ELSE	
}//END OF FOR
$smarty->assign("trans",$translations);//assigned template variable trans  
$smarty->assign("cat_lng",$cat_lng);//assigned template variable product_lng  
}//END OF FUNCTION extra_fields_translations
?>
