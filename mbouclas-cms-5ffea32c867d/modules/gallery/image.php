<?php
include($_SERVER['DOCUMENT_ROOT']."/init.php");
include("functions.php");
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ###########################################

$a = load_module_prefs("gallery_settings","gallery_settings_lng");

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
$id = check_var($id,"gallery_images","id");

$image = get_photo($id,FRONT_LANG,1);
$album = get_album($image['albumid'],FRONT_LANG);
$cat_images = get_album_photos($image['albumid'],FRONT_LANG,1,GALLERY_PAGINATION);


$medium_image = $_SERVER['DOCUMENT_ROOT'].GALLERY_SAVE_IMAGES_FOLDER."/album_".$image['albumid']."/m_".$image['image'];
$main_image = $_SERVER['DOCUMENT_ROOT'].GALLERY_SAVE_IMAGES_FOLDER."/album_".$image['albumid']."/".$image['image'];
if (!file_exists($medium_image)) {
	createResizedImage($main_image,$medium_image,0,GALLERY_MEDIUM_IM_WIDTH,GALLERY_MEDIUM_IM_WIDTH,85);	
}

for ($i=0;count($cat_images) > $i;$i++)
{
	if ($cat_images[$i]['id'] == $id)
	{
		$smarty->assign("previous_image",$cat_images[$i-1]);//assigned template variable previous_image
		$smarty->assign("next_image",$cat_images[$i+1]);//assigned template variable previous_image
	}
}//END OF FOR



################# UPDATE THE VIEWS FIELD ############################
$sql->db_Update("gallery_images","views = views+1 WHERE id = $id");
$smarty->assign("nav",gallery_cat_nav($image['albumid'],FRONT_LANG));//assigned template variable a
$smarty->assign("image",$image);
$smarty->assign("cat_images",$cat_images);
$smarty->assign("album",$album);
$smarty->assign("include_file","modules/gallery/image.tpl");//assigned template variable include_file
$smarty->display("home.tpl");//Display the home.tpl template
?>