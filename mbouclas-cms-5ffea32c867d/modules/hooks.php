<?php

class hooks {
	
	public function getHooks($fileName,$settings=0) {
		global $sql;
	    $f=HookParent::getInstance()->doTriggerHookGlobally("LoadHook",$fileName);
	    $f = ( $f == '' ) ? $fileName : $f; 
	    
	    $fields = ($settings['fields']) ? $settings['fields'] : '*';
		
		$sql->db_Select("hooks",$fields,"target = '$f'");		
		if ($sql->db_Rows()) {
			$results=execute_multi($sql);
		  foreach ($results as $k=>$v) {	
			$res[$v['type']][] = $v;
		}
	    	}

		return $res;
	}
	
	public function loadHooks($ar,$settings=0) {
		if (is_array($ar)) {
			foreach ($ar as $v) {
				include(ABSPATH.$v['file']);
			}
		}
	}
	
}//END CLASS


?>