<?php
include("../../../manage/init.php");//load from manage!!!!
############### THIS FILE HAS BEEN MODIFIED TO ADD MEDIA TYPES OF VARIOUS KINDS ######################
############### AVAILABLE TYPES ARE THUMB - IMAGES - VIDEOS - DOCS ###################################
if (array_key_exists("listings",$loaded_modules)) 
{
	$current_module = $loaded_modules['listings'];
	$smarty->assign("MODULE_FOLDER",URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$smarty->assign("CURRENT_MODULE",$current_module);
define("content_images",$current_module['settings']['images_path']);
include($_SERVER['DOCUMENT_ROOT']."/includes/class_thumbs.php");
$t = new textparse();
$Content = new Items(array('module'=>$current_module));
$type = ($_GET['type']) ? $_GET['type'] : $_POST['type'];

$id = (!empty($_GET['id'])) ? $_GET['id'] : $_POST['id'];
$media = ($_GET['media']) ? $_GET['media'] : "thumb";//THIS IS THE VARIABLE THAT WILL DEFINE THE TYPE OF MEDIA

$item_settings = array('fields'=>'*','main'=>1,'CatNav'=>1,'debug'=>0,'thumb'=>1);
$item = $Content->GetItem($id,$item_settings);

$MediaFilesSettings = MediaFiles(array('single'=>1,'type'=>$media)); 
    $FilesAllowed = $MediaFilesSettings['extentions'];
    $FilesFilters = explode(";",$FilesAllowed);
    for ($i=0;count($FilesFilters) > $i;$i++) {
    $FilesFilters[$i] = str_replace('*.','',$FilesFilters[$i]);
    }//END FOR	

if ($_GET['action'] == 'DeleteImage') {
	include($_SERVER['DOCUMENT_ROOT']."/modules/common_functions.php");
		$settings_arr = array (
		'lang' => DEFAULT_LANG,
		'module' => $current_module,
		'itemid' => $content['id'],
		'uid' => ID
		);
	$a = new HandleUploadedItem($settings_arr);
	$a->RemoveFromDb(array('itemid'=>$_GET['imageid']));
	header("Location: content_modify_img.php?id=".$_GET['id']."&type=".$_GET['type']."&media=".$_GET['media']);
	exit();
}//END DELETE IMAGE
elseif ($_GET['action'] == 'EditImage')
{
	$sql->db_Select("item_images_aditional","*","imageid = ".$_GET['imageid']." AND itemid = ".$_GET['id']." AND image_type = 'main'");
	$image = execute_single($sql);
	$smarty->assign("CropImage",$image);
//	$smarty->assign("ImageSize",getimagesize($_SERVER['DOCUMENT_ROOT'].));
}

$ImagesCls = new ItemImages(array('module'=>$current_module));

$imageCategories = $ImagesCls->ImageCategories();
$smarty->assign('image_categories',$imageCategories);


if ($media == 'thumb' OR $media == 'image') 
{
if ($media == 'thumb') {
	$_GET['type'] = 0;
	$type = 0;
}
else {
	if (!$type) {
		foreach ($imageCategories as $v) {
			if ($v['settings']['default']) {
				$type = $v['id'];
			}
		}
	}
}

$db_type = (isset($_GET['type'])) ? $_GET['type'] : $type;
//$all_images = get_folder_content_images(ID,$uploaddir,$content['id'],$current_module);
$images = $ImagesCls->GetItemDetailedImages($id,$db_type);

$smarty->assign("image_types",get_image_types($current_module['id']));
}//END IMAGES
elseif ($media == 'video') 
{
    $Docs = new ItemMedia(array('module' => $current_module,'itemid' => $id,'debug'=>0,'orderby' => 'orderby'));
    $smarty->assign("videos",$Docs->ItemVideos());
    $type = ($type) ? $type : 'FromFile';
}
elseif ($media == 'document') 
{
    $Docs = new ItemMedia(array('module' => $current_module,'itemid' => $id,'debug'=>0,'orderby' => 'orderby'));
    $smarty->assign("docs",$Docs->ItemDocuments());
}//END DOCUMENTS
}//END MODULE

$smarty->assign("FilesAllowed",$FilesAllowed);
$smarty->assign("media",$media);
$smarty->assign("allcategories",$Content->AllItemsTreeCategories());
$smarty->assign("more_content",$Content->LatestItems(array('results_per_page'=>10,'active'=>2,'get_provider'=>0,'get_user'=>0,'debug'=>0,'just_results'=>1
		,'categoryid'=>$item['category']['catid'])));
$smarty->assign("id",$id);
$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","detailed_images");//USED ON ADDITIONAL MENUS
$smarty->assign('nav',$item['nav']);
$smarty->assign("type",$type);//assigned template variable type
$smarty->assign("images",$images);//assigned template variable images 
$smarty->assign("item",$item);//assigned template variable content
$smarty->assign("action_title", $content['title']);//assigned template variable action_title
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/listings/admin/detailed_images.tpl");
$smarty->display("admin/home.tpl");

?>