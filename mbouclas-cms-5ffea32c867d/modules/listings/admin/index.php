<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("listings",$loaded_modules)) {
	$current_module = $loaded_modules['listings'];
	$smarty->assign("MODULE_FOLDER",$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$Content = new Items(array('module'=>$current_module,'debug'=>0));
	$items_per_page = ($current_module['settings']['items_per_page_admin']) ? $current_module['settings']['items_per_page_admin'] : 30;
    $posted_data = array('results_per_page'=>$items_per_page,'active'=>2,'debug'=>0,'just_results'=>1,
	'cat_details'=>1);
	$smarty->assign("latest",$Content->LatestItems($posted_data));
}
$smarty->assign("SavedData",form_settings_string($posted_data));
$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","main");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']);
$smarty->assign("include_file",$current_module['folder']."/admin/main.tpl");
$smarty->display("admin/home.tpl");

?>