<?php
include("../../../manage/init.php");//load from manage!!!!
$current_module = $loaded_modules[CURRENT_MODULE];
$current_module = $loaded_modules[CURRENT_MODULE];
$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
$module_path = URL."/".$current_module['folder']."/admin";
$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

$item_settings = array('fields'=>'*','thumb'=>0,'CatNav'=>1,'debug'=>0);
$item = $Content->GetItem($id,$item_settings);
$smarty->assign("item",$item);
$smarty->assign("id",$id);
$smarty->assign('nav',$item['nav']);

$smarty->assign("related",$Content->RelatedContent($id,array('debug'=>0,'orderby'=>'orderby','get_results'=>1)));

$smarty->assign("menu",$module['name']);
$smarty->assign("section","related");//assigned template variable extra_field
$smarty->assign("submenu","modify");//assigned template variable extra_field
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/listings/admin/content_modify_related.tpl");
$smarty->display("admin/home.tpl");

?>