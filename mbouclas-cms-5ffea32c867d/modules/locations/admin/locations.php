<?php
include("init.php");
include($_SERVER['DOCUMENT_ROOT']."/ajax_functions.php");
$xajax = new xajax("","xajax_",'ISO-8859-7');
$xajax->registerFunction("tab_go");
$sql = new db();
$t = new textparse();
$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
$gid = ($_GET['gid']) ? $_GET['gid'] : $_POST['gid'];
$sid = ($_GET['sid']) ? $_GET['sid'] : $_POST['sid'];

$countries = get_countries(2,$trans="yes");

if ($_GET['m'] == "del_group") //Delete locations Group
{  
 $sql->db_Delete("locations_groups","id= ".$_GET['delid']."");
 $sql->db_Delete("locations_groups_lng","groupid= ".$_GET['delid']."");
 $sql->db_Update("locations","groupid = '' WHERE groupid= ".$_GET['delid']."");//clear all locations using this group
}//END OF IF
if ($_GET['m'] == "del_group_trans") 
{  
 $sql->db_Delete("locations_groups_lng","groupid= $gid AND code = '".$_GET['code']."'");
}//END OF IF
if ($_GET['mode'] == "new_group") 
{  
$smarty->assign("new_group",1);//assigned template variable new 
}//END OF IF
if ($_POST) 
{
if ($_POST['mode'] == "add_group") //Add new group
{
	if ($_POST['img']) 
	{  
	  $image = (preg_match(PRODUCT_IMAGES,$_POST['img'])) ? $_POST['img'] : PRODUCT_IMAGES.$_POST['img']; 
	}//END OF IF
	  $sql->db_Insert("locations_groups","'','".$_POST['orderby']."','".$_POST['active']."','$image','".$_POST['display']."'");
	  $gid = mysql_insert_id();
	  $sql->db_Insert("locations_groups_lng","'$gid','".DEFAULT_LANG."','".$t->formtpa($_POST['name'])."'");
########### REPLICATE DATA FOR TRANSLATIONS ###############################
$countries = get_countries("Y",$trans="yes");
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert("locations_groups_lng","'$gid','".$countries[$i]['code']."','".$t->formtpa($_POST['name'])."'");
}//END OF FOR
}//END OF IF
	  header("Location: $PHP_SELF?gid=$gid");
}

if ($_POST['mode'] == "save_group") //Update group info
{
	$image = (preg_match(PRODUCT_IMAGES,$_POST['img'])) ? $_POST['img'] : PRODUCT_IMAGES.$_POST['img'];   
	$sql->db_Update("locations_groups","orderby = '".$_POST['orderby']."',active = '".$_POST['active']."',image = '$image', display = '".$_POST['display']."' WHERE id = $gid");
	$sql->db_Update("locations_groups_lng","name = '".$t->formtpa($_POST['name'])."' WHERE groupid = $gid AND code = '".DEFAULT_LANG."'");
}//END OF IF

if ($_POST['mode'] == "upd_group") //Update group translations
{ 
		foreach ($_POST as $key => $val)
		{
		if (strstr($key,"$")) 
		{
			$key = str_replace("$","-",$key);
			list($field,$code)=split("-",$key);
			$query = "$field = '$val' WHERE groupid = $gid AND code = '$code'";
			$sql->db_Update("locations_groups_lng",$query);
		}//END OF IF
		}//END OF WHILE
}//END OF ELSEIF

if ($_POST['mode'] == "insert_group") //Insert Group Translations
{  
$sql->db_Insert("locations_groups_lng","'$gid','".$t->formtpa($_POST['locations_lng'])."','".$t->formtpa($_POST['locations_new_title'])."'");
}//END OF IF


if ($_POST['mode'] == "mod_groups") //Group locations mass modify
{  
		foreach ($_POST as $key => $val)
		{
		
		if (strstr($key,"-")) 
		{
			list($field,$code)=split("-",$key);
			$query = "$field = '$val' WHERE id = $code";
			$sql->db_Update("locations_groups",$query);
		}//END OF IF
		if (strstr($key,"-lng-")) 
		{
			list($field,$code)=split("-lng-",$key);
			$query = "$field = '$val' WHERE groupid = $code AND code = '".DEFAULT_LANG."'";
			$sql->db_Update("locations_groups_lng",$query);
		}//END OF IF
		}//END OF WHILE
}//END OF IF
	
if ($_POST['mode'] == "modify") 
 {  
 		foreach ($_POST as $key => $val)
		{		
		if (strstr($key,"-")) 
		{
			list($field,$name)=split("-",$key);
			$query = "$field = '$val' WHERE id = '$name'";
			$sql->db_Update("locations",$query);
			$query = "$field = '$val' WHERE locationsid = '$name' AND code = '".DEFAULT_LANG."'";
			$sql->db_Update("locations_lng",$query);
		}//END OF IF
		}//END OF WHILE   
 }//END OF IF 
 $smarty->assign("sid",$_POST['sid']);
}//END OF IF


if ($_POST['mode'] == "update") 
{  
	$sql->db_Update("locations","orderby = '".$_POST['orderby']."',active = '".$_POST['active']."', main = '".$_POST['main']."' WHERE id = $id");
	$sql->db_Update("locations_lng","title = '".$t->formtpa($_POST['title'])."',locations = '".$t->formtpa($_POST['locations'])."',
	link = '".$t->formtpa($_POST['link'])."', meta_descr = '".$t->formtpa($_POST['meta_descr'])."', 
	meta_keywords = '".$t->formtpa($_POST['meta_keywords'])."' WHERE locationsid = $id AND code = '".DEFAULT_LANG."'");
}//END OF IF
elseif ($_POST['mode'] == "add") 
{ 
	$date_added = time();
  $query = "'','".$_POST['active']."','".$_POST['orderby']."','".$_POST['groupid']."','".$_POST['main']."',$date_added";
  $sql->db_Insert("locations",$query);
  $cid = mysql_insert_id(); 
  $query = "'".DEFAULT_LANG."','$cid','".$t->formtpa($_POST['title'])."',
  '".$t->formtpa($_POST['link'])."','".$t->formtpa($_POST['locations'])."',
  '".$t->formtpa($_POST['meta_descr'])."','".$t->formtpa($_POST['meta_keywords'])."'";
  $sql->db_Insert("locations_lng",$query);
  if ($_POST['main'] == 1) {
  	$sql->db_Update("locations","main = 0 WHERE groupid = ".$_POST['groupid']);//RESET ALL OTHER MAIN PAGES
  	$sql->db_Update("locations","main = 1 WHERE id = ".$cid);//NEW PAGE
  }
########### REPLICATE DATA FOR TRANSLATIONS ###############################

if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
  $query = "'".$countries[$i]['code']."','$cid','".$t->formtpa($_POST['title'])."',
  '".$t->formtpa($_POST['link'])."','".$t->formtpa($_POST['locations'])."',
  '".$t->formtpa($_POST['meta_descr'])."','".$t->formtpa($_POST['meta_keywords'])."'";
  $sql->db_Insert("locations_lng",$query);
}//END OF FOR
}//END OF IF
  header("Location: $PHP_SELF?id=$cid");
}//END OF ELSEIF

if ($_GET['mode'] == "new") 
{  
$smarty->assign("new",1);//assigned template variable new 
}//END OF IF


if ($_GET['action'] == "delete") 
{ 
  $sql->db_Delete("locations","id = ".$_GET['id']."");
  $sql->db_Delete("locations_lng","locationsid = ".$_GET['id']."");
}//END OF ELSEIF

if ($id) 
{
//TRANSLATIONS
if ($_GET['m'] == "del") 
{  
 $sql->db_Delete("locations_lng","locationsid= ".$_GET['id']." AND code = '".$_GET['code']."'");
}//END OF IF
if ($_POST['mode'] == "insert") 
{  
$sql->db_Insert("locations_lng","'".$t->formtpa($_POST['locations_lng'])."','$id','".$t->formtpa($_POST['locations_new_title'])."',
'".$t->formtpa($_POST['locations_new_link'])."','".$t->formtpa($_POST['locations_new_text'])."',
'".$t->formtpa($_POST['meta_description_new'])."','".$t->formtpa($_POST['meta_keywords_new'])."'");
}//END OF IF
elseif ($_POST['mode'] == "upd") 
{ 

		foreach ($_POST as $key => $val)
		{
		
		if (strstr($key,"$")) 
		{
			$key = str_replace("$","-",$key);
			list($field,$code)=split("-",$key);
			$query = "$field = '$val' WHERE locationsid = $id AND code = '$code'";

			$sql->db_Update("locations_lng",$query);
		}//END OF IF
		}//END OF WHILE
}//END OF ELSEIF

locations_translations($id); 
$cont = get_locations(0,DEFAULT_LANG,$id);
$sid = $cont['groupid'];
$smarty->assign("cont",$cont);//assigned template variable cont 
}//END OF IF

editor("description","description1");

if ($gid) 
{
locations_group_translations($gid);
$smarty->assign("group",get_locations_group($gid,DEFAULT_LANG));//assigned template variable cont  
$smarty->assign("gid",1);//assigned template variable gid 
$smarty->assign("new_group",1);//assigned template variable gid 
}//END OF IF
if ($sid != 0) 
{
if (is_array($sid)) {$sid = implode(",",$sid);}
$locations = get_locations(0,DEFAULT_LANG,0,$sid);
//print_r($locations);
$smarty->assign("sid",$sid);
}//END OF IF

else { $locations = get_locations(0,DEFAULT_LANG);}
$smarty->assign("locations",$locations);//assigned template variable locations


$smarty->assign("countries",$countries);
$smarty->assign("allcategories",get_categories(DEFAULT_LANG_ADMIN));//assigned template variable allcategories
$smarty->assign("nav",cat_nav($sid,DEFAULT_LANG_ADMIN));//assigned template variable a
$smarty->assign("file","locations.php");
$smarty->assign("var","sid");
$smarty->assign("menu","locations");
$smarty->assign("submenu","pages");
$smarty->assign("imanager",1);//assigned template variable imanager
$smarty->assign("groups",get_locations_groups("no",DEFAULT_LANG_ADMIN));//assigned template variable groups
$smarty->assign("pop_edit",1);//assigned template variable pop_edit
$smarty->assign("locations",$locations);//assigned template variable locations
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","admin/locations.tpl");
$smarty->display("admin/home.tpl");

?>