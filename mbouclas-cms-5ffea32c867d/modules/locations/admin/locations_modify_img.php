<?php
include("../../../manage/init.php");//load from manage!!!!

if ($locations_module = module_is_active("locations",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$locations_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$locations_module['settings']);
define("locations_images",$locations_module['settings']['images_path']);
include($_SERVER['DOCUMENT_ROOT']."/includes/class_thumbs.php");
$t = new textparse();
$type = ($_GET['type']) ? $_GET['type'] : $_POST['type'];
$type = ($type) ? $type : $locations_module['settings']['default_image_type'];

$id = (!empty($_GET['id'])) ? $_GET['id'] : $_POST['id'];

$locations = get_locations($id,DEFAULT_LANG);

if ($_GET['m'] == "del") 
{  
 $sql->db_Select("locations_images","image,thumb","id = ".$_GET['img']);
 $a = execute_single($sql);
 $sql->db_Delete("locations_images","id = ".$_GET['img']);
 if ($a['thumb'] == $locations['image']) 
 {
 	$sql->db_Update("locations","image = '".$locations_module['settings']['default_thumb']."' WHERE id = ".$locations['id']);
 }
 @unlink($_SERVER['DOCUMENT_ROOT'].$a['image']);
 @unlink($_SERVER['DOCUMENT_ROOT'].$a['thumb']);
}//END OF IF

if (!empty($_POST)) 
{  
if ($_POST['new_image_category']) 
{
	
	$sql->db_Insert("locations_images_categories","'','".$_POST['new_image_category']."'");
	//update settings
	$sql->db_Select("modules_settings","options","module_id = ".$locations_module['id']." AND name = 'default_image_type'");
	$tmp = execute_single($sql);
	$temp = form_settings_array($tmp['options'],"###",":::");
	$temp[$_POST['new_image_category']] = mysql_insert_id();
	$set = form_settings_string($temp,"###",":::");
	$sql->db_Update("modules_settings","options = '$set' WHERE module_id = ".$locations_module['id']." AND name = 'default_image_type'");
	header("Location: ".URL."/".$locations_module['folder']."/admin/locations_modify_img.php?id=$id&type=".$temp[$_POST['new_image_category']]."#view");
}

if ($_POST['action'] == "modify") 
{  
foreach ($_POST as $key => $val)
{
			if (strstr($key,"-")) 
		{
			list($field,$newid)=split("-",$key);
			$query = "$field = '$val' WHERE id = $newid AND type = $type";
//echo "$query<Br>";
			$sql->db_Update("locations_images",$query);
			


		}//END OF IF
			if ($key == "thumb") 
			{
				$sql->db_Update("locations_images","type = '$type' WHERE type IN (0,$type) AND itemid = ".$locations['id']);//reset
				$sql->db_Update("locations_images","type = '0' WHERE id = $val");
				$sql->db_Select("locations_images","thumb","id = $val");
				$tmp = execute_single($sql);
				$sql->db_Update("locations","image = '".$tmp['thumb']."' WHERE id = ".$locations['id']);
				$locations['image'] = $tmp['thumb'];
			}

}
 header("Location:locations_modify_img.php?id=$id&type=$type");
 exit();
}//END OF IF UPDATE EXISTING IMAGES
elseif ($_POST['action'] == "delete_image_category")
{
	$sql->db_Select("locations_images_categories","title","id = ".$_POST['image_cat']);
	$tmp = execute_single($sql);
	$sql->db_Delete("locations_images_categories","id = ".$_POST['image_cat']);
//update settings
	$sql->db_Select("modules_settings","options","module_id = ".$locations_module['id']." AND name = 'default_image_type'");
	$tmp1 = execute_single($sql);
	$temp = form_settings_array($tmp1['options'],"###",":::");
	unset($temp[$tmp['title']]);
	$set = form_settings_string($temp,"###",":::");
	$sql->db_Update("modules_settings","options = '$set' WHERE module_id = ".$locations_module['id']." AND name = 'default_image_type'");
	//Move Images To zero category
	$sql->db_Update("locations_images","type=0 WHERE type = ".$_POST['image_cat']);
}
 }//END OF POSTED FORM
 else 
{  
//Retrieve locations details

if ($locations !=0) 
{  



}//END OF locations found
else 
{  
header("Location: search.php"); 
exit();
}//END OF ELSE
}//END OF NON SUBMITED FORM
//locations_r($locations);


//check for directories
	$cat_dir = locations_images."/category_".$locations['catid']."/";
	$uploaddir = $cat_dir."locations_".$locations['id']; 

	if (!is_dir($_SERVER['DOCUMENT_ROOT'].$uploaddir)) {
		//check cat folder
		create_locations_folders($cat_dir,$uploaddir);
	}

$db_type = ($_GET['type']) ? $_GET['type'] : $locations_module['settings']['default_image_type'];

//$all_images = get_folder_locations_images(ID,$uploaddir,$locations['id'],$locations_module);

$images = get_locations_detailed_images($id,$db_type);

$smarty->assign("image_categories",locations_images_categories());

	if ($manufacturers_module = module_is_active("manufacturers",1,1)) 
	{
		$smarty->assign("manufacturers_list",get_manufacturers(DEFAULT_LANG));
		$smarty->assign("manufacturers_module",$manufacturers_module);
		$locations['mid'] = get_locations_manufacturer($id);
	}
	############# END OF  MANUFACTURERS MODULE #####################################
	
	############# CHECK OUT FOR ESHOP MODULE #######################################
	
	if ($eshop_module = module_is_active("eshop",1,1)) 
	{
		$locations['details'] = eshop_product_details($id);
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
		
	}
}//END OF MODULE

$smarty->assign("allcategories",get_all_locations_categories(DEFAULT_LANG));//assigned template variable allcategories
$smarty->assign("more_locations",get_latest_locations($locations_module['settings']['latest_locations'],DEFAULT_LANG,"no",$locations['catid']));
$smarty->assign("id",$id);
$smarty->assign("menu",$locations_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","detailed_images");//USED ON ADDITIONAL MENUS
$smarty->assign("nav",locations_cat_nav($locations['catid'],DEFAULT_LANG));
$smarty->assign("redir_url",ADMIN_URL."/locations_modify_img.php?id=$id&type=$type");
$smarty->assign("type",$type);//assigned template variable type
$smarty->assign("imanager",1);//assigned template variable imanager
$smarty->assign("images",$images);//assigned template variable images 
$smarty->assign("locations",$locations);//assigned template variable locations
$smarty->assign("form_destination",$form_destination['locations_modify']);//assigned template variable form_destination
$smarty->assign("action_title", $locations['title']);//assigned template variable action_title
$smarty->assign("action","modify");//assigned template variable mode
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/locations/admin/detailed_images.tpl");
$smarty->display("admin/home.tpl");

?>