<?php
include("../../../manage/init.php");//load from manage!!!!

if ($locations_module = module_is_active("locations",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$locations_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
	
	

	
################################################## LOAD locations ############################################################
	$locations = get_locations($id,DEFAULT_LANG);

	if (!empty($locations)) 
	{  
	$sql->db_Select("locations_page_categories","catid","locationsid = $id");
	$all_locations_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_locations_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_locations_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_locations",get_latest_locations($locations_module['settings']['latest_locations'],DEFAULT_LANG,"no",$locations['catid']));
	$smarty->assign("extra_fields",get_locations_extra_fields("Y",DEFAULT_LANG,$id));

	$smarty->assign("id",$id);
	$smarty->assign("tags",get_locations_tags($id,","));
	$smarty->assign("nav",locations_cat_nav($locations['catid'],DEFAULT_LANG));

	


	$smarty->assign("locations",$locations);
	$smarty->assign("locations_module",$locations_module);
	$smarty->assign("action","update");
	}//END OF locations found
	else 
	{  
		header("Location: search.php"); 
		exit();
	}//END OF ELSE
################################################ END OF LOAD locations #########################################

			####################### LOAD AJAX  ###########################
			include($_SERVER['DOCUMENT_ROOT']."/".$locations_module['folder']."/admin/ajax_functions.php");
			###################### END OF AJAX #######################################	
			
}

$smarty->assign("related",get_related_locations($id,DEFAULT_LANG));
$smarty->assign("USE_AJAX","modules/locations/admin/locations_ajax.tpl");
$smarty->assign("menu",$locations_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","modify");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/locations/admin/locations_modify_related.tpl");
$smarty->display("admin/home.tpl");

?>