<?php
include("../../../manage/init.php");//load from manage!!!!

if ($locations_module = module_is_active("locations",1,1)) 
{
	$module_path = URL."/".$locations_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",$module_path);
$t = new textparse();
$page = $_POST['page'];
################################################ MASS ACTIVATE/DEACTIVATE ###################################
if ($_POST['mode'] == "deactivate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("locations","active = 0 WHERE id = $key");				
			}
		}//END OF WHILE

	if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
	
}

if ($_POST['mode'] == "activate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("locations","active = 1 WHERE id = $key");				
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
}
################################################ END MASS ACTIVATE/DEACTIVATE ###############################

################################################ MASS DELETE ###############################
if ($_POST['mode'] == "delete") 
{
	
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			if ($val == 1)//eliminates the check all/none box 
			{
	$locations_details = get_locations($id,DEFAULT_LANG);

	$categoryid = $locations_details['catid'];  
//delete the locations
$sql->db_Delete("locations","id = $id"); 
//delete translations
$sql->db_Delete("locations_lng","locationsid = $id"); 
//delete locations links
$sql->db_Delete("locations_links","locations_source = $id"); 
$sql->db_Delete("locations_links","locations_dest = $id"); 
//delete from any category
$sql->db_Delete("locations_page_categories","locationsid = $id"); 
//delete from any category
$sql->db_Delete("locations_locations","locationsid = $id"); 
//delete from images
$sql->db_Delete("locations_images","locationsid = $id"); 
//delete from featured locations
$sql->db_Delete("featured_locations","locationsid = $id"); 
//delete from extra fields
$sql->db_Delete("locations_extra_field_values","locationsid = $id"); 
//delete from classes
$sql->db_Delete("locations_class_locations","locationsid = $id"); 
//delete from bookmars
$sql->db_Delete("bookmars","locationsid = $id");
//Delete from feeds
$sql->db_Delete("feeds","locationsid = $id");
//Subtract from locations count
$sql->db_Update("locations_categories","locations_count = locations_count-1 WHERE categoryid = '$categoryid'");
//delete recipe features
//ALL DONE			
$locations_dir = $_SERVER['DOCUMENT_ROOT'].$locations_module['settings']['images_path']."/category_$categoryid/locations_$id/";
recursive_remove_directory($locations_dir);
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
			exit();
		}
		else {
				header("Location:".$module_path."/$page");
				exit();
			}	
}

################################################ END MASS DELETE ###############################

############################################### MASS FIRST ACTIVATION ##############################3
if ($_POST['mode'] == "first_activate") 
{
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "locations_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("locations_lng","title = '$val' WHERE locationsid = $code");
							
						}
						else 
						{
							$sql->db_Update("locations","$field = '$val' WHERE id = $code");
							
						}
						
					}
		}
	
	foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
					$sql->db_Update("locations","active = 0 WHERE id = $code");
			}
		}
		//clear up
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS EDIT ##############################3
if ($_POST['mode'] == "mass_edit") 
{
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "locations_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("locations_lng","title = '$val' WHERE locationsid = $code");
							
						}
						elseif ($field == "categoryid")
						{
								//PRODUCT CATEGORIES
								$sql->db_Select("locations_page_categories","catid","locationsid = $code AND main = 'Y'");

								if ($sql->db_Rows() > 0) //UPDATE
								{
									$sql->db_Update("locations_page_categories","catid = $val WHERE locationsid = $code AND main = 'Y'");
								}
								else 
								{
									$sql->db_Insert("locations_page_categories","'$val','$code','Y'");
								}
						}
						else 
						{
							$sql->db_Update("locations","$field = '$val' WHERE id = $code");
							
						}
						
					}
		}
	
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS DELETE IMAGES ##############################3
if ($_POST['mode'] == "delete_images") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
						
						list($field,$code)=split("-",$key);
						$sql->db_Select("locations_images","image,thumb,locationsid","id = $key");
 						$a = execute_single($sql);
 						$sql->db_Select("locations","image","id = ".$a['locationsid']);
 						$locations = execute_single($sql);
// 						print_r($locations);
 						if ($a['thumb'] == $locations['image']) 
 						{
 							$sql->db_Update("locations","image = '".$locations_module['settings']['default_thumb']."' WHERE id = ".$a['locationsid']);
 						}
 						@unlink($_SERVER['DOCUMENT_ROOT'].$a['image']); 
						@unlink($_SERVER['DOCUMENT_ROOT'].$a['thumb']);
 						$sql->db_Delete("locations_images","id = $key");
//echo $key."<br>";
					}
		}
	
		header("Location:".$module_path."/$page");
		exit();
}
############################################### MASS ACTIVATE IMAGES ##############################3
if ($_POST['mode'] == "activate_all_images") 
{
	$id = $_POST['id'];
	$sql->db_Update("locations_images","available = 1 WHERE itemid = $id AND type = ".$_POST['type']);
	header("Location:".$module_path."/$page");
	exit();
}

}//END OF LOAD MODULE
?>