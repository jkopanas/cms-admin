<?php
session_start();
//make sure that it is a GET/POST data and not SESSION
$check_search = ($_POST['search']) ? $_POST['search'] : $_GET['search'];
if (empty($check_search))//IF SESSION data and not GET/POST it is a new search and should be cleared 
{  
$_SESSION['data'] = ""; 
}//END OF IF

include("../../../manage/init.php");//load from manage!!!!

if ($locations_module = module_is_active("locations",1,1,1)) 
{
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ##########################################
//print_r($_SESSION);
	$module_path = URL."/".$locations_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",URL."/".$locations_module['folder']."/admin");
	$smarty->assign("allcategories",get_all_locations_categories(DEFAULT_LANG));
//	print_r($_SESSION);
//user is posting variables for a new search
if ($_POST['search']) 
{  
 $posted_data = array();//clear array
 $_SESSION['data'] = "";//clear the session to add the new data
 foreach ($_POST as $key => $value) //build the array that will hold the search data
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 $_SESSION['data'] = $posted_data;//setup the session
 $smarty->assign("search",1);
}//END OF IF
else//Pagination or refresh
{  
$posted_data = array();//setup a a clean array
$posted_data = $_SESSION['data'];//assign data to it
}
//if we have a setup array proceed with the search query
if ($posted_data) 
{
//Check if the sort field passed is valid
//$var = array_flip ($sort_fields);
//$result = in_array($_GET['sort'], $var, TRUE);
$sort = ($result == 1) ? $_GET['sort'] : $locations_module['settings']['default_product_sort'];
//End check
$direction = ($_GET['sort_direction'] == 0) ? 0 : 1;
$selected = ($_GET['sort']) ? $sort : $locations_module['settings']['default_product_sort'];
if ($sort == ("date_added")) { $selected = "locations.date_added";}
$order_by = ($_GET['sort_direction'] == 1) ? "desc" : "asc";

$smarty->assign("data",$posted_data);//assigned template variable data
$search_tables = "locations
  INNER JOIN locations_page_categories ON (locations.id = locations_page_categories.locationsid)
  LEFT OUTER JOIN locations_extra_field_values ON (locations.id = locations_extra_field_values.locationsid)
  INNER JOIN locations_lng ON (locations.id = locations_lng.locationsid)";
$fields = "locations.id";
if ($posted_data['sku'] != "") 
{  
 $search_condition .= "locations.sku = '".$posted_data['sku']."' AND ";
}//END OF IF
if ($posted_data['images_only'] != "") 
{  
 $search_condition .=  "locations.image != '' AND ";
}//END OF IF
if ($posted_data['categoryid'] != "%") 
{
if (!empty($posted_data["search_in_subcategories"])) 
{
	
# Search also in all subcategories
	$sql->db_Select("locations_categories","categoryid_path","categoryid = '".$posted_data["categoryid"]."'");
	$r = $sql->db_Fetch();
	$categoryid_path = $r['categoryid_path'];
	$sql->db_Select("locations_categories","categoryid","categoryid = '".$posted_data["categoryid"]."' OR categoryid_path LIKE '$categoryid_path/%'");
	$categoryids_tmp = execute_multi($sql,0);	
	
	if (is_array($categoryids_tmp) && !empty($categoryids_tmp)) 
	{
		foreach ($categoryids_tmp as $k=>$v) 
		{
		$categoryids[] = $v["categoryid"];
		}//END OF FOREACH

		$search_condition .= "locations_page_categories.catid $category_sign IN (".implode(",", $categoryids).") AND ";
	}//END OF IF
}//END OF IF
else //Search in top category only
  {  
 $search_condition .= "locations_page_categories.catid = ".$posted_data['categoryid']." AND ";
  }//END OF ELSE  
}//END OF IF

if (!empty($posted_data["substring"])) 
{
$posted_data['substring'] = trim($posted_data['substring']);

if ($posted_data['search_main'] != "") 
{  
 $condition[] = "locations_lng.title LIKE '%".$posted_data["substring"]."%'";
}//END OF IF
if ($posted_data['search_short'] != "") 
{  
 $condition[] = "locations_lng.description LIKE '%".$posted_data["substring"]."%'";
}//END OF IF
if ($posted_data['search_long'] != "") 
{  
 $condition[] = "locations_lng.full_description LIKE '%".$posted_data["substring"]."%'";
}//END OF IF
 foreach ($posted_data as $key => $value) 
 {   
	if (strstr($key,"-")) 
	{
		$condition[] = "locations_extra_field_values.value LIKE '%".$posted_data['substring']."%'";
	}//END OF IF
 }//END OF FOREACH

if (!empty($condition))
{ 
	$search_condition .= "(".implode(" OR ", $condition).")"." AND ";
}//END OF IF
}//END OF PATERN SEARCH
//Extra fields Radio buttons search

 foreach ($posted_data as $key => $value) 
 {   
	if (strstr($key,"$"))
	{
		$key = str_replace("$","-",$key);
		list($field,$code)=split("-",$key);
		$condition_radio[] = "(locations_extra_field_values.value = '$value' AND locations_extra_field_values.fieldid = '$code')";
	}
 }//END OF FOREACH
if (!empty($condition_radio))
{ 
	$search_condition .= "(".implode(" OR ", $condition_radio).")"." AND ";
}//END OF IF
$search_condition .= "(locations_lng.code = '".DEFAULT_LANG."') AND (main = 'Y') 
GROUP BY locations.id
ORDER BY $selected $order_by";

//EXECUTE THE QUERY AND GATHER THE ID'S
$sql->db_Select($search_tables,$fields,"$search_condition");
$tranlate = array();
$search_results = execute_paginated($sql,1,$locations_module['settings']['search_items_per_page_admin'],0);

if (count($search_results) > 0) 
{  
for ($i=0;count($search_results) > $i;$i++)
{
	$locations[$i] = get_locations($search_results[$i]['id'],DEFAULT_LANG);
}//END OF FOR
}//END OF IF
//Display the query
if ($posted_data['query'] != "") 
{  
 $query = "SELECT $fields FROM $search_tables WHERE $search_condition";
 $smarty->assign("query",$query);//assigned template variable query
}//END OF IF
 $query = "SELECT $fields FROM $search_tables WHERE $search_condition";
//echo $query;

if ($selected == ("locations.date_added")) { $selected = "date_added";}
$smarty->assign("selected",$selected);//assigned template variable selected
$smarty->assign("direction",$direction);//assigned template variable direction
$smarty->assign("results",$locations);//assigned template variable results
$smarty->assign("search_done",1);//assigned template variable num_res
}//END OF IF POSTED DATA
	
	
	
	$smarty->assign("extra_fields",get_locations_extra_fields("Y",DEFAULT_LANG));//assigned template variable extra_field
}

//print_r(get_locations(65,DEFAULT_LANG));
$smarty->assign("MODULE_SETTINGS",$locations_module['settings']);
$smarty->assign("USE_AJAX","modules/locations/admin/locations_ajax.tpl");
$smarty->assign("menu",$locations_module['name']);
$smarty->assign("submenu","search");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/locations/search_form.tpl");
$smarty->display("admin/home.tpl");

?>