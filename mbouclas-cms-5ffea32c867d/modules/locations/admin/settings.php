<?php
include("../../../manage/init.php");//load from manage!!!!
$cat = ($_GET['cat']) ? $_GET['cat'] : "general";
if ($locations_module = module_is_active("locations",1,1,0,$cat)) 
{
	if ($_POST)
	{
		$t = new textparse();
		foreach ($_POST as $key => $val)
		{
			$val = $t->formtpa($val);
			$sql->db_Update("modules_settings","value = '$val' WHERE name = '$key' AND module_id = ".$locations_module['id']);
//			echo "UPDATE modules_settings SET value = '$val' WHERE name = '$key' AND module_id = ".$locations_module['id']."<br>";
		}
		$q = (e_QUERY) ? "?".e_QUERY : "";
		header("Location: ".URL."/".$locations_module['folder']."/admin/settings.php$q");
		exit();
	}
	
	// GET ALL SETTINGS GROUPS
	$sql->db_Select("modules_settings","category","module_id = ".$locations_module['id']." GROUP BY category");
	if ($sql->db_Rows()) 
	{
		$smarty->assign("all_modules",execute_multi($sql));
	}
	$smarty->assign("MODULE_CAT",$cat);
	$smarty->assign("latest_locations",get_latest_locations($locations_module['settings']['latest_locations_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",URL."/".$locations_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$locations_module['settings']);
	$smarty->assign("MODULE_PROPERTIES",$locations_module);
	$smarty->assign("module",$locations_module['settings_full']);
}

$editor_settings['image_manager'] = 'OpenFileBrowser';
editor('desc','desc',0,0,0,$editor_settings);

$smarty->assign("menu",$locations_module['name']);
$smarty->assign("submenu","settings");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/locations/admin/settings.tpl");
$smarty->display("admin/home.tpl");

?>