<?php
class maps extends Items  {
var $settings;
var $itemid;


	
	function getMapItem($itemid,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$itemid = ($itemid) ? $itemid :0;
		$sql->db_Select("maps_items",$fields,"itemid = $itemid AND module = '".$this->module['name']."'");
		if ($settings['debug']) {
			echo "SELECT $fields FROM maps_items WHERE itemid = $itemid AND module = '".$this->module['name']."'";
		}
		if ($sql->db_Rows()) {
			$res = execute_single($sql);
			if ($settings['getItem']) {
				$res['item'] = $this->GetItem($itemid,array('debug'=>0));
			}
		}//END ROWS
		return $res;
	}//END FUNCTION
	
	function getItems($ids,$settings=0)
	{
		global $sql;
		$tables = 'maps_items';
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		if (is_array($ids)) {
			$items = implode(',',$ids);
		}
		else {
			$items = $ids;
		}
		
		if ($settings['distanceFromPoint']) {
			$fields .= ",(GLength(
			LineStringFromWKB(
			  LineString(
			    location,
			    POINT(".$settings['lat'].", ". $settings['lng'].")
			  )
			 )
			))*60*1.1515*1.609344
			AS distance";
		}

		if ($items) {
			$itemsQ = "itemid IN ($items) AND";
		}
		if ($settings['limit']) {
			$limitQ = " LIMIT ".$settings['limit'];
		}
		if ($settings['active']) {
			$tables .= " INNER JOIN ".$this->module['name']." ON ($tables.itemid=".$this->module['name'].".id)";
			$q = " AND active = ".$settings['active'];
		}
		$sql->db_Select($tables,$fields,"$itemsQ module = '".$this->module['name']."' $q $limitQ");
//		echo "SELECT $fields FROM $tables WHERE $itemsQ module = '".$this->module['name']."' $q $limitQ";
		if ($settings['debug']) {
			echo "SELECT $fields FROM $tables WHERE itemid IN ($items) AND module = '".$this->module['name']."'  $limitQ";
		}
		if ($sql->db_Rows()) {
			$res = execute_multi($sql);
			
			return $res;
		}
	}//END FUNCTION
	
	function formatItems($mapItems,$settings=0) {
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "title,id,permalink";
		$res['items'] = $mapItems;
		foreach ($res['items'] as $v) {
			$implodedItemIds[] = $v['itemid'];
		}
		$sql->db_Select($settings['module']['name'],$fields,"id IN (".implode(',',$implodedItemIds).")");
		
		$a = execute_multi($sql);
	
		foreach ($a as $v) {
			$details[$v['id']] = $v;
		}
		foreach ($mapItems as $k=>$v)
		{
			
			$lat[] = $v['lat'];
			$lng[] = $v['lng'];
			$zoom[] = $v['zoomLevel'];
			
		}
		foreach ($res['items'] as $k=>$v) {
			$res['items'][$k]['distance'] = round($res['items'][$k]['distance'],2);
			$res['items'][$k]['item'] = $details[$v['itemid']];
		}
		$mapDetails['lat'] = min($lat);
		$mapDetails['lng'] = max($lng);
		$mapDetails['zoomLevel'] = min($zoom);
		$res['map'] = $mapDetails;
		return $res;
	}
	
}//END CLASS
?>