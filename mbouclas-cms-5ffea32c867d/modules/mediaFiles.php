<?php
class mediaFiles extends Items {
	var $tables;
	var $appPath;
	var $uploadDetails;
	
	public function __construct($settings) {
		parent::items($settings);	
		
		$this->tables['images'] = 'item_images';
	  	$this->tables['images_aditional'] = 'item_images_aditional';
	  	$this->appPath = str_replace('\\','/',str_replace('modules','',dirname(__FILE__)));

	}//END CONSTRUCTOR

	public function CheckDocument($file,$upload_dir,$filename) {
        $return['mime'] = mime_content_type($file);
        $return['filesizePlain'] = filesize($file);
        $return['filesize'] = FileSizeFormat($return['filesizePlain']);
        $return['name'] = $filename;
        $return['CleanDir'] = str_replace($_SERVER['DOCUMENT_ROOT'],'',$upload_dir);
        return $return;
   }//END FUNCTION

    public function SaveDocument($itemid,$file,$f) {
    global $sql;
    $type = explode('/',$f['mime']);
    $q = array("''",$itemid,"'".$this->module['name']."'","'".$file."'","'".$f['CleanDir']."'","'".$f['CleanDir']."/".$file."'","'".$file."'","''",0,0,"'".$type[1]."'","'".$f['mime']."'",$f['filesizePlain'],time(),"''");
    $query = implode(',',$q);
    $sql->db_Insert('item_documents',$query);
    $f['id']=$sql->last_insert_id;
    return $f;
   }//END FUNCTION
   
    public function SaveVideo($itemid,$file,$f,$posted_data) {
    global $sql;
	$f['type'] = $type = explode('/',$f['mime']);
	    
    $extension = "ffmpeg";
	$extension_soname = $extension . "." . PHP_SHLIB_SUFFIX;
	$extension_fullname = PHP_EXTENSION_DIR . "/" . $extension_soname;	   		
		
    if (extension_loaded($extension)) 
	{
		
	$mov = new ffmpeg_movie($_SERVER['DOCUMENT_ROOT'].$f['CleanDir']."/".$file);
	$info['width'] = $mov->getFrameWidth();
	$info['height'] = $mov->getFrameHeight();
	$info['duration'] = $mov->getDuration();
	$info['framerate'] = $mov->getFrameRate();
	$info['filename'] = $mov->getFileName();
	$info['video_codec'] = $mov->getVideoCodec();
	$info['audio_codec'] = $mov->getAudioCodec();
	$info['audio_chanels'] = $mov->getAudioChannels();
	$info['bit_rate'] = $mov->getBitRate();
	
	//Screenshot
	$cap = $mov->getFrame(rand(1,$mov->getFrameCount()));
	$img = $cap->toGDImage(); 
	$thumb = $f['CleanDir']."/".str_replace($type[1],'',$file).".jpg";
	imagejpeg($img, $_SERVER['DOCUMENT_ROOT'].$thumb);
//		error_message($thumb);
	}
		if ($posted_data['duration']) {//FROM EMBED
			$info['duration'] = $posted_data['duration'];
		}
		if ($posted_data['image']) {//FROM EMBED
			$this_media = MediaFiles(array('type'=>$this->settings['media'],'single'=>1));
			$thumb = $this_media['folder']."/".$this->module['name']."_".$this->settings['itemid']."/".time().".jpg";
			
			if (!is_dir($this_media['folder']."/".$this->module['name']."_".$this->settings['itemid'])) {
				mkdir($_SERVER['DOCUMENT_ROOT'].$this_media['folder']."/".$this->module['name']."_".$this->settings['itemid'],0755);
			}
			$this->SaveRemoteThumb($thumb,$posted_data['image']);
		}
		$details = json_encode($info);
		$f['title'] = $title = ($posted_data['title']) ? $posted_data['title'] : $file;
		$f['embed'] = $embed = ($posted_data['embed']) ? $posted_data['embed'] : '';
		$file_url = (!$posted_data['embed']) ? $f['CleanDir']."/".$file : '';
		$f['date_added'] = $date_added = time();
	    $q = array("''",$itemid,"'".$this->module['name']."'","'".$file."'","'".$f['CleanDir']."'","'".$f['CleanDir']."/".$file."'","'$embed'","'$thumb'","'$title'","'$details'",0,0,"'".$type[1]."'","'".$f['mime']."'","'".$f['filesizePlain']."'",$date_added,"''");
	    $query = implode(',',$q);
//	    echo $query;
	    //error_message($query);
	    $sql->db_Insert('item_videos',$query);
	    $f['id']=$sql->last_insert_id;
    return $f;
   }//END FUNCTION
   
	public function handleImageUpload($itemid,$file,$path,$destinationPath,$settings=0){
		global $sql;
		//DELETE OLD ONE
		if (isset($settings['imageCategory']) AND $settings['imageCategory'] == 0) {
			$this->RemoveFromDb($itemid,0,$settings['type']);//JUST THE THUMB
		}
   		$a = $this->CreateImageSet($itemid,$file,$path,$destinationPath,$settings);

   		if ($settings['keepOriginal']) {
   			$this->CreateDirs($destinationPath."/originals");
   			copy($path."/".$file,$path."/originals/".$file);
   		}
      	$ret = $this->InsertImagesToDb($itemid,array('file'=>$file,'path'=>$destinationPath,'type'=>$settings['imageCategory']),$a,$settings['imageCategory']);//ADD IT TO DB
		foreach ($ret as $k=>$v) {
			$ret[$k] = array_merge($a[$k],$v);
		}
		$ret = array_merge($ret,$this->CheckDocument($path."/".$file,$path,$file));
		return array_merge($a,$ret);
	}//END METHOD
	
   public function CreateImageSet($itemid,$file,$path,$destinationPath,$settings=0)
   {
		global $sql;
   	    if ($settings['CustomImageSet'] AND is_array($settings['ImageSet'])) {//CUSTOM SET. GET THE IMAGE TYPES DATA
   			$ImageTypes = get_image_types($this->module['id'],array('ids'=>implode(",",$settings['ImageSet']),'debug'=>0));	
   		}
   		elseif ($settings['CustomImageSet'] AND !is_array($settings['ImageSet'])) {//FAILSAFE
   			$ImageTypes = get_image_types($this->module['id'],array('required'=>1));
   		}
   		elseif (!$settings['CustomImageSet'] AND !is_array($settings['ImageSet'])) {//DEFAULT ACTION, CREATE ALL COPIES
   			$ImageTypes = get_image_types($this->module['id']);	
   		}
   		elseif (!$settings['CustomImageSet'] AND is_array($settings['ImageSet'])) {//PROBABLY CAME FROM A READY MADE ARRAY
   			$ImageTypes = $settings['ImageSet'];	
   		}
		
   		foreach ($ImageTypes as $v)
   		{
   			$this->CreateDirs($destinationPath."/".$v['dir']);//CREATE DIRECTORY IF NOT THERE
   			if ($v['settings']['resize']) {
   				$dest = ($v['dir']) ? $destinationPath."/".$v['dir'] : $destinationPath;
   				$Image[$v['title']] = $this->CreateImge($itemid,$v,$file,$path,$dest);//Create Resampled image
   				
   			}//END IF REIZE
   		}

		return $Image;
   }//END METHOD
   
   public function InsertImagesToDb($id,$file,$copies,$type)
   {
		global $sql;

		HookParent::getInstance()->doTriggerHook($this->module['name'], "beforeInsertImage");
   		$sql->db_Insert($this->tables['images'],"'','".$this->module['name']."','$id','".$file['file']."','$alt','$title','$details','$available','$i','$type'");
//   		echo "INSERT INTO ".$this->tables['images']."'','$id','$original_file','$alt','$title','$details','$available','$i','$type'";

		$n_id = $sql->last_insert_id;
		foreach ($copies as $k => $v)
		{
			//Insert to aditional tables
			$image = $v['name'];
			$image_path = $v['CleanDir'];
			
//				fwrite($f,"BEFORE : ".$image_path."\n\r");
			$image_details = $v['size'];	
			$image_x = $image_details[0];
			$image_y = $image_details[1];
			$date_added = time();
			$image_path = str_replace("//","/",$image_path);
			$image_url = $v['CleanDir']."/".$v['name'];//WINDOWS ONLY
			$image_path = $v['CleanDir'];//WINDOWS ONLY
				
//				fwrite($f,"AFTER : ".$image_path."\n\r");
			$sql->db_Insert($this->tables['images_aditional'],"'$n_id',$id,'$image','$image_path','$image_url','$date_added','$image_x','$image_y','$k'");
//			echo "'$n_id','$image','$image_path','$image_x','$image_y','$k'<BR>";
			
			$ret[$k]['imageid'] = $n_id;
		}
		
		if ($this->module['settings']['keep_original_image']) {
			$image_path = str_replace($this->appPath,'',$file['path'])."/originals";
			$image_url = "/".str_replace($this->appPath,'',$file['path'])."/originals/".$file['file'];
			$image_path = "/".$image_path;
			$image_size = getimagesize($this->appPath.$image_path."/".$file['file']);
			$image_x = $image_size[0];
			$image_y = $image_size[1];
			$sql->db_Insert($this->tables['images_aditional'],"'$n_id',$id,'".$file['file']."','$image_path','$image_url','$date_added','$image_x','$image_y','originals'");
		}
		
		HookParent::getInstance()->doTriggerHook($this->module['name'], "AfterInsertImage",$n_id);
		return $ret;
   }//END METHOD
	

   public function CropImage($src,$dest)
   {
   
   	if (!$settings['quality']) {
	$jpeg_quality = 100;
	}
   		$ImageType = get_image_types($this->module['id'],array('title'=>$this->settings['ImageType'],'debug'=>0));
   		$ImageType = $ImageType[$this->settings['ImageType']];

   		$targ_w = $ImageType['width'];
   		$targ_h = $ImageType['height'];
		$img_r = imagecreatefromjpeg($this->appPath.$src['image_url']);
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
		  imagecopyresampled($dst_r,$img_r,0,0,$this->settings['coords']['x'],$this->settings['coords']['y'],
		    $targ_w,$targ_h,$this->settings['coords']['w'],$this->settings['coords']['h']);
		
		if (imagejpeg($dst_r, $this->appPath.$dest['image_url'], $jpeg_quality))
		{return true;}
		
		
   }//END public function
   
   public function DeleteImages($imageIDs)
   {
   	
   	foreach ($imageIDs as $v)
   	{
   		$this->RemoveFromDb(array('itemid'=>$v));
   	}
   }//END public function
   
   
 	public function RemoveFromDb($itemid,$imageid=0,$imageType=null)
 	{
 		global $sql;
 		if (!$imageid) {//GO BY IMAGE ID
			if (isset($imageType)) {
				$query = " AND type = $imageType";
			}

 		$sql->db_Select($this->tables['images'],"id","itemid = $itemid AND module = '".$this->module['name']."' $query");
		 if ($sql->db_Rows()) {
		 	$a = execute_single($sql);
		 	$id = $a['id'];
		 	$sql->db_Select($this->tables['images_aditional'],'image_url',"imageid = $id AND itemid = $itemid");
		 	$r = execute_multi($sql);
		 	foreach ($r as $v)
		 	{
		 		unlink($this->appPath.$v['image_url']);
		 	}
		 }//END ROWS
		 
		 $sql->db_Delete($this->tables['images'],"id = ".$id);
		 $sql->db_Delete($this->tables['images_aditional'],"imageid = ".$id." AND itemid = $itemid");
 		}//END DELETE BY ID
		else//DELETE A COPY OF THE IMAGE
		{
			$sql->db_Select($this->tables['images_aditional'],"image_url","imageid = $imageid AND itemid = $itemid AND image_type = '$imageType'");
			if ($sql->db_Rows()) {
				$image = execute_single($sql);
				$sql->db_Delete($this->tables['images_aditional'],"imageid = $imageid AND itemid = $itemid AND image_type = '$imageType'");
				@unlink($this->appPath.$image['image_url']);
			}//END ROWS
			
		}//END DELETE COPY OF IMAGE
 	}//END public function
 	
 	public function CreateImge($itemid,$settings,$file,$path,$destinationPath)
	{
		
		$srcFile = $path."/".$file;
		$imagesize = getimagesize($srcFile);

		$image['name'] = $settings['prefix'].$file;
		$image['dir'] = $settings['dir'];
		$image['CleanDir'] = "/".str_replace($this->appPath,"",$destinationPath);
		list($srcW, $srcH, $srcType, $html_attr) = $imagesize;
		$ext = substr($file, -3);
		$ext = strtolower($ext);
		if($ext == 'jpg') {
			$srcImage = @imagecreatefromjpeg($srcFile);
		} elseif($ext == 'gif') {
			$srcImage = @imagecreatefromgif($srcFile);
		} elseif($ext == 'png') {
			$srcImage = @imagecreatefrompng($srcFile);
		}
			$fix = ($this->settings['ImageFix']) ? $this->settings['ImageFix'] : $settings['settings']['fix'];
		if ($fix == 'ImageBox') {
			$thumb = $this->thumbnail_box($srcImage, $settings['width'], $settings['height']);
		}//END IMAGEBOX
		elseif ($fix == 'ByWidth') {
			$thumb = $this->resizeByWidth($srcImage,$settings['width'],$settings['height'],$imagesize);
		}//END RESIZE BY WIDTH
		elseif ($fix == 'ByHeight') {
			
		}//END RESIZE BY HEIGHT
		elseif ($fix == 'ByWidthAndHeight') {
			
		}//END RESIZE BY BOTH WIDTH AND HEIGHT
		
		if ($thumb) {//If it is to be resized
		imagedestroy($srcImage);
		
		if(is_null($thumb)) {
		    /* image creation or copying failed */
		    header('HTTP/1.1 500 Internal Server Error');
		    exit();
		}
		if (!$settings['quality']) {
			$settings['quality'] = 100;
		}
		$newFile = $destinationPath."/".$settings['prefix'].$file;
		imagejpeg($thumb, $newFile, $settings['quality']);
		$image['size'] = getimagesize($newFile);
		return $image;
		}
		else {//Check out original image
			return false;
			
			
		}
	}//END public function	
 	

	public function resizeByWidth($srcImage,$width,$heightm,$imagesize)
	{
			list($srcW, $srcH, $srcType, $html_attr) = $imagesize;
	
			if(!$srcImage) return false;
			//$srcW = imagesx($srcImage);
			//$srcH = imagesy($srcImage);
			$factor = $width / $srcW;
	
			$newH = (int) round($srcH * $factor);
			$newW = (int) round($srcW * $factor);
	
			$newImage = imagecreatetruecolor($newW, $newH);
			
			imagecopyresampled($newImage,$srcImage,0,0,0,0,$newW,$newH,$srcW,$srcH);
			return $newImage;
	}//END public function
	
	public function thumbnail_box($src_img, $w, $h) {
		$lowend = 0.8;
	    $highend = 1.25;
	    //create the image, of the required size
	    $new = imagecreatetruecolor($w, $h);
	    if($new === false) {
	        //creation failed -- probably not enough memory
	        return null;
	    }
	
	
	    //Fill the image with a light grey color
	    //(this will be visible in the padding around the image,
	    //if the aspect ratios of the image and the thumbnail do not match)
	    //Replace this with any color you want, or comment it out for black.
	    //I used grey for testing =)
	    $fill = imagecolorallocate($new, 255, 255, 255);
	    imagefill($new, 0, 0, $fill);
	
				$src_w = imageSX($src_img);
	            $src_h = imageSY($src_img);
	
	            $scaleX = (float)$w / $src_w;
	            $scaleY = (float)$h / $src_h;
	            $scale = min($scaleX, $scaleY);
	
	            $dstW = $w;
	            $dstH = $h;
	            $dstX = $dstY = 0;
	
	            $scaleR = $scaleX / $scaleY;
	            if($scaleR < $lowend || $scaleR > $highend)
	            {
	                $dstW = (int)($scale * $src_w + 0.5);
	                $dstH = (int)($scale * $src_h + 0.5);
	
	                // Keep pic centered in frame.
	                $dstX = (int)(0.5 * ($w - $dstW));
	                $dstY = (int)(0.5 * ($h - $dstH));
	            }
	            
	            imagecopyresampled(
	                $new, $src_img, $dstX, $dstY, 0, 0, 
	                $dstW, $dstH, $src_w, $src_h); 
	    //copy successful
	    return $new;
	}
   public function CleanUp()
   {
   		unlink($this->settings['upload_dir'].$this->settings['filename']);
   }//END public function

   public function CreateDirs($dir)
   {
		if ($dir AND !is_dir($dir)) {
				mkdir($dir,0755);
		}
   }//END public function

   public function SaveRemoteFile($fullpath,$remote_file)
   {
   		$ch = curl_init();
		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $remote_file);
		    curl_setopt($ch, CURLOPT_HEADER, 0);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		
		// grab URL and pass it to the browser
		$rawdata=curl_exec($ch);
		// close cURL resource, and free up system resources
		curl_close($ch);
		if(file_exists($this->appPath.$fullpath)){
		        unlink($this->appPath.$fullpath);
	    }
	    $fp = fopen($this->appPath.$fullpath,'x');
	    fwrite($fp, $rawdata);
	    fclose($fp);
   }//END public function   

	
}//END CLASS