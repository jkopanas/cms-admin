<?php
class menuEditor {
var $table;
var $tableItems;
	function menuEditor($settings=0)
	{
		$this->table = ($settings['table']) ? $settings['table']  : "menus";
		$this->tableItems = ($settings['tableItems']) ? $settings['tableItems']  : "menus_items";
	}//END CONSTRUCTOR

	function getMenu($id=0,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : $this->table.".*";
		$orderby = ($settings['orderby']) ? $settings['orderby'] : "date_added";
		$way = ($settings['way']) ? $settings['way'] : "DESC";
		if ($settings['num_items']) {
			$fields .= ",(SELECT count(id) FROM ".$this->tableItems." where menuid = menus.id)  as num_items";
		}
		
		if ($settings['active']) {
			$q[] = "active = ".$settings['active'];
		}
		if ($q) {
			$query = implode(" AND ",$q);
		}
		$mode = ($query) ? "default" : "no_where";
		
		if (!$id) {//ALL MENUS
			$sql->db_Select($this->table,$fields,$query,$mode);
			if ($settings['debug']) {
				echo "SELECT $fields FROM ".$this->table." WHERE $query";
			}
			$res = execute_multi($sql);
			foreach ($res as $k=>$v) {
				$res[$k]['settings'] = json_decode(base64_decode($res[$k]['settings']),true);				
			}
		}//END ALL MENUS
		elseif (is_array($id)) {//SELECTED MENUS
			$ids = implode(",",$id);
			$sql->db_Select($this->table,$fields,"id IN ($ids) $query ORDER BY $orderby $way");
			$res = execute_multi($sql);
			foreach ($res as $k=>$v) {
				$res[$k]['settings'] = json_decode(base64_decode($res[$k]['settings']),true);				
			}
		}//END SELECTED MENUS
		else
		{
			$sql->db_Select($this->table,$fields,"id = $id");
			if ($settings['debug']) {
				echo "SELECT $fields FROM ".$this->table." WHERE id = $id";
			}
			$res = execute_single($sql);
			if ($settings['returnItems']) {
				$res['items'] = $this->recurseMenuItems($res['id'],0,0,array('debug'=>0,'num_subs'=>1));
//				$this->menuItemsTree($res['id'],0,array('debug'=>1,'num_subs'=>1));
			}
			$res['settings'] = json_decode(base64_decode($res['settings']),true);
		}//END SINGLE MENU
		
 		if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
			global $Languages;
			if ($res['items']) {
				
			
			foreach ($res['items'] as $a) {
			$translations = $Languages->itemTranslations($a['id'],FRONT_LANG,'',array('debug'=>0,'groupby'=>'field','table'=>'menu_items'));
			if ($translations) {
			foreach ($translations as $k=>$v)
			{
				if ($v['translation']) {
					$a[$k] = $v['translation'];
				}
			}
			}
			}//END LOOP
			}//ITEMS
		}//END TRANSLATE CATEGORIES
		return $res;
	}//END FUNCTION

	function addMenu($data)
	{
		global $sql;
		foreach ($data as $k=>$v) {
			if (strstr($k,'settings_')) {
				$settings[str_replace('settings_','',$k)] = $v;
			}
		}
		if ($settings) {
			$settings = base64_encode(json_encode($settings));
		}
		$t = new Parse();
		$data['title'] = $t->toDB($data['title']);
		$data['description'] = $t->toDB($data['description']);
		$sql->db_Insert($this->table,"'','".$data['title']."','".$data['description']."','".$data['var']."','".time()."','$settings','".$data['active']."'");
		return $sql->last_insert_id;
	}//END FUNCTION

	function modifyMenu($id,$data){
		global $sql;
		foreach ($data as $k=>$v) {
			if (strstr($k,'settings_')) {
				$settings[str_replace('settings_','',$k)] = $v;
			}
		}
		if ($settings) {
			$settings = base64_encode(json_encode($settings));
		}
		
		$t = new Parse();
		$data['title'] = $t->toDB($data['title']);
		$data['description'] = $t->toDB($data['description']);
		$sql->db_Update($this->table,"title = '".$data['title']."',description = '".$data['description']."',settings='$settings',active ='".$data['active']."' WHERE id = $id");
	}
	
	function deleteMenu($id) {
		global $sql;
		$sql->db_Select($this->tableItems,'id',"menuid = $id");
		if ($sql->db_Rows()) {
			$r = execute_multi($sql);
			foreach ($r as $v) {
				$toDelete[] = $v['id'];			
			}
			$sql->db_Delete($this->tableItems,"id IN (".implode(',',$toDelete));
		}
		$sql->db_Delete($this->table,"id = $id");
	}
	
	function getMenuItem($id,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : $this->tableItems.".*";
		$sql->db_Select($this->tableItems,$fields,"id = $id");
		if ($sql->db_Rows()) {
			$res = execute_single($sql);
			$res['settings'] = json_decode($res['settings'],true);
		}
		return $res;
	}//END FUNCTION
	
	function getMenuItems($menuid,$settings=0)
	{
		
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : $this->tableItems.".*";
		$orderby = ($settings['orderby']) ? $settings['orderby'] : "order_by";
		$way = ($settings['way']) ? $settings['way'] : "ASC";
		$q[] = "menuid = $menuid";
		if ($settings['active']) {
			$q[] = "active = ".$settings['active'];
		}
		
		
		if ($q) {
			$query = implode(" AND ",$q);
		}
		
		$mode = ($query) ? "default" : "no_where";
		if (!$id) {//ALL ITEMS
			$sql->db_Select($this->tableItems,$fields,$query,$mode);
			if ($settings['debug']) {
				echo "SELECT $fields FROM ".$this->tableItems." WHERE $query";
			}
			$res = execute_multi($sql);
			foreach ($res as $k=>$v) {
				$res[$k] = json_decode(base64_decode($res[$k]['settings']),true);
			}
		}//END ALL ITEMS
		elseif (is_array($id)) {//SELECTED MENUS
			$ids = implode(",",$id);
			$sql->db_Select($this->tableItems,$fields,"id IN ($ids) $query ORDER BY $orderby $way");
			$res = execute_multi($sql);
			foreach ($res as $k=>$v) {
				$res[$k] = json_decode(base64_decode($res[$k]['settings']),true);
			}
		}//END SELECTED MENUS
		else
		{
			$sql->db_Select($this->tableItems,$fields,"id = $id");
			if ($settings['debug']) {
				echo "SELECT $fields FROM ".$this->tableItems." WHERE id = $id";
			}
			$res = execute_single($sql);
			$res['settings'] = json_decode(base64_decode($res['settings']),true);
		}//END SINGLE MENU
		
		return $res;
		
	}//END FUNCTION
	
	function recurseArray($arr,$order)
	{
		global $sql;
		
		foreach ($arr as $v)
		{
			if ($v['item_id'] !== 'root') {
				
			if ($v['parent_id'] == 'root') {
				$tmp[$v['item_id']]['path'] = $v['item_id'];
				$tmp[$v['item_id']]['parentid'] = $v['item_id'];
				$tmp[$v['item_id']]['orderby'] = $order[$v['item_id']];
//				echo $v['item_id'];
			}
			else {
				$tmp[$v['item_id']]['path'] = $this->buildPath($v['item_id']);
				$tmp[$v['item_id']]['parentid'] = $v['parent_id'];
				$tmp[$v['item_id']]['orderby'] = $order[$v['item_id']];
			}
			}//END NOT ROOT
		}
		
//		print_r($tmp);
		foreach ($tmp as $k=>$v) {
			if (is_array($v['path'])) {
//				echo $v['orderby'].". ".$k." -- ".implode("/",array_reverse($v['path']))." -- ".$v['parentid']."<br>";
				$sql->db_Update($this->tableItems,"menuid_path = '".implode("/",array_reverse($v['path']))."', parentid = '".$v['parentid']."', order_by = '".$v['orderby']."' WHERE id = $k");
//				echo "UPDATE ".$this->tableItems." SET menuid_path = '".implode("/",array_reverse($v['path']))."', parentid = '".$v['parentid']."', order_by = '".$v['orderby']."' WHERE id = $k\n";
			}
			else {
				if ($v['parentid'] == $k) {
					$v['parentid'] = 0;//ROOT ITEM
				}
//				echo "UPDATE ".$this->tableItems." SET menuid_path = '".$v['path']."', parentid = '".$v['parentid']."', orderby = '".$v['orderby']."' WHERE id = $k"."<br>";
				$sql->db_Update($this->tableItems,"menuid_path = '".$v['path']."', parentid = '".$v['parentid']."', order_by = '".$v['orderby']."' WHERE id = $k");
			}
			
		}
	}//END FUNCTION
	
	
	function buildPath($itemid,$arr=array())
	{
		global $posted_data;
		foreach ($posted_data as $v) {
			if ($v['item_id'] == $itemid AND $v['item_id'] != 'root') {
				array_push($arr,$v['item_id']);
				if ($v['parent_id'] !== 'none') {
					return $this->buildPath($v['parent_id'],$arr);
				}
	
			}
		}
		return $arr;
	}//END FUNCTION

	
function menuItemsTree($menuid,$cat=0,$settings=0)
	{
		global $sql,$loaded_modules;
		$fields = (!$settings['fields']) ? "*" : $settings['fields'];
		$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
		$active = $settings['active'];
		$table = $this->tableItemsItems;
		$orderby = ($settings['orderby']) ? $settings['orderby'] : "order_by";
		$way = ($settings['way']) ? $settings['way'] : "ASC";
		$searchField = ($settings['searchField']) ? $settings['searchField'] : "parentid";
	    if ($settings['num_items'] OR $settings['num_subs'])
	    {
	        $fields .= ",@id := id";
	    }

	       if ($settings['num_subs']) {
	      $fields .= ",
	(
	SELECT count(id) FROM ".$this->tableItems." WHERE 
	(".$this->tableItems.".menuid_path LIKE CONCAT('%',@id,'/%') )
	) as num_sub ";
	      }//END IF
//		echo "SELECT $fields FROM ".$this->tableItems." WHERE parentid = $cat $settings_q ORDER BY $orderby $way $limit_q<br>";
		$sql->db_Select($this->tableItems,$fields,"menuid = $menuid AND parentid = $cat $settings_q ORDER BY $orderby $way $limit_q");
		if ($settings['debug']) {
			echo "<br>SELECT $fields FROM ".$this->tableItems." WHERE $searchField = $cat $settings_q ORDER BY $orderby $way $limit_q<br>";
		}
		if ($sql->db_Rows()) {
			$items = execute_multi($sql,1);
	
		 for ($i=0;count($items) > $i;$i++)
		 {
		 	$items[$i]['settings'] = json_decode(base64_decode($items[$i]['settings']),true);
			 $id = $items[$i]['id'];
			if (defined('FRONT_LANG') AND (FRONT_LANG != DEFAULT_LANG)) {//TRANSLATE CONTENT
				global $Languages;
				$translations = $Languages->itemTranslations($id,FRONT_LANG,$this->module['name'],array('debug'=>0,'groupby'=>'field'));
				if ($translations) {
				foreach ($translations as $k=>$v)
				{
					if ($v['translation']) {
						$items[$i][$k] = $v['translation'];
					}
				}
				}
			}//END TRANSLATE CATEGORIES
				if ($items[$i]['settings']['syncWithOriginal']) {
					$itm = new Items(array('module'=>$loaded_modules[$items[$i]['module']]));
					if ($items[$i]['type'] == 'cat') {
						$a = $itm->ItemCategory($items[$i]['itemid'],array('debug'=>0,'fields'=>'category,alias,categoryid','table'=>$items[$i]['module']."_categories"));
						$items[$i]['title'] = $a['category'];
						$items[$i]['link'] = $this->buildLinks(array('itemid'=>$a['categoryid'],'module'=>$items[$i]['module'],'permalink'=>$a['alias'],'type'=>'cat'));
						$items[$i]['permalink'] = $a['alias'];
					}
					elseif ($items[$i]['type'] == 'item')
					{
						$a = $itm->GetItem($items[$i]['itemid'],array('fields'=>'title,permalink,id','debug'=>0));
						$items[$i]['title'] = $a['title'];
						$items[$i]['permalink'] = $a['permalink'];
						$items[$i]['link'] = $this->buildLinks(array('itemid'=>$a['id'],'module'=>$items[$i]['module'],'permalink'=>$a['permalink'],'type'=>'item'));
					}
				}
			 if ($settings['get_subs']) 
			 {
			 	$items[$i]['subcategories'] = $this->menuItemsTree($id,array($settings));
			 }
			 //get settings 
			 
			 
		 }//END OF FOR
		 return $items;
		}//END ROWS
	 
	}//END FUNCTION
	
	function recurseMenuItems($menuid,$ar=0,$start=0,$Catsettings=0)
	{
		global $sql;
		$ar = $this->menuItemsTree($menuid,$start,$Catsettings);
//		echo "<br> --------------------- "; print_r($ar); echo " ----------------------------------<br>";

		for ($i=0;count($ar) >$i;$i++)
		{
			if ($ar[$i]['num_sub']) {
				$ar[$i]['subs'] = $this->recurseMenuItems($menuid,$ar,$ar[$i]['id'],$Catsettings);
			}
		}
		return $ar;
	}//END FUNCTION
	
	function recurseMenuItemsSimple($menuid,$ar=0,$start=0,$Catsettings=0)
	{

		$a = $this->menuItemsTree($menuid,$start,$Catsettings);
		$ar = array();
		if ($a) {
		foreach ($a as $v)
		{
			if ($v) {
				$ar[] = $v;
				if ($ar[$i]['num_sub']) {
					$ar[] = $this->recurseMenuItemsSimple($menuid,$ar,$ar[$i]['id'],$Catsettings);
				}
			}
		}
		}
		return $a;
	}//END FUNCTION
	
	function recursebuild($ar) {
	
		$a=array();
		$b = array();
		foreach ($ar as $key => $value ) {
			$ars = array();
			if ($value['num_sub'] != 0 ) {
				$value['subs']=$this->recursebuild($value['subs']);
			} 

		$a[$key]['id'] = $value['id'];
		$a[$key]['parentid'] = $value['parentid'];
		$a[$key]['level'] = count(explode("/",$value['menuid_path']));
		//$a[$key]['text'] = "<span class='items' id='list_".$value['id']."' data-level='".$a[$key]['level']."' data-parent='".$value['parentid']."'>".$value['title']."</span>";
		$a[$key]['text'] = $value['title'];
		$a[$key]['items'] = $value['subs'];
		//$a[$key]['encoded'] = false;
		}
		
		return $a;
	}
	
	
	function buildLinks($data)
	{
		$patterns = json_decode(LINK_PATERNS,true);
		$prefixes = json_decode(PAGE_PREFIXES,true);
		$type = ($data['type']) ? $data['type'] : 'item';
		$replacements = array('MODULE'=>$data['module'],'PERMALINK'=>$data['permalink'],'ITEMID'=>$data['itemid'],'PREFIX'=>$prefixes[$data['module']]);

		
		preg_match_all('/#([A-Z]+)#/',$patterns[$type],$matches);
		foreach ($matches[1] as $v)
		{
			$find[] = "#$v#";
			$replace[] = $replacements[$v];
		}
		$link = str_replace($find,$replace,$patterns[$type]);

		if ($data['updateLinks']) {
			global $sql;
			$sql->db_Update("menus_items","link = '$link',permalink = '".$data['permalink']."' WHERE itemid = ".$data['itemid']." AND module = '".$data['module']."' AND type = '$type'");
//			echo "link = '$link',permalink = '".$data['permalink']."' WHERE itemid = ".$data['itemid']." AND module = '".$data['module']."' AND type = '$type'";
//			exit();
		}
		else {
			return $link;
		}
	}//END FUNCTION
	
	
}//END CLASS
?>