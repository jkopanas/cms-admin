<?php
include("../../../manage/init.php");//load from manage!!!!
if ($news_module = module_is_active("news",1,1,0)) 
{
	$module_path = URL."/".$news_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",URL."/".$news_module['folder']."/admin");
	$smarty->assign("allcategories",get_all_news_categories(DEFAULT_LANG));

$t = new textparse();
$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];



//root categories
if (!$cat) 
{  
 	$cat = 0;
}//END OF IF

$category = (empty($cat)) ? 0 : get_news_category($cat,DEFAULT_LANG,1);
$parentid = ($_GET['action']) ? $category['parentid'] : $cat;


##################### ADD NEW CATEGORY ######################3
if ($_POST['action'] == "addcat") 
{
	$new_category = $t->formtpa($_POST['title']);
	$new_parentid = $_POST['parent_catid'];
	$new_cat = get_news_category($new_parentid,DEFAULT_LANG);
	$new_meta_descr = $t->formtpa($_POST['meta_desc']);
	$new_meta_keyw = $t->formtpa($_POST['meta_keyw']);
	$new_description = $t->formtpa($_POST['desc']);
	$new_date_added = time();
	$new_orderby = $_POST['orderby'];
//Check out for settings
$tmp = array();
foreach ($_POST as $k => $v)
{
	if (strstr($k,"settings_")) 
	{
		list($dump,$field)=split("settings_",$k);
//		echo "$field --- $v<br>";
		$tmp[$field] = $v;
	}
	
	
}
if ($tmp) 
{
	$category_settings = form_settings_string($tmp,"###",":::");
}
	
	$sql->db_Insert("news_categories","'','$new_parentid','','$new_orderby','','$new_date_added','$category_settings'");
	$new_categoryid = mysql_insert_id();
	$new_categoryid_path = ($new_cat == 0) ? $new_categoryid : $new_cat['categoryid_path']."/".$new_categoryid;
	$sql->db_Update("news_categories","categoryid_path = '$new_categoryid_path' WHERE categoryid = '$new_categoryid'");
	
	$sql->db_Insert("news_categories_lng","'".DEFAULT_LANG."','$new_categoryid','$new_category','$new_description',
	'$new_meta_descr','$new_meta_keyw','$image'");
########### REPLICATE DATA FOR TRANSLATIONS ###############################
$countries = get_countries("Y",$trans="yes");
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert("news_categories_lng","'".$countries[$i]['code']."','$new_categoryid','$new_category','$new_description',
'$new_meta_descr','$new_meta_keyw','$image'");
}//END OF FOR
}//END OF IF
header("Location: $module_path/categories.php?cat=$cat");
exit();

}//END OF NEW CATEGORY

##################### MODIFY EXISTING CATEGORY ######################3
if ($_POST['action'] == "modify") 
{
$parentid = $_POST['parent_catid'];
if ($parentid != 0) 
{  
$parent_details = get_news_category($parentid,DEFAULT_LANG_ADMIN);
$new_catid_path  = $parent_details['categoryid_path']."/$cat";
}//END OF IF
else 
{
$new_catid_path = $cat;	
}
//Check out for settings
$tmp = array();
foreach ($_POST as $k => $v)
{
	if (strstr($k,"settings_")) 
	{
		list($dump,$field)=split("settings_",$k);
//		echo "$field --- $v<br>";
		$tmp[$field] = $v;
	}
	
	
}
if ($tmp) 
{
	$category_settings = form_settings_string($tmp,"###",":::");
}
//now modify the details of the given category
$sql->db_Update("news_categories","parentid = '$parentid', settings = '$category_settings', categoryid_path = '$new_catid_path', order_by = '".$_POST['orderby']."' WHERE categoryid = $cat");

//rearange subcategory paths
$children = get_news_categories($cat,DEFAULT_LANG);
for ($i=0;count($children) > $i;$i++)
{
$child_id = $children[$i]['categoryid'];
$new_cat_path = $new_catid_path."/$child_id";
$sql->db_Update("news_categories","categoryid_path = '$new_cat_path' WHERE categoryid = '$child_id'");
}//END OF FOR

$sql->db_Update("news_categories_lng","description = '".$t->formtpa($_POST['desc'])."',
meta_descr = '".$t->formtpa($_POST['meta_desc'])."',meta_keywords = '".$t->formtpa($_POST['meta_keyw'])."',
category = '".$t->formtpa($_POST['title'])."', image = '$image' WHERE categoryid = $cat AND code = '".DEFAULT_LANG."'");
	 header("Location: $module_path/categories.php?action=edit&cat=".$cat);
	 exit();
}

if ($_GET['action'] == "edit") 
{
	$smarty->assign("category",$category);
	$smarty->assign("edit",1);
	$smarty->assign("action","modify");
	$smarty->assign("more_categories",get_news_categories($category['parentid'],DEFAULT_LANG,0));
}
elseif ($_GET['action'] == "delete")
{
	 $sql->db_Select("news_categories","categoryid","news_categories.categoryid_path LIKE '%$cat/%'");
	 if ($sql->db_Rows() > 0) {
	 	$tmp = execute_multi($sql,0);
	 	for ($i=0;count($tmp) > $i;$i++)
	 	{
	 	$sql->db_Delete("news_categories","categoryid=".$tmp[$i]['categoryid']);
	 	$sql->db_Delete("news_categories_lng","categoryid=".$tmp[$i]['categoryid']);
//	 	echo "DELETE FROM news_categories WHERE categoryid=".$tmp[$i]['categoryid'].";<br>";
	 	}
	 }
//	 echo "DELETE FROM news_categories WHERE categoryid=".$cat.";<br>";
	 $sql->db_Delete("news_categories_lng","categoryid= $cat");
	 $sql->db_Delete("news_categories","categoryid= $cat");
	 $sql->db_Update("news_categories","catid = 0 WHERE groupid = $cat");//RESET news
	  header("Location: $module_path/categories.php?cat=".$parentid);
	 exit();
}

			####################### LOAD AJAX  ###########################
			include($_SERVER['DOCUMENT_ROOT']."/".$news_module['folder']."/admin/ajax_functions.php");
			###################### END OF AJAX #######################################	
$editor_settings['image_manager'] = 'OpenFileBrowser';
editor('desc','desc',0,0,0,$editor_settings);
}//END OF MODULE



$smarty->assign("MODULE_SETTINGS",$news_module['settings']);
$smarty->assign("USE_AJAX","modules/news/admin/news_ajax.tpl");
$smarty->assign("menu",$news_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("catid",$cat);
$smarty->assign("parentid",$parentid);
$smarty->assign("cat",get_news_categories($cat,DEFAULT_LANG,0));//assigned template variable cat
$smarty->assign("featured",get_featured_news($cat,"cat",DEFAULT_LANG));//assigned template variable featured
$smarty->assign("allcategories",get_all_news_categories(DEFAULT_LANG));//assigned template variable allcategories
$smarty->assign("nav",news_cat_nav($cat,DEFAULT_LANG_ADMIN));//assigned template variable a
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("current_category",get_news_category($cat,DEFAULT_LANG));//assigned template variable current_cat
$smarty->assign("include_file","modules/news/admin/categories.tpl");
$smarty->display("admin/home.tpl");

?>