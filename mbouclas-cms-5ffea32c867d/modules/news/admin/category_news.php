<?php
include("../../../manage/init.php");//load from manage!!!!
if ($news_module = module_is_active("news",1,1,0)) 
{
	$module_path = URL."/".$news_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",URL."/".$news_module['folder']."/admin");

$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];
$category = (empty($cat)) ? 0 : get_news_category($cat,DEFAULT_LANG);
$parentid = ($_GET['action']) ? $category['parentid'] : $cat;

$active = "0,1";
$categoryid = $category['categoryid'];

 $sql->db_Select("news_page_categories
 INNER JOIN news ON (news_page_categories.newsid=news.id)
 INNER JOIN news_categories ON (news_page_categories.catid=news_categories.categoryid)",
 "news_page_categories.newsid","(news_categories.categoryid_path LIKE '%$categoryid/%' 
 OR `news_categories`.categoryid_path LIKE '%/$categoryid' OR news_categories.categoryid_path = '$categoryid') 
 AND news_page_categories.main = 'Y' AND news.active IN ($active) ");

 
$tmp = execute_multi($sql);
 $lang = DEFAULT_LANG;

 for ($i=0;count($tmp) > $i;$i++)
 {
 	$news[] = get_news($tmp[$i]['newsid'],$lang);
 }
$smarty->assign("news",$news);

}

$smarty->assign("allcategories",get_all_news_categories(DEFAULT_LANG));//assigned template variable allcategories
$smarty->assign("MODULE_SETTINGS",$news_module['settings']);
$smarty->assign("menu",$news_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("catid",$cat);
$smarty->assign("parentid",$parentid);
$smarty->assign("cat",get_news_categories($cat,DEFAULT_LANG,0));//assigned template variable cat
$smarty->assign("nav",news_cat_nav($cat,DEFAULT_LANG));//assigned template variable a
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("current_category",get_news_category($cat,DEFAULT_LANG));//assigned template variable current_cat
$smarty->assign("include_file","modules/news/admin/category_news.tpl");
$smarty->display("admin/home.tpl");

?>