<?php
include("../../../manage/init.php");//load from manage!!!!
if ($news_module = module_is_active("news",1,1)) 
{
	$smarty->assign("latest_news",get_latest_news($news_module['settings']['latest_news_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",URL."/".$news_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$news_module['settings']);
	
	$smarty->assign("allcategories",get_all_news_categories(DEFAULT_LANG));
$id = (!empty($_GET['id'])) ? $_GET['id'] : $_POST['id'];
$sql = new db();
$t = new textparse();
if ($_POST['mode'] == "modify") 
{  
		while(list($key,$val)=each($_POST)) 
		{
		if (strstr($key,"-lng-")) 
		{ 
			list($field,$id)=split("-lng-",$key);
			$query = "$field = '$val' WHERE fieldid = $id";
			$sql->db_Update("news_extra_fields_lng",$query);
		}//END OF ELSEIF
		elseif (strstr($key,"-")) 
		{
			list($field,$id)=split("-",$key);
			
				if ($field == 'categoryids') 
				{  
					$extra_categories = $val;
					//first drop the old ones
					$sql->db_Delete("news_extra_field_categories","fieldid = '$id'");
 					for ($i=0;count($extra_categories) > $i;$i++)
				 {
				 	if ($extra_categories[$i] != "")
				 	{
				 		$sql->db_Insert("news_extra_field_categories","'".$extra_categories[$i]."','$id'");
				 	}
				 	
				 }//END OF FOR
				}//END OF IF
				else //Everything else
				{
					$query = "$field = '$val' WHERE fieldid = $id";
					$sql->db_Update("news_extra_fields",$query);
				}

			
		}//END OF IF
			//Go for categories
			

		}//END OF WHILE
}//END OF IF
//New extra field
if (!empty($_POST['fieldnew'])) 
{  
	$sql->db_Insert("news_extra_fields","'','".$_POST['activenew']."','".$_POST['typenew']."','".$_POST['varname']."'");
	$fieldid = mysql_insert_id(); 
	$sql->db_Insert("news_extra_fields_lng","'".DEFAULT_LANG."','$fieldid','".$t->formtpa($_POST['fieldnew'])."','".$t->formtpa($_POST['valuenew'])."'"); 
if ($_POST['newcategoryids']) 
				{  
					$extra_categories = $_POST['newcategoryids'];
					//first drop the old ones
					$sql->db_Delete("news_extra_field_categories","fieldid = '$fieldid'");
 					for ($i=0;count($extra_categories) > $i;$i++)
				 {
				 	if ($extra_categories[$i] != "")
				 	{
				 		$sql->db_Insert("news_extra_field_categories","'".$extra_categories[$i]."','$fieldid'");
				 	}
				 	
				 }//END OF FOR
				}//END OF IF
########### REPLICATE DATA FOR TRANSLATIONS ###############################

if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
	$countries = get_countries("Y",$trans="yes");
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert("news_extra_fields_lng","'".$countries[$i]['code']."','$fieldid','".$t->formtpa($_POST['fieldnew'])."','".$t->formtpa($_POST['valuenew'])."'"); 
}//END OF FOR
}//END OF IF
header("Location: $PHP_SELF?id=$fieldid&mode=tr");
exit();
}//END OF IF
//Delete extra field
if ($_GET['md'] == "del") 
{
 //delete the field  
 $sql->db_Delete("news_extra_fields","fieldid = ".$_GET['fieldid']);
 //delete the translations
 $sql->db_Delete("news_extra_fields_lng","fieldid= ".$_GET['fieldid']."");
 //delete the values
 $sql->db_Delete("news_extra_fields_values","fieldid = ".$_GET['fieldid']);
 //delete categories
 $sql->db_Delete("news_extra_field_categories","fieldid = ".$_GET['fieldid']);
 
}//END OF IF

//TRANSLATIONS
if ($_GET['m'] == "del") 
{
 $sql->db_Delete("news_extra_fields_lng","fieldid= ".$_GET['id']." AND code = '".$_GET['code']."'");
$smarty->assign("mode","update");//set mode to update so that smarty will display the translation tables
extra_fields_translations($id);
}//END OF IF
if ($_GET['mode'] == "tr") 
{  
news_extra_fields_translations($id);
$smarty->assign("mode","update");//assigned template variable mode
}//END OF IF
if ($_POST['mode'] == "insert") 
{  
$sql->db_Insert("news_extra_fields_lng","'".$t->formtpa($_POST['field_lng'])."','$id','".$t->formtpa($_POST['field_new_title'])."',
'".$t->formtpa($_POST['field_new_value'])."'");
$smarty->assign("mode","update");//set mode to update so that smarty will display the translation tables
extra_fields_translations($id);
}//END OF IF
elseif ($_POST['mode'] == "upd") 
{ 

		foreach ($_POST as $key => $val)
		{
		
		if (strstr($key,"-")) 
		{
			list($field,$code)=split("-",$key);
			$query = "$field = '$val' WHERE fieldid = $id AND code = '$code'";

			$sql->db_Update("news_extra_fields_lng",$query);
		}//END OF IF
		}//END OF WHILE
extra_fields_translations($id);
$smarty->assign("mode","update");//set mode to update so that smarty will display the translation tables
}//END OF ELSEIF

}//END OF MODULE


$extra_fields = get_extra_fields_news(0,DEFAULT_LANG);
$smarty->assign("allcategories",get_all_news_categories(DEFAULT_LANG));//assigned template variable allcategories
$smarty->assign("menu",$news_module['name']);
$smarty->assign("submenu","extra_fields");//assigned template variable extra_field
$smarty->assign("fields",$extra_fields);//assigned template variable extra_field
$smarty->assign("form_destination",$form_destination['product_new']);//assigned template variable form_destination
$smarty->assign("action_title",$lang['add_product']);//assigned template variable action_title
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/news/admin/extra_fields.tpl");
$smarty->display("admin/home.tpl");

?>