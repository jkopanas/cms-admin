<?php
include("../../../manage/init.php");//load from manage!!!!

if ($news_module = module_is_active("news",1,1)) 
{
	$module_path = URL."/".$news_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",$module_path);
$t = new textparse();
$page = $_POST['page'];
################################################ MASS ACTIVATE/DEACTIVATE ###################################
if ($_POST['mode'] == "deactivate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("news","active = 0 WHERE id = $key");				
			}
		}//END OF WHILE

	if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
	
}

if ($_POST['mode'] == "activate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("news","active = 1 WHERE id = $key");				
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
}
################################################ END MASS ACTIVATE/DEACTIVATE ###############################

################################################ MASS DELETE ###############################
if ($_POST['mode'] == "delete") 
{
	
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			if ($val == 1)//eliminates the check all/none box 
			{
	$news_details = get_news($id,DEFAULT_LANG);

	$categoryid = $news_details['catid'];  
//delete the news
$sql->db_Delete("news","id = $id"); 
//delete translations
$sql->db_Delete("news_lng","newsid = $id"); 
//delete news links
$sql->db_Delete("news_links","news_source = $id"); 
$sql->db_Delete("news_links","news_dest = $id"); 
//delete from any category
$sql->db_Delete("news_page_categories","newsid = $id"); 
//delete from any category
$sql->db_Delete("news_locations","newsid = $id"); 
//delete from images
$sql->db_Delete("news_images","newsid = $id"); 
//delete from featured news
$sql->db_Delete("featured_news","newsid = $id"); 
//delete from extra fields
$sql->db_Delete("news_extra_field_values","newsid = $id"); 
//delete from classes
$sql->db_Delete("news_class_news","newsid = $id"); 
//delete from bookmars
$sql->db_Delete("bookmars","newsid = $id");
//Delete from feeds
$sql->db_Delete("feeds","newsid = $id");
//Subtract from news count
$sql->db_Update("news_categories","news_count = news_count-1 WHERE categoryid = '$categoryid'");
//delete recipe features
//ALL DONE			
$news_dir = $_SERVER['DOCUMENT_ROOT'].$news_module['settings']['images_path']."/category_$categoryid/news_$id/";
recursive_remove_directory($news_dir);
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
			exit();
		}
		else {
				header("Location:".$module_path."/$page");
				exit();
			}	
}

################################################ END MASS DELETE ###############################

############################################### MASS FIRST ACTIVATION ##############################3
if ($_POST['mode'] == "first_activate") 
{
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "news_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("news_lng","title = '$val' WHERE newsid = $code");
							
						}
						else 
						{
							$sql->db_Update("news","$field = '$val' WHERE id = $code");
							
						}
						
					}
		}
	
	foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
					$sql->db_Update("news","active = 0 WHERE id = $code");
			}
		}
		//clear up
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS EDIT ##############################3
if ($_POST['mode'] == "mass_edit") 
{
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "news_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("news_lng","title = '$val' WHERE newsid = $code");
							
						}
						elseif ($field == "categoryid")
						{
								//PRODUCT CATEGORIES
								$sql->db_Select("news_page_categories","catid","newsid = $code AND main = 'Y'");

								if ($sql->db_Rows() > 0) //UPDATE
								{
									$sql->db_Update("news_page_categories","catid = $val WHERE newsid = $code AND main = 'Y'");
								}
								else 
								{
									$sql->db_Insert("news_page_categories","'$val','$code','Y'");
								}
						}
						else 
						{
							$sql->db_Update("news","$field = '$val' WHERE id = $code");
							
						}
						
					}
		}
	
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS DELETE IMAGES ##############################3
if ($_POST['mode'] == "delete_images") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
						
						list($field,$code)=split("-",$key);
						$sql->db_Select("news_images","image,thumb,newsid","id = $key");
 						$a = execute_single($sql);
 						$sql->db_Select("news","image","id = ".$a['newsid']);
 						$news = execute_single($sql);
// 						print_r($news);
 						if ($a['thumb'] == $news['image']) 
 						{
 							$sql->db_Update("news","image = '".$news_module['settings']['default_thumb']."' WHERE id = ".$a['newsid']);
 						}
 						@unlink($_SERVER['DOCUMENT_ROOT'].$a['image']); 
						@unlink($_SERVER['DOCUMENT_ROOT'].$a['thumb']);
 						$sql->db_Delete("news_images","id = $key");
//echo $key."<br>";
					}
		}
	
		header("Location:".$module_path."/$page");
		exit();
}
############################################### MASS ACTIVATE IMAGES ##############################3
if ($_POST['mode'] == "activate_all_images") 
{
	$id = $_POST['id'];
	$sql->db_Update("news_images","available = 1 WHERE newsid = $id AND type = ".$_POST['type']);
	header("Location:".$module_path."/$page");
	exit();
}

}//END OF LOAD MODULE
?>