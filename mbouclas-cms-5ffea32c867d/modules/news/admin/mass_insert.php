<?php
include("../../../manage/init.php");//load from manage!!!!

if ($module = module_is_active("products",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$module['folder']."/admin");
	$smarty->assign("allcategories",get_all_product_categories(DEFAULT_LANG));
	$smarty->assign("action",$_GET['action']);
	
	
	if ($_GET['action']) 
	{
		//Check for mass uploaded products
		$sql->db_Select("products","id","active = 2");
		if ($sql->db_Rows() > 0) 
		{
			$tmp_uploads = execute_multi($sql);
			for ($i=0;count($tmp_uploads) > $i;$i++)
			{
				$products[$i] = get_product($tmp_uploads[$i]['id'],DEFAULT_LANG);
			}
			$smarty->assign("products",$products);
		}//END OF UPLOADED PRODUCTS FOUND
	}//END OF REVIEW UPLOADED PRODUCTS
}//END OF MODULE



$smarty->assign("menu",$module['name']);
$smarty->assign("submenu","mass_insert");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/products/admin/mass_insert.tpl");
$smarty->display("admin/home.tpl");

?>