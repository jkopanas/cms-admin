<?php
include("../../../manage/init.php");//load from manage!!!!

if ($news_module = module_is_active("news",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$news_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
	
	
########################## UPDATE news ##################################################################################
if ($_POST['action'] == "update") 
{
	$required_fields = array('news_title');

	//STEP 1: grab POST variables into a formated array
 $posted_data = array();
 foreach ($_POST as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
//Database insertions
//Add the basic news details
$categoryid = $_POST['categoryid'];
$extra_categories = $_POST['categoryids'];
$sku = $_POST['sku'];
$active = $_POST['active'];
$orderby = $_POST['orderby'];
$description = $t->formtpa($_POST['short_desc']);
$long_description = $t->formtpa($_POST['long_desc']);
$meta_desc = $t->formtpa($_POST['meta_desc']);
$meta_keywords = $t->formtpa($_POST['meta_keywords']);
$title = $t->formtpa($_POST['news_title']);
$orderby = $_POST['orderby'];
//print_r($_POST);//END OF print_r
$query = "active = '$active', orderby = $orderby WHERE id = $id";
$sql->db_Update("news",$query);
//echo $query."<br><br>";
$query = "title = '$title',description = '$description',full_description = '$long_description', meta_desc = '$meta_desc', 
meta_keywords = '$meta_keywords' WHERE newsid = $id AND code = '".DEFAULT_LANG."'";
//echo $query."<br><br>";
$sql->db_Update("news_lng",$query);

//SECONDARY UPDATES

//news CATEGORIES
$sql->db_Select("news_page_categories","catid","newsid = $id AND catid = $categoryid AND main = 'Y'");

if ($sql->db_Rows() > 0) //UPDATE
{
	$sql->db_Update("news_page_categories","catid = $categoryid WHERE newsid = $id AND main = 'Y'");
}
else 
{
	$sql->db_Insert("news_page_categories","'$categoryid','$newsid','Y'");
}


//Add main category
$sql->db_Update("news_page_categories","catid = '$categoryid' WHERE main = 'Y' AND newsid = '$id'");
//See if there are any extra categories to insert



if ($extra_categories) 
{  
	//first drop the old ones
$sql->db_Delete("news_page_categories","newsid = '$id' AND main = 'N'");
 for ($i=0;count($extra_categories) > $i;$i++)
 {
 	if ($extra_categories[$i] != "")
 	{
 		$sql->db_Insert("news_page_categories","'".$extra_categories[$i]."','$id','N'");
 	}
 	
 }//END OF FOR
}//END OF IF

//add extra fields
upd_news_extra_field_values($_POST,$id,DEFAULT_LANG);
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
upd_news_extra_field_values($_POST,$id,$countries[$i]['code']);
}//END OF FOR
}//END OF IF

//UPDATE TAGS
$tags = construct_tags_array($_POST['tags'],",");
//Clear old tags
$sql->db_Delete("news_tags","newsid = $id");
//insert new tags
for ($i=0;count($tags) > $i;$i++)
{
	$tag = $tags[$i];
	$sql->db_Insert("news_tags","'','$id','$tag'");
}
$smarty->assign("updated",1);
 }//END OF NO ERRORS
 $smarty->assign("error_list",$error_list);//assigned template variable error_list<BR>
}

	
################################################## LOAD news ############################################################
	$news = get_news($id,DEFAULT_LANG);

	if (!empty($news)) 
	{  
	$sql->db_Select("news_page_categories","catid","newsid = $id AND main = 'N'");
	$all_news_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_news_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_news_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_news",get_latest_news($news_module['settings']['latest_news'],DEFAULT_LANG,"no",$news['catid']));
	$smarty->assign("extra_fields",get_extra_fields_news("Y",DEFAULT_LANG,$id,$news['catid']));

	$smarty->assign("id",$id);
	$smarty->assign("tags",get_news_tags($id,","));
	$smarty->assign("nav",news_cat_nav($news['catid'],DEFAULT_LANG));

	

	editor("description","description1",0,0,0,$editor_settings);
	editor("long","long1",0,0,0,$editor_settings);
	$smarty->assign("news",$news);
	$smarty->assign("news_module",$news_module);
	$smarty->assign("action","update");
	}//END OF news found
	else 
	{  
		header("Location: search.php"); 
		exit();
	}//END OF ELSE

################################################ END OF LOAD news #########################################
}

$smarty->assign("USE_AJAX",$news_module['folder']."/admin/ajax_functions.tpl");
$smarty->assign("menu",$news_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","modify");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/news/admin/news_modify.tpl");
$smarty->display("admin/home.tpl");

?>