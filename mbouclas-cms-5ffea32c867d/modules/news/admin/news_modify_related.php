<?php
include("../../../manage/init.php");//load from manage!!!!

if ($news_module = module_is_active("news",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$news_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
	
	

	
################################################## LOAD news ############################################################
	$news = get_news($id,DEFAULT_LANG);

	if (!empty($news)) 
	{  
	$sql->db_Select("news_page_categories","catid","newsid = $id");
	$all_news_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_news_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_news_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_news",get_latest_news($news_module['settings']['latest_news'],DEFAULT_LANG,"no",$news['catid']));
	$smarty->assign("extra_fields",get_news_extra_fields("Y",DEFAULT_LANG,$id));

	$smarty->assign("id",$id);
	$smarty->assign("tags",get_news_tags($id,","));
	$smarty->assign("nav",news_cat_nav($news['catid'],DEFAULT_LANG));

	


	$smarty->assign("news",$news);
	$smarty->assign("news_module",$news_module);
	$smarty->assign("action","update");
	}//END OF news found
	else 
	{  
		header("Location: search.php"); 
		exit();
	}//END OF ELSE
################################################ END OF LOAD news #########################################

			####################### LOAD AJAX  ###########################
			include($_SERVER['DOCUMENT_ROOT']."/".$news_module['folder']."/admin/ajax_functions.php");
			###################### END OF AJAX #######################################	
			
}

$smarty->assign("related",get_related_news($id,DEFAULT_LANG));
$smarty->assign("USE_AJAX","modules/news/admin/news_ajax.tpl");
$smarty->assign("menu",$news_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","modify");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/news/admin/news_modify_related.tpl");
$smarty->display("admin/home.tpl");

?>