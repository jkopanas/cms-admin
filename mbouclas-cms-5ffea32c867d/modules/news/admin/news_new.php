<?php
include("../../../manage/init.php");//load from manage!!!!

if ($news_module = module_is_active("news",1,1,1)) 
{
	$module_path = URL."/".$news_module['folder']."/admin";
	$smarty->assign("latest_news",get_latest_news($news_module['settings']['latest_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",$module_path);
	
	$t = new textparse();

	
	
########################## UPDATE news ##################################################################################
if ($_POST['action'] == "add") 
{
	$required_fields = array('news_title');

	//STEP 1: grab POST variables into a formated array
 $posted_data = array();
 foreach ($_POST as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
//Database insertions
//Add the basic news details
$categoryid = $_POST['categoryid'];
$extra_categories = $_POST['categoryids'];
$sku = $_POST['sku'];
$orderby = $_POST['orderby'];
$active = $_POST['active'];
$orderby = $_POST['orderby'];
$description = $t->formtpa($_POST['short_desc']);
$long_description = $t->formtpa($_POST['long_desc']);
$meta_desc = $t->formtpa($_POST['meta_desc']);
$meta_keywords = $t->formtpa($_POST['meta_keywords']);
$date_added = time();
$image = ($_POST['image']) ? $_POST['image'] : $news_module['settings']['default_thumb'];
$title = $t->formtpa($_POST['news_title']);
//print_r($_POST);//END OF print_r
$sql->db_Insert("news","'','$image','$date_added','$active','".ID."','$orderby'");
$id = mysql_insert_id();
$sql->db_Insert("news_lng","'".DEFAULT_LANG."','$id','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
//TRANSLATIONS

if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
$countries = get_countries(2,$trans="yes");
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert("news_lng","'".$countries[$i]['code']."','$id','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
}//END OF FOR
}//END OF IF


//SECONDARY UPDATES
//news CATEGORIES
	$sql->db_Insert("news_page_categories","'$categoryid','$id','Y'");

//See if there are any extra categories to insert
if (!empty($extra_categories)) 
{  
 for ($i=0;count($extra_categories) > $i;$i++)
 {
 	$sql->db_Insert("product_page_categories","'".$extra_categories[$i]."','$id','N'");
 }//END OF FOR
}//END OF IF

//See if there are any extra categories to insert
//first drop the old ones
$sql->db_Delete("news_page_categories","newsid = '$id' AND main = 'N'");
if (!empty($extra_categories)) 
{  
 for ($i=0;count($extra_categories) > $i;$i++)
 {
 	$sql->db_Insert("news_page_categories","'".$extra_categories[$i]."','$id','N'");
 }//END OF FOR
}//END OF IF

//add extra fields
add_news_extra_field_values($_POST,$id,DEFAULT_LANG);
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
add_news_extra_field_values($_POST,$id,$countries[$i]['code']);
}//END OF FOR
}//END OF IF

//ADD TAGS
$tags = construct_tags_array($_POST['tags'],",");
//insert new tags
for ($i=0;count($tags) > $i;$i++)
{
	$tag = $tags[$i];
	$sql->db_Insert("news_tags","'','$id','$tag'");
}
header("Location: $module_path/news_modify.php?id=$id");
exit();
 }//END OF NO ERRORS
 $smarty->assign("error_list",$error_list);//assigned template variable error_list<BR>
}

	
################################################## LOAD news FEATURES ############################################################
	$news = get_news($id,DEFAULT_LANG);
	

	$smarty->assign("allcategories",get_all_news_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("extra_fields",get_extra_fields_news("Y",DEFAULT_LANG));


	############# CHECK OUT FOR MANUFACTURERS MODULE ###############################

	if (module_is_active($manufacturers_module = "manufacturers",1,1)) 
	{
		$smarty->assign("manufacturers_list",get_manufacturers(DEFAULT_LANG));
		$smarty->assign("manufacturers_module",$manufacturers_module);
	}
	############# END OF  MANUFACTURERS MODULE #####################################
	
	############# CHECK OUT FOR ESHOP MODULE #######################################
	
	if (module_is_active($eshop_module = "eshop",1,1)) 
	{
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
	}
		if ($_GET['cat']) 
	{
		$tmp_cat['catid']=$_GET['cat'];
		$smarty->assign("news",$tmp_cat);
		
	}	
	editor("description","description1",0,0,0,$editor_settings);
	editor("long","long1",0,0,0,$editor_settings);
	$smarty->assign("news_module",$news_module);
	$smarty->assign("action","add");
################################################ END OF LOAD news #########################################
}//END OF LOAD MODULE



$smarty->assign("USE_AJAX",$news_module['folder']."/admin/ajax_functions.tpl");
$smarty->assign("menu",$news_module['name']);
$smarty->assign("submenu","add");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/news/admin/news_modify.tpl");
$smarty->display("admin/home.tpl");

?>