<?php
global $smarty;

$xajax->registerFunction("display_chart_item");

function display_chart_item($id,$res_div)
{
	global $sql,$smarty;
	$objResponse = new xajaxResponse();

	$smarty->assign("news",get_news($id,FRONT_LANG,1,1));
	
	$message = $smarty->fetch("modules/news/news_item.tpl");	
	
	$objResponse->addAssign($res_div,"innerHTML",$message);
//	$objResponse->addScript("setTimeout(\"document.getElementById('inr_display').innerHTML=''\",3500);");
	return $objResponse;
}

?>