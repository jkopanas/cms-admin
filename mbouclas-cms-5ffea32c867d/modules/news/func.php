<?php
function check_news_cat($cat) 
{ 
	$sql = new db();
	$sql->db_Select("news_categories","categoryid","categoryid = $cat");
	if ($sql->db_Rows() >0) 
	 {  
	 	return $cat; 
	 }//END OF IF 
	 else 
	 {  
	 header("Location: index.php");
	 exit();
	 }//END OF ELSE
}//END OF FUNCTION check_cat

function check_news($id) 
{ 
	$sql = new db();
	$sql->db_Select("news","id","id = $id AND active = 1");
	$r = $sql->db_Fetch();
	if ($sql->db_Rows() > 0)
	{
		return $r['id'];
	}
	else 
	 {  
	 	header("Location: index.php");
	 	exit(); 
	 }//END OF ELSE 
}//END OF FUNCTION check_product

function check_news_image($imageid) 
{ 
	global $sql;
	$sql->db_Select("news_images","*","id = $imageid AND available = 1");
	if ($sql->db_Rows() > 0)
	{
	 	return execute_single($sql,0);
	}
	else 
	 {  
		return 0;
	 }//END OF ELSE 
}//END OF FUNCTION check_image

function add_news_extra_field_values($data,$newsid,$lang) 
{ 
		global $sql;
		while(list($key,$val)=each($data)) 
		{
		if (strstr($key,"-")) 
		{
			list($field,$id)=split("-",$key);
			if ($val) 
			{  
			$query = "'$newsid','$id','$val','$lang'";
			$sql->db_Insert("news_extra_field_values",$query);
			}//END OF IF not empty field
		}//END OF IF
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function upd_news_extra_field_values($data,$newsid,$lang) 
{ 
		global $sql;
		$t = new textparse();
		foreach ($data as $key => $val)
		{
		if (strstr($key,"-")) 
		{

			$val = $t->formtpa($val);
			list($field,$id)=split("-",$key);
			$sql->db_Select("news_extra_field_values","newsid","newsid = '$newsid' AND fieldid = '$id' AND code = '$lang'");
			if ($sql->db_Rows() > 0) 
			{ 
			$query = "value = '$val' WHERE newsid = '$newsid' AND fieldid = '$id' AND code = '$lang'";
			
			$sql->db_Update("news_extra_field_values",$query);	
			}//END OF IF
			else 
			{ 
			if ($val) 
			 {  
			$sql->db_Insert("news_extra_field_values","'$newsid','$id','$val','$lang'"); 
			}//END OF IF 
			}//END OF ELSE
		}//END OF IF
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function get_extra_fields_news($active,$lang,$newsid=0,$catid=0) 
{ 
global $sql;
$fields = "news_extra_fields.fieldid, news_extra_fields.active,news_extra_fields.var_name, news_extra_fields.type, news_extra_fields_lng.field, news_extra_fields_lng.value";
$tables = "news_extra_fields INNER JOIN news_extra_fields_lng ON (news_extra_fields.fieldid = news_extra_fields_lng.fieldid)";
if ($catid) 
{
	//Check if exists at all
	$sql->db_Select("news_extra_field_categories","catid","catid = $catid");
	if ($sql->db_Rows() > 0)
	{
		$tables .="INNER JOIN news_extra_field_categories ON (news_extra_field_categories.fieldid=news_extra_fields.fieldid)";
		$cat = " AND catid = $catid GROUP BY fieldid";
	}

}
if ($active == 0) 
{  
 $sql->db_Select($tables,$fields,"code = '$lang' $cat ORDER BY news_extra_fields.type DESC");
}//END OF IF
else 
{  
$sql->db_Select($tables,$fields,"active = '$active' AND code = '$lang' $cat"); 
}//END OF ELSE
//echo "SELECT $fields FROM $tables WHERE code = '$lang' ORDER BY news_extra_fields.type DESC";
if ($sql->db_Rows() > 0)
 {

  $fields = execute_multi($sql,1);
  if ($newsid) 
  {
  	  for ($i=0;count($fields) > $i;$i++)
 	 {
 	 	$sql->db_Select("news_extra_field_values","*","code = '$lang' AND newsid = $newsid AND fieldid = ".$fields[$i]['fieldid']);
// 	 	echo "SELECT * FROM news_extra_field_values WHERE code = '$lang' AND newstid = $newsid AND fieldid = ".$fields[$i]['fieldid'];
 	 	$values = execute_single($sql);
  		$fields[$i]['values'] = $values;
 	 }

  }
  //GET AVAILABLE CATEGORIES
     for ($i=0;count($fields) > $i;$i++)
 	 {
			$fields[$i]['categories'] = get_news_extra_field_categories($fields[$i]['fieldid']);
 	 }
  	return $fields;
  	}
  
}//END OF FUNCTION get_extra_fields

function get_news_extra_field_categories($id)
{
	global $sql;
	 	$sql->db_Select("news_extra_field_categories","catid","fieldid = ".$id);
 	 	if ($sql->db_Rows() > 0) 
 	 	{
 	 		return  execute_multi($sql);
 	 	}
}

function get_news_extra_fields($id,$lang,$hidden=0) 
{ 
	global $sql;
	$t = new textparse();
	 	//LOAD ANY POSSIBLE EXTRA FIELDS
	$sql->db_Select("news_extra_fields INNER JOIN news_extra_field_values ON (news_extra_fields.fieldid = news_extra_field_values.fieldid)",
	"news_extra_fields.fieldid,news_extra_fields.type,news_extra_fields.var_name","newsid = $id AND code = '$lang' AND active = 'Y'");
	for ($i=0;$sql->db_Rows() > $i;$i++)
	{
		$r = $sql->db_Fetch();
if ($r['type'] != "hidden") //do no display hidden fields
 {  
$fields[] = filter_news_exta_fields($r['fieldid'],$id,$lang);
 }//END OF IF $r['type'];
		

	}//END OF FOR
	return $fields;
}//END OF FUNCTION get_product_extra_fields

function news_extra_fields($id,$lang) 
{ 
	$sql = new db();
	$t = new textparse();
	 	//LOAD ANY POSSIBLE EXTRA FIELDS
	$sql->db_Select("news_extra_fields INNER JOIN news_extra_field_values ON (news_extra_fields.fieldid = news_extra_field_values.fieldid)",
	"news_extra_fields.fieldid","id = $id AND active = 'Y'");
//	echo "SELECT extra_fields.fieldid FROM extra_fields INNER JOIN extra_field_values ON (extra_fields.fieldid = extra_field_values.fieldid) WHERE productid = $id AND active = 'Y'";
	for ($i=0;$sql->db_Rows() > $i;$i++)
	{
		$r = $sql->db_Fetch();
		$fields[$i] = filter_news_exta_fields($r['fieldid'],$id,$lang);
	}//END OF FOR
	return $fields;
}//END OF FUNCTION get_product_extra_fields

function get_news_extra_field_values($newsid,$lang) 
{ 
  $sql = new db();
  $sql->db_Select("news_extra_field_values","newsid,value","newsid = $newsid AND code = '$lang'");
  return execute_multi($sql,1);
}//END OF FUNCTION get_extra_field_values

function filter_news_exta_fields($id,$newsid,$lang) 
{ 
	$sql = new db();
	$fields = "news_extra_fields.fieldid, news_extra_fields.type,news_extra_fields.var_name, news_extra_field_values.value, news_extra_field_values.newsid, news_extra_fields_lng.field";
	$tables = "news_extra_fields INNER JOIN news_extra_field_values ON (news_extra_fields.fieldid = news_extra_field_values.fieldid)
  			   INNER JOIN news_extra_fields_lng ON (news_extra_fields.fieldid = news_extra_fields_lng.fieldid)";
	$q = "news_extra_fields.fieldid = $id AND news_extra_fields_lng.code = '$lang' AND newsid = '$newsid'";
	$sql->db_Select($tables,$fields,$q);
//echo "SELECT $fields FROM $tables WHERE $q<br><br>";
	return execute_single($sql,1);
}//END OF FUNCTION filter_exta_fields

function get_news($id,$lang,$active="no",$efields=0) 
{ 
  $active = ($active == "no") ? "" : "AND news.active = $active";
  $sql = new db();
  $t = new convert();
  $fields = "news.uid, news.orderby, news.active, news.date_added, news.id, news_lng.title, news_lng.description,
   news_lng.full_description, news_lng.meta_desc, news_lng.meta_keywords";
  $tables = "news 
INNER JOIN news_lng ON (news.id=news_lng.newsid)";
  $q = "news.id = $id AND news_lng.code = '$lang' $active";
//echo "SELECT $fields FROM $tables WHERE $q<br><br>";
  $sql->db_Select($tables,$fields,$q);
if ($sql->db_Rows() > 0) 
{  
	$news = execute_single($sql,1);
	$tmp_cat = get_news_cat($id);//product category
	$news['catid'] = $tmp_cat['catid'];//Backward compatibility
	$news['cat'] = get_news_category($news['catid'],$lang);
	$news['category'] = $news['cat']['category'];//Backward compatibility
	$news['nav_cat'] =  news_cat_nav($news['catid'],FRONT_LANG);
	$news['extra_fields'] = news_extra_fields($id,$lang);
	$news['user'] = get_users($news['uid']);
	if ($efields) 
	{
		$news['efields'] = simplify_efields(get_news_extra_fields($id,$lang));
	}
	
	$thumb = get_news_detailed_images($id,0);
		$news['image'] = $thumb[0]['thumb'];//Product Thumb (backward compatibility
		$news['image_full'] = get_news_detailed_images($id,1);
	
	return $news;
}//END OF FOUND PRODUCT
else 
{  
return 0; 
}//END OF ELSE
}//END OF FUNCTION get_product

function news_cat_nav($cat,$lang,$table="",$fields="",$tables="") 
{ 
	global $sql;
	$table_lng = ($table == "") ? "news_categories_lng" : $table."_lng";
	$table = ($table == "") ? "news_categories" : $table;
	$sql->db_Select("$table","categoryid_path","categoryid = $cat") ;
	$r = $sql->db_Fetch();
	$tmp = ($sql->db_Rows() > 0) ? explode("/",$r['categoryid_path']) : 0;


	if (is_array($tmp) and count($tmp) > 0) 
	{
		//found subcategory
		$fields = ($field == "") ? "$table_lng.category, $table_lng.image, $table.categoryid" : $fields;
		$tables = ($tables == "") ? "$table INNER JOIN $table_lng ON ($table.categoryid = $table_lng.categoryid)" : $tables;
		
		
	 	for ($i=0;count($tmp) > $i;$i++)
		{
//			echo "SELECT $fields FROM $tables WHERE $table.categoryid = ".$tmp[$i]." AND code = '$lang'";
		$sql->db_Select($tables,$fields,"$table.categoryid = ".$tmp[$i]." AND code = '$lang'");
		$ar[] = execute_single($sql,1);
		}//END OF FOR
	return $ar;
	}//END OF IF subnews_categories found
	else 
	{  
	return 0; 
	}//END OF ELSE	
}//END OF FUNCTION cat_nav

function news_extra_fields_translations($id) 
{ 
	global $smarty,$sql;
$sql->db_Select("news_extra_fields","field","fieldid = $id");
$r = $sql->db_Fetch();
$field_name = $r['field'];
$countries = get_countries("Y",$trans="yes");
$class_lng = array();
$translations = array();

for ($i=0,$j=0,$d=0;count($countries) > $i;$i++)
{
	$sql->db_Select("news_extra_fields_lng","*","code = '".$countries[$i]['code']."' AND fieldid = $id");
	if ($sql->db_Rows() == 0) 
	{  
	 	$field_lng[$j]['code'] =  $countries[$i]['code'];
	 	$field_lng[$j]['country'] =  $countries[$i]['country'];
	 	$j++;
	}//END OF IF
	else 
	{  
	//see for possible translations
	 	$r = $sql->db_Fetch();
	 	$country_details = get_lang_name($r['code']);
	 	foreach ($r as $key => $value) 
	 	{
	 	   	if (!preg_match("[0-9]",$key)) 
	 	{
	 		$translations[$d][$key] = $value;
	 	}//END OF IF
	 	}//END OF FOREACH 
	 	$translations[$d]['country'] = $country_details['country'];
	 	$d++;
	}//END OF ELSE	
}//END OF FOR
$smarty->assign("field_name", $field_name);//assigned template variable extra_fields 
$smarty->assign("field_id", $id);//assigned template variable extra_fields 
$smarty->assign("trans",$translations);//assigned template variable trans  
$smarty->assign("field_lng",$field_lng);//assigned template variable product_lng  
}//END OF FUNCTION extra_fields_translations

function get_news_pages_images($id,$limit=0,$type="images",$multi=0) 
{ 
	global $sql;

	$type = ($multi) ? " AND type IN ($type)" : " AND type = '$type'";
	$sql->db_Select("news_images","*","available = 1 AND newsid = $id $type ORDER BY orderby,date_added");
//	echo "SELECT * FROM images WHERE available = 1 AND productid = $id $type ORDER BY orderby<br>";
	if ($sql->db_Rows() > 0) 
	{
		if ($limit) 
		{
			
			return execute_paginated($sql,1,$limit);
		}
		else 
		{
			return execute_multi($sql,1);
		}
	}
	 	
}//END OF FUNCTION get_detailed_images

function get_news_cat($id) 
{ 
	$sql = new db();
	$sql->db_Select("news_page_categories","catid","newsid = $id AND main ='Y'");
//	echo "SELECT catid FROM news_page_categories WHERE newstid = $id AND main ='Y'<br>";
	return execute_single($sql,0);
}//END OF FUNCTION get_product_cat

function get_news_category($cat,$lang,$settings=0) 
{ 
  $sql = new db();
  $fields  ="news_categories_lng.category, news_categories_lng.image, news_categories.settings ,news_categories.categoryid, news_categories_lng.description,
  news_categories_lng.meta_descr, news_categories_lng.meta_keywords, news_categories.order_by, news_categories.parentid,news_categories.categoryid_path";
  $tables = "news_categories INNER JOIN news_categories_lng ON (news_categories.categoryid = news_categories_lng.categoryid)";
  $sql->db_Select($tables,$fields,"news_categories.categoryid = $cat AND code = '$lang'");
if ($sql->db_Rows() > 0) 
{
	 $a = execute_single($sql,1);
	 if ($settings) 
	 {
	 		
	 		$a['settings'] = form_settings_array($a['settings'],"###",":::");
	 }
}
 return $a;
}//END OF FUNCTION get_category

function format_news_category($data) 
{ 
  $tmp = explode("/",$data['category']);
  for ($i=0;count($tmp) > $i;$i++)
  {
  	$ar[$i]['cat'] = $tmp[$i];
  }//END OF FOR
  $tmp = explode("/",$data['categoryid_path']);
  for ($i=0;count($tmp) > $i;$i++)
  {
  	$ar[$i]['catid'] = $tmp[$i];
  }//END OF FOR
  return $ar;
}//END OF FUNCTION format_category

function get_news_pages($cat,$sort="default",$way="default",$lang,$type="cat",$active="no",$page=false,$limit="no",$efields=0,$start=0) 
{
	$active_news = $active; 
	$active = ($active == "no") ? "0,1" : "1"; 
	global $sql;
	$t = new textparse();
	if (!$page) {
		$not_paginated = 1;
	}
	$way = ($way != "default") ? $way : "DESC";
	$sort = ($sort != "default") ? $sort : "date_added";
	$page = ($page == false) ? CURRENT_PAGE : $page;
	if ($start) 
	{
		$limit_q = " LIMIT $start,$limit";
	}
	$table = ($type == "cat") ? "news_categories" : "news_locations";
	$tables = "news
 INNER JOIN news_page_categories ON (news.id=news_page_categories.newsid)
 INNER JOIN news_categories_lng ON (news_page_categories.catid=news_categories_lng.categoryid)
 INNER JOIN news_lng ON (news.id=news_lng.newsid)";
	$field = "news_page_categories.catid";
	$q = "($field = $cat) AND
  (active IN ($active)) AND 
  (news_lng.code = '".$lang."') AND 
  (news_categories_lng.code = '".$lang."') AND
  news_page_categories.main = 'Y' 
   ORDER BY $sort $way $limit_q";
	$sql->db_Select($tables,"news.id",$q);

//	echo "SELECT news.id FROM $tables WHERE $q";
	if ($sql->db_Rows() >0) 
	{ 
		if ($not_paginated) 
		{
			$tmp = execute_multi($sql); 
		}
		else {
			$tmp = execute_paginated($sql,0,$limit,0,$page); 
		}
		

		for ($i=0;count($tmp) > $i;$i++)
		{
			$news[] = get_news($tmp[$i]['id'],$lang,$active_news);
		}//END OF FOR
	}//END OF IF rows found
	if ($efields) 
	{
		for ($i=0;count($news) > $i;$i++)
		{
			$efields_val = get_news_extra_fields($news[$i]['id'],$lang);
			if ($efields_val) 
			{
				$news[$i]['efields'] = simplify_efields($efields_val);
			}
		}
	}
	
	 return $news;
}//END OF FUNCTION get_products

function get_news_categories($cat,$lang,$active=1) 
{ 
		$active = ($active == 1) ? "1" : "0,1";
global $sql;
		$fields = "news_categories_lng.category, news_categories_lng.description, news_categories_lng.meta_descr, 
		news_categories_lng.meta_keywords, news_categories_lng.image, news_categories.categoryid, news_categories.parentid, 
		news_categories.categoryid_path, news_categories.order_by,  news_categories.product_count, news_categories.date_added"; 
 $tables = "news_categories INNER JOIN news_categories_lng ON (news_categories.categoryid = news_categories_lng.categoryid)";
 $sql->db_Select($tables,"$fields","parentid = $cat AND code = '$lang' ORDER BY order_by");
//echo "SELECT $fields FROM $tables WHERE parentid = $cat AND code = '$lang' ORDER BY order_by";
 if ($sql->db_Rows() > 0) 
 {  
 $categories = execute_multi($sql,1);
 for ($i=0;count($categories) > $i;$i++)
 {
 $categoryid = $categories[$i]['categoryid'];
 $sql->db_Select("news_page_categories
 INNER JOIN news ON (news_page_categories.newsid=news.id)
 INNER JOIN news_categories ON (news_page_categories.catid=news_categories.categoryid)",
 "news_page_categories.newsid","(news_categories.categoryid_path LIKE '%$categoryid/%' 
 OR `news_categories`.categoryid_path LIKE '%/$categoryid' OR news_categories.categoryid_path = '$categoryid') 
 AND news.active IN ($active) ");

 $categories[$i]['num_news'] = $sql->db_Rows();

 
 $sql->db_Select("news_categories","categoryid","parentid = $categoryid");

 $categories[$i]['num_sub'] = $sql->db_Rows();
  //get settings 
 for ($j=0;count($categories) > $j;$j++)
 {
 	$categories[$j]['settings'] = form_settings_array($categories[$j]['settings'],"###",":::");
 }
 }//END OF FOR
return $categories;
 }//END OF IF
 else 
 {  
 return 0; 
 }//END OF ELSE
}//END OF FUNCTION get_categ

function get_all_news_categories($lang)
{
	$sql = new db();
	$fields = "news_categories_lng.category, news_categories_lng.image, news_categories.categoryid, news_categories.parentid,
	news_categories.categoryid_path";
	$tables = "news_categories INNER JOIN news_categories_lng ON (news_categories.categoryid = news_categories_lng.categoryid)";
	$sql->db_Select($tables,$fields, "code = '$lang' ORDER BY categoryid_path");
	if ($sql->db_Rows() > 0) 
	{  
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
		$r = $sql->db_Fetch();
		foreach ($r as $key => $value) 
		{
		   	if (!preg_match("[0-9]",$key)) 
		{
			$ar[$i][$key] = $value;
		}//END OF IF
		}//END OF FOREACH 
		$ar[$i]['category'] = construct_news_category($r['categoryid_path'],$lang);
		}//END OF FOR
		sort($ar);
	return $ar;	
	}//END OF IF
}//END OF FUNCTION

function construct_news_category($path,$lang) 
{ 
	$sql = new db();
	if (strstr($path,"/")) 
	{  
	$tmp = explode("/",$path);
	$ids = implode(",",$tmp);
	}//END OF IF
	else 
	{  
	$ids = $path; 
	}//END OF ELSE
$sql->db_Select("news_categories_lng","category","categoryid IN ($ids) AND code = '$lang'");
for ($i=0;$sql->db_Rows() > $i;$i++)
	 {
	 	$r = $sql->db_Fetch();
	 	$ar[] = $r['category'];
	 }//END OF FOR	 
	 if (count($ar) > 0) 
	 {  
	 	$return = implode("/",$ar); 
	 }//END OF IF
	 else 
	 {  
	 	$return = $r['category']; 
	 }//END OF ELSE

	 return $return;
}//END OF FUNCTION construct_category

function get_news_images($id,$type="images") 
{ 
	$sql = new db();
	$sql->db_Select("news_images","*","available = 1 AND newsid = $id  AND type = '$type' ORDER BY orderby");
//	echo "SELECT * FROM images WHERE available = 1 AND productid = $id  AND type = '$type' ORDER BY orderby";
	 	return execute_multi($sql,1);
}//END OF FUNCTION get_detailed_images

function get_news_image($id)
{
	global $sql;
	
	$sql->db_Select("news_images","*","id = $id");
	return execute_single($sql,1);
}

function get_news_detailed_images($newsid,$type="images",$active=0) 
{ 
  $sql = new db();
  $type = ($type == "") ? "AND type = 0" : "AND type IN (0,$type)";
  $active = ($active == 0) ? "0,1" : $active;
  $sql->db_Select("news_images","*","newsid = $newsid AND available IN ($active) $type  ORDER BY orderby");
//  echo "SELECT * FROM news_images WHERE newsid = $newsid AND available IN ($active) $type  ORDER BY orderby<br><br><Br>";
  	return execute_multi($sql,1);
}//END OF FUNCTION get_detailed_images

function get_all_news_detailed_images($newsid,$type="image",$active=0) 
{ 
  $sql = new db();
  $type = ($type == 1) ? "" : "AND type = '$type'";
  $active = ($active == 0) ? "0,1" : $active;
  $sql->db_Select("news_images","*","newsid = $newsid AND available IN ($active) $type  ORDER BY type,orderby");
  	return execute_multi($sql,1);
}//END OF FUNCTION get_detailed_images

function get_news_next_prev_images($id,$productid) 
{ 
	 $sql = new db();
	 $sql->db_Select("news_images","id","available = 1 AND productid = 1");
}//END OF FUNCTION get_next_prev_images

function get_related_news($id,$lang,$active="no") 
{ 
	$sql = new db();
	$active = ($active == "no") ? "0,1" : $active;
	$tables = "news INNER JOIN news_links ON (news.id = news_links.news_source)";
	$sql->db_Select($tables,"*","news_source = $id AND news.active IN ($active) ORDER BY news_links.orderby");
	if ($sql->db_Rows() > 0)
	{
		$tmp = execute_multi($sql);
		for ($i=0;count($tmp) > $i;$i++)
		{
			$ar[] = get_news($tmp[$i]['news_dest'],$lang);
		}
	}
//echo "SELECT * FROM $tables WHERE news_source = $id AND news.active IN ($active) ORDER BY orderby";
	
  return $ar;
}//END OF FUNCTION get_upselling

function get_featured_news($cat,$type,$lang,$active="no",$limit=0,$orderby=0) 
{ 
	$sql = new db();
	$cat = ($cat) ? $cat : 0;
	$active = ($active == "no") ? "0,1" : $active;
	$limit = ($limit) ? " LIMIT $limit" : "";
	$orderby = ($orderby) ? $orderby : "news_order";
	$tables = "news INNER JOIN news_featured ON (news.id = news_featured.newsid)";
	$sql->db_Select($tables,"newsid,news_order","categoryid = $cat AND type = '$type' AND news.active IN 
	($active) ORDER BY $orderby $limit");
//	echo "SELECT newsid,news_order FROM $tables WHERE categoryid = $cat AND type = '$type' AND news.active IN ($active) ORDER BY $orderby $limit<br>";
	if ($sql->db_Rows() > 0) 
	{  
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
		$r = $sql->db_Fetch();
		$p = get_news($r['newsid'],$lang);
		if ($p !=0) 
		{  
		$ar[$i] = $p;
		$ar[$i]['news_order'] = $r['news_order'];
		}//END OF IF
		}//END OF FOR
	return $ar;
	}//END OF IF
}//END OF FUNCTION get_featured

function get_latest_news($limit,$lang,$active="no",$cat=0)
{
	global $sql;
	$active_sql = ($active == "no") ? "0,1" : $active;
	if ($cat)//latest from a specific category
	{
		$tables = "news INNER JOIN news_page_categories ON (news.id = news_page_categories.newsid)";
		$sql->db_Select($tables,"news.id","catid = $cat AND news.active IN ($active_sql) AND
		news_page_categories.main = 'Y' ORDER BY date_added DESC LIMIT $limit");
//		echo "SELECT news.id FROM news INNER JOIN news_page_categories ON (news.id = news_page_categories.newsid) WHERE catid = $cat AND news.active IN ($active_sql) AND
//		news_page_categories = 'Y' ORDER BY date_added DESC LIMIT $limit";
	}
	else //Latest from ALL categories 
	{
		$sql->db_Select("news","id","news.active IN ($active_sql) ORDER BY date_added DESC LIMIT $limit");
//		echo "SELECT id from news WHERE news.active IN ($active_sql) ORDER BY date_added DESC LIMIT $limit<br>";
	}
	if ($sql->db_Rows()) 
	{
		$tmp = execute_multi($sql);
		for ($i=0;count($tmp) > $i;$i++)
		{
			$news[$i] = get_news($tmp[$i]['id'],$lang,$active);
		}
		return $news;
	}
	else {return 0;}
}

function news_cat_translations($id) 
{ 
	global $smarty,$sql;
$countries = get_countries("Y",$trans="yes");
$class_lng = array();
$translations = array();

for ($i=0,$j=0,$d=0;count($countries) > $i;$i++)
{
	$sql->db_Select("news_categories_lng","*","code = '".$countries[$i]['code']."' AND categoryid = $id");
	if ($sql->db_Rows() == 0) 
	{  
	 	$cat_lng[$j]['code'] =  $countries[$i]['code'];
	 	$cat_lng[$j]['country'] =  $countries[$i]['country'];
	 	$j++;
	}//END OF IF
	else 
	{  
	//see for possible translations
	 	$r = $sql->db_Fetch();
	 	$cat_details = get_lang_name($r['code']);
	 	foreach ($r as $key => $value) 
	 	{
	 	   	if (!preg_match("[0-9]",$key)) 
	 	{
	 		$translations[$d][$key] = $value;
	 	}//END OF IF
	 	}//END OF FOREACH 
	 	$translations[$d]['country'] = $cat_details['country'];
	 	$d++;
	}//END OF ELSE	
}//END OF FOR
$smarty->assign("trans",$translations);//assigned template variable trans  
$smarty->assign("cat_lng",$cat_lng);//assigned template variable product_lng  
}//END OF FUNCTION extra_fields_translations

function get_cat_news($cat,$lang,$active,$num=false) 
{ 
	$active = ($active = 1) ? "1" : "0,1";
	 $sql = new db();
	 $fields = "news.image, news.date_added, news.id, news_lng.title, news_page_categories.catid, news_lng.category";
	 $tables = "news INNER JOIN news_lng ON (news.id = news_lng.newsid)
	  INNER JOIN news_page_categories ON (news.id = news_page_categories.newsid)
	   INNER JOIN news_categories_lng ON (news_page_categories.catid = news_categories_lng.categoryid)";
	 $q = "(news_lng.code = '$lang') AND (news_categories_lng.code = '$lang') AND news_page_categories.catid = $cat AND news.active IN ($active)
	 ORDER BY date_added DESC";

	 $sql->db_Select($tables,$fields,$q);
	 if ($sql->db_Rows() > 0) 
	 {  
	 $a = ($num == false) ? execute_multi($sql,1) : execute_paginated($sql,1,$num);
	 }//END OF IF
	 return $a;
}//END OF FUNCTION get_cat_products

function get_category_news_pages($cat,$table)
{
$tree_parser = new tree_parser;

$tree_parser->load_table($table);
$cat_tree=$tree_parser->select_root_cat($cat,$table);
foreach ($cat_tree as $key => $value)
{
//	echo "$key :: $value<br>";
}
return $cat_tree;
}

function get_news_tags($id,$seperator)
{
	global $sql;
	
	$sql->db_Select("news_tags","tag","newsid = $id");
	$tags = execute_multi($sql,1);
	//simplify the array
	for ($i=0;count($tags) > $i;$i++)
	{
		$t[] = $tags[$i]['tag'];
	}
	return construct_tags_string($t,$seperator);
}

function create_news_folders($catfolder,$folder)
{
				$directory = $_SERVER['DOCUMENT_ROOT'].$folder;
				$directory_cat = $_SERVER['DOCUMENT_ROOT'].$catfolder;
				$directory_thumbs = $directory."/thumbs";
				$directory_main = $directory."/main";
				//Create the user personal folder
				if (!is_dir($directory_cat)) 
				{
					if (mkdir($directory_cat))
					{
					mkdir($directory,0755);
					mkdir($directory_thumbs,0755);
					mkdir($directory_main,0755);
					return true;
					}
					else 
					{
						echo "can't write";
					}
				}
				else 
				{
					mkdir($directory,0755);
					mkdir($directory_thumbs,0755);
					mkdir($directory_main,0755);
					return true;
				}
}

function get_folder_news_images($uid,$folder,$id,$module_settings,$type=0)
{
	global $sql;
	$f = fopen("log.txt","w");
	$thumb_size = $module_settings['settings']['news_images_thumb_width'];
	// create a new Thumbs-Object(str "imagepath", str thumb_prefix, int width, int height [, int fix ])
// last (4th) parameter is optional and desides whether the size of all thumbs is
// limited/fixed by width, height or both (0 = both, 1 = width, 2 = height)
$destination_folder = $_SERVER['DOCUMENT_ROOT'].$folder."/";
$cThumbs = new Thumbs($destination_folder, $module_settings['settings']['thumb_prefix'], $thumb_size, $thumb_size, 0);

// get the array of all found images and thumbs
$mix = $cThumbs->getImages();

if ($mix !=0) 
{
//get user images from DB
$sql->db_Select("news_images","image","newsid = '".$id."'");
if ($sql->db_Rows() > 0) 
{
	$res = execute_multi($sql,0);
for ($i=0;count($res) > $i;$i++)
{
	$tmp1[] = $_SERVER['DOCUMENT_ROOT'].$res[$i]['image'];//copy array for comparison
	$tmp[] = $res[$i]['image'];
}
}

//This should compare the DB to the folder. If there is the image count is different OR if the count is the same but the files are different
//Go ahead and write to the db, else return nothing
if (count($mix[0]) != count($tmp) OR array_diff_assoc($tmp1,$mix[0])) //Check if the user folder has changed and update the db
{

//foreach ($_POST as $k=>$v)
//{
////fwrite($f,"$k --- $v<br>\n\r ");
//}


	//start adding new images
	for ($i=0;count($mix[0]) > $i;$i++)
	{
		$image_details = getimagesize($mix[0][$i]);
		
		$image = str_replace($_SERVER['DOCUMENT_ROOT'],'',$mix[0][$i]);
		$date_added = time();
		$image_x = $image_details[0];
		$image_y = $image_details[1];
		$image_path = $mix[0][$i];
		$thumb = str_replace($_SERVER['DOCUMENT_ROOT'],'',$mix[1][$i]);
		$filesize = filesize($image_path);
		$comments = 0;
		$views = 0;
		if (is_array($tmp)) 
		{
		if (!in_array($image,$tmp)) 
		{
		$sql->db_Insert("news_images","'','$id','$image','$image_path','$thumb','$alt','$title','0','$i','$date_added',
		'$image_x','$image_y','$type'");
		return mysql_insert_id();
		}	
		}
		else 
		{
		$sql->db_Insert("news_images","'','$id','$image','$image_path','$thumb','$alt','$title','0','$i','$date_added',
		'$image_x','$image_y','$type'");
		return mysql_insert_id();
		}
		$q = "INSERT INTO news_images VALUES ('','$id','$image','$image_path','$thumb','$alt','$title','0','$i','$date_added',
		'$image_x','$image_y','$type');\n\r";
		fwrite($f,$q);
	}
}
fclose($f);
}
else {
	return 0;
}
}

function news_images_categories($id=0)
{
	global $sql;
	
	$sql->db_Select("news_images_categories","*","ORDER BY id",$mode="no_where");
	return execute_multi($sql);
}
?>