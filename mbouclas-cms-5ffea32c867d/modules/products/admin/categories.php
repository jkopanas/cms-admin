<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("products",$loaded_modules)) {
	$current_module = $loaded_modules['products'];
	$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$module_path = URL."/".$current_module['folder']."/admin";

$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];

//root categories
if (!$cat) 
{  
 	$cat = 0;
}//END OF IF

$ClsCat = new CategoriesActions(array('module'=>$current_module,'debug'=>0));
$category = (empty($cat)) ? 0 : $Content->ItemCategory($cat,array('table'=>'products_categories','settings'=>1));
$parentid = ($_GET['action']) ? $category['parentid'] : $cat;


##################### ADD NEW CATEGORY ######################3
if ($_POST['action'] == "addcat") 
{
//ADD THE CATEGORY
$ClsCat->AddTreeCategory('products_categories',$_POST,array('return'=>'cat','file'=>'products_category','debug'=>0));
header("Location: $module_path/categories.php?cat=$cat");
exit();
}//END OF NEW CATEGORY
##################### MODIFY EXISTING CATEGORY ######################3
if ($_POST['action'] == "modify") 
{
$data = $_POST;
$data['category'] = $category;
$data['cat'] = $cat;
$ClsCat->ModifyTreeCategory('products_categories',$data,array('debug'=>0));
header("Location: $module_path/categories.php?action=edit&cat=".$cat);
 exit();
}//END MODIFY CATEGORY

if ($_GET['action'] == "edit") 
{
	
	$smarty->assign("category",$category);
	$smarty->assign("edit",1);
	$smarty->assign("action","modify");
	$smarty->assign("more_categories",$Content->ItemTreeCategories($category['parentid'],array('table'=>'products_categories')));
}
elseif ($_GET['action'] == "delete")
{
	 $sql->db_Select("products_categories","categoryid","categoryid_path LIKE '%$cat/%'");
	 if ($sql->db_Rows() > 0) {
	 	$tmp = execute_multi($sql,0);
	 	for ($i=0;count($tmp) > $i;$i++)
	 	{
	 	$sql->db_Delete("products_categories","categoryid=".$tmp[$i]['categoryid']);
//	 	echo "DELETE FROM categories WHERE categoryid=".$tmp[$i]['categoryid'].";<br>";
	 	}
	 }
//	 echo "DELETE FROM categories WHERE categoryid=".$cat.";<br>";
	 $sql->db_Delete("products_categories","categoryid= $cat");
	  header("Location: $module_path/categories.php");
	 exit();
}
$all_categories = $Content->AllItemsTreeCategories();
foreach ($all_categories as $v)
{
	$simplified_categories[] = $v['categoryid'];
}

if ($_GET['mode'] == 'translate') {
	$smarty->assign("mode",$_GET['mode']);
	$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
	for ($i=0;count($availableLanguages) > $i;$i++)
	{
		if ($availableLanguages[$i]['code'])
		{
		$availableLanguages[$i]['item'] = $Languages->itemTranslations($category['categoryid'],$availableLanguages[$i]['code'],$current_module['name'],
		array('groupby'=>'field','debug'=>0,'table'=>$current_module['name']."_categories"));
		}
	}
	$smarty->assign("availableLanguages",$availableLanguages);//WE WANT FRONT END ONLY
	$smarty->assign("currentLanguage",$availableLanguages[0]);//WE WANT FRONT END ONLY
	$smarty->assign("defaultLanguage",$Languages->getDefaultLanguage('default_lang',array('return'=>'array','languageDetails'=>1)));//WE WANT FRONT END ONLY
		
}

$smarty->assign("allcategories",$all_categories);
$smarty->assign("simple_categories",$simplified_categories);
$smarty->assign("nav",$Content->CatNav($cat,array('table'=>'products_categories')));//assigned template variable a
$smarty->assign("cat",$Content->ItemTreeCategories($cat,array('table'=>'products_categories','table_page'=>'products_page_categories','num_items'=>1,'num_subs'=>1,'debug'=>0)));//assigned template variable cat
if ($cat) {
	$smarty->assign('CurrentCat',$category);
}
}//END MODULE IS ACTIVE
else {
	exit();
}
$smarty->assign("featured",$Content->FeaturedContent($cat,array('fields'=>'*','get_results'=>1,'active'=>2,'orderby'=>'orderby','debug'=>0)));

$smarty->assign("themes",get_themes());
$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']." | Categories");
$smarty->assign("include_file",$current_module['folder']."/admin/categories.tpl");
$smarty->display("admin/home.tpl");

?>