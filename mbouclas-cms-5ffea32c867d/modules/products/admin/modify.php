<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("products",$loaded_modules)) {
	$current_module = $loaded_modules[CURRENT_MODULE];
	$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$smarty->assign("CURRENT_MODULE",$current_module);
	$module_path = URL."/".$current_module['folder']."/admin";
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];



########################## UPDATE content ##################################################################################
if ($_POST['FormAction'] == "update") {
	$save = $Content->ModifyItem($_POST);
if (is_array($save)) {//SOMETHING WENT WRONG. RETURN ERROR LIST
	$smarty->assign("error_list",$save);//assigned template variable error_list<BR>
}
else {//ALL IS WELL. PROCEED
header("Location: modify.php?id=".$_POST['id']);
exit();
}
}//END POSTED FORM

################################################## LOAD content ############################################################
$item_settings = array('fields'=>'*','thumb'=>1,'CatNav'=>1,'debug'=>0,'main'=>0,'parse'=>'toForm','GetCommonCategoriesTree'=>1,'returnSimple'=>1);
		if ($current_module['settings']['use_provider']) 
		{
			$smarty->assign("providers",get_providers(DEFAULT_LANG));
			$item_settings['get_provider'] = 1;
		}
		if ($current_module['settings']['use_content_tags']) 
		{
			$item_settings['tags'] = 1;
		}

		if ($loaded_modules['locations']) {//See if there is a location for this item
				$item_settings['location'] = 1;
				$item_settings['getLocation'] = 1;
				$item_settings['getAlias'] = 1;
		}
	$item = $Content->GetItem($id,$item_settings);

	if ($item)
	{
		$all_categories = $Content->AllItemsTreeCategories();
		if (is_array($item['category'])) {
		foreach ($item['category'] as $v)
		{
			$simplified_categories[$v['catid']] = $v['main'];
		}	
		}
		$smarty->assign("allcategories",$all_categories);
		$smarty->assign("simple_categories",$simplified_categories);
		$smarty->assign('nav',$item['nav']);
		$smarty->assign("more_content",$Content->LatestItems(array('results_per_page'=>10,'active'=>2,'get_provider'=>0,'get_user'=>0,'debug'=>0,'just_results'=>1,'location'=>0,'GetCommonCategoriesTree'=>0
		,'categoryid'=>$item['main_category']['catid'])));

		$smarty->assign("id",$id);
		
		$cron = cron_tasks(array('itemid'=>$id));
		if ($cron) {
		$smarty->assign("cron",$cron);
		}
		
		$smarty->assign("item",$item);
		$smarty->assign("action","update");
        //GET EXTRA FIELDS
        $ExtraFields = new ExtraFields(array('module'=>$current_module,'debug'=>0));
        $AllExtraFields = $ExtraFields->GetExtraFields(array('debug'=>0,match_values=>1,'itemid'=>$id,'catids'=>array_flip($simplified_categories),'way'=>'ASC'));
        $smarty->assign("extra_fields",$AllExtraFields);
        $smarty->assign("image_types",get_image_types($current_module['id']));
        
         $allTreeCategories = $Content->ListCommonCategories($current_module['name'],array('type'=>'tree'));
		if ($allTreeCategories) {
			
		
         foreach ($allTreeCategories as $k=>$v)
         {
         	
         	unset($allTreeCategories[$k]);
         	$allTreeCategories[$v['table_name']] = $v;
         	$allTreeCategories[$v['table_name']]['categories'] = $Content->AllCommonTreeCategories($v['id']);
         	foreach ($allTreeCategories[$v['table_name']]['categories'] as $b)
			{
				$simplified_categories[$v['table_name']][] = $b['categoryid'];
			}
         }
         $smarty->assign("allTreeCategories",$allTreeCategories);
         $smarty->assign("simpleTreeCategories",$simplified_categories);
         }
         
         if ($loaded_modules['maps']) {
         	$map = new maps(array('module'=>$current_module));
         	$mapItem = $map->getMapItem($id,array('debug'=>0,'getItem'=>0));
         	if ($mapItem)
         	{
	         	$mapItem['item'] = $item;
				$smarty->assign('mapItemEncoded',json_encode($mapItem));
				$smarty->assign("mapItem",$mapItem);
         	}
         	
         }//END MAPS
         
		if ($loaded_modules['eshop']) 
		{
			$product['details'] = eshop_product_details($id);
			
		}
         if ($loaded_modules['bookings']) {
         	
         	$book= new bookings(array('module'=>$current_module));
		 	$smarty->assign("itemType",$book->getItemType($id,array('extended'=>1)));
		 	$smarty->assign("rentalTypes",$book->getRentalTypes());
		 	$seasons = $book->getSeasons($id,array('getPeriods'=>1,'debug'=>0,'periodSettings'=>array('debug'=>0)));
		 	$smarty->assign("seasons",$seasons);
		 	$smarty->assign("uniqueData",$book->uniqueData($seasons,'seasons'));
         }		
         
         
	}//END OF content found
	else 
	{
		header("Location: search.php"); 
		exit();
	}//END OF ELSE
################################################ END OF LOAD content #########################################

}
$smarty->assign("menu",$content_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","modify");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/products/admin/content_modify.tpl");
$smarty->display("admin/home.tpl");



?>