<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("products",$loaded_modules)) {
	$current_module = $loaded_modules[CURRENT_MODULE];
	$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$smarty->assign("CURRENT_MODULE",$current_module);
	$module_path = URL."/".$current_module['folder']."/admin";
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
	
$item_settings = array('fields'=>'*','thumb'=>1,'CatNav'=>1,'debug'=>0,'main'=>0);
$item = $Content->GetItem($id,$item_settings);
		$all_categories = $Content->AllItemsTreeCategories();
		if (is_array($item['category'])) {
		foreach ($item['category'] as $v)
		{
			$simplified_categories[$v['catid']] = $v['main'];
		}	
		}

		$smarty->assign('nav',$item['nav']);
		$smarty->assign("more_content",$Content->LatestItems(array('results_per_page'=>10,'active'=>2,'get_provider'=>0,'get_user'=>0,'debug'=>0,'just_results'=>1,'location'=>0
		,'categoryid'=>$item['main_category']['catid'])));
		$availableLanguages = $Languages->AvailableLanguages(array('findDefault'=>'default_lang','excludeDefault'=>1));
		for ($i=0;count($availableLanguages) > $i;$i++)
		{
			$availableLanguages[$i]['item'] = $Languages->itemTranslations($item['id'],$availableLanguages[$i]['code'],$current_module['name'],
			array('groupby'=>'field','debug'=>0,'table'=>$current_module['name']));
		}
		$smarty->assign("availableLanguages",$availableLanguages);//WE WANT FRONT END ONLY
		$smarty->assign("currentLanguage",$availableLanguages[0]);//WE WANT FRONT END ONLY
		$smarty->assign("defaultLanguage",$Languages->getDefaultLanguage('default_lang',array('return'=>'array','languageDetails'=>1)));//WE WANT FRONT END ONLY
		
		$smarty->assign("id",$id);
		$smarty->assign("item",$item);

}//END MODULE

$smarty->assign("menu",$content_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","translations");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/products/admin/translations.tpl");
$smarty->display("admin/home.tpl");



?>