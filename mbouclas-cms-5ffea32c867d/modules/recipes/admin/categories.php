<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("recipes",$loaded_modules)) {
	$current_module = $loaded_modules['recipes'];
	$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$module_path = URL."/".$current_module['folder']."/admin";

$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];

//root categories
if (!$cat) 
{  
 	$cat = 0;
}//END OF IF
	
$category = (empty($cat)) ? 0 : recipes_category($cat,array('table'=>'recipes_categories'));
$parentid = ($_GET['action']) ? $category['parentid'] : $cat;

################# QUICK UPDATE #####################
if ($_POST['mode'] == "quick_update") 
{
		foreach ($_POST as $key => $val)
		{
			if (strstr($key,"-")) 
			{
				list($field,$code)=split("-",$key);
				$sql->db_Update("recipes_categories","$field = '$val' WHERE categoryid = $code");
			}
		}
header("Location: $module_path/categories.php?cat=$cat");
exit();						
}
##################### ADD NEW CATEGORY ######################3
if ($_POST['action'] == "addcat") 
{
//ADD THE CATEGORY
recipes_add_tree_category('recipes_categories',$_POST,array('return'=>'cat','file'=>'recipes_category'));
header("Location: $module_path/categories.php?cat=$cat");
exit();
}//END OF NEW CATEGORY
##################### MODIFY EXISTING CATEGORY ######################3
if ($_POST['action'] == "modify") 
{
$data = $_POST;
$data['category'] = $category;
$data['cat'] = $cat;
recipes_modify_tree_category('recipes_categories',$data,array('debug'=>0));
header("Location: $module_path/categories.php?action=edit&cat=".$cat);
 exit();
}//END MODIFY CATEGORY

if ($_GET['action'] == "edit") 
{
	
	$smarty->assign("category",$category);
	$smarty->assign("edit",1);
	$smarty->assign("action","modify");
	$smarty->assign("more_categories",recipes_tree_categories($category['parentid'],array('table'=>'recipes_categories')));
}
elseif ($_GET['action'] == "delete")
{
	 $sql->db_Select("recipes_categories","categoryid","categoryid_path LIKE '%$cat/%'");
	 if ($sql->db_Rows() > 0) {
	 	$tmp = execute_multi($sql,0);
	 	for ($i=0;count($tmp) > $i;$i++)
	 	{
	 	$sql->db_Delete("recipes_categories","categoryid=".$tmp[$i]['categoryid']);
//	 	echo "DELETE FROM categories WHERE categoryid=".$tmp[$i]['categoryid'].";<br>";
	 	}
	 }
//	 echo "DELETE FROM categories WHERE categoryid=".$cat.";<br>";
	 $sql->db_Delete("recipes_categories","categoryid= $cat");
	  header("Location: $module_path/categories.php");
	 exit();
}
$smarty->assign("allcategories",recipes_all_categories(array('table'=>'recipes_categories')));
$smarty->assign("nav",recipes_cat_nav($cat,array('table'=>'recipes_categories')));//assigned template variable a
$smarty->assign("cat",recipes_tree_categories($cat,array('table'=>'recipes_categories','table_page'=>'recipes_page_categories','num_items'=>1,'num_subs'=>1)));//assigned template variable cat

}//END MODULE IS ACTIVE
else {
	exit();
}


$smarty->assign("themes",get_themes());
$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']." | Categories");
$smarty->assign("include_file",$current_module['folder']."/admin/categories.tpl");
$smarty->display("admin/home.tpl");

?>