<?php
include("../../../manage/init.php");//load from manage!!!!
if ($content_module = module_is_active("content",1,1,0)) 
{
	$module_path = URL."/".$content_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",URL."/".$content_module['folder']."/admin");

$cat = ($_GET['cat'] OR $_GET['cat'] == 0) ? $_GET['cat'] : $_POST['cat'];
$category = (empty($cat)) ? 0 : get_content_category($cat,DEFAULT_LANG);
$parentid = ($_GET['action']) ? $category['parentid'] : $cat;

$active = "0,1";
$categoryid = (is_array($category)) ? $category['categoryid'] : 0;
if ($categoryid != 0) 
{
$q = "(content_categories.categoryid_path LIKE '%$categoryid/%' 
 OR `content_categories`.categoryid_path LIKE '%/$categoryid' OR content_categories.categoryid_path = '$categoryid') 
 AND content_page_categories.main = 'Y' AND content.active IN ($active) ";
$tables = "content_page_categories
 INNER JOIN content ON (content_page_categories.contentid=content.id)
 INNER JOIN content_categories ON (content_page_categories.catid=content_categories.categoryid)";
}
else {
$tables = "content_page_categories";
$q = "catid = 0";
}
 $sql->db_Select($tables,"content_page_categories.contentid",$q);

 
$tmp = execute_multi($sql);
 $lang = DEFAULT_LANG;

 for ($i=0;count($tmp) > $i;$i++)
 {
 	$content[] = get_content($tmp[$i]['contentid'],$lang);
 }
$smarty->assign("content",$content);

}

$smarty->assign("allcategories",get_all_content_categories(DEFAULT_LANG));//assigned template variable allcategories
$smarty->assign("MODULE_SETTINGS",$content_module['settings']);
$smarty->assign("menu",$content_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("catid",$cat);
$smarty->assign("parentid",$parentid);
$smarty->assign("cat",get_content_categories($cat,DEFAULT_LANG,0));//assigned template variable cat
$smarty->assign("nav",content_cat_nav($cat,DEFAULT_LANG));//assigned template variable a
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("current_category",get_content_category($cat,DEFAULT_LANG));//assigned template variable current_cat
$smarty->assign("include_file","modules/content/admin/category_content.tpl");
$smarty->display("admin/home.tpl");

?>