<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("recipes",$loaded_modules)) {
	$current_module = $loaded_modules['recipes'];
	$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$module_path = URL."/".$current_module['folder']."/admin";

$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];

//root categories
if (!$cat) 
{  
 	$cat = 0;
}//END OF IF
	

##################### ADD NEW CATEGORY ######################3
if ($_POST) 
{
//UPDATE
	foreach ($_POST as $key=>$v)
	{
		if (strstr($key,"-")) 
		{
			list($field,$id)=split("-",$key);
			$a[$id][] = "$field = '$v'";
		}
	}
foreach ($a as $k=>$v)
{
	$ids[] = implode(",",$v)." WHERE id = $k";
}
foreach ($ids as $v) {
	$sql->db_Update("recipes_conversion_table",$v);
}
//ADD THE CATEGORY
if ($_POST['name']) {
	$new_cat = recipes_add_conversion_table($_POST,array('return'=>1));
}

header("Location: ".e_SELF);
exit();
}//END OF NEW CATEGORY
##################### MODIFY EXISTING CATEGORY ######################3
if ($_POST['action'] == "modify") 
{
$data = $_POST;
$data['category'] = $category;
$data['cat'] = $cat;
recipes_modify_flat_category('recipes_cooking_methods',$data,array('debug'=>0));
header("Location: ".e_SELF."?action=edit&cat=".$cat);
 exit();
}//END MODIFY CATEGORY

if ($_GET['action'] == "edit") 
{
	
	$smarty->assign("category",$category);
	$smarty->assign("edit",1);
	$smarty->assign("action","modify");
	$smarty->assign("more_categories",recipes_flat_categories(array('table'=>'recipes_cooking_methods')));
}
elseif ($_GET['action'] == "delete")
{
	 $sql->db_Delete("recipes_cooking_methods","categoryid= $cat");
	  header("Location: ".e_SELF);
	 exit();
}
	$smarty->assign("units",recipes_conversion_units(array('fields'=>'*','orderby'=>'id')));
	$smarty->assign("base_units",recipes_conversion_units(array('base'=>1,'fields'=>'name,id,catid')));
}//END MODULE IS ACTIVE
else {
	exit();
}


$smarty->assign("themes",get_themes());
$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","units");
$smarty->assign("area","Conversion Units");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']." | Conversion Units");
$smarty->assign("include_file",$current_module['folder']."/admin/units.tpl");
$smarty->display("admin/home.tpl");

?>