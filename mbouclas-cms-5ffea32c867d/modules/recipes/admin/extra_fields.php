<?php
include("../../../manage/init.php");//load from manage!!!!
if (array_key_exists("recipes",$loaded_modules)) {
	$current_module = $loaded_modules['recipes'];
	$smarty->assign("MODULE_FOLDER",$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$module_path = URL."/".$current_module['folder']."/admin";
	
	$smarty->assign("allcategories",recipes_all_categories(array('table'=>'recipes_categories')));
$id = (!empty($_GET['id'])) ? $_GET['id'] : $_POST['id'];
$sql = new db();
$t = new textparse();
if ($_POST['mode'] == "modify") 
{  
	$counter = 0;
	unset($tmp);
		while(list($key,$val)=each($_POST)) 
		{
		if (strstr($key,"-")) 
		{
			list($field,$id)=split("-",$key);
			
				if ($field == 'categoryids') 
				{  
					$extra_categories = $val;
					//first drop the old ones
					$sql->db_Delete("recipes_extra_field_categories","fieldid = '$id'");
 					for ($i=0;count($extra_categories) > $i;$i++)
				 {
				 	if ($extra_categories[$i] != "")
				 	{
				 		$sql->db_Insert("recipes_extra_field_categories","'".$extra_categories[$i]."','$id'");
				 	}
				 	
				 }//END OF FOR
				}//END OF IF
				else //Everything else
				{
					$query = "$field = '$val' WHERE fieldid = $id";
					$sql->db_Update("recipes_extra_fields",$query);
				}
				//Settings

	if (strstr($field,"settings_")) 
	{
		
		list($dump,$f)=split("settings_",$field);
	$tmp[$id][$f] = $val;

		
	}
			
		}//END OF IF
			//Go for categories
			
$counter++;
		}//END OF WHILE
if ($tmp) 
{
foreach ($tmp as $k1 => $v1)
{
	$field_settings = form_settings_string($v1,"###",":::");
	$query = "settings = '$field_settings' WHERE fieldid = $k1";
//	echo $query."<Br>";
	$sql->db_Update("recipes_extra_fields",$query);
	
}

}
}//END OF IF
//New extra field
if (!empty($_POST['fieldnew'])) 
{  
	$sql->db_Insert("recipes_extra_fields","'','".$_POST['activenew']."','".$_POST['typenew']."','".$t->formtpa($_POST['valuenew'])."','$settings','".$_POST['fieldnew']."','".$_POST['fieldnew']."'");
	$fieldid = mysql_insert_id(); 
if ($_POST['newcategoryids']) 
				{  
					$extra_categories = $_POST['newcategoryids'];
					//first drop the old ones
					$sql->db_Delete("recipes_extra_field_categories","fieldid = '$fieldid'");
 					for ($i=0;count($extra_categories) > $i;$i++)
				 {
				 	if ($extra_categories[$i] != "")
				 	{
				 		$sql->db_Insert("recipes_extra_field_categories","'".$extra_categories[$i]."','$fieldid'");
				 	}
				 	
				 }//END OF FOR
				}//END OF IF

header("Location: $PHP_SELF?id=$fieldid");
exit();
}//END OF IF
//Delete extra field
if ($_GET['md'] == "del") 
{
 //delete the field  
 $sql->db_Delete("recipes_extra_fields","fieldid = ".$_GET['fieldid']);
 //delete the values
 $sql->db_Delete("recipes_extra_fields_values","fieldid = ".$_GET['fieldid']);
 //delete categories
 $sql->db_Delete("recipes_extra_field_categories","fieldid = ".$_GET['fieldid']);
 
}//END OF IF


elseif ($_POST['mode'] == "upd") 
{ 

		foreach ($_POST as $key => $val)
		{
		
		if (strstr($key,"-")) 
		{
			list($field,$code)=split("-",$key);
			$query = "$field = '$val' WHERE fieldid = $id AND code = '$code'";

		}//END OF IF
		}//END OF WHILE
$smarty->assign("mode","update");//set mode to update so that smarty will display the translation tables
}//END OF ELSEIF

}//END OF MODULE


$extra_fields = get_extra_fields_recipes(0,DEFAULT_LANG);
$smarty->assign("menu",$recipes_module['name']);
$smarty->assign("submenu","extra_fields");//assigned template variable extra_field
$smarty->assign("fields",$extra_fields);//assigned template variable extra_field
$smarty->assign("form_destination",$form_destination['recipes_new']);//assigned template variable form_destination
$smarty->assign("action_title",$lang['add_recipes']);//assigned template variable action_title
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/recipes/admin/extra_fields.tpl");
$smarty->display("admin/home.tpl");

?>