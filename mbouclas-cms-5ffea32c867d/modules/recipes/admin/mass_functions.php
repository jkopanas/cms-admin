<?php
include("../../../manage/init.php");//load from manage!!!!

if ($recipes_module = module_is_active("recipes",1,1)) 
{
	$module_path = URL."/".$recipes_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",$module_path);
$t = new textparse();
$page = $_POST['page'];
################################################ MASS ACTIVATE/DEACTIVATE ###################################
if ($_POST['mode'] == "deactivate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("recipes","active = 0 WHERE id = $key");				
			}
		}//END OF WHILE

	if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
	
}

if ($_POST['mode'] == "activate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("recipes","active = 1 WHERE id = $key");				
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
}
################################################ END MASS ACTIVATE/DEACTIVATE ###############################

################################################ MASS DELETE ###############################
if ($_POST['mode'] == "delete") 
{
	
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			if ($val == 1)//eliminates the check all/none box 
			{
//	$recipes_details = get_recipes($id,DEFAULT_LANG);

	$categoryid = $recipes_details['catid'];  
//delete the recipes
$sql->db_Delete("recipes","id = $id"); 
//delete translations
$sql->db_Delete("recipes_lng","itemid = $id"); 
//delete recipes links
$sql->db_Delete("recipes_links","recipes_source = $id"); 
$sql->db_Delete("recipes_links","recipes_dest = $id"); 
//delete from any category
$sql->db_Delete("recipes_page_categories","itemid = $id"); 
//delete from any category
$sql->db_Delete("recipes_locations","itemid = $id"); 
//delete from images
$sql->db_Delete("recipes_images","itemid = $id"); 
//delete from featured recipes
$sql->db_Delete("featured_recipes","itemid = $id"); 
//delete from extra fields
$sql->db_Delete("recipes_extra_field_values","itemid = $id"); 
//delete from classes
$sql->db_Delete("recipes_class_recipes","itemid = $id"); 
$sql->db_Delete("recipes_page_cooking_methods","itemid = $id"); 
$sql->db_Delete("recipes_page_difficulty_level","itemid = $id"); 
$sql->db_Delete("recipes_page_courses","itemid = $id"); 
$sql->db_Delete("recipes_page_cuisines","itemid = $id"); 
$sql->db_Delete("recipes_page_seasonal","itemid = $id"); 
$sql->db_Delete("recipes_tags","itemid = $id"); 
$sql->db_Delete("recipes_ingredients","itemid = $id"); 
//delete from bookmars
$sql->db_Delete("bookmars","itemid = $id");
//Delete from feeds
$sql->db_Delete("feeds","itemid = $id");
//Subtract from recipes count
$sql->db_Update("recipes_categories","recipes_count = recipes_count-1 WHERE categoryid = '$categoryid'");
//delete recipe features
//ALL DONE			
$recipes_dir = $_SERVER['DOCUMENT_ROOT'].$recipes_module['settings']['images_path']."/category_$categoryid/recipes_$id/";
recursive_remove_directory($recipes_dir);
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
			exit();
		}
		else {
				header("Location:".$module_path."/$page");
				exit();
			}	
}

################################################ END MASS DELETE ###############################

############################################### MASS FIRST ACTIVATION ##############################3
if ($_POST['mode'] == "first_activate") 
{
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "recipes_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("recipes_lng","title = '$val' WHERE itemid = $code");
							
						}
						else 
						{
							$sql->db_Update("recipes","$field = '$val' WHERE id = $code");
							
						}
						
					}
		}
	
	foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
					$sql->db_Update("recipes","active = 0 WHERE id = $code");
			}
		}
		//clear up
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS EDIT ##############################3
if ($_POST['mode'] == "mass_edit") 
{
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "recipes_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("recipes_lng","title = '$val' WHERE itemid = $code");
							
						}
						elseif ($field == "categoryid")
						{
								//PRODUCT CATEGORIES
								$sql->db_Select("recipes_page_categories","catid","itemid = $code AND main = 'Y'");

								if ($sql->db_Rows() > 0) //UPDATE
								{
									$sql->db_Update("recipes_page_categories","catid = $val WHERE itemid = $code AND main = 'Y'");
								}
								else 
								{
									$sql->db_Insert("recipes_page_categories","'$val','$code','Y'");
								}
						}
						else 
						{
							$sql->db_Update("recipes","$field = '$val' WHERE id = $code");
							
						}
						
					}
		}
	
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS DELETE IMAGES ##############################3
if ($_POST['mode'] == "delete_images") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
						
				list($field,$code)=split("-",$key);
				$sql->db_Select("recipes_images","image,thumb,itemid","id = $key");
 				$a = execute_single($sql);
				$sql->db_Select("recipes_images_aditional","*","imageid = $key");
				$b = execute_multi($sql);
				foreach ($b as $v)
				{
					@unlink($v['image_path']); 
				}
				
 				
 				$sql->db_Delete("recipes_images","id = $key");
 				$sql->db_Delete("recipes_images_aditional","imageid = $key");
//echo $key."<br>";
					}
		}
	
		header("Location:".$module_path."/$page");
		exit();
}
############################################### MASS ACTIVATE IMAGES ##############################3
if ($_POST['mode'] == "activate_all_images") 
{
	$id = $_POST['id'];
	$sql->db_Update("recipes_images","available = 1 WHERE itemid = $id AND type = ".$_POST['type']);
	header("Location:".$module_path."/$page");
	exit();
}

}//END OF LOAD MODULE
?>