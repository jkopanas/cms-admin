<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("recipes",$loaded_modules)) {
	$current_module = $loaded_modules['recipes'];
	$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$module_path = URL."/".$current_module['folder']."/admin";

	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
if ($_GET['ajax_call'])
{
if (!$_POST['id'])// FIRST TIME AJAX CALL. SET THE TITLE TO SOMETHING IN ORDER TO SAVE A DRAFT!
{
$_POST['title'] = "Draft : ".strftime("%d-%m-%Y @ %H:%M");
}
echo save_recipe($_POST);
  print_r($_POST);
exit();
}
########################## UPDATE recipes ##################################################################################
if ($_POST['action'] == "update") {
	

$save = save_recipe($_POST);
if (is_array($save)) {//SOMETHING WENT WRONG. RETURN ERROR LIST
	$smarty->assign("error_list",$save);//assigned template variable error_list<BR>
}
else {//ALL IS WELL. PROCEED
header("Location: modify.php?id=".$_POST['id']);
exit();
}
}//END POSTED FORM

################################################## LOAD recipes ############################################################
	$item = get_recipe(array('active'=>2,'id'=>$id,'get_provider'=> 1,'ingredients'=>1,'main'=>0,'limit'=>0,'thumb'=>1,'difficulty_level'=>1,'cooking_methods'=>1,'debug'=>0));
	if (!empty($item)) 
	{  
	$sql->db_Select("recipes_page_categories","catid","itemid = $id AND main = 'N'");
	$all_recipes_categories = execute_multi($sql);
	$smarty->assign("units",recipes_conversion_units(array('fields'=>'id,name')));
	$smarty->assign("categoryids",$all_recipes_categories);//assigned template variable categories
	$smarty->assign("allcategories",recipes_all_categories(array('table'=>'recipes_categories')));
	$smarty->assign("cooking_methods",recipes_flat_categories(array('table'=>'recipes_cooking_methods','debug'=>0)));
	$smarty->assign("difficulty_level",recipes_flat_categories(array('table'=>'recipes_difficulty_level','debug'=>0)));
	$smarty->assign("base_units",recipes_conversion_units(array('base'=>1,'fields'=>'name,id')));
	$smarty->assign("nav",recipes_cat_nav($cat,array('table'=>'recipes_categories')));//assigned template variable a
	$smarty->assign("cat",recipes_tree_categories($cat,array('table'=>'recipes_categories','table_page'=>'recipes_page_categories','num_items'=>1,'num_subs'=>1)));//assigned template variable cat
//	$smarty->assign("more_recipes",get_latest_recipes($recipes_module['settings']['latest_recipes'],DEFAULT_LANG,"no",$recipes['catid']));
	$smarty->assign("extra_fields",get_extra_fields_recipes("Y",$id,$recipes['catid']));
	if ($recipes_module['settings']['use_provider']) {
		$recipes['provider'] = get_provider_id($id,'recipes');
	}
	$smarty->assign("id",$id);
	$smarty->assign("tags",get_recipes_tags($id,","));
	$cron = cron_tasks(array('itemid'=>$id));
	if ($cron) {
		$smarty->assign("cron",$cron);
	}
	$activation['date'] = strftime('%Y',$recipes['date_added'])."-".strftime('%m',$recipes['date_added'])."-".strftime('%d',$recipes['date_added']);
	$activation['time'] = strftime('%H',$recipes['date_added']).":".strftime('%M',$recipes['date_added']);
	$activation['timestamp'] = $cron['date'];
	

	$smarty->assign("activation",$activation);
	
	editor("description","description1",0,0,0,$editor_settings);
	editor("long","long1",0,0,0,$editor_settings);
	$smarty->assign("item",$item);
	$smarty->assign("recipes_module",$recipes_module);
	$smarty->assign("action","update");
	
	
	}//END OF recipes found
	else 
	{  
		header("Location: search.php"); 
		exit();
	}//END OF ELSE

################################################ END OF LOAD recipes #########################################


}

$smarty->assign("more_recipes",latest_recipes(array('results_per_page'=>30,'active'=>2,'get_provider'=>0,'get_user'=>0,'debug'=>0,'just_results'=>1)));
$smarty->assign("providers",get_providers(DEFAULT_LANG));
$smarty->assign("image_types",get_image_types(1));
$smarty->assign("menu",$recipes_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","modify");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']." | Modify Recipe #$id");
$smarty->assign("include_file",$current_module['folder']."/admin/content_modify.tpl");
$smarty->display("admin/home.tpl");

?>