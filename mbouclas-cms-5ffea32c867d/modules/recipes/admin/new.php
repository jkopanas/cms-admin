<?php
include("../../../manage/init.php");//load from manage!!!!


if (array_key_exists("recipes",$loaded_modules)) {
	$current_module = $loaded_modules['recipes'];
	$smarty->assign("MODULE_FOLDER",$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$module_path = URL."/".$current_module['folder']."/admin";
if ($_GET['ajax_call'])
{
if (!$_POST['title'])// FIRST TIME AJAX CALL. SET THE TITLE TO SOMETHING IN ORDER TO SAVE A DRAFT!
{
$_POST['title'] = "Draft : ".strftime("%d-%m-%Y @ %H:%M");

}
//print_r($_POST);
$save = save_recipe($_POST);
if (is_array($save)) {
// 	print_r($save);
 }
 else {
  	 echo $save;
  }

exit();
}
########################## UPDATE content ##################################################################################
if ($_POST['action'] == "add") 
{
$save = save_recipe($_POST);
if (is_array($save)) {//SOMETHING WENT WRONG. RETURN ERROR LIST
	$smarty->assign("error_list",$save);//assigned template variable error_list<BR>
}
else {
header("Location: modify.php?id=$id");
exit();

}//END OK
}//END ADD	
	
$smarty->assign("extra_fields",get_extra_fields_recipes("Y",$id,$recipes['catid']));
$smarty->assign("units",recipes_conversion_units(array('fields'=>'id,name')));
$smarty->assign("base_units",recipes_conversion_units(array('base'=>1,'fields'=>'name,id')));
$smarty->assign("allcategories",recipes_all_categories(array('table'=>'recipes_categories')));
	$smarty->assign("cooking_methods",recipes_flat_categories(array('table'=>'recipes_cooking_methods','debug'=>0)));
	$smarty->assign("difficulty_level",recipes_flat_categories(array('table'=>'recipes_difficulty_level','debug'=>0)));
$smarty->assign("nav",recipes_cat_nav($cat,array('table'=>'recipes_categories')));//assigned template variable a
$smarty->assign("cat",recipes_tree_categories($cat,array('table'=>'recipes_categories','table_page'=>'recipes_page_categories','num_items'=>1,'num_subs'=>1)));//assigned template variable cat
}//END ACTIVE MODULE






$smarty->assign("calendar",1);
$smarty->assign("providers",get_providers(DEFAULT_LANG));
$smarty->assign("menu",$content_module['name']);
$smarty->assign("submenu","add");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']." | Add New Recipe");
$smarty->assign("include_file",$current_module['folder']."/admin/content_modify.tpl");
$smarty->display("admin/home.tpl");

?>