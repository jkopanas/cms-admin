<?php
session_start();
//make sure that it is a GET/POST data and not SESSION
$check_search = ($_POST['search']) ? $_POST['search'] : $_GET['search'];
if (empty($check_search))//IF SESSION data and not GET/POST it is a new search and should be cleared 
{  
$_SESSION['data'] = ""; 
}//END OF IF

include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("recipes",$loaded_modules)) {
	$current_module = $loaded_modules['recipes'];
	$smarty->assign("MODULE_FOLDER", URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$module_path = URL."/".$current_module['folder']."/admin";

####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ##########################################
//recipes_r($_SESSION);
	
//	recipes_r($_SESSION);
//user is posting variables for a new search


	
	
	
$smarty->assign("extra_fields",get_recipes_extra_fields("Y",DEFAULT_LANG));//assigned template variable extra_field
	

}

if ($_POST['search']) 
{  
 $posted_data = array();//clear array
 $_SESSION['search_data'] = "";//clear the session to add the new data
 foreach ($_POST as $key => $value) //build the array that will hold the search data
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 $_SESSION['search_data'] = $posted_data;//setup the session
 $smarty->assign("search",1);
}//END OF IF
else//Pagination or refresh
{  
$posted_data = array();//setup a a clean array
$posted_data = $_SESSION['search_data'];//assign data to it
if ($_GET['page']) {$posted_data['page'] = $page;}
}

if ($_GET['tag']) 
{
 $posted_data = $_GET;//clear array
 $_SESSION['search_data'] = "";//clear the session to add the new data
  $_SESSION['search_data'] = $posted_data;//setup the session
 $smarty->assign("search",1);
}
if ($_GET['provider_id']) 
{
 $posted_data = $_GET;//clear array
 $_SESSION['search_data'] = "";//clear the session to add the new data
  $_SESSION['search_data'] = $posted_data;//setup the session
 $smarty->assign("search",1);
 $smarty->assign("provider_id",$_GET['provider_id']);//NO CATEGORY THUS SET THE AREA FOR THE PAGINATOR
 $smarty->assign("area",'author_'.$_GET['provider_id']);
 $smarty->assign("provider",get_providers(FRONT_LANG,$_GET['provider_id']));
}

if ($_GET['efield'] AND $_GET['recipesid']) 
{
 $posted_data = $_GET;//clear array
 $_SESSION['search_data'] = "";//clear the session to add the new data
  $_SESSION['search_data'] = $posted_data;//setup the session
 $smarty->assign("search",1);
}

if ($locations_module = module_is_active("locations",1,1)) {
	$smarty->assign("locations_module",$locations_module);
	$smarty->assign("all_locations",get_all_locations_categories(DEFAULT_LANG));
}

//$posted_data = ($_POST) ? $_POST : $_GET;
if ($_REQUEST) 
{

	  $res = recipes_search($posted_data,$current_module,$settings['page'],$settings['debug']);
}


$smarty->assign("module",'recipes');
$smarty->assign("data",$res['data']);
$smarty->assign("results",$res['results']);
//recipes_r(get_recipes(65,DEFAULT_LANG));
$smarty->assign("allcategories",recipes_all_categories(array('table'=>'recipes_categories')));
$smarty->assign("menu",$recipes_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("MODULE_SETTINGS",$recipes_module['settings']);
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/recipes/search_form.tpl");
$smarty->display("admin/home.tpl");

?>