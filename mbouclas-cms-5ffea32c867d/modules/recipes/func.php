<?php
function recipes_tree_categories($cat=0,$settings)
{
	global $sql;
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$orderby = ($settings['orderby']) ? $settings['orderby'] : "orderby";
	$way = ($settings['way']) ? $settings['way'] : "ASC";
	$active = $settings['active'];
	$table = $settings['table_page'];
	$table_cat = $settings['table'];
		 
	$sql->db_Select($settings['table'],$fields,"parentid = $cat ORDER BY $orderby $limit_q");
	if ($settings['debug']) {
		echo "SELECT $fields FROM $table_cat WHERE parentid = $cat ORDER BY $orderby $way $limit_q";
	}
	if ($sql->db_Rows()) {
		$categories = execute_multi($sql,1);
	
	 for ($i=0;count($categories) > $i;$i++)
	 {
		 $categoryid = $categories[$i]['categoryid'];
		
		 if ($settings['get_subs']) 
		 {
		 	$categories[$i]['subcategories'] = recipes_tree_categories($categoryid,array($settings));
		 }
		 
		 if ($settings['num_items']) {

		 $sql->db_Select("$table
		 INNER JOIN recipes ON ($table.itemid=recipes.id)
		 INNER JOIN $table_cat ON ($table.catid=$table_cat.categoryid)",
		 "$table.itemid","($table_cat.categoryid_path LIKE '%$categoryid/%' 
		 OR `$table_cat`.categoryid_path LIKE '%/$categoryid' OR $table_cat.categoryid_path = '$categoryid') 
		 AND recipes.active IN ($active) ");
			if ($settings['debug']) {
				echo "<br>SELECT $table.itemid FROM $table
		 INNER JOIN recipes ON ($table.itemid=recipes.id)
		 INNER JOIN $table_cat ON ($table.catid=$table_cat.categoryid) WHERE $table.itemid","($table_cat.categoryid_path LIKE '%$categoryid/%' 
		 OR `$table_cat`.categoryid_path LIKE '%/$categoryid' OR $table_cat.categoryid_path = '$categoryid') 
		 AND recipes.active IN ($active)";				
			}
		 $categories[$i]['num_items'] = $sql->db_Rows();
		}//END NUM ITEMS
		if ($settings['num_subs']) {
			 $sql->db_Select("$table_cat","categoryid","parentid = $categoryid");
				if ($settings['debug']) {
					echo "<Br>SELECT categoryid FROM $table_cat WHERE parentid = $categoryid";
				}
			 $categories[$i]['num_sub'] = $sql->db_Rows();
		}
		 
		 //get settings 
		 $categories[$i]['settings_array'] = form_settings_array($categories[$i]['settings'],"###",":::");
		 
	 }//END OF FOR
	 return $categories;
	}//END ROWS
 
}//END FUNCTION

function recipes_flat_categories($settings)
{
	global $sql;
	
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$active = $settings['active'];
	$table = $settings['table_page'];
	$table_cat = $settings['table'];
		 
	$sql->db_Select($settings['table'],$fields,"ORDER BY orderby $limit_q",$mode="no_where");
	if ($settings['debug']) {
		echo "SELECT $fields FROM $table_cat ORDER BY orderby $limit_q";
	}
	if ($sql->db_Rows()) {
		$categories = execute_multi($sql,1);
	
	 for ($i=0;count($categories) > $i;$i++)
	 {
		 $categoryid = $categories[$i]['categoryid'];
		
	 
		 if ($settings['num_items']) {

		 $sql->db_Select("$table
		 INNER JOIN recipes ON ($table.itemid=recipes.id)
		 INNER JOIN $table_cat ON ($table.catid=$table_cat.categoryid)",
		 "$table.itemid","recipes.active IN ($active) ");
			if ($settings['debug']) {
				echo "<br>SELECT $table.itemid FROM $table
		 INNER JOIN recipes ON ($table.itemid=recipes.id)
		 AND recipes.active IN ($active)";				
			}
		 $categories[$i]['num_items'] = $sql->db_Rows();
		}//END NUM ITEMS
		 
		 //get settings 
		 if ($settings['settings']) {
		 	 $categories[$i]['settings_array'] = form_settings_array($categories[$i]['settings'],"###",":::");
		 }
		
		 
	 }//END OF FOR
	 return $categories;
	}//END ROWS
}//END FUNCTION

function recipes_flat_category_items($settings)
{
	global $sql;
	//Check for the item first
	$sql->db_Select($settings['page_table'],'catid','itemid = '.$settings['itemid']);
	if ($sql->db_Rows()) {
		if ($settings['multi']) {
			$cat = execute_multi($sql);
			for ($i=0;count($cat)>$i;$i++)
			{
				$a[] = recipes_flat_category($cat[$i]['catid'],array('table'=>$settings['table'],'debug'=>0));
			}
			return $a;
		}
		else {
			$cat = execute_single($sql);
			return recipes_flat_category($cat['catid'],array('table'=>$settings['table'],'debug'=>0));
		}

	}
}

function recipes_add_flat_category($table,$data,$settings=0)
{
	global $sql,$loaded_modules;
	$t = new textparse();
	$new_category = $t->formtpa($data['title']);
	$new_cat = recipes_category($new_parentid,array('table'=>$table));
	$new_description = $t->formtpa($data['description']);
	$new_date_added = time();
	$new_orderby = $data['orderby'];
	$new_active = $data['active'];
	$new_alias = $t->formtpa(str_replace(" ","_",$data['alias']));
	$image = $data['image'];
	//Check out for settings
$tmp = array();
foreach ($data as $k => $v)
{
	if (strstr($k,"settings_")) 
	{
		list($dump,$field)=split("settings_",$k);
//		echo "$field --- $v<br>";
if ($v) 
{
	$tmp[$field] = $v;
}
		
	}
	
}
if ($tmp) 
{
	$category_settings = form_settings_string($tmp,"###",":::");
}
	
	$sql->db_Insert($table,"'','$new_category','$new_description','$image','$new_orderby','$new_date_added','$new_active','$new_alias','$category_settings'");
	$new_categoryid = mysql_insert_id();
	if ($data['debug']) {
		echo "INSERT INTO $table VALUES ('','$new_category','$new_parentid','','$new_description','$new_alias','$new_orderby','$new_date_added','$category_settings')<br>";
	}

if ($settings['return'] == 'cat') {
	return $new_categoryid;
}
}//END FUNCTION

function recipes_modify_flat_category($table,$data,$settings=0)
{
	global $sql,$loaded_modules;
	$old_cat = $data['category'];
	$t = new textparse();
//Check out for settings
$tmp = array();
foreach ($data as $k => $v)
{
	if (strstr($k,"settings_")) 
	{
		list($dump,$field)=split("settings_",$k);
//		echo "$field --- $v<br>";
		$tmp[$field] = $v;
	}
}
if ($tmp) 
{
	$category_settings = form_settings_string($tmp,"###",":::");
}
$category = $t->formtpa($data['title']);
$description = $t->formtpa($data['description']);
$image = $t->formtpa($data['image']);
$alias = $t->formtpa(str_replace(" ","_",$data['alias']));
//now modify the details of the given category
$sql->db_Update($table,"alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', 
orderby = '".$data['orderby']."', image = '".$image."' WHERE categoryid = ".$data['cat']);
if ($settings['debug']) {
	echo "UPDATE $table SET  alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', categoryid_path = '$new_catid_path', 
order_by = '".$data['orderby']."' WHERE categoryid = ".$data['cat'];
}

}//END FUNCTION

function recipes_flat_category($cat,$settings)
{
	global $sql,$loaded_modules;
  $sql->db_Select($settings['table'],"*",$settings['table'].".categoryid = $cat");
  if ($settings['debug'] == 1) {
  	echo "SELECT * FROM ".$settings['table']." WHERE ".$settings['table'].".categoryid = $cat<Br>";
  }
	if ($sql->db_Rows()) 
	{
		 $a = execute_single($sql,1);
		 if ($settings['settings']) 
		 {
		 		
		 		$a['settings'] = form_settings_array($a['settings'],"###",":::");
		 }
	}
 return $a;
	
}


function recipes_category($cat,$settings) 
{ 
  global $sql;
  $sql->db_Select($settings['table'],"*",$settings['table'].".categoryid = $cat");
  if ($settings['debug'] == 1) {
  	echo "SELECT * FROM ".$settings['table']." WHERE ".$settings['table'].".categoryid = $cat<Br>";
  }
	if ($sql->db_Rows()) 
	{
		 $a = execute_single($sql,1);
		 if ($settings['settings']) 
		 {
		 		
		 		$a['settings'] = form_settings_array($a['settings'],"###",":::");
		 }
	}
 return $a;
}//END OF FUNCTION get_category

function all_recipes_categories($settings)
{
	global $exclude,$sql,$depth;

$nav_query = MYSQL_QUERY("SELECT categoryid,parentid,category FROM ".$settings['table']." ORDER BY `categoryid`");
if ($settings['debug']) {
	echo "SELECT categoryid,parentid,category FROM ".$settings['table']." ORDER BY `categoryid`<br>";
}
$tree = "";                         // Clear the directory tree
$depth = 1;                         // Child level depth.
$top_level_on = 1;               // What top-level category are we on?
$exclude = ARRAY();               // Define the exclusion array
ARRAY_PUSH($exclude, 0);     // Put a starting value in it
 
WHILE ( $nav_row = MYSQL_FETCH_ARRAY($nav_query) )
{
     $goOn = 1;               // Resets variable to allow us to continue building out the tree.
     FOR($x = 0; $x < COUNT($exclude); $x++ )          // Check to see if the new item has been used
     {
          IF ( $exclude[$x] == $nav_row['categoryid'] )
          {
               $goOn = 0;
               BREAK;                    // Stop looking b/c we already found that it's in the exclusion list and we can't continue to process this node
          }
     }
     IF ( $goOn == 1 )
     {
          ARRAY_PUSH($exclude, $nav_row['categoryid']);          // Add to the exclusion list
          IF ( $nav_row['category_id'] < 18 )
          { $top_level_on = $nav_row['categoryid']; }
          $a[] = $nav_row;
 		  $tree .= $nav_row['category'];
          $tree .= recipes_recurse_child($nav_row['categoryid'],$exclude,$settings);          // Start the recursive function of building the child tree
          $a[] = recipes_recurse_child($nav_row['categoryid'],$exclude,$settings); 
     }
}

return $tree;
}

FUNCTION recipes_recurse_child($oldID,$exclude,$settings)               // Recursive function to get all of the children...unlimited depth
{
     GLOBAL $exclude, $depth,$sql;               // Refer to the global array defined at the top of this script
     $child_query = MYSQL_QUERY("SELECT * FROM ".$settings['table']." WHERE parentid=" . $oldID);
     if ($settings['debug']) {
     	echo "<br>SELECT * FROM ".$settings['table']." WHERE parentid=" . $oldID;
     }
     WHILE ( $child = MYSQL_FETCH_ARRAY($child_query) )
     {

          IF ( $child['categoryid'] != $child['parentid'] )
          {
               $depth++;          // Incriment depth b/c we're building this child's child tree  (complicated yet???)
               $tempTree .= "/".$child['category'].recipes_recurse_child($child['categoryid'],$exclude,$settings);          // Add to the temporary local tree
               $t = $child;
               $depth--;          // Decrement depth b/c we're done building the child's child tree.
               if (is_array($exclude)) {
               	ARRAY_PUSH($exclude, $child['categoryid'],$exclude);               // Add the item to the exclusion list
               }
          }
     }
 
     RETURN $tempTree;          // Return the entire child tree
//return $t;
}

function recipes_all_categories($settings)
{
	global $sql;
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	$sql->db_Select($settings['table'],$fields, "ORDER BY categoryid_path",$mode="no_where");
	if ($sql->db_Rows() > 0) 
	{  
		$categories = execute_multi($sql);
		for ($i=0;count($categories) > $i;$i++)
		{
			$categories[$i]['category'] = construct_recipes_category($categories[$i]['categoryid_path'],$settings['table']);
		}//END OF FOR
		sort($categories);
	return $categories;	
	}//END OF IF
}//END OF FUNCTION

function construct_recipes_category($path,$table) 
{ 
	global $sql;
	if (strstr($path,"/")) 
	{  
	$tmp = explode("/",$path);
	$ids = implode(",",$tmp);
	}//END OF IF
	else 
	{  
	$ids = $path; 
	}//END OF ELSE
$sql->db_Select($table,"category","categoryid IN ($ids)");
if ($sql->db_Rows()) {
	$a = execute_multi($sql);
	for ($i=0;count($a) > $i;$i++)
	{
		$ar[] = $a[$i]['category'];
	}
	 if (count($ar) > 0) 
	 {  
	 	$return = implode("/",$ar); 
	 }//END OF IF
	 else 
	 {  
	 	$return = $r['category']; 
	 }//END OF ELSE

	 return $return;
}//END ROWS

}//END OF FUNCTION construct_category



function get_recipes_cat($settings)
{
	global $sql;
	$table = ($settings['table']) ? $settings['table'] : "recipes";
	$id = $settings['id'];
	$sql->db_Select($table."_page_categories","catid","itemid = $id");
//	echo "SELECT catid FROM ".$table."_page_categories WHERE itemid = $id<br>";
	if ($settings['cat_details']) {
		$cat = execute_multi($sql);
		for ($i=0;count($cat) >$i;$i++)
		{
			$a[] = recipes_category($cat[$i]['catid'],array('table'=>$table."_categories",'settings'=>$settings['form_settings'],'debug'=>0));
		}
		return $a;
	}
	else {
		return execute_multi($sql);
	}
//	echo "<Br>SELECT catid FROM page_categories WHERE productid = $id AND main ='Y'<br>";
	
}


function recipes_add_tree_category($table,$data,$settings=0)
{
	global $sql,$loaded_modules;
	$t = new textparse();
	$new_category = $t->formtpa($data['title']);
	$new_parentid = $data['parent_catid'];
	$new_cat = recipes_category($new_parentid,array('table'=>$table));
	$new_description = $t->formtpa($data['desc']);
	$new_date_added = time();
	$new_orderby = $data['orderby'];
	$new_alias = $t->formtpa(str_replace(" ","_",$data['alias']));
	//Check out for settings
$tmp = array();
foreach ($data as $k => $v)
{
	if (strstr($k,"settings_")) 
	{
		list($dump,$field)=split("settings_",$k);
//		echo "$field --- $v<br>";
if ($v) 
{
	$tmp[$field] = $v;
}
		
	}
	
	
}
if ($tmp) 
{
	$category_settings = form_settings_string($tmp,"###",":::");
}
	
	$sql->db_Insert($table,"'','$new_category','$new_parentid','','$new_description','$new_alias','$new_orderby','$new_date_added','$category_settings'");
	$new_categoryid = mysql_insert_id();
	$new_categoryid_path = ($new_cat == 0) ? $new_categoryid : $new_cat['categoryid_path']."/".$new_categoryid;
	$sql->db_Update($table,"categoryid_path = '$new_categoryid_path' WHERE categoryid = '$new_categoryid'");
	if ($data['debug']) {
		echo "INSERT INTO $table VALUES ('','$new_category','$new_parentid','','$new_description','$new_alias','$new_orderby','$new_date_added','$category_settings')<br>";
	}

if ($settings['return'] == 'cat') {
	return $new_categoryid;
}

}//END FUNCTION


function recipes_modify_tree_category($table,$data,$settings=0)
{
	global $sql,$loaded_modules;
	$old_cat = $data['category'];
	$t = new textparse();
$parentid = $data['parent_catid'];
if ($parentid != 0) 
{  
$parent_details = recipes_category($parentid,array('table'=>$table));
$new_catid_path  = $parent_details['categoryid_path']."/".$data['cat'];
}//END OF IF
else 
{
$new_catid_path = $data['cat'];	
}

//Check out for settings
$tmp = array();
foreach ($data as $k => $v)
{
	if (strstr($k,"settings_")) 
	{
		list($dump,$field)=split("settings_",$k);
//		echo "$field --- $v<br>";
		$tmp[$field] = $v;
	}
	
	
}
if ($tmp) 
{
	$category_settings = form_settings_string($tmp,"###",":::");
}
$category = $t->formtpa($data['title']);
$description = $t->formtpa($data['description']);
$alias = $t->formtpa(str_replace(" ","_",$data['alias']));
//now modify the details of the given category
$sql->db_Update($table,"parentid = '$parentid', alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', categoryid_path = '$new_catid_path', 
order_by = '".$data['orderby']."' WHERE categoryid = ".$data['cat']);
if ($settings['debug']) {
	echo "UPDATE $table SET parentid = '$parentid', alias = '$alias', settings = '$category_settings',category = '$category',description = '$description', categoryid_path = '$new_catid_path', 
order_by = '".$data['orderby']."' WHERE categoryid = ".$data['cat'];
}
//rearange subcategory paths
$children = recipes_tree_categories($data['cat'],array('table'=>$table));
for ($i=0;count($children) > $i;$i++)
{
$child_id = $children[$i]['categoryid'];
$new_cat_path = $new_catid_path."/$child_id";
$sql->db_Update($table,"categoryid_path = '$new_cat_path' WHERE categoryid = '$child_id'");
}//END OF FOR


}//END FUNCTION

function recipes_cat_nav($cat,$settings) 
{ 
	global $sql;
	$sql->db_Select($settings['table'],"categoryid_path","categoryid = $cat") ;
	if ($settings['debug']) {
		echo "SELECT categoryid_path FROM ".$settings['table']." WHERE categoryid = $cat<br>";
	}
	if ($sql->db_Rows()) {
		$r =execute_single($sql);
		$tmp = explode("/",$r['categoryid_path']);
		
		if (is_array($tmp) and count($tmp) > 0) 
		{
	 	for ($i=0;count($tmp) > $i;$i++)
		{
//			echo "SELECT $fields FROM $tables WHERE $table.categoryid = ".$tmp[$i]." AND code = '$lang'<br>";
			$ar[] = recipes_category($tmp[$i],array('table'=>$settings['table']));
		}//END OF FOR
	return $ar;
	}//END OF IF subcategories found
	}
}//END OF FUNCTION cat_nav



function recipes_search($posted_data,$settings,$page=0,$query=0)
{
	global $sql,$smarty;
//print_r($posted_data);
	$results['data'] = $posted_data;
	$included_tables = array();
############### SETUP QUERY ##############################
$search_tables[] = "recipes ";
############# SKU ######################
if ($posted_data['id'] != "") 
{  
 $search_condition[] = " recipes.id = '".$posted_data['id'];
}//END OF IF
################## CATEGORIES ####################
if ($posted_data['categories'] AND is_array($posted_data['categories'])) {
	foreach ($posted_data['categories'] as $v) {
		$m[] = $v['categoryid'];
	}
	 $search_condition[] = " recipes_page_categories.catid IN (".implode(",",$m).")";
	$search_tables[] = "INNER JOIN recipes_page_categories ON (recipes.id = recipes_page_categories.itemid)";
	$included_tables[] = 'recipes_page_categories';
}//END MULTIPLE CATEGORIES
if ($posted_data['categoryid'] != "%" AND $posted_data['categoryid']) 
{
if (!empty($posted_data["search_in_subcategories"])) 
{
	
# Search also in all subcategories
	$sql->db_Select("recipes_categories","categoryid_path","categoryid = '".$posted_data["categoryid"]."'");
	$r = $sql->db_Fetch();
	$categoryid_path = $r['categoryid_path'];
	$sql->db_Select("recipes_categories","categoryid","categoryid = '".$posted_data["categoryid"]."' OR categoryid_path LIKE '$categoryid_path/%'");
	$categoryids_tmp = execute_multi($sql,0);	
	
	if (is_array($categoryids_tmp) && !empty($categoryids_tmp)) 
	{
		foreach ($categoryids_tmp as $k=>$v) 
		{
		$categoryids[] = $v["categoryid"];
		}//END OF FOREACH

		$search_condition[] = "recipes_page_categories.catid $category_sign IN (".implode(",", $categoryids).")";
	}//END OF IF
}//END OF IF
else //Search in top category only
  {  
 $search_condition[] = "recipes_page_categories.catid = ".$posted_data['categoryid'];
  }//END OF ELSE  
  
$search_tables[] = "INNER JOIN recipes_page_categories ON (recipes.id = recipes_page_categories.itemid)";
$included_tables[] = 'recipes_page_categories';
}//END OF IF

##################### PROVIDER ###########3
if ($posted_data['provider_id'] != "%" AND $posted_data['provider_id']) 
{
$search_tables[] = "INNER JOIN provider_items ON (recipes.id = provider_items.itemid)";
$search_condition[] = " provider_items.type = 'recipes' AND provider_items.uid =".$posted_data['provider_id'];
$included_tables[] = 'provider_items';
}
if ($posted_data['user'] != "%" AND $posted_data['user']) 
{
//First get the userid
$sql->db_Select("users","id","uname = '".$posted_data['user']."'");
$a =execute_single($sql);
$search_tables[] = "INNER JOIN provider_items ON (recipes.id = provider_items.itemid)";
$search_condition[] = " provider_items.type = 'recipes' AND provider_items.uid =".$a['id'];
$included_tables[] = 'provider_items';	

}

############## PATTERN SEARCH ################
if (!empty($posted_data["substring"])) 
{
$posted_data['substring'] = trim($posted_data['substring']);
if ($posted_data['search_main'] != "") 
{  
 $condition[] = "recipes.title LIKE '%".$posted_data["substring"]."%'";
}//END OF IF
if ($posted_data['search_short'] != "") 
{  
 $condition[] = "recipes.description LIKE '%".$posted_data["substring"]."%'";
}//END OF IF
if ($posted_data['search_long'] != "") 
{  
 $condition[] = "recipes.description_long LIKE '%".$posted_data["substring"]."%'";
}//END OF IF

############### KEYWORDS AS TAGS ##################
if ($posted_data['search_tags']) 
{
	$keywords = explode(" ",$posted_data['substring']);
	foreach ($keywords as $v)
	{
		$tag[] = "'".$v."'";
	}
	$this_join = ($posted_data['search_main'] OR $posted_data['search_short'] OR $posted_data['search_long']) ? "LEFT OUTER JOIN" : "INNER JOIN";
	$search_tables[] = " $this_join recipes_tags ON (recipes.id = recipes_tags.itemid) ";
	$condition[] = " recipes_tags.tag IN (".implode(",",$tag).") ";
	$included_tables[] = 'recipes_tags';
}

 foreach ($posted_data as $key => $value) 
 {   
	if (strstr($key,"efield-")) 
	{
		$condition[] = "recipes_extra_field_values.value LIKE '%".$posted_data['substring']."%'";
	}//END OF IF
 }//END OF FOREACH

if (!empty($condition))
{ 
	if ($posted_data['efields']) 
	{
		$search_tables[] = "LEFT OUTER JOIN recipes_extra_field_values ON (recipes.id = recipes_extra_field_values.itemid)";
		$included_tables[] = 'recipes_extra_fields';
		$included_tables[] = 'recipes_extra_field_values';
	}

	$search_condition[] = "(".implode(" OR ", $condition).")";
}//END OF IF
}//END OF PATERN SEARCH

################### SINGLE TAG ######################
if ($posted_data['tag']) 
{
	$tag = recipes_lookup_tag($posted_data['tag']);
	$sql->db_Select("recipes_tags","itemid","tag = '".$tag['tag']."'");
	$ts = execute_multi($sql);
	foreach ($ts as $v) {
		$m[] = $v['itemid'];
	}
	$search_condition[] = " recipes.id IN (".implode(",",$m).")";
}
########### MULTIPLE SELECTED TAGS ###################
if ($posted_data['tags']) 
{
	foreach ($posted_data['tags'] as $v)
	{
		unset($tmp);
		$tmp = content_lookup_tag($v);
		$tag[] = "'".$tmp['tag']."'";
		$data_tag[] = $tmp['tag'];
	}
	
	$search_tables[] = "INNER JOIN recipes_tags ON (recipes.id = recipes_tags.itemid)";
	$search_condition[] = " recipes_tags.tag IN (".implode(",",$tag).") AND";
	$results['data']['substring'] = implode(" ",$data_tag);
	$results['data']['search_tags'] = 1;
	$included_tables[] = 'recipes_tags';
}

if ($posted_data['ingredient_id']) 
{
	$ingredient = recipes_ingredients(array('ingredient_id'=>$posted_data['ingredient_id'],'fields'=>'base','return'=>'list'));
foreach ($ingredient as $v) {
	$p[] = "'".$v['base']."'";
}
	$search_condition[] = " recipes.id IN (SELECT itemid FROM recipes_ingredients WHERE base IN (".implode(",",$p)."))";
}
if ($posted_data['ingredients_list']) 
{
	$ingredients = array();
	if (is_array($posted_data['ingredients_list'])) {
		foreach ($posted_data['ingredients_list'] as $v) {
			if ($v != '') {
				$ingredients[] = " recipes_ingredients.base = '$v'";
			}
			
		}
	}
		if (is_array($ingredients) AND !empty($ingredients)) {
		$ings = implode(" OR ",$ingredients);

	$search_condition[] = " recipes.id IN (SELECT itemid FROM (SELECT recipes_ingredients.itemid,count(itemid) as countmofo FROM recipes_ingredients WHERE $ings GROUP BY itemid HAVING countmofo = ".count($ingredients).") as itemid) ";
		}
}
if ($posted_data['ings_neg']) 
{
	$ingredients = array();
	if (is_array($posted_data['ings_neg'])) {
		foreach ($posted_data['ings_neg'] as $v) {
			if ($v != '' AND !empty($v)) {
				$ingredients[] = " (recipes_ingredients.base = ('$v'))";
			}
			
		}
	}
	if (is_array($ingredients) AND !empty($ingredients)) {
		$ings = implode(" OR ",$ingredients);
	$search_condition[] = " recipes.id NOT IN (SELECT itemid FROM (SELECT recipes_ingredients.itemid,count(itemid) as countmofo1 FROM recipes_ingredients WHERE $ings GROUP BY itemid HAVING countmofo1 = ".count($ingredients).") as itemid) ";
	}
}
if ($posted_data['method_id']) 
{
	$sql->db_Select("recipes_page_cooking_methods","itemid","catid = ".$posted_data['method_id']);
	if ($sql->db_Rows()) {
		$m = execute_multi($sql);
		$a = array();
		foreach ($m as $v) {
			$a[] = $v['itemid'];
		}
		$search_condition[] = " recipes.id IN (".implode(",",$a).")";
	}
}
if ($posted_data['methods']) 
{
	if (is_array($posted_data['methods'])) {
		$methods = implode(",",$posted_data['methods']);
	}
	$sql->db_Select("recipes_page_cooking_methods","itemid","catid IN (".$methods.") GROUP BY itemid");
	if ($sql->db_Rows()) {
		$m = execute_multi($sql);
		$a = array();
		foreach ($m as $v) {
			$a[] = $v['itemid'];
		}
		$search_condition[] = " recipes.id IN (".implode(",",$a).")";
	}
}
if ($posted_data['difficulty_id']) 
{
	$sql->db_Select("recipes_page_difficulty_level","itemid","catid = ".$posted_data['difficulty_id']);
	if ($sql->db_Rows()) {
		$m = execute_multi($sql);
		$a = array();
		foreach ($m as $v) {
			$a[] = $v['itemid'];
		}
		$search_condition[] = " recipes.id IN (".implode(",",$a).")";
	}
}
if ($posted_data['difficulty_id']) 
{
	$sql->db_Select("recipes_page_difficulty_level","itemid","catid = ".$posted_data['method_id']);
	if ($sql->db_Rows()) {
		$m = execute_multi($sql);
		$a = array();
		foreach ($m as $v) {
			$a[] = $v['itemid'];
		}
		$search_condition[] = " recipes.id IN (".implode(",",$a).")";
	}
}

//Extra fields Radio buttons search
if ($posted_data)
{
 foreach ($posted_data as $key => $value) 
 {   
	if (strstr($key,"$"))
	{
		$key = str_replace("$","-",$key);
		list($field,$code)=split("-",$key);
		$condition_radio[] = "(recipes_extra_field_values.value = '$value' AND recipes_extra_field_values.fieldid = '$code')";
		$included_tables[] = 'recipes_extra_fields';
		$included_tables[] = 'recipes_extra_field_values';
	}
 }//END OF FOREACH
}
if (!empty($condition_radio))
{ 
	$search_condition[] = "(".implode(" OR ", $condition_radio).")"." AND ";
}//END OF IF

############################# EXTRA FIELD VALUE ############################
	if ($posted_data['efield'] AND $posted_data['itemid']) 
	{
		//first get the value we're looking for
		$sql->db_Select("recipes_extra_field_values","*","itemid = ".$posted_data['itemid']." AND fieldid = ".$posted_data['efield']);
		if ($sql->db_Rows()) 
		{
			$field_values = execute_single($sql);
			$search_tables[] = "LEFT OUTER JOIN recipes_extra_field_values ON (recipes.id = recipes_extra_field_values.itemid)";
			$included_tables[] = 'recipes_extra_fields';
			$included_tables[] = 'recipes_extra_field_values';
			$search_condition[] = "recipes_extra_field_values.value = '".$field_values['value']."' 
			AND recipes_extra_field_values.fieldid = ".$posted_data['efield']." AND ";
		}

	}
##################### EXCLUDE ITEMS #######################
if ($posted_data['exclude']) {
	$search_condition[] = " recipes.id NOT IN (".$posted_data['exclude'].")";
}


###################### SORTING ###################	
$sort = ($posted_data['sort']) ? $posted_data['sort'] : $settings['settings']['default_recipes_sort'];
//End check
$direction = ($posted_data['sort_direction']) ? $posted_data['sort_direction'] : $settings['settings']['default_sort_direction'];
$selected = ($posted_data['sort']) ? $sort : $settings['settings']['default_recipes_sort'];

if ($sort == ("date_added")) { $selected = "recipes.date_added";}
elseif ($sort == "title") 
{
	$selected = 'recipes.title';
}
elseif ($sort == 'catid')
{
		if (!in_array('recipes_page_categories',$included_tables)) 
	{
		$search_tables[] = "INNER JOIN recipes_page_categories ON (recipes.id = recipes_page_categories.itemid)";
		$included_tables[] = 'recipes_page_categories';
	}
}

if ($posted_data['availability'] OR $posted_data['availability'] == 0) 
{
	if ($posted_data['availability'] == 1) 
	{
		$search_condition[] = " active = 1";
		$active = 1;
	}
	elseif ($posted_data['availability'] == 0)
	{
		$search_condition[] = " active = 0";
		$active = 0;
	}
	else 
	{
		$search_condition[] = " active IN (1,0) ";
		$active = "no";
	}
}


$fields = "recipes.id";
$search_tables = implode(" ",$search_tables);
$search_condition = implode(" AND ",$search_condition);
$search_condition .= " 
GROUP BY recipes.id
ORDER BY $selected $direction";
//EXECUTE THE QUERY AND GATHER THE ID'S
$sql->db_Select($search_tables,$fields,$search_condition);
#################### PAGINATE ###################
$current_page = ($posted_data['page']) ? $posted_data['page'] : 1;

$results_per_page = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $settings['settings']['items_per_page'];
if (isset($posted_data['start'])) 
{
	$start = $posted_data['start'];
}
else {
	$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
}

//echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page."<br>";

$total = $sql->db_Rows();
//echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page;
if ($query == 1) {
	$smarty->assign("query", "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page);
}
elseif ($query == 2)
{
	echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page."<br>";
}

//echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start,".$results_per_page;
if ($total) 
{
//echo "SELECT $fields FROM $search_tables WHERE $search_condition LIMIT $start, $results_per_page";
$sql->db_Select($search_tables,$fields,"$search_condition LIMIT $start,".$results_per_page);
$search_results = execute_multi($sql,1);

paginate_results($current_page,$results_per_page,$total);

for ($i=0;count($search_results) > $i;$i++)
{
	$recipes[$i] = get_recipe(array('active'=>$posted_data['availability'],'id'=>$search_results[$i]['id'],'get_provider'=> $posted_data['get_provider'],'get_user'=>$posted_data['get_user'], 
	'debug'=>$posted_data['debug'],'thumb'=>$posted_data['thumb'],'ingredients'=>$posted_data['ingredients'],'main'=>$posted_data['main'],'count_comments'=>$posted_data['count_comments']));
}//END OF FOR

$results['results'] = $recipes;


}//END OF IF

return $results;
}



function latest_recipes($settings)
{
	global $current_module;
		 $posted_data = $settings;
		 $posted_data['availability'] = ($settings['active']) ? $settings['active'] : 1;//compatibility fix
		 $posted_data['sort'] = ($settings['sort']) ? $settings['sort'] : $current_module['settings']['default_sort'];
		 $posted_data['sort_direction'] = ($settings['sort_direction']) ? $settings['sort_direction'] : $current_module['settings']['default_sort_direction'];
		 $posted_data['results_per_page'] = ($settings['results_per_page']) ? $settings['results_per_page'] : $current_module['settings']['items_per_page'];	
		 $posted_data['categories'] = $settings['categories'];
		 $posted_data['exclude'] = $settings['exclude'];
		 $posted_data['provider_id'] = $settings['uid'];
		 if ($settings['just_results']) {
		 	$a = recipes_search($posted_data,$current_module,$settings['page'],$settings['debug']);
		 	return $a['results'];
		 }
		 else {
		 	return recipes_search($posted_data,$current_module,$settings['page'],$settings['debug']);
		 }
		 
}

function recipes_featured($settings)
{
	global $current_module,$sql;
	
	if ($settings['categories']) {
		$q[] = 'categoryid IN ('.implode(',',$settings['categories']).")";
	}
	if ($settings['limit']) {
		$limit = ' LIMIT '.$settings['limit'];
	}
	if (is_array($q)) {
		$q = implode(" AND ",$q);
		$mode = 'default';
	}
	else {
		$mode = "no_where";
	}
	$sql->db_Select("recipes_featured","itemid",$q.$limit,$mode);
	if ($settings['debug']) {
		echo "Rows : ".$sql->db_Rows();
		echo "SELECT itemid FROM recipes_featured WHERE $q $limit"; 
	}
	if ($sql->db_Rows()) {
		if ($settings['limit'] == 1) {
			$a = execute_single($sql);
			$settings['subs']['id'] = $a['itemid'];
			$res = get_recipe($settings['subs']);
		}
		else {
			$a = execute_multi($sql);
			for ($i=0;count($a) > $i;$i++)
			{
				$settings['subs']['id'] = $a[$i]['itemid'];
				$res[] = get_recipe($settings['subs']);
			}
		}
		return $res;
	}
}//END FUNCTION

function get_recipe($settings)
{
	global $sql,$loaded_modules;
	$t = new convert();
	switch ($settings['active'])
	{
		case 1:
		$active = " AND active = 1";
		$image_active = '1';
		break;
		case 0:
		$active = " AND active = 0";
		$image_active = '0';
		break;
		case 2:
		$active = "";
		$image_active = '0,1';
		break;
	}
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$q = "id = ".$settings['id'].$active;
	$sql->db_Select("recipes",$fields,$q);
	if ($settings['debug']) {
		echo "SELECT $fields FROM recipes WHERE $q<br>";
	}
	if ($sql->db_Rows()) {
		$item = execute_single($sql,1);
		$item['category'] = get_recipes_cat(array('id'=>$item['id'],'cat_details'=>1,'form_settings'=>$settings['cat_settings']));
		$item['settings'] = form_settings_array($item['settings'],'###',":::");
		if ($settings['get_provider']) {
			$item['user'] = get_provider_id($item['id'],'recipes',array('get_user'=>$settings['get_user']));
		}
		if ($settings['efields']) {
			$item['efields'] = simplify_efields(get_recipes_extra_fields($item['id']));
		}
		if ($settings['thumb'])
		{
			$a = get_recipes_detailed_images($item['id'],0,0);
			$item['image_full'] = $a[0];
		}
		if ($settings['comments']) {
			$item['comments'] = get_users_comments(array('id'=>$item['id'],'module'=>'recipes','get_user'=>1,'debug'=>0));
		}
		if ($settings['count_comments']) {
			$item['count_comments'] = get_users_comments(array('id'=>$item['id'],'module'=>'recipes','count'=>1,'debug'=>0));
		}
		if ($settings['votes']) {
			$item['votes'] = get_item_votes(array('id'=>$item['id'],'module'=>'recipes'));
			$item['votes']['sum'] = $item['votes']['votes_plus'] - $item['votes']['votes_minus'];
		}
		if ($settings['tags']) {
			$item['tags'] = create_recipes_tag_cloud(10,$item['id']);
		}
		if ($settings['images']) {
			$type = (!$settings['type']) ? $loaded_modules[$settings['modules']]['settings']['default_image_type'] : $settings['type'];
			$item['detailed_images'] = get_recipes_detailed_images($item['id'],$type,$image_active);
		}
		if ($settings['ingredients']) {
			$item['ingredients'] = recipes_ingredients(array('id'=>$item['id'],'main'=>$settings['main'],'limit'=>$settings['limit'],'full-recipe'=>$settings['ingredients-full']));
		}
		if ($settings['difficulty_level']) {
			$item['difficulty_level'] = recipes_flat_category_items(array('itemid' => $item['id'],'table'=>'recipes_difficulty_level','page_table'=>'recipes_page_difficulty_level'));
		}		
		if ($settings['cooking_methods']) {
			$item['cooking_methods'] = recipes_flat_category_items(array('itemid' => $item['id'],'table'=>'recipes_cooking_methods','page_table'=>'recipes_page_cooking_methods','multi'=>1));
		}		
		return $item;
	}//END ROWS
}//END FUNCTION


function recipes_ingredients($settings)
{
	global $sql;
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : " ORDER BY orderby";
	$way = ($settings['way']) ? $settings['way'] : " ASC";
	$main = ($settings['main']) ? " AND main = ".$settings['main'] : "";
	$limit = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	if ($settings['id']) {
	$sql->db_Select("recipes_ingredients","*","itemid = ".$settings['id'].$main.$orderby.$way.$limit);
//	echo "SELECT * FROM recipes_ingredients WHERE itemid = ".$settings['id'].$main.$orderby.$way.$limit.";<br>";
	if ($settings['full-recipe']) {
		$item = execute_multi($sql);
		for ($i=0;count($item) > $i;$i++)
		{
			if ($item[$i]['type'] == 'ingredient') {
				$item[$i]['unit_text'] = recipes_conversion_units(array('id'=>$item[$i]['unit_id']));
			}			
		}
		return $item;
	}
	else {
	return execute_multi($sql);
	}
	}//END ITEMS INGREDIENTS 
	elseif ($settings['return'] == 'list')
	{
		if (is_array($settings['ingredient_id'])) {
			$sql->db_Select("recipes_ingredients","base","id IN (".implode(",",$settings['ingredient_id']).") GROUP BY base");
		}
		else {
			$sql->db_Select("recipes_ingredients","base","id = ".$settings['ingredient_id']);
//			echo "SELECT base FROM recipes_ingredients WHERE id = ".$settings['ingredient_id'];
		}
		
		return execute_multi($sql);
		
	}
	elseif ($settings['ingredient_id'])
	{
		$sql->db_Select("recipes_ingredients","base","id = ".$settings['ingredient_id']);
		$a = execute_single($sql);
		$sql->db_Select("recipes_ingredients","itemid","base = '".$a['base']."'");
		$b = execute_multi($sql);
		return $b;
	}
	elseif ($settings['ingredient_name'])
	{
		$sql->db_Select("recipes_ingredients","base","id = ".$settings['ingredient_id']);
		$a = execute_single($sql);
		$sql->db_Select("recipes_ingredients","itemid","base = '".$a['base']."'");
		$b = execute_multi($sql);
		return $b;
	}
	else {//GET ALL INGREDIENTS
		$mode = ($settings['base'] OR $settings['type']) ? "default" : "no_where";
		$type_q = ($settings['type']) ? "type = '".$settings['type']."' " : "";
//		echo "SELECT * FROM recipes_ingredients WHERE $type_q GROUP BY base ".$main.$orderby.$way.$limit;
		$sql->db_Select("recipes_ingredients","*","$type_q GROUP BY base ".$main.$orderby.$way.$limit,$mode);
		if ($sql->db_Rows()) {
			return execute_multi($sql);
		}
	}
}

function get_recipes_extra_field_categories($id)
{
	global $sql;
	 	$sql->db_Select("recipes_extra_field_categories","catid","fieldid = ".$id);
 	 	if ($sql->db_Rows() > 0) 
 	 	{
 	 		return  execute_multi($sql);
 	 	}
}

function get_recipes_extra_fields($id,$hidden=0) 
{ 
	global $sql;
	$t = new textparse();
	 	//LOAD ANY POSSIBLE EXTRA FIELDS
	$sql->db_Select("recipes_extra_fields INNER JOIN recipes_extra_field_values ON (recipes_extra_fields.fieldid = recipes_extra_field_values.fieldid)",
	"recipes_extra_fields.fieldid,recipes_extra_fields.type,recipes_extra_fields.var_name,recipes_extra_fields.settings","itemid = $id AND active = 'Y'");
//	echo "SELECT recipes_extra_fields.fieldid,recipes_extra_fields.type,recipes_extra_fields.var_name,recipes_extra_fields.settings FROM
//	recipes_extra_fields INNER JOIN recipes_extra_field_values ON (recipes_extra_fields.fieldid = recipes_extra_field_values.fieldid) WHERE
//	itemid = $id AND active = 'Y'<br>";
$a = execute_multi($sql);
for ($i=0;count($a)>$i;$i++)
{
	if ($a[$i]['type'] !='hidden') {
		$fields[] = filter_recipes_exta_fields($a[$i]['fieldid'],$id);
	}
}

	return $fields;
}//END OF FUNCTION get_recipes_extra_fields

function recipes_extra_fields($id) 
{ 
	global $sql;
	$t = new textparse();
	 	//LOAD ANY POSSIBLE EXTRA FIELDS
	$sql->db_Select("recipes_extra_fields INNER JOIN recipes_extra_field_values ON (recipes_extra_fields.fieldid = recipes_extra_field_values.fieldid)",
	"recipes_extra_fields.fieldid","id = $id AND active = 'Y'");
//	echo "SELECT recipes_extra_fields.fieldid FROM recipes_extra_fields INNER JOIN recipes_extra_field_values ON (recipes_extra_fields.fieldid = recipes_extra_field_values.fieldid) WHERE itemid = $id AND active = 'Y'";
	for ($i=0;$sql->db_Rows() > $i;$i++)
	{
		$r = $sql->db_Fetch();
		$fields[$i] = filter_recipes_exta_fields($r['fieldid'],$id);
		
	}//END OF FOR
	return $fields;
}//END OF FUNCTION get_recipes_extra_fields

function get_recipes_extra_field_values($itemid) 
{ 
  global $sql;
  $sql->db_Select("recipes_extra_field_values","itemid,value","itemid = $itemid");
  return execute_multi($sql,1);
}//END OF FUNCTION get_extra_field_values

function filter_recipes_exta_fields($id,$itemid) 
{ 
	global $sql;
	$fields = "*";
	$tables = "recipes_extra_fields INNER JOIN recipes_extra_field_values ON (recipes_extra_fields.fieldid = recipes_extra_field_values.fieldid)";
	$q = "recipes_extra_fields.fieldid = $id AND itemid = '$itemid'";
	$sql->db_Select($tables,$fields,$q);
//echo "SELECT $fields FROM $tables WHERE $q<br><br>";
	$field = execute_single($sql,1);
	if ($field) {
	$tmp = $field['settings'];
	$field['settings'] = '';
	}
	if ($tmp) {
		$field['settings'] = form_settings_array($tmp,"###",":::");
	}
	return $field;
}//END OF FUNCTION filter_exta_fields

function get_extra_fields_recipes($active,$itemid=0,$catid=0) 
{ 
global $sql;
$fields = "*";
$tables = "recipes_extra_fields";
if ($catid) 
{
	//Check if exists at all
	$sql->db_Select("recipes_extra_field_categories","catid","catid = $catid");
	if ($sql->db_Rows() > 0)
	{
		$tables .="INNER JOIN recipes_extra_field_categories ON (recipes_extra_field_categories.fieldid=recipes_extra_fields.fieldid)";
		$cat = " AND catid = $catid GROUP BY fieldid";
	}

}
if ($active == 0) 
{  
 $mode = ($cat) ? "default" : "no_where";
 $sql->db_Select($tables,$fields,"$cat ORDER BY recipes_extra_fields.type DESC",$mode);
}//END OF IF
else 
{  
$sql->db_Select($tables,$fields,"active = '$active' $cat"); 
}//END OF ELSE
//echo "SELECT $fields FROM $tables WHERE  ORDER BY recipes_extra_fields.type DESC";
if ($sql->db_Rows() > 0)
 {

  $fields = execute_multi($sql,1);
  if ($itemid) 
  {
  	  for ($i=0;count($fields) > $i;$i++)
 	 {
 	 	$sql->db_Select("recipes_extra_field_values","*","itemid = $itemid AND fieldid = ".$fields[$i]['fieldid']);
// 	 	echo "SELECT * FROM recipes_extra_field_values WHERE itemid = $itemid AND fieldid = ".$fields[$i]['fieldid'];
 	 	$values = execute_single($sql);
  		$fields[$i]['values'] = $values;
  		$tmp = $fields[$i]['settings'];
  		$fields[$i]['settings'] = '';
  		$fields[$i]['settings'] = form_settings_array($tmp,"###",":::");
  		
 	 }

  }
  	  for ($i=0;count($fields) > $i;$i++)
 	 {
  		$tmp = $fields[$i]['settings'];
  		$fields[$i]['settings'] = '';
  		$fields[$i]['settings'] = form_settings_array($tmp,"###",":::");
  		
 	 }

  //GET AVAILABLE CATEGORIES
     for ($i=0;count($fields) > $i;$i++)
 	 {
			$fields[$i]['categories'] = get_recipes_extra_field_categories($fields[$i]['fieldid']);
 	 }
  	return $fields;
  	}
  
}//END OF FUNCTION get_extra_fields

function add_recipes_extra_field_values($data,$itemid) 
{ 
		global $sql;
		
		while(list($key,$val)=each($data)) 
		{
		if (strstr($key,"efields-")) 
		{
			list($field,$id)=split("-",$key);
			if ($val) 
			{  
			$query = "'$itemid','$id','$val'";
			$sql->db_Insert("recipes_extra_field_values",$query);
			}//END OF IF not empty field
		}//END OF IF
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function upd_recipes_extra_field_values($data,$itemid) 
{ 
		global $sql;
		$t = new textparse();
		foreach ($data as $key => $val)
		{
		if (strstr($key,"efields-")) 
		{

			$val = $t->formtpa($val);
			list($field,$id)=split("efields-",$key);
			$sql->db_Select("recipes_extra_field_values","itemid","itemid = '$itemid' AND fieldid = '$id'");
			if ($sql->db_Rows() > 0) 
			{ 
			$query = "value = '$val' WHERE itemid = '$itemid' AND fieldid = '$id'";
			$sql->db_Update("recipes_extra_field_values",$query);	
			}//END OF IF
			else 
			{ 
			if ($val) 
			 {  
			$sql->db_Insert("recipes_extra_field_values","'$itemid','$id','$val'"); 
			
			}//END OF IF 
			}//END OF ELSE
		}//END OF IF
		}//END OF WHILE
}//END OF FUNCTION add_extra_field_values

function get_recipes_detailed_images($itemid,$type="images",$active=0,$imageid=0,$thumbid=0) 
{ 
  $sql = new db();

  $imageid_sql = ($imageid) ? " AND imageid = $imageid" : "";
  $type = ($type ==0) ? "AND type = 0" : "AND type = $type";
  $active = ($active == 0) ? "0,1" : $active;
  $query = "itemid = $itemid AND available IN ($active) $imageid_sql $type  ORDER BY orderby";
  $fields = "recipes_images.*";
  $tables = "recipes_images";
//echo "SELECT $fields FROM $tables WHERE $query<br>";
  $sql->db_Select($tables,$fields,$query);
  if ($sql->db_Rows()) 
  {
  	$res = execute_multi($sql);
  	$tmp = array();
  	$i=0;
  	//Now start looping to create the array. Certain things will be done for backward compatibility
  	for ($i=0;count($res) > $i;$i++)
  	{
  		//First of all get the rest of the image types
  		$sql->db_Select("recipes_images_aditional","*","imageid =".$res[$i]['id']);
  		$tmp = execute_multi($sql);
  		
//  		echo $tmp['image_type']."<br>";
//  		echo "SELECT * FROM recipes_images_aditional WHERE imageid =".$ar['id']."<br>";
		foreach ($tmp as $ar) 
		{
			$res[$i][$ar['image_type']] = $ar;

  			$res[$i][$ar['image_type']] = str_replace($_SERVER['DOCUMENT_ROOT'],"",$ar['image_url']);

		}
  		
  	}
  }


  	return $res;
}//END OF FUNCTION get_detailed_images

function get_all_recipes_detailed_images($itemid,$type="image",$active=0) 
{ 
  $sql = new db();
  $type = ($type == 1) ? "" : "AND type = '$type'";
  $active = ($active == 0) ? "0,1" : $active;
  $sql->db_Select("recipes_images","*","itemid = $itemid AND available IN ($active) $type  ORDER BY type,orderby");
  	return execute_multi($sql,1);
}//END OF FUNCTION get_detailed_images

function recipes_images_categories($id=0)
{
	global $sql;
	
	$sql->db_Select("recipes_images_categories","*","ORDER BY id",$mode="no_where");
	return execute_multi($sql);
}

function get_recipes_tags($id,$seperator,$mode=0)
{
	global $sql;
	
	$sql->db_Select("recipes_tags","tag","itemid = $id");
	$tags = execute_multi($sql,1);
	//simplify the array
	for ($i=0;count($tags) > $i;$i++)
	{
		$t[] = $tags[$i]['tag'];
	}
	if ($mode) 
	{
		return $t;
	}
	else 
	{
		return construct_tags_string($t,$seperator);
	}
}

function create_recipes_tag_cloud($limit=0,$recipes=0,$settings=0)
{
	global $sql;
	if ($recipes) 
	{
		
	
	if (is_array($recipes)) 
	{
		foreach ($recipes as $k => $v)
		{
			$recipes_ids_array[] = $v['id'];
		}
		$recipes_ids_q = "itemid IN (".implode(",",$recipes_ids_array).")";

		$orderby_q = ($limit) ? " ORDER BY quantity DESC LIMIT $limit" : " ORDER BY tag ASC";

		$sql->db_Select("recipes_tags","id,tag , COUNT(id) AS quantity","$recipes_ids_q GROUP BY tag $orderby_q");
//		echo "SELECT tag , COUNT(id) AS quantity FROM recipes_tags WHERE $recipes_ids_q GROUP BY tag ORDER BY tag ASC";
	}
	else 
	{
		$orderby_q = ($limit) ? " ORDER BY quantity DESC LIMIT $limit" : " ORDER BY  tag";

		$sql->db_Select("recipes_tags","id,tag , COUNT(id) AS quantity","itemid = $recipes GROUP BY tag $orderby_q");
//		echo "SELECT id,tag, COUNT(id) AS quantity FROM recipes_tags WHERE itemid = $recipes GROUP BY tag $orderby_q<br>";
	}
	}
	else 
	{
		if ($limit) 
		{
			$limit_q = " LIMIT $limit";
		}
//		echo "SELECT id,tag , COUNT(id) AS quantity FROM recipes_tags GROUP BY tag ORDER BY tag ASC $limit_q";
		$sql->db_Select("recipes_tags","id,tag , COUNT(id) AS quantity","GROUP BY tag ORDER BY quantity DESC $limit_q",$mode="no_where");
	}
	
	if ($sql->db_Rows()) 
	{

	$tmp = execute_multi($sql);
	
	for ($i=0;count($tmp) > $i;$i++)
	{
		$tags[$tmp[$i]['tag']] = $tmp[$i]['quantity'];
	}
//	echo "SELECT tag , COUNT(id) AS quantity FROM recipes_tags "
	$max_size = ($settings['max_size']) ? $settings['max_size'] : 250; // max font size in %
	$min_size = ($settings['min_size']) ? $settings['min_size'] : 100; // min font size in %
	
	$max_qty = max(array_values($tags));
	$min_qty = min(array_values($tags));
	
		// find the range of values
	$spread = $max_qty - $min_qty;
	if (0 == $spread) 
	{ // we don't want to divide by zero
    	$spread = 1;
	}
	
	// determine the font-size increment
	// this is the increase per tag quantity (times used)
	$step = ($max_size - $min_size)/($spread);
	
	for ($i=0;count($tmp) > $i;$i++)
	{
		$size = $min_size + (($tmp[$i]['quantity'] - $min_qty) * $step);
		$tmp[$i]['size'] = $size;
	}
	}//END OF ROWS
	return $tmp;
}

function recipes_conversion_units($settings)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$orderby = ($settings['orderby']) ? $settings['orderby'] : "name";
	$way = ($settings['way']) ? $settings['way'] : "ASC";
	
	if ($settings['id']) {
		$sql->db_Select("recipes_conversion_table",$fields,"id = ".$settings['id']);
		return execute_single($sql);
	}
	elseif ($settings['base']) {
		$sql->db_Select("recipes_conversion_table",$fields,"base = 1 ORDER BY $orderby $way");
		return execute_multi($sql);	
	}
	else {
		$sql->db_Select("recipes_conversion_table",$fields,"ORDER BY $orderby $way",$mode="no_where");
		return execute_multi($sql);	
	}

}
function recipes_lookup_tag($tagid)
{
	global $sql;
	
	$sql->db_Select("recipes_tags","*","id = $tagid");
	if ($sql->db_Rows()) 
	{
		return execute_single($sql,1);
	}
}


function recipes_add_conversion_table($data,$settings)
{
	global $sql;
	$t = new textparse();
	$name = $t->formtpa($data['name']);
	$description = $t->formtpa($data['description']);
	$value = $t->formtpa($data['value']);
	$catid = $t->formtpa($data['catid']);
	$base = $t->formtpa($data['base']);
	
	$sql->db_Insert("recipes_conversion_table","'','$name','$description','$value','$catid','$base'");
	
	if ($settings['return']) {
		return mysql_insert_id();
	}
}

function save_recipe($data)
{
	global $sql;
	$t = new textparse();
	$id = $data['id'];
	
if ($data['action'] == "update") 
{
	$required_fields = array('title');
//print_r($data);
//exit();
	//STEP 1: grab POST variables into a formated array
 $posted_data = array();
 
 foreach ($data as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
//Database insertions
//Add the basic recipes details
$orderby = $data['orderby'];
$allow_comments = $data['allow_comments'];
$active = ($data['AutoActivate']) ? 0 : $data['active'];
$orderby = $data['orderby'];
$description = $t->formtpa($data['description']);
$description_long = $t->formtpa($data['description_long']);
$uid = ID;
$title = $t->formtpa($data['title']);
$date_modified = time();
$date_added = mktime($data['Time_Hour'],$data['Time_Minute'],0,$data['Date_Month'],$data['Date_Day'],$data['Date_Year']);
//SETTINGS
foreach ($data as $key => $val)
{
if (strstr($key,"settings_")) 
{
list($field,$code)=split("settings_",$key);
$s[$code] = $val;
}//END OF IF
}//END OF FOREACH
if (is_array($s)) {
	$settings = form_settings_string($s,"###",":::");
}

//print_r($data);//END OF print_r
$query = "active = '$active', orderby = '$orderby', date_modified= '$date_added',title = '$title',description = '$description',description_long = '$description_long',settings ='$settings' WHERE id = $id";
//echo $query;
$sql->db_Update("recipes",$query);


if ($date_added > time()) {//Auto publish
	//First chck for existing cron jobs
	$sql->db_Select("cron_tasks","id","itemid = $id AND module = 'recipes' AND action = 'activate'");
	if ($sql->db_Rows()) {
		$sql->db_Update("cron_tasks","date = '$date_added' WHERE itemid = $id AND module = 'recipes' AND action = 'activate'");
	}
	else {//new entry
		$sql->db_Insert('cron_tasks',"'','$id','recipes','activate','$date_added'");
	}
}
else {//Auto publish off
	//First chck for existing cron jobs
	$sql->db_Select("cron_tasks","id","itemid = $id AND module = 'recipes' AND action = 'activate'");
	if ($sql->db_Rows()) {
		$sql->db_Delete("cron_tasks","id","itemid = $id AND module = 'recipes' AND action = 'activate'");
	}
}
//SECONDARY UPDATES

//recipes CATEGORIES


//See if there are any extra categories to insert
if (is_array($data['cat'])) 
{  
	$sql->db_Delete("recipes_page_categories","itemid = $id");
 for ($i=0;count($data['cat']) > $i;$i++)
 {
 	$sql->db_Insert("recipes_page_categories","'".$data['cat'][$i]."','$id'");
 }//END OF FOR
}//END OF IF

//Providers
if ($data['providerid'] AND $data['providerid'] != '%') 
{
	$sql->db_Select("provider_items","id","itemid = $id AND type = 'recipes'");
	if ($sql->db_Rows()) 
	{
		$sql->db_Update("provider_items","uid = ".$data['providerid']." WHERE itemid = $id AND type = 'recipes' ");
	}
	else 
	{
		$sql->db_Insert("provider_items","'','".$data['providerid']."','$id','recipes','".time()."'");
//		echo "'','".$data['provider']."','$id','".time()."'";
	}
}
//Cooking Methods
if (is_array($data['cooking_methods'])) 
{  
	$sql->db_Delete("recipes_page_cooking_methods","itemid = $id");
 for ($i=0;count($data['cooking_methods']) > $i;$i++)
 {
 	$sql->db_Insert("recipes_page_cooking_methods","'".$data['cooking_methods'][$i]."','$id'");
 }//END OF FOR
}//END OF IF

//Difficulty Levels
if ($data['difficulty_level'] AND $data['difficulty_level'] != '%') 
{
	$sql->db_Select("recipes_page_difficulty_level","catid","itemid = $id");
	if ($sql->db_Rows()) 
	{
		$sql->db_Update("recipes_page_difficulty_level","catid = ".$data['difficulty_level']." WHERE itemid = $id");
	}
	else 
	{
		$sql->db_Insert("recipes_page_difficulty_level","'".$data['difficulty_level']."','$id'");
	}
}

####################### INGREDIENTS ###############################
$sql->db_Delete("recipes_ingredients","itemid = $id");
$j=1;
$tmp = array();
	foreach ($data as $k => $v) {
		if (strstr($k,'-') AND !strstr($k,'efields-')) {
			list($field,$ingid)=split("-",$k);
			$tmp[$ingid][$field] = $v;
			$tmp[$ingid]['orderby'] = $j;
			$j++;
		}
		
	}
	if ($data['ingredient_order']) {
		$order = explode(",",$data['ingredient_order']);
		for ($i=0;count($order) > $i;$i++)
		{
			if (is_numeric($order[$i])) {
				$tmp[$order[$i]]['orderby'] = $i+1;
			}
		}
	}
	else {
		
	}
	$tmp = array_merge($tmp,array());
//loop through the list
for ($i=0;count($tmp) > $i;$i++)
{
	$type = ($tmp[$i]['type'] == 'header') ? "header" : "ingredient";
	$tmp[$i]['orderby'] = (!$data['ingredient_order']) ? $i+1 : $tmp[$i]['orderby'];
	$tmp1[] = "('','".$tmp[$i]['ingredient']."','".$data['id']."','".$tmp[$i]['unit']."','".$tmp[$i]['quantity']."','".$tmp[$i]['base']."','".$tmp[$i]['main']."','".$tmp[$i]['orderby']."','$type')";
}


mysql_query("INSERT INTO recipes_ingredients VALUES ".implode(",",$tmp1));

//add extra fields
upd_recipes_extra_field_values($data,$data['id']);


//UPDATE TAGS
if ($data['tags']) 
{
$tags = construct_tags_array($data['tags'],",");
//Clear old tags
$sql->db_Delete("recipes_tags","itemid = ".$data['id']);
//insert new tags
for ($i=0;count($tags) > $i;$i++)
{
	$tag = $tags[$i];
	if ($tag) {
		$sql->db_Insert("recipes_tags","'','".$data['id']."','$tag'");
	}
	
}
}

return "modified";
 }//END OF NO ERRORS
 else {
 	return $error_list;
 }
}//END UPDATE
else //ADD ITEM
{
//	print_r($data);
//exit();
	$required_fields = array('title');
	//STEP 1: grab POST variables into a formated array
 $posted_data = array();
 foreach ($data as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
 	$t = new textparse();
//Database insertions
//Add the basic content details
$categoryid = $data['categoryid'];
$orderby = $data['orderby'];
$allow_comments = $data['allow_comments'];
$active = ($data['AutoActivate']) ? 0 : $data['active'];
$orderby = $data['orderby'];
$description = $t->formtpa($data['description']);
$description_long = $t->formtpa($data['description_long']);
$date_added = mktime($data['Time_Hour'],$data['Time_Minute'],0,$data['Date_Month'],$data['Date_Day'],$data['Date_Year']);
$uid = ID;
$title = $t->formtpa($data['title']);
//SETTINGS
foreach ($data as $key => $val)
{
if (strstr($key,"settings_")) 
{
list($field,$code)=split("settings_",$key);
$s[$code] = $val;
}//END OF IF
}//END OF FOREACH
if (is_array($s)) {
	$settings = form_settings_string($s,"###",":::");
}
//print_r($data);//END OF print_r
$sql->db_Insert("recipes","'','$title','$description','$description_long','$uid','$date_added',$date_added,'$active',0,'$orderby','$settings'");
//echo "INSERT INTO recipes VALUES ('','$title','$description','$description_long',$uid,'$date_added',$date_added,'$active',$allow_comments,0,'$orderby','$settings')<br>";
$id = mysql_insert_id();

//CRON JOBS
if ($data['AutoActivate']) {
	$sql->db_Insert('cron_tasks',"'','$id','recipes','activate','$date_added'");
}

//SECONDARY UPDATES

//See if there are any extra categories to insert
if (is_array($data['cat'])) 
{  
 for ($i=0;count($data['cat']) > $i;$i++)
 {
 	$sql->db_Insert("recipes_page_categories","'".$data['cat'][$i]."','$id'");
 }//END OF FOR
}//END OF IF


//Providers
if ($data['providerid'] AND $data['providerid'] != '%') 
{
	$sql->db_Select("provider_items","id","itemid = $id AND type = 'recipes'");
	if ($sql->db_Rows()) 
	{
		$sql->db_Update("provider_items","uid = ".$data['providerid']." WHERE itemid = $id AND type = 'recipes' ");
	}
	else 
	{
		$sql->db_Insert("provider_items","'','".$data['providerid']."','$id','recipes','".time()."'");
//		echo "'','".$data['provider']."','$id','".time()."'";
	}
}

//add extra fields
add_recipes_extra_field_values($data,$id);


//ADD TAGS
if ($data['tags']) 
{
$tags = construct_tags_array($data['tags'],",");	
}

if (is_array($tags)) {
//insert new tags
for ($i=0;count($tags) > $i;$i++)
{
	$tag = $tags[$i];
	$sql->db_Insert("recipes_tags","'','$id','$tag'");
}
}//IF
//Cooking Methods
if (is_array($data['cooking_methods'])) 
{  
	$sql->db_Delete("recipes_page_cooking_methods","itemid = $id");
 for ($i=0;count($data['cooking_methods']) > $i;$i++)
 {
 	$sql->db_Insert("recipes_page_cooking_methods","'".$data['cooking_methods'][$i]."','$id'");
 }//END OF FOR
}//END OF IF

//Difficulty Levels
if ($data['difficulty_level'] AND $data['difficulty_level'] != '%') 
{
	$sql->db_Select("recipes_page_difficulty_level","catid","itemid = $id");
	if ($sql->db_Rows()) 
	{
		$sql->db_Update("recipes_page_difficulty_level","catid = ".$data['difficulty_level']." WHERE itemid = $id");
	}
	else 
	{
		$sql->db_Insert("recipes_page_difficulty_level","'".$data['difficulty_level']."','$id'");
	}
}
####################### INGREDIENTS ###############################
$j=1;
$tmp = array();
	foreach ($data as $k => $v) {
		if (strstr($k,'-') AND !strstr($k,'efields-')) {
			list($field,$ingid)=split("-",$k);
			$tmp[$ingid][$field] = $v;
			$tmp[$ingid]['orderby'] = $j;
			$j++;
		}
		
	}
	if ($data['ingredient_order']) {
		$order = explode(",",$data['ingredient_order']);
		for ($i=0;count($order) > $i;$i++)
		{
			if (is_numeric($order[$i])) {
			$tmp[$order[$i]]['orderby'] = $i+1;
			}
		}
	}
	else {
		
	}
	$tmp = array_merge($tmp,array());
//loop through the list
for ($i=0;count($tmp) > $i;$i++)
{
	$type = ($tmp[$i]['type'] == 'header') ? "header" : "ingredient";
	$tmp[$i]['orderby'] = (!$data['ingredient_order']) ? $i+1 : $tmp[$i]['orderby'];
	$tmp1[] = "('','".$tmp[$i]['ingredient']."','$id','".$tmp[$i]['unit']."','".$tmp[$i]['quantity']."','".$tmp[$i]['base']."','".$tmp[$i]['main']."','".$tmp[$i]['orderby']."','$type')";
}
mysql_query("INSERT INTO recipes_ingredients VALUES ".implode(",",$tmp1));
//echo "INSERT INTO recipes_ingredients VALUES ".implode(",",$tmp1);
return $id;
 }//END OF NO ERRORS
 else {
 	return $error_list;
 }
}//END ADD
}//END FUNCTION

function get_recipes_box($uid,$settings)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] :  "*";
	$q[] = 'uid ='.$uid;
	if ($settings['type']) {
		$q[] = "type = '".$settings['type']."'";
	}
	if ($settings['status']) {
		$q[] = "status = ".$settings['status'];
	}
	if ($settings['sharing']) {
		$q[] = "sharing = '".$settings['sharing']."'";
	}
	if ($settings['difficulty']) {
		$q[] = "difficulty = ".$settings['difficulty'];
	}
	if ($settings['categories']) {
		$q[] = "categories LIKE '%".$settings['categories']."%'";
	}
	if ($settings['methods']) {
		$q[] = "methods LIKE '%".$settings['methods']."%'";
	}
	if ($settings['itemid']) {
		$q[] = "itemid = ".$settings['itemid'];
	}
	if ($settings['id']) {
		$q[] = "id = ".$settings['id'];
	}
	if ($settings['limit']) {
		$limit = " LIMIT ".$settings['limit'];
	}
	if ($settings['orderby']) {
		$orderby = " ORDER BY ".$settings['orderby']." ".$settings['way'];
	}
	
	$query = implode(" AND ",$q);
	

	if ($settings['simple_mode']) {//Return 1d Array with only the ids in for checking purposes
		
	}
	if ($settings['paginate']) 
	{
		#################### PAGINATE ###################
		$current_page = ($settings['page']) ? $settings['page'] : 1;
		
		$results_per_page = ($settings['results_per_page']);;
		if (isset($posted_data['start'])) 
		{
			$start = $settings['start'];
		}
		else {
			$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
		}
		$sql->db_Select("users_recipes_box",$fields,$query);
		if ($settings['debug']) {
			echo "SELECT $fields FROM users_recipes_box WHERE $query<br>";
		}
		$total = $sql->db_Rows();
		if ($total) 
		{
		$sql->db_Select("users_recipes_box",$fields,$query.$orderby." LIMIT $start,".$results_per_page);
		if ($settings['debug']) {
			echo "SELECT $fields FROM users_recipes_box WHERE ".$orderby." LIMIT $start,".$results_per_page."<br>";
		}
		$res = execute_multi($sql,1);
		
		paginate_results($current_page,$results_per_page,$total);
		return $res;

		
		

}//END OF IF
	}//END PAGINATED RESULTS 
	elseif ($settings['single'])//SINGLE ITEM RETURN
	{
		$sql->db_Select("users_recipes_box",$fields,$query);
		$res = execute_single($sql);

		return $res;
		
	}
	else {//NO PAGINATION
		$sql->db_Select("users_recipes_box",$fields,$query.$orderby.$limit);
		if ($settings['itemid'] AND $settings['check']) {//WE HAVE to check out if an itemid is in recipes box
			return $sql->db_Rows();
		}//END CHECK
		else {
			$res = execute_multi($sql);
			return $res;
		}
	}
	
}//END FUNCTION
?>