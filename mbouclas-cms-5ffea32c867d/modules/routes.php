<?php
class routes
{
      
    function searchRoutes($settings)
    {
    	//print_r($settings);
        global $sql;
        
         
        $table = "smsRoutingTable
                    INNER JOIN smsConnections ON smsRoutingTable.connectionID = smsConnections.id
                    INNER JOIN dialing_codes ON smsRoutingTable.countryID = dialing_codes.id
                    ";
        $fields = (!$settings['fields']) ? "*" : $settings['fields'];
        $orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
    	$way = ($settings['way']) ? "  ".$settings['way'] : "";
    	$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
    	$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
    	
        if(isset($settings['searchFields']['countries']))
    	{
    	    
            $countries=trim($settings['searchFields']['countries']," ,");
            $countries=explode(",",$countries);
            $countries = "('".implode("','",$countries)."')";
    		$q[] = "dialing_codes.Country IN ".$countries;
            unset($settings['searchFields']['countries']);
    	}
        
        if(is_array($settings['searchFields']['connections']))
    	{
    		$q[] = "smsConnections.title IN ('".implode("','",$settings['searchFields']['connections'])."')";
            unset($settings['searchFields']['connections']);
    	}
        if (is_array($settings['searchFields'])) {
    		foreach ($settings['searchFields'] as $k => $v) {
    			$q[] = "$k = '$v'";
    		}
    	}//END SETUP SEARCH FIELDS
    
    	if (is_array($settings['searchFieldsLike'])) {
    		foreach ($settings['searchFieldsLike'] as $k => $v) {
    			$q[] = "$k LIKE '%$v%'";
    		}
    	}//END SETUP SEARCH FIELDS WITH LIKE
    	
    	if (is_array($settings['greaterThan'])) {
    		foreach ($settings['greaterThan'] as $k => $v) {
    			$q[] = $k." > ".$v;
    		}
    	}//END SETUP SEARCH FIELDS WITH LIKE
    	
    	if (is_array($settings['lessThan'])) {
    		foreach ($settings['lessThan'] as $k => $v) {
    			$q[] = $k." < ".$v;
    		}
    	}
    	
        if (is_array($settings['missingFields'])) {
    		foreach ($settings['missingFields'] as $k => $v) {
    			$q[] = "$k != '$v'";
    		}
    	}//END SETUP SEARCH FIELDS        
    	 
        if($q)
        {
            $query=implode(" AND ",$q);
        }

        if (!$query) 
				{
					$query_mode='no_where';
				}
				else {
					$query_mode = 'default';
				}

			if ($settings['return'] == 'multi') //Return all results, NO PAGINATION
			{
			
				$sql->db_Select($table,$fields,"$query $orderby $way $limit",$query_mode);
				if ($settings['debug']) {
					echo "SELECT ".$fields." FROM ".$table." WHERE $query $orderby $way $limit<br>";
				}
				if ($sql->db_Rows()) 
				{
					$res = execute_multi($sql);
					
				}
			}//END ALL RESULTS
			elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
			{
			 
				$sql->db_Select($table,'smsRoutingTable.id',"$query $orderby $way",$query_mode);//GET THE TOTAL COUNT
				
					if ($settings['debug']) 
					{
						echo "SELECT smsRoutingTable.id FROM ".$table." WHERE $query $orderby $way<br>";
					}
				if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
				{
					$total = $sql->db_Rows();
					$current_page = ($settings['page']) ? $settings['page'] : 1;
					$results_per_page =  $settings['results_per_page'];
					if (isset($settings['start'])) 
					{
						$start = $settings['start'];
					}
					else {
						$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
					}
					$limit = "LIMIT $start,".$results_per_page;
					$sql->db_Select($table,$fields,"$query $orderby $way $limit",$query_mode);
					if ($settings['debug']) {
						echo "<br>SELECT $fields FROM ".$table." WHERE $query $orderby $way $limit";
					}
					$res = execute_multi($sql,1);
					paginate_results($current_page,$results_per_page,$total);
					
				}//END FOUND RESULTS
			}//END PAGINATION        
        	elseif ($settings['return'] == 'single') // RETURN PAGINATED RESULTS
        	{
				$sql->db_Select($table,$fields,"$query $orderby $way $limit",$query_mode);
				if ($settings['debug']) {
					echo "SELECT ".$fields." FROM ".$table." WHERE $query $orderby $way $limit<br>";
				}
				if ($sql->db_Rows()) 
				{
					$res = execute_single($sql);	
				}
				
				
				if ($settings['getUserDetails']) {
							$res['userDetails'] = $this->users->userDetails($res['uid'],array('fields'=>'id,uname'));
				}//END USER DETAILS
        				
        	}
        	elseif ($settings['return'] == 'count') // RETURN COUNT RESULTS
			{
				$sql->db_Select($table,"count(".$table.".id) as total","$query $orderby $way $limit",$query_mode);
				if ($settings['debug']) {
					echo "SELECT count(".$table.".id) as total FROM ".$table." WHERE $query $orderby $way $limit<br>";
				}
				$res = execute_single($sql);
				return $res['total'];
			}
        	
        	if ($settings['return']!='single') // RETURN PAGINATED RESULTS
        	{
				if ($settings['getUserDetails']) {
				    $users = new user(); 
					for ($i=0;count($res) > $i;$i++)
					{
						$res[$i]['userDetails'] = $users->userDetails($res[$i]['uid'],array('fields'=>'id,uname'));
					}
				}//END USER DETAILS
        	}
            if ($settings['checkCustomRoutes'])
            {
                for ($i=0;count($res) > $i;$i++)
				{
				    if($sql->db_Count("smsRoutingPricingPolicies",'(id)'," WHERE rid='".$res[$i]['id']."'")>0)
                    $res[$i]['hasCustomRoutes']=1;
                    else
                    $res[$i]['hasCustomRoutes']=0;
				}
            
            }
            	    
			
        return $res;
    }
    /*
    function getRoutes($args,$extraFields,$mode=0)
    {
        global $sql;
        $user = new user();
        $conn = new connections();
        if($mode!=0) $mode="default";
        else $mode="notdefault";
        $table = "smsRoutingTable
                    INNER JOIN smsConnections ON smsRoutingTable.connectionID = smsConnections.id
                    INNER JOIN dialing_codes ON smsRoutingTable.countryID = dialing_codes.id
                    ";
        $fields="smsRoutingTable.*,".$extraFields;
        $fields = trim($fields,",");
        
        $sql->db_Select($table,$fields,$args,$mode);
        
        $data = execute_multi($sql);
        
        for ($i=0;count($data) > $i;$i++)
		{
            if($data[$i]['uid']==0)
            {
                $data[$i]['user'] = array("id"=>0,"uname"=>"All users");
            }
            else
            {
                $data[$i]['user'] = $user->userDetails($data[$i]['uid'],array('fields'=>'id,uname'));    
            }	
		}
        
        for ($i=0;count($data) > $i;$i++)
		{
			$data[$i]['connection'] =  $conn->getConnectionDetails($data[$i]['connectionID']);
		}
        
        return $data;
    }
    */
    function newRoute($settings)
    {
        global $sql;        
        
        foreach($settings as $k=>$v)
        {
            $args.="$k,";
            $vals.="'$v',";
        }
        $args.="date_added";
        $vals.=time();  
        
        $sql->db_Insert("smsRoutingTable ($args)",$vals);
        
        //echo "INSERT INTO smsRoutingTable ($args) VALUES ($vals)";
    }
    
    
    function newCustomRoute($settings)
    {
        global $sql;        
        
        foreach($settings as $k=>$v)
        {
            $args.="$k,";
            $vals.="'$v',";
        }
        $args=trim($args,",");
        $vals=trim($vals,",");
        
        $sql->db_Insert("smsRoutingPricingPolicies ($args)",$vals);
        
        
        //echo "INSERT INTO smsRoutingPricingPolicies ($args) VALUES ($vals)";
    }
    
    function updateRoute($routeID,$settings)
    {
        global $sql;
        
        foreach($settings as $k=>$v)
        {
            $args.="$k='$v',";
        }
        $args = trim($args,",");
        
        $sql->db_Update("smsRoutingTable",$args." WHERE id=$routeID");
    }
    
    function updateCustomRoute($routeID,$settings)
    {
        global $sql;
        
        foreach($settings as $k=>$v)
        {
            $args.="$k='$v',";
        }
        $args = trim($args,",");
        
        $sql->db_Update("smsRoutingPricingPolicies",$args." WHERE id=$routeID");
    }
    
    function deleteRoute($routeID)
    {
        global $sql;
        $sql->db_Delete("smsRoutingTable","id=$routeID");
    }
    
    function deleteCustomRoute($routeID)
    {
        global $sql;
        $sql->db_Delete("smsRoutingPricingPolicies","id=$routeID");
    }
    
    function getRouteDetails($id)
    {
        global $sql;
        $user = new user();
        $conn = new connections();
        
        $sql->db_Select("smsRoutingTable","*","id='$id'");
        
        $data = execute_single($sql);
        if($data['uid']==0)
        {
             $data['user'] = array("id"=>0,"uname"=>"All users");
        }
        else
        {
            $data['user'] = $user->userDetails($data['uid'],array('fields'=>'id,uname'));    
        }
		
		$data['connection'] =  $conn->getConnectionDetails($data['connectionID']);
        
		
        return $data;
    }
    
    
      function getCustomRouteDetails($id)
    {
        global $sql;
        $user = new user();
        
        $sql->db_Select("smsRoutingPricingPolicies","*","id='$id'");
        
        $data = execute_single($sql);
        if($data['uid']==0)
        {
             $data['user'] = array("id"=>0,"uname"=>"All users");
        }
        else
        {
            $data['user'] = $user->userDetails($data['uid'],array('fields'=>'id,uname'));    
        }
        
		
        return $data;
    }
    
    function getSearchData($post,$page)
    {
    
        $posted_data = $post['data'];
        $secondaryData = $post['secondaryData'];
        $secondaryData['searchFields'] = $posted_data;
        $secondaryData['searchFieldsLike'] = $post['likeData'];
        $secondaryData['debug']=0;
        $secondaryData['fields']="smsRoutingTable.*,dialing_codes.Country as country,smsConnections.title as connection";
        $secondaryData['checkCustomRoutes']=1;
        $secondaryData['getUserDetails']=1;
        if($secondaryData['dateFrom'])
        {
        	$date = explode("/",$secondaryData['dateFrom']);
        	$date = mktime(0,0,0,$date[1],$date[0],$date[2]);
        	$secondaryData['greaterThan']=array('date_updated'=>$date);	
        	unset($secondaryData['dateFrom']);	
        }
        
        if($secondaryData['dateTo'])
        {
        	
        	$date = explode("/",$secondaryData['dateTo']);
        	
        	$date = mktime(0,0,0,$date[1],$date[0],$date[2]);
        	
        	$secondaryData['lessThan']=array('date_updated'=>$date);
        	unset($secondaryData['dateTo']);	
        }
        
        $secondaryData['return'] = 'paginated';
        $secondaryData['page'] = ($page) ? $page : 1;
        
        return $secondaryData;
    }
    
    function getRouteUpdates()
    {
        global $sql;
        
        $this->download("http://10.11.12.18/smsRoutingTable.csv","smsRoutingTable.csv");
        
        $sql->db_Select("smsRoutingTable","routeUniqueID");
        $list = execute_multi($sql);
        
        $dblist = array();
        foreach($list as $item)
        {
            $dblist[]=$item['routeUniqueID'];    
        }
        
        $query= "CREATE TEMPORARY TABLE `smsRoutingTableTemp` (
          `routeUniqueID` int(11) default NULL,
          `connectionUniqueID` int(11) default NULL,
          `title` varchar(40) default NULL,
          `routingType` varchar(20) default NULL,
          `regex` text,
          `settings` text,
          `active` tinyint(1) default NULL,
          `cost` decimal(12,2) default NULL,
          `countryID` int(11) default NULL,
          PRIMARY KEY  (`routeUniqueID`),
          UNIQUE KEY `routeUniqueID` (`routeUniqueID`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8";
        
        $sql->q($query);
        
        $query = "LOAD DATA INFILE '".$_SERVER['DOCUMENT_ROOT']."/material/smsRoutingTable.csv'
        INTO TABLE smsRoutingTableTemp 
        FIELDS TERMINATED BY ','";  
        
        //echo $_SERVER['DOCUMENT_ROOT']."/material/smsRoutingTable.csv";
        
        $sql->q($query);
        
        $sql->db_Select("smsRoutingTableTemp","routeUniqueID");
        $list = execute_multi($sql);
        
        $templist = array();
        foreach($list as $item)
        {
            $templist[]=$item['routeUniqueID'];    
        }
        
        unset($list);
                
        $deleteList=array_diff($dblist,$templist);
        /*DELETE*/
        $sql->db_Delete("smsRoutingTable","routeUniqueID IN ('".implode("','",$deleteList)."')"); 
        $deleteCount = count($deleteList);
        unset($deleteList);
        
        //$sql->db_Delete("smsRoutingTableTemp","routeUniqueID IN ('".implode("','",$insertList)."')");<br />
        
        /*UPDATE*/
        $updateList=array_intersect($dblist,$templist);
        $sql->db_Update("smsRoutingTable as d,smsRoutingTableTemp as s","
        d.connectionUniqueID=s.connectionUniqueID,
        d.title=s.title,
        d.routingType=s.routingType,
        d.regex=s.regex,
        d.settings=s.settings,
        d.active=s.active,
        d.cost=s.cost,
        d.countryID=s.countryID  WHERE d.routeUniqueID=s.routeUniqueID");
        $updateCount = count($updateList);
        unset($updateList);
        /*INSERT*/
        $insertList=array_diff($templist,$dblist);
        $query = "INSERT INTO smsRoutingTable (routeUniqueID,connectionUniqueID,title,routingType,regex,settings,active,cost,countryID)
        SELECT * FROM smsRoutingTableTemp WHERE routeUniqueID IN ('".implode("','",$insertList)."')";
        $sql->q($query);
        $insertCount = count($insertList);
        unset($insertList);
        
        
        echo "old=".count($dblist);
        echo '<br>';
        echo "new=".count($templist);
        echo '<br>';
        echo "update=".$updateCount;
        echo '<br>';
        echo "delete=".$deleteCount;
        echo '<br>';
        echo "insert=".$insertCount;
        
        
        /*DROP TEMP TABLE*/
        $query = "DROP TEMPORARY TABLE smsRoutingTableTemp";
        
        $sql->q($query);
    }
    
    function getConnectionUpdates()
    {
           //$this->download("http://10.11.12.18/test.csv","Connections.csv");
           global $sql;
           for($i=5001;$i<10000;$i++)
           {
                $cost= rand(100,300)/100;            
                $sql->db_Insert("smsRoutingTable (routeUniqueID,cost)","'$i','$cost'");
           }   
    }
    
    function download($remote, $local)
    {
    	$cp = curl_init($remote);
    	$fp = fopen($local, "w");
    	
    	curl_setopt($cp, CURLOPT_FILE, $fp);
    	curl_setopt($cp, CURLOPT_HEADER, 0);
    	
    	curl_exec($cp);
    	curl_close($cp);
    	fclose($fp);	
    }
}

?>