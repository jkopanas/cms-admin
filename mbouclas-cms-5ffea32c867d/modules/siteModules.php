<?php
class siteModules {
	
	
	/**
	 * Pool of boxids
	 */
	public $boxids = array();	
	
	/**
	 * Return boxes Assign varaibles 
	 * ready to form json
	 */
	public $boxsAssigns = array();	

	/**
	 * Will setup the boxes according to the areas array provided
	 *
	 * @param array $areas
	 */
	public function setupBoxes($areas,$settings=0)
	{
		global $sql;

		//LOOP THROUGH THE AREAS
		foreach ($areas as $areaName =>$v)
		{
		 if ($v) {
		 	$boxes[] = implode(',',$v);
		 }
		}//END LOOP AREAS
		$boxes = implode(',',$boxes);
		$filters = $settings['filters'] = $settings['filters'] = array('active'=>1);
		$templates = $this->getBoxes($boxes,array('debug'=>0,'setupKeys'=>1,'fields'=>$settings['fields'],'query'=>$filters));
		foreach ($areas as $key=>$v)
		{
			foreach ($v as $k=>$val)
			{
				$areas[$key][$k] = $templates[$val];
			}
		}

		return $areas;
	}//END METHOD

	/**
	 * Will return the modules asked
	 *
	 * @param string $ids
	 */
	private function getBoxes($ids,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$q[] = "id IN ($ids)";
		if ($settings['query']) {
			foreach ($settings['query'] as $k=>$v)
			{
				$q[] = "$k = '$v'";
			}
		}
		if ($q) {
			$query = implode(" AND ",$q);
		}
		$sql->db_Select("modules_boxes",$fields,"$query");
		if ($settings['debug']) {
			echo "SELECT $fields FROM modules_boxes WHERE $query<br>";
			echo $sql->mySQLerror;
		}
		if ($sql->db_Rows()) {
			$res = execute_multi($sql);
				
			foreach ($res as $k=>$v)
			{
				$v['settings'] =  json_decode($v['settings'],true);
					
				if ($settings['setupKeys']) {
					$tmp[$v['id']] = $v;
				}
			}
			if ($settings['setupKeys'] AND $tmp) {
				$res = $tmp;
			}
			return $res;
		}

	}//END METHOD

	public function getBox($id,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		if (is_numeric($id)) {
			$q[] = "id = $id";
		}
		elseif (is_array($id)) {
			$q[] = "id IN (".implode(',',$id).")";
		}
		else {
			$q[] = "name = '$id'";
		}
		if ($settings['query']) {
			foreach ($settings['query'] as $k=>$v)
			{
				$q[] = "$k = '$v'";
			}
		}
		if ($q) {
			$query = implode(" AND ",$q);
		}
		$sql->db_Select("modules_boxes",$fields,$query);
		if ($settings['debug']) {
			echo "SELECT $fields FROM modules_boxes WHERE $query<br>";
			echo $sql->mySQLerror;
		}
		if ($sql->db_Rows()) {
			if (is_array($id)) {
				$res = execute_multi($sql);
				foreach ($res as $k=>$v) {
					$res[$k]['settings'] = json_decode($v['settings'],true);
					if ($settings['setupKeys']) {
						$tmp = array();
						$tmp[$v['id']] = $v;
					}
					if ($settings['setupKeys'] AND $tmp) {
						$res[$k] = $tmp;
					}
				}
			}
			else {
				$res = execute_single($sql);		
				$res['settings'] = json_decode($res['settings'],true);
				if ($settings['setupKeys']) {
					$tmp[$v['id']] = $v;
				}
				if ($settings['setupKeys'] AND $tmp) {
					$res = $tmp;
				}
			}
			return $res;
		}
		return array();
	}//END METHOD

	public function getBoxByName($name,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$q[] = "name = '$name'";
		if ($settings['query']) {
			foreach ($settings['query'] as $k=>$v)
			{
				$q[] = "$k = '$v'";
			}
		}
		if ($q) {
			$query = implode(" AND ",$q);
		}
		$sql->db_Select("modules_boxes",$fields,"$query");
		if ($settings['debug']) {
			echo "SELECT $fields FROM modules_boxes WHERE $query<br>";
			echo $sql->mySQLerror;
		}
		if ($sql->db_Rows()) {
			$res = execute_single($sql);
				
			$res['settings'] = $v['settings'] = json_decode($v['settings'],true);
			if ($settings['setupKeys']) {
				$tmp[$v['id']] = $v;
			}
			if ($settings['setupKeys'] AND $tmp) {
				$res = $tmp;
			}
			return $res;
		}
		return array();
	}//END METHOD

	/**
	 * A Method that fetches the box view by box_view_id or by box_view_name
	 * @param string $id
	 * @param array $settings
	 * @return mixed
	 */
	function getBoxView($id,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$q[] = (is_numeric($id) ? "id = $id" :  "name = '$id'");
		if ($settings['query']) {
			foreach ($settings['query'] as $k=>$v)
			{
				$q[] = "$k = '$v'";
			}
		}
		if ($q) {
			$query = implode(" AND ",$q);
		}
		$sql->db_Select("modules_boxes_views",$fields,$query);
		if ($settings['debug']) {
			echo "SELECT $fields FROM modules_boxes_views WHERE $query<br>";
			echo $sql->mySQLerror;
		}
		if ($sql->db_Rows()) {
			$res = execute_single($sql);
			
			$res['settings'] = json_decode($res['settings'],true);
			
			return $res;
		}
		return array();
	} //END METHOD
	
	
	public function getBoxIDs() {
		global $smarty,$loaded_modules;
			$ids = $this->boxids;
		foreach ($ids as $key => $value ) {
				unset($this->boxids[$key]);
				$boxIDs=$this->getBox($value);
				include_once($boxIDs['file']);
		}
		
	}
	
	public function setupMultiBox(array $ar) {
		
		$this->boxids=$ar;
		foreach($ar as $key => $value ) {
			$this->boxsAssigns[$value] = array();
		}
		
	}
	
	
	
	public  function pageBoxes($module='',$page=0,$area="front",$settings=0)
	{
		global $sql;

		$fields = ($settings['fields']) ? $settings['fields'] : "*";

		$q[] = "area = '$area'";
		if ($page) {
			$q[] = "page = '$page'";
		}
		if ($module) {
			$q[] = "module = '$module'";
		}
		if ($settings['filters']) {
			foreach ($settings['filters'] as $k=>$v) {
				$q[] = "$k = '$v'";
			}
		}
		$query = implode(' AND ',$q);
		$sql->db_Select("modules_boxes_positions",$fields,$query);
		if ($settings['debug']) {
			echo "SELECT $fields FROM modules_boxes_positions WHERE $query";
		}
		if ($sql->db_Rows()) {
			if ($page) {
				$res = execute_single($sql);
				$res['settings'] = json_decode($res['settings'],true);
				if ($settings['getBoxes']) {
					$res['boxes'] = json_decode($res['boxes'],true);
					if ($settings['getBoxes'] == 'full') {
						$boxFields = ($settings['boxFields']) ? $settings['boxFields'] : 'id,template,title,settings,name,file';
						$res['boxes'] = $this->setupBoxes($res['boxes'],array('fields'=>$boxFields,'debug'=>0,'filters'=>$settings['boxFilters']));
					}
				}
			}
			else {
				$res = execute_multi($sql);
				foreach ($res as $k=>$v) {
					$res[$k]['settings'] = json_decode($v['settings'],true);
				}
			}
			if ($settings['init']) {
				$this->initBoxes($res['boxes'],$settings);
			}
			return $res;
		}
	}//END METHOD

	public function initBoxes($boxes,$settings) {
		global $loaded_modules,$smarty;
		foreach ($boxes as $k=>$v) {
			foreach ($v as $p) {
				if ($p['fetchType'] == 'static') {
					$me = $p;
					$mySettings = $settings;
					include($p['file']);
				}
			}
		}//END setup
	}//END METHOD
	
}//END CLASS


?>