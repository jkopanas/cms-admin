<?php
include(ABSPATH."/includes/xajax/xajax.inc.php");
$xajax = new xajax();
global $smarty;

$xajax->registerFunction("load_module_settings");
$xajax->registerFunction("save_module_box_settings");



function load_module_settings($id,$resdiv)
{
	global $sql,$smarty;
	$module = get_module_box($id);
	$module['settings'] = form_settings_array($module['settings'],"###",":::");
	$module['pages'] = explode(":::",$module['pages']);
	$module['available_areas'] = explode(":::",$module['available_areas']);
	$sql->db_Select("modules_boxes_positions","*");
	$smarty->assign("files",execute_multi($sql));
	$smarty->assign("module",$module);
	$smarty->assign("res_div",$resdiv);
	$smarty->assign("mid",$id);
	$message = $smarty->fetch("modules/site_modules/admin/module_settings.tpl");
	
	$objResponse = new xajaxResponse();
    $objResponse->addAssign($resdiv,"innerHTML","");
    $objResponse->addAssign($resdiv,"innerHTML",$message);
    return $objResponse;
}

function save_module_box_settings($id,$resdiv,$aFormValues)
{
	global $sql,$smarty;
	print_r($aFormValues);
	$module = get_module_box($id);
	$module['settings'] = form_settings_array($module['settings'],"###",":::");
	$module['pages'] = explode(":::",$module['pages']);
	$module['available_areas'] = explode(":::",$module['available_areas']);
	$sql->db_Select("modules_boxes_positions","*");
	$smarty->assign("files",execute_multi($sql));
	$smarty->assign("module",$module);
	$smarty->assign("res_div",$resdiv);
	$smarty->assign("mid",$id);
	$message = $smarty->fetch("modules/site_modules/admin/module_settings.tpl");
	
	$objResponse = new xajaxResponse();
    $objResponse->addAssign($resdiv,"innerHTML","");
    $objResponse->addAssign($resdiv,"innerHTML",$message);
    return $objResponse;
}

$xajax->processRequests();
$smarty->assign("ajax_requests",$xajax->getJavascript(URL)); 
?>