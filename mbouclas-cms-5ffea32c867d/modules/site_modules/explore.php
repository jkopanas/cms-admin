<?php
$sql->db_Select("modules_boxes","*","file = 'explore.php'");
$box = execute_single($sql);
$box_settings = form_settings_array($box['settings'],"###",":::");

$current_page = ($current_page) ? $current_page : 1;
$start = ($current_page) ? ($current_page*$box_settings['items_per_box'])-$box_settings['items_per_box'] : 0;
$sql->db_Select("news_page_categories","newsid","catid = ".$box_settings['cid']);
$total = $sql->db_Rows();
$pg = new pagination($current_page,$box_settings['items_per_box']);

$pg->setTotalRecords($total);
$settings = ($main_modules['content']['settings']) ? $main_modules['content']['settings'] : $site_modules['content']['settings'];
$smarty->assign("items",get_content_pages($box_settings['cid'],"date_added","DESC",FRONT_LANG,"cat",1,1,$box_settings['items_per_box'],0,$start));
$smarty->assign("explore_box_settings",$settings);
$smarty->assign("list",$pg->getListCurrentRecords());
$smarty->assign("navigation",$pg->getNavigation());
$smarty->assign("num_links",$pg->getCurrentPages());
$smarty->assign("PAGE",$current_page);
$smarty->assign("catid",$box_settings['cid']);
$box_content[$box['id']] = $smarty->fetch($module_boxes['folder']."/".$box['template']);
?>