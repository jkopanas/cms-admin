<?php
function load_site_modules($list,$module_boxes,$page)
{
	global $sql,$loaded_modules,$box_content,$smarty,$main_modules;
	//Assuming there is a list of modules to load we proceed
	$main_modules = array();
	$order = array();

for ($i=0;count($list) >$i;$i++)
	{
		//check if this module is available for this page
		$available_pages = construct_tags_array($list[$i]['pages'],":::");
		if (in_array($page,$available_pages)) //This module is to be loaded on this page
		{		
			//check requirements
			$required_modules = construct_tags_array($list[$i]['required_modules'],":::");

			//now check which modules are running and load the ones missing
			for ($j=0;count($required_modules) > $j;$j++)
			{
				if (!in_array($required_modules[$j],$loaded_modules)) 
				{
					//load missing modules

					$main_modules[$required_modules[$j]] = module_is_active($required_modules[$j],1,1);
				}
			}
			//We now have all the required modules loaded. proceeding by loading the site module
			require_once(ABSPATH."/".$module_boxes['folder']."/".$list[$i]['file']);
			$order[$i] = $list[$i]['id'];
		}//END OF LOADING ON THIS PAGE
		//now get positioning
	}
//print_r($box_content);
	//Check Available areas
//	$areas = construct_tags_array($module_boxes[])

	//All done returning the missing modules list
	$box_order = array();

if ($_COOKIE['boxes']) //USER SET ORDER
{
//echo "FOUND COOKIE";
$test = explode("&",$_COOKIE['boxes']);
$i=0;
foreach ($test as $l)
{
	list($field,$code)=split('=',$l);
//	echo "$field --- $code<br>";
	$box_order[$i]['mid'] = str_replace("box_",'',$code);
	$box_order[$i]['area'] = str_replace('[]','', str_replace('sort_','',$field));
	$areas[] = $box_order[$i]['area'];
	$i++;
}

//echo "<br>".$_COOKIE['boxes']."<br>";
$areas =array_unique($areas);
$available_areas = array_merge(array(),$areas);

//print_r($areas);
}//END USER ORDER
else 
{
	$sql->db_Select("modules_boxes_positions","*","page = '$page'");
	$pos = execute_single($sql);
if ($sql->db_Rows()) 
{
//	$box_order = form_settings_array($pos['modules'],"###",":::");//THIS IS THE BOX ORDER
	$tmp = explode("###",$pos['modules']);

	for ($i=0;count($tmp) > $i;$i++)
	{
		$t = explode(":::",$tmp[$i]);
		$box_order[$i]['mid'] = $t[0];
		$box_order[$i]['area'] = $t[1];
	}
	$available_areas = construct_tags_array($pos['areas'],":::");
}//END OF DEFAULT ORDER
}//END FOUND RESULTS
//	print_r($box_order);
	//GET AVAILABLE AREAS FOR THIS PAGE
	
	//Loop Through Areas

	
	for ($i=0;count($available_areas) > $i;$i++)
	{
		$tmp = array();
	//loop available boxes

		for ($j=0;count($box_order) > $j;$j++)
		{
//echo $available_areas[$i]." --- ".$box_order[$j]['mid']."<Br>";
			if ($box_order[$j]['area'] == $available_areas[$i]) 
			{
//				echo "<Br>".$available_areas[$i]." --- ".$box_order[$j]['mid']."<Br>";
//				echo $box_content[$box_order[$j]['mid']];
				$tmp[$j] = get_module_box($box_order[$j]['mid']);
				$tmp[$j]['content'] = $box_content[$box_order[$j]['mid']];
//				echo $box_order[$j]['mid']."<Br>";
			}
			
		}

			$smarty->assign("area_".$available_areas[$i],$tmp);

	}

//NOW SORT THE LOADED BOXES ACCORDING TO THE DB ORDER



	return $main_modules;
}

function get_module_box($id)
{
	global $sql;
	
	$sql->db_Select("modules_boxes","*","id = $id");
	return execute_single($sql);
}

?>