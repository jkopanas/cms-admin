<?php
// ------------------- 
// INCLUDES
// -------------------
include_once($_SERVER['DOCUMENT_ROOT']."/includes/weather/class.xml.parser.php");
include_once($_SERVER['DOCUMENT_ROOT']."/includes/weather/class.weather.php");

$days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');

// ------------------- 
// LOGIC
// -------------------
// Create the new weather object!
// CIXX0020 = Location Code from weather.yahoo.com
// 3600     = seconds of cache lifetime (expires after that)
// C        = Units in Celsius! (Option: F = Fahrenheit)

$timeout=3*60*60;  // 3 hours
if (isset($_ENV["TEMP"]))
  $cachedir=$_ENV["TEMP"];
else if (isset($_ENV["TMP"]))
  $cachedir=$_ENV["TMP"];
else if (isset($_ENV["TMPDIR"]))
  $cachedir=$_ENV["TMPDIR"];
else
// Default Cache Directory  
  $cachedir="/tmp";
  
$cachedir=str_replace('\\\\','/',$cachedir);
if (substr($cachedir,-1)!='/') $cachedir.='/';

$weather_chile = new weather("GRXX0046", 3600, "c", $cachedir);

// Parse the weather object via cached
// This checks if there's an valid cache object allready. if yes
// it takes the local object data, what's much FASTER!!! if it
// is expired, it refreshes automatically from rss online!
$weather_chile->parsecached(); // => RECOMMENDED!

$date['today'] = date("l");
$date['tomorrow'] =  strftime("%A",mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
$date['day_after'] =  strftime("%A",mktime(0, 0, 0, date("m")  , date("d")+2, date("Y")));

$smarty->assign("weather",$weather_chile->forecast);
$smarty->assign("date",$date);


$sql->db_Select("modules_boxes","*","file = 'weather.php'");
$box = execute_single($sql);
$box_settings = form_settings_array($box['settings'],"###",":::");
$smarty->assign("box_settings",$box_settings);
$box_content[$box['id']] = $smarty->fetch($module_boxes['folder']."/".$box['template']);
?>