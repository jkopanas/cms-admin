<?php
include("../manage/init.php");//load from manage!!!!
include($_SERVER['DOCUMENT_ROOT']."/modules/common_functions.php");//load from manage!!!!
$settings = form_settings_array($_POST['settings']);
$current_module = $loaded_modules[$settings['module']];
//error_message($_POST);
if ($_GET['mode'] == 'CheckFile') {
$targetPath = $_SERVER['DOCUMENT_ROOT'] . $current_module['settings']['images_path']."/".$current_module['name']."_".$settings['itemid']."/";

$fileArray = array();
foreach ($_POST as $key => $value) {
	if ($key != 'folder') {
		if (file_exists($targetPath."/" . $value)) {
			$fileArray[$key] = $value;

		}
	}
}

echo json_encode($fileArray);
exit();
}

if (!empty($_FILES)) {
    $media = (!$settings['media']) ? 'image' : $settings['media'];
    $MediaFilesSettings = MediaFiles(array('single'=>1,'type'=>$media)); 
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $MediaFilesSettings['folder']."/".$current_module['name']."_".$settings['itemid']."/";
	error_message($targetPath);
    if (!is_dir($targetPath)) {
    mkdir($targetPath,0755);
    }//END IF
	$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];
	$fileName =  $_FILES['Filedata']['name'];
	
	if (!is_dir($targetPath)) {
		mkdir($targetPath,'0755');
	}

	if (move_uploaded_file($tempFile,$targetFile)) {
	    
		/*
		* $settings['ImageSet'] SHOULD BE AN ARRAY CONTAINING THE IDS OF THE IMAGE TYPES TO BE PROCESSED. IF EMPTY PROCESS ALL IMAGE TYPES
		* $settings['CustomImageSet'] BOOLEAN THAT DEFINES WHETHER THE DEFAULT IMAGE SET WILL BE CREATED. IF SET TO FALSE ONLY REQUIRED SIZES WILL BE PROCESSED
		*/
		if ($current_module['settings']['keep_original_image'] AND $settings['action'] != 'ReplaceImageCopy' AND $media == 'image') {
			if (!is_dir($targetPath."/originals")) {
				mkdir($targetPath."/originals",0755);
			}
				copy($targetFile,$targetPath."/originals/".$fileName);
		}
		//OVERRIDE DEFAULT SET
		foreach ($settings as $k=>$v)
		{
			if (preg_match('/^ImageSet([0-9]*)/',$k)) {
				$settings['ImageSet'][] = $v;
			}
		}
//		print_r($settings);
		######### CHECK FILE TYPE FOR ACTIONS ############
		if ($settings['action'] == 'Savethumb') {
		$settings_arr = array (
		'lang' => DEFAULT_LANG,
		'module' => $current_module,
		'filename' => $fileName,
		'upload_dir' => $targetPath,
		'file_type' => 'Thumb',
		'type' => '0',
		'ImageSet' => $settings['ImageSet'],
		'itemid' => $settings['itemid'],
		'ImageFix' => $settings['ImageFix'],
		'uid' => ID,
		'date_added' => time(),
		'image_types' => $image_types,
		'CustomImageSet' => $settings['CustomImageSet'],
		'ExistingThumb' => $settings['ExistingThumb']
		);
			
		$a = new HandleUploadedItem($settings_arr);
			
		}//END THUMB
		elseif ($settings['action'] == 'ReplaceImageCopy')
		{
			$settings_arr = array (
			'lang' => DEFAULT_LANG,
			'module' => $current_module,
			'filename' => $fileName,
			'upload_dir' => $targetPath,
			'file_type' => 'ReplaceCopy',
			'itemid' => $settings['itemid'],
			'ImageID' => $settings['ImageID'],
			'ImageCopy' => $settings['ImageCopy'],
			'ImageFix' => $settings['ImageFix'],
			'uid' => ID,
			'date_added' => time()
			);
			$a = new HandleUploadedItem($settings_arr);
		}
		elseif ($settings['action'] == 'Saveimage')
		{
			$settings_arr = array (
			'lang' => DEFAULT_LANG,
			'module' => $current_module,
			'filename' => $fileName,
			'upload_dir' => $targetPath,
			'file_type' => 'images',
			'type' => $settings['ImageCategory'],
			'ImageSet' => $settings['ImageSet'],
			'itemid' => $settings['itemid'],
			'ImageFix' => $settings['ImageFix'],
			'uid' => ID,
			'date_added' => time(),
			'image_types' => $image_types,
			'CustomImageSet' => $settings['CustomImageSet']
			);

		$a = new HandleUploadedItem($settings_arr);
			
		}//END IMAGES
		elseif ($settings['action'] == 'SaveCopy')
		{
			$settings_arr = array (
			'lang' => DEFAULT_LANG,
			'module' => $current_module,
			'filename' => $fileName,
			'upload_dir' => $targetPath,
			'file_type' => 'images',
			'type' => $settings['ImageCategory'],
			'ImageSet' => $settings['ImageSet'],
			'itemid' => $settings['itemid'],
			'ImageFix' => $settings['ImageFix'],
			'uid' => ID,
			'date_added' => time(),
			'image_types' => $image_types,
			'CustomImageSet' => $settings['CustomImageSet']
			);

		$a = new HandleUploadedItem($settings_arr);
			
		}//END IMAGES
		elseif ($settings['action'] == 'Savedocs')
		{
			
		}//END DOCUMENTS
		elseif ($settings['action'] == 'Savevideos')
		{
			
		}//END VIDEO
        elseif ($settings['action'] == 'SimpleUpload')//Uploads files from filemanager. No database saves
		{
		 
            $settings_arr = array (
			'lang' => DEFAULT_LANG,
			'module' => $current_module,
			'filename' => $fileName,
			'upload_dir' => $targetPath,
			'media' => $settings['media'],
            'file_type' => $settings['action'],
			'type' => $settings['ImageCategory'],
			'ImageSet' => $settings['ImageSet'],
			'itemid' => $settings['itemid'],
			'ImageFix' => $settings['ImageFix'],
			'uid' => ID,
			'date_added' => time(),
			'image_types' => $image_types,
			'CustomImageSet' => $settings['CustomImageSet']
			);
            
        if ($settings['media'] == 'image' OR $settings['media'] == 'document') {
        $settings['return'] = 'SimpleUpload';
        }//END IF
        
		$a = new HandleUploadedItem($settings_arr);
		}//END SIMPLE UPLOAD
        elseif ($settings['action'] == 'SaveMedia')//Uploads DOCUMMENTS - VIDEOS - AUDIO
		{
		 
            $settings_arr = array (
			'lang' => DEFAULT_LANG,
			'module' => $current_module,
			'filename' => $fileName,
			'upload_dir' => $targetPath,
			'media' => $settings['media'],
            'file_type' => $settings['action'],
			'type' => $settings['ImageCategory'],
			'ImageSet' => $settings['ImageSet'],
			'itemid' => $settings['itemid'],
			'ImageFix' => $settings['ImageFix'],
			'uid' => ID,
			'date_added' => time(),
			'image_types' => $image_types,
			'CustomImageSet' => $settings['CustomImageSet']
			);
          
        if ($settings['media'] == 'image' OR $settings['media'] == 'document' OR $settings['media'] == 'video') {
        $settings['return'] = 'SimpleUpload';
        }//END IF
        
		$a = new HandleUploadedItem($settings_arr);
		}//END SIMPLE UPLOAD  
          
		###################### RETURN STUFF ##########################
		if ($settings['return'] == 'Thumb') {
			$b = $a->ReturnDetails();
			echo "1|".$b['thumb']['CleanDir']."/".$b['thumb']['name']."|".$b['thumb']['imageid'];//SUCCESS
		}
        elseif ($settings['return'] == 'SimpleUpload')
        {
            $b = $a->ReturnDetails();
            echo "1|".json_encode($b);//SUCCESS
        }//END SIMPLE UPLOAD
		elseif ($settings['return'] == 'ReplaceImageCopy')
		{
			$b = $a->ReturnDetails();
			if ($b[key($b)] !== 'error') {
				echo "1|".$b[key($b)]['CleanDir']."/".$b[key($b)]['name']."|".$b[key($b)]['imageid']."|".key($b);//SUCCESS			
			}
			else {
				echo "1|error";
			}
			
		}

	}
	else {
		echo "ERROR MOVING FILE";
	}

}//END FILES

?>