<?php

/**
 * Adds functionality needed by the user module
 * @package MCMS
 * @author Michael Bouclas
 * @copyright 2011
 * @version $Id$
 * @access public
 */

class user {
	
	public $settings;
	public $module;
	public $uid;
	public $tables;
	public $tablesDetailed;


	/**
	 * Constructor
	 *
	 * @return user
	 */
	function user()
	{
		
		$this->tables['users'] = array('table'=>'users','key'=>'id');
		$this->tables['profile'] = array('table'=>'users_profile','key'=>'id');
		$this->tables['online'] = array('table'=>'users_online');
		$this->tables['comments'] = array('table'=>'users_comments','key'=>'uid');
		$this->tables['points'] = array('table'=>'users_points','key'=>'uid');
		$this->tables['points_list'] = array('table'=>'users_points_list','key'=>'');
		$this->tables['votes'] = array('table'=>'users_votes','key'=>'uid');
		$this->tables['item_votes'] = array('table'=>'users_items_votes','key'=>'');
		$this->tables['images'] = array('table'=>'users_images','key'=>'uid');
		$this->tables['albums'] = array('table'=>'users_albums','key'=>'uid');
		$this->tables['harvester'] = array('table'=>'user_harvester','key'=>'');
		$this->tables['favorites'] = array('table'=>'users_favorites','key'=>'uid');
		$this->tables['audience'] = array('table'=>'users_audience','key'=>'uid');
		$this->tables['lists'] = array('table'=>'users_audience_lists','key'=>'uid');
		$this->tables['listMembers'] = array('table'=>'users_audience_list_members','key'=>'uid');
		$this->tables['audenceTemp'] = array('table'=>'users_audienceTemp','key'=>'uid');
		$this->tables['memberLists'] = array('table'=>'users_audience_list_members','key'=>'uid');
		
	}//END FUNCTION


    function UpdateUser ($settings) {
    	 global $sql;
    	
    	foreach ($settings as $key => $value ) {	
    		
    		foreach ($value['where'] as $k => $v) {
    			$where[] = $k ."='".$v."'"; 
    		}
    		$w = implode(",",$where);
    	    foreach ($value['field'] as $k => $v) {
    	    	if ( $k == "settings" ) {
    				$sql->db_Select($key,"settings",$w);
    				$row=execute_single($sql);
    				$t=array_merge(json_decode($row['settings'],true),$v);			
    				$field[] = $k ."='".json_encode($t)."'"; 
    	    	} else {
    	    		$field[] = $k ."='".$v."'"; 
    	    	}
    		}
    		$f = implode(",",$field);
    		 $sql->db_Update($key, $f." WHERE ".$w);
    		
    	}
    		
    }
	
	
	
/**
 * grabs data for a signle user
 *
 * @param int $id
 * @param array $settings
 * @return user array
 */
function userDetails($id=0,$settings=0)
{
	global $sql,$loaded_modules;
	$fields = (!$settings['fields']) ? "*" : $settings['fields'];
	if ($id) {
		$q[] = "id = $id";
	}
	
	if (is_array($q)) {
		$query = implode(" AND ",$q);
	}
	$sql->db_Select($this->tables['users']['table'],$fields,$query);
	if ($sql->db_Rows() > 0) 
	{

		$user = execute_single($sql,1);
		if ($settings['subscriptions']) 
		{
			//get subscribtions
			$user['subscriptions'] = $this->userSubscriptions($id);	
		}//END SUBSCRIPTIONS
		if ($settings['images']) {
			$images = new userImages();
            
            $settings = array('uid'=>$id);
            $user['images'] = $images->getImages($settings);
            
            
		}//END USER IMAGES
		
		if ($settings['items']) {
			
		}//END USER ITEMS
		if ($settings['comments']) {
			$comments = new UserComments();
            $settings=array('searchFields'=>array('itemid'=>$id,'module'=>'users'),'getUserDetails'=>1,'orderby'=>'date_added','way'=>'desc','return'=>'paginated','results_per_page'=>5,'debug'=>0);
            $user['comments'] = $comments->getComments($settings);
		}
		if ($settings['favorites']) {
			
		}
		if ($settings['votes']) {
			
		}
		if ($settings['points']) {
			
		}

		if ($settings['profile']) {
			$user['profile'] = $this->userProfile($id,$settings);
		}
        if ($settings['efields']) {
            $Efields = new ExtraFields($this->module);
            $user['efields'] = $Efields->getExtraFieldValuesByItemID($id);
        }
        
	}//END ROWS
	return $user;
}//END FUNCTION

/**
 * The users profile 
 *
 * @param int $id
 * @param array $settings
 * @return array
 */
function userProfile($id,$settings=0)
{
	global $sql;
	
	$fields = ($settings['field']) ? $settings['field'] : "*";
	$sql->db_Select($this->tables['profile']['table'],$fields,"id = $id");
	if ($sql->db_Rows()) {
		if ($settings['quick']) {//GET RAW DATA
			$profile = execute_single($sql);
			if ($settings['efields']) {
				
			}
		}//END QUICK
		else {//ANALYZE THE PROFILE
			$profile = execute_single($sql);
			$profile['profile'] = json_decode($profile['profile'],true);
			$profile['settings'] = json_decode($profile['settings'],true);
			if ($profile['friends_list'] AND $settings['friends_list']) {
				$profile['friends_list'] = json_decode($profile['friends_list'],true);
				if ($settings['fullFriendsDetails']) {
					for ($i=0;$profile['friends_list'] > $i;$i++) {
						$profile['friends_list'][$i] = $this->userDetails($profile['friends_list'][$i]);
					}
				}
			}//END FRIENDS
		}//END FULL
		return $profile;
	}//END FOUND
	
}//END FUNCTION

/**
 * gets the users subscriptions
 *
 * @param int $id
 * @return array
 */
function userSubscriptions($id)
{
	return $res;
}


/**
 * gets the users comments
 *
 * @param array $settings
 * @return array
 */
function userComments($settings)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
	$way = ($settings['way']) ? "  ".$settings['way'] : "";
	$limit = ($settings['limit']) ? "LIMIT ".$settings['limit'] : "";
	
	if ($settings['id']) {//get for one item only
		$sql->db_Select($this->tables['comments']['table'],$fields,"module = '".$settings['module']."' AND itemid = ".$settings['id'].$settings['orderby']);
		if ($settings['debug']) {
			echo "SELECT  * FROM ".$this->tables['comments']['table']." WHERE module = '".$settings['module']."'  AND itemid = ".$settings['id'].$settings['orderby'];
		}
		if ($settings['count']) {
			return $sql->db_Rows();	
		}
		else {
		if ($sql->db_Rows()) {
			$comments = execute_multi($sql);
			if ($settings['get_user']) {//Get the user data as well
				for ($i=0; count($comments) > $i;$i++)
				{
					$comments[$i]['user'] = $this->userDetails($comments[$i]['uid'],$settings);
				}
			}//END USER DATA
			return $comments;
		}//END ROWS
		}

	}//END SINGLE ITEM
	else { //MULTIPLE ITEMS
		$q[] = "module = '".$settings['module']."'";
		
		if ($settings['uid']) {
			$q[] = "uid = ".$settings['uid'];
		}
		if ($settings['itemid']) {
			$q[] = "itemid = ".$settings['itemid'];
		}
		if ($settings['status']) {
			$q[] = "status = ".$settings['status'];
		}
		if ($q) {
			$query = implode(" AND ",$q);
		}
		$sql->db_Select($this->tables['comments']['table'],$fields,"$query $orderby $way $limit");
			if ($settings['debug']) {
				echo "SELECT  $fields FROM users_comments WHERE $query $orderby $way $limit";
			}
		if ($sql->db_Rows()) {
			$comments = execute_multi($sql);
			if ($settings['get_user']) {//Get the user data as well
				for ($i=0; count($comments) > $i;$i++)
				{
					$comments[$i]['user'] = $this->userDetails($comments[$i]['uid'],$settings);
				}
			}

			if ($settings['get_item']) {
				$item = new Items(array('module'=>$settings['module']));
				for ($i=0; count($comments) > $i;$i++)
				{
					$comments[$i]['item'] = $item->GetItem($comments[$i]['itemid'],$settings);
				}
			}
			return $comments;
		}//END ROWS
	}
	if ($settings['uid']) {//COMMENTS FROM THIS USER
		
		$sql->db_Select($this->tables['comments']['table'],$fields,"uid = ".$settings['uid']." AND module = '".$settings['module']."' AND status = ".$settings['status']." $orderby $way $limit");
		if ($sql->db_Rows()) {
			$comments = execute_multi($sql);
			if ($settings['get_item']) {
				for ($i=0;count($a) > $i;$i++)
				{
					$a[$i]['item'] = $this->userDetails($comments[$i]['uid'],$settings);
				}
			}
			return $comments;
		}//END ROWS
	}//END USER
	
}//END FUNCTION

/**
 * Filters users based on criteria
 * return should be multi or paginated
 * filters is array of key=val
 * searchFields is array of key=>val
 * @param array $settings
 * @return array
 */
function searchUsers($settings=null)
{
	global $sql;
	$search_tables[] = $this->tables['users']['table'];
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
	$way = ($settings['way']) ? "  ".$settings['way'] : "";
	$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
	$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
	
	if (is_array($settings['searchFields'])) {
		foreach ($settings['searchFields'] as $k => $v) {
			$q[] = "$k = '$v'";
		}
	}//END SETUP SEARCH FIELDS
	if (is_array($settings['searchFieldsIN'])) {
		foreach ($settings['searchFieldsIN'] as $k => $v) {
			$q[] = "$k  IN ($v)";
		}
	}//END SETUP SEARCH FIELDS WITH IN
	if (is_array($settings['searchFieldsINMulti'])) {
		foreach ($settings['searchFieldsINMulti'] as $v) {
			foreach ($v as $k=>$b) {
				$q[] = "$k  IN ($b)";
			}
			
		}
	}//END SETUP SEARCH FIELDS WITH IN
    if (is_array($settings['searchFieldsLike'])) {
    	foreach ($settings['searchFieldsLike'] as $k => $v) {
   			$q[] = "$k LIKE '%$v%'";
    	}
    }//END SETUP SEARCH FIELDS WITH LIKE
    
    if (is_array($settings['searchFieldsNotLike'])) {
    	foreach ($settings['searchFieldsNotLike'] as $k => $v) {
   			$q[] = "$k NOT LIKE '%$v%'";
    	}
    }//END SETUP SEARCH FIELDS WITH NOT LIKE  
    
    if (is_array($settings['searchFieldsBetween'])) {
		foreach ($settings['searchFieldsBetween'] as $k => $v) {	
				$q[] = "$k  between $v[0] and $v[1] ";
		}
	}//END SETUP SEARCH FIELDS WITH Between
	
    if ($settings['JoinInner']) {
		$search_tables[] = "LEFT JOIN ".$this->tables['profile']['table'] ." ON (".$this->tables['users']['table'].".id = ".$this->tables['profile']['table'] .".id)";
    }// SEARCH PROFILE
    
  
    $filters = $settings["filters"];	

		if (isset($filters['missing'])) 
		{
			$q[] = $filters['missing']." IS NULL ";//UNTESTED
		}
		if (isset($filters['custom_field'])) 
		{
			$q[] = $filters['custom_field']." LIKE '%".$filters['custom_value']."%'";
		}
		if ($filters['user_class_deny']) 
		{
			$tmp = explode(",",$filters['user_class_deny']);
			if (count($tmp) == 1) 
			{
				$q[] = "user_class != '".$filters['user_class_deny']."'";
			}
			else 
			{
				foreach ($tmp as $v)
				{
					$q[] = "user_class != ".$v."";	
				}
			}	
		}
		if ($q) {
			$query = implode(" AND ",$q);
		}
				if (!$query) 
				{
					$query_mode='no_where';
				} else {
					$query_mode = 'default';
				}
				
			if ($settings['return'] == 'multi') //Return all results, NO PAGINATION
			{
			
				$search_tables = implode(" ",$search_tables);
				$sql->db_Select($search_tables,$fields,"$query $orderby $way $limit",$query_mode);
				if ($settings['debug']) {
					echo "SELECT $fields FROM ".$search_tables." WHERE $query $orderby $way $limit<br>";
				}
				if ($sql->db_Rows()) 
				{
					
					$tmp = execute_multi($sql);
	
					foreach ($tmp as $k =>  $v) {
						$res[] = $this->userDetails($v['id'],$settings);
					
					}

					return $res;
								
				}
				
			}//END ALL RESULTS
			elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
			{
				$sql->db_Select($this->tables['users']['table'],'count(id) as total',"$query",$query_mode);//GET THE TOTAL COUNT
				
					if ($settings['debug']) 
					{
						echo "SELECT count(id) as total FROM ".$this->tables['users']['table']." WHERE $query $orderby $way<br>";
					}
				if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
				{
					$a = execute_single($sql);
					$total = $a['total'];
					$current_page = ($settings['page']) ? $settings['page'] : 1;
					$results_per_page =  $settings['results_per_page'];
					if (isset($settings['start'])) 
					{
						$start = $settings['start'];
					}
					else {
						$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
					}
					$limit = "LIMIT $start,".$results_per_page;
					$sql->db_Select($this->tables['users']['table'],"id","$query $orderby $way $limit",$query_mode);
					if ($settings['debug']) {
						echo "<br>SELECT id FROM ".$this->tables['users']['table']." WHERE $query $orderby $way $limit";
					}
					$tmp = execute_multi($sql,1);
					paginate_results($current_page,$results_per_page,$total);
					foreach ($tmp as $v) {
						$res[] = $this->userDetails($v['id'],$settings);
					}
					return $res;
					
				}//END FOUND RESULTS
			}//END PAGINATION
		elseif ($settings['return'] == 'count') // RETURN COUNT RESULTS
		{
			$search_tables = implode(" ",$search_tables);
			$sql->db_Select($search_tables,"count(".$this->tables['users']['table'].".id) as total","$query ",$query_mode);
			if ($settings['debug']) {
				echo "SELECT count(".$this->tables['users']['table'].".id) as total FROM ".$this->tables['users']['table']." WHERE $query  $limit<br>";
			}
			$res = execute_single($sql);
			return $res['total'];
		}
		elseif ($settings['return'] == 'single')
		{
				$sql->db_Select($this->tables['users']['table'],$fields,"$query $orderby $way $limit",$query_mode);
				
				if ($settings['debug']) {
					echo "SELECT $fields FROM ".$this->tables['users']['table']." WHERE $query $orderby $way $limit<br>";
				}
				if ($sql->db_Rows()) 
				{
					$tmp = execute_single($sql);
					$res = $this->userDetails($tmp['id'],$settings);
					return $res;
				}
		}
}//END FUNCTION

function selectAllUsers()
{
	global $sql;
	$sql->db_Select($this->tables['users']['table'],'id');
	return $sql->db_Rows();
}

/**
 * grabs the users points
 *
 * @param int $itemid
 * @param int $uid
 * @param int $settings
 * @return array
 */
function userVotes($itemid,$uid,$settings)
{
	global $sql;
		$sql->db_Select($this->tables['votes']['table'],"*","itemid = $itemid AND module = '".$settings['module']."' AND uid = $uid");
//		echo "SELECT * FROM users_votes WHERE itemid = ".$settings['id']." AND module = '".$settings['module']."' AND uid = ".$settings['uid'];
		if ($sql->db_Rows()) {
			if ($settings['check']) {
				return 1;
			}
			else {return execute_single($sql);}
	}

}//END FUNCTION


function get_users_points_list($settings=0)
{
	global $sql;
	if ($settings['orderby']) {
		$sql->db_Select("users_points_list","*","ORDER BY ".$settings['orderby']." ".$settings['way'],$mode="no_where");
	}
	else {
		$sql->db_Select("users_points_list","*");
	}

	if ($sql->db_Rows()) {
		if ($settings['form_array']) {
			$v = execute_multi($sql);
			for ($i=0;count($v) > $i;$i++)
			{
				$ar[$v[$i]['var']] = $v[$i];
			}
			return $ar;
		}
		else {
			
			return execute_multi($sql);
		}
		
	}
}

function removePoints($points_list,$settings) 
{
	
	global $sql;
	
	foreach ($settings as $key => $value ) {
		$q[] = $key . "='". $value. "'";
	}
	$sql->db_Delete($this->tables['points']['table'],implode(" and ",$q));
	if (!isset($settings['action'])) {
		$sql->db_Update("users_profile","points = 0 WHERE id = ".$settings['uid']);
	} else {
		$sql->db_Update("users_profile","points = points - ".$points_list[$settings['action']]['value']." WHERE id = ".$settings['uid']);
	}
}


function addPoints($points_list,$settings)
{
	global $sql;
	//First check if this is a unique action
	if ($points_list[$settings['action']]['unique_per_page'])//NOW CHECK IF THIS ACTION HAS ALREADY BEEN DONE
	{
	
		$sql->db_Select($this->tables['points']['table'],"id","uid = ".$settings['uid']." AND itemid = '".$settings['id']."' AND module = '".$settings['module']."' AND action = '".$settings['action']."'");
		if ($settings['debug']){
		echo "SELECT id FROM users_points WHERE id = ".$settings['uid']." AND itemid = '".$settings['id']."' AND module = '".$settings['module']."' AND action = '".$settings['action']."'";
		}
		if ($sql->db_Rows()) {//Action has been already made. no go
			return 0;
		}
		else {//FIRST TIMER. ADD POINTS
			
			$sql->db_Insert($this->tables['points']['table'],"'',".$settings['uid'].",'".$settings['id']."','".$settings['module']."','".$settings['action']."',".$points_list[$settings['action']]['value'].",".time());
		//update the users profile table
			$sql->db_Update("users_profile","points = points + ".$points_list[$settings['action']]['value']." WHERE id = ".$settings['uid']);
		
		}//END ADD ACTIOM
	}
	else {
	
		$sql->db_Insert($this->tables['points']['table'],"'',".$settings['uid'].",'".$settings['id']."','".$settings['module']."','".$settings['action']."',".$points_list[$settings['action']]['value'].",".time());
		#print_ar($sql);
		//update the users profile table
		$sql->db_Update("users_profile","points = points + ".$points_list[$settings['action']]['value']." WHERE id = ".$settings['uid']);

	}
	
}//END FUNCTION

function userFavorites($settings)
{
	global $sql,$loaded_modules;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$item_q = ($settings['itemid']) ? " AND itemid = ".$settings['itemid'] : "";
	$limit = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby']." ".$settings['way'] : "";
	$sql->db_Select($this->tables['favorites']['table'],$fields,"uid = ".$settings['uid']." AND module = '".$settings['module']."' $item_q $orderby $limit");
	if ($settings['debug']) {
		echo "SELECT $fields FROM users_favorites WHERE uid = ".$settings['uid']." AND module = '".$settings['module']."' $item_q";
	}
	if ($sql->db_Rows()) {
		if ($settings['analyze']) {
			$a = execute_multi($sql);
			$items = new Items(array('module'=>$loaded_modules[$settings['module']]));
			for ($i=0;count($a) > $i;$i++)
			{
				$a[$i]['item'] = $items->GetItem($a[$i]['itemid'],array('active'=>1));
			}
			return $a;
		}//end ANALYZE
		else {
			return execute_multi($sql);
		}
		
	}
	else {
		return 0;
	}
}//END FUNCTION

/**
 * add or modifies a user
 * $data is the $_POST data usually
 * 
 * @param array $data 
 * @param array $settings
 * @return boolean
 */
function addUser($data,$settings=0)
{
	global $sql,$loaded_modules;
	$t = new Parse();

	 foreach ($data as $key => $value) 
	 {   
	 	if ($key != "settings" ) {
		 	$data[$key] = $t->toDB($value);
	 	} else {
	 		$data['settings'] = json_encode($value,true);
	 	}
	 }//END OF FOREACH
 	
	if (!empty($data['pass'])){
 		$data['pass'] = text_crypt($data['pass']);
 	}
 		
 	$data['user_join'] = time();
 	//$data['user_image'] = $loaded_modules['users']['settings']['default_user_image'];
 	$data['admin'] = ($data['user_class'] != 'A' OR !$data['user_class']) ? 0 : 1;
	$data['user_prefs'] = ($data['user_prefs']) ? json_encode($data['prefs']) : '';
	$settings['action'] = ($settings['action']) ? $settings['action'] : 'modify';
	if ($settings['action'] == 'addUser') 
	{ 
		foreach ($data as $k => $v)
		{
			$keys[] = $k;
			$values[] = "'$v'";
		}
		
		$sql->db_Insert($this->tables['users']['table']." (".implode(",",$keys).")",implode(",",$values));
		
		if ($settings['debug']) {
			echo "INSERT INTO ".$this->tables['users']['table']." (".implode(",",$keys).") VALUES (".implode(",",$values).")";
		}
	 	$uid = $sql->last_insert_id;
	 	
	 	if ($settings['use_profile']) {
	  		//Create user profile
	  		$settings['use_profile']['id'] = $uid;
			$this->AddUserProfile($settings['use_profile']);
	 	}
	
	 	$verification_url = URL."/verify_$uid"."_".text_crypt($data['user_join']).".html";
	 	$extra_find[] = '#USERNAME#';$extra_replace[] = $data['uname'];
	 	$extra_find[] = '#VERIFICATION_URL#';$extra_replace[] = $verification_url;
	 	//$this->sendMailToUser(array('namefrom' => COMPANY_NAME,'mailfrom' => COMPANY_EMAIL,'mailto' =>$data['email'],'mail_subject' => process_text($loaded_modules['users']['settings']['verification_mail_subject'],$extra_find,$extra_replace),'mailmessage' => process_text($loaded_modules['users']['settings']['verification_mail_body'],$extra_find,$extra_replace)));
		 $user_folder = ABSPATH . "/media_files/users/" . $uid;
         mkdir($user_folder, 0755);
	 	 $sql->db_Update($this->tables['users']['table'],"user_folder = '$user_folder' WHERE id = $uid");
		return $uid;
	}//END ADD
elseif ($settings['action'] == 'modifydUser')
{
	foreach ($data as $k=>$v)
	{
		if ($k != "settings")  {
			$updateQuery[] = "$k = '$v'";
		} else {
			$sql->db_Select($this->tables['users']['table'],"*","id=".$data['id']);
			$row=execute_single($sql);
			$ar=json_decode($v,true);
			$row['settings'] = array();
			foreach($ar as $key => $value) {
				$row['settings'][$key] = $value;
			}
			$updateQuery[] = $k ."= '".json_encode($row['settings'])."'";
		}
	}
	$sql->db_Update($this->tables['users']['table'],implode(",",$updateQuery)." WHERE id = ".$data['id']);
	
	if ($settings['debug']) {
		echo "UPDATE ".$this->tables['users']['table']." SET ".implode(",",$updateQuery)." WHERE id = ".$data['id'];
	}
}//END UPDATE
return true;
	
}//END FUNCTION


public function AddUserProfile($data) {
	
	$sql->db_Insert($this->tables['profile']['table'],$data['id']."','','','','',''");
	
}


function bulkModifyUser($data,$settings=0)
{
	global $sql;
	
	if ($settings['action'] == 'activate') {
		$sql->db_Update($this->tables['users']['table'],"user_login = 1 WHERE id IN (".implode(",",$data).")");
	}//END ACTIVATE
	elseif ($settings['action'] == 'deactivate') {
		$sql->db_Update($this->tables['users']['table'],"user_login = 0 WHERE id IN (".implode(",",$data).")");
	}//END DEACTIVATE
	elseif ($settings['action'] == 'delete') {

		foreach ($this->tables as $v)
		{ 
			$sql->db_Delete($v['table'],$v['key']." IN (".implode(",",$data).")");
		}
	}//END DELETE
	
}//END FUNCTION

/**
 * Sends a mail to the user
 *
 * @return boolean
 */
function sendMailToUser($settings)
{

	$mailer = new PHPMailer();
	$mailer->IsHTML(true);
	$mailer->From = $settings['mailfrom'];
	$mailer->FromName = $settings['namefrom'];
	$mailer->Subject = $settings['mail_subject'];
	$mailer->Body = $settings['mailmessage'];
	$mailer->CharSet ="utf-8";
	$mailer->AddAddress($settings['mailto']);
  	if($mailer->Send())
   	{
		$res = 1;
   	} 
   	else {
		$res =0;
   	}
 	return $res;
}//END FUNCTION


function setupSearchData($data)
{
	$posted_data = $data['data'];
	$secondaryData = $data['secondaryData'];
	$secondaryData['searchFieldsLike'] = $data['likeData'];
	if ($secondaryData['custom_field'] AND $secondaryData['custom_value']) {
		$searchFilters['custom_field'] = $secondaryData['custom_field'];
		$searchFilters['custom_value'] = $secondaryData['custom_value'];
	}
	if ($secondaryData['missing']) {
		$searchFilters['missing'] = $secondaryData['missing'];
	}
	if ($posted_data) {
	foreach ($posted_data as $k=>$v) {
		if (is_array($v)) {
			$secondaryData['searchFieldsIN'][$k] = $v;
			unset($posted_data[$k]);
		}		
	}
	}
	
	if ($secondaryData['dateFrom']) {
		$tmp = explode("/",$secondaryData['dateFrom']);
		$secondaryData['searchFieldsGT']['date_added'] = mktime(23,59,0,$tmp[1],$tmp[0],$tmp[2]);
	}
	
	if ($secondaryData['dateTo']) {
		$tmp = explode("/",$secondaryData['dateTo']);
		$secondaryData['searchFieldsLT']['date_added'] = mktime(23,59,0,$tmp[1],$tmp[0],$tmp[2]);
	}
	
	$secondaryData['searchFields'] = $posted_data;
	$secondaryData['debug'] = ($secondaryData['debug']) ? $secondaryData['debug'] : 0;
	$secondaryData['page'] = ($secondaryData['page']) ? $secondaryData['page'] : 1;
	$secondaryData['searchFields'] = $posted_data;
	$secondaryData['filters'] = $searchFilters;
	$secondaryData['debug'] = ($secondaryData['debug']) ? $secondaryData['debug'] : 0;
	$secondaryData['return'] = ($secondaryData['return']) ? $secondaryData['return'] : 'paginated';
	$secondaryData['getTotalAmount'] = ($secondaryData['getTotalAmount']) ? $secondaryData['getTotalAmount'] : 0;
	$secondaryData['getUserDetails'] = ($secondaryData['getUserDetails']) ? $secondaryData['getUserDetails'] : 0;

	return $secondaryData;
}//END FUNCTION

function parseAudienceCSV($targetFile,$settings=0)
{
ini_set('auto_detect_line_endings', true);
$delimiter = ($settings['delimiter']) ? $settings['delimiter'] : ";";
$enclose = ($settings['enclose']) ? $settings['enclose'] : '';
if (($handle = fopen($targetFile, "r")) !== FALSE) {

$row = 0;

    while (($data = fgetcsv($handle, 10*1024, $delimiter,$enclose)) !== FALSE) {
        $num = count($data);
        	$csv[$row] = $data;
        $row++;
    }//END LOOP
   
    fclose($handle);
}//END HANDLE
$headers = $csv[0];
if ($settings['firstRowIsHeader']) {
	 unset($csv[0]);
}

$res['headers'] = $headers;
$res['file'] = $targetFile;
$res['total'] = $row;
if ($settings['lines']) {
	for ($i=0;$settings['lines'] > $i;$i++)
	{
		$res['results'][$i] = $csv[$i];
	}
}
else {
	$res['results'] = $csv;
}

return $res;

}//END FUNCTION

function importAudienceCSV($data,$settings)
{
	global $sql;
/* TO GET UNIQUES
1: GET ALL AUDIENCE FROM DB
2: IF EMAIL AND PHONE EXISTS DROP
3: IF EMAIL OR PHONE EXISTS UPDATE ( array to update )
4: ELSE INSERT
*/
$time_start = microtime(true);

	$sql->db_Select($this->tables['audience']['table'],'id,mobile,email',"uid = ".ID);
	
	if ($sql->db_Rows()) {
		$allAudience = execute_multi($sql);
		foreach ($allAudience as $v)
		{
			if ($v['mobile']) {
				$allMobiles[$v['mobile']] = $v['id'];
			}
			if ($v['email']) {
				$allEmail[$v['email']] = $v['id'];
			}	
		}
	}//END ROWS
	$status = ($settings['status']) ? $settings['status'] : 1;
	$takeAction = ($settings['takeAction']) ? $settings['takeAction'] : 'ignore';
	$takeAction = 'update';
     $fieldsToCheck = array('email','mobile');
	foreach ($settings as $k => $v)
	{
		if (strstr($k,"row-")) {
			list($useless, $column) = split("row-",$k);
			$res[$v] = $column;
			$headers[] = $v;
		}
	}

	//WE NEED A LOOP OF THE DATA TO GET THE GROUPS TO IMPORT, JUST IN CASE WE NEED TO INSERT A LIST AND THE NUMBER OF COLUMNS
	$i=0;
	foreach ($data as $k=>$v)
	{
		if ($i==0) {
			$numCols = count($v);
		}
			$allGroups[] = $v[$res['list']];
			$allGroupsQuery[] = "'".trim($v[$res['list']])."'";
	}
	if ($allGroups) {
		$allGroups = array_unique($allGroups);	//ALL THE LISTS IN THE CSV
		$allGroupsQuery = array_unique($allGroupsQuery);	//ALL THE LISTS IN THE CSV
		$allGroupsQuery = implode(',',$allGroupsQuery);
	}
	
	if ($settings['defaultList']) {//WE HAVE A DEFAULT LIST TO USE
		$lists = $this->audienceLists(ID,$settings['defaultList'],array('return'=>'single'));
	}//END DEFAULT
	elseif ($res['list']) {
		
		$lists = $this->audienceLists(ID,0,array('return'=>'multi','debug'=>0,'searchFieldsIN'=>array('title'=>$allGroupsQuery),'fields'=>'id,title'));
		//SIMPLIFY TO CROSS CHECK
		if ($lists) {
			foreach ($lists as $k=>$v)
			{
				$audienceLists[$v['title']] = $v['id'];
			}		
		}//END EXISTING
		else {//NO EXISTING LISTS, CREATE NEW
			foreach ($allGroups as $k=>$v)
			{
				$sql->db_Insert($this->tables['lists']['table']." (title,uid,date_added,active)","'$v','".ID."','".time()."',1");
				$allGroups[$sql->last_insert_id] = $v;
				$audienceLists[$v] = $sql->last_insert_id;
				unset($allGroups[$k]);
			}
		}//END NEW LISTS

	}//END CSV LISTS

$headers['date_added'] = 'date_added';//ADD THE TIME
$headers['active'] = 'active';//ADD THE TIME
$headers['uid'] = 'uid';//ADD THE TIME
$headers['settings'] = 'settings';//ADD THE MICROTIME
$res['date_added'] = $numCols++;
$res['active'] = $numCols++;
$res['uid'] = $numCols++;
$res['settings'] = $numCols++;
$parsed = array();
$duplicates = array();
if ($settings['splitName'] AND !array_key_exists('surname') ) {
	$headers[] = 'surname';
	$res['surname'] = $numCols++;
}
$i=0;
	foreach ($data as $k=>$v)
	{
		foreach ($res as $key=>$b) {
			$cols[$key] = $v[$b];
		}//END LOOP FIELDS
			if ($settings['splitName'] AND $cols['name']) {
				$tmpName = explode(" ",$cols['name']);
				if ($settings['splitName'] == 'nameSurname')
				{
					$cols['name'] = $tmpName[0];
					$cols['surname'] = $tmpName[1];
				}
				else {
					$cols['name'] = $tmpName[1];
					$cols['surname'] = $tmpName[0];
				}
			}
			$cols['date_added'] = time();
			$cols['settings'] = str_replace(array(' ','.') , array('',''), microtime());
			$cols['active'] = $status;
			$cols['uid'] = ID;
############################################# DUPLICITY CHECKS ############################################################
				########################## CHECK FOR DUPLICATES IN THIS CSV ###############################################
				//CHECK IF BOTH EMAIL AND MOBILE
				//BREAK DOWN THE CHECK PER COLUMN
				if (array_key_exists('mobile',$res) AND $cols['mobile']) {
					if (array_key_exists($cols['mobile'],$parsed['mobiles'])) {
						$duplicates['mobile'][$cols['mobile']][$i] = $cols;
						unset($cols);
					}
					else {
						$parsed['mobiles'][$cols['mobile']]['key'] = $i;
						$parsedData['mobiles'][$cols['mobile']] = $cols;
					}
					
				}//END MOBILES
				if (array_key_exists('email',$res) AND $cols['email'])
				{
					if (array_key_exists($cols['email'],$parsed['email'])) {
						$duplicates['email'][$cols['email']][$i] = $cols;
						unset($cols);
					}
					else {
						$parsed['email'][$cols['email']] = $i;
						$parsedData['email'][$cols['email']] = $cols;
					}
				}//END EITHER				
				######################### END CHECK FOR DUPLICATES IN THIS CSV ############################################
				$uniqueCsvData[$i] = $cols;
			
		unset($cols);
		$i++;
	}//END FORM CSV

$result['uploadedTotal'] = count($lines);
	############## HANDLE DUPLICATES IN CSV ############################
	if ($duplicates) {
		$headersCsv = $headers;
		unset($headersCsv['date_added']);
		unset($headersCsv['active']);
		unset($headersCsv['uid']);
		
		if ($duplicates['mobile']) {
			$fileMobileDupes = ABSPATH."/uploads/duplicates-mobiles-".ID.".csv";
			$duplicatesCSV = array();
			$duplicatesCSV[] = implode($settings['delimiter'],$headersCsv);
	$dupes = 0;
	foreach ($duplicates['mobile'] as $key => $v) {
		unset($parsedData['mobiles'][$key]['date_added']);
		unset($parsedData['mobiles'][$key]['active']);
		unset($parsedData['mobiles'][$key]['uid']);
		$duplicatesCSV[] = implode($settings['delimiter'],$parsedData['mobiles'][$key]);
		foreach ($v as $k=>$b) {
			unset($b['date_added']);
			unset($b['active']);
			unset($b['uid']);
			$duplicatesCSV[] = implode($settings['delimiter'],$b);
			unset($uniqueCsvData[$k]);
			$dupes++;
		}
		unset($uniqueCsvData[$parsed['mobiles'][$key]['key']]);
	}//END LOOP MOBILES

	$f = fopen($fileMobileDupes,'w');
	fwrite($f,implode("\n",$duplicatesCSV));
	fclose($f);
	$result['dupesMobile'] = count($duplicates['mobile'])+$dupes;
	}//END MOBILE
	if ($duplicates['email']) {
		$dupes = 0;
		$fileEmailDupes = ABSPATH."/uploads/duplicates-email-".ID.".csv";
		$duplicatesCSV = array();
		$duplicatesCSV[] = implode($settings['delimiter'],$headersCsv);
		foreach ($duplicates['email'] as $key => $v) {
			unset($parsedData['email'][$key]['date_added']);
			unset($parsedData['email'][$key]['active']);
			unset($parsedData['email'][$key]['uid']);
			$duplicatesCSV[] = implode($settings['delimiter'],$parsedData['email'][$key]);
			foreach ($v as $k=>$b) {
				unset($b['date_added']);
				unset($b['active']);
				unset($b['uid']);
				$duplicatesCSV[] = implode($settings['delimiter'],$b);
				unset($uniqueCsvData[$k]);
				$dupes++;
			}
			unset($uniqueCsvData[$parsed['email'][$key]]);
		}//END LOOP email
		$f = fopen($fileEmailDupes,'w');
		fwrite($f,implode("\n",$duplicatesCSV));
		fclose($f);
		$result['dupesEmail'] = count($duplicates['email'])+$dupes;
	}//END EMAIL
	unset($parsedData);
	unset($duplicatesCSV);
	}//END DUPLICATES
	############## END HANDLE DUPLICATES IN CSV ############################
	$result['duplicates'] = $duplicates;
	#################### NOW GO FOR DB DUPLICATES #########################
	$i=0;
	foreach ($uniqueCsvData as $k=>$cols)
	{
		if (array_key_exists($cols['email'],$allEmail)) {
//			echo "Sdsd";
		}
############################################# DUPLICITY CHECKS ############################################################
			if (array_key_exists($cols['mobile'],$allMobiles) AND array_key_exists($cols['email'],$allEmail)) {//EXISTS IN BOTH
				
				if ($takeAction == 'update') {
					unset($cols['list']);
					$tmp[] = $allMobiles[$cols['mobile']];
					foreach ($cols as $c) {
						$tmp[] = $c;
					}
						$updateList[] = implode(";",$tmp);
					
				}//END UPDATE
			}//END EXISTS IN BOTH
			elseif (array_key_exists($cols['mobile'],$allMobiles) OR array_key_exists($cols['email'],$allEmail))
			{
//				echo $cols['mobile']." - ".$allMobiles[$cols['mobile']]."<br>";
				unset($cols['list']);
				$tmp[] = (array_key_exists($cols['mobile'],$allMobiles)) ? $allMobiles[$cols['mobile']] : $allEmail[$cols['email']];
				foreach ($cols as $c) {
					$tmp[] = $c;
				}
				
						$updateList[] = implode(";",$tmp);
			}//END EXISTS IN ONE - UPDATE
			else {//NEW ENTRY - INSERT
				
				if ($cols['list']) {//CHECK FOR LISTS
					$listMembers[$cols['settings']] = ID.";".$audienceLists[$cols['list']].";";
					$datesAdded[] = $cols['settings'];
				}//END LISTS
				
				unset($cols['list']);
				foreach ($cols as $c) {
					$tmp[] = $c;
				}
				
				$lines[$i] = implode(";",$tmp);
			}//END TO INSERT
			
############################################# END DUPLICITY CHECKS ########################################################
		unset($tmp);	
		unset($cols);
		$i++;
	}//END FORM CSV
	#################### END GO FOR DB DUPLICATES #########################

foreach ($headers as $k=>$v) {
	if ($v == 'list') {
		unset($headers[$k]);
	}
}
	$headersFlat = implode(",",$headers);

	$file = ABSPATH."/uploads/insertCSV".time().".csv";
	$fileListMembers = ABSPATH."/uploads/listMembersCSV".time().".csv";
	$fileUpdate = ABSPATH."/uploads/updateMembersCSV".time().".csv";
	
	$f = fopen($file,"w");
	fwrite($f,implode("\n",$lines));
	fclose($f);
	mysql_query("LOAD DATA INFILE '".str_replace('\\','/',$file)."' INTO TABLE users_audience FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n'
($headersFlat)
;");
	if ($lines AND $datesAdded) {//IF WE HAVE INSERTS
	
	$sql->db_Select($this->tables['audience']['table'],'id,settings',"settings IN (".implode(",",$datesAdded).")");

	if ($sql->db_Rows()) {
		$ids = execute_multi($sql);
		foreach ($ids as $v) {
			if (array_key_exists($v['settings'],$listMembers)) {
				$listMembers[$v['settings']] .= $v['id'];
			}
		}
	}//END ASSIGN IDS

		$fp = fopen($fileListMembers,"w");
		fwrite($fp,implode("\n",$listMembers));
		fclose($fp);
		mysql_query("LOAD DATA INFILE '".str_replace('\\','/',$fileListMembers)."' INTO TABLE users_audience_list_members FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' (uid,listID,audienceid);");

	}//END SETUP LIST MEMBERS
$result['totalUpdated'] = count($updateList);
if ($updateList) {

	$tmpTable = "tmpTable_".time();
//	$tmpTable = "users_audienceTemp";
		mysql_query("CREATE TEMPORARY TABLE `$tmpTable` (
	  `id` int(11) NOT NULL default '0',
	  `uid` int(11) default NULL,
	  `name` varchar(70) default NULL,
	  `surname` varchar(70) default NULL,
	  `phone` varchar(30) default NULL,
	  `mobile` varchar(30) default NULL,
	  `email` varchar(60) default NULL,
	  `date_added` int(11) default NULL,
	  `active` tinyint(1) default NULL,
	  `settings` text,
	  KEY `uid` (`uid`),
	  KEY `mobile` (`mobile`),
	  FULLTEXT KEY `email` (`email`)
	) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
	
	$f = fopen($fileUpdate,"w");
	fwrite($f,implode("\n",$updateList));
	fclose($f);
	$headers = array_merge(array('id'=>'id'),$headers);
	$headersFlat = implode(",",$headers);
	mysql_query("LOAD DATA INFILE '".str_replace('\\','/',$fileUpdate)."' INTO TABLE $tmpTable FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n'
($headersFlat)
;");
	

foreach ($headers as $k=>$v)
{
	$updateQ[$k] = "a.$v = b.$v";
}
unset($updateQ['id']);
unset($updateQ['date_added']);
unset($updateQ['uid']);
$updateQ = implode(",",$updateQ);
	mysql_query("UPDATE users_audience a
    JOIN $tmpTable b USING (id)
SET $updateQ ;");
}
	mysql_query("DROP TEMPORARY TABLE $tmpTable");
	$settings['debug'] = 0;
	if ($settings['debug']) {
	echo "LOAD DATA INFILE '".str_replace('\\','/',$file)."' INTO TABLE users_audience FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n'
($headers)
;";
	
	echo mysql_error();
	}//END DEBUG

$time_end = microtime(true);
$time = $time_end - $time_start;
//unlink($file);
//unlink($fileListMembers);
//unlink($fileUpdate);
$result['total'] = count($data);
$result['totalAdded'] = count($lines);
$result['memoryUsed'] = convert_filesize(memory_get_peak_usage());
$result['titmeTaken'] = $time;
$result['dupesMobileFile'] = str_replace(ABSPATH,URL,$fileMobileDupes);
$result['dupesEmailFile'] = str_replace(ABSPATH,URL,$fileEmailDupes);
return $result;
}//END FUNCTION

function audienceLists($uid,$id=0,$settings=0)
{
	global $sql;
	
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
	$way = ($settings['way']) ? "  ".$settings['way'] : "";
	$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
	$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
	
	
	$q[] = "uid = $uid";
	if ($id) {
		$q[] = "id = $id";
	}
	if (is_array($settings['searchFields'])) {
		foreach ($settings['searchFields'] as $k => $v) {
			$q[] = "$k = '$v'";
		}
	}//END SETUP SEARCH FIELDS
    if (is_array($settings['searchFieldsLike'])) {
    	foreach ($settings['searchFieldsLike'] as $k => $v) {
   			$q[] = "$k LIKE '%$v%'";
    	}
    }//END SETUP SEARCH FIELDS WITH LIKE
	if (is_array($settings['searchFieldsIN'])) {
		foreach ($settings['searchFieldsIN'] as $k => $v) {
			$q[] = "$k  IN ($v)";
		}
	}//END SETUP SEARCH FIELDS WITH IN
	
	if ($q) {
		$query = implode(" AND ",$q);
	}
	if (!$query) 
	{
		$query_mode='no_where';
	}
	else {
		$query_mode = 'default';
	}
	
	$sql->db_Select($this->tables['lists']['table'],$fields,$query,$query_mode);
	if ($settings['debug']) {
		echo "SELECT $fields FROM ".$this->tables['lists']['table']." WHERE $query<br>";
	}
	if ($sql->db_Rows()) {
		if ($settings['return'] == 'multi') {
			$res = execute_multi($sql);
		}//END MULTI
		elseif ($settings['return'] == 'paginated')
		{
			
		}//END PAGINATION
		elseif ($settings['return'] == 'single')
		{
			$res = execute_single($sql);
		}//END SINGLE
		
	return $res;	
	}//END ROWS
	
}//END FUNCTION

function audience($uid,$id=0,$settings)
{	
	global $sql;
	
	$fields = ($settings['fields']) ? $settings['fields'] : $this->tables['audience']['table'].".*";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
	$way = ($settings['way']) ? "  ".$settings['way'] : "";
	$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
	$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
	$groupBy = ($settings['groupBy']) ? " GROUP BY ".$settings['groupBy'] : "";
	$q[] = $this->tables['audience']['table'].".uid = $uid";
	if ($id) {
		$q[] = "id = $id";
	}
	if (is_array($settings['searchFields'])) {
		foreach ($settings['searchFields'] as $k => $v) {
			if ($k == 'listid') {
				$tables['lists'] = " INNER JOIN ".$this->tables['memberLists']['table']." ON (".$this->tables['memberLists']['table'].".audienceid=".$this->tables['audience']['table'].".id)";
				$q[] = $this->tables['audience']['table'].".".$k." = '$v'";
			}
			else {
				$q[] = "$k = '$v'";
			}
		}
	}//END SETUP SEARCH FIELDS
    if (is_array($settings['searchFieldsLike'])) {
    	foreach ($settings['searchFieldsLike'] as $k => $v) {
   			$q[] = "$k LIKE '%$v%'";
    	}
    }//END SETUP SEARCH FIELDS WITH LIKE
	if (is_array($settings['searchFieldsIN'])) {
		foreach ($settings['searchFieldsIN'] as $k => $v) {
			if ($k == 'listid') {
				$tables['lists'] = " INNER JOIN ".$this->tables['memberLists']['table']." ON (".$this->tables['memberLists']['table'].".audienceid=".$this->tables['audience']['table'].".id)";
				$q[] = $this->tables['memberLists']['table'].".".$k." IN (".implode(",",$v).")";
			}
			else {
				$q[] = "$k  IN ($v)";
			}
			
		}
	}//END SETUP SEARCH FIELDS WITH IN

     if (is_array($settings['searchFieldsGT'])) {
    	foreach ($settings['searchFieldsGT'] as $k => $v) {
   			$q[] = $this->tables['audience']['table'].".$k > $v";
    	}
    }//END SETUP SEARCH FIELDS WITH NOT LIKE 	

     if (is_array($settings['searchFieldsLT'])) {
    	foreach ($settings['searchFieldsLT'] as $k => $v) {
   			$q[] = $this->tables['audience']['table'].".$k < $v";
    	}
    }//END SETUP SEARCH FIELDS WITH NOT LIKE 	

      if (is_array($settings['searchFieldBETWEEN'])) {
    	foreach ($settings['searchFieldBETWEEN'] as $k => $v) {
   			$q[] = "(".$this->tables['audience']['table'].".$k BETWEEN $v)";
    	}
    }//END SETUP SEARCH FIELDS WITH NOT LIKE 	
	
	$tables = (is_array($tables)) ? $this->tables['audience']['table']." ".implode(" ",$tables) : $this->tables['audience']['table'];
	
	if ($q) {
		$query = implode(" AND ",$q);
	}
	if (!$query) 
	{
		$query_mode='no_where';
	}
	else {
		$query_mode = 'default';
	}
	

	
		if ($settings['return'] == 'multi') {
			$sql->db_Select($tables,$fields,"$query $groupBy $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT $fields FROM ".$tables." WHERE $query $orderby $way $groupBy $limit<br>";
			}
			$res = execute_multi($sql);
		}//END MULTI
		elseif ($settings['return'] == 'paginated')
		{
			$sql->db_Select($tables,$fields,"$query $groupBy $orderby $way",$query_mode);
			$total = $sql->db_Rows();
			$current_page = ($settings['page']) ? $settings['page'] : 1;
			$results_per_page =  $settings['results_per_page'];
			if (isset($settings['start'])) 
			{
				$start = $settings['start'];
			}
			else {
				$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
			}
			$limit = "LIMIT $start,".$results_per_page;
			$sql->db_Select($tables,"$fields","$query $groupBy $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "<br>SELECT $fields FROM ".$tables." WHERE $query $orderby $way $limit";
			}
			$res = execute_multi($sql,1);
			paginate_results($current_page,$results_per_page,$total);
	
		}//END PAGINATED
		elseif ($settings['return'] == 'single')
		{
			$sql->db_Select($tables,$fields,"$query $orderby $way $limit",$query_mode);
				
			$res = execute_single($sql);
		}//END SINGLE
		elseif ($settings['return'] == 'count') // RETURN COUNT RESULTS
		{
			$sql->db_Select($tables,"count(".$this->tables['audience']['table'].".id) as total","$query  $orderby $way",$query_mode);
			if ($settings['debug']) {
				echo "SELECT count(".$this->tables['audience']['table'].".id) as total FROM $tables WHERE $query  $orderby $way<br>";
			}
			$res = execute_single($sql);
			if ($settings['returnLists']) {
				$res['total'] = $res['total'];
				$sql->db_Select($this->tables['audience']['table']." INNER JOIN ".$this->tables['memberLists']['table']." ON (".$this->tables['audience']['table'].".id=".$this->tables['memberLists']['table'].".audienceid)"
				,$this->tables['audience']['table'].".id","$query",$query_mode);

				if ($sql->db_Rows()) {
					$tmp = execute_multi($sql);
					foreach ($tmp as $v) {
						$ids[] = $v['id'];
					}
					
					$sql->db_Select($this->tables['audience']['table']." INNER JOIN ".$this->tables['memberLists']['table']." ON (".$this->tables['audience']['table'].".id=".$this->tables['memberLists']['table'].".audienceid)",'listid,count(audienceid) as counter',
					"audienceid IN (".implode(",",$ids).")  GROUP BY listid");

					if ($sql->db_Rows()) {
						$tmp = execute_multi($sql);
						foreach ($tmp as $v) {
						$res['lists'][$v['listid']] = $v['counter'];
					}					
				}
				}//END IDS
				return $res;
			}
			else {
				return $res['total'];
			}
			
		}
		if ($sql->db_Rows()) {
			
		
		if ($settings['detailed']) {
			if ($settings['return'] == 'single') {
				$res['settings'] = json_decode($res['settings'],true);
				//HOOKS GOES HERE
			}
			else {
				foreach ($res as $k=>$v)
				{
					if ($settings['lists']) {
						$res[$k]['lists'] = $this->memberLists($v['id'],$settings['listSettings']);
					}
					$res[$k]['settings'] = json_decode($v['settings'],true);
					//HOOKS GOES HERE
				}
			}
		}//END USER DETAILS
		}//END ROWS
	return $res;	
	
}//END FUNCTION

function memberLists($uid,$settings)
{
	global $sql;
	
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
	$way = ($settings['way']) ? "  ".$settings['way'] : "";
	$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
	$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
	if ($settings['groupBy']) {
		$groupBy = " GROUP BY ".$settings['groupBy'];
	}
	
	$q[] = (is_array($uid)) ? "audienceid IN (".implode(",",$uid).")" : "audienceid = $uid";

	if (is_array($settings['searchFields'])) {
		foreach ($settings['searchFields'] as $k => $v) {
			$q[] = "$k = '$v'";
		}
	}//END SETUP SEARCH FIELDS
    if (is_array($settings['searchFieldsLike'])) {
    	foreach ($settings['searchFieldsLike'] as $k => $v) {
   			$q[] = "$k LIKE '%$v%'";
    	}
    }//END SETUP SEARCH FIELDS WITH LIKE
	if (is_array($settings['searchFieldsIN'])) {
		foreach ($settings['searchFieldsIN'] as $k => $v) {
			$q[] = "$k  IN ($v)";
		}
	}//END SETUP SEARCH FIELDS WITH IN
	
	if ($q) {
		$query = implode(" AND ",$q);
	}
	if (!$query) 
	{
		$query_mode='no_where';
	}
	else {
		$query_mode = 'default';
	}
	$sql->db_Select($this->tables['memberLists']['table'],$fields,"$query $groupBy",$query_mode);	
//	echo "<br>SELECT $fields FROM ".$this->tables['memberLists']['table']." WHERE $query $groupBy";
	if ($sql->db_Rows()) {
		if ($settings['return'] == 'multi') {
			$res = execute_multi($sql);
		}//END MULTI
		if ($settings['listDetails']) {
			foreach ($res as $k=>$v) {
				$res[$k]  = $this->audienceLists(ID,$v['listid'],array('return'=>'single'));
			}
		}
		return $res;
	}//END ROWS
}//END FUNCTION

function audienceSaveList($data)
{
	global $sql;
	$p = new Parse();
	$data['title'] = $p->toDB(trim($data['title']));
	$sql->db_Insert($this->tables['lists']['table']." (uid,title,date_added,active)",ID.",'".$data['title']."',".time().",1");
	return array('title'=>$data['title'],'id'=>$sql->last_insert_id);
	
}//END FUNCTION

function getUserID($settings)
{
    global $sql;
    $args = "uname='".$settings['uname']."'";
    $sql->db_Select($this->tables['users']['table'],"id",$args);
    $result = execute_single($sql); 
    return $result['id']; 
}//END FUNCTION

function compareUsers($orig,$dest,$settings=0)
{
	//START WITH USER 1
	foreach ($orig['efields'] as $v) {
		$efields1[$v['fieldid']] = $v;
	}//END ORIG
	//USER 2
	foreach ($dest['efields'] as $v) {
		$efields2[$v['fieldid']] = $v;
	}//END ORIG
	foreach ($efields1 as $k=>$v) {
		if ($v['value'] == $efields2[$k]['value']) {
			$matches['fields'][$k] = $v;
		}//END MATCH		
	}
	$matches['percent'] = (count($matches['fields'])/count($efields1))*100;
	print_r($matches);
	
}//END FUNCTION

function getUserByID($id)
{
	global $sql;
	
	$args = (is_numeric($id) ? "id= ".$id : "uname='".$id."'");
	$sql->db_Select($this->tables['users']['table'],"id",$args);
	return execute_single($sql);
}//END FUNCTION

}//END CLASS
?>