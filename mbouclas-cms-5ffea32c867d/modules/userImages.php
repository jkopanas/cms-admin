<?php

class userImages{
    
    var $tables;
    
    /*******************************IMAGES********************************/
    
    function userImages()
    {
        $this->tables['users'] = array('table'=>'users','key'=>'id');
        $this->tables['images'] = array('table'=>'users_images','key'=>'id');
        $this->tables['image_types'] = array('table'=>'image_types','key'=>'id');
        $this->tables['image_additional'] = array('table'=>'users_images_aditional');
		$this->tables['albums'] = array('table'=>'users_albums','key'=>'id');
    }
    
    function newImage($data,$settings=0)
    {
        global $sql;
        
        $input['original_file'] = $data['original_file'];
        $input['albumid'] = $data['albumid'];
        $input['title'] = $data['title'];
        $input['settings'] = json_encode($data['settings']);
        $input['available'] = $data['available'];
        $input['orderby'] = $data['orderby'];
        $input['type'] = $data['type'];
        $input['uid'] = $data['uid'];
        
        
        
        foreach($input as $k=>$v)
        {
            $keys[] = $k;
			$values[] = "'$v'";
        }
        
        $sql->db_Insert($this->tables['images']['table']." (".implode(",",$keys).")",implode(",",$values));
        if($settings['debug'])
        {
            echo "INSERT INTO ".$this->tables['images']['table']." (".implode(",",$keys).")"." VALUES (".implode(",",$values).")";
        }
        
        $input['imageid']=$sql->last_insert_id;
        
        $this->generateImages($input);
        
        return $input['imageid'];
    }
    
    function updateImage($settings)
    {
        global $sql;
        
        if(!$settings['available'])
        $settings['available']=0;
        
        foreach($settings as $k=>$v)
        {
            if($k=='settings')
            $v=json_encode($v);
            $input[]= $k."='$v'";
        }
        
        
        
        /*
        $input['original_file'] = $data['original_file'];
        $input['albumid'] = $data['albumid'];
        $input['title'] = $data['title'];
        $input['settings'] = json_encode($data['settings']);
        $input['available'] = $data['available'];
        $input['orderby'] = $data['orderby'];
        $input['type'] = $data['type'];
        $input['uid'] = $data['uid'];
        */
        
        
        $sql->db_Update($this->tables['images']['table'],implode(",",$input)." WHERE id='".$settings['id']."'");
        
        //echo implode(",",$input)." WHERE id='".$settings['id']."'";
    }
  
    function deleteImage($settings)
    {
        global $sql;
        
               
        if($settings['albumid']){
            $args[]="albumid='".$settings['albumid']."'";
            $add_args[]="albumid='".$settings['albumid']."'";
        }
        
        if($settings['id']){
            $args[]="id='".$settings['id']."'";
            $add_args[]="imageid='".$settings['id']."'";
        }
        
        $mode="default";
        
        if(count($args)>1)
            $args = implode(" AND ",$args);
        elseif(count($args)==1)
            $args = $args[0];
        else
            exit();
            
        if(count($add_args)>1)
            $add_args = implode(" AND ",$add_args);
        elseif(count($add_args)==1)
            $add_args = $add_args[0];
        else
            exit();
        
        
        $sql->db_Select($this->tables['image_additional']['table'],"image_path as path",$add_args);
        //echo 'SELECT image_path FROM '.$this->tables['image_additional']['table']. ' WHERE '.$add_args;
        $add_data = execute_multi($sql);
        
        if($sql->db_Rows()>0)
        foreach ($add_data as $item)
        {
            
            $file = $item['path'];
            unlink($file);            
        }
        
        $sql->db_Delete($this->tables['image_additional']['table'],$add_args);
        
        $sql->db_Select($this->tables['images']['table'],"original_file as file",$args);
        $data = execute_multi($sql);
        
        if($sql->db_Rows()>0)
        foreach ($data as $item)
        {
            $path = $_SERVER['DOCUMENT_ROOT']."/media_files/users/".ID."/images/";
            $original = $path."original/";
            unlink($original.$item['file']);         
        }
        
        $sql->db_Delete($this->tables['images']['table'],$args);
    }
    
    function searchImages($settings)
    {
        global $sql;
        
        if(!$settings['fields']){
            $settings['fields']="*";
        }
        
        if($settings['albumid']){
            $args[] = "albumid='".$settings['albumid']."'";
        }
        
        if($settings['id']){
            $args[] = "id='".$settings['id']."'";
        }
        
        if($settings['uid']){
            $args[] = "uid='".$settings['uid']."'";
        }
        
        if($settings['orderby']){
            $orderby=" ORDER BY ".$settings['orderby'];
        }
        
        $mode="default";
        
        if(count($args)>1)
            $args = implode(" AND ",$args);
        elseif(count($args)==1)
            $args = $args[0];
        else
            $mode="no_where";
        
        $sql->db_Select($this->tables['images']['table'],$settings['fields'],$args,$mode);
        
        
        if($settings['debug']){
            echo 'SELECT '.$settings['fields'].' FROM '.$this->tables['images']['table'].' WHERE '.$args;
        }
        
        if($settings['single'])
        {
            $result = execute_single($sql);
            
            $sql->db_Select($this->tables['image_additional'],'image_path,image_type',"imageid='".$result['id']."'");
            $data=execute_multi($sql);
            foreach($data as $item)
            {
                $result['images'][$item['image_type']]=$item['image_path'];
            }
        }
        else
        {
            $result = execute_multi($sql);
            
            //print_r($result);
            foreach($result as $k=>$v)
            {
                $sql->db_Select($this->tables['image_additional']['table'],'image_url,image_type',"imageid='".$result[$k]['id']."'");
                //echo 'SELECT image_path,image_type FROM '.$this->tables['image_additional']['table'].' WHERE '."imageid='".$result[$k]['id']."'";
                $data=execute_multi($sql);
                
                //print_r($data);
                foreach($data as $item)
                {
                    $result[$k]['images'][$item['image_type']]=$item['image_url'];
                }
                
            }
        }
        
        //print_r($result);
        return $result;
    }
    
    function generateImages($settings)
    {
        global $sql;
        $sql->db_Select($this->tables['image_types']['table'],"*","module_id='13'");
        $types = execute_multi($sql);
        
        $path = $_SERVER['DOCUMENT_ROOT']."/media_files/users/".$settings['uid']."/images/";
        
        /**        
                
        $input['original_file'] = $data['original_file'];
        $input['albumid'] = $data['albumid'];
        $input['title'] = $data['title'];
        $input['settings'] = json_encode($data['settings']);
        $input['available'] = $data['available'];
        $input['orderby'] = $data['orderby'];
        $input['type'] = $data['type'];
        $input['uid'] = $data['uid'];
        
        
        $myFile = $_SERVER['DOCUMENT_ROOT']."/material/log.txt";
        $fh = fopen($myFile, 'a') or die("can't open file");
        
        fwrite($fh,$targetFile,true);
        $stringData = print_r($settings,true);
        fwrite($fh, $stringData);
        $stringData = print_r($types,true);
        fwrite($fh, $stringData);

        fclose($fh);
        */
        
        
        if($types)
        foreach($types as $type)
        {
            $source =$path."original/".$settings['original_file'];
            $destination_folder = $path.$type['dir']."/";//.$type['prefix'].$settings['original_file'];
            
            if (!is_dir($destination_folder)) {
                mkdir($destination_folder,0755,true);
            }//END IF
            
            $destination = $destination_folder.$type['prefix'].$settings['original_file'];
            
            $width = $type['width'];
            $height = $type['height'];
            $quality = $type['quality'];
            
            $this->resize($source,$destination,$width,$height,$quality);
            
            $settings['image_path']=$destination;
            $settings['image']=$type['prefix'].$settings['original_file'];
            $settings['width']=$width;
            $settings['height']=$height;
            $settings['quality']=$quality;
            $settings['image_type']=$type['title'];
            
            $this->addAdditionalImage($settings);
        }
    }
    
    function  addAdditionalImage($settings)
    {
        global $sql;
        
        $input['imageid']=$settings['imageid'];
        $input['albumid']=$settings['albumid'];
        $input['image']=$settings['image'];
        $input['image_path']=$settings['image_path'];
        $input['image_url']=str_replace($_SERVER['DOCUMENT_ROOT'],"",$settings['image_path']);
        $input['date_added']=time();
        $input['image_X']=$settings['width'];
        $input['image_y']=$settings['height'];
        $input['image_type']=$settings['image_type'];
        
        foreach($input as $k=>$v)
        {
            $keys[] = $k;
			$values[] = "'$v'";
        }
        
        $sql->db_Insert($this->tables['image_additional']['table']." (".implode(",",$keys).")",implode(",",$values));
        
        if($settings['debug'])
        {
            $debug = "INSERT INTO ".$this->tables['image_additional']['table']." (".implode(",",$keys).")"." VALUES (".implode(",",$values).")";
        
        
            $myFile = $_SERVER['DOCUMENT_ROOT']."/material/log.txt";
            $fh = fopen($myFile, 'a') or die("can't open file");
            
            fwrite($fh,"/******************START FOR ENTRY***********************/");
            fwrite($fh,$debug); 
            $stringData = print_r($settings,true);
            fwrite($fh, $stringData);
            $stringData = print_r($input,true);
            fwrite($fh, $stringData);
            fwrite($fh,"/******************END FOR ENTRY***********************/");
            fclose($fh);
        }
     
        
        
           
    }
    
    /*******************************ALBUMS********************************/
    
    function newAlbum($data,$settings=0)
    {
        global $sql;
        
        $input['title'] = $data['title'];
        $input['description'] = $data['description'];
        $input['date_added'] = time();
        $input['image'] = $data['image'];
        $input['blog_entry'] = $data['blog_entry'];
        $input['uid'] = ID;
        
        foreach($input as $k=>$v)
        {
            $keys[] = $k;
			$values[] = "'$v'";
        }
        
        $sql->db_Insert($this->tables['albums']['table']." (".implode(",",$keys).")",implode(",",$values));
        
        return $sql->last_insert_id;
    }
    
    function updateAlbum($settings)
    {
        global $sql;
        
        $input['title'] = $settings['title'];
        $input['description'] = $settings['description'];
        $input['image'] = $settings['image'];
        $input['blog_entry'] = $settings['blog_entry'];
        
        foreach($input as $k=>$v)
        {
             $input[$k] = $k."='$v'";
        }
        
        if($settings['id'])
        $sql->db_Update($this->tables['albums']['table'],implode(",",$input)." WHERE id='".$settings['id']."'");
    }
  
    function deleteAlbum($id)
    {
        global $sql;
        
        
        
        
        $sql->db_Delete($this->tables['albums']['table'],"id='".$id."'");
        $this->deleteImage(array('albumid'=>$id));
    }
    
    function searchAlbums($settings)
    {
        global $sql;
        
        if($settings['uid'])
        $args[]="uid='".$settings['uid']."'";
        
        if($settings['id'])
        $args[]="id='".$settings['id']."'";
        
        $mode="default";
        
        if(count($args)>1)
            $args = implode(" AND ",$args);
        elseif(count($args)==1)
            $args = $args[0];
        else
            $mode="no_where";
        
        
        $sql->db_Select($this->tables['albums']['table'],'*',$args,$mode);
        if($settings['debug']){
            echo "SELECT * FROM ".$this->tables['albums']['table']." WHERE ".$args;
        }
        
        if($settings['single'])
        {
            $albums = execute_single($sql);
            if($settings['fullImages'])
            {
                $albums["images"]=$this->searchImages(array("albumid"=>$albums['id'])); 
            }     
        }   
        else
        {
            $albums = execute_multi($sql);
            
            if($settings['fullImages'])
            {
                foreach($albums as $k=>$album)
                {
                    $albums[$k]["images"]=$this->searchImages(array("albumid"=>$album['id']));
                }   
            }     
        }
       
        
        if($settings['count'])
        {
            foreach($albums as $k=>$v)
            {
                $albums[$k]['images']=$this->getImagesCount($v['id']);
            }
        }
        return $albums;
    }
    
    function getImagesCount($albumid)
    {
        global $sql;
        $sql->db_Select($this->tables['images']['table'],"count(id) as images","albumid='".$albumid."' ORDER BY albumid");
               
        $data = execute_single($sql);
        
        return $data['images'];
    }
    
    function resize($source_image, $destination, $tn_w, $tn_h, $quality = 100)
    {
        $info = getimagesize($source_image);
        $imgtype = image_type_to_mime_type($info[2]);
    
        #assuming the mime type is correct
        switch ($imgtype) {
            case 'image/jpeg':
                $source = imagecreatefromjpeg($source_image);
                break;
            case 'image/gif':
                $source = imagecreatefromgif($source_image);
                break;
            case 'image/png':
                $source = imagecreatefrompng($source_image);
                break;
            default:
                die('Invalid image type.');
        }
    
        #Figure out the dimensions of the image and the dimensions of the desired thumbnail
        $src_w = imagesx($source);
        $src_h = imagesy($source);
    
    
        #Do some math to figure out which way we'll need to crop the image
        #to get it proportional to the new size, then crop or adjust as needed
    
        $x_ratio = $tn_w / $src_w;
        $y_ratio = $tn_h / $src_h;
    
        
        
        if (($src_w <= $tn_w) && ($src_h <= $tn_h)) {
            $new_w = $src_w;
            $new_h = $src_h;
        } elseif (($x_ratio * $src_h) < $tn_h) {
            $new_h = ceil($x_ratio * $src_h);
            $new_w = $tn_w;
        } else {
            $new_w = ceil($y_ratio * $src_w);
            $new_h = $tn_h;
        }
    
        $newpic = imagecreatetruecolor(round($new_w), round($new_h));
        imagecopyresampled($newpic, $source, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
        $final = imagecreatetruecolor($tn_w, $tn_h);
        $backgroundColor = imagecolorallocate($final, 255, 255, 255);
        imagefill($final, 0, 0, $backgroundColor);
        //imagecopyresampled($final, $newpic, 0, 0, ($x_mid - ($tn_w / 2)), ($y_mid - ($tn_h / 2)), $tn_w, $tn_h, $tn_w, $tn_h);
        imagecopy($final, $newpic, (($tn_w - $new_w)/ 2), (($tn_h - $new_h) / 2), 0, 0, $new_w, $new_h);
    
        if (imagejpeg($final, $destination, $quality)) {
            return true;
        }
        return false;
    }
}

?>