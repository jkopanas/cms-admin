<?php
$default_user_prefs = array("show_history_box" => 1, "show_recipe_box" => 1,"show_searches_box" => 1,"num_recipes" => 5,"num_history"=>5);
function get_user_image($uid,$id)
{
	$sql = new db();
	$sql->db_Select("user_images","*","uid = $uid AND id = $id");
	return execute_single($sql,1);
}

function get_user_albums($uid,$id="no",$mode="album",$orderby="date_added",$way="desc",$limit="no")
{
	$sql = new db();
	$sql2 = new db();
	$p = new convert();
		if ($mode == "album") 
		{
	################################## IF SINGLE ALBUM REQUESTED ################################
	if ($id !="no") 
	{
		$sql->db_Select("user_albums","*","uid = $uid AND id = $id ORDER BY date_added DESC");
		
		if ($sql->db_Rows() > 0) {
		$res = execute_single($sql,1);
		$res['date_added'] = $p->convert_date($res['date_added'],"short");
		return $res;
		}

	}
	else 
	{

	############################ FIRST GET THE AVAILABLE YEARS IN THE ALBUMS #####################
		$sql->db_Select("user_albums","YEAR(FROM_UNIXTIME(date_added)) as y","uid = $uid GROUP BY y ORDER BY y DESC");
		$r = execute_multi($sql,0);
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
	############################ THEN GET THE ALBUMS PER YEAR #####################################		
			$sql2->db_Select("user_albums","*,YEAR(FROM_UNIXTIME(date_added)) as y","uid = $uid HAVING y = '".$r[$i]['y']."' ORDER BY y DESC");
			$res[$i]['year'] = $r[$i]['y'];
			$res[$i]['details'] = execute_multi($sql2,1);
		}
		return $res;
		}//END OF ALBUM MODE
	}//END OF MULTIPLE RESULTS	
			elseif ($mode=="year")
		{
			$limit = ($limit == "no") ? "" : " LIMIT $limit";
			$sql2->db_Select("user_albums","*,YEAR(FROM_UNIXTIME(date_added)) as y","uid = $uid HAVING y = '".$id."' ORDER BY y DESC $limit");
			$res = execute_multi($sql2,1);
				for ($i=0;$sql2->db_Rows() > $i;$i++)
				{
					$sql->db_Select("user_images","thumb","uid = $uid AND albumid = ".$res[$i]['id']." ORDER BY RAND() LIMIT 1");
					$a = execute_single($sql,0);
					$res[$i]['thumb'] = $a['thumb'];
					$res[$i]['date_added'] = $p->convert_date($res[$i]['date_added'],"short");
				}
			return $res;
		}//END OF YEAR MODE
}

function get_user_album_photos($uid,$albumid,$limit=0,$public=0)
{
	global $sql,$page;

$public_q = ($public == 0) ? "" : " AND public = 1";
	$p = new convert();
	$sql->db_Select("user_images","*","uid = $uid AND albumid = $albumid $public_q ORDER BY date_added DESC");
	if ($limit == 0) {
		$res = execute_multi($sql,1);
	}
	else {
		
		$res = execute_paginated($sql,1,$limit);
	}
	

if ($sql->db_Rows() > 0) {

		for ($i=0;count($res) > $i;$i++)
	{
		$res[$i]['date_added'] = $p->convert_date($res[$i]['date_added'],"short");
	}
}


	return $res;
}

function latest_user_images($uid,$number,$lang,$mode) 
{ 
	$sql = new db();
	$g = new convert();

	if ($mode == "images") 
	{
		$sql->db_Select("user_images","*","uid = $uid ORDER BY date_added DESC LIMIT ".$number);
		$res = execute_multi($sql,1);
		for ($i=0;$sql->db_Rows() > $i;$i++)
		{
			$res[$i]['date_added'] = $g->convert_date($res[$i]['date_added'],"short");
		}
		return $res;
	}

}//END OF FUNCTION latest_products


function get_user_details($id,$settings=0)
{
	global $sql;
	$sql->db_Select("users","*","id = $id");
	if ($sql->db_Rows() > 0) 
	{
		$user = execute_single($sql,1);
		if ($settings['subscriptions']) 
		{
			//get subscribtions
			$user['subscriptions'] = get_user_subscriptions($id);	
		}
		if ($settings['user_content']) //Get the content pages
		{
			if ($settings['user_count']) //Only a count
			{
			
			}
			else //get the actual items
			{
				
			}
		}		
		if ($settings['user_products']) 
		{
			if ($settings['user_count']) //Only a count
			{
			
			}
			else //get the actual items
			{
				
			}
		}
		if ($settings['user_images']) 
		{
			if ($settings['user_images_count']) //Only a count
			{
			
			}
			else //get the actual items
			{
				
			}
		}
	}
	return $user;
}

function get_users_info($settings=0)
{
	global $sql;

	//Single user
	if ($settings['id']) 
	{
		return get_user_details($settings['id'],$settings);
	}
	elseif ($settings['get_by_uname'])
	{
		$sql->db_Select("users",'id',"uname = '".$settings['uname']."' $admin_sql");
		if ($sql->db_Rows()) 
		{
			$user = execute_single($sql);
			if ($settings['get_data']) 
			{
				get_user_details($user['id']);
				return $user;
			}
			else {
				return true;
			}
			
		}
		else {
			return false;
		}
	}
	elseif ($settings['get_by_email'])
	{
		$sql->db_Select("users",'id',"email = '".$settings['email']."' $admin_sql");
		if ($sql->db_Rows()) 
		{
			$user = execute_single($sql);
			if ($settings['get_data']) 
			{
				get_user_details($user['id']);
				return $user;
			}
			else {
				return true;
			}
			
		}
		else {
			return false;
		}
	}
	else //Multiple users
	{
		$filters = $settings['filters'];
		$sort_direction = ($filters['sort_direction'])? $filters['sort_direction'] :'DESC';
		$sort_field = ($filters['sort_field']) ? $filters['sort_field'] : 'id';
		###################### FIRST BUILD THE QUERY #################
		if (isset($filters['admin']))
		{
			$q_ar[] = "admin = ".$filters['admin'];
		}
		if ($filters['user_class']) 
		{
				$q_ar[] = "user_class = '".$filters['user_class']."'";

		}
		if (isset($filters['missing'])) 
		{
			$q_ar[] = $filters['missing']." IS NULL ";
		}
		if (isset($filters['custom_field'])) 
		{
			$q_ar[] = $filters['custom_field']." LIKE '%".$filters['custom_value']."%'";
		}
		if ($filters['user_class_deny']) 
		{
			$tmp = explode(",",$filters['user_class_deny']);
			echo "sdsd";
			if (count($tmp) == 1) 
			{
				$q_ar[] = "user_class != '".$filters['user_class_deny']."'";
			}
			else 
			{
				foreach ($tmp as $v)
				{
					$q_ar[] = "user_class != ".$v."";	
				}
			}	
		}
		if (isset($filters['user_login']))
		{
			$q_ar[] = "user_login = '".$filters['user_login']."'";
		}
		if ($filters['user_location']) 
		{
			$q_ar[] = "user_location = '".$filters['user_location']."'";
		}
		if (count($q_ar) > 1) 
		{
			$q = implode(" AND ",$q_ar);
		}
		else { $q = $q_ar[0]; }
		$q .= " ORDER BY $sort_field $sort_direction ";

		###################### BUILD THE FIELDS LIST #################
		if ($settings['quick_get']) //Very few stuff for quick mass display
		{
			$fields = 'id,uname,user_name,email,user_join,admin,user_class,user_login,user_lastvisit';	
		}
		if ($settings['full_get']) //Get all fields
		{
			$fields = '*';
		}
		if ($settings['custom_get']) //Custom fields set
		{
			$fields = $settings['custom_get'];
		}
		################# EXECUTE ###########################

			if ($settings['return'] == 'multi') //Return all results, NO PAGINATION
			{
				if ($settings['limit']) 
				{
					$q .= "LIMIT ".$settings['limit'];
				}
				$sql->db_Select("users",$fields,$q);
				if ($sql->db_Rows()) 
				{
					return execute_multi($sql);
				}
			}//END ALL RESULTS
			elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
			{
				if (count($q_ar) == 0) 
				{
					$query_mode='no_where';
				}
				else {
					$query_mode = 'default';
				}
				$sql->db_Select("users",'id',$q,$query_mode);//GET THE TOTAL COUNT
					if ($settings['debug']) 
					{
						echo "<br>SELECT $fields FROM users WHERE $q<br>";
					}
				if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
				{
					$total = $sql->db_Rows();
					$current_page = ($settings['page']) ? $settings['page'] : 1;
					
					$results_per_page =  $settings['limit'];
					if (isset($settings['start'])) 
					{
						$start = $settings['start'];
					}
					else {
						$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
					}
					$q .= "LIMIT $start,".$results_per_page;
					$sql->db_Select("users",$fields,$q,$query_mode);
					$search_results = execute_multi($sql,1);
					paginate_results($current_page,$results_per_page,$total);
					
					return $search_results;
					
				}//END FOUND RESULTS


			}//END PAGINATION

		
	}//END MULTIPLE USERS
}//END FUNCTION

/*
* ADD NEW USER
*/
function users_add_new($data,$lang,$settings=0)
{
	global $sql,$smarty;
	$t = new textparse();
	$required_fields = $settings['error_list'];
	 foreach ($data as $key => $value) 
	 {   
		 $data[$key] = $t->formtpa($value);
	 }//END OF FOREACH
	 for ($i=0;count($data) > $i;$i++)
	 {
	 	for ($j=0;count($required_fields) > $j;$j++)
	 	{
	 		if ($data[$required_fields[$j]] == "") 
	 		{  
	 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
	 	 		$error = 1;
	 	 		
	 		}//END OF IF
	 	}//END OF FOR
	 }
	//check if user already exists
	if (get_users_info(array('get_by_uname' => 1, 'uname' => $data['uname'])) == 1 AND $settings['action'] == 'AddUser') 
	{
  	 		$error_list[$required_fields[count($error_list)+1]] = "<b>".$data['uname']."</b> ".$lang["uname_taken"];
 	 		$error = 1;	 
	}
	 if ($data['password'] != $data['confirm_password']) 
 	{
  	 		$error_list[$required_fields[count($error_list)+1]] = $lang["pass_no_match"];
 	 		$error = 1;	
 	}
 	if (valid_email($data['email']) == 0) 
 	{
 		$error_list[$required_fields[count($error_list)+1]] = $lang["error_valid_email"];
 		$error =1;
 	}
 	if ($settings['unique_user_mail'] AND $settings['action'] == 'AddUser') {//This will use unique email address
 		if (get_users_info(array('get_by_email' => 1, 'email' => $data['email'])) == 1)
 		{
 			$error_list[$required_fields[count($error_list)+1]] = "<b>".$data['email']."</b> ".$lang["error_dublicate_email"];
 			$error = 1;
 		}
 	}
 if ($error == 1) 
 {
 	$smarty->assign("error_list",$error_list);//assigned template variable error_list<BR>
 	if ($settings['return_html']) {
 		return $smarty->fetch("common/error_list.tpl");
 	}
 	else {
 		return $error_list;
 	}
 }
else //add new user
 {
		//add user to database
 	$uname = $data['uname'];
 	$pass = text_crypt($data['password']);
 	$email = $data['email'];
 	$user_name  = $data['user_name'];
 	$user_join = time();
 	$user_class = $data['user_class'];
 	$user_image = $settings['default_user_image'];
 	$user_location = $data['user_location'];
 	$admin = ($data['user_class'] == 'U' OR !$data['user_class']) ? 0 : 1;
 if (!isset($data['user_login']) AND $settings['send_verification_mail']) {//FROM SITE, SEND EMAIL TO VERIFY
 	$user_login = 0;
 	$send_mail = 1;
 }
 elseif (!isset($data['user_login']) AND !$settings['send_verification_mail']) {//FROM SITE, DON'T SEND MAIL BUT VERIFY HERE
 	$user_login = 1;
 }
 else {//FROM ADMIN
 	$user_login = (isset($data['user_login'])) ? $data['user_login'] : 0;
 }
// 	$user_prefs = form_user_settings($default_user_prefs);
if ($settings['action'] == 'AddUser') 
{

$sql->db_Insert("users","'','$uname', '$pass','$user_sess','$email','$user_name','$icq','$aim','$msn','$user_location','$birthday',
'$user_signature','$user_image','$user_timezone','$user_join','$user_last_visit','$user_current_visit',
'$user_comments','$user_prefs','$admin','$user_login','$user_class','$user_perms','$reviews','$articles'");
 	$uid = mysql_insert_id();
 	if ($settings['use_profile']) {
  	//Create user profile
	$sql->db_Insert("users_profile","'$uid','','','','',''");		
 	}

if ($send_mail == 1) {
 	$verification_url = URL."/verify_$uid"."_".text_crypt($user_join).".html";
 	$extra_find[] = '#USERNAME#';$extra_replace[] = $uname;
 	$extra_find[] = '#VERIFICATION_URL#';$extra_replace[] = $verification_url;
 	users_send_verification_mail(array('namefrom' => COMPANY_NAME,'mailfrom' => COMPANY_EMAIL,'mailto' =>$email,'mail_subject' => process_text($settings['verification_mail_subject'],$extra_find,$extra_replace),'mailmessage' => process_text($settings['verification_mail_body'],$extra_find,$extra_replace)));
}
 	$user_folder = $settings['user_folder']."/user_".$uid;
 	mkdir(ABSPATH.$user_folder,0755);
 	$sql->db_Update("users","user_folder = '$user_folder' WHERE id = $uid");
######################### NEWSLETTER ###########################################
if ($settings['newsletter']) 
{
if ($data['newsletter'] == 1) 
{
	if (get_user_subscriptions($uid) == 0)
	{
	$ip = $_SERVER['REMOTE_ADDR'];
	$sql->db_Insert("maillist","'','$user_name','$email','$uid','$user_join','',1,0,'$ip'");
	}
}	
}
}//END ADD
elseif ($settings['action'] == 'UpdateUser')
{
	
	if ($data['password']) {
		$pass_txt = " , pass = '$pass'";
	}
	$sql->db_Update("users","uname = '$uname', user_class = '$user_class', user_login = '$user_login', admin = '$admin', user_location = '$user_location',
	 user_name = '$user_name',email = '$email' $pass_txt
	WHERE id = ".$data['id']);
}//END UPDATE
return true;
 }//END NO ERRORS
	
}//END FUNCTION

function users_send_verification_mail($settings)
{
include(ABSPATH."/includes/phpmailer/class.phpmailer.php");
$mailer = new PHPMailer();
$mailer->IsHTML(true);
$mailer->From = $settings['mailfrom'];  // This HAS TO be your gmail address or it won't work!
$mailer->FromName = $settings['namefrom']; // This is the from name in the email, you can put anything you like here
$mailer->Subject = $settings['mail_subject'];
$mailer->Body = $settings['mailmessage'];
$mailer->CharSet ="utf-8";
$mailer->AddAddress($settings['mailto']);
  if(!$mailer->Send())
   {
		$res = 1;
   } 
   else {
		$res =0;
   }
 	return $res;
}

function get_users_favorites($settings)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$item_q = ($settings['itemid']) ? " AND itemid = ".$settings['itemid'] : "";
	$limit = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
	$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby']." ".$settings['way'] : "";
	$sql->db_Select("users_favorites",$fields,"uid = ".$settings['uid']." AND module = '".$settings['module']."' $item_q $orderby $limit");
	if ($settings['debug']) {
		echo "SELECT $fields FROM users_favorites WHERE uid = ".$settings['uid']." AND module = '".$settings['module']."' $item_q";
	}
	if ($sql->db_Rows()) {
		if ($settings['analyze']) {
			$a = execute_multi($sql);
			for ($i=0;count($a) > $i;$i++)
			{
				$a[$i]['item'] = get_item(array('module'=>$a[$i]['module'],'id'=>$a[$i]['itemid'],'active'=>1,'lang'=>$settings['lang'],'debug'=>$settings['debug']));
			}
			return $a;
		}//end ANALYZE
		else {
			return execute_multi($sql);
		}
		
	}
	else {
		return 0;
	}
}

function get_item_votes($settings)
{
	global $sql;
	$sql->db_Select("users_items_votes","*","itemid = ".$settings['id']." AND module = '".$settings['module']."'");
//	echo "SELECT * FROM users_items_votes WHERE itemid = ".$settings['id']." AND module = '".$settings['module']."'";
	if ($sql->db_Rows()) {
		if ($settings['check']) {
			return 1;
		}
		else {return execute_single($sql);}
	}
}

function get_users_votes($settings)
{
	global $sql;
	if ($settings['id']) {//just check
		$sql->db_Select("users_votes","*","itemid = ".$settings['id']." AND module = '".$settings['module']."' AND uid = ".$settings['uid']);
//		echo "SELECT * FROM users_votes WHERE itemid = ".$settings['id']." AND module = '".$settings['module']."' AND uid = ".$settings['uid'];
		if ($sql->db_Rows()) {
			if ($settings['check']) {
				return 1;
			}
			else {return execute_single($sql);}
			
	}

		
	}
}

function get_users_comments($settings)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	if ($settings['id']) {//get for one item only
		$sql->db_Select("users_comments",$fields,"module = '".$settings['module']."' AND itemid = ".$settings['id'].$settings['orderby']);
		if ($settings['debug']) {
			echo "SELECT  * FROM users_comments WHERE module = '".$settings['module']."'  AND itemid = ".$settings['id'].$settings['orderby'];
		}
		if ($settings['count']) {
			return $sql->db_Rows();	
		}
		else {
		if ($sql->db_Rows()) {
			$comments = execute_multi($sql);
			if ($settings['get_user']) {//Get the user data as well
				for ($i=0; count($comments) > $i;$i++)
				{
					$comments[$i]['user'] = get_users($comments[$i]['uid'],0);
				}
			}
			return $comments;
		}//END ROWS
		}

	}//END SINGLE ITEM
	else {//MULTIPLE ITEMS
		$uid = ($settings['uid']) ? " AND uid = ".$settings['uid'] : "";
		$sql->db_Select("users_comments",$fields,"module = '".$settings['module']."' $uid".$settings['orderby'].$settings['limit']);
			if ($settings['debug']) {
				echo "SELECT  $fields FROM users_comments WHERE module = ' $uid".$settings['orderby'].$settings['limit'];
			}
		if ($sql->db_Rows()) {
			$comments = execute_multi($sql);
			if ($settings['get_user']) {//Get the user data as well
				for ($i=0; count($comments) > $i;$i++)
				{
					$comments[$i]['user'] = get_users($comments[$i]['uid'],0);
				}
			}

			if ($settings['get_item']) {
				for ($i=0; count($comments) > $i;$i++)
				{
					$comments[$i]['item'] = get_item(array('module'=>$settings['module'],'lang'=>$settings['lang'],'active'=>$settings['active'],'id'=>$comments[$i]['itemid'],'thumb'=>$settings['thumb']));
				}
			}
			if ($settings['get_user']) {//Get the user data as well
				for ($i=0; count($comments) > $i;$i++)
				{
					$comments[$i]['user'] = get_users($comments[$i]['uid'],0);
				}
			}
			return $comments;
		}//END ROWS
	}
	if ($settings['uid']) {//COMMENTS FROM THIS USER
		
		$limit_q = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
		$sql->db_Select("users_comments",$fields,"uid = ".$settings['uid']." AND module = '".$settings['module']."' AND status = ".$settings['status']." ORDER BY date_added $limit_q");
		if ($sql->db_Rows()) {
			$a = execute_multi($sql);
			if ($settings['get_item']) {
				for ($i=0;count($a) > $i;$i++)
				{
					$a[$i]['item'] = get_item(array('module'=>$a[$i]['module'],'id'=>$a[$i]['itemid'],'active'=>1,'lang'=>$settings['lang']));
				}
			}
			return $a;
		}//END ROWS
	}//END USER
	
}

function get_item($settings)
{
	if ($settings['module'] == 'content') {
		return get_content($settings['id'],$settings['lang'],$settings['active']);
	}
	elseif ($settings['module'] == 'products') {
		return get_products($settings['id'],$settings['lang'],$settings['active']);
	}
	elseif ($settings['module'] == 'recipes') {
		return  get_recipe(array('active'=>$settings['active'],'id'=>$settings['id'], 
	'debug'=>$settings['debug'],'thumb'=>$settings['thumb'],'ingredients'=>$settings['ingredients'],'main'=>$settings['main']));
	}
}

function get_users_profile($settings)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$sql->db_Select("users_profile",$fields,"id = ".$settings['id']);
	if ($sql->db_Rows()) {
		if ($settings['quick']) {//GET RAW DATA
			return execute_single($sql);
		}//END QUICK
		else {//ANALYZE THE PROFILE
			$profile = execute_single($sql);
			$profile['profile'] = form_settings_array($profile['profile'],"###",":::");
			$profile['settings'] = form_settings_array($profile['settings'],"###",":::");
			if ($profile['friends_list'] AND $settings['friends_list']) {
				$friends = explode("|",$profile['friends_list']);
				for ($i=0;count($friends) > $i;$i++)
				{
					$profile['friends'][$i] = get_users($friends[$i],0);
				}
			}//END FRIENDS
			return $profile;
		}//END FULL
	}//END FOUND
}//END FUNCTION

function get_users_points($settings)
{
	global $sql;
	if ($settings['quick']) {
		$sql->db_Select("users_points","SUM(points) AS sum_points","uid = ".$settings['uid']);
		$a = execute_single($sql);
		if ($settings['debug']) {
			echo "SELECT SUM(points) AS sum_points FROM users_points WHERE uid = ".$settings['uid'];
		}
		return $a['sum_points'];
	}
	
}

function add_points($points_list,$settings)
{
	global $sql;
	//First check if this is a unique action
	if ($points_list[$settings['action']]['unique_per_page'])//NOW CHECK IF THIS ACTION HAS ALREADY BEEN DONE
	{
		$sql->db_Select("users_points","id","uid = ".$settings['uid']." AND itemid = ".$settings['id']." AND module = '".$settings['module']."' AND action = '".$settings['action']."'");
		if ($settings['debug']){
		echo "SELECT id FROM users_points WHERE id = ".$settings['uid']." AND itemid = ".$settings['id']." AND module = '".$settings['module']."' AND action = '".$settings['action']."'";
		}
		if ($sql->db_Rows()) {//Action has been already made. no go
			return 0;
		}
		else {//FIRST TIMER. ADD POINTS
			$sql->db_Insert("users_points","'',".$settings['uid'].",".$settings['id'].",'".$settings['module']."','".$settings['action']."',".$points_list[$settings['action']]['value'].",".time());
		//update the users profile table
			$sql->db_Update("users_profile","points = points + ".$points_list[$settings['action']]['value']." WHERE id = ".$settings['uid']);
		}//END ADD ACTIOM
	}
	else {
		$sql->db_Insert("users_points","'',".$settings['uid'].",".$settings['id'].",'".$settings['module']."','".$settings['action']."',".$points_list[$settings['action']]['value'].",".time());
		//update the users profile table
		$sql->db_Update("users_profile","points = points + ".$points_list[$settings['action']]['value']." WHERE id = ".$settings['uid']);
	}
	
}

function get_users_points_list($settings=0)
{
	global $sql;
	if ($settings['orderby']) {
		$sql->db_Select("users_points_list","*","ORDER BY ".$settings['orderby']." ".$settings['way'],$mode="no_where");
	}
	else {
		$sql->db_Select("users_points_list","*");
	}
	
	if ($sql->db_Rows()) {
		if ($settings['form_array']) {
			$v = execute_multi($sql);
			for ($i=0;count($v) > $i;$i++)
			{
				$ar[$v[$i]['var']] = $v[$i];
			}
			return $ar;
		}
		else {
			return execute_multi($sql);
		}
		
	}
}
?>