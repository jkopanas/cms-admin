<?php
/** 
* Version 0.1. Will perform tasks concerning the categories.
*
*/
HookParent::getInstance()->doBindHookGlobally("afterCategoryUpdate","afterCategoryUpdateActions");
HookParent::getInstance()->doBindHookGlobally("afterCategoryAdd","afterCategoryAddActions");

function afterCategoryUpdateActions($data){
	checkMenus($data);
}

function afterCategoryAddActions($data){
	checkMenus($data);	
}

/**
 * will check which menus have the auto add children, will add the new categories to them. If the user moved the menu, then it will parse it and see what to put where
 *
 * @param array $data
 */
function checkMenus($data) {
	
}

/**
 * When a category is added, it will check to see if the parent belongs to an extra field. it will then add it automatically. 
 *
 * @param array $data
 */
function checkEfields($data) {
	
}
?>