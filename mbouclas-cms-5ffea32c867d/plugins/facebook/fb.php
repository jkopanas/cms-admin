<?php
class fb extends user {


	/**
	 * The users profile
	 *
	 * @param int $id
	 * @param array $settings
	 * @return array
	 */
	
	const APP_ID = '188422644620707';
	const SECRET_ID = 'eafd0993b73d9a7125456bb94bf1cd08';
	const NAME_SPACE = 'fbtetrapak';
	const SCOPE_PERMISIONS = 'email,user_birthday,status_update,publish_stream,user_about_me,read_stream,user_likes,friends_likes';
   
	protected $okuser="";
	protected $nouser="";
	protected $insertuser="http://www.facebook.com/dialog/oauth/";
	
	public $facebook_uid = 0;
	public $like_app = array();
	public $facebook_profile = 0;
	public $fbObj = '';
	public $sign_request='';
	
	public function __construct() {
		
		parent::user();

		require_once ABSPATH.'/plugins/facebook/facebook_sdk3/facebook.php';

		$this->fbObj = new Facebook(array(
  					'appId'  => self::APP_ID,
  					'secret' => self::SECRET_ID,
					'cookie' => true
				));
				
		$this->sign_request=$this->fbObj->getSignedRequest();
		
	}
	
	public function checkUserState () {
		
		if ($this->fbObj->getUser()) {
			
			$this->GetRegisterUser($this->sign_request['user_id']);
		} 
		
	}
	
	
	public function insert_fb_user () {
		
		$data=$this->fbObj->api('/me');
		$ar = array( "user_name" =>$data['first_name'], "user_surname" => $data['last_name'], "email" => $data['email'], "uname" => $data['username'] );
		$gender = ($data['gender'] == "male") ? 1 : 0;
		list($day, $month, $year) = explode('/',$data['birthday']);
		$ar_profile = array( 
										"birthday" => mktime(0, 0, 0, $month, $day, $year),
										"facebook_uid" => $data['id'],
									    "gender" => $gender,
									    "settings" => json_encode(array("hometown" => $data['hometown'],"timezone" => $data['timezone'], "location" => $data['location'], "locale" => $data['locale'], "link" => $data['link']))
									 );
		$this->addUser($ar, array( "use_profile" => $ar_profile, "action" => "addUser", "debug" => 0 ));
		$this->facebook_profile = $this->userProfile(0,array("getUser" => 1,"searchFields" => array( "facebook_uid" => $data['id']) ));
		$settings = json_decode($this->facebook_profile['profile']['settings'],true);
		$this->like_app = ($settings['like'])? $settings['like'] : array();
		
	}
	
	
	public function GetRegisterUser($uid) {

			$this->facebook_uid=$uid;
			$this->facebook_profile = $this->userProfile(0,array("getUser" => 1,"searchUser" => array("facebook_uid" => $uid )));
			$settings = json_decode($this->facebook_profile['profile']['settings'],true);
			$this->facebook_profile['profile']['settings']=$settings;
			$this->like_app = ($settings['like'])? $settings['like'] : array();
			return (empty($this->facebook_profile['id'])) ? $this->insert_fb_user() : true;
	}

	
	public function userProfile($id = 0,$settings=0)
	{
		global $sql;
		$fields = ($settings['field']) ? $settings['field'] : "*";
		
		if ($id) {
			$q[] = "id =$id";
		}

		if ($settings['searchUser']) {			
			foreach ($settings['searchUser'] as $key => $value ) {
				$q[] = $key. "='".$value."'";
			}	
		}
	
		if (is_array($q)) {
			$query = implode(" AND ",$q);
		}
		
		$sql->db_Select($this->tables['profile']['table'],$fields,$query);
		if ($sql->db_Rows() > 0) {
			$u=execute_single($sql);
			if ($settings['getUser']) {
				 $user=$this->searchUsers(array( "return" =>"single","debug" =>0,"searchFields" => array("id" =>$u['id'])));	
				 $user['profile'] = $u;
				 return $user;
			}
		} else {
			return array();
		}
		
	}//END FUNCTION
	
	
	public function addUserProfile($data,$settings=array()) {
		global $sql;
		foreach ($data as $k => $v)
		{ 	 				 	       
			$keys[] = $k;
			$values[] = "'$v'";
		}
		
		$sql->db_Insert($this->tables['profile']['table']." (".implode(",",$keys).")",implode(",",$values));
		if ($settings['debug']) {
			echo "INSERT INTO ".$this->tables['profile']['table']." (".implode(",",$keys).") VALUES (".implode(",",$values).")";
		}
		
		
		
	}



}