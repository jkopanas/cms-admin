<?php
	if (!$_POST['signed_request'] && !$_REQUEST['ajax']) { return; }
	include ABSPATH.'/plugins/facebook/fb.php';

	$user = new fb();
	
	$user->checkUserState();
	
	HookParent::getInstance()->doBindHook('content', "AfterInsertImage","InsertUserImages");
	HookParent::getInstance()->doBindHook('content', "AfterUpload","OpenUploadPopUp");
	HookParent::getInstance()->doBindHookGlobally("HomePreFetch","StartFacebook");
	HookParent::getInstance()->doBindHook('content', "ContentFetch","ContentFacebook");
	
	HookParent::getInstance()->doBindHook('content', "ContentPageFetch","ContentPageFacebook");
	
	
	HookParent::getInstance()->doBindHookGlobally("LoadHook","RemoveHook");
	
	
	HookParent::getInstance()->doBindHookGlobally("BeforeLoadTemplate","LoadUserFilterTemplate");
	HookParent::getInstance()->doBindHookGlobally("AfterInitUser","UserExtendFb");
	HookParent::getInstance()->doBindHookGlobally("AfterManageUserTemplate","AddGridCommands");

	
	
	function InsertUserImages($id) {
		global $sql,$user;
		$sql->db_Insert("users_items_image"," '',".$user->facebook_profile['id'].",".$id);
		
		return;
		
	}
	
	function OpenUploadPopUp() {
		global $smarty;
		$smarty->display("plugins/facebook/fileUploader.tpl");
		exit;
	}
	
	function RemoveHook($fileName) {

		return '/plugins/facebook/index.php';
		
	}
	
	
	function ContentPageFacebook () {
		
		global $smarty,$item,$current_category;

		if (is_numeric($_GET['image'])) {
			
			$item['data']['template']=$smarty->fetch("plugins/facebook/ShowImage.tpl");
   			foreach ($item['detailed_images']['images'] as $k => $v ) {
   				if ( $v['id'] == $_GET['image'] ) {
   					$data = $v;
   				}
   			}
   			$item['detailed_images']['images'] = $data;
			$items['data']=$item['data'];
			$items['results']['item'] = $item;
			
		} else {
			
			$item['data']['template']=$smarty->fetch($current_category['settings']['facebook_content_page']);
			$items['data']=$item['data'];
			$items['results']['item'] = $item;

		}

		$items['results']['featured']=$smarty->getTemplateVars('featured');

		if ($_POST['ajax']) { echo json_encode($items); exit; }
		

		$smarty->display("plugins/facebook/home.tpl");
		
	}
	
	
	function ContentFacebook ($module) {
		
		global $smarty,$items;
		
	
		
		$items['data']['template']=$smarty->fetch($smarty->getTemplateVars('include_file'));		
		$item['data']=$items['data'];
		
		$item['results']['item'] = $items['results'];
		$item['results']['featured']=$smarty->getTemplateVars('featured');

		if( $signedRequest['page']['liked'] == 1 ) {
			$smarty->$smarty->fetch("include_file", "plugins/facebook/facebook_not_like.tpl");
		}
		
		if ($_POST['ajax']) { echo json_encode($item); exit; }
		

		$smarty->display("plugins/facebook/home.tpl");
		

	}
	
	function StartFacebook () {
		
		global $smarty,$Content,$user;
	
		$featured['featured'] = $Content->FeaturedContent(27,array('get_results'=>1,'active'=>1,'debug'=>0,'thumb'=>1,'images'=>1,'orderby'=>'orderby','cat_details'=>1));

		$featured['user'] = $user->facebook_profile;
	
		$data['template']=$smarty->fetch("plugins/facebook/facebook_like.tpl");

		
		if ($_POST['ajax']) { echo json_encode(array("data" => $data, "results" => $featured )); exit; }
		$smarty->display("plugins/facebook/home.tpl");
		exit;
	}

	function AddGridCommands ($module) {
		global $smarty;
		$smarty->assign("plugin_file", "plugins/facebook/admin/GridCommands.tpl");
	}

	function LoadUserFilterTemplate ($ar) {
		global $smarty;
		$smarty->append("boxFilter",array("title" => "Facebook Search","file" => "plugins/facebook/admin/FilterTpl.tpl"));
	}


	function UserExtendFb () {
		global $user;
		include ABSPATH.'/plugins/facebook/fb.php';
		$user = new fb();
	}


?>
