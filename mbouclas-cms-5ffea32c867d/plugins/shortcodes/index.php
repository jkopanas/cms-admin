<?php

$s = new ShortcodeParser();
$s->register('tpl', 'sc_tpl');
foreach ($loaded_modules as $v) {
	if ($v['is_item']) {
		HookParent::getInstance()->doBindHook($v['name'], "afterItemFetch","applyShortCodes");
	}
}
function applyShortCodes($item) {
	global $s;
	$item['description'] = $s->parse($item['description'],array('target'=>'description','data'=>$item));
	$item['description_long'] = $s->parse($item['description_long'],array('target'=>'description_long','data'=>$item));
	return $item;
}

function sc_tpl($p,$c,$i) {
	//p = params; c = content enclosed
	if ($p['php']) {
		include_once(ABSPATH."/plugins/".$p['php']);
	}
	if ($p['template']) {
		global $smarty;
		$smarty->assign('params',$p);
		$smarty->assign('c',$c);
		$smarty->assign('i',$i->extra['data']);
		return $i->extra['data'][$i->extra['target']] = $smarty->fetch("skin/plugins/shortcodes/".$p['template'].".tpl");
	}
	else {
		return $i->extra['data'][$i->extra['target']];
	}
}
?>