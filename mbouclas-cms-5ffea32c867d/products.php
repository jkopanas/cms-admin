<?php 
include("init.php");
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ###########################################
$current_module = $loaded_modules['products'];
$Content = new Items(array('module'=>$current_module));
$smarty->assign("current_module",$current_module);
if ($_GET['cat']) {
	$posted_data = $_POST;//setup a a clean array
	$cat = $_GET['cat'];
	if (is_numeric($cat)) {
	$cat = ($cat) ? $cat : 0;
	}
	else {//REVERSE LOOKUP
		$cat = str_replace("products/","",$cat);
		$sql->db_Select("products_categories","categoryid","alias = '$cat'");
		if ($sql->db_Rows()) {
			$a = execute_single($sql);
			$cat = $a['categoryid'];
		}
	}

	$current_category = $Content->ItemCategory($cat,array('table'=>'products_categories','settings'=>1));
	}
	if (array_key_exists('themes',$loaded_modules)) 
	{
		$active_theme = load_theme_by_name($current_category['settings']['theme'],$theme_module['settings']);
		$theme_settings = json_decode($active_theme['settings'],true);
		$smarty->assign("theme_settings",$theme_settings);
	}//END OF IF

	$posted_data['categoryid']= $cat;$posted_data['availability']=1;$posted_data['thumb']=1;$posted_data['main']=1;
	
//	$posted_data['sort'] = ($settings['sort']) ? $settings['sort'] : $current_module['settings']['default_sort'];
	if ($current_category['settings']['orderby'] AND !$posted_data['sort'] AND !$posted_data['sort_direction']) {
		$posted_data['sort'] = $current_category['settings']['orderby'];
		$posted_data['sort_direction'] = $current_category['settings']['way'];
	}
	elseif (!$posted_data['sort'] AND !$posted_data['sort_direction'] AND !$current_category['orderby'])
	{	
		$posted_data['sort'] = $current_module['settings']['default_content_sort'];
		$posted_data['sort_direction'] = $current_module['settings']['default_sort_direction'];
	} 
	if ($current_category['settings']['results_per_page'] AND !$posted_data['results_per_page']) {
		$posted_data['results_per_page'] = $current_category['settings']['results_per_page'];
	}
	else {
		$posted_data['results_per_page'] = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $current_module['settings']['items_per_page'];	
	}
	if ($theme_settings['efields']) {//SEE IF THE THEME REQUIRES EFIELDS
		
		$posted_data['efields'] = 1;
	}
	$posted_data['advanced'] = $posted_data['sort'];
	$posted_data['page'] = $page;
	$posted_data['suffix'] = '_'.$current_module['name'];
	$posted_data['GetCommonCategoriesTree'] = 0;
	$posted_data['search_in_subcategories']=1;
	$items = $Content->ItemSearch($posted_data,$current_module,$page,0);
	$parent = ($current_category['parentid']) ? $current_category['parentid'] : 0;
	$smarty->assign("posted_data",$posted_data);
	$smarty->assign("cat",$current_category);
	$smarty->assign('nav',$Content->CatNav($current_category['categoryid'],array('debug'=>0,'table'=>'products_categories')));
	$smarty->assign("subs",$Content->ItemTreeCategories($current_category['categoryid'],array('table'=>'products_categories','get_subs'=> 1,'num_items'=>1,'debug'=>0)));
	$smarty->assign("more_categories",$Content->ItemTreeCategories(0,array('table'=>'products_categories','get_subs'=> 1,'num_items'=>1)));

	########################## HACK TO DISPLAY IMAGES FROM ONE FEATURED ITEM ############################################
	$featuredGallery = $Content->FeaturedContent($cat,array('fields'=>'itemid,orderby','get_results'=>0,'active'=>1,'orderby'=>'orderby','return'=>'single','limit'=>1,'debug'=>0));
	if ($featuredGallery['itemid']) {
		$Img = new ItemImages(array('module'=>$current_module));
			$smarty->assign("FeaturedGallery",$Img->GetItemDetailedImages($featuredGallery['itemid'],$current_module['settings']['default_image_type']));
	}
	//print_ar($Content->ItemTreeCategories($parent,array('table'=>'products_categories','num_items'=>1)));
	if ($items['results']) {
	######################### GRAB RESULT SPECIFIC STUFF #############################
	$posted_data['justIds'] = 1;
	$allItemIds = $Content->ItemSearch($posted_data,$current_module,$page,0);
	$implodedItemIds = implode(',',$allItemIds);
	
	$filters = $Content->applyCategoryFilters($current_module['name'],$implodedItemIds,array('commonCategories'=>0,'efields'=>1,'recursive'=>1));

	$smarty->assign("commnonCategories",$filters['commonCategories']);
	$Efields =  new ExtraFields(array('module'=>array('name'=>$current_module['name'])));

//    $data=$Efields->filterItemsEfields($implodedItemIds,$current_module['name'],array('grouped'=>1,'debug'=>0,'searchable'=>1,'count'=>1,'values'=>1));
//    echo '<pre>';
//    print_r($data);
//    echo '</pre>';


	$efieldValues = $Efields->filterItemsEfields(0,$current_module['name'],array('grouped'=>1,'debug'=>0,'searchable'=>1,'count'=>1,'values'=>1,'defZero'=>1,'mergeWithItems'=>$implodedItemIds));//This will get ALL fields
	
	$smarty->assign('efields',$efieldValues);

	######################### END GRAB RESULT SPECIFIC STUFF #########################
	}//END IF RESULTS
	########################### END HACK ################################################################################
	if (array_key_exists('themes',$loaded_modules)) 
	{
		$active_theme = load_theme_by_name($current_category['settings']['theme'],$theme_module['settings']);
		$theme_settings = unserialize($active_theme['settings']);
		$smarty->assign("theme_settings",$theme_settings);
	}//END OF content MODULE
	
         if ($loaded_modules['maps']) {
         	$settings = array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid','debug'=>0);
         	$map = new maps(array('module'=>$current_module));
         	$mapItems = $map->getItems($implodedItemIds,$settings);
			
         	$smarty->assign('mapItemsRaw',json_encode($mapItems));
         	
			if ($mapItems) {
				$formatedItems = $map->formatItems($mapItems,array('module'=>$current_module));
				$smarty->assign("mapItems",$formatedItems);
				$smarty->assign("fomatedItemsRaw",json_encode($formatedItems));
			}
         	
         }//END MAPS
	
$l = new siteModules();
$layout = $l->pageBoxes('all',e_FILE,'front',array('getBoxes'=>'full','fields'=>'boxes,areas,id,settings','boxFields'=>'id,title,name,settings,fetchType,file,required_modules,template','init'=>1,'boxFilters'=>array('active'=>1),'debug'=>0));
$smarty->assign("layout",$layout['boxes']);
$display = ($active_theme['file']) ? $active_theme['file'] : 'products.tpl';
$smarty->assign("featured",$Content->FeaturedContent($cat,array('get_results'=>1,'active'=>1,'debug'=>0,'thumb'=>1,'type'=>'itm','orderby'=>'orderby','cat_details'=>1)));
$validation_arr = array( rand(1,10), rand(1,10));
$smarty->assign("validation",$validation_arr);
$smarty->assign("validation_sum",array_sum($validation_arr));
$smarty->assign("mode",$_GET['mode']);
$smarty->assign("nav_area","products");
$smarty->assign("area",$current_category['alias']);
$smarty->assign("data",$items['data']);
$smarty->assign("items",$items['results']);
$smarty->assign("include_file",$loaded_modules['themes']['folder']."/".$display);//assigned template variable include_file
include(ABSPATH."/memcache.php");
$smarty->caching = USE_SMARTY_CAHCHING;
$smarty->display("home.tpl",$url);//Display the home.tpl template
?>