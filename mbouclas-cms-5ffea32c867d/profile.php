<?php
include("init.php");
$user = new user();

$id = $user->getUserID(array("uname"=>$_GET['uname']));

$settings=array("efields"=>1,"comments"=>1);
$details = $user->userDetails($id,$settings);

$smarty->assign('user',$details);
$smarty->assign("settings_user",json_decode($details['settings'],true));

$smarty->assign("include_file","modules/users/profile.tpl");//assigned template variable include_file
$smarty->display("home.tpl");//Display the home.tpl template
?>