head.ready(function () {
    var gridCombo = '';
    var isEdit = 1;
    var db = new mcms.dataStore({
        method: "POST",
        url: "",
        dataType: "json",
        trigger: "loadComplete"
    });

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/ajax/loader.php?file=Languages.php&action=FilterLanguages",
                dataType: "json",
                type: "POST"
            },
            parameterMap: function (data, type) {
                if (data.filter != undefined) {
                	$.each(data.filter.filters, function (index, item) {		
                	    	data.filter.filters[index].table="languages";
                	});   
         		}
         		if (type == "create" ) {
         			data=data.models[0];
         		} 
                return data;
       		}
        },
        schema: {
        	data: function (data) {
        		return data.data;
        	},
            model: {
            	data: "data",
                id: "name",
                fields: {
                    name: { type: "string" },
                    descr: { type: "string" },
                    value: { type: "string" },
                    topic:  { }
                }
            },
            total: function(data) {  
            	return data.option.total;
         	},
         	parse: function(data) {
        		gridCombo = data.listbox;
        		return data;
        	}
        },
        pageSize: 10,
        filter: {field: "code", value: "en" , operator: "eq"},
        serverPaging: true,
        serverFiltering:  true,
        batch: true
    });



    var grid = $("#gridLanguage").kendoGrid({
        dataSource: dataSource,
        height: 450,
        pageable: true,
        filterable: { extra: false },
        scrollable: true,
		toolbar: [{ text: "" , template: kendo.template($("#id-toolbar-lang").html())}],
        columns: [{
            field: "name",
            title: " Variable Name"
        }, {
            field: "descr",
            title: "Description"
        }, {
            field: "topic",
            title: "Θέμα",
            width: "75px",
            editor: categoryDropDownEditor,
            filterable: false
        }, {
            field: "value",
            title: "Value",
            editor: editor
        }, {
            command: [{
                text: "",
                template: "<span class='details-button k-icon k-edit pointer'></span>"
            }, {
           		text: "",
                template: function(data) { return "<span data-id='${uid}' data-name='${name}' class='deleteLanguageVariable k-icon k-delete pointer'></span>"; }
            }],
            filterable: false,
            title: " ",
            width: "50px"
        }]
    }).data('kendoGrid');

    var wnd = $("#details");
    
     var validatable = $("#singleVar").kendoValidator().data("kendoValidator");
    
    $("#gridLanguage").delegate(".details-button", "click", function (e) {
        
     
        if ( !(obj = grid.dataItem($(this).closest("tr")))) {
        	var addurl = "loadSingleVariable";
        	var title = $.lang.add_edit_lang_var;
        } else {
        	var addurl = "loadSingleVariable&name=" + obj.name + "&uid=" + obj.uid;
        	var title = $.lang.add_edit_lang_var;
        }

        if (!wnd.data("kendoWindow")) {
            wnd.kendoWindow({
                width: "650",
                title: title,
                content: "/ajax/loader.php?file=Languages.php&action=" + addurl,
                close: closed,
                visible: true,
                modal: true
            });
            wnd.data("kendoWindow").center();
        } else {
            wnd.data("kendoWindow").refresh({
                url: "/ajax/loader.php?file=Languages.php&action=" + addurl
            });
            wnd.data("kendoWindow").center();
            wnd.data("kendoWindow").open();
        }
    });
    
      $('.saveSingleTranslation').live('click', function (e) {
	
      var validatable = $("#singleVar").kendoValidator().data("kendoValidator");
      if (validatable.validate()) {   
      var form = $(this).parentsUntil('form').parent();
     
      if ( !(uid = $('input[name=uid]').val())) {
      	uid = 0;
			varName = 0;
			var action = "addTranslationVariable";
			 db.trigger = 'loadCompleteAdd';
      } else {
      	 var varName = $('input[name=Varname]').val();
      	 var action = "saveSingleTranslation";
      	  db.trigger = 'loadComplete';
      	 
      }

      data = new mcms.formData(form.find('.SaveVar'), { 'ReturnType': 'array' });

      db.url = "/ajax/loader.php?file=Languages.php&action="+action;
      db.data = {
          'data': data,
          'uid': uid,
          'name': varName
      };
      
     
      $(db).loadStore(db);
      }
  });
  
    $(db).bind('loadCompleteAdd', function () {
	  
	  	var obj = db.records;
		 wnd.data("kendoWindow").close();
		 f=grid.dataSource.filter();
		   o = {};
		  
		   $.each(obj, function (index, value) {
		   		if (value.code == f.filters[0].value ) {
		   			  for (var attrname in value) { o[attrname] = value[attrname]; }
		   		}
		   });

	grid.dataSource.insert(0, o);
	grid.dataSource.sync();

	
});
    
  $(db).bind('loadComplete', function () {
		
		f=grid.dataSource.filter();
      var obj = db.records;
      wnd.data("kendoWindow").close();
		
      var tr = grid.dataItem(grid.tbody.find(">tr[data-uid=" + obj.uid + "]"));
      grid.editRow(grid.tbody.find(">tr[data-uid=" + obj.uid + "]"));
    
      o = {};
      $.each(obj.data, function (index, value) {
          s = index.split("-");
          if ( s[1] == f.filters[0].value) {
              o[s[0]] = value;
          }
      });
      
      for (var attrname in tr) { tr[attrname] = (o[attrname]) ? o[attrname] : tr[attrname]; }
      grid.saveRow();

  });

	function editor(container, options) {
		
		$('<textarea id="value-en"  name="value-en" rows="5" cols="10"></textarea><br><a class="ckEditor" rel="value-en" data-width="150" data-height="150" href="#">HTML edition</a>').appendTo(container);
			$(".k-edit-field").addClass("spacer-bottom");
	}

    function categoryDropDownEditor(container, options) {
		
    data = gridCombo;
    var drop = $('<input  />')
                        .appendTo(container)
                        .kendoDropDownList({
                            dataTextField: "text",
                            dataValueField: "value",
                            autoBind: false,
                            dataSource: data[options.field]
                        });
    }
    
    
    var kendoWindow = $("<div />").kendoWindow({
          title: $.lang.del_variavle,
          resizable: false,
          width:"180",
          modal: true
    });

            
   $(".langcode").live('click',function() {
   		grid.dataSource.filter({field: "code", value: $(this).data('code') , operator: "eq"});
   });
            
   $(".delete-confirm,.delete-cancel").live('click',function() {

                    if ($(this).hasClass("delete-confirm")) {
               		 	var obj = $("tr[data-uid='"+$(this).data('id')+"']");
            		 	db.url = "/ajax/loader.php?file=Languages.php&action=deleteTranslationVariable&var=" +$(this).attr('data-name');
            	 	 	db.trigger = 'deleteLanguageVar';
            	  	 	$(db).loadStore(db);
            	  
            	  	 	itemToRemove=grid.dataSource.getByUid($(this).data('id'));
            			grid.dataSource.remove(itemToRemove);
             			grid.dataSource.read();
                    }
                    
                    kendoWindow.data("kendoWindow").close();
                });
	
	$('.deleteLanguageVariable').live('click', function(e) {
    	e.preventDefault(); 
        
    	obj = { id:$(this).data('id'), name: $(this).attr('data-name')  };
	  	var template = kendo.template($("#delete-confirmation").html());
        
        kendoWindow.data("kendoWindow")
            .content(template({ item: obj }))
            .center().open();
        
      
 });  // end click 
	
	
	
	
    $("#createLang").live('click', function (e) {
		
		e.preventDefault();
    	var ts = Math.round((new Date()).getTime() / 1000);
   		var td = '<td align="center"><select id="select-'+ts+'">'+ $("select[name=new_language]").html() + '</select></td>';
   		td += '<td width="150" align="center"></td>';
   		td += '<td align="center"></td>'
   		td += '<td align="center"></td>'
   		td += '<td width="100" align="center"><span class="createRow k-icon k-update"></span><span class="cancelRow k-icon k-cancel"></span></td>';
   		$('#MainLang tr:last').after('<tr id="' + ts + '">'+td+'</tr>');


    });
    
    
    
    	$(".createRow").live('click', function () {
			var id = $(this).parents("tr").attr("id");
			var code = $("#select-"+id).val();
		   
	        db.url = "/ajax/loader.php?file=Languages.php&action=addNewLanguage";
    	    db.data = {
        		data: code,
        		id: id
        	}
        	
        	db.trigger = 'newLang';
        	$(db).loadStore(db);

        });
        
          $(db).bind( 'newLang', function () {
          			records = db.records
        			var row = '<td align="center" id="'+records.code+'" >' + records.country + '</td>';
				   row += '<td width="100" align="center"><label><input type="radio" value="Y" name="active-' + records.code + '" data-action="active" class="save setLanguageAvailability inpt_b" >'+$.lang.yes+'</label> <label><input type="radio" value="N" data-action="active"  name="active-' + records.code + '" checked  class="save setLanguageAvailability" />'+$.lang.no+'</label></td>';
				   row += '<td align="center"><input type="radio" name="default_lang_admin"  data-action="adefault" class=" save SelectDefaultLang inpt_b" value="' + records.code + '" /></td>';
				   row += '<td align="center"><input type="radio" name="default_lang"  data-action="sdefault" class="save SelectDefaultLang inpt_b" value="' + records.code + '"  /></td>';
				   row += '<td align="center" width="50"><a href="#' + records.code + '" class="DeleteLanguage"  title="'+$.lang.delete+'" rel=""><span class="k-icon k-delete pointer"></span></a></td>';

				   $("#"+records.id).html(row);
          });
    
    
        $(".cancelRow").live('click', function () {
			$(this).parents("tr").remove();    
        });
    
    
    


    $(".save").live('click', function () {


    		var thisName = $(this).attr('name').split("-");
			thisName = thisName[1]; 		
			action = $(this).attr('data-action');
            if ( action =='active' ) {
                db.url = "/ajax/loader.php?file=Languages.php&action=setLanguageAvailability&active=" + $(this).val() + "&var=active-" + thisName;
                db.trigger = 'ChangeAvailability';
            } else if ( action =='adefault' ) {
                db.url = "/ajax/loader.php?file=Languages.php&action=setDefaultLanguage&code=" + $(this).val() + '&var=default_lang_admin';
                db.trigger = 'DefaultLangAdmin';
            } else if (action == 'sdefault' ) {
                db.url = "/ajax/loader.php?file=Languages.php&action=setDefaultLanguage&code=" + $(this).val() + '&var=default_lang';
                db.trigger = 'DefaultLang';
            }

      $(db).loadStore(db);


    });



    $(".DeleteLanguage").live('click', function (e) {
    	
    	e.preventDefault();
        obj = $(this).attr('href').split('#');
    	
  	    kendoWindow = $("<div />").kendoWindow({
  	            title: $.lang.del_lang,
  	            resizable: false,
  	            width: "160",
  	            modal: true
  	        });
  	    
  	        obj_d = { obj: obj[1] };
  	        var template = kendo.template($("#delete-confirmation_lang").html());
  	        kendoWindow.data("kendoWindow")
  	        .content(template({ item: obj_d }))
  	        .center().open();
    
    });
    
    
    $(".delete-confirm_lang").live("click", function(e){ 
    	

        db.url = "/ajax/loader.php?file=Languages.php&action=deleteLanguage&code=" + obj[1];
        db.trigger = 'DeleteLang';
       $("#"+obj[1]).parents("tr").remove(); 
        
        $(db).loadStore(db);
        kendoWindow.data("kendoWindow").close();
    });


});
