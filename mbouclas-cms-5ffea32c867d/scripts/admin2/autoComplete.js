$.fn.autoComplete = function(options) {
	var aC = function(element, options) {
		var settings = $.extend( {
		'resultsPlaceHolder' : 'autoCompleteResults',
		'autoCompleteLength' : 0,
		'delay' : 500,
		'minChars' : 3,
		'enterCallback' : '',
		'url' : '../ajax/usersFront.php?action=quickFindUser',
		'clickCallback' : '',
		'matchesClass' : 'matched'
		}, options);
	
	var placeHolder = $('#' + settings.resultsPlaceHolder);

	var resultsCache = {};
	var autoCompleteLength = settings.autoCompleteLength;
	var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
	    clearTimeout (timer);
	    timer = setTimeout(callback, ms);
	  };
	})();
	var ajax = new $.dataStore({
	    method: "POST",
	    url:settings.url,
	    dataType: "json",
		trigger:"loadComplete"
	});
	
	$(ajax).bind('loadComplete',function(){
		records = ajax.records;
		if (records)
		{
			resultsCache[ajax.term] = records;
			createAutoComplete(ajax,ajax.term,records);
		}//END RESULTS
	});

	$('html').click(function(event) {
		
	    if($(event.target).parents().index($(element)) == -1) {

	        placeHolder.hide(); 
	    }        
	});
	
	$(element).keyup( function(e) {
	var code = e.keyCode || e.which;

	$(placeHolder).show();
	if ((code != 32) && (code != 40) && (code != 38) && (code != 13)) //Deactivate on space
	{
	//Do something
	}
	else if (code === 40) { //Down arrow
	
	if($('#' + settings.resultsPlaceHolder + " li.hovered").index()==$("li").length-1) return;
	if ($('#' + settings.resultsPlaceHolder + " li.hovered").length == 0) { //if no li has the hovered class
	    $('#' + settings.resultsPlaceHolder + "  li").eq(0).addClass("hovered");
	    
	} else {
	    $('#' + settings.resultsPlaceHolder + " li.hovered").eq(0).removeClass("hovered").next().addClass("hovered");
	}
	}
	else if (code === 38) { //Up arrow
	if($('#' + settings.resultsPlaceHolder + " li.hovered").index()==0) return;
	if (!$('#' + settings.resultsPlaceHolder + " li.hovered")) { //if no li has the hovered class
	    $('#' + settings.resultsPlaceHolder + " li.hovered").eq(0).removeClass("hovered");
	} else {                 $('#' + settings.resultsPlaceHolder + " li.hovered").eq(0).removeClass("hovered").prev().addClass("hovered");
	}
	}
	else if (code === 13) {
	e.preventDefault();
	if ($('#' + settings.resultsPlaceHolder + " li.hovered").length > 0) {
		callbackHandler(settings.clickCallback,$.extend({}, settings,{ event:e,obj:$("li.hovered")}));
		$(placeHolder).hide();
	}
	}
	
	if($(this).val().length != autoCompleteLength) {
	autoCompleteLength = $(this).val().length;
	
	if ($(this).val().length > settings.minChars)
	{
	if ($(this).val() in resultsCache)
	{
		createAutoComplete(dashBoxes,$(this).val(),resultsCache[$(this).val()]);
	}//END CACHED
	else
	{
		var me = $(this);
		delay(function(){
		ajax.parent = me;
		ajax.term = me.val();
		ajax.data = {data:me.val()};

		$(ajax).loadStore(ajax);
		  }, settings.delay );
	
	}//END NOT CACHED
	}//END CHAR LENGTH
	else if ($(this).val().length ==0)
	{
	$("#autoCompleteResults").hide();
	}
	}
	});
	
	var updateHaystack = function(input, needle) {
	
		var r =  new RegExp(needle, "gi");
		return input.replace(r,function(m) { return '<span class="'+ settings.matchesClass + '">' + m + '</span>'});
	}
	
    var callbackHandler = function(func)
	{
		if (this[func])
		{
			window[func].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else { console.log(func + ' is not registered'); }
	}
	
	var createAutoComplete = function(obj,term,records)
	{
			var x = ($(placeHolder).length ==0) ? $('<ul id="'+ settings.resultsPlaceHolder + '" style="width:' + obj.parent.width() + 'px; background:white;z-index:999;"></ul>').insertAfter(ajax.parent) : $(placeHolder);
			placeHolder = $(x);
			x.children().remove();
			$.each(records, function(key, val) {
				var node = '#' + val.id +' '+val.surname + ' ' + val.name + ' ' + ' ' + val.mobile + ' ' + val.email;
				x.append('<li id="' + val.id +'">' + updateHaystack(node, term) + '</li>');
			});
			placeHolder.find('li').click(function(event) {
				callbackHandler(settings.clickCallback,$.extend({}, settings,{ obj:$(this)}));
			});
	}

	};//END CLASS

return this.each(function() {

	var element = $(this);
	// Return early if this element already has a plugin instance
	if (element.data($(this).attr('id'))) return;
	var c = new aC(this,options);
	element.data($(this).attr('id'), c);
});
};

