head.ready(function(){
	
	 var db = new mcms.dataStore({
         method: "POST",
         url:"",
         //dataType: "json",
         trigger:"loadComplete"
     });
		
	///start tree and load parent categories///// 
	if ( $('input[name=table_id]').val() == "" ) {
		var url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/itemsCategories.php&action=getCategories"; 
	 } else {
		var url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/commonCategoriesTree.php&action=getCategories&tableID="+$('input[name=table_id]').val(); 
	 }     
	kendos = { dragAndDrop: true, drop: onDrop, dragend: dragEnd, select: clickable, template:  kendo.template($("#id-template").html()) }
    var treeView = $("#treeview").treeview({ kendo: kendos , source: url  }).data('tree');
	 treeView.init();
	  /// end////
	
	  
	    $(".clicked").live("click", function(){
   			node_id=$(this).attr('id');
	 		treeView.clicked(node_id);
	  		$(this).removeClass('k-icon');
 	    	$(this).removeClass('k-plus');  
 	    	$(this).removeClass('clicked');  
 	}); 
	    		    
	  				   /////delete category //////
	  
	 $(".delete").live("click", function(e){
	    	
		    node_id=$(this).attr('id');
	    	e.preventDefault(); 
	        var kendoWindow = $("<div />").kendoWindow({
	                title: $.lang.delete_category,
	                resizable: false,
	                width:"180",
	                modal: true
	            });
	        
	        kendoWindow.data("kendoWindow")
	            .content($("#delete-confirmation").html())
	            .center().open();
	        
	        kendoWindow
	            .find(".delete-confirm,.delete-cancel")
	                .click(function() {
	                    if ($(this).hasClass("delete-confirm")) {
	                    	
	                    	if ($('input[name=table_id]').val()==""){
	                   		 
	                    		var url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/itemsCategories.php&action=delete_category"; 
	                    		 
	                    	 } else {
	                    		 
	                    		 var url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/commonCategoriesTree.php&action=delete_category&tableID="+$('input[name=table_id]').val();
	                    		 	
	                    	 } 
	                    	
	                    	db.url= url;
	            	 		db.trigger ='delete_cat';
	            	 	    db.data = {'node_id':node_id}
	            	 	    $(db).loadStore(db);
	                    }
	                    
	                    kendoWindow.data("kendoWindow").close();
	                })
	                .end()
	 });  // end click 
  
	    $(db).bind('delete_cat',function(){
	    	records = db.records;
	    	datas = $.parseJSON(records);
	    	
	    	//var grid_cat = $("#gridview_cat").kgrid().data('grid');	
	    //	grid_cat.removeRow(datas.id);
	    	
	    	 $(datas).each(function(index, value) {
	    		 treeView.remove("#parent_list"+value.id);
	    	 });
	    
	    }); //end bind delete_cat
	    
	    			////end delete category/////
	    		   
	    
	   $(db).bind('loadTabs',function(){
	 	
		   records = db.records;
		   records = $.parseJSON(records);
			$.each(records,function(index,value) {
				$('#'+value.box.name).html(value.template);
			});
				


	 });
	    
			/// show featured////

		 $(".feature").live("click", function(){  
			 var node_id=$(this).attr('id');
			 var id=node_id.split("link");
			 $('input[name=category_id]').val(id[1]);
			 var name =$('#item'+id[1]).html();
				$("#f_name").html(name);
				$("#f_name_f").html(name);

			     url_itm="/ajax/loader.php?file=itemsSearch.php&action=pagesFilter&module="+$('input[name=current_module_name]').val()+"&mode=FeaturedContent&category_id="+id[1];
	 			
	 			grid = $("#gridview").kgrid().data('grid');
	 			grid.readgrid(url_itm,'reset');
	 			
	 			 url_cat="/ajax/loader.php?file=items/itemsCategories.php&module="+$('input[name=current_module_name]').val()+"&action=allFeauturedCategories&category_id="+id[1];
		 			grid_cat = $("#gridview_cat").kgrid().data('grid');
		 			grid_cat.readgrid(url_cat,false);

	 		     var url_itm = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/itemsCategories.php&action=getFeauturedCategories&id="+id[1];
	 		    var treeView_f = $("#treeview-featured").treeview().data('tree');
	 		     treeView_f.updatetree(url_itm);
		  
		 });  
		    	/// end show featured ///
	 	
	 $(".tabStrip").kendoTabStrip().css('border','none');

	 var boxes = {};var  main = {};
	 $('.boxItem').each(function(){
	 	boxes[$(this).attr('id')] = false;	
	 });

	

	 
	 
	/*	
		var node_id=$("#treeview").find('li:first').attr('id');
	  if (node_id==undefined){ 
		  var id = [];
		  id[1] = 0;
	  } else {
		  var id=node_id.split("parent_list");
			var name =$('#item'+id[1]).html();
	  }
	 
		$("input[name=category_id]").val(id[1]);
		$("#f_name").html(name);
		$("#f_name_f").html(name);
		
		*/
	 
	 $("#link0").live('click',function(e) {
		    $("input[name=category_id]").val("0");
			$("#f_name").html($.lang.homepagecat);
			$("#f_name_f").html($.lang.homepagecat);
			e.preventDefault();
	 });
	 
		
		$("input[name=category_id]").val("0");
		$("#f_name").html($.lang.homepagecat);
		$("#f_name_f").html($.lang.homepagecat);
		
		
  
   		var ar =[];
	 	$.each($(".boxItem"),function(index,value) {
	 		ar.push("boxID[]="+$(value).data('boxid'));
	 	});
  		
	 		if ($('input[name=table_id]').val()==""){
	    			db.url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+'&'+ar.join("&");
	   				db.trigger = 'loadTabs';
	   				$(db).loadStore(db);
	 		}
  
  var wndr = $('#OpenLangWnd').kendoWindow({
                width: "650",
                visible: false,
                content: '',
                modal: true
            }).data("kendoWindow");

            
            
 	
	$(db).bind('loadlang',function(){

		$('#OpenLangWnd').html(db.records);
	
        wndr.center().open();
	});
	
	$(dbLanguageBox).bind('returnlang',function() {
        wndr.close();
	});
	
	
	$(".SearchText").live('keyup',function() {
		
    	grid = $("#"+$(this).data('grid')).kgrid().data('grid'); 
    	
    	if (! isNaN ($(this).val()-0)) {
    		if ($(this).val() != "") {
    			grid.filter([{ field: $(this).data('eq') ,operator: "eq", value: $(this).val() }]);	
    		} else {
    			grid.ReturnGrid().dataSource.transport.options.read.data={};
    			grid.filter({ value: '' });
    		}
    	} else {
    		grid.filter([{field: $(this).data('contains') ,operator: "contains", value: $(this).val()}]);	
    	}	
	});
	
	
		$("#ResetSearch").live('click',function() {
			
			grid = $("#gridview_cat").kgrid().data('grid');
			grid.filter({});
			$("#searchtext").val("");
			
		});
  
$(".TranslateCategoryItem").live('click',function() {
	    
	
		data = {
			id: $(this).data('id'),
			groupby: 'field',
			table: $('input[name=current_module_name]').val()+"_categories",
			tpl: 'modules/categories/category_lang.tpl'
		}

		db.url="/ajax/loader.php?file=items/itemsCategories.php&action=loadSingleTranslation";
		db.trigger = 'loadlang';
		db.data = data;
		db.dataType='';
		$(db).loadStore(db);
	
});


				
				 function clickable(e) {
					 
				 }
				
				    function onDrop(e) { 
				    	console.log(e);
				    	var drop_position=$(e)[0].dropPosition;
				    	console.log(drop_position);
				    	if (drop_position=='over') {
				    		var destination=$(e.destinationNode).attr("id");
				    		var id =destination.split("parent_");
				    	
				    		if ("#"+$(id[1]).hasClass('clicked')) {
				     			$("#"+id[1]).trigger('click');	
				     		}	
				              //e.preventDefault();	
				    	}
				    } // end function
	 
				 
	    function dragEnd(e) {
	    	var destination=$(e.destinationNode).attr("id");
	    	var source = $(e.sourceNode).attr("id");
	    	var drop_position=$(e)[0].dropPosition;
	    	over= (e.dropPosition == "over") ? "&over=1" :"";
	    	
	    	if ($('input[name=table_id]').val()==""){
          		 
        		var url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/itemsCategories.php"+over+"&action=change_order"; 
        		 
        	 } else {
        		 
        		 var url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/commonCategoriesTree.php"+over+"&action=change_order&tableID="+$('input[name=table_id]').val(); 
        		 	
        	 } 
	  	
	    		db.url = url;
	    		db.trigger ='saveState';
	        	db.data = { destination: destination , source: source,drop_position:drop_position };
	    		
	    		$(db).loadStore(db);
	    		          
	    }//end function

	    		/// end change order and drop nodes ////
	    
});// end head


	function DrawSubcategories(options) {
		
	 	datas = options.records;
		var tree=$("#treeview").data("tree");	 	
	 	var parent = $("#list"+datas[0].parentid).parents("li:first");
	 	parent.attr('id','parent_'+datas[0].parentid);
	 	
	 	$(datas).each(function(index, value) {

	 		 		 obj= { id: value.id, text: value.text, sub_cat: value.sub_cat };
	 				 tree.append(obj,parent);
					 var parent_li = $('#item'+value.id).parents("li:first");
	    	         parent_li.attr('id','parent_list'+value.id);
	 			  
	    });
	 	

	}

