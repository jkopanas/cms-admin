head.ready(function(){
		
		 var db = new mcms.dataStore({
         method: "POST",
         url:"",
         //dataType: "json",
         trigger:"loadComplete"
     });
      
		
	    		/// add edit form ///
	    var wnd = $("#edit_form");
	    
        $(".k-edit").live( "click", function() {
        	
        	var node_id=($(this).attr('id') == undefined ) ? "" : "&node_id=" + $(this).attr('id');   	
	    	var action= ($(this).attr('id') == undefined ) ? "&action=open_form" : "&action=edit_category";
	    	
	    	if ($('input[name=table_id]').val()==""){
	   		 
	   		 var url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/itemsCategories.php"+ node_id+action; 
	   		 
	   	 } else {
	   		 
	   		 var url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/commonCategoriesTree.php"+ node_id+action+"&tableID="+$('input[name=table_id]').val(); 
	   		 	
	   	 }    
	    		    	
                if (!wnd.data("kendoWindow")) {
                        wnd.kendoWindow({
                                width: "750",
                                height: "520",
                                title : $.lang.add_edit_cat,
                                content : url,
                                visible : true,
                                modal : true
                        });
                        wnd.data("kendoWindow").open();
                        wnd.data("kendoWindow").center();
                } else {
                        wnd.data("kendoWindow").refresh({
                               url : url
                        });
                        wnd.data("kendoWindow").open();
                        wnd.data("kendoWindow").center();
                        
                }
        });  
        						/// add edit form ///
        				
        
    					/////add - edit category////
        
        $(".add_edit_cat").live("click", function(){
        	
        	var button_id =$('.add_edit_cat').attr('id');
		    var action= (button_id == 'edit_cat' ) ? "&action=edit_category_item" : "&action=new_category";
		    
		   if ($('input[name=table_id]').val()==""){
		     var url="/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/itemsCategories.php"+action;;
		   } else {
			   var url="/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/commonCategoriesTree.php"+action+"&tableID="+$('input[name=table_id]').val();

			   
		   } 
		    
	 	   	db.url=url;
	 		db.trigger ='add_edit_category';
	 	    db.data = {data:mcms.formData('.cat_details',{ getData:false})}
	 	    
	    	$(db).loadStore(db);
	    	
	 	});   
	 	
	 	
	 	$(db).bind('add_edit_category',function(){
	    	records = db.records;
	    	datas_insert = $.parseJSON(records);
	    	var button_id =$('.add_edit_cat').attr('id');
	    	var treeView=$("#treeview").data("tree");
	   		
	    	 wnd.data("kendoWindow").close(); 
	    	
		    	if (button_id=='edit_cat'){
		    		 id=$('#cat_id').val();
		    		 var new_text=$("#new_text").val();
		    		 $("#parent_list"+id).find("#item"+id).html(new_text);
		    		
		    	}
		    	else if (datas_insert.has_parent==1) {
		    	   if ($('input[name=table_id]').val()==""){	
		    		grid_cat = $("#gridview_cat").kgrid().data('grid');
			 	   	grid_cat.addRow(datas_insert);
		    	   }
		    	 var path=datas_insert.path.split("\/");
		    	 var parent_node=$('#treeview').find('#parent_list'+datas_insert.parent).find('span:first');
		    	 var parent_node_next_span=parent_node.find('span:first');
		   
	    	  if (parent_node_next_span.hasClass('new_item') || (parent_node_next_span.hasClass(''))) {
					var parent = $('#parent_list'+datas_insert.parent);
						obj= { id: datas_insert.id, text: datas_insert.text, sub_cat: 0 };
	    	    		treeView.append(obj, parent);
	 			}   
	    	  else if (!parent_node.hasClass('k-icon')) {
		    	 		 $(path).each(function(index, value) {
		    	 		  if ($("#list"+value).hasClass('clicked')) {
		    	 			$("#list"+value).trigger('click');	
		    	 			}
		    	 		}); 
	    	       } 
	    	  else {	  		
	    		  		var parent = $('#parent_list'+datas_insert.parent);
	    		  		obj= { id: datas_insert.id, text: datas_insert.text, sub_cat: 0 };
	    	    		treeView.append(obj, parent);
				}
		    	 	
		     } // end if has parent
	     	 else {
	     	   if ($('input[name=table_id]').val()==""){	 
	     		grid_cat = $("#gridview_cat").kgrid().data('grid');
		 	   	grid_cat.addRow(datas_insert);
	     	   }
		 	   	var parent_li= [];
	     		var parent_ul = $("#treeview").find("ul");
    			$("#treeview").find("li").each(function (index,value){
    				if ($(value).hasClass("k-last")){
    					 parent_li= $(value).first().parents("li");
    				}
    			}); 

    			if (parent_li.length==0){		
    				var parent_ul_li = $("#treeview").find("ul").find("li:last");
    					if (parent_ul_li.length==0){
    				obj= { id: datas_insert.id, text: datas_insert.text, sub_cat: 0 };
    				treeView.insertBefore(obj, parent_ul);
    					} else {
    						
    					obj= { id: datas_insert.id, text: datas_insert.text, sub_cat: 0 };
    				treeView.insertAfter(obj, parent_ul_li.first());
    				}
    			}	
    			else { 
    				  obj= { id: datas_insert.id, text: datas_insert.text, sub_cat: 0 };
    				  treeView.insertAfter(obj,  parent_li.last());
    			}
    		 }	
	     var parent_li = $('#item'+datas_insert.id).parents("li:first");
			 parent_li.attr('id','parent_list'+datas_insert.id);
	    }); //end bind add_edit_cat
	
	    
});// end head

