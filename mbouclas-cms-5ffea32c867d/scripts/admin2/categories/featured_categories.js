head.ready(function(){
	
	 var db = new mcms.dataStore({
         method: "POST",
         url:"",
         //dataType: "json",
         trigger:"loadFeautured"
     });

	 var id = $('input[name=category_id]').val();
      url = "/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/itemsCategories.php&action=getFeauturedCategories&id="+id;
      kendos = { dragAndDrop: true, drop: onDrop_f, dragend: dragEnd_f, select: '',  template: kendo.template($("#f-template").html())}
      var treeView_f = $("#treeview-featured").treeview({ kendo: kendos , source: url  }).data('tree');
	  treeView_f.init();
	  	
	  
  $(".saveGrid").live("click", function(){
	  		
	  	    id=$(this).attr('id'); 	   	
	 	   	db.url="/ajax/loader.php?file=ajax_responses.php&module="+$('input[name=current_module_name]').val()+"&action=AddItem&mode=FeaturedContent&id="+id+"&type=itm&category_id="+$('input[name=category_id]').val();
	 	   	db.item=$(this);
	 		db.trigger ='add_article_itm';
	 	    db.data = {'node_id':id}
	 	    $(db).loadStore(db);
  });   

	 				    
		  $(".k-add-cat").live("click", function(){
		    
		  	 	   id=$(this).attr('id');
			 	   	db.url="/ajax/loader.php?file=ajax_responses.php&module="+$('input[name=current_module_name]').val()+"&action=AddItem&mode=FeaturedContent&id="+id+"&type=cat&category_id="+$('input[name=category_id]').val();
					db.item=$(this);
			 		db.trigger ='add_article_itm';
			 	    db.data = {'node_id':id}
			 	    $(db).loadStore(db);
		    	
			});
					

	    $(db).bind('add_article_itm',function(){
	    	records = db.records;
	    	datas_insert_f = $.parseJSON(records);
	    	
	    	if (datas_insert_f.type == "itm" ) {
	    		grid = $("#gridview").kgrid().data('grid');
	    	} else {
	    		grid = $("#gridview_cat").kgrid().data('grid');
	    	}
	    	grid.removeRow(db.item);
	    	
	    	if (datas_insert_f !=null) {
	    	type = (datas_insert_f.type == "itm"  ) ? "("+$.lang.page+")" : "("+$.lang.category+")";
	    	}
	    	var parent_li= [];
     		var parent_ul = $("#treeview-featured").find("ul");
			$("#treeview-featured").find("li").each(function (index,value){
				if ($(value).hasClass("k-last")){
					 parent_li= $(value).first().parents("li");
				}
			}); 
			 
			if (parent_li.length==0){		
				var parent_ul_li = $("#treeview-featured").find("ul").find("li:last");
					if (parent_ul_li.length==0){
					
								
				obj= { id: datas_insert_f.id, text: datas_insert_f.text+" "+type, sub_cat: 0,type: datas_insert_f.type };
				treeView_f.insertBefore(obj, parent_ul); 
							
					} else {
						
						obj= { id: datas_insert_f.id, text: datas_insert_f.text+" "+type, sub_cat: 0,type: datas_insert_f.type };
					treeView_f.insertAfter(obj, parent_ul_li);
					}
			}	
			else { 
				  obj= { id: datas_insert_f.id, text: datas_insert_f.text+" "+type, sub_cat: 0,type: datas_insert_f.type };
				  treeView_f.insertAfter(obj,  parent_li.last());
			}
			var parent_li = $('#item_f'+datas_insert_f.id).parents("li:first");
			 parent_li.attr('id','parent_item_f'+datas_insert_f.id);
			 $('#item_f'+datas_insert_f.id).addClass('itm');
	    }); //end bind 
	    
	    
	    
	    $(".k-delete-f").live("click", function(e){
	  	 	  
	  	 	  var id=$(this).data('id');
	  	 	  var type = $(this).data('type');
	 
	  	 	
	  	 	e.preventDefault(); 
	        var kendoWindow = $("<div />").kendoWindow({
	        	 	title: $.lang.delete_feautured,
	                resizable: false,
	                width:"220",
	                modal: true
	            });
	        
	        kendoWindow.data("kendoWindow")
	            .content($("#delete-confirmation").html())
	            .center().open();
	        
	        kendoWindow
	            .find(".delete-confirm,.delete-cancel")
	                .click(function() {
	                  if ($(this).hasClass("delete-confirm")) {
	                	  	db.url="/ajax/loader.php?file=ajax_responses.php&module="+$('input[name=current_module_name]').val()+"&action=DeleteItem&mode=FeaturedContent&id="+id+"&type="+type+"&category_id="+$('input[name=category_id]').val();
	                	  	db.trigger ='delete_article';
	                	  	db.data = {'node_id':id}
	                	  	$(db).loadStore(db);
	                   }
	                    
	                    kendoWindow.data("kendoWindow").close();
	                })
	                .end()
	 });  // end click 
		 
		    $(db).bind('delete_article',function(){
		    	
		    		records = db.records;
		    		records = $.parseJSON(records);
		    	  if (records.type=='itm') {
		    		grid = $("#gridview").kgrid().data('grid');
	 	   			grid.addRow(records);
		    	  } else {
		    		grid_cat = $("#gridview_cat").kgrid().data('grid');
			 	   	grid_cat.addRow(records);
		    	  }	
		    	 
	 	   		var span =$("#parent_item_f"+records.id).find("#item_f"+records.id);
	 	   		
	 	   		treeView_f.remove("#parent_item_f"+records.id);
	 	   			
		    });
		    	    
			  function onDrop_f(e) { 
				  
			    	var drop_position=$(e)[0].dropPosition;
			    	if (drop_position=='over') {	
			              e.preventDefault();	
			    	}
			    	
			  } // end function
			    
			    function dragEnd_f(e) {
			    	
			    	var destination=$(e.destinationNode).attr("id");
			    	var source = $(e.sourceNode).attr("id");
			    	var drop_position=$(e)[0].dropPosition;
			    	
			    	var cat_id=$('input[name=category_id]').val();
			    	db.url="/ajax/loader.php?module="+$('input[name=current_module_name]').val()+"&file=items/itemsCategories.php&action=change_order_featured";
			    		db.trigger ='saveState_f';
			        	db.data = { destination_f: destination , source_f: source,drop_position:drop_position,cat_id:cat_id };
			    		
			    		$(db).loadStore(db);
			    	
			    }//end function 
			    
			    
});//end head
