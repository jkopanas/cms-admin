/* 
* HANDLES THE MODIFY CATEGORY STUFF. CAN BE USED AS A PLUGIN 
* CAN DRAW A MODAL WITH THE EDITOR IN
* UPDATES THE DB, YOU CAN CALL A TRIGGER FOR BEFORE & AFTER UPDATE EVENTS
* SHOWS FEATURED EXTRA FIELDS AND TRANSLATIONS
* FOR THE IMAGE UPLOAD TO WORK WE NEED THE MEDIA EDITOR PLUGIN
* CREATES A CATEGORY VIRTUAL GALLERY IN THE SETTINGS JSON
*/

(function ($) {
	jQuery.fn.center = function(parent) {
		
    if (parent) {
        parent = this.parent();
    } else {
        parent = window;
    }
    this.animate({
        "position": "absolute",
        "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
        "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
    });
return this;
}

$.categoryEditor = function( options ) {	
if (typeof $.FormFields !== 'function'){
	head.js('/scripts/admin2/formFields.js');
	var formFields = new $.FormFields({classes:'saveData inpt_a'});
}

var url = "/ajax/loader.php?file=items/itemsCategoriesNew.php";
var dbDefaults = {    
	method: "POST",
    dataType: "json",
	trigger:"getCat"
};

var appDB = new mcms.dataStore(dbDefaults);
var featuredItems = {};
/* 
* THE APP OBJECT 
* METHODS : 
* -- onAppLoaded => when app is loaded
*/
 catEdit =  $.extend(true,{
 	_trigger: function( type, data ) {
 		data = data || {};
 		if ($.isFunction(this.settings[type])){
 			eval(this.settings[type]).apply(this, Array.prototype.slice.call(arguments, 1));
 		}
 	},
	appLoaded: false,
	cache: {},
	options : { 
		open:'.modalOpen',
		target: '',//The modal window
		next:'<a href="#" class="right">next</a>',
		prev:'<a href="#" class="left">previous</a>',
		close:'<a href="#" class="close">close</a>'
	},
	templates : {},
	settings : {},
	controller : function(callBack,params) {
		this.callbackHandler(callBack,params);
		this.switchTab(this.uri.action);
	},
	init: function(options) {
		this.settings = $.extend(this.options,options);
		this.templates = $.extend(this.templates,options.templates);
		this.target = this.settings.target;
		this.loadApp();
	},
	loadApp: function() {
		//CHECK FOR PARAMS TO BRING THE REQUIRED MODULE
		var uri = this.parse(window.location.hash);
		$('#modal-back').remove();
		if (typeof uri.action != null && typeof uri.params != 'undefined') {//FOUND PARAMS
			this.uri = uri;
			//LOAD THE APP WINDOW
			this.categoryid = uri.params.categoryid;
		}
		else { return false; }
		if (this.appLoaded == false)//FIRST TIME HERE
		{
			this.cache = {};//reset in case prev next was pressed
			if (typeof this.settings.categoryList != 'object') {//DURING INITIALIZATION THE CATEGORIES TREE WAS NOT PROVIDED
				this.settings.categoryList = this.fetchCategories();
			}
			$('body').append('<div id="modal-back"></div>');
			this.appLoaded = true
		}//END FIRST TIME
		$(this.target).empty();
		$(this.target).append(this.settings.next).append(this.settings.prev).append(this.settings.close);
		$(this.target).show();
		//CALL FOR AJAX TO GET THE CATEGORY
		appDB.url = url + '&action=getCat&module='+mcms.mcmsSettings.module;
		appDB.data = {categoryid:this.uri.params.categoryid};
		appDB.method = 'POST';
		appDB.dataType = 'json';
		appDB.async = 'true';
		appDB.trigger = 'getCat';
		$(appDB).loadStore(appDB);
		this.calcNextPrev(this.settings.categoryList,this.uri.params.categoryid);
		this._trigger( "onAppLoaded",this,appDB );
	},
	renderView: function(view,obj) {
		
	},
	modifyCategory : function(obj) {//the modify pane
		this.cache['modifyCategory'] = true;
		this.categoryEditor(obj);
	},
	translations : function(obj) {//translations pane
		this.cache['translations'] = true;
	   this.categoryTranslations(this.currentCategory);
	},
	featured : function(obj) {//featured pane
		
		if (typeof $.featuredItems !== 'function'){
			head.js('/scripts/admin2/items/featuredItems.js');
		}
        if (this.cache['featured'] !== true) {
		  featuredItems = new $.featuredItems({currentCategory:this.currentCategory.cat.categoryid,target:$(this.target).find('#featured'),allCategories:this.settings.allCategories,allPages:this.settings.allPages,items:this.currentCategory.featured});//COMES FROM A PLUGIN
        }
        this.cache['featured'] = true;
	},
	fetchCategories: function(){//GOES TO AJAX AND FETCHES CATEGORIES IF NOT HERE. WILL FLATTEN THEM AS A UNIFIED LIST.
		return ;
	},
	calcNextPrev: function(all,me) {
		$(this.target + ' .left, ' + this.target + ' .right').show();
		var found =0;
		for (i in all) {
			if (all[i] == me) {
				found = parseInt(i);
			}
		}
	
		if (typeof all[found-1] == 'undefined') {
			$(this.target + ' .left').hide();
		}
		else { $(this.target + ' .left').attr('rel',all[found-1]); }
		if (typeof all[found+1] == 'undefined') {
			$(this.target + ' .right').hide();
		}
		else { $(this.target + ' .right').attr('rel',all[found+1]); }
		
		$(this.target).data({'categoryid':me,'me':me,prev:all[found-1],next:all[found+1]});

	},
	createURI: function() {
		var u = new Array;
		for (j in this.uri.params) {
			u.push(j);
			u.push(this.uri.params[j]);
		}
		return '!/'+this.uri.action+ '/' + u.join('/');
	},
	switchTab: function(obj) {
		var tabs = $(this.target).find('.tab');
		$(tabs).hide();
		var me = $(this.target + '  .tabsS a[rel='+obj+']');
		$(this.target).find('.tab[id='+obj+']').show();
		$(this.target + '  .tabsS a').removeClass('current');
		this.uri.action = obj;
		me.addClass('current');
		if (typeof this.cache[obj] == 'undefined') {//CHECK IF THE MODULE FUNCTIONS WERE LOADED
			eval(this[obj]());//WILL LOAD THE MODULE
		}
	},
	createAlias : function(inpt) {
		var txt ='';
		for (var i=0; i < inpt.length; i++) {
	    	 txt+=mcms.convertchar(inpt.charAt(i))
		}
	return txt;
	},
	saveCat: function(obj,close) {
		var tmp = mcms.formData(obj.parents('div:first').find('.saveData'),{ getData:true});
		db.url = url + '&action=updateCat&module='+mcms.mcmsSettings.module;
		db.current = obj;
		db.data = {data : tmp,categoryid:obj.attr('rel'),mode:'quick'};
		db.trigger = 'catSaved';
		this._trigger( "onBeforeSave",{el:obj,obj:this,form:tmp} );
		$(db).loadStore(db);	
	},
	close: function() {
		$('#modal-back').remove();
		$(this.target).hide();
		this.appLoaded = false;
        featuredItems = {};
        
        this.cache['featured'] = false;
		window.location.hash = '';//reset
	}

},mcms.app);//END OBJ

/* 
* THE categoryEditor OBJECT 
* METHODS : 
* -- onBeforeSave => Before submiting
* -- onAfterSave => After Submiting
*/
catEdit.categoryEditor = function(obj){
	var that = this;
	
	$(this.target).find('input[name=category]').on('keyup',function(e){
    	$(app.target).find('input[name=alias]').val(app.createAlias($(this).val()));
	});
	
	$(this.target).find('.saveCat, .saveCatExit').on('click',function(e){
		e.preventDefault();
		app.saveCat($(this));
	});
	
	$(db).bind('catSaved',function(){
		saved();
		that._trigger( "onAfterSave",{data:db.records,obj:that,form:db.data} );
		if (db.current.attr('data-close')) {
			app.close();
		}
	});
};//END MODIFY

catEdit.categoryTranslations = function(obj) {
    var that = this;
	var target = $(this.target).find('#translations');
    var tmp = this.templates['translations'](obj);
    target.append(tmp);
    
};//END MODIFY

/*var xp = new Object(catEdit);
console.log(xp)*/
catEdit.init(options);

var app=catEdit;//It's a scope thing

/* DB BINDS */

	var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
	    clearTimeout (timer);
	    timer = setTimeout(callback, ms);
	  };
	})();
	
$(appDB).bind('getCat',function(){
//	$('.debugArea').html('<pre>' + db.records + '</pre>');
	app.currentCategory = appDB.records;
	$(app.target).append(app.templates.fullEditTemplate(app.currentCategory ));
	$(app.target).center();
	app.controller(app.uri.action,app.uri.params);//OPEN THE RESPECTIVE PANE
});
   
/* EVENT BINDS */
$(window).bind('scroll', function() {
	
			delay(function(){
$(app.target).center();

		  }, 50	 );

});

$(window).bind('hashchange', function() {
	if (window.location.hash.indexOf("#!/") == -1)//OUT OF SCOPE
	{
		return false;
	}
	if (!app.appLoaded) {
		app.loadApp();
	}
});


$(document).on("click",catEdit.target + ' .close', function(e){
	e.preventDefault();
	app.close();
});

$(document).on("click",catEdit.target + ' .left', function(e){
	e.preventDefault();
	app.appLoaded = false;
	window.location.hash = '!/'+app.uri.action+ '/' + '/categoryid/' + $(this).attr('rel');
});

$(document).on("click",catEdit.target + ' .right', function(e){
	e.preventDefault();
	app.appLoaded = false;
	window.location.hash = '!/'+app.uri.action+ '/' + '/categoryid/' + $(this).attr('rel');
});

$(document).on("click",catEdit.target + '  .tabsS a', function(e){//live
	e.preventDefault();
	app.switchTab($(this).attr('rel'));
	window.location.hash = '!/'+$(this).attr('rel') + '/categoryid/' + app.uri.params.categoryid;
});


$(document).on("click",catEdit.target + '  .saveTranslations', function(e){//live
	e.preventDefault();
    var tmp = mcms.formData($(this).parents('div:first').find('.saveData'),{ getData:true});
	db.url = '/ajax/Languages.php?action=saveContentTranslations&module='+mcms.mcmsSettings.module+ '&id=' + $(this).attr('rel');
	db.data = {data : tmp};
	db.trigger = 'translationsSaved';
	$(db).loadStore(db);	
});

$(db).bind('translationsSaved',function(){
   saved(); 
});

$(window).resize(function() {
	$(app.target).center();
});

		try {
			return catEdit;
		} finally {
			catEdit = null;
		}

};




})(jQuery);