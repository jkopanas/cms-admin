head.ready(function(){

		
 $(".tabStrip").kendoTabStrip().css('border','none');
	
	$.each($(".boxItem"),function(index,value) {

		setTimeout(function() {
			$("#"+$(value).data('module')).bind("kendo:skinChange", function(e) {	
	             createChart($(value).data('module'),$(value).data());
	         });
			createChart($(value).data('module'),$(value).data());
		},400);
		
		 });
		 
		 
		 function onSelect(e) {
		
	    	tab=$(".tabStrip").kendoTabStrip().data("kendoTabStrip");
			 width=$(tab.wrapper).width();
			 data = $(e.item).data();
			 
			 
	    }
	
	 function createChart(url,data) {

		    $("#chart-"+data.name+"-"+url).kendoChart({
		    	theme: $(document).data("kendoSkin") || "metro",
		        dataSource: {
		            transport: {
		                read: {            
		                    url:"/ajax/loader.php?file=dashboard/dashboard.php&action="+data.settings.action,
		                    dataType: "json",
		                    data:{
		                    	module: url
		                    }
		                }
		                
		            },
		          //  sort: {
		                //field: "year",
		               // dir: "asc"
		           // }
		        },
		        title: {
		        //    text: "Εισαγωγή άρθρων ανά μήνα"
		        },
		        legend: {
		            position: "top"
		        },
		        seriesDefaults: {
		            type: data.settings.type
		        },
		        series: data.settings.field,
		        categoryAxis: {
		            field: "months",
		            labels: {
		                rotation: -90
		            }
		        },
		        valueAxis: {
		            labels: {
		                format: "{0:N0}"
		            },
		            majorUnit: 10
		        },
		        tooltip: {
		            visible: true,
		            format: "{0:N0}"
		        }
		    });
		}
	 
	 
	

		 function createChartactinact(url) {

			    $("#actinact-chart-"+url).kendoChart({
			    	
			        theme: $(document).data("kendoSkin") || "metro",
			        dataSource: {
			            transport: {
			                read: {               
			                    url:"/ajax/loader.php?file=dashboard/dashboard.php&action=drawactinact",
			                    dataType: "json",
			                    data:{
			                    	module: url
			                    }
			                }
			                
			            }
			        },
			        legend: {
			            position: "top"
			        },
			        seriesDefaults: {
			        	type: "bar"
			        },
			        series:
			        [{
			            field: "active",
			            color: "#309B46",
			            name: $.lang.active
			        }, {
                        field: "inactive",
                        color: "#FD0100",
                        name: $.lang.inactive
                    }
			        ],
			        categoryAxis: {
			            field: "months",
			            labels: {
			                rotation: -90
			            }
			        },
			        valueAxis: {
			            labels: {
			                format: "{0:N0}"
			            },
			            majorUnit: 10
			        },
			        tooltip: {
			            visible: true,
			            format: "{0:N0}"
			        }
			    });
			}
		 
	 
	 
});// end head

