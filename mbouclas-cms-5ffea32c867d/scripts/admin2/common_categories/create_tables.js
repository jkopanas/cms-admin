//var debug = "&start_debug=1&debug_host=10.11.11.65&debug_stop=1";
head.ready(function () {

    var db = new mcms.dataStore({
        method: "POST",
        url: "",
        dataType: "json",
        trigger: "loadComplete"
    });

var module =  $('input[name=current_module_name]').val();

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/ajax/loader.php",
                dataType: "json",
                data: {
                    file: 'items/commonCategoriesTree.php',
                    action: 'Viewtables',
                    module: module
                }
            },
            destroy: {
                url: "/ajax/loader.php",
                dataType: "json",
                data: {
                    file: 'items/commonCategoriesTree.php',
                    action: 'deleteTable'
                }
            },
            update: {
                url: "/ajax/loader.php?file=items/commonCategoriesTree.php&action=updateTable",
                dataType: "json",
                type: "POST"
            },
            create: {
                //url: "",
                //dataType: "jsonp"
            },
            parameterMap: function (data, option) {

                if (option == "destroy") {
                    var str = [];
                    $.each(data, function (index, value) {
                        if (!(value instanceof Object)) {
                            str.push(index + "=" + value);
                        }
                    });
                    str.push("id=" + data.models[0].id);
                    return str.join('&');
                } else {
                    
                    return data;
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { editable: false },
                    table_name: {},
                    title: {},
                    type: { editable: false },
                    active: "active"
                }
            }
        },
        batch: true
    });


    var grid = $("#grid").kendoGrid({
        dataSource: dataSource,
        height: "365px",
        columns: [
     			{ field: "id", title: "#ID", width: "40px" },
     			{ field: "table_name", title:$.lang.table_name },
     			{ field: "title", title:$.lang.title, template: '<a href="commonCategoriesTree.php?module='+module+'&tableID=${id}" >${title}</a>' },
                { field: "active", title: $.lang.availability, width: "80px", editor: categoryDropDownEditor, template: function (data) { return (data.active == "1") ? $.lang.yes : $.lang.no; } },
                { field: "type", title: $.lang.type, width: "60px" },
                { command: ["edit", "destroy"], title: $.lang.actions, width: "190px"}],
        editable: "inline"
    }).data('kendoGrid');


    function categoryDropDownEditor(container, options) {
        
        $('<input data-text-field="active" data-value-field="value" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: [{ id: 1, active: $.lang.yes, value: 1 }, { id: 2, active: $.lang.no, value: 0}]
                        });

    }


    $(db).bind('loadComplete', function () {

        records = db.records;
        grid.dataSource.data(records);
        grid.dataSource.read();
        $('#NewTables')[0].reset();


    });
    
    
    var validatable = $("#NewTables").kendoValidator().data("kendoValidator");

    $(".button").live('click', function (e) {
   	
        e.preventDefault();
        searchForm = $("#NewTables");

        db.url = "/ajax/loader.php?file=items/commonCategoriesTree.php&action=AddTable&module="+module;
        db.trigger = 'loadComplete';
        db.data = {
            'data': new mcms.formData(searchForm.find('.table'), {
                'ReturnType': 'array'
            })
        };

        if (validatable.validate()) {
        		$(db).loadStore(db);
        }
    });



$('.listContainer').hover(
  function () {
      $(this).css("border-color", "red")
  },
  function () {
      $(this).css("border-color", "black")
  }
);

//END FUNCTION

});     //END Head
 
	