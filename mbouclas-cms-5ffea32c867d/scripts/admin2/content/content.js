head.ready(function(){
	
	
     var args =[];
    
    
     $(".grid_posted").each(function(index,value) {
     	if ( $(value).data('grid') == "gridview" ) {
     		var str = $(value).attr('name') + "=" + $(value).val();		
     		args.push(str);  
     	}
     });
     
		var str = "&"+args.join('&');
		var module = ($('input[name=current_module_name]').val()!='') ? $('input[name=current_module_name]').val() : 'content';
    	var url = "/ajax/loader.php?file="+$('input[name=url]').val()+"&module="+module+str;
	
    	
    	//var filter=  { field: "module", operator: "eq", value: 'content' };
    	var filter = {};
    	var griddatasource = { 
    			url: url,
    			data: mcms.formData('.modules',{ getData:false}),
    			root: "results" ,
    			filter: filter
    		};
    		
  		var extraColumns = { 
  				filterable: false,
  				command: "",
  				order: 1000,
  				width: "10%"
  		};
  		
		var grid ='';

    	grid = $("#gridview").kgrid({ hasFilter: 16, toolbar: $("#template-toolbar-PagesSearch").html() , dataSource: griddatasource, extra: extraColumns, kendo:{height: 300,sortable:true } }).data('grid');
    	grid.init();
	});
