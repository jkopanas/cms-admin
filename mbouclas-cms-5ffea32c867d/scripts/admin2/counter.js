(function( $ ){  
$.fn.charCounter = function(options ) {  

	var cH = function(element, options) {
		var tbSMS = element;
	    var settings = $.extend( {
			'lblUni' : false,
			'lblRemaining' : '',
			'lblMessages' : '',
			'outputCallback' : '',
			'gsm7Chars' : '@£$¥èéùìòÇØøÅå_^{}\[~]|€ÆæßÉ!#¤%&()*+,-.,/0123456789:;<=>?ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà',
			'msgUnicode' : '(UNICODE)',
			'charset' : 'utf-8'
	    }, options);
    	

	    this.messageChanged = function()
	    {
	    	var charNum = tbSMS.value.length;
			var countDouble = 0;
			var flagUnicode = false;
			var isHex = 0;
			var maxLenght = 160;
			var charLess = 7;
			var doubleByteChars = ',12,94,123,125,92,91,126,93,124,8364,';
			var oneByteChars = ',916,934,915,923,937,928,936,931,920,926,';
			var chkUni = 0;
			
			for (i = 0; i < charNum; i++) {
			    if ((tbSMS.value.charCodeAt(i) != 13) && (!(isNaN(tbSMS.value.charCodeAt(i))))) {
			        if  (settings.charset == 'windows-1253') {
			            if (((tbSMS.value.charCodeAt(i) > 255)) && (tbSMS.value.charCodeAt(i) != 8364) && (oneByteChars.indexOf(',' + tbSMS.value.charCodeAt(i) + ',') < 0)) {
			                countDouble++;
			            }
			        }
			        else if (isUnicode(tbSMS.value.charAt(i), tbSMS.value.charCodeAt(i))) {
			                countDouble++;
			                chkUni = true;
			                
			                flagUnicode = true;
			        }
			                       
			        if ((doubleByteChars.indexOf(',' + tbSMS.value.charCodeAt(i) + ',') > 0) && ((countDouble = 0) || (!chkUni))) {
			            charNum++;
			        }
			    }
		}

		if (!flagUnicode) {
		    chkUni = false;
		}
		
		if (chkUni) {
		    //chkUni = true;
		    if (isHex != 1) {
		        maxLenght = 70;
		        charLess = 3;
		    }
		}
		
		var lblCharNo = settings.lblRemaining;
		var lblMsgNo = settings.lblMessages;

		if (isHex == 1) charNum /= 2;
			var brSms
			if (charNum > maxLenght) {
			    brSms = Math.floor(((charNum - 1) / (maxLenght - charLess)) + 1);
		        } else {
		            brSms = Math.floor(((charNum - 1) / (maxLenght)) + 1);
		        }
		
		        if (charNum > 1080)
		        {
		            tbSMS.value = tbSMS.value.substring(0, 1080);
		            charNum = tbSMS.value.length;
		        }
		        callbackHandler(settings.outputCallback,flagUnicode,charNum,brSms,tbSMS.value);

		        
	    }//END MESSAGE CHANGED
	    
	    this.checkForUnicode = function(){
	    	
	    	callbackHandler(settings.outputCallback,settings.msgUnicode);
	    	return flagUnicode;
	    }
	    
	    
	    var isUnicode = function(ch, chCode) {
		    var gsm7chars = settings.gsm7Chars;
		    var otherGSM7charCodes = ',10,12,13,32,34,39,';
		    var result = false;
		    if (gsm7chars.indexOf(ch) == -1) {
			    if (otherGSM7charCodes.indexOf("," + chCode + ",") == -1) result = true;
		    }

		   return result;
	    }//END FUNCTION
	    
	    var callbackHandler = function(func)
		{
			if (this[func])
			{
				window[func].apply(this, Array.prototype.slice.call(arguments, 1));
			}
			else { console.log(func + ' is not registered'); }
		}
	    
	};//END CLASS
	
    return this.each(function() {   
 
	    var element = $(this);
           // Return early if this element already has a plugin instance
       if (element.data($(this).attr('id'))) return;
           
		var c = new cH(this,options);	    
		element.data($(this).attr('id'), c);  
    });

  };

})( jQuery );