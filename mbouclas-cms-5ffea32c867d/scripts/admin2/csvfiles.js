head.js("../scripts/admin2/lib/datatables/jquery.dataTables.min.js");
head.js("../scripts/admin2/lib/datatables/dataTables.plugins.js");					
head.js("../scripts/admin2/lib/fancybox/jquery.easing-1.3.pack.js");
head.js("../scripts/admin2/lib/gritter/jquery.gritter.min.js");
head.js("../scripts/admin2/lib/fancybox/jquery.fancybox-1.3.4.pack.js");

head.js("../scripts/admin2/lib/jquery-ui/jquery-ui-1.8.15.custom.min.js");
head.js("../scripts/admin2/upload/jquery.ui.widget.js");
head.js("../scripts/admin2/upload/jquery.iframe-transport.js");
head.js("../scripts/admin2/upload/jquery.fileupload.js");


head.ready(function(){
$(".tabsS").flowtabs("div.tabs_content > div");

	$("#filterReuslts").fancybox({
				'transitionIn'	: 'fade',
				'scrolling'		: 'no',
				'titleShow'		: false,
				'overlayOpacity'	: '0',
				'hideOnOverlayClick': false,
				'onClosed'		: function() {
					$("#login_error").hide();
				}
			});
			
			 $('.quickEdit').live('click', function(e){
				  e.preventDefault();
				  var me = $(this);
				 $.fancybox({
					'transitionIn'	: 'fade',
					'scrolling'		: 'no',
					'titleShow'		: false,
					'overlayOpacity'	: '0',
					'hideOnOverlayClick': false,
					'type'				:'ajax',
					'href'				: me.attr('href')
				});
				 
			 });
			 
			   $('#fileupload').fileupload({
				    dataType: 'json',
				    url: '/ajax/uploadcsv.php',
				    done: function (e, data) {
				    },
				    complete: function (data) {
				        var text = data.responseText;
				        var p=text.split("]");
				    	 $('#output').html(p[1]);
				    	 duplicate();
				    }
				});


					dashBoxes = new $.dataStore({
					    method: "POST",
					    url:"/ajax/uploadcsv.php",
//					    dataType: "json",
						trigger:"loadComplete"
					});
					
					

					$(dashBoxes).bind('loadComplete',function(){
						duplicate();			
						records = dashBoxes.records;
						$('#outputimport').html(records);	
					});
					

					$('#filerun').live('click', function() {
						dashBoxes.data = { file:$("#filename").val() };
						$(dashBoxes).loadStore(dashBoxes);
					});
					
					
					dashBox = new $.dataStore({
					    method: "POST",
					    url:"/ajax/changeusers.php",
						trigger:"loadCompletes"
					});
					

					$(dashBox).bind('loadCompletes',function(){
						duplicate();			
					});
					
					$('.delete').live('click', function() {
						dashBox.data = { id:$(this).get(0).id };
						$(dashBox).loadStore(dashBox);
					});
					
					$('.update').live('click', function() {
						var id=$(this).get(0).id;
						dashBox.data = { id:id,email:$("#import_"+id).val() };
						$(dashBox).loadStore(dashBox);
					});
			 


	$('.chSel_all').live('click', function() {
		$(this).closest('table').find('input.check').attr('checked', this.checked);
	});
	
	
	$.fn.dataTableExt.oApi.fnMultiFilter = function( oSettings, oData ) {
var extraFilters = {};
var tmp = new Array;
    for ( var key in oData )
    {
        if ( oData.hasOwnProperty(key) )
        {
            for ( var i=0, iLen=oSettings.aoColumns.length ; i<iLen ; i++ )
            {
                if( oSettings.aoColumns[i].mDataProp == key )
                {
                    /* Add single column filter */
                    oSettings.aoPreSearchCols[ i ].sSearch = oData[key];
					oSettings.aoPreSearchCols[ i ].k = key;
                    break;
                } else {
					extraFilters[key] = oData[key]  ;
				 }
            }
        }
    }

	$.each(extraFilters, function(index, value) { 
	if (value)
	{
		tmp.push(index + ':::' + value);
		oData[index] = value;
	}
	});

	oSettings.aoPreSearchCols[0]['sSearch'] = tmp.join('###');

    this.oApi._fnDraw( oSettings );
};

	
function duplicate() {	
	var oTable = $('#data_table').dataTable({
	"bFilter": true,
	"bDestroy": true,
	"bStateSave": false,
	"bProcessing": true,
	"bServerSide": true,
	"bAutoWidth": false,
	"sAjaxSource": "../ajax/duplicatecsvdata.php",
	 "aoColumns": [
		{ "mDataProp": "checkID" },
		{ "mDataProp": "id" },
		{ "mDataProp": "email" }
 ],
	"aoColumnDefs": [ 
	{
	"fnRender": function ( oObj ) {
	oObj.aData.origID = oObj.aData.id;
	return '<input type="checkbox" value="'+oObj.aData.ID+'" class="check" name="id" />';
	},
	"bSortable": false,
	"sWidth": "1%",
	"aTargets": [ 0 ]
	},
	{
	"bSortable": false,
	"sWidth": "10%",
	"aTargets": [1]
	},	
	{
	"fnRender": function ( oObj ) {
	var ret = '<input type="text" class="import_' + oObj.aData.origID +'" id="import_' + oObj.aData.origID +'" value="'+oObj.aData.email+'" />';
		  ret += '<a href="#" id="'+ oObj.aData.origID+'" class="delete">delete</a>&nbsp;&nbsp;';
		  ret += '<a href="#" id="'+ oObj.aData.origID+'" class="update">update</a>';
	return (ret) ? ret : '';
	},
	"sWidth": "90%",
	"aTargets": [ 2]
	}
	]
	});

}

	
	$(".titleCol").live({
	mouseenter: function() { 
		$(this).find('.row-actions').removeClass('hidden');
	},
	mouseleave: function () {
		$(this).find('.row-actions').addClass('hidden');
	}
});
	

$('#searchTable').click( function(e) { 
//oTable.aoData.push({ "name": "dateStart", "value": 'sdsd' });
e.preventDefault();
var a = {  };
var b;
var dates = {};
$(this).closest('form').find('.filter').each(function(index) {
	
	b = $(this).attr('name').split('filter-');

	if (b[1] == 'date_from' && $(this).val() && $(this).val() !='undefined')
	{
		dates.from = 'from#@#'+$(this).val();
	}
	if (b[1] == 'date_to' && $(this).val() && $(this).val() !='undefined')
	{
		dates.to = 'to#@#'+$(this).val();
	}
	a[b[1]] = $(this).val();
});

if (dates.from && dates.to)
{
	a.date_added = dates.join('###');
}
else { 
a.date_added = (dates.from) ? dates.from : dates.to;
}


 oTable.fnMultiFilter( a );
} );


});//END JQ