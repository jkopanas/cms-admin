(function ($) {
	
	$.dataStore = function( ds ) {
	   
		$.fn.loadStore = function(ds){
			$.ajax({
				type: ds.method,
				url: ds.url,
				data: ds.data,
				dataType: ds.dataType,
				cache:false,
                async:true,
                timeout:ds.timeout?ds.timeout:10000,				
				queue: "autocomplete",
				
				contentType:'application/x-www-form-urlencoded;charset=utf-8',
				accepts: {
					xml: "application/xml, text/xml",
					json: "application/json, text/json",
					_default: "*/*"
				},
				beforeSend:function(){
					loadStatus = true;
				},
				success: function(data) {
					loadStatus = false;

					if(data)
					{ds.records=data;}
					$(ds).trigger(ds.trigger);
				},				
				error: function()
				{					
					loadStatus = false;			
					$(ds).trigger('loadError');
					
				}
			});//END AJAX
		};//END LOADSTORE

		try {
			return ds;
		} finally {
			ds = null;
		}
		
	}
}
)(jQuery);