$(document).ready(function(){
	var statusCodes = {};
$("#contentGrid").kendoGrid({
	height: 250,
	sortable: true,
	pageable: true,
    filterable:{
    				messages: {
					info: "Custom header text:", // sets the text on top of the filter menu
					filter: "CustomFilter", // sets the text for the "Filter" button
					clear: "CustomClear", // sets the text for the "Clear" button
					
					// when filtering boolean numbers
					isTrue: "custom is true", // sets the text for "isTrue" radio button
					isFalse: "custom is false" // sets the text for "isFalse" radio button
				},
				operators : {
/*					string : {
                    eq: 'EQ',
                    neq: 'NEQ',
                    startswith: "Starts with",
                    contains: "Contains123",
                    endswith: "Ends with"
					}*/
				}
					
    },
    
  columns:[
      {
          field: "id",
          title: "#ID",
          template : '<a href="\\#">${ id }</a>'
      },
      {
      	field:'email',
      	title:'email'
      },
      {
      	field:'status',
      	title:'Status',
      	template : function(e) { return statusCodes[e.status];}
      }
      ],
       schema: {
    model: {
    fields: {
        id: { type: "number" },
        email: { type: "string" },
    }
}
 },
dataSource: {
	pageSize: 20,
     serverPaging: true,
    serverFiltering: true,
    serverSorting: true,

	    error: function(e) {
        // handle event
        console.log(e)
    },
    schema : {
    data : function(data) {
    	statusCodes = data.status_codes;
     	return data.orders;
     },
     total: function (data) { 
		return data.list.total
    }	
    },
  transport: {
read: {
    // the remote service url
    url: "/ajax/eshop.php?action=search",

    dataType: "json",
	type : 'post',
    data: {
        secondaryData: {orderby : 'eshop_orders.id',results_per_page:20,way:'desc'},'return':'json'
    }
}
}	
}
});

	
dashBoxes = new $.dataStore({
    method: "POST",
    url:"../ajax/ajaxRequests.php?action=loadContent",
//    dataType: "json",
	trigger:"loadComplete"
});


$(dashBoxes).bind('loadComplete',function(){
	records = dashBoxes.records;
    $('#' + dashBoxes.data.target +' .box_c_content').html(records)

});
	
$('.dashBox').each(function(index) {

dashBoxes.data = {};
dashBoxes.data = $(this).data();
dashBoxes.data.target = $(this).attr('id');
dashBoxes.data.sortableItem = '';//BUG FIX FOR FF
$(dashBoxes).loadStore(dashBoxes);

});//END LOAD BOXES


});//END JQ