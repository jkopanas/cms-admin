(function ($) {
$.featuredItems = function( options ) {	
	var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
	    clearTimeout (timer);
	    timer = setTimeout(callback, ms);
	  };
	})();
	
	var templates = {},control = {},cache={};
	var opts = {
		target:'',
		results_per_page:50,
		itemsTarget:'ol#featuredItems',
		url: {
			'add' : '/ajax/loader.php?file=items/itemsCategoriesNew.php&action=addFeatured&module=' + mcms.settings.module,
			'read': '/ajax/loader.php?file=items/itemsCategoriesNew.php',
			'delete': '/ajax/loader.php?file=items/itemsCategoriesNew.php&action=deleteFeatured&module=' + mcms.settings.module,
			'update': '/ajax/loader.php?file=items/itemsCategoriesNew.php&action=updateFeaturedOrder&module=' + mcms.settings.module
		},
		templates:'/skin/common/featuredItems.tpl',
		sortItems:true,
		sortableDefualts: {
			handle: '.dragHandle',
			update:function(e,ui) {//CHANGE THIS TO OVERRIDE DEFAULT FUNCTIONALITY, LIKE WHAT HAPPENS ON DROP
				control.updateOrder(e,ui);
			}
		}
	};
	
	var settings = $.extend(opts,options);
	
	$.ajax({
	  url: settings.templates,
	  async:false,
	  success: function(data){
	  	var o = $(data).find('script');
	  	var p = o.length;
	
	  	for (p;p--;) {
	  		templates[$(o[p]).attr('id')] = kendo.template($(o[p]).html());
	  	}
	  },
	  cache:false,
	  dataType: 'html'
	});
	

	var url = settings.url;
	var dbDefaults = {    
		method: "POST",
	    dataType: "json",
		trigger:"loadComplete"
	};
	
	var appDB = new mcms.dataStore(dbDefaults);
	
	var control = {
		settings: settings,
		target: {},
	 	_trigger: function( type, data ) {
	 		data = data || {};
	 		if ($.isFunction(this.settings[type])){
	 			eval(this.settings[type]).apply(this, Array.prototype.slice.call(arguments, 1));
	 		}
	 	},
		init: function(){
			var me = this;
			if (settings.allCategories === null) {
				this.getCategories();
			}
			this.allCategories = (typeof settings.allCategories === 'undefined') ? this.getCategories() : settings.allCategories;
			
			this.allPages = (typeof settings.allPages === 'undefined') ? this.getPages() : settings.allPages;
			this.items = (typeof settings.items === 'undefined') ? this.getItems() : settings.items;
			this.target = this.settings.target;
			this.templates = $.extend(settings.templates,templates);//YOU CAN REPLACE ALL THE TEMPLATES THROUGH THE SETTINGS
			this.itemsFlat = {};
			if (typeof this.items === 'object' && this.items !== null){
				
				for (l=this.items.length;l--;){
					if (typeof this.itemsFlat[this.items[l]['type']] === 'undefined') {
						this.itemsFlat[this.items[l]['type']] = new Array;
					}
					this.itemsFlat[this.items[l]['type']].push(this.items[l]['itemid']);
				}
			}
			
			this.body = templates['body'](this);
			this.target.append(this.body);
			if (this.settings.sortItems) {
				this.sortable = $(this.settings.itemsTarget).sortable(this.settings.sortableDefualts);
			}
			this._trigger( "onAppLoaded",this,appDB );
		},
		getCategories: function() {//Gets categories for this module
			
		},
		getPages: function() {//Gets content items for this module
			
		},
		getItems: function(type) {//Gets existing items. Type can be featured - links - menus etc
			
		},
		renderRecursive: function(obj,level,tpl) {//tpl should be a kendo template
			var render ='';
			
			level = (level == null) ? 0 : level;
			for (i in obj) {
				obj[i]['level'] = level;
				render += (typeof tpl === 'object') ? tpl.template({d:obj[i],t:tpl}) : tpl.template(obj[i]);//this is used to do advanced template operations like check with existing elements
				if (typeof obj[i]['subs'] == 'object') {
					render += tpl.encStart + this.renderRecursive(obj[i]['subs'],level+1,tpl) + tpl.encEnd;
				}
			}
			return render;
		},
		renderPages: function(obj) {
			return this.templates['pages'](obj);
		},
		renderItemsList: function(obj,level) {
			if (obj === null) {
				return '';
			}
			if (this.settings.type == 'recursive') {
				return renderRecursive(obj,0,this.settings.itemsTemplate);
			}
			else {
				return this.templates['items'](obj);
			}
		},
		filterPages: function(obj) {
			var source = obj.parents('.mAccordion:first').find('.itemsResults');
			if (typeof cache[obj.attr('data-module')] == 'object' && obj.val() in cache[obj.attr('data-module')]) {
				this.drawResults(source,{allPages:cache[obj.attr('data-module')][obj.val()],itemsFlat:this.itemsFlat});
				return;
			}
			
			var me = obj;
			var filter = {};
			var filters = new Array;
			var v = mcms.formData(me.parents('.mAccordion').find('.SearchData'),{ getData:true});
			if (v.substring) {
				filter.filter = {};
				filter['filter']['filters'] = [{field:'q',operator:'contains','logic':'and',value:v.substring['value']}];
			}
			if (v.categoryid) {
				filter.categoryid =  v.categoryid['value'];
				filter.search_in_subcategories =  1;
			}
			appDB.source = source;
			appDB.query = obj.val();
			appDB.module = obj.attr('data-module');
            appDB.trigger = 'loadComplete';
			delay(function(){
				filter.pageSize = control.settings.results_per_page;
				appDB.url = "/ajax/loader.php?file=itemsSearch.php&module=" + mcms.settings.module + "&action=pagesFilter";
				appDB.data = filter;
				$(appDB).loadStore(appDB);
			}, 400 );			
			
		},
		updateOrder: function(e,ui){
			appDB.url = url.update;
			appDB.me = $(this);
			appDB.data = { categoryid :app.settings.currentCategory, items:this.sortable.sortable('toArray') };
			appDB.trigger = 'updateOrder';
			$(appDB).loadStore(appDB);

			this._trigger( "onBeforeUpdateOrder",this,appDB,ui );
		},
		addToList: function(obj){
			this._trigger( "onBeforeAddtoList",this,appDB );
		},
		deleteFromList: function(obj) {
			var r=confirm($.lang.u_sure);

			if (r === true) {
				appDB.url = url.delete;
				appDB.me = $(obj);
				appDB.data = { categoryid :this.settings.currentCategory, itemid:$(obj).parent().attr('rel'),type:$(obj).parent().attr('data-type') };
				appDB.trigger = 'itemsDeleted';
				$('#'+ $(obj).parent().attr('data-type') + '-' + $(obj).parent().attr('rel')).removeClass('hidden');
				$('#'+ $(obj).parent().attr('data-type') + '-' + $(obj).parent().attr('rel')).parents('li:first').removeClass('inUse');
				var p = app.itemsFlat[$(obj).parent().attr('data-type')].length;
				for(p;p--;) {
					if (app.itemsFlat[$(obj).parent().attr('data-type')][p] == parseInt($(obj).parent().attr('rel'))) {
						app.itemsFlat[$(obj).parent().attr('data-type')].splice(p, 1);
					}
				}
				$(appDB).loadStore(appDB);
			}
			this._trigger( "onBeforeDelete",this,appDB );
		},
		switchTabs: function(obj){//ACCORDION
			
		},
		drawResults: function(target,obj) {
			target.empty().append(this.templates['pages'](obj));
		}
		
	};
	
	control.init();
	
	var app = control;	
	
	/* EVENT BINDS */
	
	$('.mAccordion h4').on('click',function(e){//ACCORDION
		e.preventDefault();
		$(this).parents('.mAccordion').find('.sub_section').hide();
		$(this).parents('.micro').find('.sub_section').show();
	});
	
	app.settings.target.find('.quickFilter').on('click',function(e){
		e.preventDefault();
		app.filterPages($(this).parents('.mAccordion').find('.filter'));
	});
	
	app.settings.target.find('.filter').on('keyup',function(e){
		app.filterPages($(this));
	});
	app.settings.target.find('.addItems').on('click',function(e){
		e.preventDefault();
		appDB.url = url.add;
		var tmp = mcms.callInternalMethod('listAttributes',$(this),{prefix:'data-',returnType:'keyVal',stripPrefix:true});
		tmp.lastOrderby = $(app.settings.itemsTarget+':first').children('li').length;
		
		appDB.data = {cat : tmp ,data :mcms.formData($(this).parents('.micro').find('input[type=checkbox]:checked'),{ getData:true})};
		appDB.trigger = 'itemsAdded';
		$(appDB).loadStore(appDB);
	});
    
    app.settings.target.on('click','img.wRemove',function(e){//delegate cause live registers the event on every load
		e.preventDefault();
		app.deleteFromList($(this));
	});
	
	
	//AJAX BINDS
	$(appDB).bind('itemsDeleted',function(){
		app._trigger( "onAfterDelete",this,appDB );
		$(appDB.me).parents('li:first').remove();
		saved();
	});
	
	$(appDB).bind('itemsAdded',function(){
		var r = '',p = new Array;
		for(j in appDB.data.data) {
			var el = $('#'+appDB.data.data[j]['el']);
			var d = {};
			d['itemid'] = appDB.data.data[j]['value'];
			d['item'] = {};
			d['item']['title'] = el.parents('li:first').find('span').text();
			d['type'] = appDB.data.data[j]['type'];
			p.push(d);
            if (typeof app.itemsFlat[d['type']] == 'undefined') {
                app.itemsFlat[d['type']] = new Array;
            }
            app.itemsFlat[d['type']].push(d['itemid']);
			el.parents('li:first').addClass('inUse');
			el.addClass('hidden');
			el.attr('checked',false);//reset
		}
		$(app.settings.itemsTarget).append(app.templates['items'](p));
		if (app.settings.sortItems) {
			app.sortable = $(app.settings.itemsTarget).sortable(app.settings.sortableDefualts);//reinitialize sortables
		}
		saved();
	});
	
	$(appDB).bind('loadComplete',function(){
		var res = appDB.records;
		if (appDB.query.length > 0) {
			if (typeof cache[appDB.module] == 'undefined') {
				cache[appDB.module] = {};
			}
			cache[appDB.module][appDB.query] = res.results;
		}

		app.drawResults(appDB.source,{allPages:res.results,itemsFlat:app.itemsFlat});
	});
	
	return control;
};//END CLASS
	
})(jQuery);	