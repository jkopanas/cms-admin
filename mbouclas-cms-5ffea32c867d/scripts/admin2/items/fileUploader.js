head.ready(function(){
	$(".tabStrip").kendoTabStrip().css('border','none');
var fileTypes = jQuery.parseJSON($('textarea[name=fileTypes]').val());

var fileType = $('#fileType').val();
var allowedFiles; var mediaType;

switch (fileType) {
	case 'thumb' : allowedFiles = fileTypes['images'].extentions; mediaType= fileTypes['images'].type; 
	break;
	case 'image' : allowedFiles = fileTypes['images'].extentions; mediaType= fileTypes['images'].type;
	break;
	case 'video' : allowedFiles = fileTypes['videos'].extentions; mediaType= fileTypes['videos'].type;
	break;
	case 'audio' : allowedFiles = fileTypes['audio'].extentions; mediaType= fileTypes['audio'].type;
	break;
	case 'archive' : allowedFiles = fileTypes['archives'].extentions; mediaType= fileTypes['archives'].type;
	break;
	case 'document' : allowedFiles = fileTypes['documents'].extentions; mediaType= fileTypes['documents'].type;
	break;
}
allowedFiles = allowedFiles.split(';');

var uploadedTemplate = kendo.template($("#new"+fileType+"Template").html());
var imageCategory = $('#imageCategory').val();
	var upload = $(".uploader").kendoUpload( {   
		multiple:true,
		async: {
        saveUrl: '/ajax/loader.php?action=saveImage&file=items/itemsActions.php&module=' + mcms.mcmsSettings.module +'&id='+$('input[name=itemID]').val()+'&media='+mediaType,
//        removeUrl: "removeHandler.php",
        removeField: "fileNames[]",
        autoUpload:false
    },
    select: function(e) {
    	$('.k-upload-files').show();
		var files = e.files;
		$('#newImagePlaceHolder').children().remove();
		$.each(e.files, function(index, value) {
		if (allowedFiles.indexOf(value.extension) == -1) {
            alert("Only .jpg files can be uploaded")
            e.preventDefault();
        }
		});
    	
    },
    success: function(e) {
		    	
    	var x = $(uploadedTemplate(e)).appendTo('#uplaodedFiles');
    	if ($('input[name=callBack]').val()){
    		mcms.callbackHandler($('input[name=callBack]').val(),e);
    	}
    	setTimeout(function() {	$(this.element).parentsUntil('uploadContainer').parent().find('.k-upload-files').empty();$('.k-upload-files').hide(); }, 3000); 
    },
    cancel: function(e) {
    	
    },
    remove: function(e) {
    	
    },
    error: function(e) {
    	console.log(e)
    },
    complete: function(e) {
//    	console.log(e)
    },
    upload: function(e) {
    	e.data = { fileParam:this.element.attr('name'),fileType: this.element.attr('data-fileType'), imageCategory:$('#imageCategory').val(),ImageSet:1 };//Attach to post
    }
	
	}).data("kendoUpload");

});//END HEAD