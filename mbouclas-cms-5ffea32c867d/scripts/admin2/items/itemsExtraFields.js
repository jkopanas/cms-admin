head.ready(function(){
	var itemid = $('input[name=itemID]').val();
	var currentModule = $('input[name=module]').val();

	$('.saveEfields').live('click', function(e) {
		db.url = '/ajax/loader.php?action=saveEfields&file=items/itemsActions.php&id='+itemid+'&module='+currentModule;
		db.trigger = 'efieldsSaved';
		db.data = {data : mcms.formData($('.efields'),{ getData:true}) };	
		$(db).loadStore(db);
	});

	$(db).bind('efieldsSaved',function(){
		saved();		
	});
	
});//END HEAD