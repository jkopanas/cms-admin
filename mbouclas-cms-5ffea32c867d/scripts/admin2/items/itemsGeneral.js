var loadedBoxes = {};
var moreItemsTpl;
head.ready(function(){
$("#menu").kendoMenu();
var uploadWindow = {};
var itemid = $('input[name=itemID]').val();
moreItemsTpl = kendo.template($('#autoCompleteTemplate').html());
var currentModule = $('input[name=module]').val();
var dbDefaults = {    method: "POST",
    url:"/ajax/loader.php",
    dataType: "json",
	trigger:"loadComplete",
	placeHolder:'#mainItem'
};
db = new mcms.dataStore(dbDefaults);

$(window).bind('hashchange', function() {
	if (window.location.hash.indexOf("#!/") == -1)//OUT OF SCOPE
	{
		return false;
	}
	var x = window.location.hash.replace("#!/", "");
	var y = $('a[data-name='+x+']');
	$('.navigator').removeClass('k-state-hover');
	y.addClass('k-state-hover k-active');
	$(db.placeHolder).find('.loadedTemplates').hide();
	if(x in loadedBoxes) {
		$(db.placeHolder).find('#template-'+x).show();
	}
	else { 
		db.url = "/ajax/loader.php?module="+currentModule+'&boxID='+y.attr('data-boxID')+'&id='+$('input[name=itemID]').val()
		db.dataType =  dbDefaults.dataType;
		db.trigger = ($(this).attr('data-trigger')) ? $(this).attr('data-trigger') : 'fetchComplete';
		db.lastRequest = y;
		$(db).loadStore(db);
	}
	
});
	
$(db).bind('fetchComplete',function(){
	records = db.records;
	loadedBoxes[db.lastRequest.attr('data-name')] = db.lastRequest;
	var a = $('<div id="template-'+$(db.lastRequest).attr('data-name')+'" class="loadedTemplates"><img src="/images/ajax-loader.gif" /></div>').appendTo(db.placeHolder);
	$(a).html(records.template);
	if ($('#breadcrumbs').find('.current').length ==0){
		$('#breadcrumbs').append('<li class="current">'+db.lastRequest.text()+'</li>');
	}
	else {
		$('#breadcrumbs').find('.current').text(db.lastRequest.text());
	}
});


	if(window.location.hash.indexOf("#!/") != -1)
	{
		$(window).trigger("hashchange");
	}
	else if (window.location.hash.indexOf("#") == -1)
	{
		window.location.hash = $('.navigator:first').attr('href');//DEFAULT
	}
	
	$('.navigator').live('click', function(e) {
		$(this).parent().addClass('k-state-hover');
	});
	uploadWindow = $("#uploadWindow"),
    undo = $(".uploadWindow").live("click", function(e) {
    	e.preventDefault();
   	var additional = mcms.callInternalMethod('listAttributes',$(this),{prefix:'data-',returnType:'keyVal',stripPrefix:true});
	var addUrl= new Array;
	 $.each(additional,function(k,v){
	 	addUrl.push(k+'='+v);
	 });
if (!uploadWindow.data("kendoWindow")) {
    uploadWindow.kendoWindow({
        width: "800px",
        height:'90%',
        title: $.lang.upload,
        content: "/ajax/loader.php?module=products&file=items/fileUploader.php&id=" + itemid+'&'+addUrl.join('&'),
        close: onClose,
        visible:false,
        modal:true
    });
    
    $(window).resize(function() {
	uploadWindow.attr('height',$(window).height()-10 +'px');
	uploadWindow.data("kendoWindow").center();
});
}
else {
	uploadWindow.data("kendoWindow").refresh("/ajax/loader.php?module=products&file=items/fileUploader.php&id=" + itemid+'&'+addUrl.join('&'));
}
				
                uploadWindow.data("kendoWindow").open();
                uploadWindow.data("kendoWindow").center();
                undo.hide();
});
 
var onClose = function() {
    undo.show();
}
 

if ($('#moreItemsJson').val() && itemid) {//FIRST TIME
	var moreItems = $.parseJSON($('#moreItemsJson').val());
	renderMoreItems(moreItems);
}




});//END HEAD

function renderMoreItems(moreItems) {
	var des ='';
		for (i in moreItems) {
		des += '<li class="k-item">'+moreItemsTpl(moreItems[i])+'</li>';
	}
	
	$('#template ol').append(des);
	$('#template').parents('li').show();
}
