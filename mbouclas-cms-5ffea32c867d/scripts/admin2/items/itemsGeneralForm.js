head.js("../scripts/admin2/tagInput.js");
var cache = {}; var prevState = {};
head.ready(function(){
	
var itemid = $('input[name=itemID]').val();
if($('#quickEdit').length > 0) {
	var qEdit = $('#quickEdit').val();
}

var currentModule = $('input[name=module]').val();
var tagTemplate = kendo.template($('#tagTemplate').html());

var tags = {
	createTags: function(){
		var t = new Array;		
		$('ul#tagsContainer').children('li').each(function(){
				t.push($.trim($(this).text()));
		});
		
		return t;
	},
	removeDuplicates: function(obj){
		//first get the existing ones, if any
		var t = new Array,p = new Array;
		
		t = this.createTags();
		var tmp = obj.length;

		//now loop the new ones and return uniques
		for (j=0;tmp>j;j++) {
			if (t.indexOf(obj[j]) == -1 && p.indexOf(obj[j]) == -1) {//unique  all over the place
				p.push(obj[j]);
			}
		}
		
		return p;
	}
}

var itemTags = $.parseJSON($('textarea[name=itemTags]').val());

if ( itemTags != null) {
	
	var l = '',p= new Array;
	for (j=0;itemTags.length > j;j++) {
		l += tagTemplate({tag:itemTags[j].tag,orderby:itemTags[j].orderby});
		p.push(itemTags[j].tag);
	}
	$('ul#tagsContainer').append(l);
	$('input[name=tags]').val(tags.createTags());
}
$('#tags').AutoComplete({
delay:500,
url:'/ajax/tags.php?limit=15&module='+currentModule,
minChars:2,
onPressEnter: function(e,v){
var tmp = $(e).val().split(',');

var l ='';
var order  = $('ul#tagsContainer').children('li').length;
var q = tags.removeDuplicates(tmp);
var p = q.length;
for (j=0;p>j;j++){
	if (q[j].length > 0){
		l += tagTemplate({tag:q[j],orderby:order++});
	}
}
$('ul#tagsContainer').append(l);
$('input[name=tags]').val(tags.createTags());
$(e).val('');
this.hideResults();
},
onSelect:function(e,v){
	console.log($(e).val().split(','));
}
});

if (typeof db == 'undefined') {
	var dbDefaults = {    method: "POST",
    url:"/ajax/loader.php",
    dataType: "json",
	trigger:"loadComplete",
	placeHolder:'#mainItem'
};
db = new mcms.dataStore(dbDefaults);
}
if (itemid) {
//original state. We need it to see if any dependencies have changed so to update the app
prevState = mcms.formData($('#itemGeneralForm').find('.toSave'),{ getData:true});
}
	$(".mSelect").chosen();
	$(window).resize(function() {
		$(".mSelect").attr('width','95%');
	});
	var newThumbTemplate = kendo.template($("#newThumbTemplate").html());

	var validatable = $(".toValidate").kendoValidator().data("kendoValidator");
	
$('input[name=title]').keyup(function(){
	var txt ='';
	var texto = $(this).val();
	for (var i=0; i < texto.length; i++) {
     txt+=mcms.convertchar(texto.charAt(i))
	}

    $('input[name=permalink]').val(txt);


});
$(db).bind('itemSaved',function(){
	records = db.records;
	$('input[name=itemID]').val(records.id);
	if (records.mode == 'add') { 
		var patern = /#!\/[a-zA-Z]+/i;
		var match = patern.exec(document.location.href);
		document.location.href.replace(match,'&id='+records.id+''+match)
		window.history.pushState("string", document.title, window.location.pathname + window.location.search + '&id='+records.id+''+match);
		renderMoreItems(records.moreItems);
		$('.navigator').parent().show();
		prevState = db.data;//reset
		$('#itemTitle').find('.itemid').html(records.id);
		$('#itemTitle').find('.title').html(records.item.title);
		 $('#itemThumb').show();
		 
	}
	if (prevState.categoryid.value != db.data.categoryid.value) {//RESET category based tabs
		//kill all categorybased tabs
		if (typeof loadedBoxes != 'undefined') {
			delete loadedBoxes.extraFields;
			$('#template-extraFields').remove();
		}
		prevState = db.data;//reset
	}
	saved();
	
	if (qEdit) {//Will trigger something if came from popup
		$(window).trigger(qEdit,records.item);
	}
	
});

$('#itemGeneralForm').submit(function(e){
	 e.preventDefault();
	  if (validatable.validate()) {
			var a = mcms.formData($(this).find('.toSave'),{ getData:true});
			
			db.url = '/ajax/loader.php?action=saveItem&file=items/itemsActions.php&returnItem=1&module=' + mcms.mcmsSettings.module +'&id='+$('input[name=itemID]').val();
			if (qEdit) {
				db.url = db.url + '&returnItem=1';
			}
			db.data = a;
			db.trigger = 'itemSaved';
			db.dataType = 'json';
			
			$(db).loadStore(db);
	  }
});

if (qEdit) {
	
		uploadWindow = $("#uploadWindow"),
    uploadWin = $(".uploadWindow").bind("click", function(e) {

    	e.preventDefault();
    	
   	var additional = mcms.callInternalMethod('listAttributes',$(this),{prefix:'data-',returnType:'keyVal',stripPrefix:true});
	var addUrl= new Array;
	 $.each(additional,function(k,v){
	 	addUrl.push(k+'='+v);
	 });
if (!uploadWindow.data("kendoWindow")) {
    uploadWindow.kendoWindow({
        width: "800px",
        height:'90%',
        title: $.lang.upload,
        content: "/ajax/loader.php?module=products&file=items/fileUploader.php&id=" + itemid+'&'+addUrl.join('&'),
        close: onClose,
        visible:false,
        modal:true
    });
    
    $(window).resize(function() {
	uploadWindow.attr('height',$(window).height()-10 +'px');
	uploadWindow.data("kendoWindow").center();
});
}
else {
	uploadWindow.data("kendoWindow").refresh("/ajax/loader.php?module=products&file=items/fileUploader.php&id=" + itemid+'&'+addUrl.join('&'));
}
				
                uploadWindow.data("kendoWindow").open();
                uploadWindow.data("kendoWindow").center();
                uploadWin.hide();

});
 
var onClose = function() {
    uploadWin.show();
}
}


$(document).on("click",'.deleteTag', function(e){//live
	e.preventDefault();
	$(this).parent().remove();
	$('input[name=tags]').val(tags.createTags());
});

});//END HEAD

function autoSave()
{
	
	formData = mcms.FormData($('.post'),{'ReturnType':'array'});//IDS AND STUFF
		if (!mcms.objectEquals(cache,formData))
		{
			cache = formData;
			pm.url="../ajax/promotions.php?action=autosave";
			pm.data = new $.FormData($('.secondary'),{'ReturnType':'array'});//IDS AND STUFF
			pm.data.data = formData;//ACTUAL FORM
			pm.trigger = 'savePromotion';
			pm.dataType = 'json';
			$(pm).loadStore(pm);
		}
	
		setTimeout("autoSave()", 20000); // Autosaves every 20 sec.
}

function quick_thumb_uploaded(e) {
	var newImageTemplate = kendo.template($("#newThumbTemplate").html());
    var scriptData = { src: e.response.uploadedFile.thumb.CleanDir+'/'+ e.response.uploadedFile.thumb.name, id: e.response.uploadedFile.thumb.imageid };
    $('#imagePlaceHolder').empty().append(newImageTemplate(scriptData));
   
	
}
