var uploadModel = {};
var sortable;
head.ready(function(){
/*$(".tabStrip").kendoTabStrip();*/
$(".tabStrip").kendoTabStrip().css('border','none');

var data = jQuery.parseJSON($("#data").val());//GET ALL THE DATA
var exisingThumb = kendo.template($("#existingThumb").html());
var existingImages = kendo.template($("#existingImages").html());
var existingDocument = kendo.template($("#existingDocument").html());
var existingVideo = kendo.template($("#existingVideo").html());
if (data.images && data.images.thumbs) {
$('#t-thumbs tbody.content').empty().append(exisingThumb(data.images.thumbs));
}

var tpl = '';
if (data.images) {
	$.each(data.images,function(key,val){
		if (key != 'thumbs' && val.length > 0) {
			
			$.each(val,function(k,v){
				tpl += existingImages(v);
			});
			$('#t-'+key).find('tbody.content').append(tpl);	
			$('#t-'+key+" tbody.content").sortable({handle:'td.dragHandle',stop: function(event, ui) { uploadModel.sort(ui) }});
		}
		tpl = '';
	}); 
}//END IMAGES
tpl = '';

if (data.documents) {
	$.each(data.documents,function(k,v){
		
				tpl += existingDocument(v);
	}); 
	
	$('#t-documents tbody.content').append(tpl).sortable({handle:'td.dragHandle',stop: function(event, ui) { uploadModel.sort(ui) }});		
}

if (data.videos) {
	$.each(data.videos,function(k,v){
		
				tpl += existingVideo(v);
	}); 
	
	$('#t-videos tbody.content').append(tpl).sortable({handle:'td.dragHandle',stop: function(event, ui) { uploadModel.sort(ui) }});		
}
var defaultValues = {    method: "POST",
    url:"/ajax/loader.php?file=items/itemsActions.php&module="+$('input[name=module]').val(),
    dataType: "json",
	trigger:"loadComplete"
};
connector = new mcms.dataStore(defaultValues);

$(connector).bind('itemsEnabled',function(){
	var message = (uploadModel.mode == 'enable') ? '<span class="notification ok_bg">'+$.lang.yes+'</span>' : '<span class="notification error_bg">'+$.lang.no+'</span>';
//	uploadModel.tmpTarget.find('.avail').text(message);
	$.each(uploadModel.affectedRows,function(k,v){
		v.find('.avail').html(message);
	});
	
});

$(connector).bind('itemsDeleted',function(){
//	uploadModel.tmpTarget.find('.avail').text(message);
	$.each(uploadModel.affectedRows,function(k,v){
		v.remove();
	});
	
});

$(connector).bind('thumbDeleted',function(){
$('#t-thumbs tbody.content').empty();
});


$(connector).bind('itemsSorted',function(){
//	console.log(connector.records);
});




uploadModel = kendo.observable({
tmpTarget:{},
mode : '',
affectedRows :{},
media:'images',
callAjax : function(options) {
	connector.url = options.url;
	connector.dataType = options.dataType;
	connector.trigger = options.trigger;
	connector.data = options.data;
	
	$(connector).loadStore(connector);
},
callBack : function(func) {
	if (typeof eval(this[func]) == 'function')
	{
		return eval(this[func]).apply(this, Array.prototype.slice.call(arguments, 1));
	}
	else { console.log(func + ' is not registered'); }
},
doActions : function(e) {
	e.preventDefault();
	var func = $(e.currentTarget).attr('data-method');
	var table = $(e.currentTarget).parentsUntil('table').parent();
	var el = table.find('tbody.content');
	this.tmpTarget = el;
	this.media = table.attr('data-media');
	this.callBack(func,el);
},
deleteThumb : function(el) {
	var r=confirm($.lang.u_sure);
	if (r==true)
    {
		var toSave = new Array;
		toSave.push($('#itemThumb').attr('data-id'))
		var t = new Array;
		this.affectedRows =  t;
		this.mode = 'enable';
		this.callAjax({url:defaultValues.url+'&action=MediaActions&mode=deleteMedia&media='+this.media,trigger:'thumbDeleted',dataType:defaultValues.dataType,data:{'ids':toSave}});
    }
},
updateItems : function(el) {
	this.callAjax({url:defaultValues.url+'&action=MediaActions&mode=updateMedia&media='+this.media,trigger:'itemsUpdated',dataType:defaultValues.dataType,data:mcms.formData(el.find('.toSave'),{ getData:false})});
},
deleteItems : function(el) {
	var r=confirm($.lang.u_sure);
	if (r==true)
    {
		var toSave = new Array;
		var t = new Array;
		el.find('.check-values:checked').each(function(k,v){
			toSave.push($(this).val());
			t.push($(this).parentsUntil('TR').parent());
		});
		this.affectedRows =  t;
		this.mode = 'enable';
		this.callAjax({url:defaultValues.url+'&action=MediaActions&mode=deleteMedia&media='+this.media,trigger:'itemsDeleted',dataType:defaultValues.dataType,data:{'ids':toSave}});
    }
},
updateItems : function(el) {
	this.callAjax({url:defaultValues.url+'&action=MediaActions&mode=updateMedia&media='+this.media,trigger:'itemsUpdated',dataType:defaultValues.dataType,data:mcms.formData(el.find('.toSave'),{ getData:false})});
},
enableItems : function(el) {
	var toSave = new Array;
	var t = new Array;
	el.find('.check-values:checked').each(function(k,v){
		toSave.push($(this).val());
		t.push($(this).parentsUntil('TR').parent());
	});
	this.affectedRows =  t;
	this.mode = 'enable';
	this.callAjax({url:defaultValues.url+'&action=MediaActions&mode=enableMedia&media='+this.media,trigger:'itemsEnabled',dataType:defaultValues.dataType,data:{'ids':toSave}});
},
disableItems : function(el) {
	var toSave = new Array;
	var t = new Array;
	el.find('.check-values:checked').each(function(k,v){
		toSave.push($(this).val());
		t.push($(this).parentsUntil('TR').parent());
	});
	this.mode = 'disable';
	this.affectedRows = t;
	this.callAjax({url:defaultValues.url+'&action=MediaActions&mode=disableMedia&media='+this.media,trigger:'itemsEnabled',dataType:defaultValues.dataType,data:{'ids':toSave}});
},
checkOne : function(e) {
	var checkbox = $(e.currentTarget);
	var table = $(e.currentTarget).parentsUntil('table').parent();
	var el = table.find('tbody.content');
	var row = checkbox.parentsUntil('TR').parent();
	if (checkbox.prop('checked') == true)
	{
		table.find('.messages').html(el.find('.check-values:checked').length);
		row.addClass('row_selected');
	}
	else 
	{
		table.find('.messages').html(el.find('.check-values:checked').length);
		row.removeClass('row_selected');
	}
},
checkAll : function(e) {
	var checkbox = $(e.currentTarget);
	var table = $(e.currentTarget).parentsUntil('table').parent();
	var el = table.find('tbody.content');
	
	
	if (checkbox.prop('checked') == true)
	{
		el.find('.check-values').attr('checked',true);
		el.find('.check-values').parentsUntil('TR').parent().addClass('row_selected');
		table.find('.messages').html(el.find('.check-values:checked').length);

	}
	else 
	{
		el.find('.check-values').attr('checked',false);
		el.find('.check-values').parentsUntil('TR').parent().removeClass('row_selected');
		table.find('.messages').html(el.find('.check-values:checked').length);
	}
},
sort: function(ui) {
	var table = ui.item.parentsUntil('table').parent();
	var media = table.attr('data-media');
	this.callAjax({url:defaultValues.url+'&action=MediaActions&mode=sortOrder&media='+media,trigger:'itemsSorted',dataType:defaultValues.dataType,data:{'ids':$(table).find('tbody.content').sortable('toArray')}});
}
});


kendo.bind($(".tab"), uploadModel);

});//END HEAD


function thumb_uploaded(e) {
	var newThumb = kendo.template($("#newThumb").html());;
	var x = newThumb(e.response.uploadedFile);
	$('#t-thumbs tbody.content').empty().append(x);
	kendo.bind($(".tab"), uploadModel);
}

function document_uploaded(e) {
	var newDoc = kendo.template($("#newDocument").html());
	var x = newDoc(e.response.uploadedFile);
	$('#t-documents tbody.content').append(x).sortable({handle:'td.dragHandle',stop: function(event, ui) { uploadModel.sort(ui) }});
	kendo.bind($(".tab"), uploadModel);
}

function image_uploaded(e) {
	var newImages = kendo.template($("#newImages").html());
	var p = $('#t-'+$('input[name=parent]').val()).find('tbody.content');
	var x = newImages(e.response.uploadedFile);
	p.append(x).sortable({handle:'td.dragHandle',stop: function(event, ui) { uploadModel.sort(ui) }});
	kendo.bind($(".tab"), uploadModel);
}

function video_uploaded(e) {
	var newDoc = kendo.template($("#newVideo").html());
	var x = newDoc(e.response.uploadedFile);
	$('#t-videos tbody.content').append(x).sortable({handle:'td.dragHandle',stop: function(event, ui) { uploadModel.sort(ui) }});
	kendo.bind($(".tab"), uploadModel);
}
