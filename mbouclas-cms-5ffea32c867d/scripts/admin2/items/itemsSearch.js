var itemsSearch = function(el, options) {
	var $el = $(el);
	var obj = {
		defaults : {
			inputClass: "ac_input",
			resultsClass: "ac_results",
			loadingClass: "ac_loading",
			minChars: 1,
			valKey:'value',
			delay: 400,
			type:'GET',
			dataType:'JSON',
			matchCase: false,
			matchSubset: true,
			matchContains: false,
			cacheLength: 10,
			max: 100,
			mustMatch: false,
			extraParams: {},
			selectFirst: true,
			formatItem: function(row) { return row; },
			formatMatch: function(row, i, max) {
					return row[this.valKey] + " " + i;
				},
			autoFill: false,
			width: 0,
			multiple: false,
			multipleSeparator: ", ",
		
		    scroll: true,
		    scrollHeight: 180
		},
		options :{},
		allData:{},
		init : function(el,options) {
			this.options = $.extend(this.defaults, options)
			$el.bind("search", function() {
				
			}).
			bind("setOptions", function() {
				$.extend(options, arguments[1]);
			}).	
			bind("destroy", function() {
				$el.unbind();
			});
			
			if (this.options.drawGrid) {//EXPECTS OPTIONS FOR GRID CREATION LIKE URL FOR DATA SOURCE
				var g = $('<div class="grid"></div>').appendTo(el);
				
				el.data('grid',this.drawGrid(g,$.extend(true,this.itemSearchSettings,this.options.drawGrid)));//return the entire grid object to handle methods
				this.grid = el.data('grid');
				if (el.find('.searchBox').length > 0) {
					this.createSearchFilter(el);
				}
			}
			return el;
		},
		createSearchFilter: function(el) {
			var box = $(el).find('.searchBox');
			box.find('.submitSearch').bind('click',function(e){ 
				$.extend(el.data('grid').dataSource.transport.options.read.data,{data:mcms.formData(box.find('.filterdata'),{ getData:true})});
				el.data('grid').dataSource.fetch();
				el.data('grid').refresh();
			});
		},
		setOptions : function(options) { this.options = $.extend(this.options, options)},
		search: function(options) {},
		drawGrid : function(el,options) {
			options = $.extend(this.gfidDefaults,options);

			return el.kendoGrid(
			$.extend(true,options,{
		    dataSource: {
				change: function(e) {
      				  // handle event
    			},
				error: function(e) {
      				  // handle event
    			},
				requestStart: function(e) {
      				  // handle event
    			},
		        pageSize: this.gridPageSize(options.dataSource.pageSize)
		    },
		    columns: this.gridColumns(options.columns)
		})).data('kendoGrid');
	},
	templates : {
		gridToolBar : '<div class="k-button k-button-icontext Filter float-right" id="Filter-PagesSearch" data-id="gridview">Filter&nbsp;&nbsp;<span class=" k-icon k-filter"></span></div>',
		
	},
	gridPageSize : function(size) { return (size) ? size : 10},
	gfidDefaults :{
		height: 350,
	    scrollable: true,
	    sortable: true,
	    filterable: false,
	    pageable: true
	},
	gridColumns : function(options) {
		if (options) {
			return options;
		}
		else {
			return 	 [
		        {
		            field: "id",
		            title: "#ID",
		            width: 70
		        }
		    ]
		}
	},
	itemSearchSettings : {
		drawGrid : {
			dataSource : {
				serverPaging:true,
				pageSize :5,
	            serverFiltering: true,
                serverSorting: true,
		        schema: {
		        	data : function(data) {
						return data;
		        	},
		        	total: function(data) {
             			return data.data.total;
         			},
		            model: {
		                fields: {
		                    id:    { type: "number" },
		                    title: { type: "string" }
		                }
		            }
		        },
				transport: {
			        read: {
			        	url : '',
						dataType : 'json',
						data : {availability:2},
						type:'POST'
			        }
	        	}
			},
			resizable: true,
			filterable: true,
   			sortable: true,
            pageable: true,			
			columns : [
		        {
		            field: "id",
		            title: "#ID",
		            width: 70
		            
		        },
		        {
		            field: "title",
		            title: $.lang.title,
		            filterable:false
		        },
		        {
		            title: "Actions",
		            width: 70,
		            filterable:false
		        }
		    ]
   		}
	}
		
	}//END OBJ

obj.init($el,options);
return obj;
}//END CLASS

$.fn.searchItems = function(options ) {  

	return this.each(function() {
		
		var element = $(this);
		if (element.data('search')) return;
		var c = new itemsSearch(this, options);
		element.data('search', c);  
	});
};
head.ready(function(){
	var itemSearchSettings = {
		drawGrid : {
			dataSource : {
				serverPaging:true,
				pageSize :5,
	            serverFiltering: true,
                serverSorting: true,
		        schema: {
		        	data : function(data) {
						return data;
		        	},
		        	total: function(data) {
             			return data.data.total;
         			},
		            model: {
		                fields: {
		                    id:    { type: "number" },
		                    title: { type: "string" }
		                }
		            }
		        },
				transport: {
			        read: {
			        	url : '',
						dataType : 'json',
						data : {availability:2},
						type:'POST'
			        }
	        	}
			},
			resizable: true,
			filterable: true,
   			sortable: true,
            pageable: true,			
			columns : [
		        {
		            field: "id",
		            title: "#ID",
		            width: 70
		            
		        },
		        {
		            field: "title",
		            title: $.lang.title,
		            filterable:false
		        },
		        {
		            title: "Actions",
		            width: 70,
		            filterable:false
		        }
		    ]
   		}
	};

	var itemCategorySettings = {
	url : '',
	drawGrid : {
		root : 'results',
		dataSource : {
            serverFiltering: true,
            serverSorting: true,
	        schema: {
	        	data : function(data) {
					return data.results;
	        	},
	            model: {
	                fields: {
	                    id:    { type: "number" },
	                    text: { type: "string" }
	                }
	            }
	        },
			transport: {
		        read: {
		        	url :'',
					dataType : 'json',
					data : {availability:2},
					type:'POST'
		        }
        	}
		},
		filterable: true,
			sortable: true,
		columns : [
	        {
	            field: "id",
	            title: "#ID",
	            width: 70
	            
	        },
	        {
	            field: "text",
	            title: $.lang.category,
	            filterable:false
	        },
	        {
	            title: "Actions",
	            width: 70,
	            filterable:false
	        }
	    ]
	}
};

});