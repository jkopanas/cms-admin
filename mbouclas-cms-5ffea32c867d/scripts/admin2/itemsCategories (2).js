/* 
* We include the select categories and items plugin for the featured. The action when adding to our featured is control from in here.
* Also included is the category details for the full edit view of the category.
* Image in category is handled by the universal media selector plugin
*/
var cache = {},catObj = {},validatable = {},templates={},sortObj = {},themes ={},newCatForm={},sortableOptions = {
			disableNesting: 'no-nest',
			forcePlaceholderSize: true,
			handle: 'img.dragHandle',
			items: 'li',
			opacity: .6,
			placeholder: 'placeholder',
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div',
			stop: function(event, ui) {
				catObj.updateState(ui);
			}
};
jQuery.fn.center = function(parent) {
    if (parent) {
        parent = this.parent();
    } else {
        parent = window;
    }
    this.animate({
        "position": "absolute",
        "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
        "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
    });
return this;
}
head.ready(function(){
sortObj = $('ol.sortable');

$(".toValidate").each(function(k,v){
	validatable[$(this).attr('id')] = $(this).kendoValidator().data("kendoValidator");
});

$('.templates script').each(function(k,v){
	templates[$(this).attr('id')] = kendo.template($(this).html());
});

var url = "/ajax/loader.php?file=items/itemsCategoriesNew.php";
var dbDefaults = {    method: "POST",
    dataType: "json",
	trigger:"loadComplete"
};
db = new mcms.dataStore(dbDefaults);

var catItems = $.parseJSON($('textarea[name=catItems]').val());
themes = $.parseJSON($('textarea[name=themesEnc]').val());

catObj = {
	elOrder :{},
	options : { 
		target: ''
	},
	templates : { 
		catItemTemplate:{}
	},
	settings : {},
	init: function(options) {
		this.settings = $.extend(this.options,options);
		this.templates = $.extend(this.templates,options.templates);
	},
	drawCat : function(obj,level) {
		var render ='';
		level = (level == null) ? 0 : level;
		for (i in obj) {
			obj[i]['level'] = level;
			render += this.templates.catItemTemplate(obj[i]);
			if (typeof obj[i]['subs'] == 'object') {
				render += '<ol data-level="'+level+'">' + this.drawCat(obj[i]['subs'],level+1) + '</ol>';
			}
		}
		return render;
	},
	addToList : function(obj,save,target) {
		if (save) {
			db.url = url + '&action=addCat&module='+mcms.mcmsSettings.module;
			db.data = {data :obj,all:1};//All will get the tree again
			db.trigger = 'newItem';
			if (target != null) {
				db.target = target;
			}
			$(db).loadStore(db);
			return;
		}
		if (target == null) {
			$(this.options.target).append(this.drawCat(obj));
		}
		/* THERE IS A BUG HERE. IT ATTACHES IT ON THE WRONG PARENT */
		else {
				target.append('<ol data-level="'+ parseInt(target.attr('data-level'))+1 +' ">' + this.drawCat(obj,parseInt(target.attr('data-level'))+1) + '</ol>');

			
		}
		sortObj.nestedSortable(sortableOptions);		
	},
	arraied : function (obj) {
	return obj.nestedSortable('toArray', {startDepthCount: 0});
	},
	newCategory : function(obj) {
		
	},
	recurseCategories : function(dad,ext) {
		
	},
	flattenCategories : function(arr,obj) {
		
	},
	renderCategoriesTree : function(obj,level) {
		
	},
	updateSubCounts : function(el,level) {
		var me = this;
		
		el.children('li').each(function(k,v){
			level = $(this).parents('ol').length-1;
			var lng = $(this).find('li').length;

			$(this).find('.numSub:first').html(lng);
			if ($(this).children('ol').length > 0) {
				$(this).find('.toggleChildren:first').parent().show();
				me.updateSubCounts($(this).children('ol:first'),level+1);
			} 
						if (lng == 0){
				$(this).find('.toggleChildren:first').parent().hide();
			}
		});
	},
	updateState : function(el) {
		//NEED TO LOOP THIS RECURSIVELY. CREATE A FUNCTION
		var level = $(el.item).parents('ol').length-1;
		$(el.item).find('div:first').removeClass('level-'+$(el.item).attr('data-level')).addClass('level-' + level);
		$(el.item).attr('data-level',level);
		
		var x = this.calculateLevel($(el.item).parents('ol:first').parent());//we update everything from the parent down to update the order as well
		
		//RUN UPDATE AJAX
		db.url = url + '&action=updateOrder&module='+mcms.mcmsSettings.module;
		db.data = {data : x,all:1};
		db.trigger = 'updateState';
		$(db).loadStore(db);
	},
	calculateLevel : function(el,level,ret){//CALCULATE LEVEL AND PATH OF A LI ELEMENT
		var me = this;
		if (typeof ret == 'undefined') {
			var ret =  new Array;
		}
		el.attr('data-order_by',el.index());
		me.calculateParent(el);//SELF
		ret.push(mcms.callInternalMethod('listAttributes',el,{prefix:'data-',returnType:'keyVal',stripPrefix:true}));
		el.children('ol').each(function(key,val){
			$(this).children('li').each(function(k,v){
				level = $(this).parents('ol').length-1;
				me.calculateParent($(this));
				$(this).attr('data-order_by',$(this).index());
				ret.push(mcms.callInternalMethod('listAttributes',$(this),{prefix:'data-',returnType:'keyVal',stripPrefix:true}));
				$(this).find('div:first').removeClass('level-'+$(this).attr('data-level')).addClass('level-'+level)
				$(this).attr('data-level',level);
				if ($(this).children('ol').length > 0) {
					
					me.calculateLevel($(this),level+1,ret);
				}
			});
		});
		return ret;
	},
	calculateParent : function(el) {
		var parentid = 0;
		if (el.parents('ol:first').parent().attr('data-id') != null) {
			parentid = el.parents('ol:first').parent().attr('data-id');
		}
		var tmp = new Array;
		el.parents('ol').each(function(k,v){//path
			if (typeof $(this).parent().attr('data-id') != 'undefined') {
				tmp.push($(this).parent().attr('data-id'))
			}
		});
		tmp.reverse();
		tmp.push(el.attr('data-id'));
		el.attr('data-parentid',parentid);
		el.attr('data-categoryid_path',tmp.join('/'));
	},
	drawResults : function(target,ob) {
		
	},
	drawCatSelect : function(obj,level) {
		var render ='';
		level = (level == null) ? 0 : level;
		for (i in obj) {
			obj[i]['level'] = level;
			render += this.templates.catSelectTemplate(obj[i]);
			if (typeof obj[i]['subs'] == 'object') {
				render += this.drawCatSelect(obj[i]['subs'],level+1);
			}
		}
		return render;
	},
	createAlias : function(inpt) {
		var txt ='';
		for (var i=0; i < inpt.length; i++) {
	    	 txt+=mcms.convertchar(inpt.charAt(i))
		}
	return txt;
	}
	
};//EMD CAT HANDLER
/* START APP */
catObj.init({target:$('#catItems'),templates:templates});
$('select[name=parent_catid]').append(catObj.drawCatSelect(catItems));
catObj.addToList(catItems);
sortObj.nestedSortable(sortableOptions);
/* BEGIN BINDS */

$(db).bind('newItem',function(){
	if (db.records.target) {
	catObj.addToList(db.records.items,null,$('#list_'+db.records.target));
	}
	else {
		catObj.addToList(db.records.items);//root element
	}
	$('select[name=parent_catid]').find('option:gt(0)').remove();
	$('select[name=parent_catid]').append(catObj.drawCatSelect(db.records.all));
	document.getElementById("newCategory").reset();
});

$('.toggleAll').on("click", function(e){//delegate
	e.preventDefault();
	$('#catItems').find('li[data-level=0]').each(function(k,v){
		$(this).find('.toggleChildren').trigger('click');
	});
});


$(document).on("click",'.wToogle', function(e){//live
	e.preventDefault();
	$(this).parents('.catItem:first').find('.formEl_a').toggle('fast');
});

$(document).on("click",'.toggleChildren', function(e){//live
	e.preventDefault();
	$(this).parents('.catItem:first').parent().children('ol').toggle(150);
});

$(db).bind('updateState',function(){
//	$('.debugArea').html('<pre>' + db.records + '</pre>');
	catObj.updateSubCounts(sortObj);
	$('select[name=parent_catid]').find('option:gt(0)').remove();
	$('select[name=parent_catid]').append(catObj.drawCatSelect(db.records.all));
	saved();
});

$('.catItem').live({
    mouseenter:
       function()
       {
			$(this).find('.row-actions').show();
       },
    mouseleave:
       function()
       {
			$(this).find('.row-actions').hide();
       }
   }
);

$('input[name=category]').keyup(function(){
    $('input[name=alias]').val(catObj.createAlias($(this).val()));
});

$('.category').live('keyup',function(e){
	 $('input[name=alias-'+$(this).attr('data-id') + ']').val(catObj.createAlias($(this).val()));	
});



$('.saveCat').on('click',function(e) {
	e.preventDefault();
	if (validatable[$(this).parents('form:first').attr('id')].validate()) {
		var tmp = mcms.formData($(this).parents('form:first').find('.saveData'),{ getData:true});
		tmp['parent_catid'] = $.extend(tmp['parent_catid'],mcms.callInternalMethod('listAttributes',$('select[name=parent_catid] option:selected'),{prefix:'data-',returnType:'keyVal',stripPrefix:true}));
		catObj.addToList(tmp,1,$('#list_'+tmp['parent_catid']['value']));
	}
});
/* NEED TO CONVERT IT TO A FUNCTION THAT SETS CURRENT => CALCULATES PREV NEXT */
$(document).on("click",'.modifyCategory', function(e){//live	
	e.preventDefault();
	$('body').append('<div id="modal-back"></div>');
	$('#modal-back').css({'top':0}).center()
	$('#modal').show().center();
	modalCalc($(this));


	
	disable_scroll();
});

$('#modal .close').on("click", function(e){//delegate
	e.preventDefault();
	$('#modal-back').remove();
	$('#modal').hide();
	enable_scroll();
});

$('#modal .left').on("click", function(e){//delegate
	e.preventDefault();
	modalCalc($('#list_'+ $($('#modal').data('prev')).attr('data-id')).find('.modifyCategory:first'));
});

$('#modal .right').on("click", function(e){//delegate
	e.preventDefault();
	modalCalc($('#list_'+ $($('#modal').data('next')).attr('data-id')).find('.modifyCategory:first'));
});

$(window).resize(function() {
	$('#editCat').center();
});
});//END HEAD

function modalCalc(current) {
	$('#modal .left, #modal .right').show();
	var found =0;
	var all = current.parents('ol:last').find('li');
	var me = current.parents('li:first');
	all.each(function(k,v){
		if ($(this).attr('data-id') == me.attr('data-id')) {
			found = k;
		}
	});
	
	if (typeof all[found-1] == 'undefined') {
		$('#modal .left').hide();
	}
	if (typeof all[found+1] == 'undefined') {
		$('#modal .right').hide();
	}
	$('#modal').data({'categoryid':me.attr('data-id'),'me':me,prev:all[found-1],next:all[found+1]});
	$('#modal h2').html(me.attr('data-id'));
}

/* disable scrolling */

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault) {
        e.preventDefault();
    }
    e.returnValue = false;
}

function keydown(e) {
    for (var i = keys.length; i--;) {
        if (e.keyCode === keys[i]) {
            preventDefault(e);
            return;
        }
    }
}

function wheel(e) {
    preventDefault(e);
}

function disable_scroll() {
    if (window.addEventListener) {
        window.addEventListener('DOMMouseScroll', wheel, false);
    }
    window.onmousewheel = document.onmousewheel = wheel;
    document.onkeydown = keydown;
}

function enable_scroll() {
    if (window.removeEventListener) {
        window.removeEventListener('DOMMouseScroll', wheel, false);
    }
    window.onmousewheel = document.onmousewheel = document.onkeydown = null;
}