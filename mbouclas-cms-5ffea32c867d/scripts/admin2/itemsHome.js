var checkedIDs = {};
var allData ={};
var grid;
var m = {};
var usedCols = {};
head.ready(function(){
	$(".mSelect").chosen();
	 $(".calendar").kendoCalendar({
	 	change:function(e) {
			var str = kendo.toString(this.value(), 'yyyy-MM-dd');
			var d = str.match(/\d+/g);
			$('#'+e.sender.element.attr('data-trigger')).val(+new Date(d[0], d[1] - 1, d[2])/1000);
	 	}
	 });
var module = $.parseJSON($('textarea[name=currentModuleEnc]').val());
var currentModule = $('input[name=module]').val();

var db = new mcms.dataStore({
     method: "POST",
     url:"",
     //dataType: "json",
     trigger:"loadComplete"
 });
 var templates = {};//Look out for them on the actual tpl
 $('.kTemplate').each(function(k,v){
 	templates[$(this).attr('data-name')] = kendo.template($(this).html()); 
 });


var toolBar = [
	{name:'filter', template : kendo.template($("#toolBar").html()) },
	{name:'enable', template : kendo.template('<a class="k-button k-button-icontext" href="\\#" data-bind="click: doActions" data-method="enableItems"><span class="k-icon k-enable"></span>'+$.lang.enable+'</a>') },
	{name:'disable', template : kendo.template('<a class="k-button k-button-icontext k-grid-delete" href="\\#" data-bind="click: doActions" data-method="disableItems"><span class="k-icon k-disable"></span>'+$.lang.disable+'</a>') },
	{name:'delete', template : kendo.template('<a class="k-button k-button-icontext k-grid-delete" href="\\#" data-bind="click: doActions" data-method="deleteItems"><span class="k-icon k-delete"></span>'+$.lang.delete+'</a>') }
]

var columnSet = {
idCheck : {
    	title: "<input type='checkbox' class='check-all' id='total_check'>",
    	field: "id",
        width: 40,
        template : templates.checkBox,
        filterable:false,
        sortable:false,
        reorderable:false
        
    },
id :    {
        field: "id",
        title: "#ID",
        width: 45
        
    },
title :    {
        field: 'title',
        type: 'string',
        title: "Title",
        filterable:true,
        template:templates.title,
        width:'40%'
    },
category :     {
        field: 'category',
        title: $.lang.category,
        filterable:false,
        template:templates.category,
        width:'35%',
        sortable:false
    },
date_added :    {
        field: 'date_added',
        title: $.lang.date_added,
        filterable:false,
        template:templates.date_added,
        width:'15%'
    },
active :     {
        field: 'active',
        title: $.lang.availability,
        filterable:false,
        template:templates.active,
        width:'10%'
    },
price : {
	    field: 'eshop.price',
        type: 'string',
        title: "Price",
        filterable:false,
        sortable:false,
        template:templates.price,
        width:'10%'
	
},
sku : {
	    field: 'sku',
        type: 'string',
        title: "SKU",
        filterable:true,
        template: templates.sku,
        width:'10%'
},
numOrders : {}
};
var columns = new Array;
var defaultCols = ['idCheck','id','title','category','date_added','active'];
if (typeof module.settings.quickSearchCols != 'undefined') {
var cols = $.parseJSON(module.settings.quickSearchCols);
}
var d = (typeof cols == 'undefined') ? defaultCols : cols;

for (var i in d) {
	columns.push(columnSet[d[i]]);
	usedCols[d[i]] = d[i]
}

var s = {
    data : function(data) {
    	allData = data;

    	if (data.results.length == 0) {
    		return [];
    	}

		return data.results;
	},
	total :function(data) {
		return data.data.total;
	}
}


var f =  {
				name: "FilterMenu",
				extra: false, // turns on/off the second filter option
				messages: {
					info: "Filter results:", // sets the text on top of the filter menu
					filter: $.lang.apply, // sets the text for the "Filter" button
					clear: $.lang.txt_reset, // sets the text for the "Clear" button
					
					// when filtering boolean numbers
					isTrue: "custom is true", // sets the text for "isTrue" radio button
					isFalse: "custom is false" // sets the text for "isFalse" radio button
				},
				operators: {
					//filter menu for "string" type columns
					string: {
						contains: "Contains",
						eq: "Equals to",
						neq: "Is not equal to",
					},
					//filter menu for "number" type columns
					number: {
						eq: "Custom Equal to",
						neq: "Custom Not equal to",
						gte: "Custom Is greater than or equal to",
						gt: "Custom Is greater than",
						lte: "Custom Is less than or equal to",
						lt: "Custom Is less than"
					},
					//filter menu for "date" type columns
					date: {
						eq: "Custom Equal to",
						neq: "Custom Not equal to",
						gte: "Custom Is after or equal to",
						gt: "Custom Is after",
						lte: "Custom Is before or equal to",
						lt: "Custom Is before"
					}
				}
			};
grid = $('#gr');
m = grid.searchItems({drawGrid : {height:500,filterable:f,toolbar:toolBar,columns:columns,dataSource : {serverPaging:true,schema:s,pageSize:50,transport: {read : {type:'POST',dataType:'json',url :"/ajax/loader.php?file=itemsSearch.php&module=" + mcms.mcmsSettings.module + '&action=pagesFilter'}}}}}).data('search');



m.grid.bind("dataBound", function(e) {
	if (typeof allData.data == 'undefined') {
		return;
	}
var t = grid.find('.k-pager-wrap');
if (t.find('.float-right').length == 0) {
	t.append('<div class="float-right">'+$.lang.total+' : <span>'+allData.data.total+'</span></div>');
}
else {
	t.find('.float-right span').html(allData.data.total)
}
 });
 
$('.itemRow').live({
    mouseenter:
       function()
       {
			$(this).find('.row-actions').show();
       },
    mouseleave:
       function()
       {
			$(this).find('.row-actions').hide();
       }
   }
);

$('#total_check').live('click', function(e){
	var checkbox = $(this);
	var el = grid;
	var table = grid.find('.messages');

	if (checkbox.prop('checked') == true)
	{
		el.find('.check-values').attr('checked',true);
		el.find('.check-values').parentsUntil('TR').parent().addClass('row_selected');
		table.html(el.find('.check-values:checked').length);

	}
	else 
	{
		el.find('.check-values').attr('checked',false);
		el.find('.check-values').parentsUntil('TR').parent().removeClass('row_selected');
		table.html(el.find('.check-values:checked').length);
	}
});


$('.check-values').live('click', function(e){
	var checkbox = $(this);
	var el = grid;
	var table = grid.find('.messages');
	var row = checkbox.parentsUntil('TR').parent();
	if (checkbox.prop('checked') == true)
	{
		table.html(el.find('.check-values:checked').length);
		row.addClass('row_selected');
	}
	else 
	{
		table.html(el.find('.check-values:checked').length);
		row.removeClass('row_selected');
	}
});

gridModel = kendo.observable({
defaultValues : {url :'/ajax/loader.php?file=ajax_responses.php&module='+currentModule, dataType:'json' },
callAjax : function(options) {
	db.url = options.url;
	db.dataType = options.dataType;
	db.trigger = options.trigger;
	db.data = options.data;
	
	$(db).loadStore(db);
},
callBack : function(func) {
	if (typeof eval(this[func]) == 'function')
	{
		return eval(this[func]).apply(this, Array.prototype.slice.call(arguments, 1));
	}
	else { console.log(func + ' is not registered'); }
},
doActions : function(e) {
	e.preventDefault();
	var func = $(e.currentTarget).attr('data-method');
	var el = grid;
	var table = grid.find('.messages');
	this.tmpTarget = el;
	this.callBack(func,el);
},
enableItems : function(el) {
	var toSave = new Array;
	var t = new Array;
	el.find('.check-values:checked').each(function(k,v){
		toSave.push($(this).val());
		t.push($(this).parentsUntil('TR').parent());
	});
	this.affectedRows =  t;
	this.mode = 'enable';
	
	this.callAjax({url:this.defaultValues.url+'&action=ItemActions&mode=ActivateChecked',trigger:'itemsEnabled',dataType:this.defaultValues.dataType,data:{'ids':toSave}});
},
disableItems : function(el) {
	var toSave = new Array;
	var t = new Array;
	el.find('.check-values:checked').each(function(k,v){
		toSave.push($(this).val());
		t.push($(this).parentsUntil('TR').parent());
	});
	this.affectedRows =  t;
	this.mode = 'disable';
	
	this.callAjax({url:this.defaultValues.url+'&action=ItemActions&mode=DeactivateChecked',trigger:'itemsEnabled',dataType:this.defaultValues.dataType,data:{'ids':toSave}});
},
deleteItems : function(el) {
	var toSave = new Array;
	var t = new Array;
	var r=confirm($.lang.u_sure);
	if (r==true)
    {
		el.find('.check-values:checked').each(function(k,v){
			toSave.push($(this).val());
			t.push($(this).parentsUntil('TR').parent());
		});
		this.affectedRows =  t;
		this.mode = 'disable';
	
		this.callAjax({url:this.defaultValues.url+'&action=ItemActions&mode=DeleteChecked',trigger:'itemsDeleted',dataType:this.defaultValues.dataType,data:{'ids':toSave}});
		/* REMOVE FROM GRID AFTER REMOVING FROM DS FIRST */
	}
},	
});

$(db).bind('itemsEnabled',function(){
	var message = (gridModel.mode == 'enable') ? '<span class="notification ok_bg">'+$.lang.yes+'</span>' : '<span class="notification error_bg">'+$.lang.no+'</span>';
	$.each(gridModel.affectedRows,function(k,v){
		v.find('.notification').parent().html(message);
	});
	
});
$(db).bind('itemsDeleted',function(){
	var message = (gridModel.mode == 'enable') ? '<span class="notification ok_bg">'+$.lang.yes+'</span>' : '<span class="notification error_bg">'+$.lang.no+'</span>';
	$.each(gridModel.affectedRows,function(k,v){
		v.remove();
	});
			
		m.grid.dataSource.fetch();
		m.grid.refresh();
	
});

kendo.bind(grid, gridModel);
$(window).bind('changeGrid',function(){
	var i = arguments[1];
	$.extend(i,{module:currentModule,itemid:i.id});
	
	$.each(templates,function(k,v){
		if (typeof usedCols[k] != 'undefined') {		
			var a = templates[k];
			$('#'+k+'-'+i.id).parent().html(a(i));
		}
	});

});


 itemWindow = $("#itemWindow"),
    undo = $(".quickEdit").live("click", function(e) {
    	e.preventDefault();
   	var additional = mcms.callInternalMethod('listAttributes',$(this),{prefix:'data-',returnType:'keyVal',stripPrefix:true});
	var addUrl= new Array;
	 $.each(additional,function(k,v){
	 	addUrl.push(k+'='+v);
	 });
if (!itemWindow.data("kendoWindow")) {
    itemWindow.kendoWindow({
        width: "800px",
        height:'90%',
        title: $.lang.modify,
        content: "/ajax/loader.php?trigger=changeGrid&file=items/itemsGeneralForm.php&simple=1&"+addUrl.join('&'),
        visible:false,
        modal:true
    });
    $(window).resize(function() {
	itemWindow.attr('height',$(window).height()-10 +'px');
	itemWindow.data("kendoWindow").center();
	
});
}
else {
	 bound = true;

	itemWindow.data("kendoWindow").refresh("/ajax/loader.php?file=items/itemsGeneralForm.php&trigger=changeGrid&simple=1&" +addUrl.join('&'));
}

    itemWindow.data("kendoWindow").open();
    itemWindow.data("kendoWindow").center();
   
 });
 
 $("#filtersWindow").kendoWindow({
    width: 600,
    height:650,
    title: $.lang.filter,
    visible:false,
    modal:true,
    close:function() { $("#Filter-PagesSearch").show()}
});

$("#Filter-PagesSearch").live("click", function(e) {
	$(this).hide();
$("#filtersWindow").data("kendoWindow").open().center();
});

$('.PerformSearchBtn').live('click', function(e){
	e.preventDefault();
	$('#searchform').trigger('submit');
});

$('#searchform').live('submit', function(e){
	e.preventDefault();
//	console.log(m.grid.dataSource)
	var x = mcms.formData($('.SearchData'),{ getData:true});
	var filters = {};
	var d = {};
	$.each(x,function(k,v){//FORM FILTERS AS PHP EXPECTS THEM
		if (typeof v.operator != 'undefined') {//ITEM SPECIFIC
			v['field'] = k;
			filters[k] = v;
		}
		else {//ALL OTHER.
			d[k] = v['value'];
		}
		
	});
	m.grid.dataSource.transport.options.read.type = 'post';
	m.grid.dataSource.transport.options.read.data = $.extend(d,{filter:{filters:filters}});
	m.grid.dataSource.read();
	m.grid.refresh();
	
});

$('.categories').live('click', function(e){
	e.preventDefault();
	$('#searchform').find('select#categoryid').val($(this).attr('rel'));
	$("#searchform .mSelect").trigger("liszt:updated")
	$('#searchform').trigger('submit');
});

});//END JQ