$.fn.mCms = function(options) {
	
	var mcms = function(element, options) {
	var baseSettings = $.extend({
		lang : 'en',
		module : 'content'
	}, options);
	var l = {};
	this.editors = {};
	this.mcmsSettings = baseSettings;
	this.settings = baseSettings;
	this.callInternalMethod = function(func) {//EXPOSE INTERNAL METHODS ON DEMAND	
		if (typeof eval(func) == 'function')
		{
			return eval(func).apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else { console.log(func + ' is not registered'); }
	}//END METHOD
	this.system = $.parseJSON($('textarea[name=systemPrefs]').val());
	
		this.dataStore = function( ds ) {
	   
		$.fn.loadStore = function(ds){
			$.ajax({
				type: ds.method,
				url: ds.url,
				data: ds.data,
				dataType: ds.dataType,
				cache:false,
                async:ds.async?ds.async:false,
                timeout:ds.timeout?ds.timeout:10000,				
				queue: "autocomplete",
				contentType:'application/x-www-form-urlencoded;charset=utf-8',
				accepts: {
					xml: "application/xml, text/xml",
					json: "application/json, text/json",
					_default: "*/*"
				},
				beforeSend:function(){
					loadStatus = true;
				},
				success: function(data) {
					loadStatus = false;
					if(data)
					{ds.records=data;}
					$(ds).trigger(ds.trigger);
				},				
				error: function()
				{					
					loadStatus = false;			
					$(ds).trigger('loadError');
					
				}
			});//END AJAX
		};//END LOADSTORE

		try {
			return ds;
		} finally {
			ds = null;
		}
		
	}//END METHOD
	lang = new this.dataStore({ url:'/scripts/lang/lang.'+baseSettings.lang+'.json',trigger:'loadComplete','dataType':'json'});
	
	
	$(lang).bind('loadComplete',function(e){
			l = lang.records;
		});
		
	$(lang).loadStore(lang);
		
	this.getLang = function() {
		return l;
	}
		
	this.init = function(options) {
		var settings = $.extend( {
		expandMode : ''
		}, options);
		
		
	}
	

	
	this.showLang = function() {
		return this.l;
	}
	
	this.formData = function(obj,options){
		var settings = $.extend( {
			'ref' : 'name',
			returnType : 'array',
			getData: false
		}, options);

		obj = $(obj);
		var me = this;
		var all_inputs = new Array();
		var data = {};
		var x = {};
		var i = 0;
		var d = {};
		
		obj.each(function (k,v) {
//			console.log(listAttributes($(this),{prefix:'data-',returnType:'keyVal',stripPrefix:true}));
		    if ($(this).prop('type') == 'radio' || $(this).prop('type') == 'checkbox') {
		        if ($(this).is(':checked')) {
		        	
		        	if (strstr($(this).attr(settings.ref),'[]'))//ARRAY
					{
			
					if (!$.isArray(x[$(this).attr(settings.ref).replace('[]','')]))
					{
						x[$(this).attr(settings.ref).replace('[]','')] = Array();
					}
						 x[$(this).attr(settings.ref).replace('[]','')].push( $(this).val());
						 data[$(this).attr(settings.ref).replace('[]','')] = x[$(this).attr(settings.ref).replace('[]','')];
//						 d[$(this).attr(settings.ref).replace('[]','')] = $('input['+settings.ref+'="'+$(this).attr(settings.ref)+'"]');
						 d[$(this).attr(settings.ref).replace('[]','')] = $(this);
					}
		        	else {
		        		 data[$(this).attr(settings.ref)] = $(this).val();
		        		  d[$(this).attr(settings.ref)] = $(this);
		        	}
		            all_inputs.push($(this).attr(settings.ref) + ':::' + $(this).val());
		//            $(data).attr($(this).attr(settings.ref),$(this).val());
		          
		        }//END IF CHECKED
		    }//END IF 
		    else  {
		        if ($(this).val()) {
		        	if (strstr($(this).attr(settings.ref),'[]'))//ARRAY
					{
					if (!$.isArray(x[$(this).attr(settings.ref).replace('[]','')]))
						{
							x[$(this).attr(settings.ref).replace('[]','')] = Array();
						}
						 x[$(this).attr(settings.ref).replace('[]','')].push( $(this).val());
						 data[$(this).attr(settings.ref).replace('[]','')] = x[$(this).attr(settings.ref).replace('[]','')] ;
//						 d[$(this).attr(settings.ref).replace('[]','')] = $($(this).prop('type')+'['+settings.ref+'="'+$(this).attr(settings.ref)+'"]');
						 d[$(this).attr(settings.ref).replace('[]','')] = $(this);
					}
		        	else {
		        		 data[$(this).attr(settings.ref)] = $(this).val();
		        		  d[$(this).attr(settings.ref)] = $(this);
		        	}
		            all_inputs.push($(this).attr(settings.ref) + ':::' + $(this).val());
					
		        }//END IF
		    }//END IF
		    i++;
		});//END FOREACH

		if (settings.getData) {
			var m = {};
			$.each(d,function(k,v) {
				if (isArray(v)) {data[k] }
				m[k] = $.extend(listAttributes(v,{prefix:'data-',returnType:'keyVal',stripPrefix:true}),{value:data[k]});
			});
			return m;
		}
		
		if (settings['returnType'] == 'string') {
		return all_inputs;
		}//END IF
		else if (settings['returnType'] == 'array') {
		   return data;  
		}//END IF
		
	}//END METHOD
	
	var strstr = function (haystack, needle, bool) {
    var pos = 0;

    haystack += '';
    pos = haystack.indexOf(needle);
    if (pos == -1) {
        return false;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
	}
	
	var isArray = function(a)
	{
		return Object.prototype.toString.apply(a) === '[object Array]';
	}
	
	var mapAttributes = function(el,prefix) {
			var maps = [];
			el.each(function() {
				var map = {};
				for(var key in this.attributes) {
					if(!isNaN(key)) {
						if(!prefix || this.attributes[key].name.substr(0,prefix.length) == prefix) {
							map[this.attributes[key].name] = this.attributes[key].value;
						}
					}
				}
				maps.push(map);
			});
			return (maps.length > 1 ? maps : maps[0]);
		}
		
	var listAttributes = function(el,options) {
		var settings = $.extend( {
			'pefix' : '',
			returnType : '',
			stripPrefix: false
		}, options);

			var list = [];

			el.each(function() {
				var attributes = [];

				for(var key in this.attributes) {
					
					if(!isNaN(key) && this.attributes[key] !== null && typeof(this.attributes[key]) != 'undefined') {
						if(!settings.prefix ||  this.attributes[key].name.substr(0,settings.prefix.length) == settings.prefix) {
							attributes.push(this.attributes[key].name);
						}
					}
				}
				list.push(attributes);
			});

			if (settings.returnType == 'keyVal') { 
				var ret = {};	
				var key ='';
				for(var i in list) {
					if (list[i].length > 0) {
						for(var x in list[i]) {
							key = (settings.stripPrefix) ? list[i][x].replace(settings.prefix,'') : list[i][x];
							ret[key] = el.attr(list[i][x]);
						}
					}
				}
				return ret;
			}

			return (list.length > 1 ? list : list[0]);
		}//END METHOD

	var objectEquals = function( x, y ) {
	  if ( x === y ) return true;
	    // if both x and y are null or undefined and exactly the same
	
	  if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) return false;
	    // if they are not strictly equal, they both need to be Objects
	
	  if ( x.constructor !== y.constructor ) return false;
	    // they must have the exact same prototype chain, the closest we can do is
	    // test there constructor.
	
	  for ( var p in x ) {
	    if ( ! x.hasOwnProperty( p ) ) continue;
	      // other properties were tested using x.constructor === y.constructor
	
	    if ( ! y.hasOwnProperty( p ) ) return false;
	      // allows to compare x[ p ] and y[ p ] when set to undefined
	
	    if ( x[ p ] === y[ p ] ) continue;
	      // if they have the same strict value or identity then they are equal
	
	    if ( typeof( x[ p ] ) !== "object" ) return false;
	      // Numbers, Strings, Functions, Booleans must be strictly equal
	
	    if ( ! Object.equals( x[ p ],  y[ p ] ) ) return false;
	      // Objects and Arrays must be tested recursively
	  }
	
	  for ( p in y ) {
	    if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) return false;
	      // allows x[ p ] to be set to undefined
	  }
	  return true;
	}
	
	this.callbackHandler =  function (func)
	{
		if (window[func])
		{
			window[func].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else { console.log(func + ' is not registered'); }
	}
	
	this.convertchar = function(inp_char)
	{
		if (inp_char=="α") {return "a"}
		else if (inp_char=="ά") {return "a"}
		else if (inp_char=="Α") {return "a"}
		else if (inp_char=="Ά") {return "a"}
		else if (inp_char=="β") {return "b"}
		else if (inp_char=="Β") {return "b"}
		else if (inp_char=="γ") {return "g"}
		else if (inp_char=="Γ") {return "g"}
		else if (inp_char=="δ") {return "d"}
		else if (inp_char=="Δ") {return "d"}
		else if (inp_char=="ε") {return "e"}
		else if (inp_char=="έ") {return "e"}
		else if (inp_char=="Ε") {return "e"}
		else if (inp_char=="Έ") {return "e"}
		else if (inp_char=="ζ") {return "z"}
		else if (inp_char=="Ζ") {return "z"}
		else if (inp_char=="η") {return "i"}
		else if (inp_char=="ή") {return "i"}
		else if (inp_char=="Η") {return "i"}
		else if (inp_char=="Ή") {return "i"}
		else if (inp_char=="θ") {return "th"}
		else if (inp_char=="Θ") {return "th"}
		else if (inp_char=="ι") {return "i"}
		else if (inp_char=="ί") {return "i"}
		else if (inp_char=="ϊ") {return "i"}
		else if (inp_char=="ΐ") {return "i"}
		else if (inp_char=="Ι") {return "I"}
		else if (inp_char=="Ί") {return "I"}
		else if (inp_char=="Ϊ") {return "I"}
		else if (inp_char=="κ") {return "k"}
		else if (inp_char=="Κ") {return "k"}
		else if (inp_char=="λ") {return "l"}
		else if (inp_char=="Λ") {return "l"}
		else if (inp_char=="μ") {return "m"}
		else if (inp_char=="Μ") {return "m"}
		else if (inp_char=="ν") {return "n"}
		else if (inp_char=="Ν") {return "n"}
		else if (inp_char=="ξ") {return "ks"}
		else if (inp_char=="Ξ") {return "ks"}
		else if (inp_char=="ο") {return "o"}
		else if (inp_char=="ό") {return "o"}
		else if (inp_char=="Ο") {return "O"}
		else if (inp_char=="Ό") {return "O"}
		else if (inp_char=="π") {return "p"}
		else if (inp_char=="Π") {return "p"}
		else if (inp_char=="ρ") {return "r"}
		else if (inp_char=="Ρ") {return "r"}
		else if (inp_char=="σ") {return "s"}
		else if (inp_char=="ς") {return "s"}
		else if (inp_char=="Σ") {return "s"}
		else if (inp_char=="τ") {return "t"}
		else if (inp_char=="Τ") {return "t"}
		else if (inp_char=="υ") {return "y"}
		else if (inp_char=="ύ") {return "y"}
		else if (inp_char=="ϋ") {return "y"}
		else if (inp_char=="ΰ") {return "y"}
		else if (inp_char=="Υ") {return "y"}
		else if (inp_char=="Ύ") {return "y"}
		else if (inp_char=="Ϋ") {return "y"}
		else if (inp_char=="φ") {return "f"}
		else if (inp_char=="Φ") {return "f"}
		else if (inp_char=="χ") {return "x"}
		else if (inp_char=="Χ") {return "x"}
		else if (inp_char=="ψ") {return "ps"}
		else if (inp_char=="Ψ") {return "ps"}
		else if (inp_char=="ω") {return "w"}
		else if (inp_char=="ώ") {return "w"}
		else if (inp_char=="Ω") {return "w"}
		else if (inp_char=="Ώ") {return "w"}
		else if (inp_char=="\n") {return ""}
		else if (inp_char==" ") {return "-"}
		else if (inp_char=="!") {return ""}
		else if (inp_char=="@") {return ""}
		else if (inp_char=="#") {return ""}
		else if (inp_char=="$") {return ""}
		else if (inp_char=="%") {return ""}
		else if (inp_char=="^") {return ""}
		else if (inp_char=="&") {return ""}
		else if (inp_char=="*") {return ""}
		else if (inp_char=="(") {return ""}
		else if (inp_char==")") {return ""}
		else if (inp_char=="=") {return ""}
		else if (inp_char==".") {return ""}
		else if (inp_char=="?") {return ""}
		else if (inp_char==";") {return ""}
		else if (inp_char=="?") {return ""}
		else if (inp_char=="<") {return ""}
		else if (inp_char==">") {return ""}
		else if (inp_char=="»") {return ""}
		else if (inp_char=="«") {return ""}
		else if (inp_char==":") {return ""}
		else if (inp_char=="'") {return ""}
		else if (inp_char=='"') {return ""}
		else if (inp_char=='…') {return ""}
		else if (inp_char=='-') {return "-"}
		else if (inp_char==',') {return "-"}
		else if (inp_char=='\\') {return "-"}
		else if (inp_char=="'") {return ""}	
		else return inp_char.toLowerCase();
	}
	
	this.checkFileExtension = function(e,l){ 
			
	}
	
	this.strftime = function(time,fmt){
		var d = new Date(time * 1000);

		Date.ext = {};
		Date.ext.util = {};
		Date.ext.util.xPad = function (x, pad, r) {
		    if (typeof (r) == "undefined") {
		        r = 10
		    }
		    for (; parseInt(x, 10) < r && r > 1; r /= 10) {
		        x = pad.toString() + x
		    }
		    return x.toString()
		};
		Date.prototype.locale = "en-GB";
		if (document.getElementsByTagName("html") && document.getElementsByTagName("html")[0].lang) {
		    Date.prototype.locale = document.getElementsByTagName("html")[0].lang
		}
		Date.ext.locales = {};
		Date.ext.locales.en = {
		    a: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
		    A: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
		    b: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		    B: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		    c: "%a %d %b %Y %T %Z",
		    p: ["AM", "PM"],
		    P: ["am", "pm"],
		    x: "%d/%m/%y",
		    X: "%T"
		};
		
		Date.ext.locales["en-US"] = Date.ext.locales.en;
		Date.ext.locales["en-US"].c = "%a %d %b %Y %r %Z";
		Date.ext.locales["en-US"].x = "%D";
		Date.ext.locales["en-US"].X = "%r";
		Date.ext.locales["en-GB"] = Date.ext.locales.en;
		Date.ext.locales["en-AU"] = Date.ext.locales["en-GB"];
		Date.ext.formats = {
		    a: function (d) {
		        return Date.ext.locales[d.locale].a[d.getDay()]
		    },
		    A: function (d) {
		        return Date.ext.locales[d.locale].A[d.getDay()]
		    },
		    b: function (d) {
		        return Date.ext.locales[d.locale].b[d.getMonth()]
		    },
		    B: function (d) {
		        return Date.ext.locales[d.locale].B[d.getMonth()]
		    },
		    c: "toLocaleString",
		    C: function (d) {
		        return Date.ext.util.xPad(parseInt(d.getFullYear() / 100, 10), 0)
		    },
		    d: ["getDate", "0"],
		    e: ["getDate", " "],
		    g: function (d) {
		        return Date.ext.util.xPad(parseInt(Date.ext.util.G(d) / 100, 10), 0)
		    },
		    G: function (d) {
		        var y = d.getFullYear();
		        var V = parseInt(Date.ext.formats.V(d), 10);
		        var W = parseInt(Date.ext.formats.W(d), 10);
		        if (W > V) {
		            y++
		        } else {
		            if (W === 0 && V >= 52) {
		                y--
		            }
		        }
		        return y
		    },
		    H: ["getHours", "0"],
		    I: function (d) {
		        var I = d.getHours() % 12;
		        return Date.ext.util.xPad(I === 0 ? 12 : I, 0)
		    },
		    j: function (d) {
		        var ms = d - new Date("" + d.getFullYear() + "/1/1 GMT");
		        ms += d.getTimezoneOffset() * 60000;
		        var doy = parseInt(ms / 60000 / 60 / 24, 10) + 1;
		        return Date.ext.util.xPad(doy, 0, 100)
		    },
		    m: function (d) {
		        return Date.ext.util.xPad(d.getMonth() + 1, 0)
		    },
		    M: ["getMinutes", "0"],
		    p: function (d) {
		        return Date.ext.locales[d.locale].p[d.getHours() >= 12 ? 1 : 0]
		    },
		    P: function (d) {
		        return Date.ext.locales[d.locale].P[d.getHours() >= 12 ? 1 : 0]
		    },
		    S: ["getSeconds", "0"],
		    u: function (d) {
		        var dow = d.getDay();
		        return dow === 0 ? 7 : dow
		    },
		    U: function (d) {
		        var doy = parseInt(Date.ext.formats.j(d), 10);
		        var rdow = 6 - d.getDay();
		        var woy = parseInt((doy + rdow) / 7, 10);
		        return Date.ext.util.xPad(woy, 0)
		    },
		    V: function (d) {
		        var woy = parseInt(Date.ext.formats.W(d), 10);
		        var dow1_1 = (new Date("" + d.getFullYear() + "/1/1")).getDay();
		        var idow = woy + (dow1_1 > 4 || dow1_1 <= 1 ? 0 : 1);
		        if (idow == 53 && (new Date("" + d.getFullYear() + "/12/31")).getDay() < 4) {
		            idow = 1
		        } else {
		            if (idow === 0) {
		                idow = Date.ext.formats.V(new Date("" + (d.getFullYear() - 1) + "/12/31"))
		            }
		        }
		        return Date.ext.util.xPad(idow, 0)
		    },
		    w: "getDay",
		    W: function (d) {
		        var doy = parseInt(Date.ext.formats.j(d), 10);
		        var rdow = 7 - Date.ext.formats.u(d);
		        var woy = parseInt((doy + rdow) / 7, 10);
		        return Date.ext.util.xPad(woy, 0, 10)
		    },
		    y: function (d) {
		        return Date.ext.util.xPad(d.getFullYear() % 100, 0)
		    },
		    Y: "getFullYear",
		    z: function (d) {
		        var o = d.getTimezoneOffset();
		        var H = Date.ext.util.xPad(parseInt(Math.abs(o / 60), 10), 0);
		        var M = Date.ext.util.xPad(o % 60, 0);
		        return (o > 0 ? "-" : "+") + H + M
		    },
		    Z: function (d) {
		        return d.toString().replace(/^.*\(([^)]+)\)$/, "$1")
		    },
		    "%": function (d) {
		        return "%"
		    }
		};
		Date.ext.aggregates = {
		    c: "locale",
		    D: "%m/%d/%y",
		    h: "%b",
		    n: "\n",
		    r: "%I:%M:%S %p",
		    R: "%H:%M",
		    t: "\t",
		    T: "%H:%M:%S",
		    x: "locale",
		    X: "locale"
		};
		Date.ext.aggregates.z = Date.ext.formats.z(new Date());
		Date.ext.aggregates.Z = Date.ext.formats.Z(new Date());
		Date.ext.unsupported = {};
		Date.prototype.strftime = function (fmt) {
		    if (!(this.locale in Date.ext.locales)) {
		        if (this.locale.replace(/-[a-zA-Z]+$/, "") in Date.ext.locales) {
		            this.locale = this.locale.replace(/-[a-zA-Z]+$/, "")
		        } else {
		            this.locale = "en-GB"
		        }
		    }
		    var d = this;
		    while (fmt.match(/%[cDhnrRtTxXzZ]/)) {
		        fmt = fmt.replace(/%([cDhnrRtTxXzZ])/g, function (m0, m1) {
		            var f = Date.ext.aggregates[m1];
		            return (f == "locale" ? Date.ext.locales[d.locale][m1] : f)
		        })
		    }
		    var str = fmt.replace(/%([aAbBCdegGHIjmMpPSuUVwWyY%])/g, function (m0, m1) {
		        var f = Date.ext.formats[m1];
		        if (typeof (f) == "string") {
		            return d[f]()
		        } else {
		            if (typeof (f) == "function") {
		                return f.call(d, d)
		            } else {
		                if (typeof (f) == "object" && typeof (f[0]) == "string") {
		                    return Date.ext.util.xPad(d[f[0]](), f[1])
		                } else {
		                    return m1
		                }
		            }
		        }
		    });
		    d = null;
		    return str
		};
		
		return d.strftime(fmt);
	}//END DATETIME
	
	this.moneyFormat = function (num,c,d,t) {
			//(123456789.12345).formatMoney(2, '.', ',');	
			if (typeof num == 'undefined' || typeof num == 'null') { return ; }	
		Number.prototype.formatMoney = function(c, d, t){
		var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 	};	
	 	
	 	return num.formatMoney(2, '.', ',');
	}
	
	
			this.app = {
			options : {
				callBack: ''		
			},
			parseAction : function(tag,ret) {
//				var p = tag.match(/[#!/]+([a-zA-Z]+)/i,'');
				var u = new RegExp("(\#!/)+([a-zA-Z]+)")
				var p = tag.match(u,'');
				if (p != null) {
					if (ret) { return p[1]; }			
					return p;// JUST THE ACTION
				}
			},
			parseParams : function(string) {
				var e = this.parseAction(string,1);		
				var params = string.match(/[^/]+/g);
				if (params != null) {
					var x = {};	
					$.each(params,function(k,v){
						if (k%2 ==0) {
							x[v] = v;
							prev = v;
						}
						else {
							x[prev] = v;
						}
					});
					return x;
				}
				return this; // Just in case
			},
			parse : function(tag,options) {
				var settings = $.extend(this.options,options);
				var ret = {};
				var o = this.parseAction(tag);
				if (o != null) {
					ret['action'] = o[2];
					ret['params'] = this.parseParams(tag.replace(o[0],''));
				}
				if (settings.callBack) {
					this.callbackHandler(settings.callBack,ret);
				}
				return ret;
			},
			callbackHandler : function (func)
			{
				if (this[func])
				{
					this[func].apply(this, Array.prototype.slice.call(arguments, 1));
				}
				else { 
					console.log(func + ' is not registered'); 
				}
				return this; // Just in case
			}
		}// END Inner Class
	
};//END CLASS

return this.each(function() {

	var element = $(this);
	// Return early if this element already has a plugin instance
	if (element.data('mcms')) return;
	var c = new mcms(this,options);
	element.data('mcms', c);
});

}
