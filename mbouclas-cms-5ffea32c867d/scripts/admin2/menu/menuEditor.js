//var debug = "&start_debug=1&debug_host=10.11.11.65&debug_stop=1";
head.ready(function () {

    var db = new mcms.dataStore({
        method: "POST",
        url: "",
        dataType: "json",
        trigger: "loadComplete"
    });


    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/ajax/loader.php",
                dataType: "json",
                data: {
                    file: 'menus/menuEditor.php',
                    action: 'ViewMenu'
                }
            },
            destroy: {
                url: "/ajax/loader.php",
                dataType: "json",
                data: {
                    file: 'menus/menuEditor.php',
                    action: 'deleteMenu'
                }
            },
            update: {
                url: "/ajax/loader.php?file=menus/menuEditor.php&action=updateMenu",
                dataType: "json",
                type: "POST"
            },
            create: {
                url: "/ajax/create_users.php",
                dataType: "jsonp"
            },
            parameterMap: function (data, option) {

                if (option == "destroy") {
                    var str = [];
                    $.each(data, function (index, value) {
                        if (!(value instanceof Object)) {
                            str.push(index + "=" + value);
                        }
                    });
                    str.push("id=" + data.models[0].id);
                    return str.join('&');
                } else {
                    
                    return data;
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { editable: false },
                    vars: {},
                    title: {},
                    num_items: { editable: false },
                    active: "active"
                }
            }
        },
        batch: true
    });


    var grid = $("#grid").kendoGrid({
        dataSource: dataSource,
        height: "365px",
        columns: [
     			{ field: "id", title: "#ID", width: "50px" },
     			{ field: "vars", title: $.lang.lbl_varName },
     			{ field: "title", title: $.lang.title, template: '<a href="menuEditor.php?action=modify&id=${id}" >${title}</a>' },
                { field: "num_items", title: $.lang.items, width: "50px" },
                { field: "active", title: $.lang.availability, width: "80px", editor: categoryDropDownEditor, template: function (data) { return (data.active == "1") ? $.lang.yes : $.lang.no; } },
                { command: ["edit", "destroy"], title: " ", width: "210px"}],
        editable: "inline"
    }).data('kendoGrid');


    function categoryDropDownEditor(container, options) {
        
        $('<input data-text-field="active" data-value-field="value" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: [{ id: 1, active: "Ναι", value: 1 }, { id: 2, active: "Οχι", value: 0}]
                        });

    }


    $(db).bind('loadComplete', function () {

        records = db.records;
        grid.dataSource.data(records);
        grid.dataSource.read();
        $('#NewMenus')[0].reset();


    });
    
    
    var validatable = $("#NewMenus").kendoValidator().data("kendoValidator");

    $(".button").live('click', function (e) {

    	
        e.preventDefault();
        searchForm = $("#NewMenus");

        db.url = "/ajax/loader.php?file=menus/menuEditor.php&action=AddMenus";
        db.trigger = 'loadComplete';
        db.data = {
            'data': new mcms.formData(searchForm.find('.menu'), {
                'ReturnType': 'array'
            })
        };

        if (validatable.validate()) {
        		$(db).loadStore(db);
        }
        
    });




    $("#treeview").kendoTreeView({
        dragAndDrop: true
    });



    $('.editMenuItem').live('click', function (e) {
        e.preventDefault();
        $.fn.colorbox({ iframe: true, innerWidth: 900, innerHeight: 700, href: '/ajax/menuEditor.php?action=modifyMenuItem&id=' + $(this).attr('rel') });
    });




    $('#SearchContainerModal').live('click', function (e) {
        $.fn.colorbox({ href: $('#' + $(this).attr('rel')), width: 950, height: 600, inline: true });
    });


    $('#saveMenuItem').click(function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $.ajax({
            url: "/ajax/menuEditor.php?action=addMenuItem",
            async: false,
            type: 'POST',
            data: { 'data': new $.FormData(form.find('.menuItem'), { 'ReturnType': 'array' })
            },
            cache: false,
            success: function (data) {
                //					$('.debugArea').html('<pre>' + data + '</pre>');
                addMenuItem(data, '#menuItems');
                $.fn.colorbox.close();
            }
        }); //END AJAX 
    }); //END FUNCTION

    $('.saveMenuItem').live('click', function (e) {
        e.preventDefault();
        var el = $(this);
        var form = $(this).closest('form');
        $.ajax({
            url: "/ajax/menuEditor.php?action=addMenuItem&" + $(this).attr('rel'),
            async: false,
            type: 'POST',
            data: {
        },
        cache: false,
        success: function (data) {
            //					$('.debugArea').html('<pre>' + data + '</pre>');
            el.parent().parent().remove();
            addMenuItem(data, '#menuItems');
            //					$.fn.colorbox.close();
        }
    }); //END AJAX 
}); //END FUNCTION

$('#editMenuItem').click(function (e) {
    e.preventDefault();
    var form = $(this).closest('form');
    $.ajax({
        url: "/ajax/menuEditor.php?action=editMenuItem",
        async: false,
        type: 'POST',
        data: { 'data': new $.FormData(form.find('.menuItem'), { 'ReturnType': 'array' })
        },
        cache: false,
        success: function (data) {
            //					$('.debugArea').html('<pre>' + data + '</pre>');
            var obj = jQuery.parseJSON(data);
            parent.$('#title_' + obj.id).html(obj.title);
            parent.$.fn.colorbox.close();

        }
    }); //END AJAX 
}); //END FUNCTION


$('.deleteMenuItem').live('click', function (e) {
    e.preventDefault();
    var el = $(this);
    $.ajax({
        url: "/ajax/menuEditor.php?action=deleteMenuItem&id=" + $(this).attr('rel'),
        async: false,
        type: 'POST',
        data: {
    },
    cache: false,
    success: function (data) {
        el.parentsUntil('li').parent().remove();
        $('.debugArea').html('<pre>' + data + '</pre>');
    }
}); //END AJAX 
});
$('.listContainer').hover(
  function () {
      $(this).css("border-color", "red")
  },
  function () {
      $(this).css("border-color", "black")
  }
);

//END FUNCTION

$('.getCategories').click(function () {
    $.ajax({
        url: "/ajax/menuEditor.php?action=getCategories&" + $(this).attr('rel'),
        async: false,
        type: 'POST',
        data: {
    },
    cache: false,
    success: function (data) {
        $('#categoriesTable').html(data);

        //     $('.debug').html(data);

    }
}); //END AJAX 
});

});     //END Head
 
	function arraied (obj)
	{
		return obj.nestedSortable('toArray', {startDepthCount: 0});
	}
	
	function dump(arr,level) {
		var dumped_text = "";
		if(!level) level = 0;
 
		//The padding given at the beginning of the line.
		var level_padding = "";
		for(var j=0;j<level+1;j++) level_padding += "    ";
 
		if(typeof(arr) == 'object') { //Array/Hashes/Objects
			for(var item in arr) {
				var value = arr[item];
 
				if(typeof(value) == 'object') { //If it is an array,
					dumped_text += level_padding + "'" + item + "' ...\n";
					dumped_text += dump(value,level+1);
				} else {
					dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
				}
			}
		} else { //Stings/Chars/Numbers etc.
			dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
		}
		return dumped_text;
	}

function addMenuItem (options,target) {
	var obj = jQuery.parseJSON(options);
//	console.log(obj)
	$('#Tmp').html($('#TemplateMenuItem').html()); 
	$('#Tmp').render([
   		{'ID': obj.id, 'TITLE': obj.title,'ITEMID':obj.itemid }
   ]); 
	$(target).append($('#Tmp').html());
	
}