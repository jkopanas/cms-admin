//var debug = "&start_debug=1&debug_host=10.11.11.65&debug_stop=1";	
head.ready(function(){

	
	var db = new mcms.dataStore({
    	method: "POST",
    	url:"",
    	dataType: "json",
 		trigger:"loadComplete"
	});
	
    var tree = [];

	url="/ajax/loader.php?file=menus/menuEditor.php&action=ShowTree&id="+$("#menuid").val();
    kendos = { dragAndDrop: true,  dragend: dragEnd, select: edit,hint:function(){ console.log($(this)); return $(this).clone().css("opacity","0.1"); }, template: kendo.template($("#id-templatet").html()) }
    var treeView = $("#treeview").treeview({ kendo: kendos , source: url }).data('tree');
	treeView.init();
	  			 
	 $.each($('.items'),function(index,value){
                      		var p= ($(value).attr("data-parent") == 0 ) ? 'root' : $(value).attr("data-parent"); 
                      		id=$(value).attr("id");
                     		var ids=id.split("_");
                      		tree.push({ id: ids[1] ,level: $(value).attr("data-level") , text: "list["+ids[1]+"]="+p } );
	});
	
	 $(db).bind('loadTabs',function(){
	 	
	 		records = db.records;
	 		$.each(records,function(index,value) {
	 			$('#'+value.box.name).html(value.template);
				boxes[value.box.name] = false;
	 		});
	 		
		
			var x = true;
			for (i in boxes) {
				if (boxes[i] == false) {
					x = false;
				}
			}

	 });
	 
		
		  function dragEnd(e) {
		  	
		  				if (e.dropPosition == "over" ) {
		  				    id=$(e.destinationNode).children("div").find("span:last").attr("id");
                            var ids=id.split("_");
                            $(e.sourceNode).find("span:last").attr("data-parent",ids[1]);
		  				} else {
		  					 id=$(e.destinationNode).attr('id');
		  					 var ids=id.split("_");
		  					 id= ids.pop();	
		  					var p= ($("#list_"+id).attr("data-parent") == 0 ) ? 'root' : $("#list_"+id).attr("data-parent"); 
		  					 id=$(e.sourceNode).attr('id');
		  					 var ids=id.split("_");
		  					 id= ids.pop();
		  					$("#list_"+id).attr("data-parent",p);
		  				}
		  				var str = [];
		  				tree = new Array();
		  				var data = new Array();
		  			//	console.log($('.items'));
		  		 	  $.each($('.items'),function(index,value){
		  		 	  
                      		var p = ($(value).attr("data-parent") == 0 ) ? 'root' : $(value).attr("data-parent"); 
                      		id=$(value).attr("id");
                     		var ids=id.split("_");
                      		tree.push({ id: ids[1] , text: "list["+ids[1]+"]="+p } );
                      		str[index] = "list["+ids[1]+"]="+p;
                      		
                      		///data[index]['depth']	= $(value).attr("data-parent");
                      		
          						data[index] = {};
          						data[index].item_id	= ids[1];
          						data[index].depth = $(value).attr("data-level");
								data[index].parent_id = p;
                      		
                      });
                      
               			db.url="/ajax/loader.php?file=menus/menuEditor.php&action=saveState";
		 				db.trigger ='saveState';
						db.data = { data: data , serial: str.join("&") };
		  				
		  				$(db).loadStore(db);
		  				          
		  }
			
		  
		  
		   $("#panelbar").kendoPanelBar({expandMode: "single"});
		 
		  
		   
		 var boxes = {};var  main = {};
		 $('.boxItem').each(function(){
			boxes[$(this).attr('id')] = false;	
		 });
		 
		

	 var ar =[];
	 $.each($(".AllBox"),function(index,value) {
	 	ar.push("boxID[]="+$(value).data('box'));
	 	
	 });

	 db.url = "/ajax/loader.php?module=content&action=LoadBox&"+ar.join("&");
	 db.trigger = 'loadTabs';
	$(db).loadStore(db);



		$(db).bind('editMenu',function(){
			records = db.records;
			$("#list_"+records.id).html(records.title);
			wnd.data("kendoWindow").close();
			 
		});
                
                	
		 $(db).bind('addMenu',function(){
			  records = db.records;

			  var parent_li= [];
	     		var parent_ul = $("#treeview").find("ul:first");

    				if ($.isArray(records.itemid)) {
    					$.each(records.itemid,function(index,value){
    						obj= { id: records.id[index], text: records.title[index], parentid: 0, level: 1 };
    				 		id = parent_ul.find("li:last").attr('id');
    		  				treeView.append(obj, parent_ul );	
    		  				$("#treeview").find("li:last").attr('id',"parent_list_"+records.id[index]);					
    					});
    					if (records.subs.itemid) {
    						
    						$.each(records.subs.itemid,function(index,value){
    							obj= { id: records.subs.id[index], text: records.subs.title[index], parentid: records.subs.parentid[index] , level: 2 };		
    		  					treeView.append(obj, $("#parent_list_"+records.subs.parentid[index]) );
    		  					$("#list_"+records.subs.id[index]).parents("li:first").attr('id',"parent_list_"+records.subs.id[index]);		
    						});
    						
    					}
    					
    				} else {
    					
    					obj= { id: records.id, text: records.title, parentid: 0, level: 1 };
    		  			treeView.append(obj, parent_ul );
    		  			$("#treeview").find("li:last").attr('id',"parent_list_"+records.id);
    		  			
    				}
    		  
		});
		
		
		$(db).bind('deleteMenu',function(){
			records = db.records;
    		treeView.remove($("#parent_list_"+records.id));
		});
	
		 $('input[name=title]').live("keyup", function(){	
    		 var txt ='';
    		 var texto = $(this).val();
    		 for (var i=0; i < texto.length; i++) {
    		     txt+=mcms.convertchar(texto.charAt(i))
    		 }
    		    $('input[name=permalink]').val(txt);
    		});
		

		function edit (e) {
			e.preventDefault();
			
		}
		
		 var wnd = $("#edit_form");
		 
		$('.edit').live('click', function(e){
				var validatable = $("#menuItemDetails-menuItem").kendoValidator().data("kendoValidator");
			   if (!wnd.data("kendoWindow")) {
                        wnd.kendoWindow({
                                width: "500",
                                height: "440",
                                title : "Προσθήκη / Επεξεργασία  menu",
                                content : "/ajax/loader.php?file=menus/menuEditor.php&action=modifyMenuItem&id=" + $(this).data('id'),
                                visible : true,
                                modal : true
                        });
                        wnd.data("kendoWindow").open();
                        wnd.data("kendoWindow").center();
                } else {
                	
                        wnd.data("kendoWindow").refresh({
                               url : "/ajax/loader.php?file=menus/menuEditor.php&action=modifyMenuItem&id=" + $(this).data('id')
                        });
                        wnd.data("kendoWindow").open();
                        wnd.data("kendoWindow").center();       
                        
                }
			
			
		});
		
		
		
		 var validatable_menuItem = $("#menuItemDetails-addItem").kendoValidator().data("kendoValidator");
		 var validatable = $("#menuItemDetails-menuItem").kendoValidator().data("kendoValidator");

		    


   		$(".delete-confirm,.delete-cancel").live('click',function() {
   	
   					if ($(this).hasClass("delete-confirm")) {
   						var id=$(this).data('id');
            			db.url = "/ajax/loader.php?file=menus/menuEditor.php&action=deleteMenuItem&id=" + $(this).data('id');
						db.trigger = 'deleteMenu';
						$(db).loadStore(db);
                    }
                  
                    kendoWindow.close();
                    
        });
        
        
        var kendoWindow = $("#delete-confirmation").kendoWindow({
                title: $.lang.menudelete,
                resizable: false,
                visible: false,
                width:"180",
                modal: true
            }).data("kendoWindow");
        
		
		$('.delete').live('click', function(e){
			
			e.preventDefault();
			var id = $(this).data('id');
			
			var templates = kendo.template($("#delete-confirmation_menu").html());	    
			
			kendoWindow.content(templates({ item: id }));
			kendoWindow.center().open();
	
           
		});
		
	
$('.saveGrid').live('click', function(e){
		e.preventDefault();
		url = "module=" + $(this).data('module') + "&itemid=" + $(this).data('itemid') + "&type="+ $(this).data('type') +"&menuid=" + $("#menuid").val();
		db.url = "/ajax/loader.php?file=menus/menuEditor.php&action=addMenuItem&" + url;
		db.trigger = 'addMenu';
		$(db).loadStore(db);
});//END FUNCTION


$('.addLink').live('click', function(e){
	
	e.preventDefault();
	db.url = "/ajax/loader.php?file=menus/menuEditor.php&action=addMenuItem";
	db.data = { 'data': new mcms.formData($('.addItem'),{'ReturnType':'array'}), 'menuid': $("#menuid").val()  };
	db.trigger = 'addMenu';
	
	if (validatable_menuItem.validate()) {
		$(db).loadStore(db);
		$(this).closest('form').find("input[type=text], textarea").val("");
	}

});



$('.TranslateMenu').live('click', function(e){
	
	e.preventDefault();
	db.url = "/ajax/loader.php?file=menus/menuEditor.php&action=addMenuItem";
	db.data = { 'data': new mcms.formData($('.addItem'),{'ReturnType':'array'}), 'menuid': $("#menuid").val()  };
	db.trigger = 'addMenu';
	
	if (validatable_menuItem.validate()) {
		$(db).loadStore(db);
		$(this).closest('form').find("input[type=text], textarea").val("");
	}

});

var wndr = $('#OpenLangWnd').kendoWindow({
                width: "650",
                visible: false,
                content: '',
                modal: true
            }).data("kendoWindow");

            
            
 	
	$(db).bind('loadlang',function(){

		$('#OpenLangWnd').html(db.records);
	
        wndr.center().open();
	});
	
	$(dbLanguageBox).bind('returnlang',function() {
        wndr.close();
	});


$(".TranslateMenuItem").live('click',function() {
	    
		data = {
			id: $(this).data('id'),
			action: "edit",
			table: "menus_items",
			field: "title",
			groupby: 'field',
			tpl: 'admin2/menu/menu_lang.tpl'
		}

		db.url="/ajax/loader.php?file=menus/menuEditor.php&action=loadSingleTranslation";
		db.trigger = 'loadlang';
		db.data = data;
		db.dataType='';
		$(db).loadStore(db);
	
});






$("#syncWithOriginal-menuItem").live('click',function(){

	var c = this.checked ? 1 : 0; 
	$("#settings_syncWithOriginal-menuItem").val(c);

});

$("#syncWithOriginal-addItem").live('click',function(){

	var c = this.checked ? 1 : 0;  
	$("#settings_syncWithOriginal-addItem").val(c);
	
});

	$('.editMenuItem').live('click',function(e){

		e.preventDefault();

		db.url = "/ajax/loader.php?file=menus/menuEditor.php&action=editMenuItem";
		db.data = { 'data': new mcms.formData($('.menuItem'),{'ReturnType':'array'}), 'id': $(this).data('id')  };
		db.trigger = 'editMenu';
		if ($("#menuItemDetails-menuItem").kendoValidator().data("kendoValidator").validate()) {
			$(db).loadStore(db);
		}
	
	});//END FUNCTION


		$('#total_check').live('click', function(e){
			var checkbox = $(this);
			grid = $("#"+$(this).data('id')).kgrid().data('grid');
			var el = grid.ReturnGrid();

			var table = el.wrapper.find('.messages');

			if (checkbox.prop('checked') == true)
			{
				el.tbody.find('.check-values').attr('checked',true);
				el.tbody.find('.check-values').parentsUntil('TR').parent().addClass('row_selected');
				table.html(el.tbody.find('.check-values:checked').length);

			}
			else 
			{
				el.tbody.find('.check-values').attr('checked',false);
				el.tbody.find('.check-values').parentsUntil('TR').parent().removeClass('row_selected');
				table.html(el.tbody.find('.check-values:checked').length);
			}
		});
		
		
		$('.check-values').live('click', function(e){
			var checkbox = $(this);

			 grid = $("#"+$(this).parents('table').attr('id')).kgrid().data('grid');
			var el = grid.ReturnGrid();
			var table = el.wrapper.find('.messages');
			var row = checkbox.parentsUntil('TR').parent();
			if (checkbox.prop('checked') == true)
			{
				table.html(el.tbody.find('.check-values:checked').length);
				row.addClass('row_selected');
			}
			else 
			{
				table.html(el.tbody.find('.check-values:checked').length);
				row.removeClass('row_selected');
			}	
		});

	
		
	$(".ToolBarActions").live('click', function(e){
			e.preventDefault();
			var BatchAction = new Array;
			
			var module='';
			$.each($("#tabStrip").find('li'),function(index,value) {
				if ($(value).hasClass('k-state-active')) {
					module=$(value).data('module');
				}
			});
			
			grid = $("#gridsview_"+module).kgrid().data('grid');
			el =grid.ReturnGrid();
			el.tbody.find('.check-values:checked').each(function(k,v){
				BatchAction.push($(this).val());
			});
			
			if (BatchAction.length >= 1 ) {
				e.preventDefault();
				
				url = "module=" + module + "&itemid=1&type="+ $(this).data('type') +"&subs="+$(this).data('subs')+"&menuid=" + $("#menuid").val();
				db.url = "/ajax/loader.php?file=menus/menuEditor.php&action="+$(this).data('action')+"&" + url;
				db.trigger = $(this).data('trigger');
      			db.data = {
      				checkboxes: BatchAction
      			}
      			$(db).loadStore(db);	
			}
			
	});
	



//END FUNCTION
});//END Head