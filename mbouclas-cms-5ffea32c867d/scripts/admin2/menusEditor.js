/**
* Known Bugs :
* - Delete does not delete all children from DB if you delete a parent
*
*/

function echo(a) {
	console.log(a);
}
var cache = {};
head.ready(function(){
var sortObj = $('ol.sortable');
$("#menu").kendoMenu();
var itemsTemplate = kendo.template($("#itemsResults").html());
var categoriesTemplate = kendo.template($("#categoriesResults").html());
var menuTemplate = kendo.template($("#menuTemplate").html());
var validatable = {};

$(".toValidate").each(function(k,v){
	validatable[$(this).attr('id')] = $(this).kendoValidator().data("kendoValidator");
});

var url = "/ajax/loader.php?file=menuEditor.php";
var dbDefaults = {    method: "POST",
    url:"/ajax/loader.php?file=menuEditor.php",
    dataType: "json",
	trigger:"loadComplete"
};
db = new mcms.dataStore(dbDefaults);

var sortableOptions = {
			disableNesting: 'no-nest',
			forcePlaceholderSize: true,
			handle: 'div.box_c_heading',
			items: 'li',
			opacity: .6,
			placeholder: 'placeholder',
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div',
			stop: function(event, ui) {
				updateState($(this));
			}
};


$('.wToogle').live('click', function(e){
	$(this).parents('.listContainer').find('.box_c_content').toggle('fast');
});


$(db).bind('updateState',function(){
//	$('.debugArea').html('<pre>' + db.records + '</pre>');
	saved();
});


$('.editMenu').bind('click',function(e){
	e.preventDefault();
	$('#newMenu').toggle('fast');
});

$('.saveForm').bind('click',function(e){
	e.preventDefault();
	$('#menuItem').trigger('submit');
});

$('#menuItem').bind('submit',function(e){
	e.preventDefault();

});

$('.mAccordion h4').bind('click',function(e){
	e.preventDefault();
	$(this).parents('.mAccordion').find('.sub_section').hide();
	$(this).parents('.micro').find('.sub_section').show();
});

$('.tabsS a').bind('click',function(e){
	e.preventDefault();
	$('.tabsS a').removeClass('current');
	$(this).addClass('current');
	$('.tab').hide();
	$('#tab-'+$(this).attr('rel')).show();
	
});


var latestItems = $.parseJSON($('textarea[name=latestItems]').val());
var menuItems = $.parseJSON($('textarea[name=menuItems]').val());
var categoriesFlat = {};//using this to compare with the items to add. Otherwise we can't add a subcat cause there is no direct reference
drawLatest(latestItems);
addToMenu(menuItems);
sortObj.nestedSortable(sortableOptions);


$('.wRemove').live('click', function(e){
	var r=confirm($.lang.u_sure);
	if (r==true)
    {
//    	$(this).parents('.listContainer').parent().children().remove();
		$(this).parents('.listContainer').parent().remove();

		db.url = url + '&action=deleteMenuItem&id='+$(this).attr('data-id');
		db.trigger = 'itemDeleted';
		$(db).loadStore(db);
		
    }
});


$('.addItems').bind('click',function(e){
	e.preventDefault();
	var me = $(this);
	var type = $(this).attr('data-type');
	var id = $(this).attr('data-id');
	var obj = new Array;
	var tmp = {};
	var daddys = new Array;
	var poke = '';
	if (type != 'custom') {
		tmp = mcms.formData($(this).parents('.micro-content').find('ul li input:checked'),{ getData:true});
		for (var i ='' in tmp) {
			tmp[i]['type'] = me.attr('data-typeAbbr');

			if (type == 'categories') {
				tmp[i] = mapMenuObj(tmp[i]);
				var current = $('#'+tmp[i]['type']+'-'+tmp[i]['id']).parent().parent();
				tmp[i]['menuid'] = current.attr('data-menuid');
				tmp[i]['parentid'] = current.attr('data-parentid');
				obj.push(tmp[i]);
				var dad = $(this).parents('.micro-content').find('ul #'+tmp[i]['type']+'-'+tmp[i]['id']).parent().parent();
				if (dad.children('ol').length > 0)
				{
//					$.extend(recurseCategories(dad,{type:me.attr('data-typeAbbr')}),obj);
					var childs = recurseCategories(dad,{type:me.attr('data-typeAbbr')});
					if (childs.length > 0) {
						for (o in childs) {
							obj.push(childs[o]);
						}
					}
				}
			}
			
			
//			$(this).parents('.micro-content').find('ul #'+tmp[i]['type']+'-'+tmp[i]['id']).parent().remove();
			
		}
/*		var r=confirm('The following have children, add them too?\n'+ daddysChildren);
		if (r==true)// now collect and push the children into our array. We need to make sure we keep the depth
   		 {

   		 }//END CONFIRM*/

		addToMenu(obj,1,me.attr('data-module'));
	}
	else {//custom link
		
	}
});


function recurseCategories(dad,ext) {
	var kids = dad.find('li');
	var par = {};
	var ret = new Array;
	kids.each(function(k,v){
			par[$(v).attr('data-id')] = {id:$(v).attr('data-id'),menuid:$(v).attr('data-menuid'),parentid:$(v).attr('data-parentid'),level:$(v).attr('data-level')};
			var u = new Array;
			u.push($(v).attr('data-id'));
			$(v).parents('li').each(function(o,p){
				u.push($(p).attr('data-id'));
			});
			u.reverse();
			par[$(v).attr('data-id')]['menuid_path'] = u.join('/');
			par[$(v).attr('data-id')] = $.extend(par[$(v).attr('data-id')],ext);
			
	});

	for (var j=0 in par) {
		ret.push(par[j]);
	}
	return ret;
}//End function

function mapMenuObj(obj) {//used for categories to map the fields
	return $.extend(obj,{title:obj.category,permalink:obj.alias,id:obj.categoryid});
}

function drawLatest(obj) {
	
	$('.tab').each(function(k,v){
		$(this).find('.itemsResults ul').append(itemsTemplate(obj[$(this).attr('data-id')]['items']));
		$(this).find('.categoriesResults ul').append(renderCategoriesTree(obj[$(this).attr('data-id')]['categories']));
//		flattenCategories($(this).attr('data-id'),obj[$(this).attr('data-id')]['categories']);
	});
}

function flattenCategories(arr,obj) {
	if (typeof categoriesFlat[arr] == 'undefined') {
		categoriesFlat[arr] = {};
	}
	for (i in obj) {
		if (typeof categoriesFlat[arr][obj[i]] == 'undefined') {
			categoriesFlat[arr][obj[i]] = {};
		}
		categoriesFlat[arr][obj[i]['categoryid']] = obj[i];
		if (typeof obj[i]['subs'] == 'object') {
			flattenCategories(arr,obj[i]['subs']);
		}
	}
}

function renderCategoriesTree(obj,level) {
	var render ='';
	level = (level == null) ? 0 : level;
	for (i in obj) {
		obj[i]['level'] = level;
		render += categoriesTemplate(obj[i]);
		if (typeof obj[i]['subs'] == 'object') {
			render += '<ol data-level="'+level+'">' + renderCategoriesTree(obj[i]['subs'],level+1) + '</ol>';
		}
	}
	return render;
}

function addToMenu(obj,save,module,target) {
	if (save) {
		db.url = url + '&action=addMenuItem&module='+module;
		db.data = {data :obj};
		db.trigger = 'newItem';
		if (target != null) {
			db.target = target;
		}
		$(db).loadStore(db);
		return;
	}
	if (target == null) {
		$('#menuItems').append(drawMenu(obj));
	}
	else {
		target.append(append(drawMenu(obj)));
	}
	sortObj.nestedSortable(sortableOptions);
}

function updateState(el){
	var a = arraied(el);
	db.url = url + '&action=saveState';
	db.data = { 'data': a,'serial':el.nestedSortable('serialize') };
	db.trigger = 'updateState';
	$(db).loadStore(db);
}

function drawMenu(obj,level) {
	var render ='';
	level = (level == null) ? 0 : level;
	for (i in obj) {
		obj[i]['level'] = level;
		render += menuTemplate(obj[i]);
		if (typeof obj[i]['subs'] == 'object') {
			render += '<ol data-level="'+level+'">' + drawMenu(obj[i]['subs'],level+1) + '</ol>';
		}
	}
	return render;
}



function arraied (obj)
{
	return obj.nestedSortable('toArray', {startDepthCount: 0});
}


$(db).bind('newItem',function(){
	
	addToMenu(db.records);
});

var autoComplete = new mcms.dataStore({    
	method: "POST",
    url:'',
    data:{},
    dataType: "json",
	trigger:"loadComplete"
});
	var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
	    clearTimeout (timer);
	    timer = setTimeout(callback, ms);
	  };
	})();
	
$('.quickFilter').bind('click',function(e){
	e.preventDefault();
	performQuickSearch($(this).parents('.mAccordion').find('.filter'));
});

$('.filter').bind('keyup',function(e){
performQuickSearch($(this));
});

function performQuickSearch(obj) {
	var source = obj.parents('.mAccordion').find('.itemsResults ul');
	if (typeof cache[obj.attr('data-module')] == 'object' && obj.val() in cache[obj.attr('data-module')]) {
		drawResults(source,cache[obj.attr('data-module')][obj.val()]);
		return;
	}
	
	var me = obj;
	var filter = {};
	var filters = new Array;
	var v = mcms.formData(me.parents('.mAccordion').find('.SearchData'),{ getData:true});
	if (v.substring) {
		filter.filter = {};
		filter['filter']['filters'] = [{field:'q',operator:'contains','logic':'and',value:v.substring['value']}];
	}
	if (v.categoryid) {
		filter.categoryid =  v.categoryid['value'];
		filter.search_in_subcategories =  1;
	}
	autoComplete.source = source;
	autoComplete.query = obj.val();
	autoComplete.module = obj.attr('data-module');

		delay(function(){
			autoComplete.url = "/ajax/loader.php?file=itemsSearch.php&module=" + me.attr('data-module') + "&action=pagesFilter";
			autoComplete.data = filter;
			$(autoComplete).loadStore(autoComplete);
		}, 400 );
	
		
}

$(autoComplete).bind('loadComplete',function(){
	var res = autoComplete.records;
	if (autoComplete.query.length > 0) {
		if (typeof cache[autoComplete.module] == 'undefined') {
			cache[autoComplete.module] = {};
		}
		cache[autoComplete.module][autoComplete.query] = res.results;
	}
	drawResults(autoComplete.source,res.results);
});

function drawResults(target,obj) {
	target.empty().append(itemsTemplate(obj));
}

$('.saveItemSettings').bind('click',function(e){
	e.preventDefault();
	var v = mcms.formData($('.saveData'),{ getData:true});
	db.url = url + '&action=editMenuItem';
	db.data = v;
	db.dataType = 'html';
	db.trigger = 'updateItems';
	$(db).loadStore(db);
});


$(db).bind('updateItems',function(){
	$('.debug').html(db.records);
});

$('.saveCustomItem').bind('click',function(e){
	e.preventDefault();
	if (validatable[$(this).parents('form').attr('id')].validate()) {
		var v = mcms.formData($(this).parents('form').find('.customSave'),{ getData:true});
		var obj = {};
		for (i in v) {
			obj[i] = v[i]['value'];
		}
		obj['type'] = $(this).attr('data-type')
		obj['menuid'] = $(this).attr('data-menuid')
		addToMenu({0:obj},1,$(this).attr('data-module'));
	}
});


$(db).bind('itemDeleted',function(){
//	$('.debugArea').html('<pre>' + db.records + '</pre>');

	saved();
});


$('.addMenu').bind('click',function(e){
	e.preventDefault();
	$('#newMenu').show();
});

$('.savMenu').bind('click',function(e){
	e.preventDefault();
	$('#NewMenus').trigger('submit');
});

$('#NewMenus').bind('submit',function(e){
	e.preventDefault();
	if (validatable['NewMenus'].validate()) {
		var v = mcms.formData($('.menu'),{ getData:false});
		db.url = url + '&action=addMenu';
		db.data = v;
		db.trigger = 'menuSaved';
		$(db).loadStore(db);
	}
});

$(db).bind('menuSaved',function(){
	if (db.records.action == 'add') {
		window.location.href = 'menus.php?action=modify&id='+ db.records.id;
	}//redirect
	else {//update
		saved();
	}
	
});


$('.deleteMenu').bind('click',function(e){
	e.preventDefault();
	var r=confirm($.lang.u_sure);
	
	if (r==true)
    {
		db.url = url + '&action=deleteMenu&id='+$(this).attr('rel');
		db.trigger = 'menuDeleted';
		$(db).loadStore(db);
    }
});

$(db).bind('menuDeleted',function(){
	window.location.href = 'menus.php'
});



});//END HEAd