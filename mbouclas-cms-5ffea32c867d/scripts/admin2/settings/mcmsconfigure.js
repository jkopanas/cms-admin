var configSetUrl = "/ajax/loader.php?file=settings/mcmsconfigure.php";
head.ready(function() {
	var sortableList = {}; 
	var configSetDataSource = new kendo.data.DataSource({
		batch: false,
		serverPaging : true,
		serverSorting : true,
		serverFiltering : true,
        transport: {
            read:  {
                url: configSetUrl,
            },
            update: {
                url: configSetUrl,
            },
            destroy: {
                url: configSetUrl,
            },
            create: {
                url: configSetUrl,
            },
            parameterMap: function(data, option) {
            	if (option == "read") {
					return data;
				}
            	if (option == "update") {
            		var tmpArr = {};
            		tmpArr.model = {};
            		tmpArr.model = data;
            		tmpArr.id = data['name'];
            		//tmpArr.action = "update";
            		tmpArr.module = $("[name=current_module]").val();
					return tmpArr;
				}
            	if (option == "create") {
            		var tmpArr = {};
            		tmpArr.model = {};
            		tmpArr.model = data;
            		tmpArr.id = data['name'];
            		//tmpArr.action = "update";
            		tmpArr.module = $("[name=current_module]").val();
					return tmpArr;
				}
            	if (option == "destroy") {
            		var tmpArr = {};
            		tmpArr.model = {};
            		tmpArr.model = data;
            		tmpArr.id = data['name'];
            		tmpArr.action = "destroy";
            		tmpArr.module = $("[name=current_module]").val();
					return tmpArr;
				}
				return data;
            }
        },
		schema : {
			model: {
                id: "name",
                fields: {
                	name: { text: "Name", editable: true, nullable: false },
                	configvalue: { text: "Configvalue", editable: true, nullable: true },
                	type: { text: "Type", editable: true, nullable: true },
                	category: { text: "Category", editable: true, nullable: true },
                	orderby: { text: "Order By", editable: true, nullable: true },
                    defvalue: { text: "Default", editable: true, nullable: true },
                }
            },
			data : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = obj.data;
				return objVal;
			},
			parse : function(data) {
				return data;
			}
		},
		requestStart: function(e) {
	    },
		change: function(e) {
	    }
	});
	
	var resultGrid = $("#resultGrid").kendoGrid({
        dataSource: configSetDataSource,
        height: 450,
        sortable: true,
        editable: true,
        toolbar: [
	          { name: "refresh", text: "Refresh", template: '<a class="k-button k-button-icontext" id="refreshBtn" ><span class="k-icon "></span>Refresh</a>' },
	          { name: "addNew", text: "Add New Setting", template: '<a class="k-button k-button-icontext k-add-button" ><span class="k-icon k-add"></span>Add new Setting</a>' }
		],
        detailTemplate: kendo.template($("#editTemplate").html()),
        detailInit: detailInit,
        dataBound: function() {
            //this.expandRow(this.tbody.find("tr.k-master-row").first());
        	$('td.k-hierarchy-cell').prepend("<img src=\"/images/admin/move-arrow.png\" width=\"16\" height=\"16\" class=\"handle\">");
        	$('td.k-hierarchy-cell a').css("display","none");
        	$(".k-grid-content table tbody .k-master-row").addClass("draggable");
        	$(".k-grid-content table tbody").addClass("draggable");
        	//renderSortable();
        	sortableList = $(".k-grid-content table tbody").sortable({handler:"img",update:sortList}).data("sortable");
        },
        editable: {
            update: false,
            destroy: false
        },
        edit: function(e) {
        	//e.preventDefault();
        	return false;
        },
        save: function(e) {
        	//e.preventDefault();
        	//this.dataSource.sync();
        	return false;
        },
        columns: [
            {
                field: "name",
                title: "Variable Name",
                filterable: false, 
            	sortable : false,
            	template: function(data){return "<span class=\"gridRowid-"+data.name+"\" id=\"gridRow-ID\">"+data.name+"</span>";},
				editor: function(container, options) { return false; }
            },
            {
                field: "value",
                title: "Variable Value",
                filterable: false, 
            	sortable : false,
            	template: function(data){ var txt=""; console.log(data); for (var i in data.configvalue) txt +=i+"="+data.configvalue[i]+", ";  return txt;},
            	
            	
            },
            {
            	command: [
    	          {text:"Edit", className: "k-edit-button", template: "<span class=\"EditSet k-icon k-edit pointer spacer-right k-edit-button\" id=\"configSetListEditBtn\"></span>"},
    	          {text:"Delete", className: "k-delete-button", template: "<span class=\"DeleteSet k-icon k-delete pointer k-delete-button\" id=\"configSetListDeleteBtn\"></span>"}
            	], 
            	title: "Commands", 
            	width: 85, 
            	filterable: false
            }
        ],
    }).delegate(".k-edit-button", "click", function(e) {
    	var thisTarget = $(e.target).parents("tr.k-master-row");
    	resultGrid.expandRow(thisTarget);
    	resultGrid.editRow(thisTarget);
    	$("#resultGrid").delegate(".k-cancel-button", "click", function(e) {
        	resultGrid.collapseRow(thisTarget);
            e.preventDefault();
            $(".k-detail-row").remove()
        });
        e.preventDefault();
    }).delegate(".k-add-button", "click", function(e) {
    	resultGrid.addRow();
    	var thisTarget = $("#resultGrid").find("tr.k-master-row").first();
    	resultGrid.expandRow(thisTarget);
    	$("#resultGrid").delegate(".k-cancel-button", "click", function(e) {
    		resultGrid.removeRow(thisTarget);
        	resultGrid.collapseRow(thisTarget);
        	$(".k-detail-row").remove()
            e.preventDefault();
        });
        e.preventDefault();
    }).delegate(".k-delete-button", "click", function(e) {
    	var thisTarget = $(e.target).parents("tr.k-master-row");
    	showConfirmationModal("You are about to delete this setting, are you sure?", function() {
    		var tmpModel = resultGrid.dataItem(thisTarget);
    		var db = new mcms.dataStore({
    	    	method: "POST",
    	    	url: configSetUrl,
    	    	dataType: "json",
    	 		trigger:"loadComplete",
    	 		data: {
    	 			id: tmpModel.name,
    	 			action: "destroy",
    	 			module : $("[name=current_module]").val()
    	 		},
    		});
    		$(db).bind('loadComplete',function(){
    			resultGrid.removeRow(thisTarget);
    		});
    		$(db).loadStore(db);
    	});
        e.preventDefault();
    }).delegate(".k-update-button", "click", function(e) {
    	var tmpModel = resultGrid.dataItem($("#resultGrid").find(".k-grid-edit-row").first());
    	var tmpModelClone = {};
    	var tmpChildList = $(e.target).parents(".list-box-view");
    	
    	for(var x in tmpModel) {
    		try {
    			var tmpElem = tmpChildList.find(".configSetValue[name="+x+"]");
    			if(tmpElem.val().length>0) {
    				tmpModel._set(x,tmpElem.val());
    				tmpModelClone[x] = tmpElem.val();
    			}
    		} catch(ee) { }
    	}
    	var db = new mcms.dataStore({
	    	method: "POST",
	    	url: configSetUrl,
	    	dataType: "json",
	 		trigger:"loadComplete",
	 		data: {
	 			id: tmpModelClone.name,
	 			action: "update",
	 			model : tmpModelClone,
	 			module : $("[name=current_module]").val()
	 		},
		});
		$(db).bind('loadComplete',function(){
			$("#refreshBtn").trigger("click");
		});
		$(db).loadStore(db);
        resultGrid.saveRow();
        
    }).delegate("#refreshBtn", "click", function(e) {
    	configSetDataSource.fetch (function(data) {
    		//console.log(data);
    	});
    	//renderSortable();
    	sortableList = $(".k-grid-content table tbody").sortable({handler:"img",update:sortList}).data("sortable");
    }).data("kendoGrid");
	
	function detailInit(e) {
	    var detailRow = e.detailRow;
	    detailRow.find("#tabStrip").kendoTabStrip({
            animation: {
                open: { effects: "fadeIn" }
            }
        });
	    detailRow.find("[name=description]").kendoEditor();
	    detailRow.find("[name=comments]").kendoEditor();
	    detailRow.find("[name=type]").kendoComboBox();
	}
	
	sortableList = $(".k-grid-content table tbody").sortable({handler:"img",update:sortList}).data("sortable");
	function renderSortable(arg) {
		$('.k-grid-content table tbody').mcmsSortable({
		    items: '.k-grid-content table tbody .k-master-row',
		    handle: 'img',
		    connectWith: '.connected'
		}).bind('sortupdate', function(e,a) {
    		sortList(e);
    	}).bind('changed', function(e,a) {
    	}).bind('dragStart', function(e,a) {
    	}).bind('droped', function(e,a) {
    	});
	}
	
	function sortList(e) {
		var data = [];
		$(e.target).find("#gridRow-ID").each(function (i, v) {
			data[i] = {};
			data[i].name = $(v).first().html();
			data[i].orderby = i;
		});
		var db = new mcms.dataStore({
	    	method: "POST",
	    	url: configSetUrl,
	    	dataType: "json",
	 		trigger:"loadComplete",
	 		data: {
	 			action: "updateorder",
	 			model : data,
	 			module : $("[name=current_module]").val()
	 		},
		});
		
		$(db).bind('loadComplete',function(){
			$("#refreshBtn").trigger("click");
		});
		$(db).loadStore(db);
	}
	//var sortableList = $(".k-grid-content table tbody").sortable({handler:"img",update:sortList}).data("sortable");
});

function showConfirmationModal(confirmationText,confirmationCallback) {
	var confirmationModal = $("<div />").kendoWindow({
	    title: "Confirm",
	    resizable: false,
	    modal: true
	});

	confirmationModal.data("kendoWindow")
		.content("<p class=\"confirmationMessage\" style=\"margin: 5px;\">" + confirmationText+" </p>"
				+"<center> <button class=\"confirmationConfirmBtn k-button\" >Yes</button>"
				+"<button class=\"confirmationCancelBtn k-button\">No</button></center>")
		.center().open();
	
	confirmationModal.find(".confirmationConfirmBtn,.confirmationCancelBtn")
		.click(function() {
		    if ($(this).hasClass("confirmationConfirmBtn")) {
		    	confirmationCallback();
		    }
	    	confirmationModal.data("kendoWindow").close().destroy();
	}).end();
}