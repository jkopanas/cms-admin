var modSetUrl = "/ajax/loader.php?file=settings/modules.php&module="+$("[name=current_module]").val();
head.ready(function() {
	var sortableList = {}; 
	var modSetDataSource = new kendo.data.DataSource({
		batch: false,
		serverPaging : true,
		serverSorting : true,
		serverFiltering : true,
        transport: {
            read:  {
                url: modSetUrl,
            },
            update: {
                url: modSetUrl,
            },
            destroy: {
                url: modSetUrl,
            },
            create: {
                url: modSetUrl,
            },
            parameterMap: function(data, option) {
            	if (option == "read") {
					return data;
				}
            	if (option == "update") {
            		var tmpArr = {};
            		tmpArr.model = {};
            		tmpArr.model = data;
            		tmpArr.id = data['id'];
            		//tmpArr.action = "update";
            		tmpArr.module = $("[name=current_module]").val();
					return tmpArr;
				}
            	if (option == "create") {
            		var tmpArr = {};
            		tmpArr.model = {};
            		tmpArr.model = data;
            		tmpArr.id = data['id'];
            		//tmpArr.action = "update";
            		tmpArr.module = $("[name=current_module]").val();
					return tmpArr;
				}
            	if (option == "destroy") {
            		var tmpArr = {};
            		tmpArr.model = {};
            		tmpArr.model = data;
            		tmpArr.id = data['id'];
            		tmpArr.action = "destroy";
            		tmpArr.module = $("[name=current_module]").val();
					return tmpArr;
				}
				return data;
            }
        },
		schema : {
			model: {
                id: "id",
                fields: {
                	id: { text: "ID", editable: true, nullable: false },
                	name: { text: "Name", editable: true, nullable: false },
                	value: { text: "Value", editable: true, nullable: true },
                	type: { text: "Type", editable: true, nullable: true },
                	category: { text: "Category", editable: true, nullable: true },
                	orderby: { text: "Order By", editable: true, nullable: true },
                    comments: { text: "Comments", editable: true, nullable: true },
                    options: { text: "options", editable: true, nullable: true },
                    description: { text: "Description", editable: true, nullable: true }
                }
            },
			data : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = obj.data;
				return objVal;
			},
			parse : function(data) {
				return data;
			}
		},
		requestStart: function(e) {
	    },
		change: function(e) {
	    }
	});
	
	var resultGrid = $("#resultGrid").kendoGrid({
        dataSource: modSetDataSource,
        height: 450,
        sortable: true,
        editable: true,
        toolbar: [
	          { name: "refresh", text: "Refresh", template: '<a class="k-button k-button-icontext" id="refreshBtn" ><span class="k-icon "></span>Refresh</a>' },
	          { name: "addNew", text: "Add New Setting", template: '<a class="k-button k-button-icontext k-add-button" ><span class="k-icon k-add"></span>Add new Setting</a>' }
		],
        detailTemplate: function (data) {
        	data['options'] = clearInfection(data['options']);
        	return kendo.template($("#editTemplate").html())(data);
        },
        detailInit: detailInit,
        dataBound: function() {
            //this.expandRow(this.tbody.find("tr.k-master-row").first());
        	$('td.k-hierarchy-cell').prepend("<img src=\"/images/admin/move-arrow.png\" width=\"16\" height=\"16\" class=\"handle\">");
        	$('td.k-hierarchy-cell a').css("display","none");
        	$(".k-grid-content table tbody .k-master-row").addClass("draggable");
        	$(".k-grid-content table tbody").addClass("draggable");
        	//renderSortable();
        	sortableList = $(".k-grid-content table tbody").sortable({items: '.k-master-row', handler:"img",update:sortList}).data("sortable");
        },
        editable: {
            update: false,
            destroy: false
        },
        edit: function(e) {
        	return false;
        },
        save: function(e) {
        	//e.preventDefault();
        	//this.dataSource.sync();
        	return false;
        },
        columns: [
			{
			    field: "id",
			    title: "ID",
			    filterable: false, 
				sortable : false,
				editable: false,
				width: 35,
				template: function(data){return "<span class=\"gridRowid-"+data.id+"\" id=\"gridRow-ID\">"+data.id+"</span>";},
				editor: function(container, options) { return false; }
			},
            {
                field: "name",
                title: "Variable Name",
                filterable: false, 
            	sortable : false
            },
            {
                field: "value",
                title: "Variable Value",
                filterable: false, 
            	sortable : false
            },
            {
            	command: [
    	          {text:"Edit", className: "k-edit-button", template: "<span class=\"EditSet k-icon k-edit pointer spacer-right k-edit-button\" id=\"modSetListEditBtn\"></span>"},
    	          {text:"Delete", className: "k-delete-button", template: "<span class=\"DeleteSet k-icon k-delete pointer k-delete-button\" id=\"modSetListDeleteBtn\"></span>"}
            	], 
            	title: "Commands", 
            	width: 85, 
            	filterable: false
            }
        ],
    }).delegate(".k-edit-button", "click", function(e) {
    	var thisTarget = $(e.target).parents("tr.k-master-row");
    	resultGrid.expandRow(thisTarget);
    	resultGrid.editRow(thisTarget);
    	$("#resultGrid").delegate(".k-cancel-button", "click", function(e) {
        	resultGrid.collapseRow(thisTarget);
            e.preventDefault();
            $(".k-detail-row").remove()
        });
        e.preventDefault();
    }).delegate(".k-add-button", "click", function(e) {
    	resultGrid.addRow();
    	var thisTarget = $("#resultGrid").find("tr.k-master-row").first();
    	resultGrid.expandRow(thisTarget);
    	$("#resultGrid").delegate(".k-cancel-button", "click", function(e) {
    		resultGrid.removeRow(thisTarget);
        	resultGrid.collapseRow(thisTarget);
        	$(".k-detail-row").remove()
            e.preventDefault();
        });
        e.preventDefault();
    }).delegate(".k-delete-button", "click", function(e) {
    	var thisTarget = $(e.target).parents("tr.k-master-row");
    	showConfirmationModal("You are about to delete this setting, are you sure?", function() {
    		var tmpModel = resultGrid.dataItem(thisTarget);
    		var db = new mcms.dataStore({
    	    	method: "POST",
    	    	url: modSetUrl,
    	    	dataType: "json",
    	 		trigger:"loadComplete",
    	 		data: {
    	 			id: tmpModel.id,
    	 			action: "destroy",
    	 			module : $("[name=current_module]").val()
    	 		},
    		});
    		$(db).bind('loadComplete',function(){
    			resultGrid.removeRow(thisTarget);
    		});
    		$(db).loadStore(db);
    	});
        e.preventDefault();
    }).delegate(".k-update-button", "click", function(e) {
    	var tmpModel = resultGrid.dataItem($("#resultGrid").find(".k-grid-edit-row").first());
    	var tmpModelClone = {};
    	var tmpChildList = $(e.target).parents(".list-box-view");
    	
    	for(var x in tmpModel) {
    		try {
    			var tmpElem = tmpChildList.find(".modSetValue[name*="+x+"]");
    			if(tmpElem.size()>1) {
    				var tmpKeyList = tmpChildList.find(".modSetValueKey");
    				var tmpValList = tmpChildList.find(".modSetValueVal");
    				tmpModelClone[x] = {};
    				$.each(tmpElem, function(k, v) {
    					try {
    						tmpModelClone[x][$(v).val()] = $(tmpValList[k]).val();
    					}
    					catch(eee) { }
					});
    				//tmpModelClone[x] = clearInfection(tmpModelClone[x]);
    			}
    			else if(tmpElem.val().length>0) {
    				tmpModel._set(x,tmpElem.val());
    				tmpModelClone[x] = tmpElem.val();
    			}
    		} catch(ee) {
    		}
    	}
    	
    	var db = new mcms.dataStore({
	    	method: "POST",
	    	url: modSetUrl,
	    	dataType: "json",
	 		trigger:"loadComplete",
	 		data: {
	 			id: tmpModelClone.id,
	 			action: "update",
	 			model : tmpModelClone,
	 			module : $("[name=current_module]").val()
	 		},
		});
		$(db).bind('loadComplete',function(){
			$("#refreshBtn").trigger("click");
		});
		$(db).loadStore(db);
        resultGrid.saveRow();
        
    }).delegate("#refreshBtn", "click", function(e) {
    	modSetDataSource.fetch (function(data) {
    		//console.log(data);
    	});
    	//renderSortable();
    	sortableList = $(".k-grid-content table tbody").sortable({items: '.k-master-row', handler:"img",update:sortList}).data("sortable");
    }).data("kendoGrid");
	
	function detailInit(e) {
	    var detailRow = e.detailRow;
	    detailRow.find("#tabStrip").kendoTabStrip({
            animation: {
                open: { effects: "fadeIn" }
            }
        });
	    detailRow.find("[name=description]").kendoEditor();
	    detailRow.find("[name=comments]").kendoEditor();
	    detailRow.find("[name=type]").kendoComboBox();
	}
	
	sortableList = $(".k-grid-content table tbody").sortable({items: '.k-master-row', handler:"img",update:sortList}).data("sortable");
	function renderSortable(arg) {
		$('.k-grid-content table tbody').mcmsSortable({
		    items: '.k-grid-content table tbody .k-master-row',
		    handle: 'img',
		    connectWith: '.connected'
		}).bind('sortupdate', function(e,a) {
    		sortList(e);
    	}).bind('changed', function(e,a) {
    	}).bind('dragStart', function(e,a) {
    	}).bind('droped', function(e,a) {
    	});
	}
	
	function sortList(e) {
		var data = [];
		$(e.target).find("#gridRow-ID").each(function (i, v) {
			data[i] = {};
			data[i].id = $(v).first().html();
			data[i].orderby = i;
		});
		var db = new mcms.dataStore({
	    	method: "POST",
	    	url: modSetUrl,
	    	dataType: "json",
	 		trigger:"loadComplete",
	 		data: {
	 			action: "updateorder",
	 			model : data,
	 			module : $("[name=current_module]").val()
	 		},
		});
		
		$(db).bind('loadComplete',function(){
			$("#refreshBtn").trigger("click");
		});
		$(db).loadStore(db);
	}
	//var sortableList = $(".k-grid-content table tbody").sortable({handler:"img",update:sortList}).data("sortable");
	
	$("#addRow").live("click",function(e) {
		e.preventDefault();
   		var rows = parseInt($("#icounter").val());
   		$("#dataTable tbody.settingcontent").append(
			'<tr class="dataRow" id="row'+rows+'"> '
			+ '<td><input type="text" name="options[]" class="SettingDataJson modSetValue modSetValueKey inpt_a" value=""></td>'
			+ '<td><input type="text" name="options[]" class="SettingDataJson modSetValueVal inpt_a" value=""></td>'
			+ '<td><a href="\#" class="removeRow" rel="row'+rows+'">Remove</a></td></tr>'
		);
		rows++;
		$("#icounter").val(rows);
	});
	
	$("select#type").live('change', function(e){
    	if($(this).val()=="select") {
    	    $("div.multioptions").removeClass("hidden");
    	}
    	else {
    	    $("div.multioptions").addClass("hidden");
    	}
	});
	
	$(".removeRow").live("click",function(e) {
    	e.preventDefault();
        var row = $("#"+$(this).attr('rel'));
        row.remove();
    });
	/*
	$(db).bind('UpdateSetting', function() {
		records = db.records;
		grids = grid.ReturnGrid();
		var tr = grids.dataItem(grids.tbody
				.find(">tr[data-uid=" + db.uid + "]"));

		o = {};
		$.each(records, function(index, value) {
			o[index] = value;
		});
		for ( var attrname in tr) {
			tr[attrname] = (o[attrname]) ? o[attrname]
					: tr[attrname];
		}
		grids.saveRow();
		LockEdit = false;
	});
	
	$(db).bind('CreateSetting',function(){
		records=db.records;
		grids = grid.ReturnGrid();
		grids.cancelChanges();
		o = {varName: '', title: '', file: ''};
	    for (var attrname in records) { o[attrname] = records[attrname]; }
		grids.dataSource.insert(0, o);
		LockEdit=false;
	});
	*/
});

function showConfirmationModal(confirmationText,confirmationCallback) {
	var confirmationModal = $("<div />").kendoWindow({
	    title: "Confirm",
	    resizable: false,
	    modal: true
	});

	confirmationModal.data("kendoWindow")
		.content("<p class=\"confirmationMessage\" style=\"margin: 5px;\">" + confirmationText+" </p>"
				+"<center> <button class=\"confirmationConfirmBtn k-button\" >Yes</button>"
				+"<button class=\"confirmationCancelBtn k-button\">No</button></center>")
		.center().open();
	
	confirmationModal.find(".confirmationConfirmBtn,.confirmationCancelBtn")
		.click(function() {
		    if ($(this).hasClass("confirmationConfirmBtn")) {
		    	confirmationCallback();
		    }
	    	confirmationModal.data("kendoWindow").close().destroy();
	}).end();
}

function clearInfection(argArr) {
	var tmpElem = [
	               '_events', 'Array', 'uid','init','shouldSerialize','toJSON', 'get', '_set', 'set', 'wrap', 'constructor', 'bind', 'one', 'trigger', 'unbind'
	];
	var tmpArr = {};
	for(var i=0; i<tmpElem.length; i++) {
		try {
			delete argArr[tmpElem[i]];
		}
		catch(e) {
		}
	}
	for(var x in argArr) {
		tmpArr[x] = argArr[x];
	}
	return tmpArr;
}
