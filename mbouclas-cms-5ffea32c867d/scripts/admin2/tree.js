
(function($) {
	
    $.fn.treeview = function(options)
    {
        return this.each(function()
        {
            var element = $(this);
           
            // Return early if this element already has a plugin instance
            if (element.data('tree')) return;

            // pass options to plugin constructor
            var myplugin = new tree(this, options);
		           
            // Store plugin object in this element's data
            element.data('tree', myplugin);
        });
    };
	

    var tree = function(element, options)
    {
        var elem = $(element);
        var objTree = '';
        var settings = $.extend({
        	kendo: {
            	dragAndDrop: false,
            	template: '',
            	dataSource: ''
        	},
            source: '',
            data: {}
        }, options || {});
        
        
  	var ajaxTree = new mcms.dataStore({
	    method: "POST",
	    url:settings.source,
	    dataType: "json",
		trigger:"loadCompletes"
	});
	
	$(ajaxTree).bind('loadCompletes',function(){
		records = ajaxTree.records;
		
			CreateTree(records.results);
		
	});
	      
        this.o = { //HOOKS CAN BY APPLIED HERE TO OVERCOME THE SCOPE
            init : function() {	
	         	
            },
            setup : function() {
	         
            }
        }//END OBJECT

	       
        this.init = function() {	   
           
        	
     		  ajaxTree.data = {};
     		  if (settings.source) {
        	  	$(ajaxTree).loadStore(ajaxTree);
     		  } else {
     		  	CreateTree(settings.data);
     		  }
        	
        };
        
        var CreateTree = function(data)
        {
        	
        	obj = {	
        		dataSource: data,
            	source: ''
        	}
        	
        	var setting=$.extend(
        	 settings.kendo
        	, obj || {});
        	
        	objTree =  $(elem).kendoTreeView(setting).data("kendoTreeView");	
        	
	    	 $('.k-in').find('span:first').each(function(index,value){
	    		 var id=$(value).attr('id');
	    		 $(value).parents("li:first").attr('id','parent_'+id);
	    	 });

        };
        
        this.updatetree=function(url){
        	objTree='';
           	elem.html('');
        	elem.removeProp('class');
           	elem.removeProp('data-role');
            ajaxTree.url=url;
            ajaxTree.data = {};
       	    $(ajaxTree).loadStore(ajaxTree);
        	
        }
        

	 	
	 	this.clicked = function (node_id) {
	 		
	 		ajaxTree.trigger ='Subcategories';
	 	    ajaxTree.data = {'node_id': node_id}	 	   
	 	    $(ajaxTree).loadStore(ajaxTree);
	 	   
	 	}
	 	

	 	
	 	 $(ajaxTree).bind('Subcategories',function(){
	 	 
	    	 records = ajaxTree.records;
	    	 callbackHandler('DrawSubcategories',$.extend({},{records: records.results, tree: objTree}))

	    }); //end bind Subcategories
	    
	    this.append = function (obj,parent) {
	    	 var template = settings.kendo.template;
	    	  objTree.append({text: template({ item: obj }), encoded: false}, parent);
	    }
	    
	    
	    this.insertBefore = function (obj,parent) {
	    	 var template = settings.kendo.template;
	    	  objTree.insertBefore({text: template({ item: obj }), encoded: false}, parent);
	    }
	    
	    this.insertAfter = function (obj,parent) {
	    	 var template = settings.kendo.template;
	    	  objTree.insertAfter({text: template({ item: obj }), encoded: false}, parent);
	    }
	    
	    this.remove = function(parent) {
	    	//console.log(parent);
	    	 objTree.remove(parent);
	    }
	    
        
        
		var callbackHandler = function(func)
		{
			if (this[func])
			{
				window[func].apply(this, Array.prototype.slice.call(arguments, 1));
			}
			else { console.log(func + ' is not registered'); }
    	};
    }
	  
})(jQuery);
