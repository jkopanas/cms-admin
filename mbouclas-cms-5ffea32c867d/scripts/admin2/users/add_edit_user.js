head.ready(function(){
		
		 var db = new mcms.dataStore({
         	method: "POST",
         	url:"",
         	//dataType: "json",
         	trigger:"loadComplete"
     	 });
     
		 //***** check if username is available *****//
		 
	     $("#check_availability").live("click", function(){
	        	
	        	var uname = $("#uname").val();
	         if (uname!=""){	
		 	  	db.url="/ajax/loader.php?file=user_functions.php&action=checkUser";
		 		db.trigger ='check_avail';
		 	    db.data = {uname:uname}
		    	$(db).loadStore(db);
	         }	else {
	        	 $('#fail').show().html("Εισάγετε ένα όνομα χρήστη").delay(3000).fadeOut('slow');
	         }
		 });   
		 	
		    $(db).bind('check_avail',function(){
		    	records = db.records;
		    	datas = $.parseJSON(records);
		    	
		    	if (datas.status == 1) {
					$('#success').show().html(datas.message).delay(3000).fadeOut('slow');
				} else if (datas.status == 0) {
					$('#fail').show().html(datas.message).delay(3000).fadeOut('slow');

				}
		    
		    }); 
		    //***** end check if username is available *****//
		 
		    
		    
			 //***** display password *****//
			 
		     $("#show_pass").live("click", function(e){
		    	 e.preventDefault();
		    	 var pass = $("#pass").val();
		    		$(".messagePlaceHolder").show().html(pass).delay(3000).fadeOut('slow');
			 });   
			 	
			    //***** end display password *****//
		    
		    
		    
		    //*****  add edit form *****//
	    		

        				//*****  add edit form *****//

        
        $(".k-delete").live("click", function(e){
	  	 	  
        	 var id=$(this).attr('id');
	  	 	  
	  	 	e.preventDefault();
	  	    
	  	    kendoWindow = $("<div />").kendoWindow({
	  	            title: "Διαγραφή Χρήστη",
	  	            resizable: false,
	  	            width: "160",
	  	            modal: true
	  	        });
	  	    
	  	        obj = { id: id };
	  	        var template = kendo.template($("#delete-confirmation").html());
	  	    kendoWindow.data("kendoWindow")
	  	        .content(template({ item: obj }))
	  	        .center().open();
	  	 	  
		 	    
		 	});   
        
   	 $(".delete-confirm").live("click", function(e){    
   		 	
   		 	var total_users=parseInt($("#total_users").val());
   			$("#tot_users").html(parseInt(total_users-1));
 			$("#total_users").val(parseInt(total_users-1));
   		 	var id=$(this).data('id');
       		grid = $("#gridview").kgrid().data('grid');
       		grid.removeRow(id);
   		    kendoWindow.data("kendoWindow").close();
	  		db.url="/ajax/loader.php?file=user_functions.php&action=delete_user";
    	  	db.trigger ='delete_user';
    	  	db.data = {'user_id':id}
    	  	$(db).loadStore(db);	 
   	 });
        
   	 
   	 $(".delete-cancel").live("click", function(e){    
		 kendoWindow.data("kendoWindow").close();

    });
   	 

});// end head

