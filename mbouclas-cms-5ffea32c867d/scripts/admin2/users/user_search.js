head.ready(function() {
	
     $(".calendar").kendoCalendar({
	 	change:function(e) {
			var str = kendo.toString(this.value(), 'yyyy-MM-dd');
			var d = str.match(/\d+/g);
			$('#'+e.sender.element.attr('data-trigger')).val(+new Date(d[0], d[1] - 1, d[2])/1000);
	 	}
	 });
	
	
	
	 var wnd = $("#FilterForm-ShowUsers");

	   $("#Search-ShowUsers").live("click", function(e){
		   var data= {};  
	   	   $.each($(".submitSearch"),function(index,value) {
		   		data[$(value).data('table')] = mcms.formData('.'+$(value).val(),{ getData:true});
		   });
		   var url="/ajax/loader.php?file=usersFront.php&action=filterUsers";
		   grid = $("#gridview").kgrid().data('grid');
		   grid.readgrid(url,data);
		   wnd.data("kendoWindow").close();
		   
		}); 
	
	   
}); //end head 