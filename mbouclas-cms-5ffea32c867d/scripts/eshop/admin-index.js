head.ready(function() {
	var indexUrl = "/ajax/loader.php?boxID=index";
	/******************************* /
	$('#breadcrumbs .current').html("Dashboard");
	$("#searchBox").kendoAutoComplete({
		dataTextField: "text",//"payment_method.title",
	    dataValueField: "value",//"payment_method.title",
		minLength : 1,
		placeholder : "Search to navigate...",
		//separator : ", ",
		filter : 'contains', // startswith, endswith
		suggest: true,
		// template: kendo.template(" "),
		dataSource: [
	         { text: "Παραγγελείες", value: "1" },
	         { text: "orders", value: "1" },
	         { text: "Πληρωμών, μεθόδοι", value: "2" },
	         { text: "Payment Methods", value: "2" },
	         { text: "Μεταφορών, μέθοδοι", value: "3" },
	         { text: "Shipping Methods", value: "3" },
	    ],
		delay : 500,
		height : 500,
		select : function(e) {
		},
		change : function(a) {
			var value = this.value();
	        if (value) 
	        {
	        	switch(value)
	        	{
		        	case "Παραγγελείες":
		        	case "orders":
		        		document.location.hash = "#!/orders";
		        		break;
		        	case "Πληρωμών, μεθόδοι":
		        	case "Payment Methods":
		        		document.location.hash = "#!/payment_methods";
		        		break;
		        	case "Μεταφορών, μέθοδοι":
		        	case "Shipping Methods":
		        		document.location.hash = "#!/shipping_methods";
		        		break;
		        	default:
		        		break;
	        	}
	        }
		},
		close : function() {
		},
		open : function() {
		}
	});
	$("#searchBox").data("kendoAutoComplete").value(" ");
	
	/************************************ /
	// TODO
	$("#searchCombo").kendoComboBox({
		dataTextField: "text",//"payment_method.title",
	    dataValueField: "value",//"payment_method.title",
	    autoBind: false,
	    optionLabel: "All",
	    placeholder : "Search to navigate...",
	    dataSource: [
            { text: "Παραγγελείες", value: "1" },
            { text: "orders", value: "1" },
            { text: "Πληρωμών, μεθόδοι", value: "2" },
            { text: "Payment Methods", value: "2" },
            { text: "Μεταφορών, μέθοδοι", value: "3" },
            { text: "Shipping Methods", value: "3" },
        ],
	    change: function() {
	        var value = this.value();
	        if (parseInt(value)>0) 
	        {
	        	switch(parseInt(value))
	        	{
		        	case 1:
		        		document.location.hash = "#!/orders";
		        		break;
		        	case 2:
		        		document.location.hash = "#!/payment_methods";
		        		break;
		        	case 3:
		        		document.location.hash = "#!/shipping_methods";
		        		break;
		        	default:
		        		break;
	        	}
	        }
	    }
	});
	/************************************/
	
	var dropDown = $("#DBDDL").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        autoBind: false,
        optionLabel: "All",
	    placeholder : "All",
        dataSource: [
            { text: "Orders by day Stats", value: 1 },
            { text: "Orders by Month Stats", value: 4 },
            { text: "Orders by Year Stats", value: 5 },
            { text: "Orders by Hour Stats", value: 6 },
            { text: "Payment methods Usage", value: 2 },
            { text: "Shipping methods Usage", value: 3 },
        ],
        change: function() {
        	var value = this.value();
	        if (parseInt(value)>0)
	        {
	        	fetchDashboardInfo(value);
	        }
        }
    });

	setTimeout(function() {
		fetchDashboardInfo(1);
    }, 400);
	
});

function fetchDashboardInfo(reqId)
{
	var dashboardInfo = new mcms.dataStore({
    	method: "GET",
    	url:"/ajax/loader.php?boxID=index",
    	dataType: "json",
 		trigger:"loadComplete",
 		data: {
 			id : reqId,
 		},
	});
	var thisReqID = reqId;
	var callBackFunc;
	var graphName;
	var graphTitle;
	switch(parseInt(thisReqID))
	{
    	case 1:
    		graphName = "orders per day";
    		graphTitle = "Orders";
    		dashboardInfo.data.action = "orders";
    		dashboardInfo.data.period = "daily";
    		callBackFunc = function(data, headers, name, title) {
    			createLineChart(data, headers, name, title);
    		};
    		break;
    	case 4:
    		graphName = "orders per Month";
    		graphTitle = "Orders";
    		dashboardInfo.data.action = "orders";
    		dashboardInfo.data.period = "monthly";
    		callBackFunc = function(data, headers, name, title) {
    			createLineChart(data, headers, name, title);
    		};
    		break;
    	case 5:
    		graphName = "orders per Year";
    		graphTitle = "Orders";
    		dashboardInfo.data.action = "orders";
    		dashboardInfo.data.period = "yearly";
    		callBackFunc = function(data, headers, name, title) {
    			createLineChart(data, headers, name, title);
    		};
    		break;
    	case 6:
    		graphName = "orders per Hour per Day";
    		graphTitle = "Orders";
    		dashboardInfo.data.action = "orders";
    		dashboardInfo.data.period = "hourly";
    		callBackFunc = function(data, headers, name, title) {
    			createLineChart(data, headers, name, title);
    		};
    		break;
    	case 2:
    		graphName = "Payment Methods per orders";
    		graphTitle = "Payment Methods";
    		dashboardInfo.data.action = "pmethods";
    		callBackFunc = function(data, headers, name, title) {
    			createBarChart(data, headers, name, title);
    		};
    		break;
    	case 3:
    		graphName = "Shipping Methods per orders";
    		graphTitle = "Shipping Methods";
    		dashboardInfo.data.action = "shipmethods";
    		callBackFunc = function(data, headers, name, title) {
    			createPieChart(data, headers, name, title);
    		};
    		break;
    	default:
    		break;
	}
	
	$(dashboardInfo).bind('loadComplete',function(){
		var tmpArr = dashboardInfo.records;
		callBackFunc(tmpArr.values, tmpArr.headers, graphName, graphTitle);
		//var tmpTempl = kendo.template($("#eshopDashboardDDLTPL").html());
		//tmpTempl($.parseJSON(orderInfo.records.data.values));
	});
	$(dashboardInfo).loadStore(dashboardInfo);
}

function createBarChart(dataArr, headers, entityName, chartTitle) {
    $("#drawAreaCanvas").kendoChart({
        theme: "metro",
        dataSource: {
            data: dataArr
        },
        title: {
            text: (chartTitle ? chartTitle : entityName)
        },
        legend: {
            position: "bottom"
        },
        seriesDefaults: {
            type: "bar",
            labels: {
                visible: true,
            }
        },
        series: [{
        	name: entityName,
        	data: dataArr
        }],
        valueAxis: {
            labels: {
            }
        },
        categoryAxis: {
        	categories: headers
        }
    });
}

function createLineChart(dataArr, headers, entityName, chartTitle) {
    $("#drawAreaCanvas").kendoChart({
        theme: "metro",
        dataSource: {
            data: dataArr
        },
        title: {
            text: (chartTitle ? chartTitle : entityName)
        },
        legend: {
            position: "bottom"
        },
        seriesDefaults: {
        	type: "line",
            labels: {
                visible: true,
            }
        },
        series: [{
        	name: entityName,
        	data: dataArr
        }],
        valueAxis: {
            labels: {
            }
        },
        categoryAxis: {
        	categories: headers
        }
    });
}
function createPieChart(dataArr, headers, entityName, chartTitle) {
	var tmpArr = [];
	for(var i=0; i<dataArr.length; i++)
	{
		tmpArr[i] = {
				"source": headers[i],
                "percentage": parseInt(dataArr[i]),
                "explode": true
		};
	}
    $("#drawAreaCanvas").kendoChart({
        theme: "metro",
        dataSource: {
            data: tmpArr
        },
        title: {
            text: (chartTitle ? chartTitle : entityName)
        },
        legend: {
            position: "bottom"
        },
        seriesDefaults: {
        	type: "pie",
            labels: {
                visible: true,
            }
        },
        series: [{
        	type: "pie",
        	field: "percentage",
            categoryField: "source",
            explodeField: "explode",
        	name: entityName,
        }],
        tooltip: {
            visible: true,
            template: "${ category } - ${ value }%"
        }
    });
}
