
$.extend($.fn.mCms, {
	eshopDataLocalDataSource : new kendo.data.DataSource({
		data : []
	}),
	eshopCatLocalDataSource : new kendo.data.DataSource({
		data : []
	}),
});

function showConfirmationModal(confirmationText,confirmationCallback)
{
	var confirmationModal = $("<div />").kendoWindow({
	    title: "Confirm",
	    resizable: false,
	    modal: true,
	});

	confirmationModal.data("kendoWindow")
	.content("<p class=\"confirmationMessage\" style=\"margin: 5px;\">" + confirmationText+" </p>"
			+"<center> <button class=\"confirmationConfirmBtn k-button\" >Yes</button>"
			+"<button class=\"confirmationCancelBtn k-button\">No</button></center>")
	.center().open();
	
	confirmationModal.find(".confirmationConfirmBtn,.confirmationCancelBtn")
	.click(function() {
	    if ($(this).hasClass("confirmationConfirmBtn")) {
	    	confirmationCallback();
	    }
	    confirmationModal.data("kendoWindow").close().destroy();
	}).end();
}



/*******************************************************************
 * O => YOU ARE HERE
 * 
 * ViewContrl -> View Controler; controler.js
 * View -> the View of the box; tpl render
 * ModContrl -> the modular_box controler; loader.php
 * Module -> the modular_box; The php file of the box
 * 
 * A View request is been answered, as the view asks for its data. 
 * Data are been processed by the modular box and returned to view
 * witch should render the fetched json.
 *
 * 		ViewContrl			View		ModContrl			Module
 *---------------------------------------------------------------------------
 *-REQVIEW->_I_				 |				 |				 |
 * 			|O|	Call View	 |				 |				 |
 * 			| |---------------------------->_I_				 |
 * 							 | Smarty Displ | |Fetch view	 |
 * 							_I_<------------| |				 |
 * 		Ajax Display		| |				 |				 |
 * <------------------------| |				 |				 |
 * 							 |				 |				 |
 * 							_I_				 |				 |
 *  						| |Call loader	 |				 |
 *  						| |------------>_I_				 |
 *  						 |				| |Ajax-Load	 |
 * 							 |				| |------------>_I_
 * 							 |					Return Data	| | Fetch Data
 * 							_I_<----------------------------| |
 * 							| |Render Data
 * 			Display Data	| |
 * <------------------------| |
 *
 * A Request is been handled, as the command should be proccesed 
 * and return its state.
 *
 * -REQ-CMD---Module+Comonent+RequestCMD--->___
 * 											| |Ajax-Load
 * 											| |------------>___
 * 												Return Data	| | Process CMD 
 * 							_I_<----------------------------| | & Fetch Data
 * 							| |Render Data
 * 			Display Data	| |
 * <------------------------| |
 *
 *******************************************************************/
