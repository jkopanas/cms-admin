var ordersUrl = "/ajax/loader.php?boxID=orders";
head.ready(function() {
	$('#breadcrumbs .current').html("Orders");
	var comboBoxDataSource = new kendo.data.DataSource({
        data: [ ]
	});
	var ordersDataSource = new kendo.data.DataSource({
		type : "json",
		serverPaging : true,
		serverSorting : true,
		serverFiltering : true,
		serverAggregates : true,
		pageSize : 40,
		transport : {
			read : {
				url : ordersUrl
			},
			parameterMap : function(data, option) {
				if (option == "read") {
					return data;
				}
				return data;
			}
		},
		aggregate : [
		    { field : "id", aggregate : "count" },
			{ field : "amount", aggregate : "sum" },
		],
		schema : {
			aggregates : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = jQuery.parseJSON(obj.data.values);
				return {
					id : {
						count : obj.data.count,
					},
					amount : {
						sum : obj.data.sum,
					}
				};
			},
			total : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = jQuery.parseJSON(obj.data.values);
				return obj.data.count;
			},
			model : {
				id : "id",
				fields : {
					id : {
						type : "Number",
						nullable : true
					},
					email : {
						type : "string",
						nullable : true
					},
					status : {
						nullable : true
					},
					amount : {
						type : "Number",
						nullable : true
					},
					payment_method : {
						nullable : true
					},
					shipping_method : {
						nullable : true
					},
					notes : {
						type : "string",
						nullable : true
					},
					archive : {
						nullable : true
					},
				},
			},
			data : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = jQuery.parseJSON(obj.data.values);
				try {
					for (i in objVal) {
						objVal[i].text = "";
						objVal[i].text += objVal[i].id + " "
								+ objVal[i].email + " "
								+ objVal[i].amount;
					}
				} catch (e) {
					// console.log(e);
				}
				try {
					if (obj.data.payment_methods) {
						var tmpArr = [];
						for (i in obj.data.shipping_methods) {
							var tmpObj = obj.data.shipping_methods[i];
							tmpArr[i] = {};
							tmpArr[i].id = tmpObj.id;
							tmpArr[i].title = tmpObj.shipping_time;
						}
						comboBoxDataSource
								.data(obj.data.payment_methods
										.concat(tmpArr));
						comboBoxDataSource.fetch(function(data) {
							// console.log(data);
						});
					}
				} catch (e) {
					// console.log(e);
				}
				// console.log(objVal);
				return objVal;
			},
			parse : function(data) {
				return data;
			}
		},
		requestStart : function(e) {
		},
		change : function(e) {
		}
	});
	/** **************************** */
	var resultsGrid = $("#resultsGrid").kendoGrid({
		dataSource : ordersDataSource,
		height : 400,
		pageSize: 40,
		sortable : true,
		navigatable : true,
		pageable: true,
		//selectable : 'row',
		scrollable : {
			virtual : false
		},
		toolbar: [
		          { name: "reset", text: "Reset Data", template: '<a class="k-button k-reset k-button-icon " id="resetOrdersBtn"><span class=" "></span><span id="text">Reset</span></a>' },
		          //{ name: "archive", text: "Show Archived", template: '<a class="k-button k-button-icontext " id="showArviveOrdersBtn"><span class="hidden" id="isActive">1</span><span id="text"> Show Archived</span></a>' },
		          { name: "ordersFilter", text: "Filter Data", template: '<a class="k-button k-reset k-button-icon " id="doShowOrdersFiltersBtn"><span class=" "></span><span id="text">Filter Data</span></a>' },
		          //{ name: "ordersStatus", template: kendo.template($("#gridOrderStatus").html()) },
		          
		],
		filterable: true,
		columns : ( function(data) {
			gridColumns = [];
			gridColumns[gridColumns.length] = { field: "id", title: "#ID", filterable: false, width: 95, footerTemplate: "Total Count: #=count#"};//, template: '<a href="?entity=order&action=modify&id=${id}" >${id}</a>'};
			gridColumns[gridColumns.length] = { field: "email", title: "Email", filterable: false, sortable : false,};
			gridColumns[gridColumns.length] = { field: "status", title: "Status", template: function (data) { return orderStatusConverter(data.status); }, filterable: false, sortable : false,};
			gridColumns[gridColumns.length] = { field: "amount", title: "Amount", width: 90, filterable: false, template: function (data) { return data.amount + " &euro; "; }, footerTemplate: function(data) { return "<div>Sum: "+data.amount.sum+" &euro;</div>"; }};
			gridColumns[gridColumns.length] = { field: "payment_method", title: "Payment Method", template: function (data) { return (data.payment_method.title ? data.payment_method.title : "Δεν υπάρχει"); }, filterable: false, sortable : false, };
			//gridColumns[gridColumns.length] = { field: "payment_method", title: "Payment Description", width: "150px", height: "10px", template: function (data) { return data.payment_method.description; } };
			gridColumns[gridColumns.length] = { field: "shipping_method", title: "Shipping Method", template: function (data) {return (data.shipping_method.shipping_time? data.shipping_method.shipping_time : (data.shipping_method? data.shipping_method : "Δεν υπάρχει") ); }, filterable: false, sortable : false, };
			gridColumns[gridColumns.length] = { field: "notes", title: "Notes", filterable: false, sortable : false,};
			gridColumns[gridColumns.length] = { field : "archive", title: "Archive", editor: optionsDropDownEditor, width: 60, template: function (data) { return (parseInt(data.archive) > 0 ? "<span class=\"fr notification ok_bg\">"+$.lang.yes+"</span>" : "<span class=\"fr notification error_bg\">"+$.lang.no+"</span>"); }, filterable: false, sortable : false, };
			gridColumns[gridColumns.length] = { command: [{text:"Open", className: "open-button"}], title: "Commands", width: 95, filterable: false};
			
			return gridColumns;
		})({}),
		editable: "inline",
		/******************************* /
		change : function(e) {
			var currTR = this.current().closest('tr');
			// Close other TRs
			this.collapseRow(currTR.siblings());
			// Check if next tr already is a detail-row (KendoUI hides
			// an existing detail-row on collapseRow)
			if (currTR.next('tr').hasClass('k-detail-row')) {
				var detailTR = currTR.next('tr');
				if (detailTR.is(':visible')) {
					this.collapseRow(currTR);
				} else {
					this.expandRow(currTR);
				}
			} else {
				this.expandRow(currTR);
			}
		}
		/*******************************/
	});
	/******************************* /
	$("#searchBox").kendoAutoComplete({
		minLength : 4,
		autoBind: false,
		template: '${ data.text } &euro;',
		dataTextField : "text",
		dataValueField: "id",
		placeholder : "Enter Order ID, User name or email...",
		separator : ", ",
		filter : 'contains', // startswith, endswith
		// suggest: true,
		// template: kendo.template(" "),
		dataSource : ordersDataSource,
		delay : 500,
		height : 500,
		select : function(e) {
			if(parseInt(this.value())>0)
			{
				e.preventDefault();
				var orderInfo = new mcms.dataStore({
			    	method: "POST",
			    	url:"/ajax/loader.php?boxID=orders",
			    	dataType: "json",
			 		trigger:"loadComplete",
			 		data: {
			 			id : parseInt(this.value()),
			 		},
				});
				
				$(orderInfo).bind('loadComplete',function(){
					drawOrdersDetailsModal($.parseJSON(orderInfo.records.data.values));
					
				});
				$(orderInfo).loadStore(orderInfo);
			}
		},
		change : function(a) {
			var thisValue = this.value().split(" ");
			ordersDataSource.filter({
                field: "text",
                //operator: "eq",
                value:  thisValue[0]
            });
		},
		close : function() {
		},
		open : function() {
		}
	});
	$("#searchBox").data("kendoAutoComplete").value("");
	/*******************************/
	
	/************************************ /
	// TODO
	$("#searchCombo").kendoComboBox({
	    dataTextField: "title",//"payment_method.title",
	    dataValueField: "id",//"payment_method.title",
	    autoBind: false,
	    optionLabel: "All",
	    placeholder : "Filter by Payment or Shipping Method...",
	    dataSource: comboBoxDataSource,//ordersDataSource,
	    /************************************ /
	    {
	        //type: "jsonp",
	        severFiltering: true,
	        transport: {
	            read: "/ajax/loader.php?boxID=orders"
	        }
	    },
	    /************************************ /
	    change: function() {
	    	console.log(this.value());
	    	/************************************ /
	        var value = this.value();
	        if (parseInt(value)>0) {
	        	ordersDataSource.filter({
	                field: "spmethod",
	                //operator: "eq",
	                value: parseInt(value)
	            });
	        }
	        /************************************ /
	    }
	});
	/************************************/
	
	var detailedOrderWindow = $("#details").kendoWindow({
		title : "Order Details",
		modal : true,
		visible : false,
		resizable : true,
        width: "85%",
        height: "85%",
	}).data("kendoWindow").center();
	
	 var detailsTemplate = kendo.template( $("#modalTemplate").html() );
	 
	$("#resultsGrid").delegate(".open-button", "click", function(e) {
		e.preventDefault();
		//var dataItem = $("#resultsGrid").data("kendoGrid").dataItem($(this).closest("tr"));
		drawOrdersDetailsModal( $("#resultsGrid").data("kendoGrid").dataItem($(this).closest("tr")));
	});
	
	function drawOrdersDetailsModal(dataItem)
	{
		detailedOrderWindow.content(detailsTemplate(dataItem));
		$("#tabStrip").kendoTabStrip().css('border','none');; 
		detailedOrderWindow.center().open();
		
		$("#orderStatusList").kendoDropDownList({
	        dataTextField: "text",
	        dataValueField: "value",
	        autoBind: true,
	        dataSource: [
	            { text: "Έναρξη παραγγελίας", value: "1" },
	            { text: "Σε αναμονή", value: "2" },
	            { text: "Αναμονή από κάρτα", value: "3" },
	            { text: "Επιβεβαίωση παραγγελίας", value: "4" },
	            { text: "Προετοιμασία", value: "5" },
	            { text: "Αποστολή", value: "6" },
	            { text: "Ολοκληρώθηκε", value: "7" },
	            { text: "Ακυρώθηκε", value: "8" },
	        ],
	        change: function(e) {
	        	$("#orderStatusList").val()
	        	var orderStatusReq = new mcms.dataStore({
                	method: "POST",
                	url:"/ajax/loader.php?boxID=orders",
                	dataType: "json",
             		trigger:"loadComplete",
             		data: {
             			action : "update",
             			id : $("#orderArchiveBtn #orderArchiveVal").html(),
             			status: $("#orderStatusList").val()
             		},
            	});
            	$(orderStatusReq).bind('loadComplete',function(){
            		ordersDataSource.fetch (function(data) {
            			//console.log(data);
            		});
            		detailedOrderWindow.close();
            	});
            	$(orderStatusReq).loadStore(orderStatusReq);
	        }
	    });
	}
	
	$("#orderArchiveBtn").live("click",function(e) {
		e.preventDefault();
	    
		var thisOrderID = $("#orderArchiveBtn #orderArchiveVal").html();
		
	    var confirmationModal = $("<div />").kendoWindow({
	            title: "Confirm",
	            resizable: false,
	            modal: true,
	        });
	    
	    confirmationModal.data("kendoWindow")
	        .content($("#confirmationModal").html())
	        .center().open();
	    
	    confirmationModal.find(".confirmationConfirmBtn,.confirmationCancelBtn")
            .click(function() {
                if ($(this).hasClass("confirmationConfirmBtn")) {
                	var orderArchiveReq = new mcms.dataStore({
                    	method: "GET",
                    	url:"/ajax/loader.php?boxID=orders",
                    	dataType: "json",
                 		trigger:"loadComplete",
                 		data: {
                 			action : "archive",
                 			id : thisOrderID,
                 		},
                	});
                	$(orderArchiveReq).bind('loadComplete',function(){
                		//console.log(orderArchiveReq.records);
                		ordersDataSource.fetch (function(data) {
                			//console.log(data);
                		});
                		confirmationModal.data("kendoWindow").destroy();
                		detailedOrderWindow.close();
                	});
                	$(orderArchiveReq).loadStore(orderArchiveReq);
                }
                confirmationModal.data("kendoWindow").close();
            }).end();
	});
	
	
	$("#orderPrintBtn").live("click",function(e) {
		e.preventDefault();
		var thisId = parseInt($("#orderPrintBtn #orderPrintVal").html());
		window.open("/ajax/loader.php?boxID=orders&action=print&id="+thisId, 'Order '+thisId);
		return false;
		/************************************ /
		var orderAjaxPrintReq = new mcms.dataStore({
	    	method: "GET",
	    	url:"/ajax/loader.php?boxID=orders",
	    	dataType: "json",
	 		trigger:"loadComplete",
	 		data: {
	 			action: 'print',
	 			id : parseInt($("#orderPrintBtn #orderPrintVal").html()),
	 		},
		});
		
		$(orderAjaxPrintReq).bind('loadComplete',function(){
			$("<div />").html(orderInfo.records.data.values);
		});
		$(orderAjaxPrintReq).loadStore(orderAjaxPrintReq);
		/************************************/
	});
	
	
	$("#resetOrdersBtn").live("click",function(e) {
		e.preventDefault();
		ordersDataSource.options.transport.read.url = ordersUrl;
		ordersDataSource.transport.options.read.url = ordersUrl;
		//$("#showArviveOrdersBtn #isActive").html("1");
		//$("#showArviveOrdersBtn #text").html("Show Archived");
		//$("#orderStatusDDL").data("kendoDropDownList").search("All");
		//$("#searchCombo").data("kendoComboBox").text(" ");
		$("#searchBox").data("kendoAutoComplete").value("");
		ordersDataSource.filter([]);
		//ordersDataSource.fetch (function(data) { //console.log(data); });
	});
	
	/****************************************************************** /
	$("#showArviveOrdersBtn").live("click",function(e) {
		e.preventDefault();
		if($("#showArviveOrdersBtn #isActive").html()>0)
		{
			ordersDataSource.options.transport.read.url += "&archived=1";
			ordersDataSource.transport.options.read.url += "&archived=1";
			$("#showArviveOrdersBtn #isActive").html("0");
			$("#showArviveOrdersBtn #text").html("Show All");
			ordersDataSource.fetch (function(data) {
				//console.log(data);
			});
			return;
		}
		var tmp = ordersDataSource.options.transport.read.url.replace("&archived=1", "");
		ordersDataSource.options.transport.read.url = tmp;
		ordersDataSource.transport.options.read.url = tmp;
		$("#showArviveOrdersBtn #isActive").html("1");
		$("#showArviveOrdersBtn #text").html("Show Archived");
		ordersDataSource.fetch (function(data) {
			//console.log(data);
		});
		return;
	});
	/******************************************************************/
	/****************************************************************** /
	var dropDown = $("#orderStatusDDL").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
        optionLabel: "All",
        dataSource: [
            { text: "Έναρξη παραγγελίας", value: "1" },
            { text: "Σε αναμονή", value: "2" },
            { text: "Αναμονή από κάρτα", value: "3" },
            { text: "Επιβεβαίωση παραγγελίας", value: "4" },
            { text: "Προετοιμασία", value: "5" },
            { text: "Αποστολή", value: "6" },
            { text: "Ολοκληρώθηκε", value: "7" },
            { text: "Ακυρώθηκε", value: "8" },
        ],
        change: function() {
            var value = parseInt(this.value());
            if (value) {
            	var tmp = ordersDataSource.options.transport.read.url.replace(/&order_status=\d/g,"");
            	ordersDataSource.options.transport.read.url = tmp + "&order_status="+value;
    			ordersDataSource.transport.options.read.url = tmp + "&order_status="+value;
    			ordersDataSource.fetch (function(data) {
    				//console.log(data);
    			});
    			return;
            	//resultsGrid.data("kendoGrid").dataSource.filter({ field: "status", operator: "eq", value: parseInt(value) });
            }
            var tmp = ordersDataSource.options.transport.read.url.replace(/&order_status=\d/g,"");
    		ordersDataSource.options.transport.read.url = tmp;
    		ordersDataSource.transport.options.read.url = tmp;
    		ordersDataSource.fetch (function(data) {
    			//console.log(data);
    		});
    		return;
        }
    });
	/******************************************************************/
	$("#doShowOrdersFiltersBtn").live("click", function(e) {
		e.preventDefault();
		
		var ordersFiltersWindow = $("<div />").kendoWindow({
			title : "Order Filters",
			modal : true,
			visible : false,
			resizable : true,
	        width: "530px",
	        height: 375,
	        close: function() {
	        	this.destroy();
	        },
		}).data("kendoWindow");
		
		//var ordersFiltersTemplate = kendo.template( $("#ordersFiltersModalTemplate").html() );
		
		ordersFiltersWindow.content(kendo.template( $("#ordersFiltersModalTemplate").html() ));
		ordersFiltersWindow.center().open().center();
		
		$("#orderStatusFilterList").kendoDropDownList({
	        dataTextField: "text",
	        dataValueField: "value",
	        optionLabel: "Select a Status",
	        autoBind: true,
	        dataSource: [
	            { text: "Έναρξη παραγγελίας", value: "1" },
	            { text: "Σε αναμονή", value: "2" },
	            { text: "Αναμονή από κάρτα", value: "3" },
	            { text: "Επιβεβαίωση παραγγελίας", value: "4" },
	            { text: "Προετοιμασία", value: "5" },
	            { text: "Αποστολή", value: "6" },
	            { text: "Ολοκληρώθηκε", value: "7" },
	            { text: "Ακυρώθηκε", value: "8" },
	        ],
	    });
		$("#orderAmountOperatorFilterList").kendoDropDownList({
	        dataTextField: "text",
	        dataValueField: "value",
	        autoBind: true,
	        dataSource: [
	            { text: "> More than", value: "1" },
	            { text: "< Les than", value: "2" },
	            { text: "= equal", value: "3" },
	        ],
	    });
		$("#orderAmountValueFilterList").kendoNumericTextBox({
            format: "c",
            decimals: 3
        });
    	var orderFiltersDDLReq = new mcms.dataStore({
        	method: "GET",
        	dataType: "json",
     		data: {
     			method: 'simple',
     		},
    	});
		
		orderFiltersDDLReq.url = "/ajax/loader.php?boxID=payment_methods";
		orderFiltersDDLReq.trigger = "pmethodsComplete";
    	$(orderFiltersDDLReq).bind('pmethodsComplete',function(){
			var objVal = jQuery.parseJSON(orderFiltersDDLReq.records.data.payment_methods);
			var data = [];
			console.log(objVal);
			try
			{
				for(i in objVal)
				{
					data[i] = {
							value: 0,
							text: ""
					};
					data[i].value = objVal[i].id;
					data[i].text += objVal[i].id+" "+objVal[i].title+" ";
				}
			}
			catch (e) {
			}
			$("#orderPMethodsFilterList").kendoDropDownList({
		        dataTextField: "text",
		        dataValueField: "value",
		        optionLabel: "Select a Method",
		        autoBind: true,
		        dataSource: data,
		    });
    	});
    	$(orderFiltersDDLReq).loadStore(orderFiltersDDLReq);
		
    	orderFiltersDDLReq.url = "/ajax/loader.php?boxID=shipping_methods";
    	orderFiltersDDLReq.trigger = "shipmethodsComplete";
    	$(orderFiltersDDLReq).bind('shipmethodsComplete',function(){
			var objVal = jQuery.parseJSON(orderFiltersDDLReq.records.data.shipping_methods);
			var data = [];
			try
			{
				for(i in objVal)
				{
					data[i] = {
							value: 0,
							text: ""
					};
					data[i].value = objVal[i].id;
					data[i].text += objVal[i].id+" "+objVal[i].shipping+" ";
				}
			}
			catch (e) {
				
			}
			$("#orderShipMethodsFilterList").kendoDropDownList({
		        dataTextField: "text",
		        dataValueField: "value",
		        optionLabel: "Select a Method",
		        autoBind: true,
		        dataSource: data,
		    });
    	});
    	$(orderFiltersDDLReq).loadStore(orderFiltersDDLReq);
    	
    	$("#doSearchFiltersBtn").live("click",function(e) {
    		e.preventDefault();
    		var filter = [];
    		
    		if($("#orderArchiveFilterCB").is(':checked')===true)
    		{
    			filter[filter.length] = {
        				field: "archive",
        				operator: 'eq',
        				value: 1
        		};
    		}
    		if(parseInt($("#orderStatusFilterList").val())>0)
    		{
    			filter[filter.length] = {
        				field: "status",
        				operator: "eq",
        				value: $("#orderStatusFilterList").val()
        		};
    		}
    		if(parseInt($("#orderPMethodsFilterList").val())>0)
    		{
    			filter[filter.length] = {
        				field: "payment_method",
        				operator: "eq",
        				value: $("#orderPMethodsFilterList").val()
        		};
    		}
    		if(parseInt($("#orderShipMethodsFilterList").val())>0)
    		{
    			filter[filter.length] = {
        				field: "shipping_method",
        				operator: "eq",
        				value: $("#orderShipMethodsFilterList").val()
        		};
    		}
    		if(parseInt($("#orderAmountValueFilterList").val())>0)
    		{
    			var tmpamountVal = parseInt($("#orderAmountOperatorFilterList").val());
    			filter[filter.length] = {
        				field: "amount",
        				operator: (tmpamountVal == 3 ? 'eq' : (tmpamountVal == 2 ? 'le' : 'ge') ),
        				value: $("#orderAmountValueFilterList").val()
        		};
    		}
    		ordersDataSource.filter(filter);
    		ordersFiltersWindow.close();
    		return;
    	});
    	
	});
	
});



function optionsDropDownEditor(container, options) {
    $('<input data-text-field="active" data-value-field="value" data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        autoBind: false,
                        dataSource: [{ id: 1, active: "Yes", value: 1 }, { id: 2, active: "No", value: 0}]
                    });
}

function fetchOrderInfo(orderId)
{
	var orderInfo = new mcms.dataStore({
    	method: "POST",
    	url:"/ajax/loader.php?boxID=orders",
    	dataType: "json",
 		trigger:"loadComplete",
 		data: {
 			id : orderId,
 		},
	});
	
	$(".orderDetailsContainer").html(" ");
	$(".orderDetailsContainer").addClass("ac_loading");
	$(orderInfo).bind('loadComplete',function(){
		//console.log(orderInfo.records);
		$(".orderDetailsContainer").removeClass("ac_loading");
		var tmpTempl = kendo.template($("#extraDetails").html());
		//console.log($.parseJSON(orderInfo.records.data.values).details);
		$(".orderDetailsContainer").html(tmpTempl($.parseJSON(orderInfo.records.data.values)));
	});
	
	$(orderInfo).loadStore(orderInfo);
}

// orderStatusCodes is initiated at index page - inlineTempalte
function orderStatusConverter(id)
{
	return (orderStatusCodes[id]? orderStatusCodes[id] : "Αγνωστο");
}



