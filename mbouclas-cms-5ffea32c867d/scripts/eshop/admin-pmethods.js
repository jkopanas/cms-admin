var paymentMethodsUrl = "/ajax/loader.php?boxID=payment_methods";
head.ready(function() {
	$('#breadcrumbs .current').html("Payment Methods");
	
	var comboBoxDataSource = new kendo.data.DataSource({
        data: [ ]
	});
	var pMethodDataSource = new kendo.data.DataSource({
		batch: true,
		serverPaging : true,
		serverSorting : true,
		serverFiltering : true,
        transport: {
            read:  {
                url: paymentMethodsUrl,
                //dataType: "jsonp"
            },
            update: {
                url: paymentMethodsUrl,
                //dataType: "jsonp"
            },
            destroy: {
                url: paymentMethodsUrl,
               //dataType: "jsonp"
            },
            create: {
                url: paymentMethodsUrl,
                //dataType: "jsonp"
            },
            parameterMap: function(data, option) {
            	if (option == "read") {
					return data;
				}
            	if (option == "update") {
            		//delete data.models[0].settings;
            		var tmpArr = data.models[0];
            		delete tmpArr.settings;
            		delete tmpArr.shipping;
            		tmpArr['active'] = tmpArr['active'] ? 1 : 0;
            		tmpArr['action'] = "modify";
					return tmpArr;
				}
            	if (option == "create") {
            		console.log(data);
            		//delete data.models[0].settings;
            		var tmpArr = data.models[0];
            		//delete tmpArr.settings;
            		tmpArr['active'] = tmpArr['active'] ? 1 : 0;
            		tmpArr['action'] = "addnew";
					return tmpArr;
				}
            	if (option == "destroy") {
            		console.log(data);
            		//delete data.models[0].settings;
            		var tmpArr = data.models[0];
            		delete tmpArr.settings;
            		delete tmpArr.shipping;
            		tmpArr['active'] = tmpArr['active'] ? 1 : 0;
            		tmpArr['action'] = "delete";
					return tmpArr;
				}
				return data;
            }
        },
		schema : {
			model: {
                id: "id",
                fields: {
                    title: { type : "string", text: "Title", editable: true, nullable: false },
                    description: { type : "string", text: "Description", editable: true, nullable: true },
                    active: { text: "Active", editable: true, nullable: true },
                    surcharge: { type : "Number", text: "Surcharge", editable: true, nullable: true },
                    surcharge_type: { text: "Surcharge Type", editable: true, nullable: true },
                    orderby: { text: "Order By", editable: true, nullable: true },
                    date_added: { text: "Date", editable: true, nullable: true },
                    shipping_methods: { text: "Shipping Methods IDs", editable: true, nullable: true },
                    shipping: { text: "Shipping Methods", editable: true, nullable: true },
                    settings: { text: "Settings", editable: true, nullable: true },
                }
            },
			data : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = jQuery.parseJSON(obj.data.payment_methods);
				
				return objVal;
			},
			parse : function(data) {
				return data;
			}
		},
		requestStart: function(e) {
			
	    },
		change: function(e) {
			//console.log(e);
	    }
	});
	
	
	var listView = $("#resultsList").kendoListView({
        dataSource: pMethodDataSource,
        selectable: false,
        template: kendo.template($("#listTemplate").html()),
        editTemplate: kendo.template($("#editTemplate").html()),
        navigatable: true,
        edit: function(e) {
        	e.preventDefault();
        	$("#tabStrip").kendoTabStrip({ animation: false  }).css('border','none');
        	renderListView(e.model.shipping,e.model.id);
        },
        editable: true
    }).data("kendoListView");
	
	$("#resultsList").delegate(".k-edit-button", "click", function(e) {
        listView.edit($(this).closest(".list-box-view"));
        
        e.preventDefault();
    }).delegate(".k-delete-button", "click", function(e) {
    	e.preventDefault();
    	var thisClosest = $(this).closest(".list-box-view");
    	showConfirmationModal("You are about to delete this Payment Method, are you sure?", function() {
    		listView.remove(thisClosest);
    	});
        e.preventDefault();
    }).delegate(".k-update-button", "click", function(e) {
        listView.save();
        pMethodDataSource.fetch();
        e.preventDefault();
    }).delegate(".k-cancel-button", "click", function(e) {
        listView.cancel();
        e.preventDefault();
    });
	
    $(".k-add-button").click(function(e) {
        listView.add();
        e.preventDefault();
    });
    
    $("#openASettingsBtn").live("click", function(e) {
        e.preventDefault();
        fetchPMethodInfo($("#openASettingsVal").html());
    });
});

function fetchPMethodInfo(pmethodId)
{
	var pmethodInfo = new mcms.dataStore({
    	method: "POST",
    	url: paymentMethodsUrl,
    	dataType: "json",
 		trigger:"loadComplete",
 		data: {
 			id : pmethodId,
 		},
	});
	
	var pmethodInfoWindow = $("<div />").kendoWindow({
		title : "Payment Method Settings",
		modal : true,
		visible : false,
		resizable : true,
		width : 500
	}).data("kendoWindow").center();
		
	$(pmethodInfo).bind('loadComplete',function(){
		var tmpTempl = kendo.template($("#pmethodModalTemplate").html());
		pmethodInfoWindow.content(tmpTempl($.parseJSON(pmethodInfo.records.data.settings)));
		pmethodInfoWindow.center().open();
	});
	
	$("#pmethodModalSaveBtn").live("click", function(e) {
        e.preventDefault();
        pmethodInfo.data = {
        		id : pmethodId,
        		action : "modifysettings",
        		"accid": $("#PaymentSettingsAccid").val(),
        	    "accname": $("#PaymentSettingsAccname").val(),
        	    "key": $("#PaymentSettingsKey").val(),
        	    "apikey" : $("#PaymentSettingsApiKey").val(),
        	    "authkey": $("#PaymentSettingsAuthKey").val(),
        	    "email": $("#PaymentSettingsEmail").val(),
        	    "uname": $("#PaymentSettingsUname").val(),
        	    "pass": $("#PaymentSettingsPass").val(),
        	    "sucurl": $("#PaymentSettingsSucurl").val(),
        	    "failurl": $("#PaymentSettingsFailurl").val(),
        	    "cburl": $("#PaymentSettingsCBurl").val(),
        	    "extra": $("#PaymentSettingsExtra").val(),
        	    "payment_processor": "",
        };
        pmethodInfo.trigger = "modifySettings";
        
        $(pmethodInfo).bind('modifySettings',function(){
        	pmethodInfoWindow.close();
    	});
        $(pmethodInfo).loadStore(pmethodInfo);
    });
    $("#pmethodModalCancelBtn").live("click", function(e) {
        e.preventDefault();
        pmethodInfoWindow.close();
    });
	
	$(pmethodInfo).loadStore(pmethodInfo);
}

function renderListView(data,id)
{
	/*****/
	var tmpPMethodID = id;
	var shipMethdInnerList = $("#shipMethdInnerList-"+id).kendoListView({
        dataSource: data,
        selectable: false,
        template: kendo.template($("#innerListTemplate").html()),
        editTemplate: kendo.template($("#innerEditTemplate").html()),
        edit: function(e) {
        	e.preventDefault();
        },
        editable: false
    }).data("kendoListView");
	
	var shipMethodsUrl = "/ajax/loader.php?boxID=shipping_methods";
	var shipmethodInfo = new mcms.dataStore({
    	method: "POST",
    	url: shipMethodsUrl,
    	dataType: "json",
 		trigger:"loadComplete",
 		data: {
 			nothing : [],
 		},
	});
	var shipMethdInnerCB = {};
	var shipMethdInnerDataSource = new kendo.data.DataSource({
        data: [ ]
	});
	$(shipmethodInfo).bind('loadComplete',function(){
		// Fixing Data Ovelroad bug
		var tmpArr = [];
		tmpArr[0] = {};
		var tmpJSONObj = $.parseJSON(shipmethodInfo.records.data.values);
		for (i in tmpJSONObj) {
			var tmpObj = tmpJSONObj[i];
			tmpArr[i] = {};
			tmpArr[i].id = tmpObj.id;
			tmpArr[i].shipping = tmpObj.shipping;
		}
		shipMethdInnerDataSource.data(tmpArr);
		shipMethdInnerDataSource.fetch ([]);
		
		shipMethdInnerCB = $("#shipMethdInnerCB-"+tmpPMethodID).kendoComboBox({
		    dataTextField: "shipping",
		    dataValueField: "id",
		    placeholder : "Add a new Shipping Method...",
		    dataSource : shipMethdInnerDataSource,
		    filter: "contains",
		    change: function() {
		        var value = this.value();
		        var thisText = this.text();
		        if (parseInt(value)>0) {
		        	shipmethodInfo.data = {
		        			id: tmpPMethodID,
		        			smID: value,
		        			action: 'addnewshipmethod'
		        	};
		        	shipmethodInfo.url = "/ajax/loader.php?boxID=payment_methods";
		        	shipmethodInfo.method = "GET"
		        	shipmethodInfo.trigger = "addnewshipmethod";
		        	$(shipmethodInfo).bind('addnewshipmethod',function(){
		        		//pMethodDataSource.fetch ([]);
		        		var innerListTemplate = kendo.template( $("#innerListTemplate").html() );
		        		$("#shipMethdInnerList-"+tmpPMethodID).append(innerListTemplate({id:value,shipping:thisText}));
		        	});
		        	$(shipmethodInfo).loadStore(shipmethodInfo);
		        }
		    }
		}).data("kendoComboBox");
	});
	
	$(shipmethodInfo).loadStore(shipmethodInfo);
	    
	$("#shipMethdInnerList-"+id).delegate("#innerListDeleteBtn", "click", function(e) {
    	e.preventDefault();
    	var thisClosest = $(this).closest(".innerList-box-view");
    	showConfirmationModal("You are about to delete this Shipping Method, are you sure?", function() {
    		shipmethodInfo.data = {
        			id: tmpPMethodID,
        			smID: thisClosest.find(".innerUniqueID").html(),
        			action: 'deleteshipmethod'
        	};
        	shipmethodInfo.url = "/ajax/loader.php?boxID=payment_methods";
        	shipmethodInfo.method = "GET"
        	shipmethodInfo.trigger = "deleteshipmethod";
        	$(shipmethodInfo).bind('deleteshipmethod',function(){
        		shipMethdInnerList.remove(thisClosest);
        	});
        	$(shipmethodInfo).loadStore(shipmethodInfo);
    	});
        e.preventDefault();
    }).delegate("#innerEditSaveBtn", "click", function(e) {
    	shipMethdInnerList.save();
        pMethodDataSource.fetch();
        e.preventDefault();
    }).delegate("#innerEditCancelBtn", "click", function(e) {
    	shipMethdInnerList.cancel();
        e.preventDefault();
    });
}

/************************************ /
$("#searchBox").kendoAutoComplete({
	minLength : 1,
	autoBind: false,
	dataTextField : "text",
	dataValueField: "id",
	placeholder : "Enter Payment Method Name...",
	//separator : ", ",
	filter : 'contains', // startswith, endswith
	suggest: true,
	// template: kendo.template(" "),
	dataSource : comboBoxDataSource,
	delay : 500,
	height : 500,
	select : function(e) {
		var value = this.value();
        if (parseInt(value)>0) {
        	//console.log(listView.element.children().first());
        	listView.select($(".list-item-"+parseInt(value)));
        }
	},
	change : function(a) {
	},
	close : function() {
	},
	open : function() {
	}
});
$("#searchBox").data("kendoAutoComplete").value(" ");

$("#searchCombo").kendoComboBox({
    dataTextField: "text",
    dataValueField: "id",
    autoBind: false,
    //optionLabel: "All",
    placeholder : "Filter by Payment Method...",
    dataSource: comboBoxDataSource,
    change: function() {
        var value = this.value();
        if (parseInt(value)>0) {
        	console.log(value);
        	//console.log(listView.element.children().first());
        	listView.select($(".list-item-"+parseInt(value)));
        }
    }
});
/************************************/
/******************************* /
var panelPager = $(".k-pager-wrap #pager").kendoPager({
	dataSource : pMethodDataSource,
});
/*******************************/