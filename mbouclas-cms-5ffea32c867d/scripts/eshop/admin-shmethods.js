var shipMethodsUrl = "/ajax/loader.php?boxID=shipping_methods";
head.ready(function() {
	$('#breadcrumbs .current').html("Shipping Methods");
	var comboBoxDataSource = new kendo.data.DataSource({
        data: [ ]
	});
	var shipMethDataSource = new kendo.data.DataSource({
		batch: true,
		serverPaging : true,
		serverSorting : true,
		serverFiltering : true,
        transport: {
            read:  {
                url: shipMethodsUrl,
                //dataType: "jsonp"
            },
            update: {
                url: shipMethodsUrl,
                //dataType: "jsonp"
            },
            destroy: {
                url: shipMethodsUrl,
               //dataType: "jsonp"
            },
            create: {
                url: shipMethodsUrl,
                //dataType: "jsonp"
            },
            parameterMap: function(data, option) {
            	if (option == "read") {
					return data;
				}
            	if (option == "update") {
            		delete data.models[0].settings;
            		var tmpArr = data.models[0];
            		//console.log(tmpArr.settings);
            		try
            		{
            			tmpArr.costequation.replace(" ", "");
                		tmpArr.costequation.replace('\n', "");
                		tmpArr.costequationvars.replace(" ", "");
                		tmpArr.costequationvars.replace('\n', "");
            		}
            		catch(e)
            		{
            			
            		}
            		tmpArr['active'] = tmpArr['active'] ? 1 : 0;
            		tmpArr['action'] = "modify";
					return tmpArr;
				}
            	if (option == "create") {
            		console.log(data);
            		//delete data.models[0].settings;
            		var tmpArr = data.models[0];
            		//delete tmpArr.settings;
            		tmpArr.costequation.replace(" ", "");
            		tmpArr.costequation.replace('\n', "");
            		tmpArr.costequationvars.replace(" ", "");
            		tmpArr.costequationvars.replace('\n', "");
            		tmpArr['active'] = tmpArr['active'] ? 1 : 0;
            		tmpArr['action'] = "addnew";
					return tmpArr;
				}
            	if (option == "destroy") {
            		console.log(data);
            		//delete data.models[0].settings;
            		var tmpArr = data.models[0];
            		delete tmpArr.settings;
            		tmpArr['active'] = tmpArr['active'] ? 1 : 0;
            		tmpArr['action'] = "delete";
					return tmpArr;
				}
				return data;
            }
        },
		schema : {
			model: {
                id: "id",
                fields: {
                	shipping: { text: "Name", editable: true, nullable: false },
                	shipping_time: { text: "Code Name", editable: true, nullable: true },
                	description: { text: "Description", editable: true, nullable: true },
                	parent: { text: "Belongs to", editable: true, nullable: false },
                    active: { text: "Active", editable: true, nullable: true },
                    weight_min: { text: "Weight Minimum", editable: true, nullable: true },
                    weight_limit: { text: "Weight Limit", editable: true, nullable: true },
                    base_cost: { text: "Base Cost", editable: true, nullable: true },
                    costequation: { text: "Base Cost", editable: true, nullable: true },
                    costequationvars: { text: "Base Cost", editable: true, nullable: true },
                }
            },
			data : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = obj.data.shipping_methods;
				objVal = jQuery.parseJSON(objVal);
				// Format Equation Data
				try
				{
					for(i in objVal)
					{
						objVal[i].text = "";
						objVal[i].text += objVal[i].id+" "+objVal[i].shipping;
					}
				}
				catch (e) {
					//console.log(e);
				}
				try
				{
					if(objVal)
					{
						comboBoxDataSource.data(objVal);
						comboBoxDataSource.fetch (function(data) {
		        			//console.log(data);
		        		});
					}
				}
				catch (e) {
					//console.log(e);
				}
				try
				{
					for (i in objVal)
					{
						objVal[i].settings = jQuery.parseJSON(objVal[i].settings);
						objVal[i].costequation = objVal[i].settings.costequation;
						var tmpArr = [];
						var j = 0;
						for (x in objVal[i].settings.costequationvars)
						{
							tmpArr[j] = ""+ x + "=" + objVal[i].settings.costequationvars[x];
							j++;
						}
						objVal[i].costequationvars = tmpArr.join("; ");
					}
				}
				catch (e)
				{
					
				}
				return objVal;
			},
			parse : function(data) {
				return data;
			}
		},
		requestStart: function(e) {
	    },
		change: function(e) {
	    }
	});
	
	
	var listView = $("#resultsList").kendoListView({
        dataSource: shipMethDataSource,
        template: kendo.template($("#listTemplate").html()),
        editTemplate: kendo.template($("#editTemplate").html()),
        navigatable: true,
        selectable: false,
        edit: function(e) {
        	$("#tabStrip").kendoTabStrip({ animation: false  }).css('border','none');
        },
        editable: true
    }).data("kendoListView");
	
	$("#resultsList").delegate(".k-edit-button", "click", function(e) {
        listView.edit($(this).closest(".list-box-view"));
        e.preventDefault();
    }).delegate(".k-delete-button", "click", function(e) {
    	var thisClosest = $(this).closest(".list-box-view");
    	showConfirmationModal("You are about to delete this Shipping Method, are you sure?", function() {
    		listView.remove(thisClosest);
    	});
        e.preventDefault();
    }).delegate(".k-update-button", "click", function(e) {
        listView.save();
        shipMethDataSource.fetch();
        e.preventDefault();
    }).delegate(".k-cancel-button", "click", function(e) {
        listView.cancel();
        e.preventDefault();
    });
	
    $(".k-add-button").click(function(e) {
        listView.add();
        e.preventDefault();
    });
    
    $("#fetchChildsBtn").live("click",function(e) {
		e.preventDefault();
		var thisClosest = $(this).children("#fetchChildsVal").html();
		var thisClosestTitle = $(this).children("#fetchChildsTitle").html();
		shipMethDataSource.options.transport.read.url = shipMethodsUrl+"&parrent="+thisClosest;
		shipMethDataSource.transport.options.read.url = shipMethodsUrl+"&parrent="+thisClosest;
		$("#parrentBreadcrumb").removeClass("hidden");
		$("#parrentBreadcrumb").html($("#parrentBreadcrumb").html()+" "+thisClosestTitle+" > ");
		$("#backShipMethodBtn").show();
		shipMethDataSource.fetch(function(data) {
			//console.log(data);
		});
	});
    
    $("#resetShipMethodBtn, #backShipMethodBtn").live("click",function(e) {
		e.preventDefault();
		$("#backShipMethodBtn").hide();
		$("#searchBox").data("kendoAutoComplete").value("");
		shipMethDataSource.options.transport.read.url = shipMethodsUrl;
		shipMethDataSource.transport.options.read.url = shipMethodsUrl;
		$("#parrentBreadcrumb").html("Childs Of: ");
		$("#parrentBreadcrumb").addClass("hidden");
		shipMethDataSource.fetch (function(data) {
			//console.log(data);
		});
	});
    
});

function fetchShipMethodInfo(shipmethodId)
{
	var shipmethodInfo = new mcms.dataStore({
    	method: "POST",
    	url: shipMethodsUrl,
    	dataType: "json",
 		trigger:"loadComplete",
 		data: {
 			id : shipmethodId,
 		},
	});
	
	var shipmethodInfoWindow = $("<div />").kendoWindow({
		title : "Shipping Method Settings",
		modal : true,
		visible : false,
		resizable : true,
		width : 500
	}).data("kendoWindow").center();
		
	$(shipmethodInfo).bind('loadComplete',function(){
		var tmpTempl = kendo.template($("#shipmethodModalTemplate").html());
		shipmethodInfoWindow.content(tmpTempl($.parseJSON(shipmethodInfo.records.data.settings)));
		shipmethodInfoWindow.center().open();
	});
	
	$("#shipmethodModalSaveBtn").live("click", function(e) {
        e.preventDefault();
        
        shipmethodInfo.data = {
        		id : shipmethodId,
        		action : "modifysettings",
        		"accid": $("#ShippingSettingsAccid").val(),
        	    "accname": $("#ShippingSettingsAccname").val(),
        	    "key": $("#ShippingSettingsKey").val(),
        	    "apikey" : $("#ShippingSettingsApiKey").val(),
        	    "authkey": $("#ShippingSettingsAuthKey").val(),
        	    "email": $("#ShippingSettingsEmail").val(),
        	    "uname": $("#ShippingSettingsUname").val(),
        	    "pass": $("#ShippingSettingsPass").val(),
        	    "sucurl": $("#ShippingSettingsSucurl").val(),
        	    "failurl": $("#ShippingSettingsFailurl").val(),
        	    "cburl": $("#ShippingSettingsCBurl").val(),
        	    "extra": $("#ShippingSettingsExtra").val(),
        	    "shipping_processor": "",
        };
        shipmethodInfo.trigger = "modifySettings";
        
        $(shipmethodInfo).bind('modifySettings',function(){
        	shipmethodInfoWindow.close();
    	});
        $(shipmethodInfo).loadStore(shipmethodInfo);
    });
	
    $("#shipmethodModalCancelBtn").live("click", function(e) {
        e.preventDefault();
        shipmethodInfoWindow.close();
    });
	
	$(shipmethodInfo).loadStore(shipmethodInfo);
}





/******************************* /
$("#searchBox").kendoAutoComplete({
	minLength : 1,
	autoBind: false,
	dataTextField : "text",
	dataValueField: "id",
	placeholder : "Enter Shipping Method Name...",
	//separator : ", ",
	filter : 'contains', // startswith, endswith
	suggest: true,
	// template: kendo.template(" "),
	dataSource : comboBoxDataSource,
	delay : 500,
	height : 500,
	select : function(e) {
		var value = this.value();
        if (parseInt(value)>0) {
        	//console.log(listView.element.children().first());
        	listView.select($(".list-item-"+parseInt(value)));
        }
	},
	change : function(a) {
	},
	close : function() {
	},
	open : function() {
	}
});
$("#searchBox").data("kendoAutoComplete").value(" ");

// TODO
$("#searchCombo").kendoComboBox({
    dataTextField: "text",
    dataValueField: "id",
    autoBind: false,
    //optionLabel: "All",
    placeholder : "Filter by Shipping Method...",
    dataSource: comboBoxDataSource,
    change: function() {
        var value = this.value();
        if (parseInt(value)>0) {
        	//console.log(listView.element.children().first());
        	listView.select($(".list-item-"+parseInt(value)));
        }
    }
});

/******************************* /
var panelPager = $(".k-pager-wrap #pager").kendoPager({
	dataSource : shipMethDataSource,
});
/*******************************/