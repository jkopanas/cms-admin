head.ready(function() {
	var plainDataSource = new kendo.data.DataSource({
		type : "json",
		serverPaging : true,
		serverSorting : true,
		serverFiltering : true,
		pageSize : 40,
		transport : {
			read : {
				url : "/ajax/loader.php?boxID=orders"
			},
			parameterMap : function(data, option) {
				if (option == "read") {
					return data;
				}
				return data;
			}
		},
		schema : {
			model : {
				id : "id",
				fields: {
					id: {
                       nullable: true
                    },
                    email: {
                    	type: "string",
                       nullable: true
                    },
                    status: {
                        nullable: true
                    },
				},
			},
			data : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = obj.data.values;
				
				return jQuery.parseJSON(obj.data.values);
			},
			parse : function(data) {
				/*
				d = [];
				$.each(data, function(index, value) {
					value.active = 1;
					d.push(value);
				});
				*/
				return data;
			}
		// type: "jsonp"
		},
		requestStart: function(e) {
	    },
		change: function(e) {
	    }
	});
	/*******************************/
	$("#resultsGrid").kendoGrid({
		dataSource : plainDataSource,
		height : 400,
		pageSize: 40,
		sortable : true,
		navigatable : true,
		pageable: true,
		//selectable : 'row',
		scrollable : {
			virtual : false
		},
		toolbar: [ 
		           { name: "archive", text: "Show Archived", template: '<a class="k-button k-button-icontext " id="showArviveOrdersBtn"><span class=" "></span>Show Archived</a>' },
		           { name: "ordersSum", template: '<span class="gridText">Sum: </span>' },
		],
		filterable: true,
		columns : ( function(data) {
			gridColumns = [];
			gridColumns[gridColumns.length] = { field: "id", title: "#ID", filterable: false };//, template: '<a href="?entity=order&action=modify&id=${id}" >${id}</a>'};
			gridColumns[gridColumns.length] = { field: "email", title: "Email", filterable: false};
			gridColumns[gridColumns.length] = { field: "status", title: "Status", template: function (data) { return orderStatusConverter(data.status); }};
			gridColumns[gridColumns.length] = { field: "amount", title: "Amount"};
			gridColumns[gridColumns.length] = { field: "payment_method", title: "Payment Method", template: function (data) { return data.payment_method.title; }, filterable: false };
			//gridColumns[gridColumns.length] = { field: "payment_method", title: "Payment Description", width: "150px", height: "10px", template: function (data) { return data.payment_method.description; } };
			gridColumns[gridColumns.length] = { field: "shipping_method", title: "Shipping Method", template: function (data) {return data.shipping_method; }, filterable: false };
			gridColumns[gridColumns.length] = { field: "notes", title: "Notes", filterable: false};
			gridColumns[gridColumns.length] = { field : "archive", title: "Archive", editor: optionsDropDownEditor, template: function (data) { return (data.archive == "1") ? "Yes" : "No"; }, filterable: false };
			gridColumns[gridColumns.length] = { command: [{text:"Open", className: "open-button"}], title: "Commands", filterable: false};
			
			return gridColumns;
		})({}),
		editable: "inline",
		/******************************* /
		change : function(e) {
			var currTR = this.current().closest('tr');
			// Close other TRs
			this.collapseRow(currTR.siblings());
			// Check if next tr already is a detail-row (KendoUI hides
			// an existing detail-row on collapseRow)
			if (currTR.next('tr').hasClass('k-detail-row')) {
				var detailTR = currTR.next('tr');
				if (detailTR.is(':visible')) {
					this.collapseRow(currTR);
				} else {
					this.expandRow(currTR);
				}
			} else {
				this.expandRow(currTR);
			}
		}
		/*******************************/
	});
	
	$("#searchCombo").kendoComboBox({
	    dataTextField: "shipping_method",
	    dataValueField: "shipping_method",
	    autoBind: false,
	    optionLabel: "All",
	    dataSource: {
	        type: "jsonp",
	        severFiltering: true,
	        transport: {
	            read: "/ajax/loader.php?boxID=orders"
	        }
	    },
	    change: function() {
	        var value = this.value();
	        if (value) {
	        	$("#resultsGrid").data("kendoGrid").dataSource.filter({
	                field: "shipping_method",
	                operator: "eq",
	                value: parseInt(value)
	            });
	        } else {
	        	$("#resultsGrid").data("kendoGrid").dataSource.filter({});
	        }
	    }
	});
	
	var detailedOrderWindow = $("#details").kendoWindow({
		title : "Order Details",
		modal : true,
		visible : false,
		resizable : true,
		width : 500
	}).data("kendoWindow").center();
	
	 var detailsTemplate = kendo.template($("#modalTemplate").html());
	 
	$("#resultsGrid").delegate(".open-button", "click", function(e) {
		e.preventDefault();
		var dataItem = $("#resultsGrid").data("kendoGrid").dataItem($(this).closest("tr"));
		detailedOrderWindow.content(detailsTemplate(dataItem));
		detailedOrderWindow.center().open();
		
		$("#orderStatusList").kendoDropDownList({
	        dataTextField: "text",
	        dataValueField: "value",
	        autoBind: true,
	        dataSource: [
	            { text: "Έναρξη παραγγελίας", value: "1" },
	            { text: "Σε αναμονή", value: "2" },
	            { text: "Αναμονή από κάρτα", value: "3" },
	            { text: "Επιβεβαίωση παραγγελίας", value: "4" },
	            { text: "Προετοιμασία", value: "5" },
	            { text: "Αποστολή", value: "6" },
	            { text: "Ολοκληρώθηκε", value: "7" },
	            { text: "Ακυρώθηκε", value: "8" },
	        ]
	    });
	});
	
	
	$("#orderArchiveBtn").live("click",function(e) {
		e.preventDefault();
	    
		var thisOrderID = $("#orderArchiveBtn #orderArchiveVal").html();
		
	    var confirmationModal = $("<div />").kendoWindow({
	            title: "Confirm",
	            resizable: false,
	            modal: true,
	        });
	    
	    confirmationModal.data("kendoWindow")
	        .content($("#confirmationModal").html())
	        .center().open();
	    
	    confirmationModal.find(".confirmationConfirmBtn,.confirmationCancelBtn")
            .click(function() {
                if ($(this).hasClass("confirmationConfirmBtn")) {
                	var orderArchiveReq = new mcms.dataStore({
                    	method: "GET",
                    	url:"/ajax/loader.php?boxID=orders",
                    	dataType: "json",
                 		trigger:"loadComplete",
                 		data: {
                 			action : "archive",
                 			id : thisOrderID,
                 		},
                	});
                	$(orderArchiveReq).bind('loadComplete',function(){
                		//console.log(orderArchiveReq.records);
                		confirmationModal.data("kendoWindow").destroy();
                		
                		//detailedOrderWindow.restore();
                		detailedOrderWindow.close();
                	});
                	$(orderArchiveReq).loadStore(orderArchiveReq);
                }
                confirmationModal.data("kendoWindow").close();
            }).end();
	});
	
	$("#showArviveOrdersBtn").live("click",function(e) {
		e.preventDefault();
		plainDataSource.options.transport.read.url += "&archived=1";
		plainDataSource.transport.options.read.url += "&archived=1";
		plainDataSource.fetch (function(data) {
			//console.log(data);
		});
	});
	
});



function optionsDropDownEditor(container, options) {
    
    $('<input data-text-field="active" data-value-field="value" data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        autoBind: false,
                        dataSource: [{ id: 1, active: "Yes", value: 1 }, { id: 2, active: "No", value: 0}]
                    });

}

function fetchOrderInfo(orderId)
{
	var orderInfo = new mcms.dataStore({
    	method: "POST",
    	url:"/ajax/loader.php?boxID=orders",
    	dataType: "json",
 		trigger:"loadComplete",
 		data: {
 			id : orderId,
 		},
	});
	
	$(".orderDetailsContainer").html(" ");
	$(".orderDetailsContainer").addClass("ac_loading");
	$(orderInfo).bind('loadComplete',function(){
		console.log(orderInfo.records);
		$(".orderDetailsContainer").removeClass("ac_loading");
		var tmpTempl = kendo.template($("#extraDetails").html());
		console.log($.parseJSON(orderInfo.records.data.values).details);
		$(".orderDetailsContainer").html(tmpTempl($.parseJSON(orderInfo.records.data.values)));
	});
	
	$(orderInfo).loadStore(orderInfo);
}

// orderStatusCodes is initiated at index page - inlineTempalte
function orderStatusConverter(id)
{
	return (orderStatusCodes[id]? orderStatusCodes[id] : "Αγνωστο");
}



