var eshopObserver = {
	method : "POST",
	url : "",
	dataType : "json",
	trigger : "loadComplete",
	data : {},
	url : "",
};

var eshopControler = {
	method : "GET",
	url : "/ajax/loader.php?module=eshop&file=eshop/admin2/",
	data : {},
	trigger : "onSuccesLoad"
};

var lastReq = "index";
var lastBoxReq = "index";
var lastViewReq = "index";

$(eshopControler).bind("onSuccesLoad", function() {
	if(this.records)
	{
		lastViewReq = (lastBoxReq = lastReq);
		//$("#resultsGrid").html(this.records);
		$("#adminDrawableArea").html(" ");
		$("#adminDrawableArea").html(this.records);
		return;
	}
	$(eshopControler).trigger("loadError");
});

$(eshopControler).bind("loadError", function() {
	$('body').append("<div id='errorMdal' >Error Loading Page</div>");
	var wnd = $("#errorMdal");
	wnd.kendoWindow({
		// width: "750",
		// height: "45%",
		title : "Page Error",
		// content: "Error Loading Page",
		close : closed,
		visible : true,
		modal : true
	});
	wnd.data("kendoWindow").center();
});

head.ready(function(){
	eshopControler = new mcms.dataStore(eshopControler);
	eshopObserver = new mcms.dataStore(eshopObserver);
});

	$(window).bind('hashchange', function(e, b, c) {
	//e.preventDefault();
	if((window.location.hash.indexOf("#!/")>=0)) {
		eshopControler.data.fetch = window.location.hash.replace("#!/", "");
		eshopControler.url = "/ajax/loader.php";
		eshopControler.data = {
				module : "eshop",
				boxID : (eshopControler.data.fetch ? eshopControler.data.fetch : "index"),
				view : (eshopControler.data.fetch ? eshopControler.data.fetch : "index")
		};
		//console.log(eshopControler);
		lastReq = eshopControler.data.fetch;
		head.ready( function() {
			$(eshopControler).loadStore(eshopControler);
		});
	}

});

$(window).trigger("hashchange");
	



/*******************************************************************
* O => YOU ARE HERE
* 
* ViewContrl -> View Controler; controler.js
* View -> the View of the box; tpl render
* ModContrl -> the modular_box controler; loader.php
* Module -> the modular_box; The php file of the box
* 
* A View request is been answered, as the view asks for its data. 
* Data are been processed by the modular box and returned to view
* witch should render the fetched json.
*
* 		ViewContrl			View		ModContrl			Module
* --------------------------------------------------------------------------
* -REQVIEW->_I_				 |				 |				 |
* 			|O|	Call View	 |				 |				 |
* 			|O|---------------------------->_I_				 |
* 							 | Smarty Displ | |Fetch view	 |
* 							_I_<------------| |				 |
* 		Ajax Display		| |				 |				 |
* <-------------------------| |				 |				 |
* 							 |				 |				 |
* 							_I_				 |				 |
* 							| |Call loader	 |				 |
* 							| |------------>_I_				 |
* 							 |				| |Ajax-Load	 |
* 							 |				| |------------>_I_
* 							 |					Return Data	| | Fetch Data
* 							_I_<----------------------------| |
* 							| |Render Data
* 			Display Data	| |
* <-------------------------| |
*
* A Request is been handled, as the command should be proccesed 
* and return its state.
*
* -REQ-CMD---Module+Comonent+RequestCMD---->___
* 											| |Ajax-Load
* 											| |------------>___
* 												Return Data	| | Process CMD 
* 							_I_<----------------------------| | & Fetch Data
* 							| |Render Data
* 			Display Data	| |
* <-------------------------| |
*
*******************************************************************/


/*
 {
 $("<div style=' color: #9F6000; background-color: #FEEFB3; width: 300px; margin: auto; padding:5px; text-align:center; border:solid 1px #DEDEDE; '><h1>Load Error</h1> Failed to fetch page</div>")
 .css({
 "display" : "block",
 "opacity" : 0.96,
 "top" : $(window).scrollTop() + 100
 }).appendTo($('#adminCanvasWrapper')).delay(1500).fadeOut(400,
 function() {
 $(this).remove();
 });
 });
 */
