$(document).ready(function () {
    var sharableDataSource = new kendo.data.DataSource({
        type:"json",
        serverPaging:true,
        serverSorting:true,
        serverFiltering:true,
        sort:[
            { field:"LastName", dir:"asc" }
        ],
        pageSize:40,
        transport:{
            read:{
                url:"/ajax/eshop/admin/testdata-names.php"
            }
        }
    });

    var companyDataSource = new kendo.data.DataSource({
        type:"json",
        serverPaging:true,
        serverSorting:true,
        serverFiltering:true,
        sort:[
            { field:"Name", dir:"asc" }
        ],
        pageSize:40,
        transport:{
            read:{
                url:"/ajax/eshop/admin/testdata-cat.php"
            }
        }
    });

    $("#searchCombo").kendoComboBox({
        autobind:false,
        filter:"startswith",
        minLength:1,
        placeholder:"Filter by company...",
        dataTextField:"Name",
        dataValueField:"ID",
        dataSource:companyDataSource,
        change:function (event) {
            // Todo: figure out why change event fires twice.
            // checking for the first one that has the correct this.value() information
            if (this.current() !== null) {
                sharableDataSource.filter({
                    field:"Company.ID",
                    operator:"eq",
                    value:this.value()
                })
            }
        }
    });
    
    $("#resultsGrid").kendoGrid({
        dataSource:sharableDataSource,
        detailTemplate:kendo.template($("#gridTemplate").html()),
        height:400,
        sortable:true,
        navigatable:true,
        selectable:'row',
        // Todo: virtual true doesn't render under Safari iPad 2
        scrollable:{
            virtual:true
        },
        columns:[
            { title:"Last Name", field:"LastName" },
            { title:"First Name", field:"FirstName" },
            { title:"Company", field:"CompanyName", sortable:false }
        ],
        change:function (e) {
            var $currTR = this.current().closest('tr');

            // Close other TRs
            this.collapseRow($currTR.siblings());

            // Check if next tr already is a detail-row (KendoUI hides an existing detail-row on collapseRow)
            if ($currTR.next('tr').hasClass('k-detail-row')) {
                var $detailTR = $currTR.next('tr');
                if ($detailTR.is(':visible')) {
                    this.collapseRow($currTR);
                }
                else {
                    this.expandRow($currTR);
                }
            }
            else {
                this.expandRow($currTR);
            }
        }
    });
    
});