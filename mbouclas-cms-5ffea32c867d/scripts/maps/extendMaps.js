$.extend(gMap.prototype, {
	drawingManager : function(options) {

			var settings = $.extend( {
				circleOptions: {
		            fillColor: '#ffff00',
		            fillOpacity: 1,
		            strokeWeight: 5,
		            clickable: false,
		            editable: true,
		            zIndex: 1
	          },
	          markerOptions: {
	            icon: 'images/beachflag.png'
	          },
	         drawingControlOptions: {
	 		 position: google.maps.ControlPosition.TOP_CENTER,
    		drawingModes: [google.maps.drawing.OverlayType.MARKER, google.maps.drawing.OverlayType.CIRCLE]
	          },
	          drawingControl: true,
	          drawingMode: google.maps.drawing.OverlayType.MARKER
			}, options);
			console.log(settings)
	          manager = new google.maps.drawing.DrawingManager(settings);
	          
	          manager.setMap(this.getMap());
	},//END MANAGER
	setDrawingModes : function(types) {
	    var t = new Array;
	    for (var i=0; i<types.length; i++) {
	    	var b = types[i].toUpperCase()
	    	t.push(google.maps.drawing.OverlayType[b]);
	    }
	    return t;
	},
	setPosition : function(pos) {
		return google.maps.ControlPosition[pos.toUpperCase()];
	}
});

