var gMap = function( element,options){
  	var elem = $(element);
  	var obj = this;
	var map = {}; 
	var gMarkers = [];
	var markersList = {};
	var geocoderResults = {};
	var markerCluster = {};
	var boundingBoxes = new Array()
	
	var settings = $.extend( {
			zoom: 8,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControl: true,
			zoomControlOptions : {style: google.maps.ZoomControlStyle.DEFAULT},
			elements: '#list .maplocation', //links selector
			map_opt: {},          // custom map options object
			clat: 35.01107294249947,        // set the lat default map center
			clng: 33.1880304,               // set the lng default map center
			mapstyle_name: '',              // custom map style label and id
			mapstyle: '',                   // mapstyle object
			cluster_styles: {},             // custom cluster icons object
			marker_icon: '',                // custom marker icon url
			infowindows: true,              // shows infoWindows grabing html from the .infobox element
			infobox: false,                 // enable custom infoWindows using infobox class
			infobox_s: {},          	    // default color scheme for custom infobox container
			trigger: 'map_open',            // you can set a event trigger for each link/marker
			clickedzoom: 15,                // set the zoom level when you click the single marker
			timeout: 100,                   // delay between click and zoom on the single marker
			mode: 'latlng',                 // switch mode
			wait: 500,                      // timeout between geocode requests
			maxtry:10,                      // limit of time to bypass query overlimit
			cat_style: {},                  // costum icons and click zoom level
			createMarker: false,                  // costum icons and click zoom level
			fitbounds: true,                // on|off fit bounds
			defzoom : 20,                   // default zoom level if fitbounds is off
			showzoom: false,                // bind current map zoom level event
			before: function () {},         // before create map callback
			after: function () {},          // after create map callback 
			afterUpdate: function () {}     // after update map callback
			
		}, options);
    
	this.init = function( ) {
        	map.center = new google.maps.LatLng(settings.clat, settings.clng);
        	settings.center = map.center;
        	map.obj = new google.maps.Map(element, settings);
			map.bounds = new google.maps.LatLngBounds();
			
			if (settings.createMarker)
			{
				this.createMarker(settings.createMarker)
			}
			
			if (settings.mapListeners)
			{	
				$.each(settings.mapListeners, function(key, val) {
					google.maps.event.addListener(map.obj, val.listener, function() {
					callbackHandler(val.callback,$.extend({}, map.obj,{ event:val.listener}))
					});
				});
			}//END LISTENERS
			

     }
     
     this.getMap = function() { return map.obj; }
	
	 this.refreshMap = function() {var c = map.obj.getCenter();google.maps.event.trigger(map.obj,'resize'); map.obj.setCenter(c); } 
	
	 this.Markers = function(){return gMarkers;}
     this.createMarker = function(options) {
	
		var opts = $.extend({}, {		
			draggable:false,
			map: map.obj,
			position: map.center
		}, options);
		

		var marker = new google.maps.Marker(opts);

		if (opts.markerListeners)
		{	
			$.each(opts.markerListeners, function(key, val) {
				google.maps.event.addListener(marker, val.listener, function() {
					if (val.ListenerReverseGeoCoding)//REVERSE GEOCODING LISTENER
					{
						ListenerReverseGeoCoding(val.listener,$.extend({}, {marker : marker }, settings),val.callback);
					}//END REVERSE GEOCODING
					else {
						callbackHandler(val.callback,$.extend({}, marker,{ event:val.listener,map:this.gMap}));
					}
				});
			});
		}//END LISTENERS
		
		if (opts.category)
		{
			gMarkers.push(marker);
		}
		
		if (opts.infoWindow)
		{
			createInfobox(marker,opts.infoWindow)
		}//END INFO WINDOW
		
		if(typeof opts.onComplete == 'function'){//CALLBACK FUNCTION
      		opts.onComplete.call(this, marker);
    	}
		
		return marker;
     }//END createMarker
     
     var createInfobox = function(marker,options) {
     	var opts = $.extend({
		 content: "",
		 disableAutoPan: false,
		 maxWidth: 0,
		 zIndex: null,
		 isHidden: false,
		 infoWindowEvents : {},
		 pane: "floatPane",
		 enableEventPropagation: false
		}, options);
		
	 	var infowindow = new google.maps.InfoWindow(opts);

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map.obj,marker);
			if (opts.infoWindowEvents.setZoom)
			{
					map.obj.setZoom(opts.infoWindowEvents.setZoom)
			}
			if (opts.infoWindowEvents.MapTypeId)
			{
					map.obj.setMapTypeId(settings.infoWindowEvents.MapTypeId)
			}
			
			map.obj.setCenter(marker.getPosition());
			
		});
		google.maps.event.addListener(map.obj, 'click', function() {
		  infowindow.close();
		});
	 	
		return infowindow;
     }//END createInfobox
     
	this.geoCode = function(options,callback) {
	
		var opts = $.extend({}, {language:'en',
			region:'US',
			address: ''}, options);
	
		if ( typeof opts.focus  == 'undefined' ) { opts.focus=true;}
		
		 geocoder = new google.maps.Geocoder();
		
		if (geocoder) {
		  geocoder.geocode( opts.geocoder, function(results, status) {
		    if (status == google.maps.GeocoderStatus.OK) {
		    	
		    if (opts.focus) {
			    map.obj.setCenter(results[0].geometry.location);
				DrawBoundingBox(results[0],opts);
	
				var markerOpts = $.extend({}, {position: results[0].geometry.location,
						draggable:false,
						infoWindow: {content: results[0].formatted_address},
						infoWindowListener: 1}, opts.markers);

			      var marker = obj.createMarker(markerOpts);
				//Clear old markers
				var id = (markersList.length) ? markersList.length + 1 : 1;
				clearMarkers(markersList);
				markersList[id] = marker;
				results[0]['marker'] = marker;
		    }

		      if(opts.ListenerReverseGeoCoding)
		      {
		      	ListenerReverseGeoCoding({marker:marker,settings:opts})
		      }//END REVERSE GEOCODING LISTENER
		      

				if (opts.addListeners)
				{
					$.each(opts.addListeners, function(key, val) {
						google.maps.event.addListener(map.obj, val.listener, function() {
						callbackHandler(val.callback,$.extend({}, map.obj,{ event:val.listener}))
						});
					});
			}//END LISTENERS
				if (callback)
				{
					callbackHandler(callback,results);
				}
		    } else {
		      callbackHandler(opts.notfound,results);
		      alert("Geocode was not successful for the following reason: " + status);
		    }
		  });//END GEOCODE
		}//END GEOCODER
	
	}//END FUNCTION
     
      
     var ListenerReverseGeoCoding = function(event,options,callback) {
		//Add listener to marker for reverse geocoding
		var point = options.marker;
		var me = this;
		var latlng = point.getPosition();
		var opts = $.extend({}, {'latLng': new google.maps.LatLng(latlng.lat(), latlng.lng())}, options.geocoder);
		if (!this.geocoder)
		{
			geocoder = new google.maps.Geocoder();
		}
		
		geocoder.geocode( opts, function(results, status) {
		  if (status == google.maps.GeocoderStatus.OK) {
		    if (results[0]) {
		    	var ret = $.extend({}, point,{ event:event,map:me.gMap});
				callbackHandler(callback,$.extend({}, {results : results[0]},ret))
		    }
		  }
		});
	
	}//END REVERSE GEOCODING
     
    var clearMarkers = function(list)
    {
    	
    	if (list) {
    		$.each(list, function(key, val) {
    			list[key].setMap(null);
    		});

	  	}	
    }
    
    this.showMarkerCategory =function(category)
    {
        for (var i=0; i<gMarkers.length; i++) {
          if (gMarkers[i].category == category) {
            gMarkers[i].setVisible(true);
          }
        }
        // == check the checkbox ==
//        document.getElementById(category+"box").checked = true;
    }
   
    this.hideMarkerCategory =function(category)
    {
    	
        for (var i=0; i<gMarkers.length; i++) {
          if (gMarkers[i].category == category) {
            gMarkers[i].setVisible(false);
          }
        }
        // == check the checkbox ==
//        document.getElementById(category+"box").checked = true;
    }    
     
    this.destroyMarkers = function()
    {
     console.log(gMarkers);
     var m =this.Markers();
     $.e<br />ach(m, function(key, val) {
      gMarkers[key].setMap(null)
      console.log(gMarkers[key])
     })
/*        for (var i=0; i<m.length; i++) {
            m[i].setMap(null);
            console.log(m[i])
            m[i] = null;
        }*/
    }
     
    this.clearBoundingBoxes = function()
	{
	  for (i in this.boundingBoxes) {
	      this.boundingBoxes[i].setMap(null);
	    }
	}
     
    var callbackHandler = function(func)
	{
		if (this[func])
		{
			window[func].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else { console.log(func + ' is not registered'); }
	}
	
     var DrawBoundingBox = function(res,options){
			 var ne = res.geometry.viewport.getNorthEast();
		     var sw = res.geometry.viewport.getSouthWest();
		//          console.log(ne + ' ' + sw);
		      map.obj.fitBounds(res.geometry.viewport); 
		      var boundingBoxPoints = [
		        ne, new google.maps.LatLng(ne.lat(), sw.lng()),
		        sw, new google.maps.LatLng(sw.lat(), ne.lng()), ne
		     ];
		     var boundingBox = new google.maps.Polyline({//This will draw the geocoder results box
		        path: boundingBoxPoints,
		        strokeColor: '#FF0000',
		        strokeOpacity: 1.0,
		        strokeWeight: 2
		     });
			//clear old ones
			clearBoundingBoxes();
			boundingBoxes.push(boundingBox);
		     boundingBox.setMap(map.obj);
		     
			return boundingBoxPoints;
     }

     var clearBoundingBoxes = function()
	{
	  for (i in boundingBoxes) {
	      boundingBoxes[i].setMap(null);
	    }
	}
}//END CLASS gMap
  
  $.fn.googleMaps = function( options ) {
       return this.each(function()
       {
           var element = $(this);
           // Return early if this element already has a plugin instance
           if (element.data($(this).attr('id'))) return;
			
           // pass options to plugin constructor
           var gm = new gMap(this, options);
           // Store plugin object in this element's data
           element.data($(this).attr('id'), gm);
       });
  };




