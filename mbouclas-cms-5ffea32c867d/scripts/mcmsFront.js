$.fn.mCms = function(options) {
	var mcms = function(element, options) {
		var l = {};
		var baseSettings = $.extend({
			lang : 'en',
			module : 'content'
		}, options);
		this.settings = baseSettings;
		this.dataStore = function( ds ) {
			$.fn.loadStore = function(ds){
				$.ajax({
					type: ds.method,
					url: ds.url,
					data: ds.data,
					dataType: ds.dataType,
					cache:false,
	                async:false,
	                timeout:ds.timeout?ds.timeout:10000,				
					queue: "autocomplete",
					
					contentType:'application/x-www-form-urlencoded;charset=utf-8',
					accepts: {
						xml: "application/xml, text/xml",
						json: "application/json, text/json",
						_default: "*/*"
					},
					beforeSend:function(){
						loadStatus = true;
						return this; // Just in case
					},
					success: function(data) {
						loadStatus = false;
						if(data)
						{
							ds.records=data;
						}
						$(ds).trigger(ds.trigger);
						return this; // Just in case
					},				
					error: function()
					{
						loadStatus = false;
						$(ds).trigger('loadError');
						return this; // Just in case
					}
				});// END AJAX
			};// END LOADSTORE

			try {
				return ds;
			} 
			finally {
				ds = null;
			}
	
			return this; // Just in case
		}// END METHOD
			
		lang = new this.dataStore({ url:'/scripts/lang/lang.'+baseSettings.lang+'.json',trigger:'loadComplete'});
		
		
		$(lang).bind('loadComplete',function(e){
			l = lang.records;
		});
			
		$(lang).loadStore(lang);
			
		this.getLang = function() {
			return l;
		}
			
		this.init = function(options) {
			var settings = $.extend( {
			expandMode : ''
			}, options);
			return this; // Just in case
		}
		
		this.showLang = function() {
			return this.l;
		}
		
		this.callInternalMethod = function(func) {//EXPOSE INTERNAL METHODS ON DEMAND	
			if (typeof eval(func) == 'function')
			{
				return eval(func).apply(this, Array.prototype.slice.call(arguments, 1));
			}
			else { console.log(func + ' is not registered'); }
		}//END METHOD
	
		this.callbackHandler =  function (func) {
			if (window[func]) {
				window[func].apply(this, Array.prototype.slice.call(arguments, 1));
			}
			else {
				console.log(func + ' is not registered'); 
			}
			return this; // Just in case
		}
	
		this.formData = function(obj,options){
			var settings = $.extend( {
				'ref' : 'name',
				returnType : 'array',
				getData: false
			}, options);
	
			obj = $(obj);
			var me = this;
			var all_inputs = new Array();
			var data = {};
			var x = {};
			var i = 0;
			var d = {};
			
			obj.each(function (k,v) {
				// console.log(listAttributes($(this),{prefix:'data-',returnType:'keyVal',stripPrefix:true}));
			    if ($(this).prop('type') == 'radio' || $(this).prop('type') == 'checkbox') {
			        if ($(this).is(':checked')) {
			        	if (strstr($(this).attr(settings.ref),'[]')) { // ARRAY
							if (!$.isArray(x[$(this).attr(settings.ref).replace('[]','')])) {
								x[$(this).attr(settings.ref).replace('[]','')] = Array();
							}
							 x[$(this).attr(settings.ref).replace('[]','')].push( $(this).val());
							 data[$(this).attr(settings.ref).replace('[]','')] = x[$(this).attr(settings.ref).replace('[]','')];
							 // d[$(this).attr(settings.ref).replace('[]','')] =
							 // $('input['+settings.ref+'="'+$(this).attr(settings.ref)+'"]');
							 d[$(this).attr(settings.ref).replace('[]','')] = $(this);
						}
			        	else {
			        		 data[$(this).attr(settings.ref)] = $(this).val();
			        		 d[$(this).attr(settings.ref)] = $(this);
			        	}
			            all_inputs.push($(this).attr(settings.ref) + ':::' + $(this).val());
			            // $(data).attr($(this).attr(settings.ref),$(this).val());
			        }// END IF CHECKED
			    }// END IF
			    else  {
			        if ($(this).val()) {
			        	if (strstr($(this).attr(settings.ref),'[]')) { // ARRAY
			        		if (!$.isArray(x[$(this).attr(settings.ref).replace('[]','')])) {
								x[$(this).attr(settings.ref).replace('[]','')] = Array();
							}
			        		x[$(this).attr(settings.ref).replace('[]','')].push( $(this).val());
			        		data[$(this).attr(settings.ref).replace('[]','')] = x[$(this).attr(settings.ref).replace('[]','')] ;
			        		// d[$(this).attr(settings.ref).replace('[]','')] =
							// $($(this).prop('type')+'['+settings.ref+'="'+$(this).attr(settings.ref)+'"]');
							d[$(this).attr(settings.ref).replace('[]','')] = $(this);
						}
			        	else {
			        		 data[$(this).attr(settings.ref)] = $(this).val();
			        		  d[$(this).attr(settings.ref)] = $(this);
			        	}
			            all_inputs.push($(this).attr(settings.ref) + ':::' + $(this).val());
			        }// END IF
			    }// END IF
			    i++;
			});// END FOREACH
	
			if (settings.getData) {
				var m = {};
				$.each(d,function(k,v) {
					if (isArray(v)) { data[k] }
					m[k] = $.extend(listAttributes(v,{prefix:'data-',returnType:'keyVal',stripPrefix:true}),{value:data[k]});
				});
				return m;
			}
			
			if (settings['returnType'] == 'string') {
				return all_inputs;
			}// END IF
			else if (settings['returnType'] == 'array') {
			   return data;  
			}// END IF
			
			return this; // Just in case
		}// END METHOD
		
		var strstr = function (haystack, needle, bool) {
			var pos = 0;
	
			haystack += '';
			pos = haystack.indexOf(needle);
			if (pos == -1) {
				return false;
			} 
			else {
				if (bool) {
					return haystack.substr(0, pos);
				} 
				else {
					return haystack.slice(pos);
				}
			}
			return this; // Just in case
		}// END METHOD
		
		var isArray = function(a) {
			return Object.prototype.toString.apply(a) === '[object Array]';
		}
		
		var mapAttributes = function(el,prefix) {
			var maps = [];
			el.each(function() {
				var map = {};
				for(var key in this.attributes) {
					if(!isNaN(key)) {
						if(!prefix || this.attributes[key].name.substr(0,prefix.length) == prefix) {
							map[this.attributes[key].name] = this.attributes[key].value;
						}
					}
				}
				maps.push(map);
			});
			return (maps.length > 1 ? maps : maps[0]);
		}
			
		var listAttributes = function(el,options) {
			var settings = $.extend( {
				'pefix' : '',
				returnType : '',
				stripPrefix: false
			}, options);
	
			var list = [];

			el.each(function() {
				var attributes = [];
				for(var key in this.attributes) {
					if(!isNaN(key) && this.attributes[key] !== null && typeof(this.attributes[key]) != 'undefined') {
						if(!settings.prefix ||  this.attributes[key].name.substr(0,settings.prefix.length) == settings.prefix) {
							attributes.push(this.attributes[key].name);
						}
					}
				}
				list.push(attributes);
				return this; // Just in case
			});

			if (settings.returnType == 'keyVal') {
				var ret = {};	
				var key ='';
				for(var i in list) {
					if (list[i].length > 0) {
						for(var x in list[i]) {
							key = (settings.stripPrefix) ? list[i][x].replace(settings.prefix,'') : list[i][x];
							ret[key] = el.attr(list[i][x]);
						}
					}
				}
				return ret;
			}
			return (list.length > 1 ? list : list[0]);
		}// END METHOD
		
		this.moneyFormat = function (num,c,d,t) {
			// (123456789.12345).formatMoney(2, '.', ',');
			if (typeof num == 'undefined' || typeof num == 'null') { return ; }	
			Number.prototype.formatMoney = function(c, d, t) {
				var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
			   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		 	};	
		 	
		 	return num.formatMoney(2, '.', ',');
		}
	
		this.app = {
			options : {
				callBack: ''		
			},
			parseAction : function(tag,ret) {
				var p = tag.match(/[#!/]+([a-zA-Z]+)/i,'');
				if (p != null) {
					if (ret) { return p[1]; }			
					return p;// JUST THE ACTION
				}
			},
			parseParams : function(string) {
				var e = this.parseAction(string,1);		
				var params = string.match(/[^/]+/g);
				if (params != null) {
					var x = {};	
					$.each(params,function(k,v){
						if (k%2 ==0) {
							x[v] = v;
							prev = v;
						}
						else {
							x[prev] = v;
						}
					});
					return x;
				}
				return this; // Just in case
			},
			parse : function(tag,options) {
				var settings = $.extend(this.options,options);
				var ret = {};
				var o = this.parseAction(tag);
				if (o != null) {
					ret['action'] = o[1];
					ret['params'] = this.parseParams(tag.replace(o[0],''));
				}
				if (settings.callBack) {
					this.callbackHandler(settings.callBack,ret);
				}
				return ret;
			},
			callbackHandler : function (func)
			{
				if (this[func])
				{
					this[func].apply(this, Array.prototype.slice.call(arguments, 1));
				}
				else { 
					console.log(func + ' is not registered'); 
				}
				return this; // Just in case
			}
		}// END Inner Class
		
		return this; // Just in case
		
	};// END CLASS
	
	return this.each(function() {
	
		var element = $(this);
		// Return early if this element already has a plugin instance
		if (element.data('mcms')) return;
		var c = new mcms(this,options);
		element.data('mcms', c);
		
		return this; // Just in case
	});
}