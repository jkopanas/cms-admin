$.fn.mcmsSearch = function(dataSrc) {
	var localDatasrc = {
			ds : (dataSrc.dataSource? dataSrc.dataSource : {}),
			url: (dataSrc.url? dataSrc.url : window.location.protocol+"//"+window.location.host+window.location.pathname),
			submitBtn: (dataSrc.submitBtn? dataSrc.submitBtn : 'input[type=submit]'),
			searchFilter: (dataSrc.searchFilter? dataSrc.searchFilter : "contains"),
			autoComplete: (dataSrc.autoComplete? dataSrc.autoComplete : {}),
			resultsContainer: (dataSrc.resultsContainer? dataSrc.resultsContainer : "#results"),
			resultsTemplate: (dataSrc.resultsTemplate? dataSrc.resultsTemplate : "#resultsTemplate"),
			searchDataClass: (dataSrc.searchDataClass? dataSrc.searchDataClass : ".SearchData"),
			autoCompleteElement: (dataSrc.autoCompleteElement? dataSrc.autoCompleteElement : "#searchform .SearchData[data-type=filter]" )
	};

	var autoComplete = {};
	if(!jQuery.isEmptyObject(localDatasrc.autoComplete))
	{
		autoComplete = $(localDatasrc.autoCompleteElement).kendoAutoComplete(localDatasrc.autoComplete).data("kendoAutoComplete");
	}
	
	mcms.search = $.extend(true,{
		ds : new kendo.data.DataSource(localDatasrc.ds),
		controller : function(hash,callBack) {
			return this.parse(hash,callBack);
		},
		callAjax : function(options) {
			this.ds.fetch();
			return this;
		},
		doActions : function(e) {
			//localDoActions(e,this);
			e.preventDefault();
			try
			{
				dataSrc.doActions(e,search);
			}
			catch (e) 
			{
				
			}			
			var func = $(e.currentTarget).attr('data-method');
			this.callbackHandler(func,$(e.target));
			return this;
		},
		parseDataForm: function(e) {
			var p = mcms.formData(e.parents('form').find(localDatasrc.searchDataClass),{ getData:true});
			var q = {};
			var returns = {};
			q.filter ={};
			q.filter.filters = {};
			var uri = new Array;
			$.each(p,function(k,v){
				
				if (v.type == 'filter' && v.field != null) {
					q.filter.filters[k] = v;
					uri.push(k+'/'+v.value);
				}
				else {
					if (v.type == 'filter') {
						uri.push(k+'/'+v.value);
					}
					q[k] = v['value'];
				}
			});
			returns.uri = uri;
			returns.q = q;
			
			return returns;
		},
		fullSearchToURL : function(e) {
			var returns = this.parseDataForm(e);
			
			window.location.href = localDatasrc.url+'#!/search/'+returns.uri.join('/');
			this.ds.transport.options.read.data = returns.q;
			this.ds.transport.options.read.data.thumb = 1;
			this.ds.fetch(this.renderResults);
			return this;
		},
		fullSearch : function(e) {
			var returns = this.parseDataForm(e);
			console.log(localDatasrc.url+'#!/search/'+returns.uri.join('/'));
			window.location.href = localDatasrc.url+'#!/search/'+returns.uri.join('/');
			this.ds.transport.options.read.data = returns.q;
			this.ds.transport.options.read.data.thumb = 1;
			
			this.ds.fetch(this.renderResults);
			return this;
		},
		setupSearchForm : function(e) {
			var uri = this.controller(window.location.hash);
			//console.log(uri);
			if (typeof uri.params != 'undefined') {
				e.find(localDatasrc.searchDataClass).each(function(k,v){
					if ($(this).attr('name') in uri.params) {
						if ($(this).prop('type') == 'radio' || $(this).prop('type') == 'checkbox') {
							$(this).attr('checked',true);
						}
						else  {
							$(this).val(uri.params[$(this).attr('name')]);
						}
					}
				});
				this.fullSearch(e.find(localDatasrc.submitBtn));
			}
			return this;
		},
		fetchResults : function(e) {
			return this;
		},
		renderResults :function(e) {
			//this is the actual datasource as we are calling this in a callback
			var t = kendo.template($(localDatasrc.resultsTemplate).html());
			$(localDatasrc.resultsContainer).html(t(this.data()));
			
			return this;
		}
	},mcms.app);
	
	kendo.bind('body', mcms.search);
	
	return mcms.search;
};

/******
 * Example of use
 *
head.ready(function(){
	var ds ={
		url: "/facebook.php",
		submitBtn: "PerformSearch",
		searchFilter: "contains",
		resultsContainer: "#results",
		resultsTemplate: "#resultsTemplate",
		searchDataClass: ".SearchData",
		autoCompleteElement: "#searchform .SearchData[data-type=filter]",
		autoComplete: {
			dataSource: {
				serverFiltering: true,
				serverPaging: true,
				schema:{
					data : function(data) {
						allData = data;
						if (data.results.length == 0) {
							return [];
						}
						return data.results;
					}
				},
				pageSize:10,
				transport: {
					cache: "inmemory",
					read : {
						dataType:'json',
						url :"/ajax/loader.php?file=itemsSearch.php&module=" + mcms.settings.module + "&action=pagesFilter",
						type:'post',
						data:{}
					}
				}
			},
			dataTextField:'title',
			filter: "contains",
		},
		dataSource: {
			serverFiltering: true,
			serverPaging: true,
			schema:{
				data : function(data) {
					if (data.results.length == 0) {
						return [];
					}
					return data.results;
				}
			},
			pageSize:10,
			transport: {
				cache: "inmemory",
				read : {
					dataType:'json',
					url :"/ajax/loader.php?file=itemsSearch.php&module=" + mcms.settings.module + "&action=pagesFilter",
					type:'post',
					data:{}
				}
			}
		}
	};
	var search = $('body').mcmsSearch(ds);
	search.setupSearchForm($('#searchform'));
});//END HEAD

<form name="searchform" id="searchform" method="POST">
	<div class="spacer-bottom">
		<label class="float-left">
			Search phrase
			<input type="text" name="substring" size="30" value="" class="SearchData" data-type="filter" data-field="title" data-operator="contains">
    	</label>
    <div class="clear"></div>
    </div>
    <input type="submit" value="Search" id="PerformSearch" name="PerformSearch" data-bind="click: doActions" data-method="fullSearch">
</form>
<div id="results" > </div>

<script id="resultsTemplate" type="text/x-kendo-template">
# $.each(data,function(k,a) { #
	<div data-id="item-#= a.id #" data-type="webdesign graphics" class="third item">
		# if (typeof a.image_full != 'undefined' && a.image_full != null) { #
			<span class="img_frame"><a href="/#= mcms.settings.lang #/pages/#= a.id #/#= a.permalink #.html" title="#= a.title #"><img src="#= a.image_full.thumb #" title="#= a.title #" alt="#= a.title #"  /></a></span>
		# } #
		<h3>#= a.title #</h3>
		<p>#= a.description.substring(0,70) #</p>
		<a href="/#= mcms.settings.lang #/pages/#= a.id #/#= a.permalink #.html" title="#= a.title #" class="button_readmore">#= $.lang.more #</a>
	</div>
# }); #
</script>

*/