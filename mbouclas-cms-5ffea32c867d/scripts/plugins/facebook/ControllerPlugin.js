$.fn.Controller = function(dataSrc) {
	
	mcms.fbController = $.extend(true,kendo.observable({
		facebookUrl: "http://apps.facebook.com/fbtetrapak/",
		baseUrl: "http://tetrapak.net-tomorrow.com",
		mcmsds: new mcms.dataStore({ method: "POST", url:"/ajax/loader.php?file=plugins/facebook/facebook.php&action=getTemplate", trigger:"loadComplete"}),
		ds : new kendo.data.DataSource(dataSrc),
		doActions : function(e) {		
			e.preventDefault();	
			var func = $(e.currentTarget).attr('data-method');
			this.callbackHandler(func,$(e.target));
			return this;
		},
		actionUrl: function(e) {
		
    		window.top.location.href = this.facebookUrl+"#"+$(e).data('href').substring(1);
    		window.location.hash = "#"+$(e).data('href').substring(1);
			this.ds.transport.options.read.url=$(e).data('href');
			this.ds.fetch(this.renderResults);
			return this;
		},
		AllData: "",
		setAction: function (e) {	
			window.top.location.href = this.facebookUrl+"#"+e.substring(1);
			window.location.hash = "#"+e.substring(1);
   			this.ds.transport.options.read.url=e;
			this.ds.fetch(this.renderResults);		
			return this;
   		},
		renderResults: function(e) {
			$("#load_template").html(mcms.fbController.AllData.data.template);
			var t = kendo.template($("#resultsTemplate").html());
			$("#results").html(t(this.data()));	
			kendo.bind('body', mcms.fbController);
			return this;		
		}
	}),mcms.app);
	
	kendo.bind('body', mcms.fbController);
	

	
	return mcms.fbController;
};