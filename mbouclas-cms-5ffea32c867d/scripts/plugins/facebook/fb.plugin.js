var facebook = '';

head.ready(function() {

	facebook = function(options) {

		var MyApp = '';
		var CurrentUser = '';
		var Status = 0;

		db = new mcms.dataStore({
					method : "POST",
					url : '/ajax/loader.php?file=plugins/facebook/facebook.php',
					dataType : "json",
					trigger : "loadComplete"
				});
				
				
		$(db).bind('InsertFbUser',function(){
			records = db.records;
			control.setAction('/');
		});	
	
		$(db).bind('GetUser',function(){
			records = db.records;
			CurrentUser = records;
			if ( FB.Canvas.getHash ) {
				FB.Canvas.getHash(function(e) {
					if (e) {
						control.setAction("/"+e);
					} else {
						control.setAction('/');
					}
				});
			} else {
				control.setAction('/');
			}
		});

		var settings = $.extend({
					appid : ''
				}, options);

		this.init = function() {

					FB.init({
						appId : settings.appid,
						channelUrl : settings.channelurl,
						status : false, // check login status
						cookie : true, // enable cookies to allow the server to
						xfbml : false, // parse XFBML
						oauth : true
					});
					
					FB.Event.subscribe('edge.create',like);
					FB.Event.subscribe('edge.remove',like);
					FB.Event.subscribe('comment.create',comment);
					FB.Event.subscribe('comment.remove',uncomment);
					FB.getLoginStatus(updateStatus);
	
					
					
		}
		

		var updateStatus = function(response) {

			if (response.status == "connected") {
				db.data = {
					action : "get",
					id : response.authResponse.userID
				}
				db.trigger = "GetUser";
				$(db).loadStore(db);
				
			} else if (response.status == "not_authorized") {
				
 				var oauth_url = 'https://www.facebook.com/dialog/oauth/';
  				oauth_url += '?client_id='+settings.appid;
  				oauth_url += '&redirect_uri=' + encodeURIComponent('http://apps.facebook.com/fbtetrapak/');
  				oauth_url += '&scope=email,user_birthday,status_update,publish_stream,user_about_me,read_stream,user_likes,friends_likes';
  				window.top.location = oauth_url;
				
			} else {
				// O xristis den einai logged in sto fb
				control.setAction('/facebook/ajax/');
			}
			
		}

		this.logout = function() {

			FB.logout(function(response) {
							logout(response);
						
					});

		}

		this.inviteFriends = function(options,callback) {
			 FB.ui($.extend({method: 'apprequests',
    			message: 'Iothetise ton drimo sou !!!!',
 				new_style_message: true
  			},options), function (response) {
				mcms.callbackHandler(callback,response);
			});
		}




		this.GraphStreamPublish = function(options,callback) {
		
			FB.ui($.extend({
				method: 'feed',
				message : '',
				link : '',
				picture : '',
				name : '',
				description : ''
			},options), function (response) {
				mcms.callbackHandler(callback,response);
			});
			
		};

		this.Query = function() {

			FB.api('/me', function(response) {
				var query = FB.Data
						.query(
								'select name, profile_url, sex, pic_small from user where uid={0}',
								response.id);
				query.wait(function(rows) {

						});
			});
		};

		this.getUser = function() {
			return (CurrentUser == '') ? false : CurrentUser;
		}

	};

	// Load the SDK Asynchronously
	(function(d) {
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement('script');
		js.id = id;
		js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));

});
