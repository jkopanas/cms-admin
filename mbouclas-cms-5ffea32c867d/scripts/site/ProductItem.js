head.js('/scripts/kendo/js/kendo.pager.min.js');
head.ready(function(){
/*
	ProductFilter = new mcms.dataStore({
	    method: "POST",
	    url:"/ajax/loader.php?file=user_functions.php&action=add_user",	
    	dataType: "",
		trigger:"GetProduct"
	});


    	UserForm.data = {
    		data: mcms.formData('.adduser',{'ReturnType':'array'}),
    		settings: mcms.formData('.adduserSettings',{'ReturnType':'array'})
    	};
    	$(UserForm).loadStore(UserForm);
*/
    	
	var total_items = '';
    var dataSource = new kendo.data.DataSource({
		transport: {
                  read: {
                           url: "/ajax/products_front.php",
                           data: { 
                           	cat: $("#categoryid").val()
                           },
                           dataType: "json"
                  }
         },
         schema: {
         	 	total: function (data) {
         	 		total_items = data.data.total;
         	 		return  data.data.total;
         	 	},
         		data: "results",
         		model: {
         			 id: "id"
         		}
         },
         serverSorting: true,
   		 sort: { field: "id", dir: "asc" },
     	serverPaging: true,
     	pageSize: "12"
	});
		


/*
	$(".pager-kendo").kendoPager({
    	   dataSource: dataSource,
    	   selectTemplate: '<li class="current">#=text#</li>',
    	   linkTemplate: '<li><a href="\\#" data-#=ns#page="#=idx#">#=text#</a></li>',
    	   messages: {
        		 display: "<div class='float-left'> items  {1} of {2}   </div>",
        		 itemsPerPage: "&nbsp;&nbsp; items"
    	    }
	}).data("kendoPager");
	

	
	var product_view=$("#products-list").kendoListView({
          template: kendo.template($("#template-product-grid").html()),
          dataSource: dataSource
    }).data("kendoListView");
    */
	
    //$("#products-list").removeClass('k-widget');
    //$("#products-list").removeClass();
    //$("#products-list").addClass('products-grid');
    	      
   //var tpl = kendo.template($("#template-mode-theme").html());
  
   //$(".view-mode").html(tpl({ mode: "grid" }));
	
   
   
    $(".head").live('click',function(){
    	$(this).next("ul").toggle();
    	$(this).parents("li:first").toggleClass("selected");
    	
    });
    
    $(".takeItem").live('change',function(){
    	$("input[name=result_per_page]").val($(this).val());
    	$("#toolbarSearch").submit();
    });
    
    	
    $(".SortBy").live('change',function(){
    	//product_view.dataSource.sort({ field: $(this).val(), dir: $(".direction").data('direction') });
    	$("input[name=sort]").val($(this).val());
    	$("#toolbarSearch").submit();
    });
    
    $(".mode").live('click',function() {
    	$("input[name=view]").val($(this).data('value'));
    	$("#toolbarSearch").submit();
    });
    
    $(".direction").live('click',function(){
    	console.log($(this).data('direction'));
    	$("input[name=sort_direction]").val($(this).data('direction'));
    	$("#toolbarSearch").submit();
    });
    
});