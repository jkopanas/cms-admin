
head.ready(function(){
	
		
var ret_user = true;
var user_mail = true;

	
	UserEdit = new mcms.dataStore({
	    method: "POST",
	    url:"/users/update.html",
    	dataType: "",
		trigger:"EditProfile"
	});



       
	viewModel = kendo.observable({
		
		
		editMode: function (e) {	
				e.preventDefault();
				 $.each($(".fieldValue"),function(index,value) {
				 	$(value).html("<input class='"+$(value).data('class')+"' type='"+$(value).data('type')+"' name='"+$(value).data('name')+"' value='"+$(value).data('value')+"' />");
				 });
				 $(".buttons").toggleClass('hidden');

		},
		save: function(e) {
				e.preventDefault();
				UserEdit.data = {
    				mode: "save",
					data:  mcms.formData('.UpdateUser',{'ReturnType':'array'}),
					settings: mcms.formData('.UpdateUserSettings',{'ReturnType':'array'})
    			};
    			$(UserEdit).loadStore(UserEdit);
    			
    			$.each($(".fieldValue"),function(index,value) {
			 		$(value).data('value',$(value).find("input").val());
			 		$(value).html($(value).find("input").val());
			 	});
				$(".buttons").toggleClass('hidden');
				
		},
		close: function(e) {
			e.preventDefault();
			$.each($(".fieldValue"),function(index,value) {
			 		$(value).html($(value).data("value"));
			 });
			 $(".buttons").toggleClass('hidden');
			 
		}
	});
	
	kendo.bind($("#profileContainer"), viewModel);

});