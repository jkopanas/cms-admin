var validateObj ={};

	
var ret_user = true;
var user_mail = true;


head.ready(function(){
	
ValidateObj= {
	rules: {
		checkusername: function(input){
            if (input.is("[name=uname]")) {
            	var username = $("[name=uname]").val();
            	UserForm.url="/check_user.html";
    	 		UserForm.trigger = 'check_username';
    	 	    UserForm.data = { uname: username };
    	 	    $(UserForm).loadStore(UserForm);	 	    
            }
            return ret_user;
		},
		required: function(input){
            if (input.val()=="") {
            	input.css("border-color","red");
            	input.css("border-style","dashed");
            	return false;
            }
            input.css("border-color","#DDDDDD");
            input.css("border-style","solid");
            return true;
		},
		checkemail: function(input){
            if (input.is("[name=email]")) {
            	var email = $("[name=email]").val();
            	UserForm.url="/check_email.html";
    	 		UserForm.trigger ='check_mail';
    	 	    UserForm.data = {email:email}
    	 	    $(UserForm).loadStore(UserForm);
            }
            return user_mail;
		},
        password: function(input){
        	if (!input.is("[name=confirm_password]")) {
        		return true;
        	}
            return input.val() === $("#password").val();
        }
    },
    messages: {
    	checkemail: function(input){
    		if (input.is("[name=email]")) {
    		  return "Tο e-mail χρησιμοποιείται ήδη";
    		} 
    	},
    	checkusername: function(input){
    		if (input.is("[name=uname]")) {
    		  return "Tο όνομα χρήστη χρησιμοποιείται ήδη";
    		} 
    	},
    	required: function (input) {
    		
    		return "Το πεδίο " + $(input).attr('for') + " είναι υποχρεωτικό";
    	},
        password: function (input) {
    		return "Password should be the same";
    	}
    }
};
	
	UserForm = new mcms.dataStore({
	    method: "POST",
	    url: "",
    	dataType: "",
		trigger:"AddUser"
	});




$(UserForm).bind('check_username',function(){
	records = UserForm.records;
	
	 if (records == 'false') {
		 ret_user=false;
	} else { 
		ret_user=true;
	  }
	 
});

$(UserForm).bind('check_mail',function(){
	
	records = UserForm.records;
	if (records == 'false') {
		 user_mail=false;
	} else {
		user_mail=true;
	}	 
});

	
var validatable = $("#signup").kendoValidator(ValidateObj).data("kendoValidator");

$(UserForm).bind('AddUser',function(){
	window.location = '/signup/step1.html'; 
});


$("#submit_form").click(function(e){
	e.preventDefault();
	 if (validatable.validate()) {
		UserForm.url="/ajax/loader.php?file=user_functions.php&action=add_user";
		UserForm.trigger="AddUser";
    	UserForm.data = {
    		data: mcms.formData('.adduser',{'ReturnType':'array'}),
    		settings: mcms.formData('.adduserSettings',{'ReturnType':'array'})
    	};
    	$(UserForm).loadStore(UserForm);
    }
   
 });

});