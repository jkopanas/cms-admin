var cals = {};
$(document).ready(function() {
 db = new $.dataStore({	 
	    method: "POST",
	    url:"/ajax/bookings.php",
//		    dataType: "json",
		trigger:"loadComplete"
	});
	
$(db).bind('addSeason',function(){				
	records = db.records;
	
if (!records) { return ; }
	var m = $.parseJSON(records);
//$('.debug').html(records);
	$.each(m, function(key, value) {
	if (value.period[$(this).val()]) {
		$('.periodID').each(function(){	
			$(this).attr('name','periodID-'+key+'[]');
			$(this).val(value.period[$(this).val()]);
		});
	}
	if (value.season) {

		$('tr#'+key).attr('id',value.season);
		$('.seasonID').each(function(){	
			$(this).val(value.season);
			$(this).attr('name','seasonID-'+key);
		});
		$('#bookingsRates').find('input').each(function(){
			$(this).attr('name',$(this).attr('name').replace('-'+key,'-'+value.season));
		});
	}
	});
});

$(db).bind('loadEditMode',function(){				
	records = db.records;
	$('.debug').html(records);
});

$(db).bind('loadDisplayMode',function(){				
	records = db.records;
	$('.debug').html(records);
});

$('#date').DatePicker({});//Useless, initializes the methods

$('.deleteCol').live('click', function(e){
	e.preventDefault();
    var myIndex = $(this).closest("th").prevAll("th").length;
    $(this).parents("table").find("tr > td.period").each(function(){
    	if ($(this).index() == myIndex)
    	{$(this).remove(); }
    });

    $(this).closest("th").remove();
});

$('.deleteRow').live('click', function(e){
	e.preventDefault();
	$(this).parents('tr').remove();
});
	
$('.addSeason').live('click', function(e){
	e.preventDefault();
	createSeason($('#bookingsRates'));
	$('.addPeriod').show();
});

$('.addPeriod').live('click', function(e){
	e.preventDefault();
	createPeriod($('#bookingsRates'));
});

$('.saveData').live('click', function(e){
	e.preventDefault();
	db.url = '/ajax/bookings.php?action=saveSeasons';
	db.trigger = 'addSeason';
//	db.dataType = 'json';
	db.data = new $.FormData($('#bookingsRates').find('input'),{'ReturnType':'array'})
	$(db).loadStore(db);
});

$('.openCal').live('click', function(e){
	e.preventDefault();
	createCal($(this).attr('rel'));
	return false;
});

$('.editSeasons').live('click', function(e){
	db.url = '/ajax/bookings.php?action=loadEditMode&itemid='+$('input[name=itemid]').val();
	db.trigger = 'loadEditMode';	
	$(db).loadStore(db);
});

$('body').live('click', function(e){
	for ( var i in cals ) {
		$(cals[i]).hide();
	}
});



$("td.season, th#period").live('mouseenter', function() { 
    $(this).find('.deleteItem').show();
  }).live('mouseleave', function () {
    $(this).find('.deleteItem').hide();
});


});//END JQ

function createCal(id) { 
	for ( var i in cals ) {
		$(cals[i]).hide();
	}
	if (id in cals)
	{
		$(cals[id]).show()
		return;	
	}
	var now3 = new Date();
	var el = $('#widget-' + id);
	var now4 = new Date();
	now4.addDays(4);
	cals[id] = el.DatePicker({
		flat: true,
		format: 'd/m/Y',
		date: [new Date(now3), new Date(now4)],
		calendars: 4,
		mode: 'range',
		starts: 1,
		onChange: function(formated) {
			$('#date-'+id).html(formated.join(' &divide; '));
			$('#dateVal-'+id).val(formated.join('###'));
		}
	});
	
	
//	$('#widgetCalendar-'+id+' div.datepicker').css('position', 'absolute');
}

function createSeason(el) {
	
	var x;
	var id = new Date().getTime();
	if (el.find('thead #period').length==0)
	{
		var y = createCell('periodHead',id)
		el.find('thead #season').after(y);
	}
	var cells = el.find('thead#bookings > tr').children();
	for (var i=0;i<=cells.length-1;i++)
	{
		
		x += createCell($(cells[i]).attr('id'),id);
	}
	$('<tr id="'+id+'">'+x+'</tr>').appendTo(el.find('tbody#bookingsBody'));
}

function createPeriod(el) {
	var cells = el.find('thead').children().length;

	var id;
	var x;
	if (el.find('thead #period').length==0)
	{
		var y = createCell('periodHead',id)
		el.find('thead #season').after(y);
	}
	else {
		el.find('thead #period:last').after(createCell('periodHead',id));
	}

	el.find('tbody > tr').each(function(key, val) {
		id =$(this).attr('id');
		if ($(this).find('.period').length == 0) {
			$(this).find("td:eq(0)").after(createCell('period',id));
		}
		else {
			x = $(this).find('.period:last').after(createCell('period',id));
		}
	//		x.next().after(createCell('minStay',id))
		
	});
}

function createCell(type,id){
	if (type == 'season') {
		return '<td class="season"><input type="text" name="season-'+id+'" /><input type="hidden" class="seasonID" name="seasonIDNew-'+id+'" value="'+id+1+'" /> <a href="#" class="deleteRow deleteItem hidden"><img src="/images/admin/delete.gif" border="0" /></a><br /><input type="hidden" name="orderby-'+id+'" value="'+$('.orderbySeason').length+'" class="orderbySeason" /><a href="#calendarContainer" rel="'+id+'" class="openCal">Select date range</a><br><input type="hidden" id="dateVal-'+id+'" name="dateVal-'+id+'"><span id="date-'+id+'"></span><div style="position:relative;"><div id="widget-'+id+'" class="widget"><div id="widgetCalendar-'+id+'" class="widgetCalendar"></div></div><div class="seperator"></div></div></td>';
	}
	else if (type == 'periodHead') {
		var a = parseInt($('.orderbyPeriod').length)+1
		return '<th id="period"><input type="text" name="period[]" /> <a href="#" class="deleteCol deleteItem hidden"><img src="/images/admin/delete.gif" border="0" /></a><input type="hidden" name="orderbyPeriod[]" class="orderbyPeriod" value="'+ a +'" /></th>';
	}
	else if (type == 'period') {
		return '<td class="period"><input type="text" name="price-'+id+'[]" value="" /><input type="hidden" name="periodID-'+id+'[]" class="periodID" value="new-'+parseInt($('.period').length)+1 +'" /></td>';
	}
	else if (type == 'minStay') {
		return '<td><input type="text" name="minStay-'+id+'" value="" /></td>';
	}
	else if (type == 'price') {
		return '<td><input type="text" name="price-'+id+'" /></td>';
	}
}