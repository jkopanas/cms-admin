head.ready(function(){
	var sendData = {};
	var invoice = 1;
	var shippinginfo = 0;
	var viewModel ="";
	var validatableAccountCard = "";
	var validatableInvoiceCard = "";
	var validatableShippingCard = "";
	var obj = {}
	
	eshopcheckout = new mcms.dataStore({
    	method: "POST",
    	url:"/users/update.html",
    	dataType: "json",
		trigger:"loadComplete"
	});


	var accountcardTemplate = kendo.template($("#AccountCard").text());
	var invoiceTemplate = kendo.template($("#receipt_field").html());


	
	var shippinginfoTemplate = kendo.template($("#home_field").html());
	$("#shipping_info").html(shippinginfoTemplate(obj));
	validatableInvoiceCard = $("#payment_method").kendoValidator(ValidateObj).data("kendoValidator");
	validatableShippingCard = $("#shipping_info").kendoValidator(ValidateObj).data("kendoValidator");
	sendData.ShippingInfo = mcms.formData('.AccountDataShippingInfo',{'ReturnType':'array'});
	
	$("#payment_method").html(invoiceTemplate(obj));
	
	var AccountDataSend = mcms.formData('.AccountDataInvoice',{'ReturnType':'array'});
	
	
	var accountcard = $("#AccountCard");
	if (!accountcard.data("kendoWindow")) {
	    accountcard.kendoWindow({
	        width: "490px",
	        title: 'Change Address Delivery',
	        visible:false,
	        modal:true
	    });
	}

	
	
	
$(eshopcheckout).bind('saveContact',function(){
	
	records = eshopcheckout.records;
	viewModel = kendo.observable(records[0]);
	kendo.bind($("#shipping_info"), viewModel);
	sendData.ShippingInfo ={};
	var temp = viewModel.get();
	$.each(temp,function(index,value) {
		if ((index && typeof (temp[index]) != "object") && typeof (temp[index]) != "function" ) {
			sendData.ShippingInfo[index] = value;
		}
	});
	
    accountcard.data("kendoWindow").close();
    
});

$(".CheckoutTab").live('click',function(e){

	$("#checkoutSteps").find(".a-item").hide();
	$("#checkoutSteps").find("li").removeClass("allow active");
	$(this).parents('li').find(".a-item").show();
	$(this).parents('li').addClass("allow active");
	
});


$(".SaveAccount").live("click",function(e) {

	e.preventDefault();
	
 if (validatableAccountCard.validate()) {
 		eshopcheckout.url = "/users/update.html";
		eshopcheckout.trigger = "saveContact";
		eshopcheckout.data = { 
			mode: $(this).data('mode'),
			data:  mcms.formData('.Accountsave',{'ReturnType':'array'}),
			settings: mcms.formData('.AccountsaveSettings',{'ReturnType':'array'})
		}
		$(eshopcheckout).loadStore(eshopcheckout);	
	}
	
});

$(".EditAcount").live("click",function(e) {
		$("#AccountCard").show();
		if (!validatableAccountCard) {
	    	validatableAccountCard = $("#AccountCard").kendoValidator(ValidateObj).data("kendoValidator");
		}
   		accountcard.data("kendoWindow").open();
   		accountcard.data("kendoWindow").center();
});

$(".invoice_method").live("click",function() {
	
	invoiceTemplate = kendo.template($("#"+$(this).attr('id')+"_field").html());
	$("#payment_method").html(invoiceTemplate(obj));
	
	if (!validatableInvoiceCard) {
	    	validatableInvoiceCard = $("#payment_method").kendoValidator(ValidateObj).data("kendoValidator");
	}
	
	invoice = ($(this).attr('id') == "invoice" ) ? 1 : 0;

});

$(".shipping_info").live("click",function() {
	
	shippinginfoTemplate = kendo.template($("#"+$(this).attr('id')+"_field").html());
	$("#shipping_info").html(shippinginfoTemplate(obj));
	shippinginfo = ($(this).attr('id') == "hospital" ) ? 1 : 0;
	
});


$(".payment_method").live("click", function() {
	
	$('.payment_method_div').hide();
	$('.fields').hide();
	$(".shipping_method").attr('checked','');
	$('#payment_method_div'+$(this).val()).show();
	
});


$(".store").live("click", function() {
	$('.desc_store').hide();
	$('#desc-'+$(this).val()).show();
});

$(eshopcheckout).bind('LastStep',function(){
	records = eshopcheckout.records;
	$(".main").html(records.template);
	window.location.href="http://"+location.host+"/en/checkout.html#!FinishCheckout";
	
});


$("#checkOut-Button").live("click", function(e) {
	
	e.preventDefault();
	sendData.save = mcms.formData($('.save'),{ getData:true });
	
	if (invoice == 1) {
 			if (!validatableInvoiceCard.validate()) {
				return;
			 }	
	}
	sendData.AccountData = 	mcms.formData('.AccountDataInvoice',{'ReturnType':'array'});
	
	if (shippinginfo == 1) {
		if (validatableShippingCard.validate()) {
			sendData.ShippingInfo ={};
			sendData.ShippingInfo = mcms.formData('.AccountDataShippingInfoHospital',{'ReturnType':'array'});	
		}
	}
	
	
	
	eshopcheckout.url ="/FinishCheckout.html";
	eshopcheckout.trigger = "LastStep";
	eshopcheckout.data = {
		data:  sendData.save.payment_method.value
	};
	
	$(eshopcheckout).loadStore(eshopcheckout);	
	
});

$(eshopcheckout).bind('complete',function(){
	window.location.href="http://"+location.host+"/en/index.html";
});

$("#Finished").live("click", function(e) {
	e.preventDefault();
	eshopcheckout.url ="/Complete.html";
	eshopcheckout.trigger = "complete";
	eshopcheckout.data = { 
		data:  sendData
	};
	
	$(eshopcheckout).loadStore(eshopcheckout);	
	
});

if (window.location.hash) {
	window.location.href="http://"+location.host+"/en/checkout.html";
}


});//END HEAD