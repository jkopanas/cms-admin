$(document).ready(function(){
languages = new $.dataStore({
method: "POST",
url:"/LoadLanguage.html",
data : {'code' :$('body').find('input[name=loaded_language]').val() },
dataType: "json",
trigger:"loadComplete"
});

$(languages).bind('loadComplete',function(){
	$.lang = languages.records;
});
	
$(languages).loadStore(languages);





$.FormData = function(obj,options){//GATHER VALUES AND RETURN EITHER A STRING OR AN ARRAY
settings = jQuery.extend({//Defaults
ReturnType: 'string'
}, options);//END EXTEND 

var all_inputs = new Array();
var data = {};
var x = {};
var i = 0;

obj.each(function (intIndex) {
    if ($(this).prop('type') == 'radio' || $(this).prop('type') == 'checkbox') {
        if ($(this).is(':checked')) {
        	
        	if (strstr($(this).attr('name'),'[]'))//ARRAY
			{
	
			if (!$.isArray(x[$(this).attr('name').replace('[]','')]))
				{
					x[$(this).attr('name').replace('[]','')] = Array();
				}
				 x[$(this).attr('name').replace('[]','')].push( $(this).val());
				 data[$(this).attr('name').replace('[]','')] = x[$(this).attr('name').replace('[]','')] ;
			}
        	else {
        		 data[$(this).attr('name')] = $(this).val();
        	}
            all_inputs.push($(this).attr('name') + ':::' + $(this).val());
//            $(data).attr($(this).attr('name'),$(this).val());
          
        }//END IF CHECKED
    }//END IF 
    else if ($(this).prop('type') == 'text' || $(this).prop('type') == 'hidden' || $(this).prop('type') == 'textarea' || $(this).prop('type') == 'select-one' || $(this).prop('type') == 'select-multiple' || $(this).prop('type') == 'password' || $(this).prop('type') == 'email' || $(this).prop('type') == 'phone') {
        if ($(this).val()) {
        	if (strstr($(this).attr('name'),'[]'))//ARRAY
			{
			if (!$.isArray(x[$(this).attr('name').replace('[]','')]))
				{
					x[$(this).attr('name').replace('[]','')] = Array();
				}
				 x[$(this).attr('name').replace('[]','')].push( $(this).val());
				 data[$(this).attr('name').replace('[]','')] = x[$(this).attr('name').replace('[]','')] ;
			}
        	else {
        		 data[$(this).attr('name')] = $(this).val();
        	}
            all_inputs.push($(this).attr('name') + ':::' + $(this).val());
			
        }//END IF
    }//END IF
    i++;
});//END FOREACH

if (settings['ReturnType'] == 'string') {
return all_inputs;
}//END IF
else if (settings['ReturnType'] == 'array') {
   return data;  
}//END IF
};//END OBJECT

function strstr (haystack, needle, bool) {
    var pos = 0;

    haystack += '';
    pos = haystack.indexOf(needle);
    if (pos == -1) {
        return false;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
}

function FormString (arr,options){//CREATES A SIMPLE STRING OUT OF AN ARRAY
 settings = jQuery.extend({//Defaults
seperator1: ':::',
seperator2: '###'
}, options);//END EXTEND 
var ret = new Array();
for ( var i in arr )
{
	if (arr[i])
	{
    ret.push(i + settings['seperator1'] + arr[i]);
	}
   
}//END FOREACH

return String(ret.join(settings['seperator2']));
};

function FormArrayFromString (string,options){//CREATES AN ARRAY FROM A SIMPLE STRING
 settings = jQuery.extend({//Defaults
seperator1: ':::',
seperator2: '###'
}, options);//END EXTEND 
var ret = new Array();
var arr = {};
ret = string.split(settings['seperator2']);
for ( var i in ret )
{
    var tmp = ret[i].split(settings['seperator1']);
   arr[tmp[0]] = tmp[1];
   
}//END FOREACH

return arr;
};

function ArrayUnique(arrayName)
            {
                var newArray=new Array();
                label:for(var i=0; i<arrayName.length;i++ )
                {  
                    for(var j=0; j<newArray.length;j++ )
                    {
                        if(newArray[j]==arrayName[i]) 
                            continue label;
                    }
                    newArray[newArray.length] = arrayName[i];
                }
                return newArray;
}

	
	function get_form_data(className)
{
	 var all_inputs = new Array();
            $("."+className).each(function (intIndex) {
//            	console.log($(this))
                if ($(this).prop('type') == 'radio' || $(this).prop('type') == 'checkbox') {
                    if ($(this).attr('checked')) {
                        all_inputs.push($(this).attr('name') + ':::' + $(this).val());
                    }
                }
                else if ($(this).prop('type') == 'text' || $(this).prop('type') == 'hidden' || $(this).prop('type') == 'textarea' || $(this).prop('type') == 'select-one' || $(this).prop('type') == 'password') {
                    if ($(this).val()) {
                        all_inputs.push($(this).attr('name') + ':::' + $(this).val());
                    }
                }
            });
return all_inputs.join('###');
}//END FUNCTION

function array_key_exists (key, search) {

    // input sanitation
    if (!search || (search.constructor !== Array && search.constructor !== Object)) {
        return false;
    }

    return key in search;
}

function in_array (needle, haystack, argStrict) {

    var key = '',
        strict = !! argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }

    return false;
}

});//END HEAD 