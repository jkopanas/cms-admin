head.ready(function(){


$('.products-grid .item').live({
        mouseenter:
           function()
           {
				$(this).find('.product-price').parent().show();
           },
        mouseleave:
           function()
           {
				$(this).find('.product-price').parent().hide();
           }
       }
    );

var cartDialogTemplate = kendo.template($("#cartDialogTemplate").text());


$(".edit_quantity").live('click',function(e){
	e.preventDefault();
	var id=$(this).data('id');
	$("#quantity-"+id).removeAttr("disabled");
	$("#quantity-"+id).removeClass("disable");
});


$(".qty").live('change',function(e){
	 var qty=$(this).val();
	 var id =$(this).attr("id");
	 var price=$(this).data('price');
	 var total = parseInt(price)*parseInt(qty);
	 var ids=id.split("-");
	 var vat=$("#vat").val();

	 $("#allTotal-"+ids[1]).data("total",total);
	 $("#allTotal-"+ids[1])
	 $("#allTotal-"+ids[1]).html("&euro;"+total);
	 var t=0;
	 $(".totalPrice").each(function(index,value){
		 t=t+parseInt($(value).data('total'));
	 });
	 $("#t").html(t+" &euro;");
	 
	 if (parseInt(vat) != 0) {
		 $("#totalAll").html(parseInt(vat)*t/100+" &euro;");
	 } else {
		 $("#totalAll").html(t+"&euro;");
	 }
});

var cartDialog = $("#cartDialog");
if (!cartDialog.data("kendoWindow")) {
    cartDialog.kendoWindow({
        width: "490px",
        title: 'Added to cart',
        visible:false,
        modal:true
    });
}

eshop = new mcms.dataStore({
    method: "POST",
    url:"/userCart.html",
    dataType: "json",
	trigger:"loadComplete"
});
	
$(eshop).bind('addToCart',function(){
	records = eshop.records;
	cartDialog.html(cartDialogTemplate(records));
    cartDialog.data("kendoWindow").open();
    cartDialog.data("kendoWindow").center();
});
	
$(".addToCart").live('click',function(e){
	e.preventDefault();
	eshop.trigger = 'addToCart';
	eshop.data = { id :  $(this).attr('rel'), action : 'addToCart',module:'products'}
	$(eshop).loadStore(eshop);
});//END FUNCTION
	

$(".closeModal").live("click", function() {
	cartDialog.data("kendoWindow").close();
});


$(".remove-item").click(function(){
  if (confirm($.lang.removeFromCart)) {
				var itemid = $(this).attr('rel');
				var placer = new Array();
				placer[0] = '.total-price';
				placer[1] = '#total-'+itemid;
				placer[2] = '.total-products';
				placer[3] = '.total-price-no-vat';
				placer[4] = '.vat-price';
				save_quantity('/userCart.html',{'mode':'removeItem','itemid':itemid},placer);
				$(this).parent().parent().remove();
  }
});//END EMPTY CART

$(".btn-checkout").click(function(){
	
	$("#shopping-cart-table").submit();
	
})

$(".empty_cart").click(function(){
location.href = '/empty_cart.html';					
});//END EMPTY CART


});//END HEAD



function updateCart(obj) {
	if ($('.cartItems').hasClass('hidden')) { $('.cartItems').removeClass('hidden'); }
	$('.total-items').html(obj.total_quantities);
	$('.total-price').html(obj.formatedPrice);
}