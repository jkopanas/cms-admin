$(document).ready(function () {
	$.maps = {};//WILL HOLD A LIST WITH ALL THE MAPS
	var lat = 43.4882818;
	var lng = 11.90228;
	var res = {};
	var mycolor = "#ff0066";
	var mycolor2 = "#966E7E";
	var mybg_color = "#000000";
	 
	var cluster_styles = [{
	    url: 'images/m3.png',
	    height: 30,
	    width: 30,
	    opt_textSize: 14,
	    anchor: [3, 0],
	    textColor: '#222222'
	}, {
	    url: 'images/m4.png',
	    height: 40,
	    width: 40,
	    opt_textSize: 17,
	    opt_anchor: [6, 0],
	    opt_textColor: '#222222'
	}, {
	    url: 'images/m5.png',
	    width: 50,
	    height: 50,
	    opt_textSize: 21,
	    opt_anchor: [8, 0],
	    opt_textColor: '#222222',
	}, {
	    url: 'images/m5.png',
	    width: 50,
	    height: 50,
	    opt_textSize: 21,
	    opt_anchor: [8, 0],
	    opt_textColor: '#222222',
	}];
 
	var my_cat_style ={
		cat1:    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=R|cc0000|FFFFFF'},
		cat2:  { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=G|00cc00|333333'},
		cat3:   { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=B|2222cc|FFFFFF'}, 
		cat4:   { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|00cccc|333333'}
    };
	
    var markerListeners = [
//    	{listener : 'dragend',callback : 'hanleMarkerChange' }
    	{listener : 'dragend',callback : 'reverseGeoCode',ListenerReverseGeoCoding:true },
    	{listener : 'drag',callback : 'reverseGeoCode' },
	];
    var mapListeners = [
    	{listener : 'zoom_changed',callback : 'hanleMapChange' },
    	{listener : 'maptypeid_changed',callback : 'hanleMapChange' },
    	{listener : 'tilt_changed',callback : 'hanleMapChange' }
	];
/*	
$('#map_1').googleMaps({createMarker : {
	position:new google.maps.LatLng(lat,lng),
	draggable:true,
	infoWindow: {content:'asdfasdfsdf'},
	infoWindowListener: 1,}

});
*/

$('#map_1').googleMaps({
cluster_styles : { styles : cluster_styles }, cat_style: my_cat_style
});		
$('#map2').googleMaps({ 
        clat: 38.00324983510847,
        clng:  23.733608354492162,
        map_opt : {zoom:5 },
        cat_style: my_cat_style
});	
/*$('#map3').googleMaps({ 
        clat: 38.00324983510847,
        clng:  23.733608354492162,
        map_opt : {zoom:12 }
});	*/

	$("#click").click(function (e) {
$.maps['map2'].createMarker({createMarker:{
	position:new google.maps.LatLng(lat,lng),
	draggable:true,
	infoWindow: {content:'asdfasdfsdf'},
	infoWindowListener: 1,}}
	);
$.getJSON("/material/citiesJson.json", function(data) {
	
/*$.each(data, function(key, val) {

	$.maps['map_1'].createMarker({createMarker:{
		position:new google.maps.LatLng(val.lat,val.lng),
		draggable:true,
		infoWindow: {content:val.content},
		infoWindowListener: 1}}
		);
});//END LOOP*/
});

//console.log($.maps['map_1'].gMap.getMapTypeId())
});


	$("#loadMarkers").click(function (e) {

$.getJSON("/material/citiesJson.json", function(data) {
$('#markerCategories').show()
$.each(data, function(key, val) {

	$.maps['map2'].createMarker({createMarker:{
		position:new google.maps.LatLng(val.lat,val.lng),
		draggable:false,
		id:val.id,
		category:val.category,
		infoWindow: {content:val.name},
		infoWindowListener: 1}}
		);
});//END LOOP

});

//console.log($.maps['map_1'].gMap.getMapTypeId())
});

$(".categories").click(function (e) {
	var me = $(this);
	$.each($.maps['map2'].markerCategories[me.val()], function(key, val) {
	if (me.is(':checked'))//SHOW MARKERS
	{
		$.maps['map2'].markersList[val].setVisible(true);
		$.maps['map2'].markerCluster.addMarker($.maps['map2'].markersList[val]);
	}//END SHOW
	else {//HIDE MARKERS
		$.maps['map2'].markersList[val].setVisible(false);
		$.maps['map2'].markerCluster.removeMarker($.maps['map2'].markersList[val]);
	}//END HIDE
	});//END LOOP
//	$.maps['map2'].markerCluster.redraw()
});
//	 $("#map_1").gmapDraw({elements:'#map_1_list .maplocation'});

    $("#addressForm").submit(function (e) {
        e.preventDefault();
       $(res).trigger('loaded')
       
        $.maps['map_1'].geoCode({geocoder :{address:$('input[name=address]').val(),language:'el',region:'GR'}, markers : {draggable : true,listeners:markerListeners,ListenerReverseGeoCoding:true,geocoder:{language:'el',region:'GR'} },addListeners:mapListeners}
        ,'returnGeocoderObject');

    });

/*$.getJSON("/material/citiesJson.json", function(data) {
	var a  = new Array;
	$.each(data, function(key, val) {
		val['content'] = val.name;
		a.push(val);
		
  });
   $("#map_1").gmapDraw({json:a});
 });*/
		
$(".results").live('click', function(){
	      $.maps['map_1'].geoCode({geocoder :{address:$(this).html()}, markers : {draggable : true,listeners:markerListeners,ListenerReverseGeoCoding:true,geocoder:{language:'el',region:'GR'} },addListeners:mapListeners}
        ,'');
})


});//END JQ

function hanleMapChange(map)
{
	if (map.event = 'zoom_changed')
	{
		console.log(map.event)
	}
}//END FUNCTION

function hanleMarkerChange(marker)
{
	console.log(marker.map.getZoom())
}//END FUNCTION

function reverseGeoCode(results)
{
	

	if (results.event == 'dragend')
	{
		$('#geocodingDataNow .address span').html(results.results.formatted_address);
	}
	else if (results.event == 'drag')
	{
		var latLng = results.getPosition();		
		$('#geocodingDataNow .currentPosition span').html(latLng.lat() + ' , ' + latLng.lng())
		
	}
}

function returnGeocoderObject(results) { 
$('#geocodingResults').children().remove()
if (results.length > 0)
{
		$.each(results, function(key, val) {
			$('#geocodingResults').append('<li><a href="#" class="results" rel="marker-' + key + '">' + val.formatted_address + '</a></li>');
		})
}

}