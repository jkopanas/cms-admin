 head.ready(function(){
var x;


	$("#filterReuslts").fancybox({
				'transitionIn'	: 'fade',
				'scrolling'		: 'no',
				'titleShow'		: false,
				'overlayOpacity'	: '0',
				'hideOnOverlayClick': false,
				'onClosed'		: function() {
					$("#login_error").hide();
				},
				'onComplete' : function(){
					x.refreshMap();
				}
			});
			

points = new $.dataStore({
    method: "POST",
    url:"/ajax/mapActions.php?action=debug&module=listings",
    dataType: "json",
	trigger:"loadComplete"
});



	var my_cat_style ={
		cat1:    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=R|cc0000|FFFFFF'},
		cat2:  { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=G|00cc00|333333'},
		cat3:   { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=B|2222cc|FFFFFF'}, 
		cat4:   { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|00cccc|333333'}
    };
	
    var markerListeners = [
//    	{ listener : 'dragend',callback : 'hanleMarkerChange' }
    	{ listener : 'dragend',callback : 'reverseGeoCode',ListenerReverseGeoCoding:true },
    	{ listener : 'drag',callback : 'reverseGeoCode' },
		{ listener : 'click',callback : 'markerClick' }
		
	];
    var mapListeners = [
    	{ listener : 'zoom_changed',callback : 'hanleMapChange' },
    	{ listener : 'maptypeid_changed',callback : 'hanleMapChange' },
    	{ listener : 'tilt_changed',callback : 'hanleMapChange' },
    	{ listener : 'click',callback : 'hanleMapChange' }
    	
	];
	

        
$(points).bind('loadComplete',function(){
	records = points.records;
	$.each(records.items, function(key, val) {
var i = (val.category) ? val.category : 'cat3';
		 x.createMarker({ markerListeners:markerListeners, category:val.category,draggable:true,map:x.obj,position : new google.maps.LatLng(val.lat,val.lng),icon:my_cat_style[i]['icon'],infoWindow: { content : '#' + val.itemid + ' - ' + val.category + '<br /> ' + val.geocoderAddress}} );
	});
	
});

      function refreshMap() {
        if (markerClusterer) {
          markerClusterer.clearMarkers();
        }
      }
$('.map').googleMaps({ zoom:8,clat:'37.0250228',clng:'22.1088166',mapListeners:mapListeners}).data('mapItem').init();	
//$('#mapItem2').googleMaps({ zoom:15,clat:'37.0250228',clng:'22.1088166'}).data('mapItem2').init();	
x = $('#mapItem').data('mapItem'); 
x.createMarker({ map:x.obj,position : new google.maps.LatLng('37.0250228','22.1088166')} );
x.drawingManager({
	circleOptions: {
	            fillOpacity: 0.2,
	            strokeWeight: 0.5,
	            clickable: false,
	            editable: true,
	            zIndex: 1
          },drawingControlOptions : { drawingModes:x.setDrawingModes(['CIRCLE','MARKER','POLYLINE','POLYGON','RECTANGLE']),position:x.setPosition('TOP_CENTER')}});

markerClusterer = new MarkerClusterer(x.getMap(), [], {});
      

$(".showMap").click(function(e){
	e.preventDefault();
	$('#test').parent().show();
	x.refreshMap();
});

$(".showMarkers").click(function(e){
	e.preventDefault();
	$(points).loadStore(points);
	refreshMap();
     markerClusterer.addMarkers(x.Markers());
        
});

$(".showHideMarkers").click(function(){
	
        if ($(this).is(':checked')) {
          x.showMarkerCategory($(this).attr('data-category'));

        } else {
          x.hideMarkerCategory($(this).attr('data-category'));
        }
});

    $("#addressForm").live('submit',function (e) {
       e.preventDefault();
		x.geoCode({ geocoder :{ address:$('#mapaddress').val(),language:'el',region:'GR'}, markers : { draggable : true,markerListeners:markerListeners,ListenerReverseGeoCoding:true,onComplete:function(marker){return;} },addListeners:mapListeners}
        ,'returnGeocoderObject');
    });

$(".results").live('click', function(e){
	e.preventDefault();

});

$(".results").live('click', function(e){
	e.preventDefault();

	      x.geoCode({ geocoder :{ address:$(this).html(),language:'el',region:'GR'}, markers : { draggable : true,markerListeners:markerListeners,ListenerReverseGeoCoding:true },addListeners:mapListeners}
        ,'geocodedResults');
});

//$('#mapItem2').googleMaps({ zoom:15,clat:'37.0250228',clng:'22.1088166', createMarker:{ draggable:true} }).data('gm').init();	
			lga_charts.line_charts();
			lga_charts.area_charts();
			lga_charts.bar_charts();
			lga_charts.pie_charts();
			$('table.chart').hide();
			$("#dc_tabs .tabsS").flowtabs("#dc_tabs .box_c_content > div");
			$("#sc_tabs .tabsS").flowtabs("#sc_tabs .box_c_content > div");
        });
		
		function hanleMapChange(map)
{
	
	if (map.event == 'maptypeid_changed')
	{
		$('input[name=MapTypeId]').val(map.getMapTypeId());
	}
	else if (map.event == 'zoom_changed')
	{
		$('input[name=zoomLevel]').val(map.getZoom());
	}
}//END FUNCTION

function markerClick(marker) {
}


function reverseGeoCode(results)
{

	var latLng = results.getPosition();		
	if (results.event == 'dragend')
	{
		$('#geocodingDataNow .address span').html(results.results.formatted_address);
//		populateHiddenFields(results.results.formatted_address,latLng.lat(),latLng.lng(),$.maps['map_1'].gMap);
	}
	else if (results.event == 'drag')
	{
		$('#geocodingDataNow .currentPosition span').html(latLng.lat() + ' , ' + latLng.lng());
	}
}//END FUNCTION

function returnGeocoderObject(results) {

	$('#geocodingResults').children().remove();
	var latLng = results[0].marker.getPosition();


	$('#geocodingDataNow .currentPosition span').html(latLng.lat() + ' , ' + latLng.lng());
	$('#geocodingDataNow .address span').html(results[0].formatted_address);
	if (results.length > 0)
	{
		$.each(results, function(key, val) {
			$('#geocodingResults').append('<li><a href="#" class="results" rel="marker-' + key + '">' + val.formatted_address + '</a></li>');
		});
	}
}


