$(document).ready(function () {
$.maps = {};//WILL HOLD A LIST WITH ALL THE MAPS
var mapListeners = {};
var mapDefaults = {};

$(".placeItemOnMap").click(function(){

	$(this).colorbox({overlayClose:false,iframe:true, innerWidth:1000, innerHeight:700,href:'/modules/maps/placeItemOnMap.php?id=' + $('input[name=id]').val() + '&module=' + $('input[name=module]').val()});
});

if ($('#miniMap'))
{
	createMarkers($('.ids'),'miniMap',$('#items'));
}

if ($('#mapInfo').find('input[name=lat]').val() && $('#mapInfo').find('input[name=lng]').val())
{ 
	if ($('input[name=mapItem]').val())
	{
	var obj = jQuery.parseJSON($('input[name=mapItem]').val());
	var mapOpt = $.extend({}, {zoom:parseInt(obj.zoomLevel), mapTypeId:obj.MapTypeId}, {})
	mapDefaults = $.extend({map_opt: mapOpt,clat:obj.lat,clng:obj.lng,addListeners:mapListeners}, {}, mapDefaults);
	var infoWindowContent = '<h2>' + obj.item.title + '</h2>';
	infoWindowContent += '<div>' + obj.geocoderAddress + '</div>';
	}


	var mapObject = new $.FormData($('#mapInfo').find('.mapItem'),{'ReturnType':'array'});
	$('#mapItem').googleMaps({map_opt: {zoom:parseInt(mapObject.zoomLevel), mapTypeId:mapObject.MapTypeId},clat:mapObject.lat,clng:mapObject.lng,createMarker : {
	position:new google.maps.LatLng(mapObject.lat,mapObject.lng),
	infoWindowListener: 1,infoWindow: {content:infoWindowContent}}
});
}

$('body').bind('loadMap', function(e, mapObject) { //WILL BE CALLED UPON SAVE ON THE EDITOR MODAL
	
	$('#mapItem').googleMaps({map_opt: {zoom:parseInt(mapObject.zoomLevel), mapTypeId:mapObject.MapTypeId},clat:mapObject.lat,clng:mapObject.lng,createMarker : {
	position:new google.maps.LatLng(mapObject.lat,mapObject.lng),
	infoWindowListener: 1}
});

$('#mapItem').show();
});


});

function createMarkers(ids,map,itemsDiv)
{
	
//	$.maps['miniMap']
	$.ajax({
		url: "/getItemMarkers.html",
		async:false,
		type:'POST',
		data:{ 'data': new $.FormData(ids,{'ReturnType':'array'}),module:itemsDiv.find('input[name=module]').val()
		}, 
		cache: false,
		success: function(data){
		var obj = jQuery.parseJSON(data);
		if (obj.items)
		{
			$('#' + map).addClass('map');
			$('#' + map).googleMaps({ 
			        clat: obj.lat,
			        clng:  obj.lng,
			        map_opt : {zoom:obj.zoomLevel }
			});	
			$.each(obj.items, function(key, val) {
				$.maps[map].createMarker({createMarker:{
					position:new google.maps.LatLng(val.lat,val.lng),
					infoWindow: {content: '<h2>' + itemsDiv.find('a[rel=item-' + val.itemid + ']').html() + '</h2>'},
					infoWindowEvents : {setZoom:parseInt(val.zoomLevel),MapTypeId:val.MapTypeId},
					infoWindowListener: 1}}
				);
			});//END FOREACH
		}//END HAS MARKERS
		}//END SUCCESS
	});//END AJAX	
	
	
		
	/*$.each(data, function(key, val) {
	
		$.maps['map_1'].createMarker({createMarker:{
			position:new google.maps.LatLng(val.lat,val.lng),
			draggable:true,
			infoWindow: {content:val.content},
			infoWindowListener: 1}}
			);
	});//END LOOP*/
}//END FUNCTION