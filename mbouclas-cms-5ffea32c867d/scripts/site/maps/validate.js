$(document).ready(function(){	
$("#registrationForm1").validate({
		 submitHandler: function (form) {

			
        },
        wrapper: "div",
        rules: {
            uname: {
                required: true, remote:{url:"/check_user.html",type: "post"}
            },
            email: {
			  required: true,
		      email: true,
		      remote: {
        		url: "/check_email.html",
		        type: "post"
      		} 	
			}

        },
        messages : {
        	email : { required : 'fill it up',email:'not valid duche',remote : "email exists" },
        	uname : { required : 'fill it up',remote : "user exists" },
        	ageto : { required : 'must be smaller than age from' }
        },

		onkeyup: false
    });		
    
});


//			