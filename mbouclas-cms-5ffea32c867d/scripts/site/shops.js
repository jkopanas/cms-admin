var x = {};
head.ready(function(){
var markerTemplate = kendo.template($("#markerTemplate").html());
var obj = $.parseJSON($('textarea[name=fomatedItemsRaw]').val());
if (obj) {
$('#map').googleMaps({ zoom:parseInt(obj.map.zoomLevel),clat:obj.map.lat,clng:obj.map.lng}).data('map').init();
x = $('#map').data('map');

$(x).bind('populateMap',function(){
	
	if (!x.items){
		var obj = $.parseJSON($('textarea[name=fomatedItemsRaw]').val());
	}
	else {
		var obj = x.items;
		x.destroyMarkers();
		
	}

	if (obj.items) {
		$.each(obj.items, function(key, val) {
			 x.createMarker({ category:'moreItems',map:x.getMap(),position : new google.maps.LatLng(val.lat,val.lng),infoWindow: { content : markerTemplate(val)}} );
		});
	}
	x.refreshMap();
});
	

$(x).trigger('populateMap');
}
});//END HEAD