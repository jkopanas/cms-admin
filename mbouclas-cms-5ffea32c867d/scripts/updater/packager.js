var allData = {};
var cache = {};
var selected = {};
var currentClick;
head.ready(function(){
	
var dbDefaults = {
    method: "POST",
    url:"/manage/updater/readDirs.php",
    dataType: "json",
	trigger:"loadComplete"
};

var db = new mcms.dataStore({
     method: "POST",
     url:"",
     //dataType: "json",
     trigger:"loadComplete"
 });

 var templates = {};//Look out for them on the actual tpl
 $('.kTemplate').each(function(k,v){
 	templates[$(this).attr('data-name')] = kendo.template($(this).html()); 
 });
 
var columnSet = {
idCheck : {
    	title: "<input type='checkbox' class='check-all' id='total_check' data-bind='click: checkAll'>",
    	field: "file",
        width: 40,
        template : templates.checkBox,
        filterable:false,
        sortable:false,
        reorderable:false
        
    },
file :    {
        field: "file",
        title: "filename",
        template:templates.file,
        width:'60%',
        filterable:true
    },
fileSimple :    {
        field: "file",
        title: "filename",
        template:templates.fileSimple,
        width:'60%',
        filterable:true
    },
date_modified :    {
        field: 'modified',
        type: 'string',
        title: "Date modified",
        filterable:false,
        template:templates.modified
    },
type :     {
        field: 'type',
        title: 'Type',
        filterable:false,
        template:templates.type,
        sortable:true,
        width:40
    },
size : {
        field: 'size',
        title: 'Filesize',
        type:'number',
        filterable:false,
        template:templates.fileSize,
        sortable:true
    }
};
var columns = new Array;
var targetCols = new Array;
var defaultCols = ['idCheck','type','file','date_modified','size'];
var targetColsDef = ['idCheck','type','fileSimple','date_modified','size'];

for (var i in defaultCols) {
	columns.push(columnSet[defaultCols[i]]);
}

for (var i in targetColsDef) {
	targetCols.push(columnSet[targetColsDef[i]]);
}

var s = {
    data : function(data) {
    	allData = data;
    	if (typeof data.results == 'undefined') {
    		return [];
    	}
    	var ret = new Array;
    	$.each(data.results,function(k,v){
    		ret.push(v);
    	});
    		currentClick = data.data.md5;
    		cache[data.data.md5] = data;
    		return ret;
		
	}
}

var ds = {schema:s,serverFiltering:false,serverPaging:false,pageSize:10000,serverSorting:false,transport: {read : {url :dbDefaults.url,dataType:'json'}}};
var toolBar = [
	{name:'add', template : kendo.template('<a class="k-button k-button-icontext" href="\\#" data-bind="click: doActions" data-method="addItems"><span class="k-icon k-add"></span>'+$.lang.add+'</a>') },
	{name:'filter', template : kendo.template($("#toolBar").html()) },
];
var toolBarSelected = [
	{name:'add', template : kendo.template('<a class="k-button k-button-icontext" href="\\#" data-bind="click: doActions" data-method="removeItems"><span class="k-icon k-delete"></span>'+$.lang.remove+'</a>') },
	{name:'save', template : kendo.template('<a class="k-button k-button-icontext" href="\\#" data-bind="click: doActions" data-method="saveItems"><span class="k-icon k-save"></span>'+$.lang.save+'</a>') },
	{name:'filter', template : kendo.template($("#toolBar").html()) },
];
grid = $('#gr');
selectedFiles = $('#selectedFiles');
	
win = $("#window");
if (!win.data("kendoWindow")) {
	
    win.kendoWindow({
        width: "800px",
        height:'90%',
        title: $.lang.modify,
        visible:false,
        modal:true
    });
    $(window).resize(function() {
	win.attr('height',$(window).height()-10 +'px');
	win.data("kendoWindow").center();
	
});
}

var m = grid.searchItems({drawGrid : {filterable:true,toolbar:toolBar,columns:columns,dataSource : ds}}).data('search');
var x = selectedFiles.searchItems({drawGrid : {toolbar:toolBarSelected,columns:targetCols,dataSource : {serverFiltering:false,serverPaging:false,pageSize:10000,serverSorting:false,data:[]}}}).data('search');


m.grid.dataSource.bind("change", function(e) {//Write breadcrumb
	if (typeof allData.data != 'undefined') {
		buildBreadCrumbs(allData.data.path);
	}
});



$(".expand").live("click", function(e) {
	e.preventDefault();
	var md5 = $(this).attr('data-md');
	currentClick = md5;
	if (md5 in cache) {
		m.grid.dataSource.data(kendoD(cache[md5].results));
		
		buildBreadCrumbs(cache[md5].data.path);
		m.grid.refresh();
		return;
    }
    
	m.grid.dataSource.transport.options.read.url = '/manage/updater/readDirs.php?root='+$(this).attr('data-path');
	m.grid.dataSource.read();
	m.grid.refresh();
});

$('.check-values').live('click',function(e){
	var checkbox = $(this);
	var el = grid;
	var table = grid.find('.messages');
	var row = checkbox.parentsUntil('TR').parent();
	
	if (checkbox.prop('checked') == true)
	{
		table.html(el.find('.check-values:checked').length);
		row.addClass('row_selected');
	}
	else 
	{
		table.html(el.find('.check-values:checked').length);
		row.removeClass('row_selected');
	}
});

gridModel = kendo.observable({
	defaultValues : {url :'/ajax/loader.php?file=ajax_responses.php', dataType:'json' },
	fT: templates.manifest,
	callAjax : function(options) {
		db.url = options.url;
		db.dataType = options.dataType;
		db.trigger = options.trigger;
		db.data = options.data;
		
		$(db).loadStore(db);
	},
	callBack : function(func) {
		if (typeof eval(this[func]) == 'function')
		{
			return eval(this[func]).apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else { console.log(func + ' is not registered'); }
	},
	doActions : function(e) {
		e.preventDefault();
		var func = $(e.currentTarget).attr('data-method');
		var el = grid;
		var table = grid.find('.messages');
		this.tmpTarget = el;
		this.callBack(func,el);
	},
	checkAll : function(e) {
		
		var checkbox = $(e.target);
		var el = grid;
		var table = grid.find('.messages');

		if (checkbox.prop('checked') == true)
		{
			el.find('.check-values').attr('checked',true);
			el.find('.check-values').parentsUntil('TR').parent().addClass('row_selected');
			table.html(el.find('.check-values:checked').length);
	
		}
		else 
		{
			el.find('.check-values').attr('checked',false);
			el.find('.check-values').parentsUntil('TR').parent().removeClass('row_selected');
			table.html(el.find('.check-values:checked').length);
		}		
	},
	addItems : function(el) {
		el.find('.check-values:checked').each(function(k,v){
			selected[$(this).attr('data-id')] = cache[currentClick].results[$(this).attr('data-id')];
			delete cache[currentClick].results[$(this).attr('data-id')];
		});
		x.grid.dataSource.data(kendoD(selected));
		m.grid.dataSource.data(kendoD(cache[currentClick].results));
		x.grid.refresh();
		m.grid.refresh();
		el.find('.messages').html('0');
//		this.callAjax({url:this.defaultValues.url+'&action=ItemActions&mode=ActivateChecked',trigger:'itemsEnabled',dataType:this.defaultValues.dataType,data:{'ids':toSave}});
}
});
kendo.bind(grid, gridModel);

gridModel2 = kendo.observable({
	callAjax : function(options) {
		db.url = options.url;
		db.dataType = options.dataType;
		db.trigger = options.trigger;
		db.data = options.data;
		
		$(db).loadStore(db);
	},
	callBack : function(func) {
		if (typeof eval(this[func]) == 'function')
		{
			return eval(this[func]).apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else { console.log(func + ' is not registered'); }
	},
	doActions : function(e) {
		e.preventDefault();
		var func = $(e.currentTarget).attr('data-method');
		var el = selectedFiles;
		var table = grid.find('.messages');
		this.tmpTarget = el;
		this.callBack(func,el);
	},
	saveItems : function(e) {
		
		    win.data("kendoWindow").open();
		    win.data("kendoWindow").center();
		
		//		this.callAjax({url:this.defaultValues.url+'&action=ItemActions&mode=ActivateChecked',trigger:'itemsEnabled',dataType:this.defaultValues.dataType,data:{'ids':toSave}});
	},
	saveManifest : function(e) {
		console.log(e)
	}
});

kendo.bind(selectedFiles, gridModel2);

$(db).bind('manifestSent',function(){//Manifest saved

});

$('#saveManifest').live('click',function(e){
	e.preventDefault();
	var additional = mcms.formData($(this).parents('form').find('.toSave'),{ getData:true});
	var c = {};
	$.each(x.grid.dataSource.data(),function(k,v) {
		c[k] = {};
		$.each(v,function(a,b){
			
			if (typeof b != 'function' && typeof b != 'object'){
				console.log(typeof b)
				c[k][a] = b;
			}
		});
		
		
	});
	
	db.data = {data:additional,copies :c};
	db.url = '/ajax/loader.php?boxID=updater&mode=trace';
	db.trigger = 'manifestSent';
	db.dataType = 'json';	
	$(db).loadStore(db);
});

function buildBreadCrumbs(b) {
	    var breadCrumb = b.split('/');
    	$('.breadCrumb').html(templates.breadCrumb(breadCrumb))
}
});//END HEAD

function findKey(d,key) {
	var x = 0;
	for (i in d) {
		if (i == key) {
			return x;
		}
		x++;
	}

	
}

function kendoD(d) {
	var ret = new Array;
	$.each(d,function(k,v){
	ret.push(v);
});
return ret;
}