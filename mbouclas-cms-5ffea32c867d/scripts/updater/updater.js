var versions = {};
head.ready(function(){
/*	
Step 1. check server for updaates and retrieve number of updates
Step 2. inform user of number of updates and steps required. Ask him to press continue
Step 3. Create progress bars (total and current), status bar and changelog view per update
Step 4. thank you and goodbye
*/
/*$(".progressBar").progressBar( {showText: true, barImage: '/scripts/progressBar/images/progressbg_red.gif'} );
 $('#totalComplete').progressBar(44)*/
var messages = {
	fetchFiles 			: {msg : 'fetching files' },
	unzipFiles 			: {msg : 'unziping files' },
	createRestorePoint 	: {msg : 'Creating Restore Point' },
	copyFiles 			: {msg : 'copying files' },
	updateSql 			: {msg : 'updating database' },
	cleanFiles 			: {msg : 'cleaning up' },
	rollbackfiles		: {msg : 'RollBack Files' },
	rollbacksql			: {msg : 'RollBack DataBase' },
}

var dbDefaults = { 
	//url : '/ajax/updater/tester.php'
	url : '/ajax/loader.php?boxID=updater',
	remoteUrl: mcms.system.version_check_url,
};

var db = new mcms.dataStore({
     method: "POST",
     async:false,
     url:"",
     dataType: "json",
     trigger:"loadComplete"
 });



$(db).bind('versionCheck',function(){
	db.trigger = 'versionCheckComplete';
	db.data = mcms.formData($('.origData'),{ getData:false});
	db.dataType = 'jsonp';
	//db.url = dbDefaults.remoteUrl + '?action=versionCheck';
	db.url = dbDefaults.remoteUrl + '&version='+$('input[name=currentVersion]').val();
	$(db).loadStore(db);
});

$(db).bind('versionCheckComplete',function(){
	if (db.records.total > 0) {
		var t = kendo.template($('#versionsFoundTemplate').html());
		model.writeMessage(t(db.records.versions),'#versionList');
		kendo.bind($('#updater'), actions);//bind again!!! fucking live...
		versions = db.records;

	}
	else {
		model.writeMessage('already up to date');
	}
});

$(db).bind('tokenComplete',function(){
	var x = db.records;
	
	if (!x.success) {  model.callRollBack(); return false; }
	var next = parseInt(x.get.num)+ 1;
	model.drawProgressBarsCurrent(model.progressCurrent,next,x.post);
	if (typeof x.post.tokens[next] != 'undefined') {
		model.executeToken(next,x.post);
		model.nextToken = false;
	}
	else { 
		if ((typeof versions.versions[model.nextUpdate] != 'undefined')) {
			
			model.performUpdate(model.nextUpdate);
		}
		else {
			model.progressTotal.find('div.report').html('all done!!');
			model.next = true;
			$('#updater').hide();
			$('#updaterComplete').show();
		}
		model.progressCurrent.find('div.report').html('all done!!');
		return 1;
	}
});

$(db).bind('updateComplete',function(){
	var x = db.records;
	if (x.success == false) { model.callRollBack(); return; }
	model.executeTokens(x);
	
	var next = parseInt(x.get.num)+ 1;
	model.nextUpdate = next;
});

$(db).bind('rollBackComplete',function(){
	var x = db.records;
	
	if (!x.success) {  model.callRollBack(); return false; }
	var next = parseInt(x.get.num)+ 1;
	model.drawProgressBarsCurrent(model.progressCurrent,next,x.post);
	if (typeof x.post.tokens[next] != 'undefined') {
		model.executeRollBack(next,x.post);
		model.nextToken = false;
	}
	else { 
			model.progressTotal.find('div.report').html('all done!!');
			model.next = true;
			$('#updater').hide();
			$('#rollBackComplete').show();
		model.progressCurrent.find('div.report').html('all done!!');
		return 1;
	}
});


model = {
	fail:false,
	rollBack:false,
	nextToken:false,
	nextUpdate:false,
	progressTotal : $('#totalComplete'),
	progressCurrent : $('#currentComplete'),
	callRollBack : function() {
		this.writeMessage('something went wrong');
		this.fail = true;
		this.rollBack = true;
		this.executeRollBack(0,versions.rollBack);
		return false;
	},
	executeRollBack : function(i,x) {
		db.trigger = 'rollBackComplete';
		db.data = x;
		db.async = true;
		db.dataType = 'json';
		//db.url = dbDefaults.url + '?mode=update&num='+i+'&action='+x.tokens[i];//bring the first
		db.url = dbDefaults.url + '&mode=update&num='+i+'&action='+x.tokens[i];//bring the first
		$(db).loadStore(db);
	},
	drawProgressBarsTotal : function(el,i,v) {
		var p = Math.floor(100 * i / versions.total);
		el.find('span').html(p);
		el.find('div.report').html('updating to : ' + v.version);
		el.find('.changeLog').html(v.changeLog);
		el.find('.changeLog').parents('div').show();
		el.find('img').css({'background-position': 100 - p + '% 100%'});
	},
	drawProgressBarsCurrent : function(el,i,v) {
		var p = Math.floor(100 * i / v.tokens.length);
		el.find('span').html(p);
		el.find('div.report').html(messages[v.tokens[i-1]].msg);
		el.find('img').css('background-position', 100 - p + '% 100%');
	},
	writeMessage : function(message,d) {
		d = (!d) ? '.debug' : d;
		$(d).html(message);
	},
	startUpdating : function(e) {
		var me = this;
		me.performUpdate(0);
	},
	executeTokens : function(x) {
		this.executeToken(0,x);
	},
	executeToken : function(i,x) {
		if (this.fail == true) { return; }
		
		var el = this;
		
//		this.drawProgressBarsCurrent(this.progressCurrent,i+1,x);
		db.trigger = 'tokenComplete';
		db.data = x;
		db.async = true;
		db.dataType = 'json';
		//db.url = dbDefaults.url + '?mode=update&num='+i+'&action='+x.tokens[i];//bring the first
		db.url = dbDefaults.url + '&mode=update&num='+i+'&action='+x.tokens[i];//bring the first
		$(db).loadStore(db);
	},
	performUpdate : function(i) {
		if (this.fail == true) { return; }
		this.drawProgressBarsTotal(this.progressTotal,i+1,versions.versions[i]);
		db.trigger = 'updateComplete';
		db.data = versions.versions[i];
		db.async = true;
		db.dataType = 'json';
		//db.url = dbDefaults.url + '?mode=update&num='+i+'&action=updateStarted';//bring the first
		db.url = dbDefaults.url + '&mode=update&num='+i+'&action=updateStarted';//bring the first
		$(db).loadStore(db);
	}
};//END MODEL


actions = kendo.observable({
	callAjax : function(options) {
		db.url = options.url;
		db.dataType = options.dataType;
		db.trigger = options.trigger;
		db.data = options.data;
		db.dataType = 'json';
		$(db).loadStore(db);
	},
	callBack : function(func) {
		if (typeof eval(this[func]) == 'function')
		{
			return eval(this[func]).apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else { console.log(func + ' is not registered'); }
	},
	doActions : function(e) {
		e.preventDefault();
		var func = $(e.currentTarget).attr('data-method');
		this.callBack(func,$(e.target));
	},
	startUpdating : function(e) {
		$('#progressBars').show();
		e.hide();
		model.startUpdating();
	},
	showChangeLog : function(e) {
		this.openWindow({content:versions.versions[$(e).attr('data-id')].changeLog,title:'changelog'});
	},
	openWindow : function(c,d) {
		d = (!d) ? '#window' : d;
		win = $(d);
		if (typeof c.content != 'object') { 
			win.html(c.content);
		}
		if (!win.data("kendoWindow")) {
			
		    win.kendoWindow({
		        width: "800px",
		        height:'90%',
		        title: c.title,
		        visible:false,
		        modal:true
		    });
		    $(window).resize(function() {
				win.attr('height',$(window).height()-10 +'px');
				win.data("kendoWindow").center();
			});
		} 
		win.data("kendoWindow").open().center();
		
	}
	
});

kendo.bind($('#updater'), actions);
/* START CODE */

$(db).trigger('versionCheck'); 

});