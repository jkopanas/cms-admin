$(document).ready(function(){
	
$('.webmasterMode').live('click', function(e) {
	$.GlobalEditTranslationVariables(this,{'action':'edit'})
});

});//END JQ

$.GlobalEditTranslationVariables = function(obj,options){//EXPECTS OBJECT. SHOULD BE A LIST
settings = jQuery.extend({//Defaults
ReturnType: 'string'
}, options);//END EXTEND 

var thisRow = $($(obj).attr('rel'));
var thisName = $(obj).attr('rel').split('-');
thisName = thisName[1];
//var form = $(this).closest('form');

	$.ajax({
		url: "/ajax/Languages.php?action=loadSingleVariable&name="+thisName,
	    async:false,
		type:'POST',
		data:{ 
	    }, 
		cache: false,
		success: function(data){
			$('#tmp').find('div').html(data);
			$.fn.colorbox({href:'#tmpContent', width:800, height:600,inline:true});
	   }
	});//END AJAX 

	
	$('.saveSingleTranslation').live('click', function(e) {
	var form = $(this).parentsUntil('form').parent();
	var varName = $('input[name=Varname]').val();

	$.ajax({
			url: "/ajax/Languages.php?action=saveSingleTranslation",
		    async:false,
			type:'POST',
			data:{ 'data': get_form_data('SaveVar'), 'name': varName
		    }, 
			cache: false,
			success: function(data){
				var obj = jQuery.parseJSON(data);
				$('span[rel=Var-'+varName+']').text(obj['value-'+$('input[name=loaded_language]').val()]);
				console.log($('span[rel=Var-'+varName+']'));
				$.fn.colorbox.close();
		   }
	});//END AJAX  	
});//END FUNCTION
};//END OBJECT