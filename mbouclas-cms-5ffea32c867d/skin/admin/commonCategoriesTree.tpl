{include file="{$MODULE_FOLDER}/menu.tpl"}
{if $categories}
<div class="wrap padding seperator" id="box3">
<h2>{$lang.lbl_allDynamicCategories}</h2>
{foreach item=a from=$categories}
<a href="commonCategoriesTree.php?tableID={$a.id}">{$a.title}</a>
{/foreach}
</div>
{/if}


{if $tableID}

<div class="wrap padding seperator" id="box3">
<h2><a href="commonCategoriesTree.php?tableID={$tableID}">{$table.title}</a> (<a href="commonCategoriesTree.php?tableID={$tableID}#addcat">Προσθήκη</a>) {include file="admin/nav_categories.tpl" management=$lang.categories target="admin_categories" cat=$currentCategory target="commonCategoriesTree.php"}</h2>
<form name="cat_form" id="cat_form" action="" method="post">
<input type="hidden" value="SaveCategoriesOrder" name="action" />
<div id="debugArea"></div>
<table cellpadding="3" cellspacing="3" width="100%" class="DragDrop">
<tr class="nodrop nodrag">
		<th width="16">&nbsp;<input type="hidden" name="TableOrder" class="settings" value="" /></th>
		<th scope="col">ID</th>
		<th scope="col">{$lang.title}</th>
        <th scope="col">{$lang.description}</th>
        <th scope="col">#{$lang.subcategories}</th>
        <th scope="col">#{$lang.content}</th>
        <th colspan="2">{$lang.actions}</th>
	</tr>
{foreach from=$currentTable item=a}
<tr class="{cycle values='alternate,'}" id="Order{$a.categoryid}"> 
<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
<td><strong>{$a.categoryid}</strong></td>
  <td> <a href="commonCategoriesTree.php?cat={$a.categoryid}&tableID={$a.tableID}">{$a.category}</a></td>
				<td>{$a.description|truncate:50:" ...":true}</td>
				<td>{$a.num_sub|default:$lang.txt_not_available}</td>
				<td align="center"><A href="category_content.php?cat={$a.categoryid}" class="ItemsList">{$a.num_items|default:0}</A></td>
      <td><a href="commonCategoriesTree.php?action=edit&amp;cat={$a.categoryid}&tableID={$tableID}" class="edit" title="Edit Category {$a.category}"><img src="/images/admin/edit_16.png" width="16" height="16" /></a></td><td><a href="commonCategoriesTree.php?action=delete&amp;cat={$a.categoryid}&tableID={$tableID}" onclick="return confirm('You are about to delete the category \'{$a.category}\'.  All of its items will go to the default category.\n  \'OK\' to delete, \'Cancel\' to stop.')" class="delete" title="Delete Category {$a.category}"><img src="/images/admin/delete_16.png" width="16" height="16" /></a></td>
				</tr>
{/foreach}
</table>
<input type="hidden" name="cat" id="cat" value="{$currentCategory}" />
<input type="hidden" name="cat" id="cat" value="{$tableID}" />

                <input type="hidden" name="mode" id="mode" value="quick_update" />
  <input type="hidden" name="featured" id="featured" value="1" />
  </form>
</div> 

 <script>
 $(document).ready(function() {
	$("#addcat").validate(); 
 });
 
 </script>
<form name="addcat" id="addcat" action="" method="post">               
          <div class="wrap seperator padding"><a name="addcategory" id="addcategory"></a>
          <div id="zeitgeist" style="width:300px;">
  <h3>Επιλογές</h3>
  <select name="settings_theme">
  {foreach item=a from=$themes}
  <option value="{$a.name}" {if $a.name eq $category.settings.theme}selected{/if}>{$a.title}</option>
  {/foreach}
  </select>
   Επιλογή εικαστικού
  <br />Κατάταξη με 
    <select name="settings_orderby">
  <option value="title" {if  $category.settings.orderby eq "title"}selected{/if}>Τίτλος</option>
  <option value="date_added" {if  $category.settings.orderby eq "date_added"}selected{/if}>Ημερομηνία</option>
  <option value="orderby" {if  $category.settings.orderby eq "orderby"}selected{/if}>Σειρά</option>
  </select>     
  <select name="settings_way">
  <option value="ASC" {if  $category.settings.way eq 1}selected{/if}>Αύξουσα</option>
  <option value="DESC" {if  $category.settings.way eq 0}selected{/if}>Φθήνουσα</option>
  </select>
  <br />
  <input name="settings_results_per_page" type="text" value="{$category.settings.results_per_page}" size="5" maxlength="3" /> Αποτελέσματα ανά σελίδα
  <br />
    <select name="settings_featured">
  <option value="0" {if  $category.settings.featured eq 0}selected{/if}>{$lang.no}</option>
  <option value="1" {if  $category.settings.featured eq 1}selected{/if}>{$lang.yes}</option>
  </select> is featured
   <br />
  <input name="settings_image" type="text" value="{$category.settings.image}"  /> Photo
  <br />
  <input name="settings_mapIcon" type="text" value="{$category.settings.mapIcon}"  /> Map Icon
  <br />
{if $more_categories}
<h3>Περισσότερες κατηγορίες</h3>
{section name=a loop=$more_categories}
{if $category.categoryid eq $more_categories[a].categoryid}<strong>{$more_categories[a].category}</strong>{else}<a href="{$MODULE_FOLDER}/categories.php?action=edit&cat={$more_categories[a].categoryid}">{$more_categories[a].category}</a>{/if}<br />
{/section}
{/if}
</div>
            <h2>{if $edit eq 1}{$lang.modify} -> <a href="commonCategoriesTree.php?tableID={$tableID}">{$table.title}</a>   {include file="admin/nav_categories.tpl" management=$lang.categories target="admin_categories" actionMode="edit" cat=$category target="commonCategoriesTree.php"} {else}{$lang.add_new}{/if}</h2>
    
         
      <p>Θέση:<br />
        <input name="orderby" type="text" class="editform" style="background-color: rgb(255, 255, 160);" value="{$category.order_by}" size="4" id="orderby" />
         </p>
        <p>{$lang.title}:<br />
        <input name="title" type="text" style="background-color: rgb(255, 255, 160);" value="{$category.category}" class="required" />
        </p>
        <p>{$lang.permalink}:<br />
        <input name="alias" type="text" style="background-color: rgb(255, 255, 160);" value="{$category.alias}" />
        </p>
        <p>Ανήκει στην κατηγορία :<br />
          <select name="parent_catid" class="postform" id="parent_catid"> 
<option value="%">Όλες οι κατηγορίες</option>
{foreach from=$allcategories item=a}
<option value="{$a.categoryid}" {if $currentCategory}{if $a.categoryid eq $currentCategory.categoryid}selected{/if}{else}{if $a.categoryid eq $category.parentid}selected{/if}{/if}>{$a.category}</option>                
{/foreach}  
</select>

</p>
        <p>{$lang.description}: (προερετικό) <br />
        <textarea name="description" rows="5" cols="60"  id="desc">{$category.description}</textarea>
        <br />
        <a href="javascript:void(0);" class="open-editor" rel="desc">{$lang.advanced_editor}</a>
        </p>

        <p  align="left"><input name="action" value="{$action|default:'addcat'}" type="hidden" /><input type="hidden" name="cat" value="{$catid|default:$smarty.get.cat}" />
        <input type="hidden" name="tableID" value="{$tableID}" />
        <input name="submit" type="submit" class="button" value="{$lang.save}" />  {if $category}<a href="commonCategoriesTree.php?tableID={$tableID}&cat={$category.categoryid}&action=edit&mode=translate#translate">{$lang.translate}</a> {/if}
        </p>
    
</div>      
</form>
<div class="seperator"></div>
{if $mode eq "translate"}
<a href="#" id="translate"></a>
<div class="seperator padding wrap">
<div id="zeitgeist">
<h3>{$lang.available_languages}</h3>
<ul id="languageTabs"  class="idTabs">
{foreach from=$availableLanguages item=a name=b}
<li><a  href="#taber{$smarty.foreach.b.iteration}" rel="taber{$smarty.foreach.b.iteration}">{$a.country}</a></li>
{/foreach}
<li><input type="button" value="{$lang.save}" class="SaveTranslations button" /></li>
</ul>
</div>
<h2>{$lang.translations}</h2>
<form>
<input type="hidden" class="originalData" value="{$category.categoryid}" name="id" />
<input type="hidden" class="originalData" value="{$category.category}" name="category" />
<input type="hidden" class="originalData" value="{$category.alias}" name="alias" />
<input type="hidden" class="originalData" value='{$category.description}' name="description" />

<table width="650">
<Tr><td><div id="tab-container">
{foreach from=$availableLanguages item=a name=b}
<div id="taber{$smarty.foreach.b.iteration}" class="tabcontent">
<h3>{$lang.defaultLanguage} : {$defaultLanguage.details.country} - {$lang.NowEditing} {$a.country}</h3>
<p>{$lang.title} : <br /><input name="{$table.table_name}@category-{$a.code}" type="text" id="category-{$a.code}" size="45" value="{$a.item.category.translation}" class="languageTranslationBox"></p>
<p>{$lang.permalink} : <br /><input name="{$table.table_name}@alias-{$a.code}" type="text" id="alias-{$a.code}" size="45" value="{$a.item.alias.translation}" class="languageTranslationBox"></p>
<p>{$lang.description} : <br /><textarea name="{$table.table_name}@description-{$a.code}" cols="80" rows="5" id="description-{$a.code}" class="languageTranslationBox">{$a.item.description.translation}</textarea><br /><a href="javascript:void(0);" class="open-editor" rel="description-{$a.code}">{$lang.advanced_editor}</a>
</div><!-- END TAB -->
<div class="clear"></div>
{/foreach}
<div class="clear"></div>
</div><!-- END TAB CONTAINER -->
</td></Tr>
</table>
</form>

<div class="clear"></div>
</div><!-- END WRAP -->
<div class="seperator"></div>

<script src="/scripts/AdminLanguages.js"></script>
{/if}
{/if}

