<div class="wrap padding seperator">
<h2><a href="{$ADMIN_URL}" class=navigationpath>{$lang.main_page}</a>&nbsp;::&nbsp; {$lang.contact_form}</h2>

<form id="contactForm" method="post">
<p>{$lang.lbl_address} :<br>
<input type="text" name="contact_form_address" id="contact_form_address" value="{$CONTACT_FORM_ADDRESS}" /> <div class="hidden">{$lang[$CONTACT_FORM_ADDRESS]}</div>
</p>
<p>{$lang.comments} :<br>
<input type="text" name="contact_form_comments" id="contact_form_comments" value="{$CONTACT_FORM_COMMENTS}" /> <div class="hidden">{$lang[$CONTACT_FORM_COMMENTS]}</div>
</p>
<p class="spacer-top">
<h2>{$lang.formFields}</h2>
<table class="DragDrop widefat" id="fields">
<tr class="nodrop nodrag">
<th>&nbsp;</th>
<th scope="col">{$lang.field_name}</th>
<th scope="col">{$lang.title}</th>
<th scope="col">{$lang.field_type}</th>
<th scope="col">{$lang.required}</th>
<th scope="col">{$lang.description}</th>
<th scope="col">{$lang.errorMessage}</th>
<th>&nbsp;</th>
</tr>
{foreach from=$fields key=k item=a name=b}
    <tr id="{$smarty.foreach.b.iteration}">
        <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
        <td><input type="text" name="fieldName-{$smarty.foreach.b.iteration}" value="{$a.fieldName}" class="SaveData" />
        <td><input type="text" name="title-{$smarty.foreach.b.iteration}" value="{$a.translations.title}" class="SaveData" title="{$trans[$a.fieldName].title}" />{if $trans[$a.fieldName].title}<div class="hidden toolTip">{$trans[$a.fieldName].title}</div>{/if}</td>
        <td><select name="fieldType-{$smarty.foreach.b.iteration}" class="SaveData">
          <option value="text" {if $a.fieldType eq "text"}selected{/if}>{$lang.text}</option>
          <option value="area" {if $a.fieldType eq "area"}selected{/if}>{$lang.textarea}</option>
          <option value="email" {if $a.fieldType eq "email"}selected{/if}>{$lang.email}</option>
          <option value="radio" {if $a.fieldType eq "radio"}selected{/if}>{$lang.radiobtn}</option>
          <option value="hidden" {if $a.fieldType eq "hidden"}selected{/if}>{$lang.hidden}</option>
          <option value="document" {if $a.fieldType eq "document"}selected{/if}>{$lang.file}</option>
          <option value="image" {if $a.fieldType eq "image"}selected{/if}>{$lang.image}</option>
          <option value="link" {if $a.fieldType eq "link"}selected{/if}>{$lang.link}</option>
          <option value="multiselect" {if $a.fieldType eq "multiselect"}selected{/if}>{$lang.multiselect}</option>
        </select></td>
        <td><select name="required-{$smarty.foreach.b.iteration}" class="SaveData">
          <option value="0">{$lang.no}</option>
          <option value="1" {if $a.required}selected="selected"{/if}>{$lang.yes}</option>
      </select></td>
      <td>
      <input type="text" name="description-{$smarty.foreach.b.iteration}" class="SaveData" value="{$a.translations.description}" />
      {if $trans[$a.fieldName].description}<div class="hidden toolTip">{$trans[$a.fieldName].description}</div>{/if}
      </td>
      <td>
      <input type="text" name="errorMessage-{$smarty.foreach.b.iteration}" class="SaveData" value="{$a.translations.errorMessage}" />
      {if $trans[$a.fieldName].errorMessage}<div class="hidden toolTip">{$trans[$a.fieldName].errorMessage}</div>{/if}
      </td>
      <td><a href="#" class="deleteFormField" title="{$lang.delete} {$cat[a].category}"><img src="/images/admin/delete_16.png" width="16" height="16" /></a></td>
    </tr>
{/foreach}
</table>
</p>
</form>
<div class="seperator"></div>
<h2>{$lang.lbl_map}</h2>
 <form name="addressForm" id="addressForm">
 <input type="hidden" name="mapItem" value='{$mapItem}' />
<label>{$lang.lbl_address} : <input name="address" type="text" value="{$item.geocoderAddress}"  class="ContentTitle" size="45" /> </label> </label> <input type="submit" value="{$lang.search}" name="searchAddress" class="button" />
<ul id="geocodingResults" class="list"> 

</ul>
<div class="seperator"></div>
<div id="map_1" class="mapbox"></div> 
<div id="geocodingDataNow" class="spacer-top">
<p class="currentPosition">{$lang.lbl_currentPosition} : <strong><span></span></strong></p>
<p class="address">{$lang.lbl_closestAddress} : <strong><span></span></strong></p>
</div>


<input type="hidden" name="geocoderAddress" value="{$item.geocoderAddress}" class="get" />
<input type="hidden" name="lat" class="get" value="{$item.lat}" />
<input type="hidden" name="lng" class="get" value="{$item.lng}" />
<input type="hidden" name="zoomLevel" class="get" value="{$item.zoomLevel}" />
<input type="hidden" name="MapTypeId" class="get" value="{$item.MapTypeId}" />
<input type="hidden" name="itemid" value="{$id}" class="get" />
<input type="hidden" name="module" value="ContactForm" class="get" />
<div class="spacer-top"><input type="button" value="{$lang.save}" class="button {if !$item}hidden{/if}" id="savePlaceOnMap" /></div>
</form>
<div class="debugArea"></div>
<div class="seperator"></div>


<script type="text/javascript" src="http://www.google.com/jsapi?autoload=%7Bmodules%3A%5B%7Bname%3A%22maps%22%2Cversion%3A3%2Cother_params%3A%22sensor%3Dfalse%22%7D%5D%7D"></script> 
<script type="text/javascript" src="/scripts/maps/MarkerClusterer.js"></script> 
<script type="text/javascript" src="/scripts/maps/infobox.js"></script> 
<script type="text/javascript" src="/scripts/maps/gMaps2.js"></script> 
<script type="text/javascript" src="/scripts/maps/placeItemOnMap.js"></script> 
<script src="/scripts/contactFormAdmin.js"></script>

</div><!-- END PADDING -->