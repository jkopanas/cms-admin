<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$page_title} {if $product} :: #{$product.sku} {$product.title}{/if}</title>
<link href="/skin/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/css/tabs/tabcontent.css" />
<link rel="stylesheet" type="text/css" href="/css/autocomplete/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="/css/uploadify.css" />
<link rel="stylesheet" type="text/css" href="/css/importu.css" />
<link rel="stylesheet" href="/css/jquery.Jcrop.css" type="text/css" />
<link rel="stylesheet" href="/css/thickbox.css" type="text/css" />
<link rel="stylesheet" href="/css/jqueryFileTree.css" type="text/css" />
<link rel="stylesheet" href="/css/quiz.css" type="text/css" />
<link rel="stylesheet" href="/css/jquery-ui-1.8.10.custom.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="/css/jquery.multiselect1.css" />
<link rel="stylesheet" type="text/css" href="/css/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css" href="/css/jquery.multiselect_singleuse.css" />


<link href="/css/colorbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/scripts/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.cookie.js"></script>
<script type="text/javascript" src="/scripts/tabcontent.js"></script>
<script type="text/javascript" src="/scripts/jquery.livequery.js"></script>
<script type="text/javascript" src="/scripts/jquery.autocomplete.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/lang/el.js"></script>
<script type="text/javascript" src="/scripts/jquery.idTabs.min.js"></script>
<script type='text/javascript' src='/scripts/jquery.autocomplete.js'></script>
<script type='text/javascript' src='/scripts/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='/scripts/jquery.dimensions.js'></script>
<script type="text/javascript" src="/scripts/uploadify/swfobject.js"></script>
<script type="text/javascript" src="/scripts/uploadify/jquery.uploadify.v2.1.0.min.js"></script>
<script src="/scripts/thickbox-compressed.js"></script>
<script type="text/javascript" src="/scripts/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/scripts/importu.js"></script>
<script src="/scripts/jquery.Jcrop.min.js"></script>
<script src="/scripts/jquery.tablednd_0_5.js"></script>
<script src="/scripts/jqueryFileTree.js"></script>
<script src="/scripts/jquery.corner.js"></script>
<script src="/scripts/common.js"></script>
<script src="/scripts/jquery-ui-1.8.10.custom.min.js"></script>
<script src="/scripts/jquery.editinplace.js"></script>
<script type="text/javascript" src="/scripts/templates.min.js"></script>



</head>

 <body>

 <input type="hidden" name="module" value="{$CURRENT_MODULE.name}" />
<input type="hidden" name="loaded_language" value="{$BACK_LANG}" />
<!-- START MAIN CONTAINER -->
<div class="container">
{if $USER ne ""}
<div class="wrap">
<div id="wphead">
<div style="float:right; margin-right:5px;"><a href="http://www.net-tomorrow.com" target="_blank">Net-Tomorrow</a> MCMS v1.5</div>
<h1>{$SITE_NAME} <span>(<a href="{$URL}" target="_blank">View site »</a>)</span></h1>

</div>
{include file="admin/menu.tpl"}
<div style="clear:both"></div>
</div>
<!-- START INCLUDES -->

{include file=$include_file}
<!-- END INCLUDES -->
{else}{* NO USER *}
<div class="wrap">
<div id="wphead">
<h1>{$SITE_NAME} <span>(<a href="{$URL}" target="_blank">View site »</a>)</span></h1>
</div>
</div>
<div class="wrap seperator padding">
<h2>Authorization Required</h2>

{include file="admin/form.tpl"}

</div>
{/if}
</div><!-- END CONTAINER -->

<script type="text/javascript" src="/scripts/ajax.js"></script>
</body>
</html>