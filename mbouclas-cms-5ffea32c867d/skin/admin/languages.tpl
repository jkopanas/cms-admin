<div class="wrap padding seperator">
<h2><a href="{$ADMIN_URL}" class=navigationpath>{$lang.main_page}</a>&nbsp;::&nbsp; <a href="{$ADMIN_URL}/languages.php" class=navigationpath>{$lang.edit_lang}</a>
{if $selectedCountry} &nbsp;::&nbsp; {$selectedCountry.country}{/if}</h2>
</div><!-- END WRAP -->
{if !$countryCode}
<div class="debug"></div>
<div class="wrap padding seperator">
<h2>{$lang.available_languages}</h2>
<table border="0" width="100%">
<tr>
<th>{$lang.lang}</th>
<th width="240" align="center">{$lang.availability}</th>
<th width="240" align="center">{$lang.default_lang_admin}</th>
<th width="170" align="center">{$lang.default_lang}</th>
<th width="16" align="center">&nbsp;</th>
</tr>
{foreach from=$AvailableLanguages item=a}
<tr>
<td><a href="languages.php?code={$a.code}">{$a.country}</a></td>
<td align="center"><label><input type="radio" value="Y" name="active-{$a.code}" class="setLanguageAvailability" {if $a.active eq "Y"}checked{/if}> {$lang.yes}</label> <label><input type="radio" value="N" name="active-{$a.code}" {if $a.active eq "N"}checked{/if} class="setLanguageAvailability" /> {$lang.no}</label></td>
<td align="center"><input type="radio" name="default_lang_admin" class="SelectDefaultLang" value="{$a.code}" {if $DEFAULT_LANG_ADMIN eq $a.code}checked{/if} /></td>
<td align="center"><input type="radio" name="default_lang" class="SelectDefaultLang" value="{$a.code}" {if $DEFAULT_LANG eq $a.code}checked{/if} /></td>
<td align="center"><a href="languages.php?action=deleteLanguage&code={$a.code}" class="DeleteLanguage"  title="{$lang.delete}" rel="{$a.name}"><img src="/images/admin/delete_16.png" width="16" height="16" /></a></td>
</tr>
{/foreach}
</table>
</div><!-- END WRAP -->

<div class="wrap padding seperator">
<h2>{$lang.addNewLanguage}</h2>
<form  method="post" name="lang_form" id="addNewLanguage">
<div>
{$lang.choose_lang} : 
<SELECT name="new_language" class="saveData">
        <OPTION value="">{$lang.select_one}</OPTION>
		{foreach from=$AllCountryCodes item=a}
       <OPTION value="{$a.code}">{$a.country}</OPTION>    
		{/foreach}
      </SELECT>
    <input type="submit" id="addNewLanguage" name="Submit" class="button" value="{$lang.add_new}">  
</div>
</form>
</div><!-- END WRAP -->

{else}
<div class="wrap padding seperator">
<h2>{$lang.add_translation}</h2>
<form  method="post" name="add_form">
  <table width="100%"  border="0" cellpadding="5" cellspacing="5">
    <tr>
      <td width="10%">{$lang.topic}</td>
      <td width="90%"><select name="topic" id="topic" class="newTranslation">
        <option value="common" {if $smarty.post.lang_topic eq "common"}selected{/if}>Common</option>
        <option value="front" {if $smarty.post.lang_topic eq "front"}selected{/if}>Front End</option>
        <option value="admin" {if $smarty.post.lang_topic eq "admin"}selected{/if}>Admin</option>
        <option value="js" {if $smarty.post.lang_topic eq "js"}selected{/if}>Javascript</option>
      </select>
      <input type="hidden" name="code" value="{$smarty.get.language|default:$smarty.post.language}"></td>
    </tr>
    <tr>
      <td>{$lang.variable}</td>
      <td><input name="name" type="text" id="nane" class="newTranslation" /></td>
    </tr>
    <tr>
      <td>{$lang.description}</td>
      <td><input name="descr" type="text" id="description" class="newTranslation" /></td>
    </tr>
    <tr>
      <td>{$lang.value}</td>
      <td><textarea name="value" cols="60" rows="5" id="value" class="newTranslation" /></textarea>
      <br>
       <a href="javascript:void(0);" class="open-editor" rel="value">{$lang.advanced_editor}</a>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" name="Submit" value="{$lang.save}" class="button saveTranslationVariable">
      <input name="mode" type="hidden" id="mode" value="new_trans"></td>
    </tr>
  </table>
</form>
</div><!-- END WRAP -->

<div class="wrap padding seperator">
<h2>{$lang.filter} (<a href="javascript:void(0)" class="ShowFiltersTable Slide" rel="#FiltersTable">{$lang.show_hide_form}</a>)</h2>
<form  method="post" name="filter_form" id="FiltersForm">
  <table width="100%"  border="0" cellpadding="5" cellspacing="5" id="FiltersTable">
    <tr>
      <td width="10%">{$lang.topic}</td>
      <td width="90%"><select name="lang_topic" class="SearchData">
      <option value="all" selected>{$lang.all}</option>
        <option value="common">Common</option>
        <option value="front">Front End</option>
        <option value="admin">Admin</option>
		<option value="js">Javascript</option>
      </select>
      <input type="hidden" name="code" value="{$selectedCountry.code}" class="SearchData"/>
        <input name="search" type="hidden" id="search" value="1">
     <input name="ResultsDiv" type="hidden"  value="#ResultsPlaceHolder-div" class="SearchData">
    <input name="FormToHide" type="hidden"  value="#FiltersTable" class="SearchData">
      </td>
    </tr>
    <tr>
      <td>{$lang.filter}</td>
      <td><input name="filter" type="text" id="lang_filter2"  class="SearchData" /></td>
    </tr>
    <tr>
      <td>{$lang.results_per_page}</td>
      <td><input name="results_per_page" type="text" id="number" class="SearchData"  /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="ApplyFilters" id="ApplyFilters" value="{$lang.filter}" class="button"></td>
    </tr>
  </table>

</form>
</div><!-- END WRAP -->

<div id="SearchResults" class="wrap seperator padding hidden">
<h2>{$lang.searchResults}</h2>
<form name="ResultsPlaceHolder">
<div id="ResultsPlaceHolder-div"></div>
</form>
</div>

<div class="seperator"></div>
{/if}

<script src="/scripts/AdminLanguages.js"></script>