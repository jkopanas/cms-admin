{if $lang.software_updates}
<div class="wrap seperator padding">
<h2>Software Updates</h2>
{$lang.software_updates}
</div>
{/if}

<div class="wrap seperator padding">
  <h2>Θέλω να :</h2>
<ul>
<li><a href="{$URL}/modules/content/admin/new.php">Προσθέσω νέα σελίδα</a></li>
<li><a href="{$URL}/modules/content/admin/categories.php">Προσθέσω νέα κατηγορία</a></li>
{if $loaded_modules.products}<li><a href="{$URL}/modules/products/admin/new.php">Προσθέσω νέο πρϊόν</a></li>{/if}
</ul>
</div><!-- END WRAP -->


{if $loaded_modules.products}
<div class="wrap seperator padding">
  <div class="padding">
    
  <div id="zeitgeist"><h3>Με μια ματιά</h3>
  <ul>
  <li><img src="/images/admin/main_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/products/admin/">Αρχική</a></li>
  <li><img src="/images/admin/add_16.gif" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/products/admin/new.php">Προσθήκη</a></li>
  <li><img src="/images/admin/edit_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/products/admin/search.php">Τροποποίηση</a></li>
  <li><img src="/images/admin/search_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/products/admin/search.php">Αναζήτηση</a></li>
  <li><img src="/images/admin/categories_16.gif" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/products/admin/categories.php">Κατηγορίες</a></li>
  <li><img src="/images/admin/arrow_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/products/admin/mass_insert.php">Μαζική εισαγωγή</a></li>
  <li><img src="/images/admin/menus_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/products/admin/extra_fields.php">Εξτρά πεδία</a></li>
  <li><img src="/images/admin/prefs_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/products/admin/settings.php">Επιλογές</a></li>
  </ul>
  </div><!-- END HELP MODULE -->
  <h2>Προϊόντα</h2>
<!-- CONTENT STARTS HERE -->
  <div><!-- ITEMS LIST -->
<form name="latest_form" method="post">
{include file="modules/content/admin/display_content_table.tpl" items=$LatestProducts  form_name="latest_content_form" page="index.php" module="products"}
</form>
  </div><!-- END ITEMS LIST -->
    
    
    
    
  <!-- CONTENT ENDS HERE -->
  <div style="clear:both"></div>
  </div><!-- END PADDING -->
</div><!-- END WRAP -->
{/if}
<div class="wrap seperator padding">
  <div class="padding">
    
  <div id="zeitgeist"><h3>Με μια ματιά</h3>
  <ul>
  <li><img src="/images/admin/main_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/content/admin/">Αρχική</a></li>
  <li><img src="/images/admin/add_16.gif" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/content/admin/new.php">Προσθήκη</a></li>
  <li><img src="/images/admin/edit_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/content/admin/search.php">Τροποποίηση</a></li>
  <li><img src="/images/admin/search_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/content/admin/search.php">Αναζήτηση</a></li>
  <li><img src="/images/admin/categories_16.gif" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/content/admin/categories.php">Κατηγορίες</a></li>
  <li><img src="/images/admin/menus_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/content/admin/extra_fields.php">Εξτρά πεδία</a></li>
  <li><img src="/images/admin/prefs_16.png" width="16" height="16" align="absbottom" /> <a href="{$URL}/modules/content/admin/settings.php">Επιλογές</a></li>
  </ul>
  </div><!-- END HELP MODULE -->
  <h2> Περιεχόμενα</h2>
<!-- CONTENT STARTS HERE -->
  <div><!-- ITEMS LIST -->
<form name="latest_content_form" method="post">
{include file="modules/content/admin/display_content_table.tpl" items=$LatestContent  form_name="latest_content_form" page="index.php" module="content"}
</form>
  </div><!-- END ITEMS LIST -->
   
    
  <!-- CONTENT ENDS HERE -->
  <div style="clear:both"></div>
  </div><!-- END PADDING -->
</div><!-- END WRAP -->
