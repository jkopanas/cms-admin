<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Menu</title>
<link href="/skin/css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<input type="hidden" name="loaded_language" value="{$BACK_LANG}" />
<div class="seperator padding wrap">
<h2>{$lang.modify} {$menu.title}</h2>
{include file="admin/menuEditor.tpl" action="modify_form" formID="editMenuItem"} 
</div>

<script type="text/javascript" src="/scripts/jquery-1.6.1.min.js"></script>
<script src="/scripts/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.livequery.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="/scripts/jquery.colorbox-min.js"></script>
<script src="/scripts/common.js"></script>
<script src="/scripts/menuEditor.js"></script>
</body>
</html>
