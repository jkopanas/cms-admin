<div id="SomeElementId"></div>
 {capture name=dialog}
<form name="settings_form" id="settings_form" action="javascript:void(null);" onsubmit="submitSignup();">
  <table width="100%">
  {section name=r loop=$settings}
  <tr {cycle values=", class='TableSubHead'"}>
  <td width="17%">{$settings[r].comment}</td>
  <td width="83%">
{if $settings[r].type eq "text"}
   <input type="{$settings[r].type}" name="{$settings[r].name}-val" value="{$settings[r].value}" size="50">
 {elseif $settings[r].type eq "textarea"}
 <textarea name="{$settings[r].name}-val" cols="45" rows="5">{$settings[r].value}</textarea>
 {elseif $settings[r].type eq "select"}
 <select name="{$settings[r].name}-val">
 {if $settings[r].name eq "default_lang"}
 {section name=rp loop=$countries}
 <option value="{$countries[rp].code}" {if $countries[rp].code eq $settings[r].value}selected{/if}>{$countries[rp].country}</option>
 {/section}
 {elseif $settings[r].name eq "default_lang_admin"}
 {section name=rp loop=$countries}
 <option value="{$countries[rp].code}" {if $countries[rp].code eq $settings[r].value}selected{/if}>{$countries[rp].country}</option>
 {/section}
 {/if}
 </select>
 {elseif $settings[r].type eq "checkbox"}
 <input type="checkbox" name="{$settings[r].name}-val" value="1"{if $settings[r].value eq 1}checked{/if}>
 {elseif $settings[r].type eq "radio"}
  <input name="{$settings[r].name}-val" type="radio" value="1" {if $settings[r].value eq 1}checked{/if}>{$lang.on} <input name="{$settings[r].name}-val" type="radio" value="0" {if $settings[r].value eq 0}checked{/if}>{$lang.off}
 {elseif $settings[r].type eq "image"}
<div><img src="{if $settings[r].value}{$PRODUCT_IMAGES}/{$settings[r].value}{else}{$PRODUCT_IMAGES}/thumbs/{$DEFAULT_THUMB}{/if}" name="preview"  border="1">
		  <br>
<input type="hidden" name="{$settings[r].name}-val" {if $settings[r].value}value="{$settings[r].value}"{/if} id="{$settings[r].name}-val">
<input type="button" name="select" value="{$lang.add_image}" onclick="ImageSelector.select('{$settings[r].name}-val','form1');"/>
  </div>
 {/if}
  </td>
  </tr>
  {/section}
  <tr align="center">
  <td colspan="2"><input name="submit" type="submit" value="{$lang.save}" id="submitButton">
    <input name="mode" type="hidden" id="mode" value="modify">
    <input name="cat" type="hidden" id="cat" value="{$smarty.get.cat|default:$smarty.post.cat}"></td>
  </tr>
  </table>
</form>
  {/capture}
{include file="admin/dialog.tpl" content=$smarty.capture.dialog title=$lang.$cat extra="width=100%"}

