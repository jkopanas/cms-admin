<div class="wrap padding seperator">

{if $Modules}
<h2><a href="{$ADMIN_URL}" class=navigationpath>{$lang.main_page}</a>&nbsp;::&nbsp; <a href="{$ADMIN_URL}/settings_manager.php" class=navigationpath>{$lang.title_EditSettings}</a> / {$lang.title_SelectModule}</h2>
<ul>
{foreach from=$Modules item=a}
<li><a href="settings_manager.php?id={$a.module_id}">{$a.name}</a></li>
{/foreach}
</ul>
{/if}
{if $Module}
<h2><a href="{$ADMIN_URL}" class=navigationpath>{$lang.main_page}</a>&nbsp;::&nbsp; <a href="{$ADMIN_URL}/settings_manager.php" class=navigationpath>{$lang.title_SelectModule}</a> / {$Module.name}</h2>
<input type="hidden" value="SaveCategoriesOrder" name="action" />
<div id="debugArea"></div>
<ul id="IdTabs"  class="idTabs">
{foreach from=$Settings item=v key=k name=b}
<li><a  href="#tab{$smarty.foreach.b.iteration}" rel="tab{$smarty.foreach.b.iteration}" class="selected">{$k}</a></li>
{/foreach}
</ul>
<div class="clear"></div>
<div id="tab-container" class="clear">
{foreach from=$Settings item=v key=k name=b}
<div id="tab{$smarty.foreach.b.iteration}" class="tabcontent">
<table cellpadding="3" cellspacing="3" width="100%" class="DragDrop">
<tr class="nodrop nodrag">
		<th width="16">&nbsp;<input type="hidden" name="TableOrder" class="settings" value="" /></th>
		<th scope="col">{$lang.title}</th>
        <th scope="col">{$lang.description}</th>
        <th>{$lang.actions}</th>
	</tr>
{foreach from=$v item=a}
<tr class="{cycle values='alternate,'}" id="Order{$cat[a].categoryid}"> 
<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
<td> <a href="#" rel="{$a.id}" class="editSetting">{$a.name}</a></td>
				<td>{$cat[a].description|truncate:50:" ...":true}</td>
		  <td width="16"><a href="{$MODULE_FOLDER}/categories.php?action=delete&amp;cat={$a.id}" onclick="return confirm('You are about to delete the category \'{$cat[a].category}\'.  All of its items will go to the default category.\n  \'OK\' to delete, \'Cancel\' to stop.')" class="delete" title="Delete Category {$cat[a].category}"><img src="/images/admin/delete_16.png" width="16" height="16" /></a></td>
				</tr>
{/foreach}
</table>
</div><!-- END TAB -->
{/foreach}
</div><!-- END CONTAINER -->

{/if}
</div><!-- END BOX -->