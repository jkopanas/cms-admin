<div id="content_actions" class="fr"><button class="btn btn_d saveEfields">{$lang.save}</button></div>
<div class="formEl_a">
{foreach from=$extra_fields item=a name=b}
{if $a.type eq "radio" OR $a.type eq "checkbox"}{assign var="checked" value=1}{else}{assign var="checked" value=0}{/if}
<div class="sepH_b cf ">
{if $a.type eq 'radio'}{assign var=classes value="inpt_r"}{elseif $a.type eq 'checkbox'}{assign var=classes value="inpt_c"}{else}{assign var=classes value="inpt_a"}{/if}
<label for="title" class="lbl_a">{$a.field}</label> {include file="common/formFields/`$a.type`.tpl" field=$a prefix=$prefix classes="`$classes` efields" val=$a.value checked=$checked extra="data-id=\"`$a.fieldid`\""}
{if $a.type eq "text"}<a href="#" class="translateEfieldValues" title="{$lang.edit_all_translations}" rel="{$a.fieldid}" data-itemid="{$id}">{$lang.translate}</a>
{elseif $a.type eq "textarea"}<a href="#" class="translateEfieldValues" title="{$lang.edit_all_translations}" rel="{$a.fieldid}" data-itemid="{$id}">{$lang.translate}</a>
<br /><a href="#" class="ckEditor" rel="{$prefix}{$field.var_name}" data-height="150">{$lang.advanced_editor}</a>
{/if}
</li>
</div>
{/foreach}
</div>

<script src="/scripts/admin2/items/itemsExtraFields.js"></script>