<textarea name="fileTypes" class="hidden">{$fileTypes}</textarea>
<div class="tabStrip">
    <ul>
        <li  class="k-state-active">Upload new</li>
        <li>From URL</li>
        <li>From gallery</li>
    </ul>
<div>
<script id="newImageTemplate" type="text/x-kendo-template">
	<li><img src=" #= src #" /> <Br /><a href="\\#" class="imageWindow" rel=" #= id #">{$lang.modify}</a></li>
</script>
<input type="hidden" name="fileType" id="fileType" value="{$smarty.get.filetype|default:$smarty.get.fileType}" /><input type="hidden" name="imageCategory" id="imageCategory" value="{$smarty.get.imagecategory}" /> <input type="hidden" name="callBack" id="callBack" value="{$smarty.get.callback}" /> <input type="hidden" name="parent" id="parent" value="{$smarty.get.parent}" />
    <div class="uploadContainer">
        <input name="image" id="image" type="file" class="uploader"  data-fileType="image" />
    </div>
    
    <ul id="uplaodedFiles"></ul>
</div><!-- END TAB UPLOAD-->
<div>
{$smarty.get.filetype}
</div><!-- END TAB URL -->
<div>

</div><!-- END TAB CMS -->
</div><!-- END TABS -->
<div class="clear"></div>

<script id="newthumbTemplate" type="text/x-kendo-template">

<table wdith="100%">
		<tr valign="top">
			<td style="vertical-align:none">
			<p><img class="thumbnail" src="#= response.uploadedFile.thumb.CleanDir #/#= response.uploadedFile.thumb.name #" align="left" alt=""></p>
			<p></p>
			</td>
			<td>
			<p><strong>File name:</strong> #= response.uploadedFile.name #</p>
			<p><strong>File size:</strong> #= response.uploadedFile.filesize #</p>
			<p><strong>File type:</strong> #= response.uploadedFile.mime #</p>
			</td></tr>

		</table>
	<style>
	.t tr td{ vertical-align:auto; }
	</style>
</script>
<script id="newimageTemplate" type="text/x-kendo-template">
<table wdith="100%" class="t">
		<tr valign="top">
			<td style="vertical-align:auto">
			<p><img src=" #= response.uploadedFile.thumb.CleanDir #/#= response.uploadedFile.thumb.name #" /> </p>
			<p></p>
			</td>
			<td>
			<p><strong>File name:</strong> #= response.uploadedFile.name #</p>
			<p><strong>File size:</strong> #= response.uploadedFile.filesize #</p>
			<p><strong>File type:</strong> #= response.uploadedFile.mime #</p>
			</td></tr>

</table>

	
</script>
<script id="newdocumentTemplate" type="text/x-kendo-template">
<table wdith="100%">
		<tr valign="top">
			<td style="vertical-align:none">
			<p><img class="thumbnail" src="/images/admin/text.png" align="left" alt=""></p>
			<p></p>
			</td>
			<td>
			<p><strong>File name:</strong> #= response.uploadedFile.name #</p>
			<p><strong>File size:</strong> #= response.uploadedFile.filesize #</p>
			<p><strong>File type:</strong> #= response.uploadedFile.mime #</p>
			</td></tr>

		</table>
</script>
<script id="newvideoTemplate" type="text/x-kendo-template">
<table wdith="100%">
		<tr valign="top">
			<td style="vertical-align:none">
			<p><img class="thumbnail" src="/images/admin/text.png" align="left" alt=""></p>
			<p></p>
			</td>
			<td>
			<p><strong>File name:</strong> #= response.uploadedFile.name #</p>
			<p><strong>File size:</strong> #= response.uploadedFile.filesize #</p>
			<p><strong>File type:</strong> #= response.uploadedFile.mime #</p>
			</td></tr>

		</table>
</script>
<script src="/scripts/admin2/items/fileUploader.js"></script>