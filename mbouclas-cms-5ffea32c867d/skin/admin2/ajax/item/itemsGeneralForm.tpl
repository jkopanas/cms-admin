<form action="" id="itemGeneralForm" method="post" class="toValidate">
{if $quickEdit}
<input type="hidden" name="quickEdit" id="quickEdit" value="{$quickEdit}" />
<div class="debug spacer-bottom"></div>
{/if}

<input type="hidden" name="itemID" value="{$item.id}" /> 
<div id="window"></div>
<div id="content_actions" class="fr"><input type="submit" class="btn btn_d saveForm" value="{$lang.save}"></div>
<div class="clear"></div>
<div class="dp75">
<div class="formEl_a">
<div class="padding">
<div class="sepH_b">
    <label for="title" class="lbl_a">{$lang.title}</label>
    <input type="text" id="title" name="title" class="inpt_a toSave"   value='{$item.title}' required  />
    <span class="f_help">Here you can enter a headline.</span>
</div>
<div class="sepH_b">
    <label for="permalink" class="lbl_a">{$lang.permalink}</label>
    <input type="text" id="permalink" name="permalink" class="inpt_a toSave" value="{$item.permalink}" />

</div>
{if $current_module.name eq "products"}
<div class="sepH_b">
    <label for="sku" class="lbl_a">SKU</label>
    <input type="text" id="sku" name="sku" class="inpt_a toSave" value="{$item.sku}" />

</div>
{/if}
<div class="sepH_b cf">
    <label for="country_multiple" class="lbl_a">{$lang.main_cat}</label>
    <select name="categoryid" class="{if !$quickEdit}mSelect{/if} inpt_a toSave" id="categoryid" data-placeholder="Select one" data-table="categories">
{foreach from=$allcategories item=a}
<option value="{$a.categoryid}"{if $simple_categories|is_array AND  $a.categoryid|array_key_exists:$simple_categories AND $simple_categories[$a.categoryid] eq 1} selected="selected"{/if}>{$a.category}</option>
{/foreach}
</select>
</div>

<div class="sepH_b cf">
    <label for="country_multiple" class="lbl_a">{$lang.additional_cat}</label>
    <select name="categoryIDs" multiple="MULTIPLE" class="mSelect inpt_a toSave" id="categories" data-placeholder="Select one" data-table="categories">
{foreach from=$allcategories item=a}
<option value="{$a.categoryid}"{if $simple_categories|is_array AND  $a.categoryid|array_key_exists:$simple_categories AND $simple_categories[$a.categoryid] ne 1} selected{/if}>{$a.category}</option>
{/foreach}
</select>
</div>
<div class="sepH_c cf">
    <label for="description" class="lbl_a">{$lang.description}</label>
    <textarea name="description" id="description" cols="30" rows="5" class=" txtar_a toSave">{$item.description}</textarea><br />
    <a href="#" class="ckEditor" rel="description" data-height="150">{$lang.advanced_editor}</a>
</div>
<div class="sepH_c cf">
    <label for="description_long" class="lbl_a">{$lang.long_desc}</label>
    <textarea name="description_long" id="description_long" cols="10" rows="40" class=" txtar_a toSave">{$item.description_long}</textarea><br />
    <a href="#" class="ckEditor" rel="description_long">{$lang.advanced_editor}</a>
</div>

</div>
</div><!-- END PADDING -->
</div><!-- END LEFT -->
<div class="dp25 spacer-top">
<div id="rightForm">
<div class="formEl_a">

<div class="sepH_c cf">
<label class="lbl_a">{$lang.availability}</label>
<select name="active" id="active" class="inpt_a toSave">
        <option value="0" {if $item.active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $item.active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select>
</div>
{if $box.settings.includes.eshop AND $current_module.name|in_array:$box.settings.includes.eshop.modules}{include file=$box.settings.includes.eshop.file}{/if}

<script id="newThumbTemplate" type="text/x-kendo-template">
	<img src=" #= src #" /> 
</script>
<div id="itemThumb" {if !$item}class="hidden"{/if}>
<h2>{$lang.thumb}</h2>
<div id="imagePlaceHolder" class="spacer-top">{if $item.image_full.thumb}<img src="{$item.image_full.thumb}" /> {/if}</div>
<div class="clear"></div>
<a href="#" class="btn_c btn uploadWindow" rel="#imagePlaceHolder" data-fileType="thumb" data-imageCategory="0" data-callback="quick_thumb_uploaded">{$lang.add_file}</a>
</div>
<div class="seperator"></div>
<h2>Tags</h2>
<input type="text" id="tags" value="" class="inpt_a" />
<input type="hidden" id="hiddenTags" name="tags" value="" class="toSave" data-module="tags" />
<ul id="tagsContainer"></ul>
<div class="seperator"></div>
<textarea name="itemTags" class="hidden">{$itemTags}</textarea>
{if $pageSettings}
<div class="spacer-top">
<h2>{$lang.settings}</h2>
<div class="spacer-bottom"></div>
{include file="common/drawSettings.tpl" prefix="settings_" classes="toSave inpt_a" pageSettings=$pageSettings val=$item.settings}
</div>
{/if}
</div>
</div>

</div><!-- END RIGHT -->
<div class="clear"></div>
<div id="content_actions" class="fr"><input type="submit" class="btn btn_d saveForm" value="{$lang.save}"></div>
</form>

<script id="tagTemplate" type="text/x-kendo-template">
	<li data-orderby="#= orderby #">#= tag # <a href="\\#" class="deleteTag"><img src="/images/icons/icon-pricetable-false.png" align="absmiddle" /></a></li> 
</script>
<style>
#autoCompleteResults { background:white; z-index:9999; position:absolute; border:1px solid #ebebeb }
#autoCompleteResults li { padding:5px; color:#777777}
#autoCompleteResults li:hover, .hovered { background:#eeeeee;}
#autoCompleteResults  .matched { color:black; font-weight:bold; background:#ffffcc; }
#tagsContainer li { float:left; margin-right:10px; margin-bottom:10px; }
</style>

<script src="/scripts/admin2/items/itemsGeneralForm.js"></script>