<div id="gr">
<div id="itemSearchFilters" class="formEl_a	box_c_content cf searchBox">
		<div class="sepH_b  spacer-right">
       		<label for="id" class="lbl_a">#ID :</label>
        	<input name="id"  type="text"  value="" class="filterdata inpt_a" data-field="id" data-searchtype="EQ" />
        </div>
        <div class="sepH_b  spacer-right">
        	<label for="title" class="lbl_a">{$lang.title} *:</label>
        	<input name="title"  type="text"  value=""  class="filterdata inpt_a" data-field="title" data-searchtype="LIKE"/>
        </div>
       <div class="spacer-right spacer-top">
        	<input name="submit" type="button"  id="itemSearch"  class="btn btn_a submitSearch" value="{$lang.search}" /> 
        </div>
</div><!-- END MENU ITEM DETAILS -->
</div>
<div id="gr1"></div>
<script id="quickSearchActions" type="text/kendo-ui-template">
	<a href="\\#" class="addItem"><span id="#= id #" class="k-icon k-add k-add-cat"  data-module="{$smarty.get.module}" ></span></a>
</script>
<script type="text/x-kendo-template" id="toolBar">
	<div class="k-button k-button-icontext Filter float-right showFilters" id="Filter-PagesSearch" data-id="gridview">Filter&nbsp;&nbsp;<span class=" k-icon k-filter"></span></div>
</script>
<script src="/scripts/admin2/items/itemsSearch.js"></script>
<script src="/scripts/admin2/items/itemsRelated.js"></script>
{literal}
<script>
var m = $('#gr').searchItems($.extend(true,itemSearchSettings,{drawGrid : {dataSource : {transport: {read : {url :"/ajax/loader.php?file=itemsSearch.php&module=" + mcms.mcmsSettings.module + '&action=pagesFilter'}}}}})).data('search');
$('#Filter-PagesSearch').live('click', function(e){
	console.log(m.grid.dataSource.data())
});

/*$('#itemSearch').live('click', function(e){
	var data =  mcms.formData($('#itemSearchFilters .filterdata'),{ getData:true});
	$.extend(m.grid.dataSource.transport.options.read.data,{data:data});
	m.grid.dataSource.fetch();
	console.log(m.grid.dataSource.data())
	if (m.grid.dataSource.data().length == 0) {
		alert('No results');
		return;
	}
	m.grid.refresh();
});*/



var x = $('#gr1').searchItems($.extend(true,itemCategorySettings,{drawGrid : {dataSource : {transport: {read : {url :"/ajax/loader.php?file=items/itemsCategories.php&module=" + mcms.mcmsSettings.module + '&action=getCategories'}}}}})).data('search');

</script>
{/literal}