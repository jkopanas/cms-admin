<div id="test"></div>
<div id="templates" class="hidden">
<script id="existingImages" type="text/x-kendo-template">
<tr id="Copies#= id #">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
    <td><a href="#= main #" rel="pick"><img src="#= thumb #" width="60" height="60" /></a></td>
    <td><input name="id" type="checkbox" id="check_values" value="#= id #" class="check-values" data-bind="click: checkOne" /></td>
    <td class="avail"># if (available != 0) { # <span class="notification ok_bg">#= $.lang.yes #</span>  # } else { # <span class="notification error_bg">#= $.lang.no #</span> # } #</td>
    <td width="150"><select name="type-#= id #" id="type-#= id #" class="toSave">
{section name=b loop=$image_categories}
<option value="{$image_categories[b].id}" {if $image_categories[b].id eq $a.type}selected{/if}>{$image_categories[b].title}</option>
{/section}
</select></td>
    <td width="150"><input type="text" value="#= alt #" name="alt-#= id #" class="toSave" /></td>
    <td width="150"><input type="text" value="#= title #" name="title-#= id #" class="toSave" /></td>
    <td>sdsd</td>
  </tr>
</script>
<script id="newImages" type="text/x-kendo-template">
<tr id="Copies#= thumb.imageid #">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
    <td><a href="#= main.CleanDir #/#= main.name #" rel="pick"><img src="#= thumb.CleanDir #/#= thumb.name #" width="60" height="60" /></a></td>
    <td><input name="id" type="checkbox" id="check_values" value="#= thumb.imageid #" class="check-values" data-bind="click: checkOne" /></td>
    <td class="avail"><span class="notification error_bg">#= $.lang.no #</span></td>
    <td width="150"><select name="type-#= thumb.imageid #" id="type-#= thumb.imageid #" class="toSave">
{section name=b loop=$image_categories}
<option value="{$image_categories[b].id}" {if $image_categories[b].id eq $a.type}selected{/if}>{$image_categories[b].title}</option>
{/section}
</select></td>
    <td width="150"><input type="text"  name="alt-#= thumb.imageid #" class="toSave" /></td>
    <td width="150"><input type="text" name="title-#= thumb.imageid #" class="toSave" /></td>
    <td>sdsd</td>
  </tr>
</script>
<script id="newThumb" type="text/x-kendo-template">
<tr id="itemThumb" data-id="#= thumb.imageid #">
    <td><a href=" #= thumb.CleanDir #/#= main.name #""  rel="pick"><img src=" #= thumb.CleanDir #/#= thumb.name #"" width="60" height="60" /></a></td>
    <td width="150"><input type="text" name="alt-#= thumb.imageid #" class="toSave" /></td>
    <td width="150"><input type="text"name="title-#= thumb.imageid #" class="toSave" /></td>
</tr>
</script>

<script id="existingThumb" type="text/x-kendo-template">
 <tr id="itemThumb" data-id="#= id #">
    <td><a href=" #= main #"  rel="pick"><img src=" #= thumb #" width="60" height="60" /></a></td>
    <td width="150"><input type="text" name="alt-#= id #" value="#= alt #" class="toSave" /></td>
    <td width="150"><input type="text"name="title-#= id #" value="#= title #" class="toSave" /></td>
	</tr>
</script>
<script id="newDocument" type="text/x-kendo-template">
  <tr id="Copies#= id #">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
    <td><input name="id" type="checkbox" id="check_values" value="#= id #" class="check-values" data-bind="click: checkOne" /></td>
     <td class="avail"><span class="notification error_bg">#= $.lang.no #</span></td>
    <td width="150"><input name="title-#= id #" type="text" value="#= name #" class="toSave" size="40" /></td>
    <td align="center">#= mime #</td>
    <td width="150" align="center">#= filesize #</td>
    <td width="150">#= mcms.strftime( Math.round((new Date()).getTime() / 1000),'%d/%m/%Y @ %H:%M') #</td>
    </tr>
</script>
<script id="existingDocument" type="text/x-kendo-template">
  <tr id="Copies#= id #" data-itemID="#= id #">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
    <td><input name="id" type="checkbox" id="check_values" value="#= id #" class="check-values" data-bind="click: checkOne" /></td>
    <td class="avail"># if (available != 0) { # <span class="notification ok_bg">#= $.lang.yes #</span>  # } else { # <span class="notification error_bg">#= $.lang.no #</span> # } #</td>
    <td width="150"><input name="title-#= id #" type="text" value="#= title #"  class="toSave" size="40" /></td>
    <td align="center">#= mime #</td>
    <td width="150" align="center">#= FileSize #</td>
    <td width="150">#= mcms.strftime(date_added,'%d/%m/%Y @ %H:%M') #</td>
    </tr>
</script>
 <script id="delete-confirmation" type="text/x-kendo-template">
   <p class="delete-message">{$lang.confirm_deletion}</p> <br/>
   <button class="delete-confirm k-button">{$lang.delete}</button>
   <a href="javascript:void(0);" class="float-right spacer-right delete-cancel">{$lang.lbl_cancel}</a>
  </script>

<script id="newVideo" type="text/x-kendo-template">
	<tr id="Copies#= id #" data-itemID="#= id #">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
    <td><input name="id" type="checkbox" id="check_values" value="#= id #" class="check-values" data-bind="click: checkOne" /></td>
    <td class="avail"><span class="notification error_bg">#= $.lang.no #</span></td>
    <td width="150"><input name="title-#= id #" type="text" value="#= title #"  class="toSave" size="40" /></td>
    <td align="center"><textarea name="embed-#= id #" id="embed-#= id #" class="toSave">#= embed #</textarea></td>
    <td align="center">#= type #</td>
    <td align="center">#= filesize #</td>
    <td>#= mcms.strftime(date_added,'%d/%m/%Y @ %H:%M') #</td>
    <td><a href="\\#" rel="# if (embed) { # embed-#= id # # } else { # #= id # # } #" class="previewVideo">Προεπισκόπιση</a></td>
    </tr>
</script>
<script id="existingVideo" type="text/x-kendo-template">

	<tr id="Copies#= id #" data-itemID="#= id #">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
     <td><input name="id" type="checkbox" id="check_values" value="#= id #" class="check-values" data-bind="click: checkOne" /></td>
    <td class="avail"># if (available != 0) { # <span class="notification ok_bg">#= $.lang.yes #</span>  # } else { # <span class="notification error_bg">#= $.lang.no #</span> # } #</td>
    <td width="150"><input name="title-#= id #" type="text" value="#= title #"  class="toSave" size="40" /></td>
    <td align="center"><textarea name="embed-#= id #" id="embed-#= id #" class="toSave">#= embed #</textarea></td>
    <td align="center">#= type #</td>
    <td align="center">#= FileSize #</td>
    <td>#= mcms.strftime(date_added,'%d/%m/%Y @ %H:%M') #</td>
    <td><a href="\\#" rel="# if (embed && !file_url) { # embed-#= id # # } else { # #= file_url # # } #" class="previewVideo">Προεπισκόπιση</a></td>
    </tr>
	
</script>
<textarea id="data">{$data}</textarea>
</div>

<div class="padding">
 
<div id="tabStrip" class="tabStrip">
    <ul>
        <li class="k-state-active">{$lang.thumb}</li>
        {foreach name=b from=$image_categories item=a}
        <li id="{$a.id}">{$a.display_title}</li>
        {/foreach}
        <li>{$lang.lbl_documents}</li>
        <li>{$lang.videos}</li>
    </ul>
    
    

    <div class="tab">
    <h1>{$imgC[$k].display_title|default:$lang.thumb}</h1>
    <a href="#" class="btn_c btn uploadWindow" rel="#imagePlaceHolder" data-fileType="thumb" data-callback="thumb_uploaded" data-imageCategory="0">{$lang.add_file}</a>
    <div class="seperator"></div>
<table width="100%" border="0" cellspacing="5" cellpadding="5" id="t-thumbs" class="table_a smpl_tbl" data-media="images">
<tr><td colspan="4"><a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="deleteThumb"><span class="k-icon k-delete"></span>{$lang.delete}</a>  <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="updateItems"><span class="k-icon k-save"></span>{$lang.save}</a></td>
 
  <tr>
    <th width="120">&nbsp;</th>
    <th scope="col">{$lang.alt}</th>
    <th scope="col">{$lang.title}</th>
    <th>&nbsp;</th>
  </tr>
<tbody class="content">
 
</tbody>
</table>
    </div>
    
{foreach from=$image_categories item=n key=k}
<div class="tab" id="tab-{$k}">
<h1>{$n.display_title}</h1>
    <a href="#" class="btn_c btn uploadWindow" rel="#imagePlaceHolder" data-fileType="image" data-imageCategory="{$n.id}" data-callback="image_uploaded" data-parent="{$n.title}">{$lang.add_files}</a>  
    <div class="seperator"></div>
<table width="100%" border="0" cellspacing="5" cellpadding="5" id="t-{$n.title}" class="table_a smpl_tbl" data-media="images">
      <tr class="nodrop nodrag">
        <td colspan="8">{$lang.withSelectedDo}  (<span class="messages">0</span> {$lang.selected} ):  <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="enableItems"><span class="k-icon k-enable"></span>{$lang.enable}</a>  <a class="k-button" href="#" data-bind="click: doActions" data-method="disableItems"><span class="k-icon k-disable"></span>{$lang.disable}</a>  <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="deleteItems"><span class="k-icon k-delete"></span>{$lang.delete}</a>  <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="updateItems"><span class="k-icon k-save"></span>{$lang.save}</a>
        
    
        </td>
      </tr>
  <tr class="nodrop nodrag">
    <th width="16">&nbsp;<input type="hidden" name="TableOrder" class="toSave" value="" /></th>
    <th width="120">&nbsp;</th>
    <th width="16"><input type="checkbox" id="total_check" name="ids[1]" class="check-all" data-bind="click: checkAll" /></th>
    <th width="145" scope="col">{$lang.availability}</th>
    <th width="150" scope="col">{$lang.category}</th>
    <th scope="col">{$lang.alt}</th>
    <th scope="col">{$lang.title}</th>
    <th>&nbsp;</th>
  </tr>
  <tbody class="content">

  </tbody>
</table>

</div><!-- END TAB -->
    {/foreach}
<div class="tab" id="tab-docs">
<h1>{$lang.lbl_documents}</h1>
    <a href="#" class="btn_c btn uploadWindow" rel="#imagePlaceHolder" data-fileType="document"  data-callback="document_uploaded" data-parent="t-documents">{$lang.add_files}</a>  
    <div class="seperator"></div>
<table width="100%" border="0" cellspacing="5" cellpadding="5" id="t-documents" class="table_a smpl_tbl" data-media="document">
      <tr class="nodrop nodrag">
        <td colspan="8">{$lang.withSelectedDo}  (<span class="messages">0</span> {$lang.selected} ): <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="enableItems"><span class="k-icon k-enable"></span>{$lang.enable}</a>  <a class="k-button" href="#" data-bind="click: doActions" data-method="disableItems"><span class="k-icon k-disable"></span>{$lang.disable}</a>  <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="deleteItems"><span class="k-icon k-delete"></span>{$lang.delete}</a>  <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="updateItems"><span class="k-icon k-save"></span>{$lang.save}</a>

        </td>
      </tr>
  <tr class="nodrop nodrag">
    <th width="16">&nbsp;<input type="hidden" value="SaveDocumentsOrder" name="action" /> <input type="hidden" name="TableOrder" class="toSave" value="" />
<input type="hidden" name="media" value="{$media}" class="toSave" /></th>
    <th width="16"><input type="checkbox" id="total_check" name="ids[1]" class="check-all" data-bind="click: checkAll" /></th>
    <th scope="col">{$lang.availability}</th>
    <th scope="col">{$lang.file_name}</th>
    <th scope="col">{$lang.file_type}</th>
    <th scope="col">{$lang.file_size}</th>
    <th scope="col">{$lang.date_added}</th>
    </tr>
  <tbody class="content">

  </tbody>
</table>
</div><!-- END DOCUMENTS TAB -->
<div class="tab">
<h1>{$lang.videos}</h1>
    <a href="#" class="btn_c btn uploadWindow" rel="#imagePlaceHolder" data-fileType="video"  data-callback="video_uploaded" data-parent="t-videos">{$lang.add_files}</a>  
    <div class="seperator"></div>
   <table width="100%" border="0" cellspacing="5" cellpadding="5" id="t-videos" class="table_a smpl_tbl" data-media="video">
      <tr class="nodrop nodrag">
        <td colspan="9">{$lang.withSelectedDo}  (<span class="messages">0</span> {$lang.selected} ): <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="enableItems"><span class="k-icon k-enable"></span>{$lang.enable}</a>  <a class="k-button" href="#" data-bind="click: doActions" data-method="disableItems"><span class="k-icon k-disable"></span>{$lang.disable}</a>  <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="deleteItems"><span class="k-icon k-delete"></span>{$lang.delete}</a>  <a class="k-button k-button-icontext k-grid-delete" href="#" data-bind="click: doActions" data-method="updateItems"><span class="k-icon k-save"></span>{$lang.save}</a>

        </td>
      </tr>
  <tr class="nodrop nodrag">
    <th width="16">&nbsp;<input type="hidden" value="SaveVideosOrder" name="action" /> <input type="hidden" name="TableOrder" class="toSave" value="" />
<input type="hidden" name="media" value="{$media}" class="toSave" /></th>
    <th width="16"><input type="checkbox" id="total_check" name="ids[1]" class="check-all" data-bind="click: checkAll" /></th>
    <th scope="col">{$lang.availability}</th>
    <th scope="col">{$lang.file_name}</th>
    <th scope="col">{$lang.embed}</th>
    <th scope="col">{$lang.file_type}</th>
    <th scope="col">{$lang.file_size}</th>
    <th scope="col">{$lang.date_added}</th>
    <th scope="col">&nbsp;</th>
    </tr>
  <tbody class="content">

  </tbody>
</table>
    
    
</div><!-- END VIDEO TAB -->   

</div>

<script src="/scripts/admin2/items/itemsMediaFiles.js"></script>