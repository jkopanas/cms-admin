<div class="box_c {if $ajax}dashBox{/if} {$class} sortable" id="{$name}" data-id="{$id}"  {$dataProperties}>
    <h3 class="box_c_heading cf">
        <span class="fl">{$title}</span>
        <img src="../skin/images/admin2/blank.gif" alt="" class="fr square_x_11 wRemove" />
        <img src="../skin/images/admin2/blank.gif" alt="" class="fr switch_arrows_a wToogle" />
        
    </h3>
    <div class="box_c_content">
    	{if $content != ""}
        {include file="$content"}
        {/if}
        <div class="seperator"></div>
    </div>
</div><!-- END DASHBOX -->