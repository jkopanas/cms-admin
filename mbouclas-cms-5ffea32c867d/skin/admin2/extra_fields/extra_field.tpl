
<div  class="formEl_a">  
<div class="float-left dp50" >
		<fieldset>
                <legend>{$lang.basic_settings}</legend>
	  
		<form id="GroupForm" name="GroupForm" class="padding">
				<div class="sepH_b  spacer-right">
       				<label class="lbl_a" for="field_name">{$lang.field_name} :</label>
        			<input type="text" class="ExtraData inpt_a" value="{$field.field}" id="field" name="field" required>
        		</div>
       			<div class="sepH_b  spacer-right">
       				<label class="lbl_a" for="var_name">{$lang.var_name} :</label>
        			<input type="text" class="ExtraData inpt_a" value="{$field.var_name}" id="var_name" name="var_name" required>
        		</div>
       			<div class="sepH_b  spacer-right">
       				<label class="lbl_a" for="description">{$lang.description} :</label>
					<textarea name="settings_description" id="settings_description" cols="50" rows="3" class="ExtraData inpt_a">{$field.settings.description}</textarea>
        		</div>
       			<div class="sepH_b  spacer-right">
       				<label class="lbl_a" for="search">{$lang.description} ({$lang.search}) :</label>
					<textarea name="settings_searchdes" id="settings_searchdes" cols="50" rows="3" class="ExtraData inpt_a">{$field.settings.searchdes}</textarea>
        		</div>
		</form>	
    		<div class="sepH_b ">
 			<label class="lbl_a" for="field_type" >{$lang.field_type}</label>
    		<select name="type" id="type" class="ExtraData inpt_a">
		          	 <option value="text" {if $field.type == "text"}selected{/if} >{$lang.text}</option>
          			 <option value="area" {if $field.type == "area"}selected{/if} >{$lang.textarea}</option>
          			 <option value="radio" {if $field.type == "radio"}selected{/if} >{$lang.radiobtn}</option>
          			 <option value="hidden" {if $field.type == "hidden"}selected{/if} >{$lang.hidden}</option>	
          			 <option value="document" {if $field.type == "document"}selected{/if} >{$lang.file}</option>
          			 <option value="image" {if $field.type == "image"}selected{/if} >{$lang.image}</option>
          			 <option value="media" {if $field.type == "media"}selected{/if} >{$lang.media}</option>
          			 <option value="link" {if $field.type == "link"}selected{/if} >{$lang.link}</option>
          			 <option value="multiselect" {if $field.type == "multiselect"}selected{/if} >{$lang.multiselect}</option>
      		</select> 
	 	</div>
		<div class="sepH_b multioptions {if $field.type != "multiselect"}hidden{/if}">
 			<label class="lbl_a" for="field_type" >{$lang.data_type}</label>
    		<select name="settings_data_type" id="data_type" class="ExtraData inpt_a">
		          	<option value="checkbox" {if $field.settings.data_type == "checkbox"}selected{/if} >{$lang.checkbox}</option>
					<option value="radio" # {if $field.settings.data_type == "radio"}selected{/if} >{$lang.radiobtn}</option>
					<option value="dropdown" {if $field.settings.data_type == "dropdown"}selected{/if} >{$lang.dropdown}</option>
					<option value="multiple" {if $field.settings.data_type == "multiple"}selected{/if} >Multiple</option>
      		</select> 
  		 </div>
		 <div class="sepH_b {if $field.settings.data_type != "dropdown"}hidden{/if}"  id="range" >
 			<label class="lbl_a" for="field_type" >{$lang.search_field}</label>
    		<select name="settings_datatyperange" id="settings_datatyperange" class="ExtraData inpt_a">
		          	<option value="single" {if $field.settings.datatyperange == "single"}selected{/if} >{$lang.single}</option>
					<option value="range" {if $field.settings.datatyperange == "range"}selected{/if} >{$lang.range}</option>
      		</select> 
  		 </div>
		<div class="sepH_b multioptions {if $field.type != "multiselect"}hidden{/if}" >
 			<label class="lbl_a" for="field_type" >{$lang.data}</label>
			<table width="100%" border="0" cellspacing="5" cellpadding="5" id="dataTable" class="table_a smpl_tbl  spacer-top" >
				<tr>
					<td class="k-header">&nbsp;<input type="hidden" name="TableOrder" class="settings" value="" /></td>
					<td class="k-header">ID</td>
					<td class="k-header">{$lang.text}</td>
					<td class="k-header"><a href="\#" id="addRow" class="k-icon k-add"></a></td>
				</tr>
				<tbody class="extrafieldcontent">
				{assign var=i value=0}
				{if $field.settings.data}
					 {foreach from=$field.settings.data item=a name=b}
							<tr class="dataRow" id="row{$i}">
								<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
								<td><input type="text" name="efieldKey_{$i}" class="ExtraData inpt_a" value="{$a}"></td>
								<td><input type="text" name="efieldVal_{$i}" class="ExtraData inpt_a" value="{$a}"></td>
								<td>{if $i != 0}<a href="#" class="removeRow" rel="row{$i}">{$lang.remove}</a>{/if}</td>
							</tr>
						{assign var=i value=$i+1}
					{/foreach}
				{else}
						<tr class="dataRow" id="row{$i}">
							<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
							<td><input type="text" name="efieldKey_{$i}" class="ExtraData inpt_a" value="{$k}"></td>
							<td><input type="text" name="efieldVal_{$i}" class="ExtraData inpt_a" value="{$a}"></td>
							<td></td>
						</tr>
						{assign var=i value=$i+1}
			 	{/if}
				</tbody>
			</table>
			<input type="hidden" value="{$i}" id="icounter"/>
  		 </div>
	
            
		
		 </fieldset>
    </div>
   
    
    <div class="float-right dp50">
    	<fieldset>
                <legend>{$lang.extra_settings}</legend>
     	 		<div class=" float-left dp50">
 		<div class="sepH_b  spacer-right">
 			<label class="lbl_a" for="type" >{$lang.type} :</label>
    		<select name="settings_type" id="settings_type" class="ExtraData inpt_a ">
		       		<option value="">{$lang.none}</option>
    				<option value="icon" {if $field.settings.type eq 'icon'} selected {/if}>icon</option>
    				<option value="info" {if $field.settings.type eq 'info'} selected {/if}>info</option>
      		</select> 
		</div>
		 <div  class="sepH_b  spacer-right">
 			<label class="lbl_a" for="type" for="availability" >{$lang.availability}</label>
    		<select name="active" class="ExtraData inpt_a">
		          	 <option value="N" {if $field.active == "N"} selected {/if} >{$lang.disabled}</option>
          			 <option value="Y"  {if $field.active == "Y"} selected {/if} >{$lang.enabled}</option>
      		</select> 
  		 </div>
 		<div   class="sepH_b  spacer-right">
 			<label class="lbl_a" for="type" for="settings_validation" >{$lang.validationRequirements}</label>
    		<select name="settings_validation"class="ExtraData inpt_a">
		          	 <option value="0" {if $field.settings.validation == "0"} selected {/if} >{$lang.no}</option>
          			 <option value="1"  {if $field.settings.validation == "1"} selected {/if} >{$lang.yes}</option>
      		</select> 
  		 </div>
 		<div   class="sepH_b  spacer-right">
 			<label class="lbl_a" for="type" for="allow_search" >{$lang.allow_search}</label>
    		<select name="settings_search" class="ExtraData inpt_a">
		          	 <option value="0" {if $field.settings.search == "0"} selected {/if} >{$lang.no}</option>
          			 <option value="1" {if $field.settings.search == "1"} selected {/if} >{$lang.yes}</option>
      		</select> 
  		 </div>
 		<div  class="sepH_b  spacer-right">
 			<label class="lbl_a" for="type" for="isMap" >{$lang.isMap}</label>
    		<select name="settings_isMap" class="ExtraData inpt_a">
		          	 <option value="0" {if $field.settings.isMap == "0"} selected {/if} >{$lang.no}</option>
          			 <option value="1" {if  $field.settings.isMap == "1"} selected {/if} >{$lang.yes}</option>
      		</select> 
  		 </div>
		<div  class="sepH_b  spacer-right">
 			<label class="lbl_a" for="type" for="loopfilter" >{$lang.loopfilter}</label>
    		<select name="settings_loopfilter" class="ExtraData inpt_a">
		          	 <option value="0" {if $field.settings.loopfilter == "0"} selected {/if} >{$lang.no}</option>
          			 <option value="1" {if $field.settings.loopfilter == "1"} selected {/if} >{$lang.yes}</option>
      		</select>
		</div>
		</div>
		<div class=" float-left dp50">
			<div class="sepH_b">
 				<label class="lbl_a" for="field_type" >{$lang.categories} :</label>
				<label><input type="radio" value="%" name="categoryids" class="SlideUp ExtraData" rel="\#categories_list" {$checkedAll} /> {$lang.all}</label>&nbsp;&nbsp;
				<label><input type="radio" name="categoryids" class="SlideUp ExtraData" value="0" rel="\#categories_list" {$checkedNone} /> {$lang.none}</label> &nbsp;&nbsp;
				<label><input type="radio" name="categoryids" class="SlideDown" value="0" rel="\#categories_list" {$checkedCat}  /> {$lang.choose}</label>
				<div class="categories_list {if !$checkedCat}hidden{/if} spacer-top" id="categories_list">
				<select name="categories_list" multiple="MULTIPLE" class="mSelect ExtraData" id="categories_list" style="width: 200px;" data-placeholder="Select one" data-table="categories">
				 {foreach from=$field.catids item=a name=b}
          		 	<option value="{$a.categoryid}" {$a.selected}> {$a.category} </option>
 				 {/foreach}
      			</select> 		
   			</div>
			<div class="sepH_b ">
 				<label class="lbl_a" for="field_type" >{$lang.groups} : </label>
				<div class-"dp100">
				<select name="group_list" multiple="MULTIPLE" class="mSelect  ExtraData" id="group_list"  style="width: 200px;" data-placeholder="Select one" data-table="group">
				{foreach from=$groups item=a name=b}
          				<option value="{$a.id}"  {$a.selected} >{$a.title}</option>
 				{/foreach}
      			</select> 		
				</div>
			</div>
		</div>
		</div>
		
		</fieldset>
     </div> 
     <div style="clear:both;"></div>
     	<div id="GroupForm" name="GroupForm"  class="formEl_a float-right padding">
		<input type="button" id="SaveEfield" name="SaveEfield" data-id="{$field.fieldid}" class="btn btn_b" value="{$lang.save}" />
		<input type="hidden" name="mode" value="SaveEfield" />
</div>
  </div>  