<div class="login_wrapper">
		<div class="loginBox">
			<div class="heading cf">
				<ul class="login_tabs fr cf float-left">
					<li >Είσοδος διαχειριστή</li>
					
					<!--<li style="display:none"><a href="#password">Forgoten password</a></li>-->
				</ul>
			</div>
			<div class="content">
				<div class="login_panes formEl_a">
					<div>
						<form action="{$redir}" method="post" class="formEl sepH_c" id="form_login">
							<div class="msg_box msg_error" id="allErrors" style="display:none"></div>
							<div class="sepH_a">
								<label for="lusername" class="lbl_a">{$lang.username} :</label>
								<input name="uname" type="text" id="uname" value="{$smarty.post.uname}" class="inpt_a"/>
							</div>
							<div class="sepH_b">
								<label for="lpassword" class="lbl_a">{$lang.password}:</label>
								<input name="pass" type="password" id="pass" class="inpt_a" />
							</div>
							<div class="sepH_b">
								<input type="checkbox" class="inpt_c" id="remember" />
								<label for="remember" class="lbl_c">Remember me</label>
							</div>
							<center><button class="btn_a btn" type="submit">Είσοδος</button></center>
						</form>
						<div class="content_btm">
							MCMS Copyright © 2005-2012 <a href="http://www.net-tomorrow.com" target="_blank" class="small"> Net-tomorrow.com</a> 
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
    