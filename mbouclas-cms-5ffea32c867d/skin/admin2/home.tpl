<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <title>{$SITE_NAME}</title>
    <link rel="shortcut icon" href="favicon.ico" />
	
    <link rel="stylesheet" href="/skin/css/admin2/style.css" media="all" type="text/css" />
    <link rel="stylesheet" href="/skin/css/admin2/kendoadmin.css" media="all" type="text/css" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans" type="text/css" />
	<link href="/scripts/kendo/styles/kendo.common.min.css" rel="stylesheet"/>
	<link href="/scripts/kendo/styles/kendo.metro.min.css" rel="stylesheet"/>
	<link href="/scripts/multiSelect/chosen.css" rel="stylesheet"/>

<script type="text/javascript" src="/scripts/head.load.min.js"></script>
<script type="text/javascript" src="/scripts/jquery-1.7.1.min.js"></script>

<script type="text/javascript">
var mcms;
head.js('/scripts/kendo/js/kendo.web.min.js');
head.js('/scripts/ckeditor/ckeditor.js');
head.js('/scripts/ckeditor/adapters/jquery.js');
head.js('/scripts/admin2/mcms.js');
head.js('/scripts/multiSelect/chosen.jquery.min.js');


head.ready(function(){

	mcms =  $(document).mCms({ lang:$('input[name=loaded_language]').val(),module:$('input[name=module]').val()}).data().mcms;//Initialize
	$.lang = mcms.getLang();
	
});
</script>

     
</head>

<body class="bg_c stD  fluid {if $sidebar}sidebar{/if}">

   <input type="hidden" name="loaded_language" value="{$BACK_LANG}" />
   <input type="hidden" name="current_module" value="{$current_module}" />
   <input type="hidden" name="current_module_name" value="{$current_module.name}" />
   <input type="hidden" name="module" value="{$current_module.name}" />
   <textarea name="systemPrefs" class="hidden">{$SYSTEM_PREFS}</textarea>
   {if $USER ne ""}
    	
	<div id="top_bar">
		<div class="wrapper cf">
		<div id="edit_form"></div> 
			<ul id="CPMenu" class="float-left">
				<li >
						<img src="../skin/images/admin2/user_noPhoto26.gif" height="16" /> {$USER_NAME} {$USER_SURNAME}
						<ul>
							<li><a href="#" data-itemid="{$ID}" id="{$ID}" class='k-edit_user'><img src="../skin/images/admin2/user_noPhoto26.gif"  height="16" />{$lang.edit_user}</a></li>
							<li><a href="index.php?logout" ><img src="../skin/images/admin2/shutdown.png"  height="16" />{$lang.logout}</a></li>
						</ul>
				</li>
			</ul>
			
			
			<ul class="float-left spacer-left"><li><a href="{$URL}" target="_blank">{$lang.front_end_preview}</a></li></ul>
                        <div class="float-right spacer-bottom spacer-right quickSearchContainer">
                        <a href="javascript:void(0);" class="moreBtn">click</a>
                      
                        <label>{$lang.quick_search}: </label> <input type="text" id="quickSearch" style="width:350px;" />
                        <div class="options">
                        <span style="font-size:11px; font-weight:bold;">{$lang.search_in} :</span>
						{foreach from=$loaded_modules item=a name=b}
						{if $a.is_item}		
                        <label>{$lang[$a.name]} <input type="radio" name="searchModule" value="{$a.name}" {if $smarty.foreach.b.first}checked{/if} /></label>
                        {/if}
                        {/foreach}
                        </div>
              <ul class="new_items fl spacer-left hidden" id="systemMessages">
				<li><span class="count_el updatesNo"></span> <a href="{$ADMIN_URL}/updater/">{$lang.updates_found}</a></li>
                <li>|</li>
			 </ul>  
					<script id="autoCompleteTemplate" type="text/kendo-ui-template">
	<a href="{$ADMIN_URL}items.php?module=content&id=#= id #\\#!/itemsGeneral" class="foundItem">\\# #= id #. #= title #</a>
	<div class="links">
		<a href="{$ADMIN_URL}items.php?module=content&id=#= id #\\#!/mediaFiles" class="foundItem">#= $.lang.mediaFiles #</a> | 
		<a href="{$ADMIN_URL}items.php?module=content&id=#= id #\\#!/itemsTranslations" class="foundItem">#= $.lang.translations #</a> | 
		<a href="{$ADMIN_URL}items.php?module=content&id=#= id #\\#!/extraFields" class="foundItem">#= $.lang.extra_fields #</a>
		</div>
</script>
            </div>
     <div class="clear"></div>
     
			<!-- 
			<ul class="new_items fr">
				<li class="sep"><span class="count_el">2</span> <a href="#">new messages</a></li>
				<li class="sep"><span class="count_el">10</span> <a href="#">new comments</a></li>
				<li class="sep"><span class="count_el">4</span> <a href="#">tasks</a></li>
				<li id="slide_open">sliding panel<img src="/skin/images/admin2/blank.gif" alt="" class="arrow_down_a" /></li>
			 </ul>  -->
		</div>
	</div>

	<div id="header">
		<div class="wrapper cf">
			<h1>{$lang["area_`$area`"]}</h1>
		</div>
	</div>
    
	<div id="main" class="clear">

		<div class="wrapper">

			<div id="main_section" class="cf brdrrad_a">
			  
				<ul id="mainMenu" >
					
					{if isset($breadcrumb)}
						{include file=$breadcrumb}
					{else}
						<li class="parent">
						
							<a href="/manage" class="vam">{$lang.home}</a>
							</li>

							 {foreach from=$loaded_modules item=a}
								{if $a.active AND $a.on_menu}					
								<li><a href="{$a.main_file}">{$lang[$a.menu_title_admin]}</a>
                                {include file="admin2/menus.tpl" module=$a}
                                </li>
								{/if}
							{/foreach}
	                            <li><a href="languages.php">{$lang.translations}</a></li>
                                <li><a href="menus.php">{$lang.menuEditor}</a></li>
								<li><a href="SettingManager.php?config">{$lang.settings}</a></li>


						</li>
						
					{/if}
				</ul>
				<div class="seperator"></div>
				<div id="content_wrapper">
					<div id="main_content">
					{include file=$include_file}
					
					</div><!-- END MAIN CONTENT -->
				</div>
			
							{if $sidebar}
				{include file=$sidebar}
			 {/if}
			   
			</div>
		</div>
	</div>
	
	<div id="footer">
	   <div class="wrapper">
		  <div class="cf ftr_content">
			<p class="fl"><strong>MCMS</strong> Copyright &copy; 2005-2012 <a href="http://www.net-tomorrow.com" target="_blank">Net-tomorrow.com</a></p>
			<a href="#top_bar" class="toTop">{$lang.totop}</a>
		  </div>
	   </div>	
	</div>
	
	{else}{* NO USER *}
<div id="top_bar">
<div class="float-right spacer-right">
<h2> <span><a href="{$URL}" target="_blank">»  Πίσω στο site </a></span></h2>

</div>
</div>
{include file="admin2/form.tpl"}
{/if}
	
	
<script type="text/javascript">

	head.js("/scripts/admin2/jquery.stickyPanel.js");
//	head.js("/scripts/admin2/xbreadcrumbs.js");
	head.js('/scripts/admin2/global.js');
</script>
   
</body>
</html>