<textarea class="hidden" name="currentModuleEnc">{$current_module|json_encode}</textarea>
<div id="filtersWindow">{include file="admin2/ajax/item/quickSearchFilters.tpl"}</div>
<div id="itemWindow"></div>
<div id="uploadWindow"></div>
<div class="box_c dp100" >
           <h3 class="box_c_heading cf">
	           <span class="fl spcer-left">Άρθρα </span> </h3> 
		   <div class="box_c_content cf">
		<div id="load_content">
        
        <div id="gr"></div>
        
        </div>
	 </div>
</div> <!-- end posts -->
<script id="quickSearchActions" type="text/kendo-ui-template">
	<a href="\\#" class="addItem"><span id="#= id #" class="k-icon k-add k-add-cat"  data-module="{$smarty.get.module}" ></span></a>
</script>
<script id="priceTemplate" type="text/kendo-ui-template" class="kTemplate" data-name="price">
<div id="price-#= id #">

	# if (typeof eshop.price != 'undefined') { # &euro;#= mcms.moneyFormat(parseFloat(eshop.price)) # # } else { # - # } #
</div>
</script>
<script id="skuTemplate" type="text/kendo-ui-template" class="kTemplate" data-name="sku">
	<div id="sku-#= id #">#= sku #</div>
</script>

<script type="text/x-kendo-template" id="toolBar">
<div class="fl spacer-right" style="padding-top:5px;">
{$lang.withSelectedDo}  (<span class="messages">0</span> {$lang.selected} )

</div>
	<div class="k-button k-button-icontext Filter float-right" id="Filter-PagesSearch" data-id="gridview">Filter&nbsp;&nbsp;<span class=" k-icon k-filter"></span></div>
</script>
<script type="text/x-kendo-template" id="itemTemplate" class="kTemplate" data-name="title">
<div id="title-#= id #">
<div class="itemRow" style="display:block; height:30px; padding-bottom:5px;">
	<strong><a href="items.php?id=#= id #&module=#= module #">#= title #</a></strong>
<div class="row-actions hidden"><span class="edit"><a href="items.php?module={$current_module.name}&id=#= id #" >{$lang.modify}</a> | </span><span class="inline hide-if-no-js"><a href="\\#" class="quickEdit" data-id="#= id #" data-module="{$current_module.name}" title="#= title #">Quick&nbsp;Edit</a> | </span><span class="view"><a href="{$URL}/{$current_module.name}_page.php?id=#= id #" target="_blank">{$lang.preview}</a></span></div>
</div>
</div>
</script>
<script type="text/x-kendo-template" id="categoryTemplate" class="kTemplate" data-name="category">
<div id="category-#= id #">
# $.each(category,function(k,v){ #
# if (v) { #
	<a href="\\#" class="categories" rel="#= v.categoryid #">#= v.category #</a> # if(k+1 < category.length) { #,#} #
	# }  #
# });  #
</div>
</script>
<script type="text/x-kendo-template" id="date_addedTemplate" class="kTemplate" data-name="date_added">
<div id="date_added-#= id #">
<div style="text-align:center;">#= mcms.strftime(date_added,'%d/%m/%Y @ %H:%M') #</div>
</div>
</script>

<script type="text/x-kendo-template" id="checkTemplate" class="kTemplate" data-name="checkBox">
<div id="checkBox-#= id #">
<input type="checkbox" class="check-values" value="#= id #" name="selectedID" />
</div>
</script>

<script type="text/x-kendo-template" id="availabilityTemplate" class="kTemplate" data-name="active">
<div id="active-#= id #">
<div style="text-align:center;"># if (active != 0) { # <span class="notification ok_bg">#= $.lang.yes #</span>  # } else { # <span class="notification error_bg">#= $.lang.no #</span> # } #</div>
</div>
</script>

		<script id="id-commands-gridview" type="text/kendo-ui-template">
				<div class="formEl_a" data-itemid='#= id #' name="action" data-module="#= module #" >
				<select  class='inpt_a gotoDash' id="names#= id #" >
					<option value="">Select Action ... </option>
					<option value="/manage/items.php?module=#= module #&id=#= id #">Content Manage</option>
					<option value="/manage/items.php?module=#= module #&id=#= id #\#!/mediaFiles">Media Files</option>
					<option value="/manage/items.php?module=#= module #&id=#= id #\#!/itemsTranslations">Languages</option>
					<option value="/manage/items.php?module=#= module #&id=#= id #\#!/extraFields">Extra Fields</option>
				</select>
				</div>
        </script>
        
         <script id="template-toolbar-PagesSearch" type="text/kendo-ui-template">	
			{if !current_module}
			{foreach from=$curr_modules item=a name=b}
			<div class="k-button k-button-icontext margin-left ModuleFilter"  data-id="{$a}">{$a}</div>
			{/foreach}
			{/if}
		</script>
		
		
		<script type="text/x-kendo-template" id="template-active-gridview">
               # if (active != 0) { # <span class="fl notification ok_bg">#= $.lang.yes #</span>  # } else { # <span class="fl notification error_bg">#= $.lang.no #</span> # } #
        </script>

<script>head.js("../scripts/admin2/grid.js");</script>
<script>head.js("../scripts/admin2/items/itemsSearch.js");</script>
<script>head.js("../scripts/admin2/itemsHome.js");</script>
