
<script id="id-toolbar-lang" type="text/x-kendo-template">
<div class='k-button k-button-icontext details-button float-left'  ><span class=' k-icon k-add'></span>{$lang.new_record}</div>
{foreach from=$AvailableLanguages item=a}
<div class='k-button k-button-icontext  float-right langcode'  data-code="{$a.code}"  > <img src="{$a.image}" width="16" /> {$a.country}</div>
{/foreach}

</script>

<div class="debug"></div>
<div class="dp100">
<div class="box_c" />
<h3 class="box_c_heading cf">
	<span class="fl spacer-left">{$lang.available_variables}</span>
</h3>
<div class="box_c_content cf">    
<div id="gridLanguage"></div>
<div id="details"></div>
</div>
</div>
</div>

<div class="dp100">
<div class="box_c" />
<h3 class="box_c_heading cf">
	<span class="fl spacer-left">{$lang.available_languages}</span>
</h3>
<div class="box_c_content cf formEl_a">  
<table id="MainLang" class="table_a" >
<tr>
<td  class="k-header tac">{$lang.language}</td>
<td  class="k-header tac">{$lang.availability}</td>
<td  class="k-header tac">{$lang.default_lang_admin}</td>
<td  class="k-header tac">{$lang.default_lang}</td>
<td class="tac" ><a href ="#" id="createLang"><span class="k-icon k-add"></span></a></td>
</tr>
{foreach from=$AvailableLanguages item=a}
<tr>
<td class="tac" id="{$a.code}" >{$a.country}</td>
<td align="center"><label><input type="radio" value="Y" name="active-{$a.code}" data-action="active" class="save setLanguageAvailability inpt_b" {if $a.active eq "Y"}checked{/if}> {$lang.yes}</label> <label><input type="radio" value="N" data-action="active"  name="active-{$a.code}" {if $a.active eq "N"}checked{/if} class="save setLanguageAvailability" /> {$lang.no}</label></td>
<td align="center"><input type="radio" name="default_lang_admin"  data-action="adefault" class=" save SelectDefaultLang inpt_b" value="{$a.code}" {if $DEFAULT_LANG_ADMIN eq $a.code}checked{/if} /></td>
<td align="center"><input type="radio" name="default_lang"  data-action="sdefault" class="save SelectDefaultLang inpt_b" value="{$a.code}" {if $DEFAULT_LANG eq $a.code}checked{/if} /></td>
<td align="center"><a href="#{$a.code}" class="DeleteLanguage "  title="{$lang.delete}" rel="{$a.name}"><span class="k-icon k-delete"></span></a></td>
</tr>
{/foreach}
</table>

<SELECT  name="new_language" class="saveData hidden">
        <OPTION value="">{$lang.select_one}</OPTION>
		{foreach from=$AllCountryCodes item=a}
       <OPTION value="{$a.code}">{$a.country}</OPTION>    
		{/foreach}
</SELECT>

</div>
</div>
</div>

	<script id="delete-confirmation" type="text/x-kendo-template">
			<p class="delete-message" >{$lang.confirm_deletion}</p> <br/>
			<button class="delete-confirm k-button" data-id="#= item.id #" data-name="#= item.name #">{$lang.delete}</button>
			<a href="javascript:void(0);" class="float-right spacer-right delete-cancel">{$lang.lbl_cancel}</a>
		</script>
		
		
		<script id="delete-confirmation_lang" type="text/x-kendo-template">
			<p class="delete-message" >{$lang.confirm_deletion}</p> <br/>
			<button class="k-button delete-confirm_lang" data-id="#= item.id #" data-name="#= item.name #">{$lang.delete}</button>
			<a href="javascript:void(0);" class="float-right spacer-right delete-cancel">{$lang.lbl_cancel}</a>
		</script>

<script>
head.js('../scripts/admin2/AdminLanguages.js');

</script>
