<div class="login_wrapper">
		<div class="loginBox">
			<div class="heading cf">
				<ul class="login_tabs fr cf">
					<li><a href="#login">{$lang.login}</a></li>
					<li><a href="#register">{$lang.register}</a></li>
					<!--<li style="display:none"><a href="#password">Forgoten password</a></li>-->
				</ul>
			</div>
			<div class="content">
				<div class="login_panes formEl_a">
					<div>
						<form action="dashboard.html" method="post" class="formEl sepH_c" id="form_login">
							<div class="msg_box msg_error" id="allErrors" style="display:none"></div>
							<div class="sepH_a">
								<label for="lusername" class="lbl_a">{$lang.username}:</label>
								<input type="text" id="lusername" name="username" class="inpt_a" />
							</div>
							<div class="sepH_b">
								<label for="lpassword" class="lbl_a">{$lang.password}:</label>
								<input type="password" id="lpassword" name="password" class="inpt_a" />
							</div>
							<div class="sepH_b">
								<input type="checkbox" class="inpt_c" id="remember" />
								<label for="remember" class="lbl_c">Remember me</label>
							</div>
							<button class="btn_a btn" type="submit">{$lang.login}</button>
						</form>
						<div class="content_btm">
						<!-- 	<a href="#password" class="small">Forgot your username or password?</a> -->
						</div>
					</div>
					<div>
						
						<!-- <p class="sepH_b">You just need to enter a username and your e-mail adress. Password will be e-mailed to You.</p> -->
						<form id="userForm" action="" method="POST" class="formEl sepH_c">
						   <input type="hidden" id="action" value="save_user.html" />
							<div class="sepH_a">
								<label class="lbl_a">{$lang.username} :</label>
								<input type="text" name="uname"  class="userData inpt_a" />
							</div>
							<div class="sepH_a">
								<label  class="lbl_a">{$lang.email} :</label>
								<input type="text" name="email" class="userData inpt_a" />
							</div>
							<div class="sepH_a">
								<label class="lbl_a">{$lang.password} :</label>
								<input type="password" id="password" maxlength="10" name="pass" class="userData inpt_a" />
							</div>
							<div class="sepH_a">
								<label class="lbl_a">{$lang.confirm_pass} :</label>
								<input type="password" maxlength="10" name="confirm_password" class="effect inpt_a" />
							</div>
							<div ><input type="submit" name="Enter" value=" {$lang.register} " id="register_user"  class="btn_a btn baseeffect"></div>
							
						</form>
					</div>
		
				</div>
			</div>
		</div>
	</div>
    	<script type="text/javascript">
    	head.js("../scripts/member.js");
		head.js("../scripts/admin2/jquery.tools.min.js");
		head.js("../scripts/validator.js");
		head.js("../scripts/admin2/login.js");
	</script>