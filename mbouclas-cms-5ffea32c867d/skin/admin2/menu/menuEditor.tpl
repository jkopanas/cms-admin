{if !$menu}
<div class="dp75">
<div class="box_c">
<h3 class="box_c_heading cf">
	<span class="fl spacer-left">{$lang.lbl_availableMenus}</span>
</h3>


         	<style scoped>
               span.k-widget { width: 83%; }
            </style>
<div class="box_c_content cf formEl_a">
	<div id="grid"></div>
</div>
</div>
</div>
<div class="dp25">
<div class="box_c">
<h3 class="box_c_heading cf">
	<span class="fl spacer-left">{if $menu}{$lang.modify} {$menu.title}{else}{$lang.lbl_addNewMenu}{/if}</span>
</h3>
<form name="NewMenus" id="NewMenus" class="formEl_a" >
<div class="box_c_content cf">     
  		<div class="sepH_b">
        	<label class="lbl_a" for="title" >{$lang.title} *: </label>
        	<input name="title" id="title" type="text" class="inpt_a menu" value="{$menu.title}" required validationMessage=" {$lang.error_requiredField}" />
        </div>
        <div class="sepH_b">
        	<label class="lbl_a" for="title" >{$lang.lbl_varName} *: </label>
        	<input name="var" id="var" type="text" class="inpt_a menu" value="{$menu.var}" required validationMessage=" {$lang.error_requiredField}" />
        </div>
        <div class="sepH_b">
 			<label class="lbl_a" for="title" >{$lang.availability} *: </label>
    		<select name="active" id="active" class="menu" required validationMessage=" {$lang.error_requiredField}" >
		        <option value="0" {if $menu.active eq "0"}selected{/if}>{$lang.disabled}</option>
        		<option value="1" {if $menu.active eq "1"}selected{/if}>{$lang.enabled}</option>
      		</select> 
  		</div> 
        <div class="sepH_b">
        	<label class="lbl_a" for="title" > {$lang.description}: ({$lang.optional}) </label>
        	<textarea name="description" rows="5" cols="60"  class=" txtar_a menu" id="desc">{$menu.description}</textarea>
        	<a href="#" class="ckEditor" rel="desc" data-height="150">{$lang.advanced_editor}</a>
		</div>

        <p  align="left"><input name="action" value="{$action|default:'add'}" type="hidden" /><input type="hidden" name="id" value="{$menu.id}" /><input name="submit" type="submit" class="button" value="{$lang.save}" />
        </p>
</div>
</form>
</div>
</div>
</div>

</div>
{/if}
<script src="/scripts/admin2/menu/menuEditor.js"></script>