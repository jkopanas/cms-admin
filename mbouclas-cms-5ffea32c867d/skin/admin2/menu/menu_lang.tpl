<form name="singleVar" id="singleVar" class="formEl_a padding">
<h2>Translations</h2>
<table class="table_a spacer-top" >
<tr class="TableHead">
  <th width="50"><center>{$lang.language}</center></th>
      <th ><center>{$lang.value}</center></th>
  </tr>
	{foreach from=$LangTranslate item=a}
	<tr>
  		<td class="vam tac" ><strong>{$a.code}</strong></td>
  		<td style="width:150px;"><textarea id="{$a.field}-{$a.code}" cols="10" rows="5"  class="SaveTranslate" name="{$a.field}-{$a.code}" data-code="{$a.code}" data-field="{$a.field}" required validationMessage="Η τιμή δεν μπορέι να είναι κενή">{$a.value}</textarea>
      		<br>
      		<a href="#" class="ckEditor" data-height="150" rel="{$a.field}-{$a.code}">{$lang.advanced_editor}</a>
  		</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="3" ><input type="button" class="btn btn_a saveSingleTranslation float-right" data-id="{$a.uid}" data-table="{$a.table}" data-action="{$action}" value="{$lang.save}" /></td>
	</tr>
</table>
</form>