<div id="delete-confirmation"></div>

		<script id="delete-confirmation_menu" type="text/x-kendo-template">
			<p class="delete-message" >{$lang.confirm_deletion}</p> <br/>
			<button class="k-button delete-confirm" data-id="#= item #">{$lang.delete}</button>
			<a href="\#" class="float-right spacer-right delete-cancel">{$lang.lbl_cancel}</a>
		</script>

<div class="debugArea"></div>
{if $menu}
<div class="dp50">
<div class="box_c" />
<h3 class="box_c_heading cf">
	<span class="fl spacer-left">{$lang.items} </span>
</h3>


         	<style scoped>      
 			 
                .k-in span {
                	position:relative;
                	float:right;
                	padding-left: 10px;
                }


                
                #treeview .k-item {
                	padding-left: 15px;
                	padding-top: 5px;
                }

               
            </style>

<div class="box_c_content cf">   

	<input type="hidden" id="menuid"  name="menuid" value="{$smarty.get.id}" />   

	<script id="id-templatet" type="text/kendo-ui-template">	
			<span class='items float-left' id='list_#= item.id #' data-level='#= item.level #'  data-parent='#= item.parentid #'>#= item.text #</span>
			<div data-id='#= item.id #' class="k-icon k-refresh float-right TranslateMenuItem pointer"></div>
			<div data-id='#= item.id #' class="k-icon k-edit float-right edit pointer"></div>
			<div data-id='#= item.id #' class="k-icon k-delete float-right delete pointer"></div>
	</script>
	<input type="hidden" id="" class="grid_posted" data-grid="gridview_category" name="subs" value="1" />
	<div id="treeview" ></div>

</div>

</div>
</div>

	{foreach from=$layout.boxes  key=k  item=a name=b}
			{foreach from=$a item=c name=d}
		 		<input type="hidden" name="{$c.name}" data-position="{$k}" data-box="{$c.id}" class="AllBox" />
		 	{/foreach}
	{/foreach}
	

 <div id="edit_form"></div>
<div class="dp50" id="mainItem">
	{foreach from=$layout.boxes.H item=a name=b}
	<div id="{$a.name}" class="boxItem"  data-boxID="{$a.id}" ></div>
	{/foreach}
	<ul id="panelbar" class="panels panelBar">
	{foreach from=$layout.boxes  key=k item=c name=b}
		{if $k != "H"}
			{foreach from=$c  item=a name=d}
				<li class=" {if $smarty.foreach.b.first}k-state-active{/if}  boxItem"  data-boxID="{$a.id}" data-trigger="{$a.settings.trigger}" data-filter="{$a.settings.filter}"   data-mode="{$a.settings.mode}">{$lang[$a.title]|default:$a.title}
		  			<div id="{$a.name}" class="padding">
		  			</div>
				</li>		
			{/foreach}
		{/if}
	{/foreach}
		<li>Link
			<div>
			{include file="admin2/menu/menuLinkForm.tpl" class='addItem'}
			</div>
		</li>
	</ul>
</div>

		{foreach from=$modules item=a name=b}
		
		<script id="id-commands-gridsview_{$a}" class="id-commands-gridsview_{$a}" type="text/kendo-ui-template">
				<span data-itemid='#= id #' class="saveGrid k-icon k-add pointer" data-type="cat"  data-module="{$a}" ></span>  
        </script>
        
        <script  class="columns-gridsview_{$a}" type="text/kendo-ui-template">			
				<th data-order="0" data-field="checkbox" data-width="10%" data-filterable="false" data-type="string"  data-title="<input type='checkbox' data-id='gridsview_{$a}' class='check-all' id='total_check' />"><input type="checkbox" class="check-all" id="total_check"/></th>
        </script>
        
        <script type="text/x-kendo-template" id="template-checkbox-gridsview_{$a}">
			<div id="checkBox-#= id #">
				<input type="checkbox" class="check-values" value="#= id #" name="selectedID" />
			</div>
		</script>
		
	    <script type="text/x-kendo-template" id="template-text-gridsview_{$a}">
			#= unescape(text) #
		</script>
		
		{/foreach}


		<script id="id-commands-gridview" class="id-commands-gridview" type="text/kendo-ui-template">
			<div class="k-button k-button-icontext saveGrid" id='#= id #' data-itemid='#= id #' data-type="item"  data-module="#= module #">
				Add&nbsp;&nbsp;<span class="k-icon k-add pointer" ></span>		
			</div>
        </script>
        
        <script type="text/x-kendo-template" id="template-active-gridview">
			# if (active != 0) { # <span class="fl notification ok_bg">#= $.lang.yes #</span>  # } else { # <span class="fl notification error_bg">#= $.lang.no #</span> # } #
		</script>
       
       <script type="text/x-kendo-template" id="template-toolbar-PagesSearch">
				<div data-id="#= id #" id="Filter-PagesSearch" class="k-button k-button-icontext Filter float-right">
                    Filter&nbsp;&nbsp;<span class=" k-icon k-filter"></span>
                </div>
       </script>
       
        <script type="text/x-kendo-template" id="template-text-gridsview_content">
			#= unescape(text) #
		</script>
		
		<script type="text/x-kendo-template" id="template-toolbar-CategorySearch">
		                <div class="fl spacer-right" style="padding-top:5px;">
							(<span class="messages">0</span> {$lang.selected})
						</div>    		           	
    	                <a class="k-button k-button-icontext ToolBarActions" data-action="addMenuItem" data-type="BatchCat" data-subs="0" data-trigger="addMenu" href="\\#" ><span class="k-icon k-add"></span>Add Selected</a>
    	                <a class="k-button k-button-icontext ToolBarActions" data-action="addMenuItem" data-type="BatchCat" data-subs="1" data-trigger="addMenu" href="\\#" ><span class="k-icon k-add"></span>Add Selected sub categories</a>
		 </script>  
		


<style>
		.k-grid tbody .k-button {
			min-width: 44px;
		}
</style>

<div id="OpenLangWnd"></div>

<script>
head.js('/scripts/admin2/tree.js');
head.js('/scripts/admin2/grid.js');
head.js('/scripts/admin2/LanguagesBox.js');
head.js('/scripts/admin2/menu/menusEditor.js');
head.js('/scripts/admin2/menu/FilterEditor.js');
</script>

{/if}