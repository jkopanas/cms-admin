{foreach from=$menu item=a}
<li data-expanded="true" id="list_{$a.id}">
<a class="float-left">{$a.title}</a><a href="#" rel="{$a.id}" class="deleteMenuItem float-right"><img src="/images/admin/delete-icon.gif" width="9" height="11" /></a> <a href="#" rel="{$a.id}" class="editMenuItem float-right"><img src="/images/admin/cat_settings_16.png" width="11" height="11" /></a>
{if $a.subs}
 <ul>{include file="admin2/menu/recurseMenuItems.tpl" menu=$a.subs}</ul>
{/if}
</li>
{/foreach}

