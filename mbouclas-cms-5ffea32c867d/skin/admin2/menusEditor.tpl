<ul id="menu">
<li><a href="menus.php?action=add">{$lang.add}</a></li>
{foreach from=$menus item=a}
<li><a href="menus.php?id={$a.id}&action=modify">{$a.title}</a></li>
{/foreach}
</ul>
 {if $menu}<h1 class="spacer-top">{$menu.title} <small><a href="#" rel="{$menu.id}" class="editMenu">edit</a> - <a href="#" rel="{$menu.id}" class="deleteMenu">{$lang.delete}</a></small></h1>{/if}
<div class=" spacer-top clear debug"></div>

<div class="box_c spacer-top {if $menu}hidden{/if}" id="newMenu">
<h3 class="box_c_heading cf">
	<span class="fl spacer-left">{if $menu}{$lang.modify} {$menu.title}{else}{$lang.lbl_addNewMenu}{/if}</span>
</h3>
<form name="NewMenus" id="NewMenus" class="formEl_a toValidate" >
<div class="box_c_content cf">     
  		<div class="sepH_b">
        	<label class="lbl_a" for="title" >{$lang.title} *: </label>
        	<input name="title" id="title" type="text" class="inpt_a menu" value="{$menu.title}" required validationMessage=" {$lang.error_requiredField}"/>
        </div>
        <div class="sepH_b">
        	<label class="lbl_a" for="title" >{$lang.lbl_varName} *: </label>
        	<input name="var" id="var" type="text" class="inpt_a menu" value="{$menu.var}" required validationMessage=" {$lang.error_requiredField}" />
        </div>
        <div class="sepH_b">
 			<label class="lbl_a" for="title" >{$lang.availability} *: </label>
    		<select name="active" id="active" class="menu" required validationMessage=" {$lang.error_requiredField}" >
		        <option value="0" {if $menu.active eq "0"}selected{/if}>{$lang.disabled}</option>
        		<option value="1" {if $menu.active eq "1"}selected{/if}>{$lang.enabled}</option>
      		</select> 
  		</div> 
        <div class="sepH_b">
        	<label class="lbl_a" for="title" > {$lang.description}: ({$lang.optional}) </label>
        	<textarea name="description" rows="5" cols="60"  class=" txtar_a menu" id="desc">{$menu.description}</textarea><Br />
        	<a href="#" class="ckEditor" rel="desc" data-height="150">{$lang.advanced_editor}</a>
		</div>
        {include file="common/drawSettings.tpl" prefix="settings_" classes="toSave inpt_a" pageSettings=$pageSettings val=$item.settings}
<input type="hidden" name="id" value="{$menu.id}" class="menu" />
        <p><a href="#" class="btn btn_d savMenu">{$lang.save}</a></p>
</div>
</form>
</div>

{if $menu}
<textarea name="latestItems" class="hidden">{$latest}</textarea><textarea name="menuItems" class="hidden">{$menuItems}</textarea>




<div class="spacer-top"></div>
<div class="dp50">
<div class="box_c">
<div class="box_c_heading cf">
    <span class="fl">Modules</span>
    <ul class="tabsS fr">
    {foreach from=$loaded_modules item=a name=b}
    {if $a.active AND $a.is_item}	
        <li><a href="#" class="{if $smarty.foreach.b.first}current{/if}" rel="{$a.id}">{$a.name}</a></li>
    {/if}
    {/foreach}
    </ul>
</div>
<div class="box_c_content cf tabs_content">
    {foreach from=$loaded_modules item=a name=b}
	{if $a.active AND $a.is_item}	
    <div class="{if !$smarty.foreach.b.first}hidden{/if} tab" id="tab-{$a.id}" data-id="{$a.id}">
<div class="box_c_content cf mAccordion" id="acc-{$a.id}">
<div class="micro" id="micro-1">
											<h4 class="micro-open"><span class="head-inner">{$lang.category}</span></h4>
											<div class="sub_section micro-content">
        <div class="categoriesResults" id="results-{$a.id}">
        <ul style="height:250px; overflow-y:scroll">

        </ul>
                <div class="seperator"></div>
 <a href="#" class="btn btn_d addItems fr spacer-top" data-type="categories" data-typeAbbr="cat" data-id="{$a.id}"  data-module="{$a.name}">{$lang.add}</a>
 <div class="seperator"></div>
        </div>
											</div>
										</div>
										<div class="micro" id="micro-0">
											<h4 class="micro-closed"><span class="head-inner">{$lang.page}</span></h4>
											<div class="sub_section micro-content hidden">
<div class="formEl_a">
    <div class="sepH_b">
        <label for="title" class="lbl_a">{$lang.filter}</label>
     <input type="hidden" name="search" />   <input type="text" id="filter-{$a.id}" name="substring" class="inpt_b filter SearchData" data-module="{$a.name}" data-field="q"  /> 
     <select class="SearchData inpt_b" id="catid" name="categoryid" data-module="{$a.name}" data-field="categoryid">
<option value="">Όλες οι κατηγορίες</option>    
{foreach from=$categories[$a.id] item=p}
<option value="{$p.categoryid}">{$p.category}</option>
{/foreach}
</select> <a href="#" class="btn btn_d quickFilter" data-type="items"  data-typeAbbr="item" data-id="{$a.id}" data-module="{$a.name}">{$lang.search}</a>

<span class="f_help">search for title, id</span>
        <div class="itemsResults" id="results-{$a.id}">
        <h3 spacer-top spacer-bottom>{$lang.results}</h3>
       
        <ul style="height:250px; overflow-y:scroll; clear:both;">

        </ul>
        <div class="seperator"></div>
 <a href="#" class="btn btn_d addItems fr spacer-top" data-type="items" data-typeAbbr="item" data-id="{$a.id}" data-module="{$a.name}">{$lang.add}</a>
 <div class="seperator"></div>
        </div>
    </div>
</div>

											</div>
										</div>
										
										<div class="micro" id="micro-2">
											<h4 class="micro-closed"><span class="head-inner">{$lang.link}</span></h4>
											<div class="sub_section micro-content hidden">
<form method="post" name="{$formID|default:'saveMenuItem'}Form" id="{$formID|default:'saveMenuItem'}Form-{$a.name}" class="toValidate">
<div id="menuItemDetails" class="formEl_a">
		<p class="sepH_b"><label class="lbl_a">{$lang.availability}: <br />
        
        <select name="active" id="active" class="customSave inpt_a"  data-type="custom" data-menuid="{$menu.id}">
        <option value="0">{$lang.disabled}</option>
        <option value="1">{$lang.enabled}</option>
      </select> </label>
      </p>
        <p class="sepH_b"><label class="lbl_a">{$lang.title} *:<br />
        <input name="title" id="title" type="text" value="" class="customSave inpt_a" data-type="custom" data-menuid="{$menu.id}" required/>
        </label>
        </p>
        <p class="sepH_b"><label class="lbl_a">{$lang.url} *:<br />
        <input name="link" id="link" type="text" value="" class="customSave inpt_a" data-type="custom" data-menuid="{$menu.id}" required/></label>
        </p>
        <p class="sepH_b"><label class="lbl_a">{$lang.lbl_seoTitle}: <br />
          <input name="settings_title" type="text" class="customSave inpt_a" data-type="custom" data-menuid="{$menu.id}"/></label>
        </p>

        <p class="sepH_b"><a href="#" class="btn btn_d saveCustomItem fr spacer-top" data-module="{$a.name}" data-menuid="{$menu.id}" data-type="custom">{$lang.add}</a>
        </p>
</div><!-- END MENU ITEM DETAILS -->
</form>
											</div>
										</div>
									</div>



    </div><!-- END TAB -->
    {/if}
    {/foreach}
</div>
</div>
</div><!-- END LEFT -->
<div class="dp50">
<ol class="sortable" id="menuItems"> 
{* 
{if $menu.items}
{include file="admin2/recurseMenuItems.tpl" menu=$menu.items} 
{/if}
*}
  </ol> 
   <a href="#" class="btn btn_d saveItemSettings fr spacer-top" data-module="{$a.name}" data-menuid="{$menu.id}">{$lang.save}</a>
</div><!-- END RIGHT -->

{/if}
<style>
		.categoriesResults ul ol{
			margin: 0 0 0 25px;
			padding: 0;
			list-style-type: none;
		}
		ol.sortable, ol.sortable ol {
			margin: 0 0 0 25px;
			padding: 0;
			list-style-type: none;
		}
 
		ol.sortable {
			/* margin: 4em 0; */
		}
 
		.sortable li {
			margin: 7px 0 0 0;
			padding: 0;
		}
 
		.sortable li div.box_c_heading  {
			padding: 3px;
			margin: 0;
			cursor: move;
		}
		.placeholder {
			background-color: #cfcfcf;
			border:2px dashed #999;
		}
 
		.ui-nestedSortable-error {
			background:#fbe3e4;
			color:#8a1f11;
		}
</style>

<script id="itemsResults" type="text/kendo-ui-template">

	# for (i in data) { # 
	<li><label><input type="checkbox" name="item-#= data[i]['id'] #" id="item-#= data[i]['id'] #" value="#= data[i]['id'] #" data-type="item"  data-id="#= data[i]['id'] #" data-menuid="{$menu.id}" /> <strong>\\##= data[i]['id'] #</strong> #= data[i]['title'] #</label></li>
	# } #
</script>
<script id="categoriesResults" type="text/kendo-ui-template">
	<li data-level="#= level #" data-id="#= categoryid #" data-menuid="{$menu.id}" data-parentid="#= parentid #"><label><input type="checkbox" name="category-#= categoryid #" id="cat-#= categoryid #" value="#= categoryid #" data-type="category" data-id="#= categoryid #" data-menuid="{$menu.id}" data-level="#= level #" /> <strong>\\##= categoryid #</strong> #= category #</label>
	
			# if (typeof data.subs == 'undefined') { #		
		</li>
		# } #
</script>

<script id="menuTemplate" type="text/kendo-ui-template">
		<li id="list_#= id #" data-level="#= level #" data-id="#= id #">
		<div class="listContainer box_c">
		<div class="box_c_heading">
		<span class="fl"># if (itemid != null && itemid != 0) { #\\# #= itemid # #}# #= title #</span>
		<img src="/images/fancybox/blank.gif" alt="" class="fr square_x_11 wRemove" data-id="#= id #">
		<img src="/images/fancybox/blank.gif" alt="" class="fr switch_arrows_a wToogle">
		<span class="fr spacer-right">#= type #</span>
		</div>
		<div class="box_c_content cf mAccordion hidden">
		<div class="menu-item-settings" id="menu-item-settings-173" style="display: block; ">
		<div class="formEl_a">
    
								<p class="sepH_b">
					<label for="title-#= id #" class="lbl_a">
						{$lang.title}<br>
						<input type="text" id="title-#= id #" class="saveData inpt_a" name="title-#= id #" value="#= title #" data-field="title" data-value="#= id #" data-id="#= id #">
					</label>
				</p>
			
				<p class="sepH_b">
					<label class="lbl_a">
						{$lang.lbl_seoTitle}<br>
						<input type="text" id="settings_title-#= id #" class="saveData inpt_a" name="settings_title-#= id #" data-field="settings_title" data-value="#if (settings != null && typeof settings.title != 'undefined') { # #= settings.title # #}#" value="#if (settings != null && typeof settings.title != 'undefined') { # #= settings.title # #}#" data-id="#= id #" />
					</label>
				</p>
					<p class="sepH_b">
					<label class="lbl_a">
						{$lang.url}<br>
						<input type="text" id="link-#= id #" class="saveData inpt_a" name="link-#= id #" value="#= link #" data-field="link" data-value="#= link #" data-id="#= id #" />
					</label>
				</p>
									<p class="sepH_b">
					<label class="lbl_a">
						{$lang.permalink}<br>
						<input type="text" id="permalink-#= id #" class="saveData inpt_a" name="permalink-#= id #" value="#= permalink #" data-field="permalink" data-value="#= permalink #" data-id="#= id #" />
					</label>
				</p>
				
				
				
			<p class="sepH_b">
			<label class="lbl_a"><input type="checkbox" class="saveData" name="settings_syncWithOriginal-#= id #" value="1" data-field="settings_syncWithOriginal"  # if (settings != null && typeof settings.syncWithOriginal != 'undefined' && settings.syncWithOriginal == 1) { # checked #}# data-id="#= id #" /> 
            {$lang.lbl_syncWithOriginalItem}</label>
			</p>
			
			</div>
			</div>
		
		
		</div>
		</div> 
		# if (typeof data.subs == 'undefined') { #		
		</li>
		# } #
</script>

<script>
head.js('/scripts/jquery-ui-1.8.18.custom.min.js');

head.js('/scripts/admin2/nestedSortable.js');
head.js('/scripts/admin2/menusEditor.js');
</script>
