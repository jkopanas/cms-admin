<div id="window"></div>
<script id="versionsFoundTemplate" type="text/kendo-ui-template">
	<h3 class="float-left spacer-right">#= data.length # new versions found</h3><div class="float-left spacer-left"><a href="\\#" class="updateStart btn btn_d" data-bind="click: doActions" data-method="startUpdating">Start updating</a></div>
	<div class="clear"></div>
	<ul>
		# $.each(data,function(k,v){ #
			<li><a href="\\#" data-id="#= k #" data-bind="click: doActions" data-method="showChangeLog">#= v.version #</a></li>
		# }); #
	</ul>
	<div class="seperator"></div>
</script>

<h2>MCMCS updates</h2>
<h4>Current version {$VERSION_NUMBER}</h4>
<div id="updater" class="">
<div class="clear"></div>

<input type="hidden" name="currentVersion" value="{$VERSION_NUMBER}" class="origData" />
<div class="seperator"></div>
<div id="versionList"></div>
<div class="debug"></div>

<div class="seperator"></div>

<div id="progressBars" class="hidden">
<h4>Total progress</h4>
<div>
<div class="progressBar" id="totalComplete">
<img width="120"  title=" 100%" alt=" 0%" src="/scripts/progressBar/images/progressbar.gif" /> <span class="percentage">0</span>%
<div class="report"></div>
<div class="float-right hidden">
<h3>Change log</h3>
<div class="changeLog"></div>
</div>
<div class="clear">
</div>

</div>
<div class="seperator"></div>
<h4>Current progress</h4>
<div class="progressBar" id="currentComplete">
<img width="120"  title=" 100%" alt=" 0%" src="/scripts/progressBar/images/progressbar.gif" /> <span class="percentage">0</span>%
<div class="report"></div>
</div>
</div>

</div>
</div><!-- END CONTAINER -->
<div id="updaterComplete" class="hidden">
<div class="seperator">
MCMS is up to date.
</div>
</div><!-- END ALL DONE -->
<div id="rollBackComplete" class="hidden">
<div class="seperator">
Something went wrong and we had to rollback your system to the previous version. Hope it still works.
</div>
</div><!-- END RollBack -->
<style>
.progressBar img { background-position: 100% 100%; width: 120px; height: 12px;  padding: 0px; margin: 0px; background-image: url('/scripts/progressBar/images/progressbg_red.gif');}
</style>
<script>

head.js('/scripts/updater/updater.js');
</script>