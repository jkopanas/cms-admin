<textarea name="catItems" class="hidden">{$catItems}</textarea><textarea name="themesEnc" class="hidden">{$themesEnc}</textarea><textarea name="pageSettings" class="hidden">{$pageSettingsEnc}</textarea><textarea name="arr" class="hidden">{$arr}</textarea>



<div id="modal" class="hidden cf brdrrad_a">




</div>
<div>
<h1>{$lang.categories}</h1>

<div class="seperator"></div>

<div class="dp100">
<div class="box_c">
<h3 class="box_c_heading cf">
	<span class="fl spacer-left">{$lang.categories}</span>
    <img class="fr switch_arrows_a toggleAll" alt="" src="/images/fancybox/blank.gif">
            <ul class="tabsS fr spacer-right">
            <li><a href="#/modify" class="current" rel="modify">{$lang.modify}</a></li>
            <li><a href="#/featured" rel="featured">Featured</a></li>
        </ul>
    
</h3>
</div>



<div class=" spacer-top spacer-bottom clear debug"></div>
<div class="tabs_content">
<div class="tab" id="modify">
<div class="dp25">


<form name="newCategory" id="newCategory" class="formEl_a toValidate" >
<div class="box_c_heading ">
    <span class="fl">{$lang.add}</span>
</div>

<div class="formEl_a box_c_content">
    
								<p class="sepH_b">
					<label class="lbl_a" for="category">
						{$lang.title}<br>
						<input type="text" name="category" class="saveData inpt_a" id="category" required>
					</label>
				</p>
								<p class="sepH_b">
					<label class="lbl_a" for="alias">
						{$lang.permalink}<br>
						<input type="text"  name="alias" class="saveData inpt_a" id="alias">
					</label>
				</p>
                <p class="sepH_b">
                    				<label for="" class="lbl_a">{$lang.belongsToCategory} :</label>
   				 <select name="parent_catid" id="parent_catid" data-field="parentid" class="inpt_a saveData">		
				<option value="0" data-parentid="0" selected="selected">{$lang.main_cat}</option>
				</select>
                </p>
								<p class="sepH_b">
					<label class="lbl_a" for="description">
						{$lang.description}<br>
						<textarea name="description" class="saveData inpt_a"></textarea>
					</label>
				</p>
                <p class="sepH_b">
                <label class="lbl_a" for="theme">{$lang.theme}<br />
                 <select name="settings_theme" class="inpt_a saveData"> 
					{foreach from=$themes item=a name=b}	
					<option value="{$a.name}">{$a.title}</option>
				{/foreach}
				</select></label>
                </p>

						 {include file="common/drawSettings.tpl" prefix="settings_"  classes="inpt_a saveData" pageSettings=$pageSettings val=$cat_details.settings}
                 <p><a href="#" class="btn btn_d saveCat">{$lang.save}</a> <a href="#" class="btn btn_d saveCatEdit">{$lang.save} & {$lang.edit}</a></p>
                
</div>


</form>

</div>
<div class="dp75">
<ol class="sortable" id="catItems">
  

</ol>
</div><!-- END RIGHT -->
<div class="clear"></div>
</div><!-- END TAB -->
<div class="tab hidden" id="featured">
<div id="featuredTab">

</div>
</div>
</div><!-- END TABS -->

</div>
</div>

{include file="common/categories/templates.tpl"}

<style>

#modal-back { position: fixed; top: 0; left: 0; width: 100%;  height: 100%;   background: #000;   opacity: 0.5;   filter: alpha(opacity=50);}
#modal { top:50%; left:50%;position:absolute;z-index:9999; width:75%;  }
#modal .inner {  background:#f1f1f1; border:1px solid #eaeaea; padding:10px; height:450px; width:100%; overflow-y:scroll; }
#modal .left { background:url('/skin/images/admin2/modal-arrow-left.png'); width:66px; height:131px; position:absolute; top:150px; left:-54px; text-indent:-5000px }
#modal .right { background:url('/skin/images/admin2/modal-arrow-right.png'); width:66px; height:131px; position:absolute; top:150px; right:-102px; text-indent:-5000px }
#modal .close { position:absolute;  background:url('/skin/images/admin2/close.png'); width:36px; height:36px; top:-20px; right:-55px; text-indent:-5000px; }
.catItem { border:1px solid #DFDFDF; padding:10px; line-height:130%; color:#555; background-color:#f3f3f3; min-height:31px;   }
.row-actions { margin-left:25px; }
		.categoriesResults ul ol{
			margin: 0 0 0 25px;
			padding: 0;
			list-style-type: none;
		}
		 ol.sortable ol {
			margin: 0 0 0 25px;
			padding: 0;
			list-style-type: none;
		}
 
		ol.sortable {
			/* margin: 4em 0; */
		}
 
		.sortable li {
			margin: 7px 0 0 0;
			padding: 0;
		}
 
		.sortable li div.box_c_heading  {
			padding: 3px;
			margin: 0;
			cursor: move;
		}
		.placeholder {
			background-color: #cfcfcf;
			border:2px dashed #999;
		}
 
		.ui-nestedSortable-error {
			background:#fbe3e4;
			color:#8a1f11;
		}
		.level-1 { background-color:#caedff; }
		.level-2 { background-color:#caffd3; }
		.level-3 { background-color:#fafdb3; }
		.level-4 { background-color:#fbffca; }
		.featuredItemcat { background-color:#f6d8fa; }
		.featuredItemitm { background-color:#c2ffcc; }
		.inUse { color:red; text-decoration:line-through; }
</style>

<script>
head.js('/scripts/jquery-ui-1.8.18.custom.min.js');
head.js('/scripts/admin2/nestedSortable.js');
head.js('/scripts/admin2/itemsCategories.js');
</script>
