<div class=" spacer-top spacer-bottom clear debug"></div>
<form name="newCategory" id="newCategory" class="formEl_a toValidate" >
<div class="formEl_a spacer-top">
    
								<p class="sepH_b">
					<label class="lbl_a" for="category">
						{$lang.title}<br>
					  <input type="text" name="category" class="saveData inpt_a" id="category" value="#= cat.category #" data-field="category" required>
					</label>
				</p>
	<p class="sepH_b">
					<label class="lbl_a" for="alias">
						Alias<br>
					  <input type="text"  name="alias" class="saveData inpt_a" id="alias" value="#= cat.alias #" data-field="alias" />
					</label>
				</p>
								<p class="sepH_b">
					<label class="lbl_a" for="description">
						{$lang.description}<br>
						<textarea name="description" class="saveData inpt_a" data-field="description"> #= cat.description # </textarea>
					</label>
                    
	</p>
                <p class="sepH_b">
                <label class="lbl_a" for="theme">Theme<br />
                 <select name="settings_theme" class="inpt_a saveData" data-field="settings_theme"> 
                	#var k=0; for (p = themes.length; p--;){ #
	                   	<option value="#= themes[k]['name'] #" # if (themes[k]['name'] === cat.settings.theme) { # selected # } # (>#= themes[k]['title'] #</option>
                    # k++; } #
				</select></label>
                </p>
                
                # var u = pageSettings.length; for (i=0;u>i;i++) { #
                 <p class="sepH_b">
					<label class="lbl_a" for="#= formFields.settings.prefix##= pageSettings[i]['varName'] #">
						#= pageSettings[i]['title'] #<br>
                       
                        # var tmp = formFields.renderField(pageSettings[i],data,data.cat.settings[pageSettings[i]['varName']]); #
					  	#= tmp #
					</label>
				</p>
                # } #

						 {* {include file="common/drawSettings.tpl" prefix="settings_"  classes="inpt_a saveData" pageSettings=$pageSettings val=$cat_details.settings} *}
                 <p><a href="\\#" class="btn btn_d saveCat" rel="#= cat.categoryid #">{$lang.save}</a> <a href="\\#" class="btn btn_d saveCatExit" rel="#= cat.categoryid #" data-close="1">{$lang.save} & close</a></p>
                
</div>
</form>