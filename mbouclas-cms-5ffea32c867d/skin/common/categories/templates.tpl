<div class="templates">
<script id="catItemTemplate" type="text/kendo-ui-template">
		<li id="list_#= categoryid #" data-level="#= level #" data-id="#= categoryid #" data-categoryid_path="#= categoryid_path #" data-parentid="#= parentid #">
<div class="catItem level-#= level #">

<img width="16" height="16" src="/images/admin/move-arrow.png" class="spacer-right fl dragHandle"> <span class="fl spacer-right"><strong>\\##= categoryid #</strong>  <span class="catName">#= category #</span></span> <span class="fl spacer-left"><em>(<strong class="numSub">#= num_sub #</strong> {$lang.subcategories})</em></span>
<a href="\\#" rel="#= categoryid #" class="deleteCat"><img data-id="#= categoryid #" class="fr wRemove" alt="" src="/skin/images/admin2/delete.png" height="11" style="margin-left:5px;"></a>

<a href="\\#" # if (num_sub == 0)  { # class="hidden" # } #><img class="fr toggleChildren" alt="" src="/skin/images/admin2/toggle.png" height="11"></a>


<div class="clear"></div>
<div class="row-actions hidden"><span class="edit"><a href="\\#!/modifyCategory/categoryid/#= categoryid #" rel="#= categoryid #" class="modifyCategory">{$lang.modify}</a> | </span><span class="inline hide-if-no-js"><a  data-module="#= mcms.mcmsSettings.module #" data-id="#= categoryid #" class="wToogle" href="\\#">{$lang.quick_edit}</a> | </span> <span class="edit"><a href="\\#!/featured/categoryid/#= categoryid #" rel="#= categoryid #" class="modifyCategory">Featured</a> |</span> <span class="edit"><a href="\\#!/translations/categoryid/#= categoryid #" rel="#= categoryid #" class="modifyCategory">{$lang.translations}</a></span></div>

<div class="formEl_a spacer-top hidden">
    
								<p class="sepH_b">
					<label class="lbl_a" for="category-#= categoryid #">
						{$lang.title}<br>
						<input type="text" data-id="#= categoryid #" data-value="#= category #" data-field="category" value="#= category #" name="category-#= categoryid #" class="saveData inpt_a category" id="category-#= categoryid #">
					</label>
				</p>
								<p class="sepH_b">
					<label class="lbl_a" for="alias-#= categoryid #">
						{$lang.permalink}<br>
						<input type="text" data-id="#= categoryid #" data-value="#= alias #" data-field="alias" value="#= alias #" name="alias-#= categoryid #" class="saveData inpt_a alias" id="alias-#= categoryid #">
					</label>
				</p>
								<p class="sepH_b">
					<label class="lbl_a" for="title-#= categoryid #">
						{$lang.description}<br>
						<textarea name="description-#= categoryid #" id="description-#= categoryid #" class="saveData inpt_a" data-id="#= categoryid #" data-value='#= description #' data-field="description">#= description #</textarea>

					</label>
				</p>
                               <p class="sepH_b">
                <label class="lbl_a" for="theme">{$lang.theme}<br />
		
                 <select name="settings_theme-#= categoryid #" data-field="settings_theme" class="inpt_a saveData"> 
				
					# for (k in themes)  { #
					<option value="#= themes[k]['name'] #" # if (settings_array.theme == themes[k]['name']) { # selected # } #>#= themes[k]['title'] #</option>
					# } #

				</select>
				</label>
                </p> 
				<p><a href="\\#" rel="#= categoryid #" class="btn btn_d saveSignleCat">{$lang.save}</a></p>
</div>
</div>

		# if (typeof data.subs == 'undefined') { #		
		</li>
		# } #
</script>

<script id="catSelectTemplate" type="text/kendo-ui-template">

<option value="#= categoryid #" data-level="#= level #" data-parentid="#= parentid #" data-categoryid_path="#= categoryid_path #" style="margin-left:#= 5*parseInt(level) #px"># for (j=0;j < parseInt(level);j++) { # -- # } #  #= category #</option>
</script>

<script id="fullEditTemplate" type="text/kendo-ui-template">
<div class="inner dp75">
<div class="box_c">
									<div class="box_c_heading cf">
										<span class="fl">\\##= cat.categoryid # #= cat.category #</span>
										<ul class="tabsS fr">
											<li><a href="\\#" class="current" rel="modifyCategory">{$lang.modify}</a></li>
											<li><a href="\\#" rel="featured">Featured</a></li>
											<li><a href="\\#" rel="translations">{$lang.translations}</a></li>
										</ul>
									</div>
									<div class="box_c_content cf tabs_content">
									<div class="debug spacer-top spacer-bottom"></div>
							
										<div class="tab"  id="modifyCategory">
										 
										{include file="common/categories/modifyTemplate.tpl"}
										<div class="clear"></div>
										</div>
										<div class="tab hidden" id="featured">
					
										</div>
										<div class="tab hidden" id="translations">
					
										</div>
									</div>
								</div>
</div>
</script>
<script id="translations" type="text/kendo-ui-template">
# var l = availableLanguages.length; for (i=0;l>i;i++) { #
# var tmp = {}; #
# if (availableLanguages[i].item !== null) { tmp = availableLanguages[i].item; }  #
<h2>#= availableLanguages[i].country #</h2>
<div class="formEl_a spacer-top">
    
								<p class="sepH_b">
					<label class="lbl_a" for="category-#= availableLanguages[i].code #">
						{$lang.title}<br>
					  <input type="text" name="category-#= availableLanguages[i].code #" class="saveData inpt_a" id="category-#= availableLanguages[i].code #" data-code="#= availableLanguages[i].code#" value="# if (typeof tmp.category !== 'undefined') { ##= tmp.category.translation || '' ## } #" data-field="category" data-table="#= mcms.settings.module #_categories" />
					</label>
				</p>
	<p class="sepH_b">
					<label class="lbl_a" for="alias-#= availableLanguages[i].code #">
						Alias<br>
					  <input type="text"  name="alias-#= availableLanguages[i].code #" class="saveData inpt_a" id="alias-#= availableLanguages[i].code #" data-code="#= availableLanguages[i].code#" value="# if (typeof tmp.alias !== 'undefined') { ##= tmp.alias.translation || '' ## } #" data-field="alias" data-table="#= mcms.settings.module #_categories" />
					</label>
				</p>
								<p class="sepH_b">
					<label class="lbl_a" for="description-#= availableLanguages[i].code #">
						{$lang.description}<br>
						<textarea name="description-#= availableLanguages[i].code #" class="saveData inpt_a" data-field="description" data-code="#= availableLanguages[i].code#" id="description-#= availableLanguages[i].code #" data-table="#= mcms.settings.module #_categories"># if (typeof tmp.description !== 'undefined') { ##= tmp.description.translation || ''## } #</textarea>
					</label>
                    
	</p>
	 
</div>	

# } #
<p><a href="\\#" class="btn btn_d saveTranslations" rel="#= cat.categoryid #" data-table="#= mcms.settings.module #_categories">{$lang.save}</a></p>
</script>
</div><!-- END TEMPLATES -->