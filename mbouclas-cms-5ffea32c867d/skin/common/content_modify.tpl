<div id="uploadWindow"></div>
<input type="hidden" name="itemID" value="{$item.id}" /> 
<h1 id="itemTitle">#<span class="itemid">{$item.id}</span> : <span class="title">{$item.title}</span></h1>
<div class="seperator"></div>
<ul id="menu">
<li><a href="items.php?module={$current_module.name}">{$lang.add}</a></li>
{foreach from=$layout.boxes.L item=a name=b}
    <li {if !$item AND !$a.settings.required}class="hidden"{/if}><a href="#!/{$a.name}" class="navigator" data-boxID="{$a.id}"  data-dataType="{$a.settings.dataType}"  data-name="{$a.name}">{$lang[$a.name]|default:$a.title}</a></li>
{/foreach}
 <li {if !$item}class="hidden"{/if}>{$lang.more}
 <ul><li>
                    <div id="template" class="k-content padding" style="padding: 10px;">
                    <textarea id="moreItemsJson" class="hidden">{$more_items}</textarea>
                                <h2>More like this</h2>
                                <div class="seperator"></div>
                                <ol>
    
                                </ol>
   
                            </div>
                            </li></ul>
 </li>
</ul>

<div class="seperator"></div>

<div class="debug"></div>
<div id="mainItem">


</div><!-- END LEFT -->

<div class="seperator"></div>

<link href="/scripts/multiSelect/chosen.css" rel="stylesheet"/>
<script>
head.js('/scripts/multiSelect/chosen.jquery.min.js');
head.js('/scripts/jquery-ui-1.8.18.custom.min.js');
head.js('/scripts/admin2/items/itemsGeneral.js');
</script>
