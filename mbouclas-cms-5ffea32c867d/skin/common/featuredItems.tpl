<div id="templates">
<script id="body" type="text/kendo-ui-template" data-type="text">
<div class="dp50">
#var t = templates['searchItems'](data); #
#= t #
</div><!-- END LEFT -->
<div class="dp50" id="featuredItemsTab">
#var t = templates['orderItems'](data); #
#= t #
</div><!-- END RIGHT -->
</script><!-- END TEMPLATE -->


<script id="searchItems" type="text/kendo-ui-template" data-type="text">
<div class="cf mAccordion" id="acc-1">
<div class="micro" id="micro-1">
<h4 class="micro-open"><span class="head-inner">#= $.lang.page #</span></h4>
<div class="sub_section micro-content">
<label for="title" class="lbl_a">#= $.lang.filter #</label>
<input type="hidden" name="search" />   <input type="text" id="filter" name="substring" class="inpt_b filter SearchData" data-module="#= mcms.settings.module #" data-field="q"  /> 
<select class="SearchData inpt_b" id="catid" name="categoryid" data-module="#= mcms.settings.module #" data-field="categoryid">
<option value="">#= $.lang.all #</option>    
# t= renderRecursive(allCategories,0,{template:templates['catSelectTemplate'],encStart:'',encEnd:''}); #
#= t #
</select> <a href="\\#" class="btn btn_d quickFilter" data-type="items"  data-typeAbbr="item" data-module="#= mcms.settings.module #">#= $.lang.search #</a>
<span class="f_help">search for title, id</span>
<ul style="height:250px; overflow-y:scroll; clear:both;" class="itemsResults">
# t = renderPages(data); #
#= t #
</ul>
<a href="\\#" id="#= new Date().getTime() #" class="btn btn_d addItems fr spacer-top" data-type="items" data-typeAbbr="itm" data-categoryid="#= settings.currentCategory #"  data-module="#= mcms.settings.module #">#= $.lang.add #</a>
<div class="seperator"></div>
</div><!-- END CONTENT -->
</div><!-- END SECTION -->

<div class="micro" id="micro-2">
<h4 class="micro-open"><span class="head-inner">#= $.lang.category #</span></h4>
<div class="sub_section micro-content hidden">
        <div class="categoriesResults" id="results-{$a.id}">
        <ul style="height:250px; overflow-y:scroll">
		# t= renderRecursive(allCategories,0,{template:templates['categoriesResults'],encStart:'<ol>',encEnd:'</ol>',check:itemsFlat.cat}); #
		#= t #
        </ul>
                <div class="seperator"></div>
 <a href="\\#" id="#= new Date().getTime() #" class="btn btn_d addItems fr spacer-top" data-type="categories" data-typeAbbr="cat" data-categoryid="#= settings.currentCategory #" data-module="#= mcms.settings.module #">#= $.lang.add #</a>
 <div class="seperator"></div>
        </div>
</div><!-- END CONTENT -->
</div><!-- END SECTION -->



</div><!-- END ACCORDION -->

 
</script><!-- END TEMPLATE -->


<script id="orderItems" type="text/kendo-ui-template" data-type="text">
<ol class="sortable" id="featuredItems">
# t= renderItemsList(items); #
#= t #
</ol>
</script>
<script id="pages" type="text/kendo-ui-template">
 	# if (typeof data.allPages != 'undefined') { #
	# var a = data.allPages.length; for (j=0;a > j;j++) { # 
	<li# if (typeof data.itemsFlat.itm !== 'undefined' && data.itemsFlat.itm.indexOf(data.allPages[j]['id']) > -1) { # class="inUse" #}#><label><input type="checkbox" name="item-#= data.allPages[j]['id'] #" id="itm-#= data.allPages[j]['id'] #" data-el="itm-#= data.allPages[j]['id'] #" value="#= data.allPages[j]['id'] #" data-type="itm"  data-id="#= data.allPages[j]['id'] #" # if (typeof data.itemsFlat.itm !== 'undefined' && data.itemsFlat.itm.indexOf(data.allPages[j]['id']) > -1) { # class="hidden" #}# /> <strong>\\##= data.allPages[j]['id'] #</strong> <span>#= data.allPages[j]['title'] #</span></label></li>
	# } #
	 # } #
</script>
<script id="catSelectTemplate" type="text/kendo-ui-template">
<option value="#= d.categoryid #" data-level="#= d.level #" data-parentid="#= d.parentid #" data-categoryid_path="#= d.categoryid_path #" style="margin-left:#= 5*parseInt(d.level) #px"># for (j=0;j < parseInt(d.level);j++) { # -- # } #  #= d.category #</option>
</script>


<script id="categoriesResults" type="text/kendo-ui-template">

	<li data-level="#= d.level #" data-id="#= d.categoryid #"  data-parentid="#= d.parentid #" # if (typeof t.check !== 'undefined' && t.check.indexOf(d.categoryid) > -1) { # class="inUse" # } #><label><input type="checkbox" name="category-#= d.categoryid #" id="cat-#= d.categoryid #" value="#= d.categoryid #" data-type="cat" data-id="#= d.categoryid #" data-level="#= d.level #" # if (typeof t.check !== 'undefined' && t.check.indexOf(d.categoryid) > -1) { # class="hidden" # } # data-el="cat-#= d.categoryid #" /> <strong>\\##= d.categoryid #</strong> <span>#= d.category #</span></label>
	
			# if (typeof data.subs == 'undefined') { #		
		</li>
		# } #
</script>

<script id="items" type="text/kendo-ui-template">
# var p = data.length; for (i=0;p > i;i++) { #

		<li id="#=data[i].type#-#= data[i].itemid #">
		
<div class="catItem featuredItem#=data[i].type#" data-id="#= data[i].itemid #" data-type="#= data[i].type #">
<img width="16" height="16" src="/images/admin/move-arrow.png" class="spacer-right fl dragHandle"> <span class="fl spacer-right"><strong>\\##= data[i].itemid #</strong>  <span class="catName">#= data[i].item.title || data[i].item.category #</span></span>
<a href="\\#" rel="#= data[i].itemid #" data-type="#= data[i]['type'] #"><img data-id="#= data[i].itemid #" class="fr wRemove" alt="" src="/skin/images/admin2/delete.png" height="11" style="margin-left:5px;"></a>
<span class="fr"><em># if (data[i].type === 'itm') { # #= $.lang.item # # } else { # #= $.lang.category # # } #</em></span>
		</div>
		</li>
# }  #
</script>
</div><!-- END TEMPLATES -->
