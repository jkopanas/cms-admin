<div>
<script id="inputTemplate" type="text/kendo-ui-template" data-type="text">
<input type="text" name="#= settings.prefix ##= field.varName #"  class="#= settings.classes #" id="#= settings.prefix ##= field.varName #" value="#= val || '' #" #= settings.extra #  data-field="#= settings.prefix ##= field.varName #"/> 
</script>

<script id="selectTemplate" type="text/kendo-ui-template"  data-type="select">
<select name="#= settings.prefix ##= field.varName #" class="#= settings.classes#" id="#= settings.prefix ##= field.varName #" #= settings.extra # data-field="#= settings.prefix ##= field.varName #">
# var w = field['options'].values.length;   for (j=0;w > j;j++) {  #
<option value="#= field.options.values[j].value #" # if (field.options.values[j].value == val) {#selected#}#>#= $.lang[field.options.values[j].translation]#</option>
# } #
</select>
</script>
<script id="textareaTemplate" type="text/kendo-ui-template"  data-type="textarea">
<textarea name="#= settings.prefix ##= field.varName #"  class="#= settings.classes #" id="#= settings.prefix ##= field.varName #" #= settings.extra # data-field="#= settings.prefix ##= field.varName #">#= val || '' #</textarea> 
</script>
<script id="radioTemplate" type="text/kendo-ui-template"  data-type="radio">
# var w = field['options'].values.length;   for (j=0;w > j;j++) {  #
#= $.lang[field.options.values[j].translation]# : <input type="radio" name="#= settings.prefix ##= field.varName #"  class="#= settings.classes #" id="#= settings.prefix ##= field.varName #" value="#= field.options.values[j].value #" #= settings.extra # # if (field.options.values[j].value == val) {#checked#}# data-field="#= settings.prefix ##= field.varName #" /> 
# } #
</script>
</div>