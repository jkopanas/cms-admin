<select name="{$prefix}{$field.var_name|default:$field.varName}" class="{$classes}" id="{$prefix}{$field.var_name|default:$field.varName}" {$extra}>
{foreach from=$field.options.values item=x}
<option value="{$x.value}" {if $x.value eq $val}selected{/if}>{$lang[$x.translation]}</option>
{/foreach}
</select>