{capture name="sideBar"}
<ul {if !$item}class="k-state-disabled"{/if}>
<li class="k-state-active">
<span class="k-link k-state-selected">In this section</span>
                    <ul>
                        <li>Monday</li>
                        <li>Tuesday</li>
                        <li>Wednesday</li>
                        <li>Thursday</li>
                        <li>Friday</li>
                    </ul>
</li><!-- END PANEL -->
 <li>
More in this category
                    <ul>
                        <li>New Business Plan</li>
                        <li>
                            Sales Forecasts
                            <ul>
                                <li>Q1 Forecast</li>
                                <li>Q2 Forecast</li>
                                <li>Q3 Forecast</li>
                                <li>Q4 Forecast</li>
                            </ul>
                        </li>
                        <li>Sales Reports</li>
                    </ul>
                </li><!-- END PANEL -->
</ul>
{/capture}{include file="admin2/sidebar.tpl" content=$smarty.capture.sideBar}

