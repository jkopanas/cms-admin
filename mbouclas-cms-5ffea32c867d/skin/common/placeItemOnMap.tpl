<input type="hidden" name="mapItem" value='{$mapItem}' />
<input type="hidden" name="loaded_language" value="{$BACK_LANG}" />
<div>
 <form name="addressForm" id="addressForm" method="post">
<label>{$lang.lbl_address} : <input name="address" type="text" value="{$item.geocoderAddress}"  class="ContentTitle" size="45" /> </label> <input type="submit" value="Look up" name="searchAddress" class="btn btn_d button" />
</form>
<div class="clearfix example spacer-top"> 
      <div class="sx"> 
        <div  id="mapItem" class="mapbox" style="width:100%; height:350px;"></div> 
      </div> 
  <div class="dx"> 
        <h2>{$lang.searchResults}</h2>
        <ul id="geocodingResults" class="list"> 
        
        </ul>
  </div>
</div>
<div id="geocodingDataNow">
<p class="currentPosition">{$lang.lbl_currentPosition} : <strong><span></span></strong></p>
<p class="address">{$lang.lbl_closestAddress} : <strong><span></span></strong></p>
</div>
<form method="post">
<input type="hidden" name="geocoderAddress" value="{$item.geocoderAddress}" class="get" />
<input type="hidden" name="lat" class="get" value="{$item.lat}" />
<input type="hidden" name="lng" class="get" value="{$item.lng}" />
<input type="hidden" name="zoomLevel" class="get" value="{$item.zoomLevel}" />
<input type="hidden" name="MapTypeId" class="get" value="{$item.MapTypeId}" />
<input type="hidden" name="itemid" value="{$id}" class="get" />
<input type="hidden" name="module" value="{$current_module.name}" class="get" />
<button class="btn btn_d {if !$item}hidden{/if}" id="savePlaceOnMap" >{$lang.save}</button>
</form>
<div class="seperator"></div>
</div>


<script type="text/javascript" src="/scripts/maps/gMaps3.js"></script> 
<script type="text/javascript" src="/scripts/maps/placeItemOnMap.js"></script> 
