<table width="400" border="0" align="center" cellpadding="0" cellspacing="1">
    {if $favorites ne 0}
  <tr>
    <td align="center" class="categoryBox">{$lang.title}</td>
	<td align="center" class="categoryBox">{$lang.date_added}</td>
    <td align="center" class="categoryBox">&nbsp;</td>
  </tr>
  {section name=f loop=$favorites}
  <tr>
    <td><a href="{$URL}/product.php?id={$favorites[f].id}">{$favorites[f].title}</a></td>
	<td>{$favorites[f].fav_date}</td>
    <td><a href="{$SELF}?t=delfav&id={$favorites[f].id}" title="{$lang.delete}"><img src="{$ImagesDir}/individual/cpanel/delete.icon.gif" width="18" height="18" alt="{$lang.delete}" border="0"></a></td>
  </tr>
  {/section}
  {else}
  <tr><td colspan="3">{$lang.no_favorites}</td></tr>
  {/if}
</table>
