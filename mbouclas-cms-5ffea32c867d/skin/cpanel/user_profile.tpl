{if $smarty.get.res eq 1}
<div class="error">{$lang.pass_no_match}</div>
<br>
{elseif $smarty.get.res eq 2}
<div align="center">{$lang.profile_updated}</div>
<br>
{/if}

<form name="form" action="{$SELF}" method="post" id="signup">
<table width="50%" border="0" align="center" cellpadding="0" cellspacing="1">
  <tr>
      <td class="labelcell">{$lang.username}</td>
    <td class="fieldcell"><input type="text" name="uname" value="{$UNAME}"></td>
  </tr>
  <tr>
    <td class="labelcell">{$lang.user_name}</td>
    <td class="fieldcell"><input type="text" name="user_name" value="{$USER_NAME}"></td>
  </tr>
  <tr>
    <td class="labelcell">{$lang.email}</td>
    <td class="fieldcell"><input type="text" name="email" value="{$EMAIL}"></td>
  </tr>
  <tr>
    <td class="labelcell">{$lang.password}</td>
    <td class="fieldcell"><input name="password" type="password" id="password"></td>
  </tr>
  <tr>
    <td class="labelcell">{$lang.password_confirm}</td>
    <td class="fieldcell"><input name="confirm_password" type="password" id="confirm+password"></td>
  </tr>
  <tr>
    <td align="center" colspan="3"><input name="Submit2" type="submit" class="button" value="{$lang.update}">
      <input name="mode" type="hidden" id="mode" value="prf_upd"></td>
  </tr>
</table>
</form>
