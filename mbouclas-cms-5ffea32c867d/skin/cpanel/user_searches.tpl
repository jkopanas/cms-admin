<table width="400" border="0" align="center" cellpadding="0" cellspacing="1">
  {if $saved_searches ne 0}
  <tr>
      <td align="center" class="categoryBox">{$lang.title}</td>
    <td align="center" class="categoryBox">{$lang.date_added}</td>
	  <td class="categoryBox">&nbsp;</td>
  </tr>
	{section name=s loop=$saved_searches}
    <tr>
      <td><a href="{$URL}/search.php?sid={$saved_searches[s].id}" title="{$saved_searches[s].title}">{$saved_searches[s].title}</a></td>
      <td>{$saved_searches[s].date_added}</td>
	  <td><a href="{$SELF}?t=delsrc&id={$saved_searches[s].id}" title="{$lang.delete}"><img src="{$ImagesDir}/individual/cpanel/delete.icon.gif" width="18" height="18" alt="{$lang.delete}" border="0"></a></td>
    </tr>
	{/section}
  {else}
  <tr>
    <td colspan="3">{$lang.no_searches}</td>
  </tr>
  {/if}
</table>
