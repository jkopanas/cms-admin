<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={$country_details.charset}">
<link href="{$CssDir}/style_front.css" rel="stylesheet" type="text/css">
<title>{$service_title}</title>
{include file="ajax.tpl"}
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
{if $ret eq 1}
<div class="error">{$message}</div>
{else}
{$message}
{/if}	
{if $smarty.get.mode eq "adds"}
<form action="{$SELF}" method="post" name="form">
 {$lang.title} <input name="title" type="text" id="title" value="{$s.title}" />

 <input type="submit" name="Submit" value="{if $s.id}{$lang.update}{else}{$lang.save}{/if}" />
 <input name="mode" type="hidden" id="mode" value="add_search" />
 {if $s.id}
 <input name="sid" type="hidden" id="mode" value="{$s.id}" />
 {/if}
</form>
{/if}
{if $smarty.get.mode eq "fpass"}
<form action="{$SELF}" method="post" name="form">
 {$lang.username} 
   <input name="username" type="text" id="username" />

 <input type="submit" name="Submit" value="{$lang.send}" />
 <input name="mode" type="hidden" id="mode" value="forgot" />
</form>
{/if}
{if $smarty.get.mode eq "tell"}
<div id="SomeElementId"></div>
<form name="tell_form" id="tell_form" action="javascript:void(null);" onsubmit="submitTell();">
 <table width="100%" border="0" cellspacing="0" cellpadding="2">

   <tr>
     <td>{$lang.friends_email}</td>
     <td><input name="email_other" type="text" id="email_other" /> 
     <font color="#FF0000">*</font> </td>
   </tr>
   <tr>
     <td>{$lang.friends_name}</td>
     <td><input name="name_other" type="text" id="name_other" /></td>
   </tr>
   <tr>
     <td>{$lang.comments}</td>
     <td><textarea name="comments" cols="30" rows="6" id="comments"></textarea></td>
   </tr>
   <tr>
     <td colspan="2" align="center"><input type="submit" name="Submit" value="{$lang.send}" id="submitButton" />
	 </td>
     </tr>
 </table>
  <input name="id" type="hidden" id="id" value="{$smarty.get.id}" />
 <input name="mode" type="hidden" id="mode" value="tellf" />
</form>
{/if}
</td>
  </tr>
</table>
</body>
</html>
