<html>
<head>
<link href="{$URL}/{$CssDir}/style_front.css" rel="stylesheet" type="text/css">
</head>
<body>
{capture name=dialog}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>({if $other_name}{$other_name}{/if} {$other_email}) thought you might be interested in the following listing.</td>
  </tr>
  <tr>
    <td>Personal note from sender : <strong>{$comments} </strong></td>
  </tr>
  <tr>
    <td><table width="100%"  border="0">

      <tr>
        <td valign="top"><img src="{$URL}/{$product.image}" border="0" alt="{$product.title}" title="{$product.title}" align="left" class="imgborder">
<a href="{$URL}/product.php?id={$product.id}" title="{$product.title}">{$product.title}</a><br>
{$lang.sku} : #{$product.sku}<br>
{$lang.location} : 
{section name=w loop=$product.nav_loc}<a href="{$URL}/loc.php?cat={$product.nav_loc[w].categoryid}" class=navpath title="{$product.nav_loc[w].category}">{$product.nav_loc[w].category}</a> {if !$nav[w].last}:: {/if}{/section}
<br />
{$lang.category} :
{section name=w loop=$product.nav_cat}<a href="{$URL}/cat.php?cat={$product.nav_cat[w].categoryid}" class=navpath title="{$product.nav_cat[w].category}">{$product.nav_cat[w].category}</a> {if !$nav[w].last}:: {/if}{/section}
			  <br>
          <span class="description">{$product.description}</span> </td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          {if $product.description}	  {/if}
          <tr>
            <td> {if $classes}
              <table width="100%" border="0">
                {section name=t loop=$classes}
                <tr>
                  <td> {$classes[t].classtext}: 
                    {section name=az loop=$classes[t].options}
                    {$classes[t].options[az].option_name}{if !%az.last%} - {/if}
                    {/section} </td>
                </tr>
                {/section}
              </table>
              {/if} </td>
          </tr>
          {if $extra_fields}
          <tr>
            <td><TABLE width="100%" border=0>
              <TR> {section name=field loop=$extra_fields}
                {assign var="aaa" value="efields-"|concat:$extra_fields[field].fieldid}
                <TD valign="top"><div style="width:210; float:left;" align=center>
                  <table cellpadding=2 cellspacing=1 width=210 height=30>
                    <tr bgcolor="#d5dde7">
                      <td width=60%><strong>{$extra_fields[field].field}</strong></td>
                      <td> {if $extra_fields[field].type eq "radio"} {if $extra_fields[field].value eq 1} {$lang.yes} {else}{$lang.no}{/if} {else} {$extra_fields[field].value} {/if}</td>
                    </tr>
                  </table>
                </div></TD>
                {* see if we should go to the next row *} {if not ($smarty.section.field.rownum mod 3)} {if not $smarty.section.field.last} </TR>
              <TR> {/if} {/if} {if $smarty.section.field.last} {* pad the cells not yet created *} {math equation = "n - a % n" n=3 a=$products|@count assign="cells"} {if $cells ne 3} {section name=pad loop=$cells}
                <TD>&nbsp;</TD>
                {/section} {/if} </TR>
              {/if} {/section}
            </TABLE></td>
          </tr>
          {/if}
        </table>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$product.title extra="width=100%"}
</body>
</html>