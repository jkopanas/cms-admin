<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$SITE_NAME}</title>
<link href="/css/styleNew.css" rel="stylesheet" type="text/css" />
<link href="/css/verticalcollapse.css" rel="stylesheet" type="text/css" />
<link href="/scripts/lightbox/css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css" />
<link href="/scripts/slider/mcmsSlider.css" rel="stylesheet" type="text/css" />
<link href="/scripts/kendo/styles/kendo.common.min.css" rel="stylesheet"/>
<link href="/scripts/kendo/styles/kendo.metro.min.css" rel="stylesheet"/>
<script type="text/javascript" src="/scripts/head.load.min.js"></script>
<script type="text/javascript">
var mcms;
head.js('/scripts/jquery-1.7.1.min.js');
head.js('/scripts/lightbox/js/jquery.lightbox-0.5.js');
head.js('/scripts/mcmsFront.js');
head.js('/scripts/mcmsMenu.js');
head.js('/scripts/kendo/js/kendo.core.min.js');
head.js('/scripts/kendo/js/kendo.validator.min.js');
head.js('/scripts/kendo/js/kendo.data.min.js');
head.js('/scripts/kendo/js/kendo.binder.min.js');
head.js('/scripts/kendo/js/kendo.panelbar.min.js');
head.js('/scripts/kendo/js/kendo.listview.min.js');
head.ready(function(){
mcms =  $(document).mCms({ lang:$('input[name=loaded_language]').val() }).data().mcms;//Initialize
$.lang = mcms.getLang();
});
</script>
</head>

<body>
<div class="hidden">
{include file="modules/eshop/cartDialog.tpl"}
</div>
<input type="hidden" value="{$FRONT_LANG}" name="loaded_language" />
<div class="wrapper">
<div class="page">
<div class="header-container">
 <div class="header">
 <h1 class="logo"><a href="/{$FRONT_LANG}/index.html" title="{$SITE_NAME}" class="logo"><img src="/images/site/logo.png" alt="{$SITE_NAME}" width="144" height="79" /></a></h1>
   {if $ID}
 <div class="welcome-msg">{$lang.welcome} {$UNAME}</div>
 {/if}
 <div class="language-switcher">{include file="langSelect.tpl"}</div>
 <div class="topCart"></div>
    <div class="quick-access">            						
        <ul class="links">
          {if $ID}
        		<li class="first"><a href="/user/{$UNAME}/index.html" title="My Account" ><span>My Account</span></a></li>
        		<li><a href="/index.php?logout" title="Log Out" ><span>Log Out</span></a></li>  
		{ELSE}
        	  	<li><a href="/member_login.html" title="Log In" ><span>Log In</span></a></li>
        {/if}
        <li><a href="/{$FRONT_LANG}/checkout.html" title="check out" ><span>Checkout</span></a></li>
        <li class="top-links-5 last"><a href="/{$FRONT_LANG}/cart.html" title="My Cart" class="top-link-cart"><span>My Cart</span> <span class="{if !$total_quantities}hidden{/if} cartItems">(<span class="total-items">{$total_quantities|default:0}</span> Προϊόντα : <span class="price">&euro;</span> <span class="price total-price">{$total_price|formatprice}</span>)</span></a></li>
        </ul>
    </div>
            </div>
</div><!-- END HEADER -->
<div class="nav-container">
    <ul id="nav">
		<li><a href="/{$FRONT_LANG}/index.html"><span>{$lang.home}</span></a></li>
      {include file="modules/themes/menu_recursive.tpl" menu=$TopMenu.items level=0 class="selected"}
  </ul>
</div><!-- END MENU -->
<div class="main-container {if $area eq 'home' OR ($item AND $current_module.name eq "products")}col1-layout{elseif $item AND $current_module.name eq "content"}col2-left-layout{/if}">
{if $nav}{include file="modules/themes/nav_bar.tpl"}{/if}
<div class="main">
  <!-- <input type="text" class="autoComplete" name="title" id="title" data-searchBy="title" /> -->
 <div class="results" data-for="title"></div>

{include file=$include_file}
<div class="seperator"></div>
</div><!-- END MAIN -->
</div><!-- END MAIN CONTAINER -->
<div class="footer-container">
    <div class="footer">
		<div class="footer-static-links">		
			<ul>
<li><strong>Customer Service</strong></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
</ul>
<ul>
<li><strong>Ordered Info</strong></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
</ul>
<ul>
<li><strong>Customer Service</strong></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
</ul>
<ul>
<li><strong>Ordered Info</strong></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
<li><a href="index.html#/">Static footer links</a></li>
</ul>
<ul class="follow-us">
<li><strong>Follos Us</strong></li>
<li> <a href="index.html#/"><img src="/images/site/twitter_icon.gif" alt="" /></a> <a href="index.html#/"><img src="/images/site/facebook_icon.gif" alt="" /></a></li>
<li class="paypal-icon"><img src="/images/site/paypal_icon.png" alt="" /></li>
</ul> 
		</div>
	<!-- 	
		<div class="footer-newsletter"><div class="block-subscribe">
    <form action="http://demo.dazmetech.com/mage3/index.php/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">                    
			<h2><label for="newsletter">Newsletter Sign Up:</label></h2>
			<div class="newsletter-text">						
				<div class="input-box">				
				 <input type="text" name="email" id="newsletter" title="Sign up for our newsletter" class="input-text required-entry validate-email" />
				 <button type="submit" title="Subscribe" class="button"><span><span>Subscribe</span></span></button>
				</div>
			</div>
    </form>

</div>
</div>
		 -->
		<div class="footer-callus"><h1>Direct line</h1><h2>+357 0000000</h2></div>
		
				
		
		<div class="footer-bottom">
			<div class="f-left">
				<ul>
<li><a href="/{$FRONT_LANG}/index.html">Home</a></li>
<li class="last"><a href="/{$FRONT_LANG}/content/franchise/index.html">Franchise</a></li>
</ul>
<ul class="links">
<li class="top-links-1 first"><a href="/{$FRONT_LANG}/content/photo-gallery/index.html" title=""><span>Photo Gallery</span></a></li>
<li class="top-links-2"><a href="/{$FRONT_LANG}/content/catalogs/index.html" title=""><span>Catalogs</span></a></li>
<li class="top-links-3"><a href="/{$FRONT_LANG}content/our-shops/index.html" title=""><span>Our Shops</span></a></li>
<li class="top-links-3 last" ><a href="/{$FRONT_LANG}/products/eshop/index.html" title="">E-shop</a></li>
</ul>
			</div>
			<div class="f-right">
								<address>&copy; 2013 BouketoMpalonia. All Rights Reserved. Powered by <a href="http://www.net-tomorrow.com" target="_blank" title="Κατασκευή ιστοσελίδας">Net-Tomorrow.com</a></address>
			</div>			
		</div>
		<div class="clear"></div>	
    </div><!-- END FOOTER CONTAINER -->
</div><!-- END FOOTER -->


</div><!-- END PAGE -->
</div><!-- END WRAP -->


          
          <script>
head.js('/scripts/site/kendoMix.js');
head.js('/scripts/autoComplete.js');
head.js('/scripts/site/global.js');
head.js('/scripts/site/eshop.js');
</script>
</body>
</html>
