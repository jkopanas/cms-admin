<div id="lang_sel">
    <ul>
        <li><img alt="el" src="{$LanguageDetails.image}" class="iclflag" /></li>
            {foreach from=$availableLanguages item=a}
            {if $a.code ne $FRONT_LANG}
			<li class="icl-en"><a href="{$a.thisURL}"><img alt="en" src="{$a.image}" class="iclflag" /></a></li>
            {/if}
            {/foreach}

    </ul>    
</div><!-- END LANGUAGE SELECTOR -->