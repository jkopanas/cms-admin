{include file="modules/billing/admin/menu.tpl"}

<div class="seperator padding wrap">
<h2><a href="javascript:void(0)" class="ShowSearchTable Slide" rel="#SearchTable">Αναζήτηση</a></h2>
<div class="debug"></div>
<form name="searchform" id="searchTransactionsForm"  method="POST">
<table width="100%" border=0 cellpadding="5" cellspacing="5" id="SearchTable" class="">

<TR>
  <TD nowrap class=FormButton>{$lang.results_per_page}</TD>
  <TD width="933"><select name="results_per_page" id="results_per_page" class="secondarySearchData">
    <option value="10" {if $data.results_per_page eq 10}selected{/if}>10</option>
    <option value="20" {if $data.results_per_page eq 20}selected{/if}>20</option>
    <option value="60" {if $data.results_per_page eq 60}selected{/if}>60</option>
    <option value="100" {if $data.results_per_page eq 100}selected{/if}>100</option>
  </select>  </TD>
</TR>
<TR>
  <TD nowrap class=FormButton>{$lang.order_by}</TD>
  <TD> <select name="orderby" class="secondarySearchData">
  <option value="billingTransactions.id">#ID</option> 
   <option value="billingTransactions.transDate">{$lang.date_added}</option>
   <option value="billingTransactions.creditsChange">{$lang.amount}</option>
   <option value="users.uname">{$lang.uname}</option>
</select>  
{$lang.way} : 
<select name="way" id="sort_direction" class="secondarySearchData">
<option value="desc">{$lang.descending}</option>
<option value="asc">{$lang.ascending}</option>             
        </select></TD>
</TR>
<TR>
  <TD width="94" nowrap class=FormButton>#ID</TD>
  <TD>
  <INPUT name="billingTransactions.id" type=text id="billingTransactions.id" value="{$data.id}" class="SearchData"  /></TD>
</TR>
<tr>
  <td nowrap class=FormButton>{$lang.user_name}</td>
  <td><INPUT name="user_name" type=text id="id" value="{$data.id}" class="likeData"  /> {$lang.surname} : <INPUT name="user_surname" type=text id="id" value="{$data.id}" class="likeData"  /></td>
</tr>
<tr>
  <td class=FormButton nowrap>{$lang.priceRange}</td>
  <td>
    <div id="price-range" class="float-left" style="width:50%"></div> <span id="amount" class="float-left spacer-left" ></span> <input type="hidden" name="rangeActive" value="0" class="secondarySearchData" /> <input type="hidden" name="maxPrice" value="{$maxPrice}" /> <input type="hidden" name="amountFrom" value="" class="secondarySearchData" /> <input type="hidden" name="amountTo" value="" class="secondarySearchData" />
    </td>
</tr>
<tr>
  <td class=FormButton nowrap>{$lang.date}</td>
  <td>{$lang.from} : <input type="text" id="dateFrom" name="dateFrom" class="datePicker secondarySearchData" size="30"/> {$lang.to} : <input type="text" id="dateTo" name="dateTo" class="datePicker secondarySearchData" size="30"/> </td>
</tr>

<tr>
  <td class=FormButton nowrap>{$lang.specialFilters}</td>
  <td><select name="custom_field" class="secondarySearchData">
    <option value="">{$lang.none}</option>
    <option value="users.uname" {if $filters.custom_field eq "uname"}selected{/if}>{$lang.username}</option>
    <option value="users.email" {if $filters.custom_field eq "email"}selected{/if}>{$lang.email}</option>
    </select>
    <input type="text" name="custom_value" size="30" value="{$data.substring}" class="secondarySearchData" />  </td>
</tr>
<tr> 
  <td colspan="2" align="center" class=FormButton>
    <input type="submit" value="{$lang.search}" class="button" id="searchUsers" name="searchTransactions">
    <input name="page" type="hidden" id="page" value="{$page}" class="secondarySearchData">
    <input name="ResultsDiv" type="hidden"  value="#ResultsPlaceHolder" class="secondarySearchData">
    <input name="FormToHide" type="hidden"  value="#SearchTable" class="secondarySearchData"></td>
</tr>

</table>
<script src="/scripts/jquery.multiselect.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.multiselect.filter.min.js"></script>
<script src="/scripts/billing.js"></script>
</form>
</div><!-- END WRAP -->

<div id="ResultsPlaceHolder">{include file="modules/billing/admin/resultsTable.tpl"}</div>

<div id="addTransaction" class="hidden">
    <div class="content" align="center">
    <div class="seperator wrap padding">
        <h2>{$lang.addTransaction}</h2>
        <form id="addTransactionForm" class="addTransactionForm">
        <table>
        <tr>
        <td>
        	{$lang.username}:
        </td>
        <td>
        	<input type="text" name="user_name" id="user_name" class="autocomplete addCreditsData" />
        </td>
        </tr>
        <tr>
        <td>
        	{$lang.amount}:
        </td>
        <td>
        	<input type="text" name="credits" class="addCreditsData" />
        </td>
        </tr>
        <tr>
        <td colspan="2"><input type="submit" value="{$lang.apply}"/></td>
        </tr>
        </table>
        <input type="hidden" name="uid"  class="addCreditsData" id="userID" />
        </form>
        </div>
    
    </div>
</div>
