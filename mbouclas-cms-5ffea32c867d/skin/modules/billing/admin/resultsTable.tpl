
<form>
<div class="seperator padding wrap">
<h2>{$lang.Transactions} [ {$list.from} {$lang.to} {$list.to} {$lang.of_total} : {$list.total} ]<a href="#" id="addTransaction" >({$lang.addTransaction})</a></h2>
<table width="100%"  border="0" class="widefat">
  
       <tr class="nodrop nodrag hidden" id="selectAllResultsInfo">
        <td colspan="6">
    		<div class="messagePlaceHolder messageWarning">{$lang.allResultsSelected}</div>
            </td>
      </tr>    
        <TR class="TableHead" {cycle values=", class='TableSubHead'"}>
    <td width="10"><strong>ID</strong></td>
    <td><strong>{$lang.username}</strong></td>
    <td><strong>{$lang.date}</strong></td>
    <td><strong>{$lang.previousCredits}</strong></td>
    <td><strong>{$lang.creditsChange}</strong></td>
    <td><strong>{$lang.currentCredits}</strong></td>
    </tr>
{foreach from=$items item=a}
  <TR {cycle values=", class='alternate'"}>

<td width="10"><strong>{$a.id}</strong></td>
    <td>{$a.uname}</td>
    <td>{$a.transDate|date_format:"%d/%m/%Y @ %H:%M"}</td>
    <td>{$a.prevCredits}</td>
    <td>{$a.creditsChange}</td>
    <td>{$a.credits}</td>
    
    </tr>
  {/foreach}
 
  </table>
</div>
<input type="hidden" class="CheckedActions" value="" name="allItemsSelected" />
<input name="page" type="hidden" id="page" value="{$page}" class="CheckedActions">
{if $list.total gt $users|@count}
{include file="modules/content/pagination.tpl" mode="ajax" class="transactionsPage" target="searchTransactionsForm"}
{/if}
</form>
