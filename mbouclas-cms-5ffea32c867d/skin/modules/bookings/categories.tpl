<a name="translations" id="translations"></a>{include file="modules/content/admin/menu.tpl"}
{include file="modules/content/search_box.tpl" mode="start_page"}

{if !$edit}

<div class="wrap padding seperator" id="box3">
<h2><a href="{$MODULE_FOLDER}/categories.php">Κατηγορίες</a> (<a href="#addcat">Προσθήκη</a>) {include file="modules/content/nav_categories.tpl" management=$lang.categories target="admin_categories"}</h2>
<form name="cat_form" id="cat_form" action="" method="post">
<input type="hidden" value="SaveCategoriesOrder" name="action" />
<div id="debugArea"></div>
<table cellpadding="3" cellspacing="3" width="100%" class="DragDrop">
<tr class="nodrop nodrag">
		<th width="16">&nbsp;<input type="hidden" name="TableOrder" class="settings" value="" /></th>
		<th scope="col">ID</th>
		<th scope="col">{$lang.title}</th>
        <th scope="col">{$lang.description}</th>
        <th scope="col">#{$lang.subcategories}</th>
        <th scope="col">#{$lang.content}</th>
        <th colspan="2">{$lang.actions}</th>
	</tr>
{section name=a loop=$cat}
<tr class="{cycle values='alternate,'}" id="Order{$cat[a].categoryid}"> 
<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
<td><strong>{$cat[a].categoryid}</strong></td>
  <td> <a href="categories.php?cat={$cat[a].categoryid}">{$cat[a].category}</a></td>
				<td>{$cat[a].description|truncate:50:" ...":true}</td>
				<td>{$cat[a].num_sub|default:$lang.txt_not_available}</td>
				<td align="center"><A href="category_content.php?cat={$cat[a].categoryid}" class="ItemsList">{$cat[a].num_items|default:0}</A></td>
                <td><a href="{$MODULE_FOLDER}/new.php?cat={$cat[a].categoryid}" class="edit" title="Add Item"><img src="/images/admin/add_16.gif" width="16" height="16" alt="Add Item" /></a></td>
      <td><a href="{$MODULE_FOLDER}/categories.php?action=edit&amp;cat={$cat[a].categoryid}" class="edit" title="Edit Category {$cat[a].category}"><img src="/images/admin/edit_16.png" width="16" height="16" /></a></td><td><a href="{$MODULE_FOLDER}/categories.php?action=delete&amp;cat={$cat[a].categoryid}" onclick="return confirm('You are about to delete the category \'{$cat[a].category}\'.  All of its items will go to the default category.\n  \'OK\' to delete, \'Cancel\' to stop.')" class="delete" title="Delete Category {$cat[a].category}"><img src="/images/admin/delete_16.png" width="16" height="16" /></a></td>
				</tr>
{/section}
</table>
<input type="hidden" name="cat" id="cat" value="{$smarty.get.cat}" />
                <input type="hidden" name="mode" id="mode" value="quick_update" />
  <input type="hidden" name="featured" id="featured" value="1" />
  </form>
</div> 
{/if}
<div class="wrap seperator padding">
 <h2>Προβεβλημένες σελίδες ( <img src="/images/admin/add.gif" align="absmiddle" /> <a href="javascript:void(0);" class="show-div" rel="search-box">{$lang.add_new}</a> )</h2>
 <div id="search-box" class="search-box hidden">
  <ul id="IdTabs"  class="idTabs">
<li><a  href="#tab1" rel="tab1" class="selected">{$lang.page}</a></li>
<li><a  href="#tab2" rel="tab2">{$lang.category}</a></li>
</ul>
<div class="clear"></div>
<div id="tab-container" class="clear">
<div id="tab1" class="tabcontent">
 <form name="FeaturedItemsForm" action="" method="post" class="QuickSearchForm">
  <input type="hidden" name="DisplayDiv" value="SearchBoxResultsForm-div" />
 Αναζήτηση σε : <select name="field" id="QuickSearchField" class="SearchData">

      <option value="id">#ID</option>
      <option value="substring">Τίτλο</option>
    </select>
     <input name="search_substring" type="text" class="search-style SearchData" id="QuickSearchVal" size="40"/>  
     <select name="categoryid" id="catid" class="SearchData">
        <option value="%">Όλες οι κατηγορίες</option>    
{foreach from=$root_categories item=a}
<option value="{$a.categoryid}">{$a.category}</option>
{/foreach}
</select> <input type="button" id="QuickSearchBtn1" value="Εμφάνιση αποτελεσμάτων" class="button" name="PerformSearch" /> <input name="ResultsDiv" type="hidden"  value="#FeaturedResultsPlaceHolder-div" class="SearchData">
<input name="action" type="hidden"  value="QuickSearch" class="SearchData"> <input name="mode" type="hidden"  value="FeaturedContent" class="SearchData"> <input name="CurrentCat" type="hidden"  value="{$CurrentCat.categoryid}" class="SearchData">

</form>
   <div class="seperator clear"></div>
   <form name="SearchBoxResultsForm" method="post">

<div id="FeaturedResultsPlaceHolder-div"></div>
   </form>
</div><!--END TAB 1 -->
<div id="tab2" class="tabcontent">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
     <tr>
       <th scope="col">#ID</th>
       <th scope="col">Τίτλος</th>
       <th width="18">&nbsp;</th>
     </tr>
{foreach from=$allcategories item=a}
     <tr class="result_item_row {cycle values='alternate,'}" id="Row{$a.id}">
       <td>{$a.categoryid}</td>
       <td><a href="categories.php?cat={$a.categoryid}">{$a.category}</a></td>
       <td><a href="javascript:void(0);" class="AddItem" rel="{$a.categoryid}-cat" title="{$a.category}"><img src="/images/admin/add.gif" width="18" height="18" border="0" /></a></td>
     </tr>
     {/foreach}
   </table>
</div><!--END TAB 2 -->
</div><!-- END TAB CONTAINER -->
   </div>
<form name="ExistingItems" method="post"><input type="hidden" name="type" value="FeaturedContent" class="SearchData" /><input type="hidden" name="CurrentCat" value="{$CurrentCat.categoryid|default:0}" class="SearchData" />
<input type="hidden" value="SaveFeaturedOrder" name="action" />
<div id="debugArea"></div>
 <div id="existing_items">
 
 {include file="modules/content/admin/quick_search_results.tpl" mode="results" items=$featured}</div></form>
</div><!-- END WRAP -->
 <br /><!-- END SEARCH BOX -->
<form name="addcat" id="addcat" action="" method="post">               
          <div class="wrap seperator padding"><a name="addcategory" id="addcategory"></a>
          <div id="zeitgeist" style="width:300px;">
  <h3>Επιλογές</h3>
  <select name="settings_theme">
  {foreach item=a from=$themes}
  <option value="{$a.name}" {if $a.name eq $category.settings.theme}selected{/if}>{$a.title}</option>
  {/foreach}
  </select>
   Επιλογή εικαστικού
  <br />

  <select name="settings_on_menu">
  <option value="1" {if  $category.settings.on_menu eq 1}selected{/if}>{$lang.yes}</option>
  <option value="0" {if  $category.settings.on_menu eq 0}selected{/if}>{$lang.no}</option>
  </select> Εμφάνιση στο κεντρικό μενού

  <br />Κατάταξη με 
    <select name="settings_orderby">
  <option value="title" {if  $category.settings.orderby eq "title"}selected{/if}>Τίτλος</option>
  <option value="date_added" {if  $category.settings.orderby eq "date_added"}selected{/if}>Ημερομηνία</option>
  <option value="orderby" {if  $category.settings.orderby eq "orderby"}selected{/if}>Σειρά</option>
  </select>     
  <select name="settings_way">
  <option value="ASC" {if  $category.settings.way eq "ASC"}selected{/if}>Αύξουσα</option>
  <option value="DESC" {if  $category.settings.way eq "DESC"}selected{/if}>Φθήνουσα</option>
  </select>
  <br />
  <input name="settings_results_per_page" type="text" value="{$category.settings.results_per_page}" size="5" maxlength="3" /> Αποτελέσματα ανά σελίδα
  <br />
  <input name="settings_extra_template" type="text" value="{$category.settings.extra_template}" size="5" maxlength="3" /> Extra template
  <br />
{if $more_categories}
<h3>Περισσότερες κατηγορίες</h3>
{section name=a loop=$more_categories}
{if $category.categoryid eq $more_categories[a].categoryid}<strong>{$more_categories[a].category}</strong>{else}<a href="{$MODULE_FOLDER}/categories.php?action=edit&cat={$more_categories[a].categoryid}">{$more_categories[a].category}</a>{/if}<br />
{/section}
{/if}
</div>


    <h2>{if $edit eq 1}Τροποποίηση {$category.category} -> <a href="{$MODULE_FOLDER}/categories.php">Κατηγορίες</a>   {include file="modules/content/nav_categories.tpl" management=$lang.categories target="admin_categories"} {else}Προσθήκη κατηγορίας{/if}</h2>
    
         
      <p>Θέση:<br />
        <input name="orderby" type="text" class="editform" style="background-color: rgb(255, 255, 160);" value="{$category.order_by}" size="4" id="orderby" />
         </p>
        <p>{$lang.title}:<br />
        <input name="title" type="text" style="background-color: rgb(255, 255, 160);" value="{$category.category}" />
        </p>
        <p>{$lang.permalink}:<br />
        <input name="alias" type="text" style="background-color: rgb(255, 255, 160);" value="{$category.alias}" />
        </p>
        <p>Ανήκει στην κατηγορία :<br />
          <select name="parent_catid" class="postform" id="parent_catid"> 
<option value="%">Όλες οι κατηγορίες</option>
{foreach from=$allcategories item=a}
<option value="{$a.categoryid}" {if $a.categoryid eq $CurrentCat.parentid}selected{/if}>{$a.category}</option>                
{/foreach}  
</select></p>
        <p>{$lang.description}: (προερετικό) <br />
        <textarea name="description" rows="5" cols="60"  id="desc">{$category.description}</textarea>
        <br />
        <a href="javascript:void(0);" class="open-editor" rel="desc">{$lang.advanced_editor}</a>
        </p>

        <p  align="left"><input name="action" value="{$action|default:'addcat'}" type="hidden" /><input type="hidden" name="cat" value="{$catid|default:$smarty.get.cat}" /><input name="submit" type="submit" class="button" value="{$lang.save}" />  {if $category}<a href="categories.php?cat={$category.categoryid}&action=edit&mode=translate#translate">{$lang.translate}</a> {/if}
        </p>
    
</div>      
</form>
<div class="seperator"></div>
{if $mode eq "translate"}
<a href="#" id="translate"></a>
<div class="seperator padding wrap">
<div id="zeitgeist">
<h3>{$lang.available_languages}</h3>
<ul id="languageTabs"  class="idTabs">
{foreach from=$availableLanguages item=a name=b}
<li><a  href="#taber{$smarty.foreach.b.iteration}" rel="taber{$smarty.foreach.b.iteration}">{$a.country}</a></li>
{/foreach}
<li><input type="button" value="{$lang.save}" class="SaveTranslations button" /></li>
</ul>
</div>
<h2>{$lang.translations}</h2>
<form>
<input type="hidden" class="originalData" value="{$category.categoryid}" name="id" />
<input type="hidden" class="originalData" value="{$category.category}" name="category" />
<input type="hidden" class="originalData" value="{$category.alias}" name="alias" />
<input type="hidden" class="originalData" value='{$category.description}' name="description" />

<table width="650">
<Tr><td><div id="tab-container">
{foreach from=$availableLanguages item=a name=b}
<div id="taber{$smarty.foreach.b.iteration}" class="tabcontent">
<h3>{$lang.defaultLanguage} : {$defaultLanguage.details.country} - {$lang.NowEditing} {$a.country}</h3>
<p>{$lang.title} : <br /><input name="content_categories@category-{$a.code}" type="text" id="category-{$a.code}" size="45" value="{$a.item.category.translation}" class="languageTranslationBox"></p>
<p>{$lang.permalink} : <br /><input name="content_categories@alias-{$a.code}" type="text" id="alias-{$a.code}" size="45" value="{$a.item.alias.translation}" class="languageTranslationBox"></p>
<p>{$lang.description} : <br /><textarea name="content_categories@description-{$a.code}" cols="80" rows="5" id="description-{$a.code}" class="languageTranslationBox">{$a.item.description.translation}</textarea><br /><a href="javascript:void(0);" class="open-editor" rel="description-{$a.code}">{$lang.advanced_editor}</a>
</div><!-- END TAB -->
<div class="clear"></div>
{/foreach}
<div class="clear"></div>
</div><!-- END TAB CONTAINER -->
</td></Tr>
</table>
</form>

<div class="clear"></div>
</div><!-- END WRAP -->
<div class="seperator"></div>

<script src="/scripts/AdminLanguages.js"></script>
{/if}