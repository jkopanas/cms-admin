<div class="debug"></div>
{if $mode eq "edit"}
<a href="#" class="saveData">{$lang.save}</a>
<table width="100%" id="bookingsRates" border="1" cellpadding="5" cellspacing="5">
<thead id="bookings">
<tr>
<th id="season"><a href="#" class="addSeason">Add season</a> - <a href="#" class="addPeriod">Add period</a><input type="hidden" name="itemid" value="{$item.id}" />
</th>
{foreach from=$uniqueData.periods item=b key=k}
<th id="period"><input type="text" name="period[]" value="{$k}" /><input type="hidden" name="orderbyPeriod[]" value="{$b.orderby}" class="orderbyPeriod" /> <a href="#" class="deleteCol deleteItem hidden"><img src="/images/admin/delete.gif" border="0" /></a>
</th>
{/foreach}
<th id="minStay">Min Stay</th>
</tr>
</thead>
<tbody id="bookingsBody">
{foreach from=$seasons item=a name=b}
<tr id="{$a.id}">
<td class="season"><input type="text" name="season-{$a.id}" value="{$a.title}" /> <a href="#" class="deleteRow deleteItem hidden"><img src="/images/admin/delete.gif" border="0" /></a><br /><input type="hidden" name="seasonID-{$a.id}" class="seasonID" value="{$a.id}" /><input type="hidden" name="orderby-{$a.id}" value="{$a.orderby}" class="orderbySeason" />
<a href="#calendarContainer" rel="{$a.id}" class="openCal">Select date range</a><br>
<input type="hidden" id="dateVal-{$a.id}" name="dateVal-{$a.id}" value="{$a.start|date_format:'%d/%m/%Y'}###{$a.end|date_format:'%d/%m/%Y'}"><span id="date-{$a.id}">{$a.start|date_format:"%d/%m/%Y"} - {$a.end|date_format:"%d/%m/%Y"}</span>
<div style="position:relative;"><div id="widget-{$a.id}" class="widget"><div id="widgetCalendar-{$a.id}" class="widgetCalendar"></div></div><div class="seperator"></div></div>

</td>
{foreach from=$a.periods item=x}
<td class="period"><input type="text" name="price-{$a.id}[]" value="{$x.price}" /><input type="hidden" name="periodID-{$a.id}[]" class="periodID" value="{$x.id}" /></td>
{/foreach}
<td><input type="text" name="minStay-{$a.id}" value="{$a.minStay}" /></td>
</tr>
{/foreach}
</tbody>
</table>
<a href="#" class="saveData">{$lang.save}</a>
{else}
<table width="100%" id="bookingsRates" border="1" cellpadding="0" cellspacing="0">
<thead id="bookings">
<tr>
<th id="season"><input type="hidden" name="itemid" value="{$item.id}" /></th>
{foreach from=$uniqueData.periods item=b key=k}
<th>{$k}

</th>
{/foreach}
<th id="minStay">Min Stay</th>
</tr>
</thead>
<tbody id="bookingsBody">
<tr id="summary">
<th scope="row">&nbsp;</th>
{foreach from=$seasons[0].periods item=x}
<th scope="row">{$b.min} - {$b.max}</th>
{/foreach}
<th scope="row">{$uniqueData.minStayAvg|round}</th>
</tr>
{foreach from=$seasons item=a name=b}

<tr>
<td {if $smarty.foreach.b.last}class="last-bottom"{/if}><strong>{$a.title}</strong><Br />
{$a.start|date_format:"%d/%m/%Y"} - {$a.end|date_format:"%d/%m/%Y"}
</td>
{foreach from=$a.periods item=x name=c}
<td align="center" {if $smarty.foreach.b.last}class="last-bottom"{/if}>{$x.price|default:"&nbsp;"}</td>
{if $smarty.foreach.c.last} {* pad the cells not yet created *} {math equation = "n - a % n" n=$seasons[0].periods|@count a=$a.periods|@count assign="cells"}
{if $cells ne $seasons[0].periods|@count} 
  {section name=pad loop=$cells}
    <TD {if $smarty.foreach.b.last}class="last-bottom"{/if}>&nbsp;</TD>
    {/section} {/if} 
  {/if} 

{/foreach}
<td align="center" class="last-right {if $smarty.foreach.b.last}last-bottom{/if}">{$a.minStay|default:"&nbsp;"}</td>
</tr>
{/foreach}
</tbody>
</table>
{/if}