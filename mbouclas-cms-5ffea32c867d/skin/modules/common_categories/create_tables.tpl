
<div class="dp75">
<div class="box_c" />
<h3 class="box_c_heading cf">
	<span class="fl spacer-left">{$lang.available_tables}</span>
</h3>


         	<style scoped>
               span.k-widget { width: 83%; }
            </style>
<div class="box_c_content cf formEl_a">
	<div id="grid"></div>
</div>
</div>
</div>
<div class="dp25">
<div class="box_c" />
<h3 class="box_c_heading cf">
	<span class="fl spacer-left">{if $menu}{$lang.modify} {$menu.title}{else}{$lang.add_table}{/if}</span>
</h3>
<form name="NewTables" id="NewTables" class="formEl_a" >
<div class="box_c_content cf">     
  		<div class="sepH_b">
        	<label class="lbl_a" for="title" >{$lang.table_name} * : </label>
        	<input name="table_name" id="table_name" type="text" class="inpt_a table" value="" required validationMessage=" {$lang.error_requiredField}" />
        </div>
        <div class="sepH_b">
        	<label class="lbl_a" for="title" >{$lang.title} *: </label>
        	<input name="title" id="title" type="text" class="inpt_a table" value="" required validationMessage=" {$lang.error_requiredField}" />
        </div>
        <div class="sepH_b">
 			<label class="lbl_a" for="title" >{$lang.availability} *: </label>
    		<select name="active" id="active" class="table" required validationMessage=" {$lang.error_requiredField}" >
		        <option value="0" {if $menu.active eq "0"}selected{/if}>{$lang.disabled}</option>
        		<option value="1" {if $menu.active eq "1"}selected{/if}>{$lang.enabled}</option>
      		</select> 
  		</div> 
       

        <p  align="left"><input name="action" value="{$action|default:'add'}" type="hidden" /><input type="hidden" name="id" value="" /><input name="submit" type="submit" class="button" value="Προσθήκη" />
        </p>
</div>
</form>
</div>
</div>
</div>

</div>

<script src="/scripts/admin2/common_categories/create_tables.js"></script>