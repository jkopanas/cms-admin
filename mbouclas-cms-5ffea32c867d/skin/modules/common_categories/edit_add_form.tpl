<div name="" id="" class="formEl_a">  
<div class="float-left dp50" >
		<fieldset>
                <legend>{$lang.basic_settings}</legend>
		<div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.title} :</label>
                <input type="text"  class="inpt_a cat_details" id="new_text" value="{$cat_details.category}" name="title" maxlength="50">
            	<input type="hidden" id="cat_id" class="cat_details" name="old_id" value="{$cat_details.categoryid}"/>
            </div>
            
            <div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.permalink} :</label> 
                <input type="text"  class="inpt_a cat_details" value="{$cat_details.alias}" name="alias" maxlength="50">
            </div>
            {if $cat_details.parentid!=""}
            <input type="hidden" name="parent_catid" class="cat_details" value="{$cat_details.parentid}"/>
         
                 {else}   	
                <div class="sepH_b cf">
    				<label for="country_multiple" class="lbl_a">{$lang.belongsToCategory} :</label>
   				 <select name="parent_catid" class="inpt_a cat_details"> 			
				<option value="0" selected="selected">{$lang.main_cat}</option>
				 {foreach from=$categories item=a name=b}
                        <option value="{$a.categoryid}"  >{$a.category}</option>
                        {/foreach}
			
				</select>
		</div>
		{/if}
		<div class="sepH_b" >
                <label class="lbl_a" for="v_text">{$lang.description}: ({$lang.optional}) </label>
		<textarea id="desc" cols="40" rows="8" name="desc" class="cat_details">{$cat_details.description}</textarea>
		</div>
		 </fieldset>
    </div>
   
    
    <div class="float-right dp50">
    	<fieldset>
                <legend>{$lang.extra_settings}</legend>
     	 <div class="sepH_b cf">
    				<label for="country_multiple" class="lbl_a">{$lang.select_template} :</label>
   				 <select name="settings_theme" class="inpt_a cat_details"> 
					{foreach from=$themes item=a name=b}	
					<option value="{$a.name}" {if $a.name eq $cat_details.settings.theme}selected{/if}>{$a.title}</option>
				{/foreach}
				</select> <br/> <br/>
						 {include file="common/drawSettings.tpl" prefix="settings_"  classes="inpt_a cat_details" pageSettings=$pageSettings val=$cat_details.settings}
		</div> 
		</fieldset>
    </div> 
    <br style="clear:both;"/>
    <center>
    {if $cat_details.category==""}
		<input type="submit" class="btn btn_b add_edit_cat" value="{$lang.save}" name="Enter" id="save_cat">
		{else}
		<input type="submit" class="btn btn_b add_edit_cat" value="{$lang.update}" name="Enter" id="edit_cat">
		{/if}
	</center>	
  </div>  
    <script type="text/javascript">
 		   head.js('/scripts/admin2/categories/complete.js');
			</script>