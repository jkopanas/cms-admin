<div id="FilterForm-{$ParentSettings}" class="formEl_a 	box_c_content cf hidden " >
		<div class="sepH_b  spacer-right">
       		<label for="id" class="lbl_a">#ID :</label>
        	<input name="id"  type="text"  value="" data-mode="EQ"  class="filterdata inpt_a"  />
        </div>
        <div class="sepH_b  spacer-right">
        	<label for="title" class="lbl_a">{$lang.title} *:</label>
        	<input name="title"  type="text"  value=""  class="filterdata inpt_a" />
        </div>
        <div class="sepH_b  spacer-right">
        	<label for="description" class="lbl_a">{$lang.description} *:</label>
        	<input name="description_long"  type="text"  value="" class="filterdata inpt_a" />
        </div>
        <div class="sepH_b  spacer-right" data-bind="visible: isVisible">
        	<label for="module" class="lbl_a">{$lang.module} : </label>
        	<select name="module" id="module-{$ParentSettings}" class="module inpt_d">
        		{foreach from=$settings.modules key=k item=a name=b}
        		<option value="{$a}" >{$a}</option>
        		{/foreach}
      		</select> 
        </div>
        <div class="sepH_b  spacer-right" >
        	<label for="category" class="lbl_a">{$lang.category} : </label>
        	<select name="category" id="categoryid-{$ParentSettings}" data-mode='EQ' class="category categories  inpt_d">
        		<option value="" >&nbsp;</option>
        		{foreach from=$category key=k item=c name=b}
        				<option value="{$c.categoryid}">{$c.category}</option>
        		{/foreach}
      		</select> 
      	</div>
        <div class="  spacer-right">
        <label for="available" class="lbl_a">{$lang.availability} : </label>
        	<select name="active" id="active-{$ParentSettings}" data-mode='EQ' class="active availability inpt_d"  data-placeholder="Select one">
        		<option value="2" >&nbsp;</option>
        		<option value="0" >{$lang.disabled}</option>
        		<option value="1" >{$lang.enabled}</option>
      		</select> 
      </div>
      	<input type="hidden"  id="modules-{$ParentSettings}" data-bind="value: module" />
        <div class="spacer-right spacer-top">
        	<input name="submit" type="button"  id="Search-{$ParentSettings}"  class="btn btn_a fSearch" value="{$lang.search}" /> 
        </div>
</div><!-- END MENU ITEM DETAILS -->
