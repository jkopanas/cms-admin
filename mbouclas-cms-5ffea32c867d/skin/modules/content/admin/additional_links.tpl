{if $mode eq "links"}
<ul id="aditional_links">
<li>{if $section ne "modify"}<img src="/images/admin/edit_16.png" width="16" height="16" align="absbottom" /> <a href="{$MODULE_FOLDER}/modify.php?id={$id}">Τροποποίηση</a>{else}<strong>Τροποποίηση</strong>{/if}</li>
<li>|</li>
<li>{if $section ne "detailed_images"}<img src="/images/admin/mediafolder.gif" width="16" height="16" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}_img.php?id={$id}">Φωτογραφίες</a>{else}<strong>Φωτογραφίες</strong>{/if}</li>
<li>|</li>
{if $eshop_module}
<li>{if $section ne "eshop_options"}<img src="/images/admin/cart_16.gif" width="16" height="16" border="0" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}_shop.php?id={$id}">{$lang.eshop_options}</a>{else}<strong>{$lang.eshop_options}</strong>{/if}</li>
<li>|</li>
{/if}
<li>{if $section ne "related"}<img src="/images/admin/redo.gif" width="16" height="16" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}_related.php?id={$id}">Σχετικά περιεχόμενα</a>{else}<strong>{$lang.related_content}</strong>{/if}</li>

{if $maps_module}
<li>|</li>
<li>{if $section ne "maps"}<img src="./images/admin/maps_16.png" width="16" height="16" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}_map.php?id={$id}">{$lang.maps}</a>{else}<strong>{$lang.maps}</strong>{/if}</li>
{/if}
</ul>
{elseif $mode eq "nav_menu"}
<div class="seperator"></div>
<div class="wrap" id="box1">
  <div class="padding"><span style="float:right; top:0; font-size:18px;">(<img src="/images/admin/add.gif" width="18" height="18" align="absbottom" /> <a href="new.php{if $item.catid}?cat={$item.catid}{/if}">Προσθήκη</a> - <a href="{$URL}/content.php?id={$item.id}&preview=1&keepThis=true&TB_iframe=true&height=700&width=900" class="thickbox" target="_blank"><img src="/images/admin/preview_16.png" width="18" height="18" border="0" /> Preview</a> )</span>
<h2>{if $nav ne "0"} {section name=w loop=$nav}
<a href="{$MODULE_FOLDER}/category_content.php?cat={$nav[w].categoryid}" class="navpath" title="{$nav[w].category}">{$nav[w].category}</a> / {/section}{/if} #{$item.id} {$item.title}</h2>
</div><!-- END PADDING -->
<div class="padding">

<ul id="menutabs" class="shadetabs">
<li><a href="#" rel="menu1" class="selected">Γρήγορη αναζήτηση</a></li>
{if $more_content}<li><a href="#" rel="menu2">Πλοήγηση</a></li>{/if}
</ul>

<div style="border:1px solid gray; width:95%; margin-bottom: 1em; padding: 10px">
<div id="menu1" class="tabcontent"><!-- START TAB -->
<form name="searchform" action="{$MODULE_FOLDER}/modify.php" method="post" style="float: left; width: 300px; margin-right: 3em;"> 
  <fieldset> 
  <legend> #ID</legend> 
  περιεχομένου 
  <input name="id" value="" size="17" type="text" id="textid" /> 
  <input name="submit" value="Εμφάνιση" type="submit" /> 
  </fieldset>
</form>

<form name="viewarc" action="{$MODULE_FOLDER}/category_content.php" method="post" style="float: left; width: 500px; margin-bottom: 1em;">
	<fieldset>
	<legend>Κατηγορία
	<select name="cat" class="postform" id="groupid">
    <option value="0">{$lang.root_category}</option>    
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $sid}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select>
	<input name="submit" value="Εμφάνιση" type="submit" /> 
	</legend></fieldset>
</form>
</div><!-- END TAB -->
<div id="menu2" class="tabcontent"><!-- START TAB -->
<form name="quick_content" action="{$MODULE_FOLDER}/{$e_FILE}" method="GET" style="float: left; width: 550px; margin-bottom: 1em;">

	<legend>Παρόμοια
	<select name="id" class="postform" id="id">
   
{section name=cat_num loop=$more_content}

  <option value="{$more_content[cat_num].id}" {if $more_content[cat_num].id eq $item.id}selected{/if}>#{$more_content[cat_num].id} - {$more_content[cat_num].title}</option>
  
{/section}

</select>
	<input name="submit" value="Εμφάνιση" type="submit" /> 
	
</legend></form>
</div><!-- END TAB -->
<div style="clear:both"></div>
</div><!-- END TABS -->
<script type="text/javascript">

var countries=new ddtabcontent("menutabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>

</div><!-- END PADDING -->
</div>
{else if $mode eq "menu"}
<div id="MoreItems" class="hidden">{include file="modules/content/search_box.tpl" mode="start_page"}</div>
<div id="in_this_section" class="spacer-bottom">
<ul>
<li><a href="javascript:void(0);" class="Slide" rel="#MoreItems">Γρήγορη αναζήτηση</a></li>
<li><img src="/images/admin/edit_16.png" width="16" height="16" align="absbottom" /> {if $section ne "modify"} <a href="{$MODULE_FOLDER}/modify.php?id={$id}">{$lang.modify}</a>{else}<strong>{$lang.modify}</strong>{/if}</li>
{if $countries|@count gt 1}
<li>
<img src="/images/admin/language_16.png" width="16" height="16" align="absbottom" />{if $section ne "translations"} <a href="{$MODULE_FOLDER}/translations.php?id={$id}">{$lang.translateContent}</a>{else}<strong>{$lang.translateContent}</strong>{/if}
</li>
{/if}
{if $eshop_module}
<li><img src="/images/admin/cart_16.gif" width="16" height="16" border="0" align="absbottom" />{if $section ne "eshop_options"} <a href="{$MODULE_FOLDER}/{$base_file}_shop.php?id={$id}">{$lang.eshop_options}</a>{else}<strong>{$lang.eshop_options}</strong>{/if}</li>
{/if}
{if $loaded_modules.quiz}
<li>{if $section ne "quiz"} <a href="item_quiz.php?id={$id}">Ερωτηματολόγια</a>{else}<strong>Ερωτηματολόγια</strong>{/if}</li>
{/if}
<li>
<img src="/images/admin/redo.gif" width="16" height="16" align="absbottom" />{if $section ne "related"} <a href="{$MODULE_FOLDER}/related_items.php?id={$id}">Σχετικά περιεχόμενα</a>{else}<strong>Σχετικά περιεχόμενα</strong>{/if}
</li>
<li class="last">
<img src="/images/admin/mediafolder.gif" width="16" height="16" align="absbottom" />{if $section ne "detailed_images"} <a href="content_modify_img.php?id={$id}">Φωτογραφίες</a>{else}<strong>Φωτογραφίες</strong>{/if}
</li>
</ul>
</div>
{/if}