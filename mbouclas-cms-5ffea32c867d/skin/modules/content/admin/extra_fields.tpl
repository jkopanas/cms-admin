{include file="modules/listings/admin/menu.tpl"}

{if $field OR $newField}

<div class="wrap seperator padding">
<div class="debug"></div>
<form name="SaveEfield">
{if $field}
<div id="zeitgeist"><h3>{$lang.image}</h3>

<div style="padding:3px; border:1px solid #000000; width:{$image_types.thumb.width}px"><img src="{$field.image}" width="75" height="75"id="ItemThumb-img" /></div>
<input name="image" type="hidden" class="SaveData"  value="{$field.image}" id="ItemThumb" />
<A href="/modules/file_manager.php?module={$CURRENT_MODULE.name}&media=image&mode=ReplaceSrc&rel=ItemThumb&width=700&height=600&tab=url" class="InsertFile">Επιλογή αρχείου</A>
</div>
{/if}
<div id="tmp" class="hidden"><div id="tmpContent"></div></div>
<h2><a href="extra_fields.php">{$lang.extra_fields}</a> / {$field.field}</h2>
<table width="730" cellpadding="5" cellspacing="5">
<tr>
<td width="250">{$lang.field_name}:</td>
<td><input name="field" type="text" class="SaveData"  value="{$field.field}" size="50"/> {if $field}<a href="#" class="translateEfield" title="{$lang.edit_all_translations}" rel="{$field.fieldid}"><img src="/images/admin/language_16.png" width="16" height="16" /> {$lang.translate}</a>{/if}</td>
</tr>
<tr>
<td>{$lang.var_name}:</td>
<td><input name="var_name" type="text" class="SaveData"  value="{$field.var_name}" size="50"/></td>
</tr>
<tr>
  <td>{$lang.description}:</td>
  <td><textarea name="settings_description" cols="50" rows="5" class="SaveData">{$field.settings.description}</textarea></td>
</tr>
<tr>
  <td>{$lang.description} ({$lang.search}):</td>
  <td><textarea name="settings_searchdes" cols="50" rows="5" class="SaveData">{$field.settings.searchdes}</textarea></td>
</tr>
<tr>
  <td>{$lang.field_type}:</td>
  <td><select name="type" id="type" class="SaveData">
          <option value="text" {if $field.type eq "text"}selected{/if}>{$lang.text}</option>
          <option value="area" {if $field.type eq "area"}selected{/if}>{$lang.textarea}</option>
          <option value="radio" {if $field.type eq "radio"}selected{/if}>{$lang.radiobtn}</option>
          <option value="hidden" {if $field.type eq "hidden"}selected{/if}>{$lang.hidden}</option>
          <option value="document" {if $field.type eq "document"}selected{/if}>{$lang.file}</option>
          <option value="image" {if $field.type eq "image"}selected{/if}>{$lang.image}</option>
          <option value="media" {if $field.type eq "media"}selected{/if}>{$lang.media}</option>
          <option value="link" {if $field.type eq "link"}selected{/if}>{$lang.link}</option>
          <option value="multiselect" {if $field.type eq "multiselect"}selected{/if}>{$lang.multiselect}</option>
      </select></td>
</tr>
<tr class="multioptions {if $field.type neq "multiselect"}hidden{/if}">
<td>{$lang.data_type}:</td>
<td>
<select name="settings_datatype" id="data_type" class="SaveData">
<option value="checkbox" {if $field.settings.datatype eq "checkbox"}selected{/if}>{$lang.checkbox}</option>
<option value="radio" {if $field.settings.datatype eq "radio"}selected{/if}>{$lang.radiobtn}</option>
<option value="dropdown" {if $field.settings.datatype eq "dropdown"}selected{/if}>{$lang.dropdown}</option>
<option value="multiple" {if $field.settings.datatype eq "multiple"}selected{/if}>Multiple</option>
</select>
</td>
</tr>

<tr class="{if $field.settings.datatype neq "dropdown"}hidden{/if}" id="range">
<td>{$lang.search_field}:</td>
<td>
<select name="settings_datatyperange" class="SaveData">
<option value="single" {if $field.settings.datatyperange eq "single"}selected="selected"{/if}>{$lang.single}</option>
<option value="range" {if $field.settings.datatyperange eq "range"}selected="selected"{/if}>{$lang.range}</option>
</select>
</td>
</tr>

<tr class="multioptions {if $field.type neq "multiselect"}hidden{/if}">
<td>{$lang.data}:</td>

<td>
<a href="#" id="addRow">{$lang.add}</a>
<table id="dataTable" class="DragDrop">

<tr><th>&nbsp;<input type="hidden" name="TableOrder" class="settings" value="" /></th><th>ID</th><th>{$lang.text}</th></tr>
{assign var=i value=0}
{if $field.settings.data}
{foreach from=$field.settings.data item=a key=k}
<tr class="dataRow" id="row{$i}">
<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
<td><input type="text" name="efieldKey_{$i}" class="SaveData" value="{$k}"></td>
<td><input type="text" name="efieldVal_{$i}" class="SaveData" value="{$a}"></td>
<td>{if $i neq 0}<a href="#" class="removeRow" rel="row{$i}">{$lang.remove}</a>{/if}</td>
</tr>
{assign var=i value=$i+1}
{/foreach}
{else}
<tr class="dataRow" id="row{$i}">
<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
<td><input type="text" name="efieldKey_{$i}" class="SaveData" value="{$k}"></td>
<td><input type="text" name="efieldVal_{$i}" class="SaveData" value="{$a}"></td>
<td></td>
</tr>
{assign var=i value=$i+1}
{/if}


</table>
<input type="hidden" value="{$i}" id="icounter"/>
</td>
</tr>

<tr>
  <td>{$lang.availability} :</td>
  <td><select name="active" class="SaveData">
          <option value="N" {if $field.active eq "N"}selected{/if}>{$lang.disabled}</option>
          <option value="Y" {if $field.active eq "Y"}selected{/if}>{$lang.enabled}</option>
      </select></td>
</tr>
<tr>
  <td>{$lang.validationRequirements}:</td>
  <td><select name="settings_validation" class="SaveData">
          <option value="0" {if $field.settings.validation eq "0"}selected{/if}>{$lang.no}</option>
          <option value="1" {if $field.settings.validation eq "1"}selected{/if}>{$lang.yes}</option>
      </select></td>
</tr>
<tr>
  <td>{$lang.allow_search} :</td>
  <td><select name="settings_search" class="SaveData">
          <option value="0" {if $field.settings.search eq "0"}selected{/if}>{$lang.no}</option>
          <option value="1" {if $field.settings.search eq "1"}selected{/if}>{$lang.yes}</option>
      </select></td>
</tr>
<tr>
  <td>{$lang.isMap} :</td>
  <td><select name="settings_isMap" class="SaveData">
          <option value="0" {if $field.settings.isMap eq "0"}selected{/if}>{$lang.no}</option>
          <option value="1" {if $field.settings.isMap eq "1"}selected{/if}>{$lang.yes}</option>
      </select></td>
</tr>
<tr>
<td>{$lang.categories} :</td>
<td><label><input type="radio" value="%" name="categoryids" class="SlideUp SaveData" rel="#categories_list" checked="checked" /> Όλες</label>&nbsp;&nbsp; <label><input type="radio" name="categoryids" class="SlideUp SaveData" value="0" rel="#categories_list" /> Καμία</label> &nbsp;&nbsp; <label><input type="radio" name="categoryids" class="SlideDown" value="0" rel="#categories_list" /> Επιλογή</label> <div class="categories_list hidden spacer-top" id="categories_list">
    <ul>
    <li><strong>Κατηγορία</strong></li>
    {foreach from=$allcategories item=a name=b key=k}{assign var=tmp value=$a.categoryid}
    <li {if $field.catids|is_array AND  $a.categoryid|array_key_exists:$field.catids}class="alternate"{/if}><input type="checkbox" name="cat-{$a.categoryid}" id="cat-{$a.categoryid}" value="{$a.categoryid}" {if $field.catids|is_array AND  $a.categoryid|array_key_exists:$field.catids}checked{/if} class="choose-cat SaveData" /> <label for="cat-{$a.categoryid}">{$a.category}</label>
    </li>
    {/foreach}
    </ul>
    </div></td>
</tr>
<tr>
<td>{$lang.groups} :</td><td>
<table>
{foreach from=$groups item=a}
<tr>
<td><input type="checkbox" class="SaveData" name="group-{$a.id}" value="{$a.id}" {if $fieldGroups|is_array AND $a.id|array_key_exists:$fieldGroups} checked="checked" {/if} /></td><td>{$a.title}</td>
</tr>
{/foreach}
</table>
</td>
</tr>
<tr>
  <td colspan="2" align="center"><input type="button" name="SaveEfield" class="button" value="Αποθήκευση" /><input type="button" name="SaveEfieldNew" class="button" value="Αποθήκευση & Νέο" /><input type="hidden" name="fieldid" value="{$field.fieldid}" class="SaveData" /> <input type="hidden" name="mode" value="SaveEfield" /></td>
  </tr>
</table>
</form>

</div><!-- END EDIT FIELD -->
{/if}
{include file="modules/content/admin/extra_fields_groups.tpl"}
{if $shown}


<div class="wrap seperator padding">
<h2><a href="extra_fields.php">{$lang.extra_fields}</a> (<a href="extra_fields.php?newField=1">Προσθήκη νέου πεδίου</a>)</h2>

<div><form name="FilterCategory" action="" method="get">
<label>Φίλτρο ανά κατηγορία</label> <SELECT name="categoryid">
<option value="">Αδιάφορο</option>
{foreach from=$allcategories item=a}
<option value="{$a.categoryid}" {if $CurrentCat eq $a.categoryid}selected{/if}>{$a.category}</option>
{/foreach}
</SELECT>
<input type="submit" value="{$lang.apply}" class="button"/>

<div style="clear:both"></div>
</form>
</div><form action="{$PHP_SELF}" method="POST" name="upsales">
<div id="debugArea"></div>
<table border="0" cellpadding="5" cellspacing="5" width="100%" class="DragDrop">
        <tr class="nodrop nodrag">
        <td colspan="5">Με τα επιλεγμένα (<span id="messages">0 επιλέχθηκαν</span> ) : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">{$lang.activate}</option>
        <option value="DeactivateChecked">{$lang.deactivate}</option>
        <option value="DeleteChecked">Διαγραφή</option>
        </select>
         <input type="button" class="button" name="SaveEfield" value="Εκτέλεση" />
        </td>
      </tr>
    <tr>
      <th scope="cols"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
	  {if $reorder}<th scope="col" width="16"><input type="hidden" value="SaveEfieldsOrder" name="action" /></th>{/if}
      <th width="250" scope="cols">{$lang.field_name}</th>
      <th scope="cols">{$lang.var_name}</th>
      <th scope="cols">{$lang.active}</th>
      <th scope="cols">{$lang.type}</th>
      </tr>
    {foreach from=$fields item=a}
    <tr {cycle values=", class='TableSubHead'"} id="Efield{$a.fieldid}">
      <td width="2%"><input name="id" type="checkbox" id="check_values" value="{$a.fieldid}" class="check-values" /></td>
      {if $reorder}<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>{/if}
      <td><a href="extra_fields.php?fieldid={$a.fieldid}">{$a.field}</a></td>
      <td>{$a.var_name}</td>
      <td>{if $a.active eq "Y"}<span class="green">{$lang.yes}</span>{else}<span class="red">{$lang.no}</span>{/if}</td>
      <td>{$a.type}</td>
      </tr>
    {/foreach}
    
    </table>
 </form>

</div>
 {/if}



<div class="seperator"></div>
<script src="/scripts/efields.js"></script>
{if $field}<script src="/scripts/AdminLanguages.js"></script>{/if}