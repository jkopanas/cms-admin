{if $group OR $newGroup}
<div class="wrap seperator padding">
<h2><a href="extra_fields.php">{$lang.extra_fields}</a> ({if $group}{$group.title}{/if}{if $newGroup}{$lang.newgroup}{/if})</h2>
<form action="{$PHP_SELF}" method="POST" name="upsales">
<div id="debugArea"></div>

<table width="730" cellpadding="5" cellspacing="5">
<tr>
<td width="250">{$lang.title} :</td>
<td><input type="text" name="title" value="{$group.title}" class="SaveData" />{if $group}<a href="#" class="translateEfieldGroup" title="{$lang.edit_all_translations}" rel="{$group.id}"><img src="/images/admin/language_16.png" width="16" height="16" /> {$lang.translate}</a>{/if}</td>
</tr>
<tr>
<td width="250">{$lang.description} :</td>
<td><input type="text" name="description" value="{$group.description}" class="SaveData" /></td>
</tr>
<tr>
<td>{$lang.extra_fields}:</td>
<td>
<div class="float-left">
<table  id="availableItems">
<tr><td colspan="3">{$lang.availitems}</td></tr>
{foreach from=$efields key=k item=a}
{if $group.items|is_array}
{if !$a.fieldid|array_key_exists:$group.items}
    <tr id="{$group.id}_{$a.fieldid}">
        <td></td>
        <td><input class="clickableItem SaveData" type="checkbox" name="items_{$a.fieldid}" value="{$a.fieldid}" {if $group}{if $a.fieldid|array_key_exists:$group.items}CHECKED{/if}{/if}   /></td>
        <td>{$a.field|truncate:20:"...":true}</td>
    </tr>

{/if}
{else}
	 <tr id="{$group.id}_{$a.fieldid}">
        <td></td>
        <td><input class="clickableItem SaveData" type="checkbox" name="items_{$a.fieldid}" value="{$a.fieldid}" {if $group}{if $a.fieldid|array_key_exists:$group.items}CHECKED{/if}{/if}   /></td>
        <td>{$a.field|truncate:20:"...":true}</td>
    </tr>
{/if}
{/foreach}
</table>
</div>
<div class="float-left">
<table class="DragDrop" id="groupItems">
<tr><td colspan="3">{$lang.groupitems}</td></tr>
{foreach from=$group.items key=k item=a}
    <tr id="{$group.id}_{$efields.$k.fieldid}">
        <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
        <td><input class="clickableItem SaveData" type="checkbox" name="items_{$efields.$k.fieldid}" value="{$efields.$k.fieldid}" {if $group}{if $efields.$k.fieldid|array_key_exists:$group.items}CHECKED{/if}{/if}   /></td>
        <td>{$efields.$k.field|truncate:20:"...":true}</td>
    </tr>
{/foreach}
</table>
</div>
</td>
</tr>
<tr>
<td>{$lang.theme}:</td>
<td>
<table>
{foreach from=$themes item=a}
<tr>
<td><input type="checkbox" class="SaveData" name="theme_{$a.id}" value="{$a.id}" {$a.checked}  /></td>
<td>{$a.title}</td>
</tr>
{/foreach}
</table>
</td>
</tr>
<tr>
  <td colspan="2" align="center"><input type="button" name="SaveEfield" class="button" value="{$lang.save}" />
  <input type="hidden" name="groupid" value="{$group.id}" class="SaveData" />
  <input type="hidden" name="module" value="{$CURRENT_MODULE.name}" class="SaveData" /> 
  <input type="hidden" name="mode" value="SaveEfieldGroup" />
  <input type="hidden" value="SaveGroupsItemsOrder" name="action" /></td>
</tr>
</table>
</form>
</div>
{if $group}<script src="/scripts/AdminLanguages.js"></script>{/if}
{/if}

{if $shown}
<div class="wrap seperator padding">
<h2><a href="extra_fields.php">{$lang.extra_fields}</a> (<a href="extra_fields.php?newGroup=1">{$lang.add_new_group}</a>)</h2>
<form action="{$PHP_SELF}" method="POST" name="upsales">
<div id="debugArea"></div>

<table border="0" cellpadding="5" cellspacing="5" width="100%" class="DragDrop">
        <tr class="nodrop nodrag">
        <td colspan="5">{$lang.withSelectedDo} (<span id="messages">0 {$lang.selected}</span> ) : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="DeleteCheckedGroups">{$lang.delete}</option>
        </select>
        <input type="button" class="button" name="SaveEfield" value="Εκτέλεση" />
        </td>
      </tr>
    <tr>
      <th scope="cols"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
	  <th scope="col" width="16"><input type="hidden" value="SaveEfieldsGroupsOrder" name="action" /></th>
      <th width="250" scope="cols">{$lang.field_name}</th>
      <th scope="cols">{$lang.description}</th>
      <th scope="cols">{$lang.numberOfElements}</th>
    </tr>
    {foreach from=$groups item=a}
    <tr {cycle values=", class='TableSubHead'"} id="group_{$a.id}">
      <td width="2%"><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /></td>
      <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
      <td><a href="extra_fields.php?groupid={$a.id}">{$a.title}</a></td>
      <td>{$a.description}</td>
      <td>{$a.count}</td>
      
      </tr>
    {/foreach}
    
    </table>
 </form>

</div>
{/if}
<div id="tmp" class="hidden"><div id="tmpContent"></div></div>