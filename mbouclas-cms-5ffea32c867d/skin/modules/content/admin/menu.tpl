<ul id="adminmenu2">
	<li><a href="index.php" {if $submenu eq "main"}class="current1"{/if}>{$lang.home}</a></li>
	<li><a href="new.php" {if $submenu eq "add"}class="current1"{/if}>{$lang.add_new}</a></li>
	<li><a href="search.php" {if $submenu eq "search"}class="current1"{/if}>{$lang.search}</a></li>
	<li><a href="search.php" {if $submenu eq "modify"}class="current1"{/if}>{$lang.modify}</a></li>
    <li><a href="categories.php" {if $submenu eq "categories"}class="current1"{/if}>{$lang.categories}</a></li>
    <li><a href="commonCategoriesTree.php" {if $submenu eq "commonCategoriesTree"}class="current1"{/if}>{$lang.commonCategoriesTree}</a></li>
    <li><a href="extra_fields.php" {if $submenu eq "extra_fields"}class="current1"{/if}>{$lang.extra_fields}</a></li>
	<li><a href="settings.php" {if $submenu eq "settings"}class="current1"{/if}>{$lang.settings}</a></li>
</ul>