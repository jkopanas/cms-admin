{if $type}<input type="hidden" name="type" value="{$type}" class="settings" />{/if}
<input type="hidden" name="WIDTH" value="{$WIDTH}" class="settings" />
<input type="hidden" name="form_name" value="{$form_name}" class="settings" />
<input type="hidden" class="settings" name="SavedData" value="{$SavedData}" />

{if !$mode OR $mode eq "FeaturedContent" OR $mode eq "RelatedContent"}
<h2>{$lang.searchResults}</h2>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
      <tr>
        <td colspan="7">{if $list.total gt $posted_data.results_per_page}{include file="modules/content/pagination.tpl" mode="ajax"}{/if}</td>
      </tr>
     <tr>
       <th scope="col">#ID</th>
       <th scope="col">{$lang.title}</th>
       <th width="18">&nbsp;</th>
     </tr>
     {foreach from=$items item=a}
     <tr class="result_item_row {cycle values='alternate,'}" id="Row{$a.id}">
       <td>{$a.id}</td>
       <td><a href="modify.php?id={$a.id}">{$a.title}</a></td>
       <td><a href="javascript:void(0);" class="AddItem" rel="{$a.id}" title="{$a.title}"><img src="/images/admin/add.gif" width="18" height="18" border="0" /></a></td>
     </tr>
     {/foreach}
   </table>
   {elseif $mode eq "QuickSearch"}
   {if $list.total gt $posted_data.results_per_page}{include file="modules/content/pagination.tpl" mode="ajax"}{/if}
   {include file="modules/content/admin/display_content_table.tpl" content=$items  form_name="latest_content_form" page="index.php" MODULE_FOLDER=$CONTENT_MODULE_FOLDER}
   {elseif $mode eq "menuItems"}
   <h2>{$lang.searchResults}</h2>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
      <tr>
        <td colspan="7">{if $list.total gt $posted_data.results_per_page}{include file="modules/content/pagination.tpl" mode="ajax"}{/if}</td>
      </tr>
     <tr>
       <th scope="col">#ID</th>
       <th scope="col">{$lang.title}</th>
       <th width="18">&nbsp;</th>
     </tr>
     {foreach from=$items item=a}
     <tr class="result_item_row {cycle values='alternate,'}" id="Row{$a.id}">
       <td>{$a.id}</td>
       <td><a href="modify.php?id={$a.id}">{$a.title}</a></td>
       <td><a href="javascript:void(0);" class="saveMenuItem" rel="itemid={$a.id}&module={$data.module}&type=item&menuid={$data.menuid}" title="{$a.title}"><img src="/images/admin/add.gif" width="18" height="18" border="0" /></a></td>
     </tr>
     {/foreach}
   </table>
   
   {else}
   <h2>{$lang.existing_pages}</h2>
   <table width="100%" border="0" cellspacing="5" cellpadding="5" class="DragDrop">
     <tr class="nodrop nodrag">
       <th scope="col"><input type="hidden" name="TableOrder" class="settings" value="" />&nbsp;</th>
       <th scope="col">#ID</th>
       <th scope="col">{$lang.title}</th>
       <th width="32">{$lang.type}</th>
       <th width="32">&nbsp;</th>
     </tr>
     {if $items}
     {foreach from=$items item=a}
     <tr class="item_row {cycle values='alternate,'}" id="Order{if $a.type eq "cat"}{$a.item.categoryid}{else}{$a.item.id}{/if}">
      <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
       <td>{if $a.type eq "cat"}{$a.item.categoryid}{else}{$a.item.id}{/if}</td>
       <td>{if $a.type eq "cat"}<a href="categories.php?cat={$a.item.categoryid}">{$a.item.category}</a>{else}<a href="modify.php?id={$a.item.id}">{$a.item.title}</a>{/if}</td>
       <td>{if $a.type eq "itm"}Σελίδα{else}{$lang.category}{/if}</td>
       <td><a href="javascript:void(0);" class="delete-item" rel="{$a.itemid}{if $a.type}-cat{/if}" title="{$a.item.title}"><img src="/images/admin/delete_32.png" width="32" height="32" border="0" /></a></td>
     </tr>
     {/foreach}
     {else}
     <tr>
       <td colspan="7" align="center"><em>{$lang.no_content_in_list}</em></td>
     </tr>
     {/if}
   </table>
   {/if}