{include file="modules/content/admin/menu.tpl"}
{include file="modules/content/admin/additional_links.tpl" mode="menu" base_file="content_modify"}

{if $error_list}{include file="common/error_list.tpl"}{/if}
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}

<div class="seperator">
  <h2>{if $nav ne "0"} {section name=w loop=$nav}
  <a href="{$MODULE_FOLDER}/category_content.php?cat={$nav[w].categoryid}" class="navpath" title="{$nav[w].category}">{$nav[w].category}</a> / {/section}{/if} <a href="modify.php?id={$item.id}">#{$item.id} {$item.title}</a> / {$lang.translations}</h2>
</div>
<div class="seperator"></div>


<div id="ColumnLeft" class="float-left spacer-right">
<form>
<input type="hidden" class="originalData" value="{$item.id}" name="id" />
<input type="hidden" class="originalData" value="{$item.title}" name="title" />
<input type="hidden" class="originalData" value="{$item.permalink}" name="permalink" />
<input type="hidden" class="originalData" value='{$item.description}' name="description" />
<input type="hidden" class="originalData" value='{$item.description_long}' name="description_long" />
<div class="debug"></div>
<div id="tab-container" class="clear">
{foreach from=$availableLanguages item=a name=b}
<div id="tab{$smarty.foreach.b.iteration}" class="tabcontent">
<h3>{$lang.defaultLanguage} : {$defaultLanguage.details.country} - {$lang.NowEditing} {$a.country}</h3>
<div class="seperator"></div>
<div class="wrap spacer-bottom">
<h2>{$lang.title}</h2>
<div class="padding">
<input name="content@title-{$a.code}" type="text" id="title-{$a.code}" size="45" value="{$a.item.title.translation}" class="ContentTitle languageTranslationBox">
    </div><!-- END PADDING -->
    
    </div><!-- END BOX -->
<div class="wrap spacer-bottom">
<h2>{$lang.permalink}</h2>
<div class="padding">
<input name="content@permalink-{$a.code}" type="text" id="permalink-{$a.code}" size="45" value="{$a.item.permalink.translation}" class="ContentTitle languageTranslationBox">
    </div><!-- END PADDING -->
    
    </div><!-- END BOX -->
    <div class="wrap spacer-bottom">
<h2>{$lang.description}</h2>
<div class="padding">
<textarea name="content@description-{$a.code}" cols="80" rows="5" id="description-{$a.code}" class="languageTranslationBox">{$a.item.description.translation}</textarea><br /><a href="javascript:void(0);" class="open-editor" rel="description-{$a.code}">{$lang.advanced_editor}</a>
</div><!-- END PAADDING -->
</div><!-- END BOX -->
<div class="wrap spacer-bottom">
<h2>{$lang.long_desc}</h2>
<div class="padding">
<textarea name="content@description_long-{$a.code}" cols="80" rows="10" id="description_long-{$a.code}" class="languageTranslationBox">{$a.item.description_long.translation}</textarea> <br /><a href="javascript:void(0);" class="open-editor" rel="description_long-{$a.code}">{$lang.advanced_editor}</a>
</div><!-- END PAADDING -->
</div><!-- END BOX -->


    </div><!-- END TAB --> 
{/foreach}
</div><!-- END TAB CONTAINER -->
<div align="center"><input type="button" value="{$lang.save}" class="SaveTranslations button" /></div>
</form>
</div><!-- END LEFT -->
<div id="ColumnRight" class="float-left spacer-left">
<div class="wrap spacer-bottom">
<h2>{$lang.available_languages}</h2>
<div class="padding">
<ul id="languageTabs"  class="idTabs">
{foreach from=$availableLanguages item=a name=b}
<li><a  href="#tab{$smarty.foreach.b.iteration}" rel="tab{$smarty.foreach.b.iteration}">{$a.country}</a></li>
{/foreach}
<li><input type="button" value="{$lang.save}" class="SaveTranslations button" /></li>
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->

<div class="wrap spacer-bottom">
<h2>{$lang.defaultTranslation}</h2>
<div class="padding">
<textarea cols="33" rows="7" id="defaultTranslationTextArea"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->
</div><!-- END RIGHT -->
<div class="clear"></div>

<script src="/scripts/AdminLanguages.js"></script>