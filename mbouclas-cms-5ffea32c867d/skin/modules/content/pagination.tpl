{if $mode eq "search_results_admin"}
<table border=0 cellpadding=0 cellspacing="0" class="pagination">
    <tr height=20>
      <td colspan="6">{$lang.from} <font color="#FF0000">{$list.from}</font> {$lang.to} <font color="#FF0000">{$list.to}</font> {$lang.of_total} {$list.total}</td>
    </tr>
    <tr height=14>
      <td>{$lang.result_pages} : &nbsp;</td>
      {if $navigation.first}
      <td valign=middle><a href="{$MODULE_FOLDER}/search.php?search=1&page={$navigation.first}" title="{$lang.first}"><img src="{$ImagesDir}/larrow_2.gif" border="0" alt="{$lang.first}" title="{$lang.first}"></a></td>
      {/if} {if $navigation.previous}
      <td valign=middle><a href="{$MODULE_FOLDER}/search.php?search=1&page={$navigation.previous}" title="{$lang.previous}"><img src="{$ImagesDir}/larrow.gif" border="0" alt="{$lang.previous}" title="{$lang.previous}"></a></td>
      {/if} 
	  <td><table height="20"><tr>
	  {section name=p loop=$num_links} 
	  {if $num_links[p].page eq $PAGE}
      <td width=17 background="{$ImagesDir}/page.gif" align=center class="pagination"><font color="#FF0000"><b>{$num_links[p].page}</b></font></td>
      {else}
      <td width=17 background="{$ImagesDir}/page.gif" align=center><a href="{$MODULE_FOLDER}/search.php?search=1&page={$num_links[p].page}" class="pagination" title="{$lang.page} : {$num_links[p].page}">{$num_links[p].page}</a></td>
      {/if} 
	  {/section} 
	  </tr></table>
	  </td>
	  {if $navigation.next}
      <td valign=middle><a href="{$MODULE_FOLDER}/search.php?search=1&page={$navigation.next}" title="{$lang.next}"><img src="{$ImagesDir}/rarrow.gif" border="0" alt="{$lang.next}" title="{$lang.next}"></a></td>
      {/if} {if $navigation.final}
      <td valign=middle><a href="{$MODULE_FOLDER}/search.php?search=1&page={$navigation.final}" title="{$lang.final}{if $smarty.get.id}&id={$smarty.get.id}{/if}"><img src="{$ImagesDir}/rarrow_2.gif" border="0" alt="{$lang.last}" title="{$lang.last}"></a></td>
      {/if}
  </tr>
</table>
{elseif $mode eq "ajax"}
<table border=0 cellpadding=0 cellspacing="0" class="pagination">
<tr height=20>
      <td colspan="6" align="left"><strong>{$list.from}</strong> {$lang.to}<strong> {$list.to}</strong> {$lang.from} <strong>{$list.total}</strong> {$lang.lbl_results} | {$lang.page} <strong>{$PAGE}</strong> {$lang.from} <strong>{$total_pages}</strong></td>
  </tr>
  {if $total_pages gt 1}
    <tr height=14>
      <td>{$lang.result_pages} : &nbsp;</td>
      {if $navigation.first}
      <td valign=middle><a href="#{$target|default:"results"}" title="{$lang.first}" class="{$class|default:"page"}" rel="{$navigation.first}">{$lang.first}</a></td>
      {/if} {if $navigation.previous}
      <td valign=middle><a href="#{$target|default:"results"}" title="{$lang.previous}" class="{$class|default:"page"}" rel="{$navigation.previous}">{$lang.previous}</a></td>
      {/if} 
	  <td><table height="20"><tr>
	  {section name=p loop=$num_links} 
	  {if $num_links[p].page eq $PAGE}
      <td width=17 align=center class="pagination"><font color="#FF0000"><b>{$num_links[p].page}</b></font></td>
      {else}
      <td width=17 align=center><a href="#{$target|default:"results"}" title="{$lang.page} : {$num_links[p].page}" class="{$class|default:"page"}" id="page-{$num_links[p].page}" rel="{$num_links[p].page}">{$num_links[p].page}</a></td>
      {/if} 
	  {/section} 
	  </tr></table>
	  </td>
	  {if $navigation.next}
      <td valign=middle><a href="#{$target|default:"results"}" title="{$lang.next}" class="{$class|default:"page"}" rel="{$navigation.next}">{$lang.next}</a></td>
      {/if} {if $navigation.final}
      <td valign=middle><a href="#{$target|default:"results"}" title="{$lang.final}{if $smarty.get.id}&id={$smarty.get.id}{/if}" class="{$class|default:"page"}" rel="{$navigation.final}">{$lang.last}</a></td>
      {/if}
  </tr>
  {/if}
</table>
{elseif $mode eq "languagesPaginate"}
<table border=0 cellpadding=0 cellspacing="0" class="pagination">
<tr height=20>
      <td colspan="6" align="left"><strong>{$list.from}</strong>{$lang.to}<strong>{$list.to}</strong>{$lang.from}<strong>{$list.total}</strong> {$lang.lbl_results} |  {$lang.page} <strong>{$PAGE}</strong> {$lang.from}<strong>{$total_pages}</strong></td>
  </tr>
  {if $total_pages gt 1}
    <tr height=14>
      <td>{$lang.result_pages} : &nbsp;</td>
      {if $navigation.first}
      <td valign=middle><a href="javascript:void(0);" title="{$lang.first}" class="{$class|default:"paginate"}" rel="{$navigation.first}">{$lang.first}</a></td>
      {/if} {if $navigation.previous}
      <td valign=middle><a href="javascript:void(0);" title="{$lang.previous}" class="{$class|default:"paginate"}" rel="{$navigation.previous}">{$lang.previous}</a></td>
      {/if} 
	  <td><table height="20"><tr>
	  {section name=p loop=$num_links} 
	  {if $num_links[p].page eq $PAGE}
      <td width=17 align=center class="pagination"><font color="#FF0000"><b>{$num_links[p].page}</b></font></td>
      {else}
      <td width=17 align=center><a href="javascript:void(0);" title="{$lang.page} : {$num_links[p].page}" class="{$class|default:"paginate"}" id="page-{$num_links[p].page}" rel="{$num_links[p].page}">{$num_links[p].page}</a></td>
      {/if} 
	  {/section} 
	  </tr></table>
	  </td>
	  {if $navigation.next}
      <td valign=middle><a href="javascript:void(0);" title="{$lang.next}" class="{$class|default:"paginate"}" rel="{$navigation.next}">{$lang.next}</a></td>
      {/if} {if $navigation.final}
      <td valign=middle><a href="javascript:void(0);" title="{$lang.final}{if $smarty.get.id}&id={$smarty.get.id}{/if}" class="{$class|default:"paginate"}" rel="{$navigation.final}">{$lang.last}</a></td>
      {/if}
  </tr>
  {/if}
</table>
{elseif $mode eq "tags"}
<div class="pagination">
<ul>
<li><a href="{$URL}/tag/{$tag}/{$navigation.first}/" title="{$lang.first}" class="prevnext{if !$navigation.first} disablelink{/if}">&laquo; {$lang.first}</a></li>
<li><a href="{$URL}/tag/{$tag}/{$navigation.previous}/" title="{$lang.previous}" class="prevnext {if !$navigation.previous}disablelink{/if}">&laquo; {$lang.previous}</a></li>
{section name=p loop=$num_links}
<li><a href="{$URL}/tag/{$tag}/{$num_links[p].page}/" title="{$lang.page} : {$num_links[p].page}" {if $num_links[p].page eq $PAGE}class="currentpage"{/if}>{$num_links[p].page}</a></li>
{/section}
<li><a href="{$URL}/tag/{$tag}/{$navigation.next}/" title="{$lang.next}" class="prevnext{if !$navigation.next} disablelink{/if}">{$lang.next} &raquo;</a></li>
<li><a href="{$URL}/tag/{$tag}/{$navigation.final}/" title="{$lang.last}" class="prevnext{if !$navigation.final} disablelink{/if}">{$lang.last} &raquo;</a></li>
</ul>
</div>
{elseif $mode eq "admin"}
<table border=0 cellpadding=0 cellspacing="0" class="pagination">
<tr height=20>
      <td colspan="6" align="left">{$lang.from} <font color="#FF0000">{$list.from}</font> {$lang.to} <font color="#FF0000">{$list.to}</font> {$lang.of_total} {$list.total}</td>
  </tr>
    <tr height=14>
      <td>{$lang.result_pages} : &nbsp;</td>
      {if $navigation.first}
      <td valign=middle><a href="{$PHP_SELF}?page={$navigation.first}{if $cat}&cat={$cat.categoryid}{/if}" title="{$lang.first}"></a></td>
      {/if} {if $navigation.previous}
      <td valign=middle><a href="{$PHP_SELF}?page={$navigation.previous}{if $cat}&cat={$cat.categoryid}{/if}" title="{$lang.previous}"><img src="/images/first.gif" width="12" height="11" border="0" /></a></td>
      {/if} 
	  <td><table height="20"><tr>
	  {section name=p loop=$num_links} 
	  {if $num_links[p].page eq $PAGE}
      <td width=17 align=center class="pagination"><font color="#FF0000"><b>{$num_links[p].page}</b></font></td>
      {else}
      <td width=17 align=center><a href="{$PHP_SELF}?page={$num_links[p].page}{if $cat}&cat={$cat.categoryid}{/if}" class="pagination" title="{$lang.page} : {$num_links[p].page}">{$num_links[p].page}</a></td>
      {/if} 
	  {/section} 
	  </tr></table>
	  </td>
	  {if $navigation.next}
      <td valign=middle><a href="{$PHP_SELF}?page={$navigation.next}{if $cat}&cat={$cat.categoryid}{/if}" title="{$lang.next}" class="pagination"><img src="/images/last.gif" width="12" height="11" border="0" /></a></td>
      {/if} {if $navigation.final}
      <td valign=middle><a href="{$PHP_SELF}?page={$navigation.final}{if $cat}&cat={$cat.categoryid}{/if}" title="{$lang.final}{if $smarty.get.id}&id={$smarty.get.id}{/if}" class="pagination"></a></td>
      {/if}
  </tr>
</table
{else}

><ul id="pagination-flickr">
      {if $navigation.previous}<li class="next"><a href="/{$prefix}/{$cat.alias}/{$navigation.previous}.html">&laquo; {$lang.previous}</a></li>{/if}
      {foreach item=a from=$num_links} 
      {if $a.page eq $PAGE}<li class="active">{$a.page}</li>{else}<li><a href="/{$prefix}/{$cat.alias}/{$a.page}.html" title="{$a.page}">{$a.page}</a></li>{/if}
      {/foreach}
      {if $navigation.next}<li class="next"><a href="/{$prefix}/{$cat.alias}/{$navigation.next}.html">{$lang.next} &raquo;</a></li>{/if}
  </ul>
 {/if}
  