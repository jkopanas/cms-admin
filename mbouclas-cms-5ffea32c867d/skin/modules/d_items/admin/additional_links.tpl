{if $mode eq "links"}
<ul id="aditional_links">
<li>{if $section ne "modify"}<img src="/images/admin/edit_16.png" width="16" height="16" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}.php?id={$id}">{$lang.modify}</a>{else}<strong>{$lang.modify}</strong>{/if}</li>
<li>|</li>
<li>{if $section ne "detailed_images"}<img src="/images/admin/mediafolder.gif" width="16" height="16" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}_img.php?id={$id}">{$lang.media}</a>{else}<strong>{$lang.media}</strong>{/if}</li>
<li>|</li>
{if $eshop_module}
<li>{if $section ne "eshop_options"}<img src="/images/admin/cart_16.gif" width="16" height="16" border="0" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}_shop.php?id={$id}">{$lang.eshop_options}</a>{else}<strong>{$lang.eshop_options}</strong>{/if}</li>
<li>|</li>
{/if}
<li>{if $section ne "related"}<img src="/images/admin/redo.gif" width="16" height="16" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}_related.php?id={$id}">{$lang.upselling_links}</a>{else}<strong>{$lang.upselling_links}</strong>{/if}</li>
<li>|</li>
<li>{if $section ne "options"}<img src="/images/admin/optiongroup.gif" width="16" height="16" border="0" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}_opt.php?id={$id}">{$lang.product_options}</a>{else}<strong>{$lang.product_options}</strong>{/if}</li>
{if $maps_module}
<li>|</li>
<li>{if $section ne "maps"}<img src="./images/admin/maps_16.png" width="16" height="16" align="absbottom" /> <a href="{$MODULE_FOLDER}/{$base_file}_map.php?id={$id}">{$lang.maps}</a>{else}<strong>{$lang.maps}</strong>{/if}</li>
{/if}
</ul>
{elseif $mode eq "nav_menu"}
<TABLE class="open_close_tab seperator">
      <TR>
        <TD id="close1" style="display: none; cursor: hand;" onClick="visibleBox('1')"><IMG src="/skin/images/plus.gif" border="0" align="absmiddle" alt="Click to open"></TD>
        <TD id="open1" style="cursor: hand;" onClick="visibleBox('1')"><IMG src="/skin/images/minus.gif" border="0" align="absmiddle" alt="Click to close"></TD>
        <TD><A href="javascript:void(0);" onClick="visibleBox('1')"><B>Show - Hide</B></A></TD>

      </TR>
      </TABLE>
<div class="wrap" id="box1">
<div class="padding"><span style="float:right; top:0; font-size:18px;">(<img src="/images/admin/add.gif" width="18" height="18" align="absbottom" /> <a href="items_new.php{if $item.catid}?cat={$item.catid}{/if}">add new</a>)</span>
<h2><a href="{$MODULE_FOLDER}/">{$lang.items}</a> {if $nav ne "0"}/ {section name=w loop=$nav}
<a href="{$MODULE_FOLDER}/category_items.php?cat={$nav[w].categoryid}" class="navpath" title="{$nav[w].category}">{$nav[w].category}</a> / {/section}{/if} {$item.title}</h2>
</div><!-- END PADDING -->
<div class="padding">
{if $item.id}<div style="float:right">{include file="modules/d_items/admin/additional_links.tpl" mode="links"}</div>{/if}

<ul id="menutabs" class="shadetabs">
<li><a href="#" rel="menu1" class="selected">Quick find item</a></li>
{if $more_items}<li><a href="#" rel="menu2">Navigation</a></li>{/if}
</ul>

<div style="border:1px solid gray; width:95%; margin-bottom: 1em; padding: 10px">
<div id="menu1" class="tabcontent"><!-- START TAB -->
<form name="searchform" action="{$MODULE_FOLDER}/items_modify.php" method="post" style="float: left; width: 300px; margin-right: 3em;"> 
  <fieldset> 
  <legend>{$lang.product} #ID</legend> 
  <input name="id" value="" size="17" type="text" id="textid"> 
  <input name="submit" value="{$lang.display}" type="submit"> 
  </fieldset>
</form>

<form name="viewarc" action="{$MODULE_FOLDER}/category_items.php" method="post" style="float: left; width: 500px; margin-bottom: 1em;">
	<fieldset>
	<legend>Filter Category</legend>
<select name="cat" class="postform" id="groupid">
    <option value="0">{$lang.root_category}</option>    
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $sid}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select>
	<input name="submit" value="{$lang.display}" type="submit"> 
	</fieldset>
</form>
</div><!-- END TAB -->
<div id="menu2" class="tabcontent"><!-- START TAB -->
<form name="quick_items" action="{$MODULE_FOLDER}/{$e_FILE}" method="GET" style="float: left; width: 550px; margin-bottom: 1em;">
	<fieldset>
	<legend>Similar items</legend>
    <select name="id" class="postform" id="id">
   
{section name=cat_num loop=$more_items}

  <option value="{$more_items[cat_num].id}" {if $more_items[cat_num].id eq $item.id}selected{/if}>#{$more_items[cat_num].id} - {$more_items[cat_num].title}</option>
  
{/section}

</select>
	<input name="submit" value="{$lang.display}" type="submit"> 
	</fieldset>
</form>
</div><!-- END TAB -->
<div style="clear:both"></div>
</div><!-- END TABS -->
<script type="text/javascript">

var countries=new ddtabcontent("menutabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>

</div><!-- END PADDING -->
</div>
{/if}