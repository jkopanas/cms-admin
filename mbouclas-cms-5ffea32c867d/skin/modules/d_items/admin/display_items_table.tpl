{if $type eq "edit"}
<table cellpadding="0" cellspacing="0" class="widefat">
      <tr>
        <td colspan="9"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">#ID</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
<th scope="col">{$lang.provider}</th>
<th width="78" scope="col">&nbsp;</th>
</tr>

{section name=w loop=$items}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$items[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$items[w].id}</strong></td>
            <td><a href="items_modify.php?id={$items[w].id}">{$items[w].title}</a></td>
            <td><a href="category_items.php?cat={$items[w].catid}">{$items[w].category|default:$lang.root_category}</a></td>
            <td>{$items[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $items[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td>{if $ADMIN eq 1}<a href="users_items.php?id={$items[w].uid}">{$items[w].user.uname}</a>{else}{$items[w].provider}{/if}</td>
            <td><a href="{$MODULE_FOLDER}/items_modify_img.php?id={$items[w].id}" title="Media Files for {$items[w].title}"><img src="/images/admin/mediafolder.gif" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/items_modify_opt.php?id={$items[w].id}" title="Product options for {$items[w].title}"><img src="/images/admin/optiongroup.gif" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/items_modify_related.php?id={$items[w].id}" title="Related items for {$items[w].title}"><img src="/images/admin/redo.gif" width="16" height="16" border="0" /></a> <a href="{$URL}/product.php?id={$items[w].id}&keepThis=true&TB_iframe=true&height=700&width=900" title="Preview {$items[w].title}" class="thickbox"><img src="/images/admin/preview_16.png" width="16" height="16" border="0" /></a></td>
            
  </tr>
		  {/section}
      <tr>
        <td colspan="9"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
  <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "dashboard"}
<table cellpadding="0" cellspacing="0" class="widefat" {if $WIDTH}style="width:{$WIDTH}px"{/if}>
      <tr>
        <td colspan="7"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">#ID</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
<th width="78" scope="col">&nbsp;</th>
</tr>

{section name=w loop=$items}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$items[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$items[w].id}</strong></td>
            <td><a href="{$MODULE_FOLDER}/items_modify.php?id={$items[w].id}">{$items[w].title}</a></td>
            <td>{$items[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $items[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td><a href="{$MODULE_FOLDER}/items_modify_img.php?id={$items[w].id}" title="Media Files for {$items[w].title}"><img src="/images/admin/mediafolder.gif" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/items_modify_opt.php?id={$items[w].id}" title="Product options for {$items[w].title}"><img src="/images/admin/optiongroup.gif" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/items_modify_related.php?id={$items[w].id}" title="Related items for {$items[w].title}"><img src="/images/admin/redo.gif" width="16" height="16" border="0" /></a> <a href="{$URL}/product.php?id={$items[w].id}&keepThis=true&TB_iframe=true&height=700&width=900" title="Preview {$items[w].title}" class="thickbox"><img src="/images/admin/preview_16.png" width="16" height="16" border="0" /></a></td>
            
    </tr>
		  {/section}
      <tr>
        <td colspan="7"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
  <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "editor"}
<table cellpadding="3" cellspacing="0" class="widefat">
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">#ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.availability}</th>
</tr>

{section name=w loop=$items}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$items[w].id}]"  type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$items[w].id}</strong></td>
            <td><img src="{$URL}/{$items[w].image}" /></td>
            <td><input type="text" name="items_title-{$items[w].id}" value="{$items[w].title}" onkeyup="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'" /></td>
            <td><select name="categoryid-{$items[w].id}"  onchange="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'">
                       
{section name=cat_num loop=$allcategories}

              <option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $items[w].catid}selected{/if}>{$allcategories[cat_num].category}</option>
              
{/section}

            </select>            </td>
    <td align="center"><select name="active-{$items[w].id}"  onchange="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'">
        <option value="0" {if $items[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $items[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
    </select></td>
    </tr>
		  {/section}
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "search_results_admin"}
<table cellpadding="3" cellspacing="0" class="widefat">
      <tr>
        <td colspan="9">{include file="modules/d_items/pagination.tpl"}</td>
      </tr>

      <tr>
        <th colspan="7" scope="col"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />       </th>
      </tr>
  <tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">#ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">{$lang.pos}</th>
  <th scope="col">{$lang.title}</th>
{if $eshop_module}
<th scope="col">{$lang.price}</th>
{/if}
{if $locations_module}
<th scope="col">{$lang.location}</th>
{/if}
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.availability}</th>
<th width="78" scope="col">&nbsp;</th>
</tr>

{section name=w loop=$items}
          <tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$items[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong><a href="{$MODULE_FOLDER}/items_modify.php?id={$items[w].id}">{$items[w].id}</a></strong></td>
            <td><a href="{$MODULE_FOLDER}/items_modify.php?id={$items[w].id}"><img src="{$URL}/{$items[w].image}" /></a></td>
            <td><input name="orderby-{$items[w].id}" type="text" id="orderby-{$items[w].id}" size="3" value="{$items[w].orderby}" onkeyup="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'"  /></td>
            <td><input type="text" name="items_title-{$items[w].id}" value="{$items[w].title}" onkeyup="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'" /></td>
            {if $eshop_module}
            <td><input type="text" name="eshop_price-{$items[w].id}" id="eshop_price-{$items[w].id}" value="{$items[w].eshop.price}"  onkeyup="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'"/></td>
            {/if}
            {if $locations_module}
            <td><select name="locations_id-{$items[w].id}" id="locations_id-{$items[w].id}" onchange="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'">
            {section name=a loop=$all_locations}
            <option value="{$all_locations[a].id}" {if $items[w].location.categoryid eq $all_locations[a].categoryid}selected{/if}>{$all_locations[a].category}</option>
            {/section}
            </select></td>
            {/if}
            <td><select name="categoryid-{$items[w].id}" onchange="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'">         
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $items[w].catid}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select></td>
            <td align="center"><select name="active-{$items[w].id}" onchange="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'">
        <option value="0" {if $items[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $items[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select></td>
            <td><a href="{$MODULE_FOLDER}/items_modify_img.php?id={$items[w].id}" title="Media Files for {$items[w].title}"><img src="/images/admin/mediafolder.gif" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/items_modify_opt.php?id={$items[w].id}" title="Product options for {$items[w].title}"><img src="/images/admin/optiongroup.gif" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/items_modify_related.php?id={$items[w].id}" title="Related items for {$items[w].title}"><img src="/images/admin/redo.gif" width="16" height="16" border="0" /></a> <a href="{$URL}/product.php?id={$items[w].id}&keepThis=true&TB_iframe=true&height=700&width=900" title="Preview {$items[w].title}" class="thickbox"><img src="/images/admin/preview_16.png" width="16" height="16" border="0" /></a></td>
    </tr>
		  {/section}
      <tr>
        <td colspan="10"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}{if $search AND !$QUERY_STRING}?search=1{elseif $search AND $QUERY_STRING}&search=1{/if}">
       <input type="hidden" name="mode" id="mode" value="" /></td>
      </tr>
      <tr><td colspan="10">{include file="modules/d_items/pagination.tpl"}</td></tr>
</table>
{else}
<table cellpadding="0" cellspacing="0" class="widefat">
<thead>
<tr>
  <th scope="col">#ID</th>
<th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
<th scope="col">{$lang.provider}</th>
</tr>
</thead>
{section name=w loop=$items}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><strong>{$items[w].id}</strong></td>
            <td><a href="items_modify.php?id={$items[w].id}">{$items[w].title}</a></td>
            <td><a href="category_items.php?cat={$items[w].catid}">{$items[w].category}</a></td>
            <td>{$items[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $items[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td>{if $ADMIN eq 1}<a href="users_items.php?id={$items[w].uid}">{$items[w].user.uname}</a>{else}{$items[w].provider}{/if}</td>
    </tr>
		  {/section}
</table>
{/if}