{include file="modules/d_items/admin/menu.tpl"}
{include file="modules/d_items/search_box.tpl" mode="start_page" action_title=$items.title}
<div class="wrap seperator padding">
<h2><a href="{$MODULE_FOLDER}/extra_fields.php">{$lang.extra_fields}</a> (<a href="#addnew">add new</a>)</h2>
<table border="0" cellpadding="2" cellspacing="1" width="100%">
  <form action="{$PHP_SELF}" method="POST" name="upsales">
    <input type="hidden" name="cat_org" value="{$smarty.get.cat|escape:"html"}">
    <tr>
      <th scope="cols">{$lang.field_name}</th>
      <th scope="cols">{$lang.var_name}</th>
      <th scope="cols">{$lang.active}</th>
      <th scope="cols">{$lang.type}</th>
      <th scope="cols">{$lang.assign_categories}</th>
      <th scope="cols">Searchable</th>
      <th scope="cols">&nbsp;</th>
      <th scope="cols">&nbsp;</th>
    </tr>
    {if $fields} {section name=a loop=$fields}
    <tr {cycle values=", class='TableSubHead'"}>
      <td width="2%"><input type="text" value="{$fields[a].field}" name="field-lng-{$fields[a].fieldid}"></td>
      <td><input type="text" value="{$fields[a].var_name}" name="var_name-{$fields[a].fieldid}"></td>
      <td><select name="active-{$fields[a].fieldid}">
          <option value="N" {if $fields[a].active eq "N"}selected{/if}>{$lang.disabled}</option>
          <option value="Y" {if $fields[a].active eq "Y"}selected{/if}>{$lang.enabled}</option>
      </select></td>
      <td><select name="type-{$fields[a].fieldid}" id="type">
          <option value="text" {if $fields[a].type eq "text"}selected{/if}>Text Field</option>
          <option value="area" {if $fields[a].type eq "area"}selected{/if}>Text Area</option>
          <option value="radio" {if $fields[a].type eq "radio"}selected{/if}>Radio Button</option>
          <option value="hidden" {if $fields[a].type eq "hidden"}selected{/if}>Hidden</option>
          <option value="document" {if $fields[a].type eq "document"}selected{/if}>File</option>
          <option value="image" {if $fields[a].type eq "image"}selected{/if}>Image</option>
          <option value="media" {if $fields[a].type eq "media"}selected{/if}>Media</option>
          <option value="link" {if $fields[a].type eq "link"}selected{/if}>Link</option>
      </select></td>
      <td>
 
      <SELECT name="categoryids-{$fields[a].fieldid}[]" multiple size="8">
<option value="">{$lang.none}</option>
{section name=cat_num loop=$allcategories}
<OPTION value="{$allcategories[cat_num].categoryid}" {foreach from=$fields[a].categories item=catid}{if $catid.catid eq $allcategories[cat_num].categoryid}selected{/if}{/foreach}>{$allcategories[cat_num].category}</OPTION>
{/section}
</SELECT></td>
      <td><select name="settings_search-{$fields[a].fieldid}" id="settings_search-{$fields[a].fieldid}">
          <option value="1" {if $fields[a].settings.search eq 1}selected{/if}>yes</option>
          <option value="0" {if $fields[a].settings.search eq 0}selected{/if}>no</option>
        </select>
        </td>
      <td><a href="{$PHP_SELF}?id={$fields[a].fieldid}&mode=tr" class="ItemsList">[ {$lang.translations} ]</a></td>
      <td><input name="button22" type=button value="{$lang.delete}"  onClick='javascript: if (confirm("{$lang.u_sure|strip_tags}")){ldelim}self.location="{$_PHP_SELF}?md=del&fieldid={$fields[a].fieldid}&id={$fields[a].fieldid}";{rdelim}'></td>
    </tr>
    {/section} {else}
    <tr>
      <td colspan="8" align="center">{$lang.txt_no_items}</td>
    </tr>
    {/if}
    <tr>
      <td colspan="8"><a name="addnew"></a>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td class="productDetailsTitle"><font class="SubHeader2">{$lang.add_field}</font> </td>
          </tr>
          <tr>
            <td class="Line" height="1"><img src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><br></td>
          </tr>
          <tr>
            <td height="10"><img src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><br></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td width="2%"><input type="text" value="" name="fieldnew"></td>
      <td><input type="text" value="" name="valuenew"></td>
      <td><input type="checkbox" name="activenew" value="Y" {if $fields[a].active eq "y"}checked{/if}></td>
      <td><select name="typenew" id="select">
          <option value="text">Text Field</option>
          <option value="area">Text Area</option>
          <option value="radio">Radio</option>
          <option value="hidden">Hidden</option>
          <option value="document" {if $fields[a].type eq "document"}selected{/if}>File</option>
          <option value="image" {if $fields[a].type eq "image"}selected{/if}>Image</option>
          <option value="media" {if $fields[a].type eq "media"}selected{/if}>Media</option>
          <option value="link" {if $fields[a].type eq "link"}selected{/if}>Link</option>
      </select></td>
      <td><SELECT name="newcategoryids[]" multiple size="8">
<option value="">{$lang.none}</option>
{section name=cat_num loop=$allcategories}
<OPTION value="{$allcategories[cat_num].categoryid}">{$allcategories[cat_num].category}</OPTION>
{/section}
</SELECT></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
	  <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="8"><input type="submit" name="Submit2" value="{$lang.add}/{$lang.update}">
          <input name="mode" type="hidden" id="mode" value="modify"></td>
    </tr>
  </form>
</table>
</div>

{if $smarty.post.mode eq "update" OR $mode eq "update"}
<br>

{capture name=dialog}
<form name="form2" method="post" action="">
  <table width="100%"  border="0" cellspacing="0" cellpadding="2">
    <TR class="TableHead">
      <TD>{$lang.language}</TD>
      <TD>{$lang.field_name}</TD>
      <TD>{$lang.default_value}</TD>
      <TD>&nbsp;</TD>
    </TR>
{if $trans}
	{section name=b loop=$trans}
    <tr valign="top">
      <td><SPAN class='productTitle'>{$trans[b].country}</span></td>
      <td><input type="text" size="20" name="field-{$trans[b].code}" value="{$trans[b].field}"></td>
      <td><input type="text" size="20" name="value-{$trans[b].code}" value="{$trans[b].value}"></td>
      <td><input name="button2" type=button value="{$lang.delete}"  onclick='javascript: if (confirm("{$lang.u_sure|strip_tags}")){ldelim}self.location="{$_PHP_SELF}?m=del&code={$trans[b].code}&id={$field_id}";{rdelim}'></td>
    </tr>
	{/section}
    <tr valign="top">
      <td><input type="submit" name="Submit" value="{$lang.update}">
           <input type="hidden" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}" name="id">
          <input type="hidden" name="mode" value="upd"></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<br>
{/if}
{if $field_lng}
<form name="form1" method="post" action="{$PHP_SELF}">
  <table width="100%"  border="0" cellspacing="0" cellpadding="2">
    <tr valign="top">
      <td colspan="6"><TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
        <TR>
          <TD class="productDetailsTitle">{$lang.add_lang}</TD>
        </TR>
        <TR>
          <TD class="Line" height="1"><IMG src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><BR></TD>
        </TR>
        <TR>
          <TD height="10"><IMG src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><BR></TD>
        </TR>
      </TABLE></td>
    </tr>
    <tr valign="top">
     <td><select name="field_lng">
     {section name=a loop=$field_lng}       
        <option value="{$field_lng[a].code}">{$field_lng[a].country}</option>
{/section}
</select></td>
      <td><input type="text" size="20" name="field_new_title"></td>
      <td><input type="text" size="20" name="field_new_value"></td>
      <td>&nbsp;</td>

    </tr>
    <tr valign="top">
      <td><input type="submit" name="Submit" value="{$lang.add}">
	        <input type="hidden" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}" name="id">
      <input type="hidden" name="mode" value="insert"></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>

    </tr>
  </table>
</form>
{/if}
{/capture}{include file="admin/dialog.tpl" items=$smarty.capture.dialog title=$lang.international_descriptions extra="width=100%"}
{/if}