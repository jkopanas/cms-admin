{include file="modules/d_items/admin/menu.tpl"}
{if $error_list}{include file="common/error_list.tpl"}{/if}
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}


{include file="modules/d_items/admin/additional_links.tpl" mode="nav_menu" base_file="items_modify"}

<div class="wrap seperator padding">
{if $item.image_full.thumb}
<div id="zeitgeist"><h3>Thumb image</h3><div style="padding:3px; border:1px solid #000000; width:{$image_types.thumb.width}px"><img src="{$item.image_full.thumb|default:$items_module.settings.default_thumb}"/></div>{$loaded_module}</div>
{/if}

<h2>{$lang.classification}</h2>


<br><br>
<form name="form1" method="POST" action="{$PHP_SELF}" id="form1" style="width:99%;">
<input type="hidden" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}" name="id">
<input name="action" type="hidden" id="action" value="{$action|default:"add"}">
<table width="100"  border="0" cellspacing="0" cellpadding="4">
    <tr>
      <td>{$lang.pos}</td>
      <td><input name="orderby" type="text" id="orderby" size="3" value="{$item.orderby}" /></td>
    </tr>
    <tr>
      <td>{$lang.availability}</td>
      <td><select name="active">
        <option value="0" {if $item.active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $item.active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select></td>
    </tr>
    <tr>
      <td width="120">{$lang.main_cat}</td>
      <td><select name="categoryid" id="categoryid" onchange="set_efields(document.getElementById('categoryid').options[document.getElementById('categoryid').selectedIndex].value,'extra_fields_div',{$item.id|default:0})">         
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $item.catid}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select>      </td>
    </tr>
    <tr>
      <td width="120">{$lang.aditional_cat}</td>
      <td>
      <SELECT name="categoryids[]" multiple size="8">
<option value="">{$lang.none}</option>
{section name=cat_num loop=$allcategories}
<OPTION value="{$allcategories[cat_num].categoryid}" {foreach from=$categoryids item=catid}{if $catid.catid eq $allcategories[cat_num].categoryid}selected{/if}{/foreach}>{$allcategories[cat_num].category}</OPTION>
{/section}
</SELECT></td>
    </tr>
   {if $manufacturers_module}
    <tr>
      <td>{$lang.municipalities}</td>
      <td><select name="mid">
	  <option value="%">{$lang.none}</option>
	  {section name=j loop=$manufacturers_list}
	  <option value="{$manufacturers_list[j].id}" {if $product.mid eq $manufacturers_list[j].id}selected{/if}>{$manufacturers_list[j].title}</option>
	  {/section}
	  </select></td>
    </tr>
    {/if}
    {if $locations_module}
    <tr>
      <td>{$lang.location}</td>
      <td><select name="locid">
	  {section name=a loop=$all_locations}
	  <option value="{$all_locations[a].categoryid}" {if $item.locid eq $all_locations[a].categoryid}selected{/if}>{$all_locations[a].category}</option>
	  {/section}
	  </select></td>
    </tr>
    {/if}
  </table>
  <h2>{$lang.details}</h2>
  <table width="100%"  border="0" cellspacing="0" cellpadding="4">
  {if $items_module.settings.use_sku}
    <tr>
      <td width="120"><FONT class="FormButton">{$lang.sku}</font></td>
      <td><input name="sku" type="text" id="sku" size="20" value="{$item.sku}"></td>
    </tr>
    {/if}
    <tr>
      <td width="120"><FONT class="FormButton">{$lang.title}</font></td>
      <td><input name="items_title" type="text" id="items_title" size="45" value="{$item.title}"></td>
    </tr>
    <tr>
      <td width="120"><FONT class="FormButton">{$lang.short_desc}</font></td>
      <td><textarea name="short_desc" cols="80" rows="5" id="short_desc">{$item.description}</textarea><br /><a href="javascript:openEditor(document.form1.short_desc)">Advanced Editor</a></td>
    </tr>
    <tr>
      <td width="120"><FONT class="FormButton">{$lang.long_desc}</font></td>
      <td>
<textarea name="long_desc" cols="80" rows="10" id="long_desc">{$item.full_description}</textarea> <br /><a href="javascript:openEditor(document.form1.long_desc)">Advanced Editor</a>     </td>
    </tr>
  {if $items_module.settings.use_product_tags}
    <tr>
      <td>Tags</td>
      <td><textarea name="tags" cols="75" rows="4" class="tags-input" id="tags">{$tags}</textarea></td>
    </tr>
    {/if}
    <tr>
      <td>{$lang.meta_desc}</td>
      <td><textarea name="meta_desc" cols="65" rows="4">{$item.meta_desc}</textarea></td>
    </tr>
    <tr>
      <td>{$lang.meta_keyw}</td>
      <td><textarea name="meta_keywords" cols="65" rows="4">{$item.meta_keywords}</textarea></td>
    </tr>
            {if $eshop_module}
    {include file="modules/eshop/admin/product_details.tpl" mode="editor"}
    {/if}
    </table>
    {if $extra_fields}
    
     <h2>{$lang.extra_fields}</h2>
     <div id="extra_fields_div">{include file="common/extra_fields_modify.tpl"}
     </div>
     {/if}
<input type="submit" name="Submit" value="{$lang.save}">
 
</form>

</div>
{literal}
<script type='text/javascript' src='/scripts/autocomplete/jquery.autocomplete.js'></script>
<script type='text/javascript' src='/scripts/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='/scripts/autocomplete/jquery.dimensions.js'></script>

<script>
$().ready(function() {
	$("#tags").autocomplete("/ajax/tags.php", {
		width: 320,
		max: 40,
		highlight: false,
		multiple: true,
		multipleSeparator: ", ",
		scroll: true,
		scrollHeight: 300
	});
	});
	</script>
{/literal}