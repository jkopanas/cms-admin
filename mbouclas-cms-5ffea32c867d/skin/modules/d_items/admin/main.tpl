{include file="modules/d_items/admin/menu.tpl"}


<div style="clear:both"></div>
 {include file="modules/d_items/search_box.tpl" mode="start_page"}
<div class="wrap seperator padding">
  <h2 class="tab">{$lang.latest_items} ({$latest_items|@count} {$lang.items})</h2>
<form name="latest_items_form" method="post"> 
 {include file="modules/d_items/admin/display_items_table.tpl" items=$latest_items type="edit" form_name="latest_items_form" page="index.php"}
</form>
</div>