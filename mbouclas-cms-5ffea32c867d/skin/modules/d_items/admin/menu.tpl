<ul id="adminmenu2">
	<li><a href="{$MODULE_FOLDER}/" {if $submenu eq "main"}class="current1"{/if}>{$lang.home}</a></li>
	<li><a href="{$MODULE_FOLDER}/items_new.php" {if $submenu eq "add"}class="current1"{/if}>Add items</a></li>
	<li><a href="{$MODULE_FOLDER}/search.php" {if $submenu eq "search"}class="current1"{/if}>Search items</a></li>
	<li><a href="{$MODULE_FOLDER}/search.php" {if $submenu eq "modify"}class="current1"{/if}>Modify items</a></li>
        <li><a href="{$MODULE_FOLDER}/mass_insert.php" {if $submenu eq "mass_insert"}class="current1"{/if}>Mass Insert</a></li>
    <li><a href="{$MODULE_FOLDER}/categories.php" {if $submenu eq "categories"}class="current1"{/if}>Categories</a></li>
    <li><a href="{$MODULE_FOLDER}/extra_fields.php" {if $submenu eq "extra_fields"}class="current1"{/if}>Extra Fields</a></li>
	<li><a href="{$MODULE_FOLDER}/settings.php" {if $submenu eq "settings"}class="current1"{/if}>Module Settings</a></li>
</ul>