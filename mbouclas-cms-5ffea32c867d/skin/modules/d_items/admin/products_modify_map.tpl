{include file="modules/d_items/admin/menu.tpl"}
{include file="modules/d_items/search_box.tpl" mode="small_admin" action_title=$items.title}
<div class="wrap">
<div id="zeitgeist">
  <h3>{$lang.in_this_section}</h3>
<div><img src="{if $products.image}{$products.image}{else}{$products_IMAGES}{$DEFAULT_THUMB}{/if}" name="preview"  border="1" align="left" style="border:#000000 1px solid">
<div style=" margin-left:130px;">{include file="modules/d_items/admin/additional_links.tpl"}</div>
<Br />
<h3>More items...</h3>
<ul>
{section name=a loop=$more_items}
<li>{if $items.id eq $more_items[a].id}<strong>{$more_items[a].title}</strong>{else}<a href="{$MODULE_FOLDER}/products_modify_related.php?id={$more_products[a].id}">{$more_items[a].title}</a>{/if}</li>
{/section}
</ul>
</div>

</div>
 <h2>{$lang.maps} (<a href="#featured_add">add new</a>)</h2>
 <br />
 {include file="modules/maps/admin/display_maps.tpl" type="item" WIDTH=200}
 {if $mapid}
 <br>
 <table width="600" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="200" valign="top"><input name="address" type="text" id="address" />
      <input name="Button" type="button" value="{$lang.add}"  class="submit"/>
      <div id="marker_list"></div>
      </td>
    
    <td width="400" valign="top"><div id="map" style="width:400px; height:300px; border:1px solid #000000; clear:both;"></div></td>
  </tr>
</table>
{/if}

 <div style="clear:both"></div>
</div>
{if $mapid}
{include file="modules/maps/admin/map_forms.tpl" mode="add_item"}
{literal}
<script>

		

(function($){
var map = initGmap({mapCenter: [{/literal}{$map.map_x},{$map.map_y}],  mapZoom:{$map.settings.mapZoom}, mapControlSize:'{$map.settings.mapControlSize}'{literal}},"map");
initGmap.addFeed(map,{feedUrl:'/ajax/draw_map.php?{/literal}mapid={$mapid}&itemid={$items.id}&mode=item&active=no{literal}', pointIsDraggable:'true' });


	$('.submit').click(function(){
  initGmap.searchAddress(map, {
    address: $('#address').val(), pointIsDraggable:'true', markerFormMode:'product_marker'
  })
	});	




})(jQuery);
</script>
{/literal}
{/if}