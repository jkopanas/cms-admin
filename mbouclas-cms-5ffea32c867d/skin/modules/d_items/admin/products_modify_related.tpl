{include file="modules/d_items/admin/menu.tpl"}
{include file="modules/d_items/admin/additional_links.tpl" mode="nav_menu" base_file="items_modify"}
<div class="wrap seperator padding">
  <h2>Related items</h2>
  
 
 <table cellpadding="4" cellspacing="1" id="FeaturedTable" width="100%" >

  <tr>
    <td valign="top">
<div>
<a name="featured_add" id="featured_add"></a>
<form name="search_cat" id="search_cat">
<input type="hidden" name="featured" value="0" id="featured" />
<input type="hidden" name="productsid" value="{$id}" id="productsid" />
<SELECT name="cat" size="10" style="width: 100%" onChange="submitGetCategory('search_cat','cat_results','cat');return false;">
{section name=cat_idx loop=$allcategories}
<OPTION value="{$allcategories[cat_idx].categoryid}"{if $smarty.get.cat eq $allcategories[cat_idx].categoryid} selected{/if}>{$allcategories[cat_idx].category_path}{$allcategories[cat_idx].category}</OPTION>
{/section}
</SELECT>
</form>
</div></td>
  </tr>
  <tr> <td><div id="cat_results" style="border:#000000 solid 1px"></div></td></tr>
 </table>
 <br />
 <div id="related_products" style="width:600px">{include file="modules/d_items/admin/related_items_table.tpl" links=$related}</div>
 <div style="clear:both"></div>
</div>