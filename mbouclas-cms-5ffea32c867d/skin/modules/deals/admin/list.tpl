<form id="searchDealsForm">
<table class="widefat">
  <tr>
    <td colspan="6">{include file="modules/content/pagination.tpl" mode="ajax" class="dealsPage" target="searchDealsForm"}</td>
  </tr>
  <tr>
    <th scope="col">ID</th>
    <th scope="col">{$lang.title}</th>
  	<th scope="col">{$lang.category}</th>
  	<th scope="col">{$lang.shop}</th>
  	<th scope="col">{$lang.startDate}</th>
  	<th scope="col">{$lang.endDate}</th>
  </tr>

{foreach from=$deals item=a}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><strong>{$a.id}</strong></td>
            <td><a href="/modules/deals/admin/modify.php?id={$a.id}">{$a.title|truncate:60:"...":false}</a></td>
            <td>{$categories[$a.categoryid].category}</td>
            <td>{$shops[$a.shopid].title}</td>
            <td>{$a.startDate|date_format:"%d/%m/%Y"}</td>
            <td>{$a.endDate|date_format:"%d/%m/%Y"}</td>
	</tr>
{/foreach}
  <tr>
    <td colspan="6">{include file="modules/content/pagination.tpl" mode="ajax" class="dealsPage" target="searchDealsForm"}</td>
  </tr>
</table>
</form>