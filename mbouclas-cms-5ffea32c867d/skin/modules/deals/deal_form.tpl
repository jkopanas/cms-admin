<link rel="stylesheet" href="/css/jquery-ui-1.8.10.custom.css" type="text/css" />

<script type="text/javascript" src="/scripts/jquery.livequery.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/lang/el.js"></script>
<script type="text/javascript" src="/scripts/jquery.idTabs.min.js"></script>
<script src="/scripts/jquery-ui-1.8.10.custom.min.js"></script>

<div class="seperator">
<h2><strong>Επεξεργασία προσφοράς:</strong> 
{$item.title}
</h2>
</div>
<form action="" name="EditForm" method="post">
<input type="hidden" name="FormAction" value="{if $item}update{else}add{/if}" />
<input type="hidden" name="parent" value="{if $parent}{$parent}{else}0{/if}" />
<input type="hidden" name="aggid" value="{$item.id}" /> 
<div id="ColumnLeft" class="float-left spacer-right">
    <div class="wrap spacer-bottom" style="border-radius: 10px 10px 10px 10px;">
    <h2>Τίτλος</h2>
    <div class="padding">
    <input type="text" class="title" value="{$item.title}" size="100" id="title" name="title">
        </div><!-- END PADDING -->
    </div>
    
    <div class="wrap spacer-bottom">
    <h2>Permalink</h2>
    <div class="padding">
    <input name="permalink" type="text" id="permalink" size="45" value="{$item.permalink}" class="ContentTitle">
        </div><!-- END PADDING -->
    </div><!-- END BOX -->
	{*
    <div class="wrap spacer-bottom">
    <h2>{$lang.categories}</h2>
    {if $item.category}
          <div class="UsedCategories">
           <h3>Κατηγορίες σε χρήση</h3>
          <ul id="ItemCategoriesList" class="spacer-bottom">
          {foreach from=$allcategories item=a name=b key=k}{assign var=tmp value=$a.categoryid}
          {if $simple_categories|is_array AND  $a.categoryid|array_key_exists:$simple_categories}
           <li id="CatList-{$a.categoryid}"><a href="javascript:void(0);" class="DeleteCatList" rel="{$a.categoryid}"><img src="/images/admin/delete.gif" width="12" height="12" border="0" align="absmiddle" /></a> <span {if $simple_categories[$tmp]}class="red"{/if}>{$a.category}</span></li>
           {/if}
          {/foreach}
          </ul>
          </div>
          {/if}
    <div class="padding">
        <div class="categories_list" id="categories_list">
        <ul>
        <li><strong>Κατηγορία</strong><span class="float-right"><strong>Κεντρική</strong></span></li>
        {foreach from=$allcategories item=a name=b key=k}{assign var=tmp value=$a.categoryid}
        <li {if $simple_categories|is_array AND  $a.categoryid|array_key_exists:$simple_categories}class="alternate"{/if}><input type="checkbox" name="cat[]" id="cat-{$a.categoryid}" value="{$a.categoryid}" {if $simple_categories|is_array AND  $a.categoryid|array_key_exists:$simple_categories}checked{/if} class="choose-cat" /> <label for="cat-{$a.categoryid}">{$a.category}</label><span class="float-right"><input type="radio" name="main-cat" value="{$a.categoryid}" id="main-cat-{$a.categoryid}" {if $simple_categories|is_array AND !$a.categoryid|array_key_exists:$simple_categories}disabled="disabled"{/if}  {if $simple_categories[$tmp]}checked{/if}  /></span></li>
        {/foreach}
        </ul>
        </div>     
        <a id="category-add-toggle" href="javascript:void(0);" class="hide-if-no-js" tabindex="3" rel="category-add">+ Προσθήκη κατηγορίας</a>
        <div id="category-add" style="display: none">
        <p id="category-add" class="wp-hidden-child">
        <label class="screen-reader-text" for="newcat">Τίτλος κατηγορίας</label> <input name="category" id="newcat" class="new-cat" tabindex="3" aria-required="true" type="text">
    
        <label class="screen-reader-text" for="alias">Alias</label><input name="alias" id="alias" class="new-cat" tabindex="3" aria-required="true" type="text"><br />
        <label for="newcat_parent">Κεντρική κατηγορία:</label>
        <select name="parent_catid" class="new-cat" id="parent_catid">
            <option value="0">{$lang.root_category}</option>    
    {foreach from=$allcategories item=a}
    <option value="{$a.categoryid}" {if $a.categoryid eq $CurrentCat.parentid}selected{/if}>{$a.category}</option>                
    {/foreach} 
    </select>
        <input id="category-add-sumbit" class="add-cat button" value="Προσθήκη" alt="listings" tabindex="3" type="button">
    
        </div>
        </div><!-- END PADDING -->
    </div><!-- END BOX -->
	*}

    
    
    
	<div class="wrap spacer-bottom">
  		<h2>{$lang.description}</h2>
    <div class="padding">
    <textarea name="description" cols="80" rows="5" id="description">{$item.description}</textarea><br /><a href="javascript:void(0);" class="open-editor" rel="description">	{$lang.advanced_editor}</a>
    </div><!-- END PAADDING -->
    </div><!-- END BOX -->        
	

    <div class="wrap spacer-bottom">
    <h2>Βασικά στοιχεία</h2>
    <div class="padding">
    <ul>
    <li><select name="active" id="active" {if $cron} disabled="disabled"{/if}>
            <option value="0" {if $item.active eq "0"}selected{/if}>{$lang.disabled}</option>
            <option value="1" {if $item.active eq "1"}selected{/if}>{$lang.enabled}</option>
          </select> {$lang.availability}
    </li>
   
    </ul>
    </div><!-- END PADDING -->
    </div><!-- END BOX -->
    
    <div class="wrap spacer-bottom">
    <h2>Τιμή</h2>
    <div class="padding">
    <ul>
    <li>
    	<input type="text" name="price" value="{$item.price}" /> Τιμή
    </li>
    <li>
    	<input type="text" name="previous_price" value="{$item.previous_price}" /> Προηγούμενη Τιμή
    </li>
    <li>
	    <input type="text" name="discount" value="{$item.discount}" /> Έκπτωση
    </li>
    </ul>
    </div><!-- END PADDING -->
    </div><!-- END BOX -->
    
    <div class="wrap spacer-bottom">
    <h2>Ημερομηνίες</h2>
    <div class="padding">
    <ul>
    	<li>
      	    <input type="text" name="startDate" value="{$item.startDate|date_format:"%d/%m/%Y"}" class="datepicker" /> Ημ.έναρξης
        </li>
        <li>
      	    <input type="text" name="endDate" value="{$item.endDate|date_format:"%d/%m/%Y"}" class="datepicker" /> Ημ.λήξης
        </li>
    </ul>
    </div><!-- END PADDING -->
    </div><!-- END BOX -->
    
    {if $loaded_modules.locations AND $item.id}

    <div class="wrap spacer-bottom">
    <h2>{$lang.location}</h2>
    <div class="padding">
    <select name="commonCategoryTree-{$allTreeCategories.locations.id}" id="allow_comments" style="width:250px">
    <option value="">{$lang.select_one}</option>
    {foreach from=$allTreeCategories.locations.categories item=a}
    <option value="{$a.categoryid}" {if $item.locations.catid eq $a.categoryid}selected{/if}>{$a.category}</option>      
    {/foreach}
    </select>
    <div class="seperator"></div>
    <a href="#" class="placeItemOnMap" rel="edit">{$lang.lbl_placeOnMap}</a>
    
    <div id="mapInfo">
    <script type="text/javascript" src="http://www.google.com/jsapi?autoload=%7Bmodules%3A%5B%7Bname%3A%22maps%22%2Cversion%3A3%2Cother_params%3A%22sensor%3Dfalse%22%7D%5D%7D"></script> 
        <script type="text/javascript" src="/scripts/maps/MarkerClusterer.js"></script> 
        <script type="text/javascript" src="/scripts/maps/progressBar.js"></script>
    <script type="text/javascript" src="/scripts/maps/infobox.js"></script> 
    <script type="text/javascript" src="/scripts/maps/gMaps2.js"></script> 
    <script type="text/javascript" src="/scripts/maps/loadMapItem.js"></script> 
    
    <input type="hidden" name="geocoderAddress" value="{$mapItem.geocoderAddress}" class="mapItem" />
    <input type="hidden" name="lat" class="mapItem" value="{$mapItem.lat}" />
    <input type="hidden" name="lng" class="mapItem" value="{$mapItem.lng}" />
    <input type="hidden" name="zoomLevel" class="mapItem" value="{$mapItem.zoomLevel}" />
    <input type="hidden" name="MapTypeId" class="mapItem" value="{$mapItem.MapTypeId}" />
    <textarea class="hidden" name="mapItem">{$mapItemEncoded}</textarea>

    </div>
    
    <div id="mapItem" style="width:100%; height:300px; background-color:#999;" {if !$mapItem}class="hidden"{/if}></div>
    <div class="clear"></div>
    </div><!-- END PADDING -->
    
    </div><!-- END BOX -->
    
    {/if}
	<p align="center"><input type="submit" value="{$lang.save}" class="button" /></p>
    
</div>
</form>
<script type="text/javascript" src="/scripts/deals.js"></script>


<script type="text/javascript" src="/scripts/ajax.js"></script>