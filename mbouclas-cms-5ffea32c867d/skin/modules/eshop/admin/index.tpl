

<script type="text/javascript" src="/scripts/autoComplete.js"></script>
<script type="text/javascript" >
//head.js('/scripts/multiselect/jquery.multiselect.min.js');
//head.js('/scripts/multiselect/jquery.multiselect.filter.min.js');
head.js('/scripts/eshop/admin-mcms.js');
head.js('/scripts/eshop/controler.js');
//head.js('/scripts/eshop/admin-searchbox.js');
</script>
<script type="text/javascript" src="/scripts/eshop/admin-searchbox.js"></script>

<div class="pageWrapper dp100 box_c" id="adminCanvasWrapper" >
	<div class="dp100" id="adminHeaderCanvas">
		<div class="box_c" >
			<div class="padding wrap">
				<h3 class="box_c_heading cf">
					{$lang.search}
				</h3>
				<div class="debug"></div>
				<div id="details"></div>
				<form name="searchform" id="eshopSearchForm"  method="POST" >
					<input id="searchBox" class="k-input dp100" name="searchBox" />
					 {* <input id="searchCombo" name="searchCombo" style="width: 50%"/> *}
				</form>
			</div><!-- END WRAP -->
		</div>
	</div>
	
	<div class="drawArea canvasArea" id="adminDrawableArea" >
		{if isset($fetcfingPage)}
			{include file=$fetcfingPage}
		{/if}
	</div>
</div>

<div style="display: none; visibility: hidden;" >
	{if count($jsIncludedFiles) gt 0}
			{foreach from=$jsIncludedFiles key=k item=v}
				<script type="text/javascript" src="{$v}"></script>
			{/foreach}
		
	{/if}
	
	{if isset($headReadyExecute)}
		<script type="text/javascript">		
			head.ready(function(){
				{$headReadyExecute}
			});
		</script>
	{/if}
</div>




<script type="text/javascript" >
	
</script>
<script type="text/javascript" >
	{if count($orderStatusCodes) gt 0}
		var orderStatusCodes = [];
		{foreach from=$orderStatusCodes key=k item=v}
			orderStatusCodes[{$k}] = "{$v}";
		{/foreach}
	{/if}
</script>

<script id="confirmationModal" type="text/x-kendo-template">
	<p class="confirmationMessage">Are you sure?</p>
	
	<button class="confirmationConfirmBtn k-button">Yes</button>
	<button class="confirmationCancelBtn k-button">No</button>
</script>


<script type="text/x-kendo-template" id="modalTemplate">

		<h3 class="dtKey padding large dp100 float-left" > Order ID: <label class="large">#= id #</label> &rarr; #= orderStatusConverter(status) #</h3>
		<div class="clear"></div>
		<div class="container formEl_a dp50 float-left" id="modalContainer">
    		<ul>
    			<li><div class="dtKey padding large " >  Email: <label class="large">#= email #</label></div></li>
    			<li><div class="dtKey padding large " >  Amount: <label class="large">#= amount #</label></div></li>
    			<li><div class="dtKey padding large " >  Payment: <label class="large">#= (payment_method.title ? payment_method.title : "Δεν υπάρχει") #</label></div></li>
    			<li><div class="dtKey padding large " >  Payment Description: <label class="large">#= (payment_method.description ? payment_method.description : "Δεν υπάρχει") #</label></div></li>
    			<li><div class="dtKey padding large " >  Shipping: <label class="large">#= (shipping_method.shipping_time? shipping_method.shipping_time : (shipping_method? shipping_method : "Δεν υπάρχει")) #</label></div></li>
    		    <li><div class="dtKey padding large " >  User Notes: <label class="large">#= notes #</label></div></li>
			</ul>
		</div>
		<div class=" orderDetailsContainer dp50 float-left" > </div>
		<div class="clear"></div>
		<h3 class="dtKey padding large dp100 float-left" > Commands </h3>
		<div class="clear"></div>
		<div class="commandsContainer dp100 float-left" id="modalCommands">
			<ul>
				<li><div class="dtKey padding"><a class="k-button k-button-icontext" id="orderArchiveBtn" >Archive <span class="hidden" id="orderArchiveVal" >#= id #</span> </a></div></li>
				<li><div class="dtKey padding"><input id="orderStatusList" value="#= status #" /></div></dt>
				<li><div class="dtKey padding"><a class="k-button k-button-icontext" id="orderPrintBtn" >Print to PDF <span class="hidden" id="orderPrintVal" >#= id #</span> </a></div></li>
			</ul>
		</div>
	{literal}
	<script type="text/javascript">
		head.ready(function() { fetchOrderInfo("#= id #"); } );
	</script>
	{/literal}
</script>

{literal}
<script type="text/x-kendo-template" id="extraDetails">
	<div class="container" id="modalContainer">
	    <ul>
	    	<li><div class="dtKey padding large" > Date added: <label class="dtValue orderDateVal" > #= kendo.toString(new Date(date_added*1000),"dd MMMM yyyy HH:mm") # </label> </div></li>
	    	
			#if (details.extra_charge_no_vat) { #
				<li><div class="dtKey padding large" > Charge With no Vat:  <label class="nmr large" >#= details.extra_charge_no_vat # </label></div></li>
			#}#
			#if (details.extra_charge_vat) { #
				<li><div class="dtKey padding large" > Charge With Vat:  <label class="nmr large" > #= details.extra_charge_vat # </label></div></li>
			#}#
			#if (details.invoice_type) { #
				<li><div class="dtKey padding large" > Invoic Type: <label class="nmr large" >#= details.invoice_type # </label></div></li>
			#}#
			<li><div class="dtKey padding large" > Name:<label class="nmr large padding">
			#if (details.order_name) { #
				#= details.order_name #
			#}#
			#if (details.order_surname) { #
				#= details.order_name #
			#}#
			</label></div></li>

			<li><div class="dtKey padding large" > Adress: <label class="nmr large padding">
			#if (details.order_address) { #
				#= details.order_address #
			#}#
			#if (details.municipality) { #
				, #= details.municipality #
			#}#
			#if (details.order_postcode) { #
				, #= details.order_postcode #
			#}#
			#if (details.order_city) { #
				, #= details.order_city #
			#}#
			</label></div></li>
			
			<li><div class="dtKey padding large" > Phone: <label class="nmr large padding">
			#if (details.order_phone) { #
				#= details.order_phone #
			#}#
			#if (details.order_mobile) { #
				, #= details.order_mobile #
			#}#
			</label></div></li>
	    </ul>
	</div>
</script>
{/literal}

<script id="confirmationModal" type="text/x-kendo-template">
	<p class="confirmationMessage">Are you sure?</p>
	
	<button class="confirmationConfirmBtn k-button">Yes</button>
	<button class="confirmationCancelBtn k-button">No</button>
</script>​

<script type="text/x-kendo-template" id="gridOrderStatus">
	<div class="orderStatusList">
	    <label class="Order-Status-label" for="orderStatusDDL">Show Order by Status:</label>
	    <input type="search" id="orderStatusDDL" style="width: 230px"></input>
	</div>
</script>

<style scoped="scoped">
	.orderStatusList, #doShowOrdersFiltersBtn, #resetOrdersBtn {
	    float: right;
	    margin-right: .8em;
	}
	.ofmBox
	{
		margin: 15px 5px;
		position: relative;
		float: left;
		width: 466px;
	}
	
#adminHeaderCanvas #eshopSearchForm {
	display: block;
	height: 71px;
}
#adminHeaderCanvas #eshopSearchForm .k-widget
{
	position: relative !important;
	display: inline-block !important;;
	float: left !important;;
}
#adminHeaderCanvas #searchBox {
	margin: auto;
}
#adminHeaderCanvas .k-autocomplete {
	margin: 10px auto;
	padding: 5px;
	width: 100% !important;
    vertical-align: middle;
	float: left;
}
#adminHeaderCanvas .k-combobox {
	width: 49%;
	margin: 10px auto;
	padding: 4px 5px;
	vertical-align: middle;
	float: left;
}
#adminHeaderCanvas .dp50
{
	margin: auto !important;
}
</style>