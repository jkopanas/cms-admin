<div>
{if $place == "home"}

Η παραγγελεία θα έιναι για τον <strong>{$Shipping_method.user_name} {$Shipping_method.user_surname} </strong>στην διεύθυνση
<strong> {$Shipping_method.order_city}, {$Shipping_method.order_address}  {$Shipping_method.order_postcode}  </strong> με τηλέφωνα : <strong>{$Shipping_method.user_mobile}, {$Shipping_method.tel} </strong>

{else}

Η παραγγελεία θα έιναι για τον <strong> {$Shipping_method.name} {$Shipping_method.user_surname} </strong> στην κλινική
<strong> {$Shipping_method.hospital_name} </strong> στον όροφο <strong> {$Shipping_method.Orofos} </strong> δωμάτιο <strong>{$Shipping_method.room_no} </strong>
με τηλέφωνα : <strong>{$Shipping_method.mobile}, {$Shipping_method.tel} </strong>

{/if}
</div>