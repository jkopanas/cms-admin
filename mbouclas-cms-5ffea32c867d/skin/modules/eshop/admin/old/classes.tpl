{include file="modules/eshop/admin/menu.tpl"}

<div class="wrap seperator padding">
  <h2>{$lang.available_classes}  (<a href="classes.php#newclass">add new</a>)</h2>
  <form name="availableclass" id="availableclass" method="post" action="{$PHP_SELF}">
  <table width="600"  border="0">
    <TR>
      <th scope="col">&nbsp;</th>
      <th scope="col">{$lang.pos}</th>
      <th scope="col">{$lang.class}</th>
      <th scope="col">{$lang.class_text}</th>
	  <th scope="col">{$lang.class_options}</th>
    </tr>
  {if $all_classes} 
  {foreach from=$all_classes item=a}
  <tr>
    <td width="2" align="center"><input name="classid[{$a.id}]" type="checkbox" value="1" id="check_values"></td>
    <td align="center"><input name="orderby-{$a.id}" type="text" id="orderby-{$a.id}" size="3" maxlength="3" value="{$a.orderby}"></td>
    <td align="center"><a href="{$SELF}?id={$a.id}">{$a.title}</a></td>
    <td align="center">{$a.classtext}</td>
	<td align="center">{if $a.type eq "absolute"}<select name="class_options-{$all_classes[zq].classid}">
{foreach from=$a.options item=b}
	<option value="{$b.optionid}">{$b.option_name} ( {$b.price_modifier} {$b.modifier_type})</option>
{/foreach}
</select>{/if}</td>
  </tr>
  {/foreach} 
  {else}
  <tr>
    <td colspan="5">{$lang.txt_no_classes}</td>
  </tr>
    {/if}
	<TD colspan="5"><BR>
<INPUT type="submit" class="button"  value="{$lang.update}"> 
<input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.availableclass.action="classes.php";document.availableclass.mode.value="delete_class"; document.availableclass.submit();{rdelim}' value="Delete Checked" class="button" />
<input name="mode" type="hidden" id="mode" value="update_class">
<input type="hidden" name="id" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}"></TD><TR>
  </table>
</form>
<div style="clear:both"></div>
</div>

<div class="wrap seperator padding">
  <h2>{if $class}{$lang.modify} {$lang.class} {$class.title}{else}{$lang.add_new} {$lang.class}{/if} <a name="newclass"></a></h2>
  <form action="" method="post" name="AddNewClass">

    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td>{$lang.availability}</td>
        <td><select name="active" id="active">
        <option value="1" {if $class.active eq 1}selected{/if}>{$lang.yes}</option>
        <option value="0" {if $class.active eq 0}selected{/if}>{$lang.no}</option>
      </select></td>
      </tr>
      <tr>
        <td width="16%">{$lang.type}</td>
        <td width="84%"><select name="type" id="type">
          <option value="container" {if $class.type eq "container"}selected{/if}>Container</option>
          <option value="absolute" {if $class.type eq "absolute"}selected{/if}>Absolute</option>
        </select>        </td>
      </tr>
      <tr>
        <td>{$lang.options} {$lang.type}</td>
        <td><select name="option_type" id="option_type">
          <option value="text" {if $class.option_type eq "text"}selected{/if}>Text</option>
          <option value="select" {if $class.option_type eq "select"}selected{/if}>Select</option>
          <option value="image" {if $class.option_type eq "image"}selected{/if}>Image</option>
          <option value="js" {if $class.option_type eq "js"}selected{/if}>Javascript</option>
        </select></td>
      </tr>
      <tr>
        <td>{$lang.module}</td>
        <td><select name="module_id" id="module_id">
        {foreach from=$available_modules item=a}
        <option value="{$a.id}" {if $a.id eq $class.module_id}selected{/if}>{$a.name}</option>
        {/foreach}
        </select>
        </td>
      </tr>
      <tr>
        <td>{$lang.pos}</td>
        <td><input name="orderby" type="text" id="orderby" size="3" maxlength="3" value="{$class.orderby}"></td>
      </tr>
      <tr>
        <td>{$lang.class_name}</td>
        <td><input name="title" type="text" id="title" value="{$class.title}"></td>
      </tr>
      <tr>
        <td>{$lang.title}</td>
        <td><input name="classtext" type="text" id="classtext" value="{$class.classtext}"></td>
      </tr>
      {if $class.type eq "absolute"}
      <tr>
        <td>{$lang.options}</td>
        <td><table width="100%" border="0" cellpadding="5" cellspacing="0" id="editableTable">
	<thead>
		<tr>
        {if $class.option_type eq "image"}
		  <th name="email" class="pvV pvEmail">&nbsp;</th>
         {/if}
			<th name="email" class="pvV pvEmail">{$lang.pos}</th>

			<th name="first" class="pvV pvEmpty">{$lang.title}</th>
			<th name="last" class="pvV pvEmpty">Option Value</th>
			<th name="last" class="pvV pvEmpty">{$lang.value}</th>
			<th name="chicago">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
    {if $class.options}
    {foreach from=$class.options item=h}
	<tr id="TR{$h.optionid}">
{if $class.option_type eq "image"}<td><img src="{$h.option_value}" width="50" height="50" /></td>{/if}
		<td><input name="options-orderby-{$h.optionid}" type="text" id="options-orderby-{$h.optionid}" size="3" value="{$h.orderby}"></input></td>
		<td><input name="options-option_name-{$h.optionid}" type="text" id="options-option_name-{$h.optionid}" value="{$h.option_name}" /></td>

		<td><input name="options-option_value-{$h.optionid}" type="text" id="options-option_value-{$h.optionid}" value="{$h.option_value}" /></td>
		<td><input name="options-price_modifier-{$h.optionid}" id="options-price_modifier-{$h.optionid}" type="text" size="5" value="{$h.price_modifier}" />
            <select name="options-modifier_type-{$h.optionid}" id="options-modifier_type-{$h.optionid}">
              <option value="%" {if $h.modifier_type eq "%"}selected{/if}>Percent</option>
              <option value="$" {if $h.modifier_type ne "%"}selected{/if}>Absoloute</option>
            </select></td>
		<td><a href="javascript:void(0);" id="RemoveRow6" onClick="javascript: RemoveRow('TR{$h.optionid}','editableTable');">Remove This</a></td>
	</tr>
    {/foreach}
    {else}
	<tr id="TR1">
		<td><input name="options-orderby-1" type="text" id="options-orderby-1" size="3"></input></td>
		<td><input name="options-option_name-1" type="text" id="options-option_name-1" /></td>

		<td><input name="options-option_value-1" type="text" id="options-option_value-1" /></td>
		<td><input name="options-price_modifier-1" id="options-price_modifier-1" type="text" size="5" />
            <select name="options-modifier_type-1" id="options-modifier_type-1">
              <option value="%" selected="selected">Percent</option>
              <option value="$">Absoloute</option>
            </select></td>
		<td><a href="javascript:void(0);" id="RemoveRow6" onClick="javascript: RemoveRow('TR1','editableTable');">Remove This</a></td>
	</tr>
    {/if}
	<tr>
	  <td>      
	  </tbody>

</table>
        <br />
<a href="javascript:AddRow('editableTable','TR');">ADD Row</a></td>
      </tr>
      {/if}
      <tr>
        <td><input name="button" type="submit" class="button" id="button" value="{$lang.save}"></td>
        <td><input name="id" type="hidden" id="id" value="{$id}">
        <input name="mode" type="hidden" id="mode" value="{$mode|default:"add"}">
        <input type="hidden" name="remove_list" id="remove_list" /></td>
      </tr>
    </table>
  </form>
  <div style="clear:both"></div>
</div>