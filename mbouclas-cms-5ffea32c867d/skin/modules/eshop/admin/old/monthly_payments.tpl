{include file="modules/eshop/admin/menu.tpl"}

<div class="padding seperator wrap">
<h2>Δόσεις</h2>
<div class="debug"></div>
<form name="calcDoseis"> 
<input type="button" name="SavePayments" value="Αποθήκευση" class="button" />
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="col">Αριθμός δόσεων</th>
    <th scope="col">Επιτόκιο</th>
    <th scope="col">Εμφάνιση στη λίστα</th>
    <th scope="col">&nbsp;</th>
  </tr>
  {foreach from=$payments item=a}
  <tr {if $a.show}class="alternate"{/if}>
    <td align="center">{$a.payments}</td>
    <td align="center"><input type="text" value="{$a.monthly_charge}" name="monthly_charge-{$a.id}" class="SaveData" />%</td>
    <td align="center"><input type="checkbox" name="show-{$a.id}" valus="1" class="SaveData" {if $a.show}checked{/if} /></td>
    <td align="center"><a href="monthly_payments.php?id={$a.id}&amp;mode=exceptions">Εξαιρέσεις</a></td>
  </tr>
  {/foreach}
</table>
<input type="button" name="SavePayments" value="Αποθήκευση" class="button" />
</form>

</div>


<div class="seperator"></div>
<script type="text/javascript" src="/scripts/monthly_payments.js"></script>