<form name="ordersForm">
<div>{if $ordersSum}{$lang.ordersAmountSum} : <strong>&euro;{$ordersSum|number_format:2:".":","}</strong>{/if}
{if $list.total gt $users|@count}
{include file="modules/content/pagination.tpl" mode="ajax" class="ordersPage" target="searchOrdersForm"}
{/if}
</div>
<table width="100%" cellpadding="5" cellspacing="5">
  <tr>
  <td colspan="8">
  {$lang.withSelectedDo} (<span class="messages">0</span> επιλέχθηκαν) : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="archiveChecked">{$lang.archive}</option>
        <option value="DeactivateChecked">{$lang.disable}</option>
        <option value="DeleteChecked">{$lang.delete}</option>
        </select>
         <input type="button" class="button userActions" value="{$lang.apply}" />
         <input type="button" class="button selectAllResults hidden" value="{$lang.selectAllResults}" alt="#searchOrdersForm" >
         
         </td>
  </tr>
<tr>
  <th><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
<th>#ID</th>
<th>email</th>
<th>User</th>
<th>Amnt</th>
<th>status</th>
<th>Method</th>
<th>Στο αρχείο</th>
<th>Date</th>
</tr>
       <tr class="nodrop nodrag hidden" id="selectAllResultsInfo">
        <td colspan="9">
    		<div class="messagePlaceHolder messageWarning">{$lang.allResultsSelected}</div>
            </td>
      </tr>    
{foreach from=$orders item=a}
<tr {cycle values=", class='TableSubHead'"}>
  <td><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values"  /></td>
<td><a href="orders.php?id={$a.id}">{$a.id}</a></td>
<td align="center">{$a.email}</td>
<td>{$a.userDetails.user_name} {$a.userDetails.user_surname}</td>
<td align="center"><strong>&euro;{$a.amount|number_format:2:".":","}</strong></td>
<td>{$status_codes[$a.status]}</td>
<td>{$a.payment_method.title}</td>
<td>{if $a.archive eq 1}Ναί{else}Όχι{/if}</td>
<td>{$a.date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
</tr>
{/foreach}

</table>
<div>{if $ordersSum}{$lang.ordersAmountSum} : <strong>&euro;{$ordersSum|number_format:2:".":","}</strong>{/if}
{if $list.total gt $users|@count}
{include file="modules/content/pagination.tpl" mode="ajax" class="ordersPage" target="searchOrdersForm"}
{/if}
</div>
<input type="hidden" class="CheckedActions" value="" name="allItemsSelected" />
<input name="page" type="hidden" id="page" value="{$page}" class="CheckedActions">
</form>