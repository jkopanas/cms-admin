{include file="modules/eshop/admin/menu.tpl"}

<div class="wrap">
<h2>{$lang.payment_methods} (<a href="payment_methods.php#addcat">add new</a>)</h2>
<form name="shipping_form" id="shipping_form" action="{$SELF}" method="post">
<table cellpadding="3" cellspacing="3" width="100%">
<tr>
		<th scope="col">ID</th>
        <th scope="col">{$lang.title}</th>
        <th scope="col">{$lang.description}</th>
        <th colspan="2">Action</th>
	</tr>
{section name=a loop=$payment_methods}
<tr class="{cycle values='alternate,'}">
<th scope="row">{$payment_methods[a].id}</th>
<td> <a href="{$SELF}?id={$payment_methods[a].id}#methods">{$payment_methods[a].title}</a></td>
				<td>{$payment_methods[a].description}</td>
		<td><a href="{$SELF}?id={$payment_methods[a].id}&action=edit#addcat" class="edit">Edit</a></td><td><a href="{$SELF}?id={$payment_methods[a].id}&action=delete" onclick="return confirm('You are about to delete the category \'{$payment_methods[a].shipping}\'.  All of its posts will go to the default category.\n  \'OK\' to delete, \'Cancel\' to stop.')" class="delete">Delete</a></td>
	  </tr>
{/section}
    </table>


</form>
</div>
<br>
<a name="methods"></a>
<div class="wrap">
<h2>{$lang.modify} {$lang.payment_method}</h2>
<form name="addcat" id="addcat" action="" method="post">               
<p>
        <select name="active">
            <option value="0" {if $item.active eq 0}selected{/if}>{$lang.disabled}</option>
            <option value="1" {if $item.active eq 1}selected{/if}>{$lang.enabled}</option>
          </select>
          </p> 
      <p>{$lang.pos}:<br>
        <input name="orderby" type="text" class="editform" style="background-color: rgb(255, 255, 160);" value="{$item.orderby}" size="4" id="orderby">
         </p>
        <p>Name:<br>
        <input name="title" type="text" id="title" style="background-color: rgb(255, 255, 160);" value="{$item.title}">
        </p>
        <p>Description: (optional) <br>
          <textarea name="description" rows="5" cols="100"  id="description">{$item.description}</textarea>
        </p>
        <p>Payment Processor : <br />
        <select name="settings-payment_processor" id="payment_processor">
	     {foreach from=$payment_processors item=a}
          <option value="{$a.id}" {if $item.settings.payment_processor eq $a.id}selected{/if}>{$a.title}</option>
          {/foreach}
          </select>
        
    </p> 
        <p>Extra charge<Br>
        <input name="surcharge" type="text" value="{$item.surcharge}"> <select name="surcharge_type">
          <option value="%" {if $item.surcharge_type eq "%"}selected{/if}>Percent</option>
          <option value="$" {if $item.surcharge_type eq "$"}selected{/if}>Absolute</option>
        </select>
        <p>
{foreach from=$shipping item=s name=sh}
{if $s.methods}
  <strong>{$s.shipping}</strong><Br />
{foreach from=$s.methods item=m name=me}
<input type="checkbox" name="ids[{$m.shippingid}]"  value="1" {foreach from=$item.shipping item=x}{if $x.shippingid eq $m.shippingid}checked{/if}{/foreach}/> 
{$m.shipping} [{$m.base_cost}&euro;]<br />
{/foreach}
{/if}
{/foreach}</p>
        <p  align="left"><input name="action" value="{$action|default:'add'}" type="hidden"><input type="hidden" name="id" value="{$id|default:$smarty.get.id}" /><input name="submit" type="submit" class="button" value="{$lang.save} »">
        </p>
    

</form>
</div>
