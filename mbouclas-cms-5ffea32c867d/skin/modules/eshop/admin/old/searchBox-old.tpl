<div class="dp50">
	<div class="box_c" />
		<div class="seperator padding wrap">
			<h3 class="box_c_heading cf">
				<h2><a href="javascript:void(0)" class="ShowSearchTable Slide" rel="#SearchTable">Αναζήτηση</a></h2>
			</h3>
			<div class="debug"></div>
			<form name="searchform" id="searchOrdersForm"  method="POST">
				<table width="100%" border=0 cellpadding="5" cellspacing="5" id="SearchTable" class="">
				<TR>
				  <TD nowrap class=FormButton>{$lang.results_per_page}</TD>
				  <TD width="933"><select name="results_per_page" id="results_per_page" class="secondarySearchData">
				    <option value="20" {if $data.results_per_page eq 20}selected{/if}>20</option>
				    <option value="60" {if $data.results_per_page eq 60}selected{/if}>60</option>
				    <option value="100" {if $data.results_per_page eq 100}selected{/if}>100</option>
				  </select>  </TD>
				</TR>
				<TR>
				  <TD nowrap class=FormButton>{$lang.order_by}</TD>
				  <TD> <select name="orderby" class="secondarySearchData">
				  <option value="eshop_orders.id">#ID</option> 
				   <option value="date_added">{$lang.date_added}</option>
				   <option value="amount">{$lang.amount}</option>
				   <option value="uname">{$lang.uname}</option>
				</select>  
				{$lang.way} : 
				<select name="way" id="sort_direction" class="secondarySearchData">
				<option value="desc">{$lang.descending}</option>
				<option value="asc">{$lang.ascending}</option>             
				        </select></TD>
				</TR>
				<TR>
				  <TD width="94" nowrap class=FormButton>#ID</TD>
				  <TD>
				  <INPUT name="id" type=text id="id" value="{$data.id}" class="SearchData"  /></TD>
				</TR>
				<tr>
				  <td nowrap class=FormButton>{$lang.user_name}</td>
				  <td><INPUT name="user_name" type=text id="id" value="{$data.id}" class="likeData"  /> {$lang.surname} : <INPUT name="user_surname" type=text id="id" value="{$data.id}" class="likeData"  /></td>
				</tr>
				<tr>
				  <td class=FormButton>{$lang.payment_method}</td>
				  <td><select name="payment_method" id="payment_method" class="SearchData multiSelect" size="5" multiple="multiple">
				    <option value="">{$lang.all}</option>
				          {foreach from=$payment_methods item=a key=k}
					      <option value="{$a.id}">{$a.title}</option>
				          {/foreach}
					      </select></td>
				</tr>
				<tr>
				  <td class=FormButton>{$lang.status}</td>
				  <td><select name="status" id="status" class="SearchData multiSelect" size="5" multiple="multiple">
				    <option value="">{$lang.all}</option>
				          {foreach from=$status_codes item=a key=k}
					      <option value="{$k}" {if $filters.status eq $k}selected{/if}>{$a}</option>
				          {/foreach}
					      </select></td>
				</tr>
				<tr>
				  <td class=FormButton nowrap>{$lang.priceRange}</td>
				  <td>
				    <div id="price-range" class="float-left" style="width:50%"></div> <span id="amount" class="float-left spacer-left" ></span> <input type="hidden" name="rangeActive" value="0" class="secondarySearchData" /> <input type="hidden" name="maxPrice" value="{$maxPrice}" /> <input type="hidden" name="amountFrom" value="" class="secondarySearchData" /> <input type="hidden" name="amountTo" value="" class="secondarySearchData" />
				  </td>
				</tr>
				<tr>
				  <td class=FormButton nowrap>{$lang.date}</td>
				  <td>{$lang.from} : <input type="text" id="dateFrom" name="dateFrom" class="datePicker secondarySearchData" size="30"/> {$lang.to} : <input type="text" id="dateTo" name="dateTo" class="datePicker secondarySearchData" size="30"/> </td>
				</tr>
				
				<tr>
				  <td class=FormButton nowrap>{$lang.specialFilters}</td>
				  <td><select name="custom_field" class="secondarySearchData">
				    <option value="">{$lang.none}</option>
				    <option value="uname" {if $filters.custom_field eq "uname"}selected{/if}>{$lang.username}</option>
				    <option value="email" {if $filters.custom_field eq "email"}selected{/if}>{$lang.email}</option>
				    </select>
				    <input type="text" name="custom_value" size="30" value="{$data.substring}" class="secondarySearchData" />  </td>
				</tr>
				<tr> 
				  <td colspan="2" align="center" class=FormButton>
				    <input type="submit" value="{$lang.search}" class="button" id="searchUsers" name="searchUsers">
				    <input name="page" type="hidden" id="page" value="{$page}" class="secondarySearchData">
				    <input name="ResultsDiv" type="hidden"  value="#ResultsPlaceHolder" class="secondarySearchData">
				    <input name="FormToHide" type="hidden"  value="#SearchTable" class="secondarySearchData"></td>
				</tr>
				
				</table>
			
			</form>
		</div><!-- END WRAP -->
	</div>
</div>