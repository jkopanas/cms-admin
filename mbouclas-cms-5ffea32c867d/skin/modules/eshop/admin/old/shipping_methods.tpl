{include file="modules/eshop/admin/menu.tpl"}

<div class="wrap">
<h2>{$lang.shipping_companies} (<a href="#addcat">add new</a>)</h2>
<br />
<form name="shipping_form" id="shipping_form" action="{$SELF}" method="post">
<table cellpadding="3" cellspacing="3" width="100%">
<tr>
		<th scope="col">ID</th>
        <th scope="col">{$lang.title}</th>
        <th scope="col">{$lang.description}</th>
        <th colspan="2">Action</th>
	</tr>
{section name=a loop=$shipping_methods}
<tr class="{cycle values='alternate,'}"><th scope="row">{$shipping_methods[a].shippingid}</th><td> <a href="{$SELF}?id={$shipping_methods[a].shippingid}#methods">{$shipping_methods[a].shipping}</a></td>
				<td>{$shipping_methods[a].description}</td>
		<td><a href="{$SELF}?id={$shipping_methods[a].shippingid}&action=edit#addcat" class="edit">Edit</a></td><td><a href="{$SELF}?id={$shipping_methods[a].shippingid}&action=delete" onclick="return confirm('You are about to delete the category \'{$shipping_methods[a].shipping}\'.  All of its posts will go to the default category.\n  \'OK\' to delete, \'Cancel\' to stop.')" class="delete">Delete</a></td>
	  </tr>
{/section}
    </table>


</form>

<form name="addcat" id="addcat" action="" method="post">               
          <div class="wrap">
            <h2>{if $shipping}{$lang.modify} {$shipping.shipping}{else}Add New Company{/if}</h2>
        <select name="active">
            <option value="0" {if $shipping.active eq 0}selected{/if}>{$lang.disabled}</option>
            <option value="1" {if $shipping.active eq 1}selected{/if}>{$lang.enabled}</option>
          </select>
          </p> 
      <p>{$lang.pos}:<br>
        <input name="orderby" type="text" class="editform" style="background-color: rgb(255, 255, 160);" value="{$shipping.orderby}" size="4" id="orderby">
         </p>
        <p>Name:<br>
        <input name="shipping" type="text" id="shipping" style="background-color: rgb(255, 255, 160);" value="{$shipping.shipping}">
        </p>
        <p>User:<br>
       <select name="settings-form">
       <option value="full" {if $shipping.settings.form eq "full"}selected{/if}>Full Form</option>
       <option value="basic" {if $shipping.settings.form eq "basic"}selected{/if}>Basic Form</option>
       </select>
        </p>
        <p>Description: (optional) <br>
          <textarea name="description" rows="5" cols="100"  id="description">{$shipping.description}</textarea>
        </p>
        <p>&nbsp;</p>
        <p  align="left"><input name="action" value="{$action|default:'add'}" type="hidden"><input type="hidden" name="id" value="{$id|default:$smarty.get.id}" /><input name="submit" type="submit" class="button" value="{$lang.save} »">
        </p>
    
</div>      
</form>

{if $id}{$methods}
<div class="wrap"><a name="methods" id="methods"></a>
<form name="methods_form" id="methods_form" action="" method="post">
<div>{section name=a loop=$shipping_methods}<span class="{if $shipping_methods[a].shippingid eq $id}wpv-tab-button{else}wpv-tab-button-disabled{/if}"><a href="{$MODULE_FOLDER}/shipping_methods.php?id={$shipping_methods[a].shippingid}#methods">{$shipping_methods[a].shipping}</a></span> {/section}</div>
<div style="    border: 5px solid #c0c0c0;
    border-top: 10px solid #c0c0c0;
    border-bottom: 10px solid #c0c0c0;
    padding: 5px 5px 5px 5px;
    margin: 0px 0px 0px 0px; ">

<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
  <th align="left" scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
          <th align="left" scope="col">{$lang.pos}</th>
          <th scope="col">{$lang.title}</th>
          <th scope="col">{$lang.delivery_time}</th>
          <th scope="col">{$lang.weight_limit}</th>
          <th scope="col">{$lang.destination}</th>
          <th scope="col">{$lang.cost}</th>
          <th scope="col">{$lang.availability}</th>
        </tr>
        {section name=a loop=$methods}
        <tr>
          <td align="left" id="popt_box_1"><input name="ids[{$methods[a].shippingid}]" type="checkbox" value="1" /></td>
          <td align="left" id="popt_box_2"><input name="orderby-{$methods[a].shippingid}" type="text" size="4" value="{$methods[a].orderby}" /></td>
          <td id="popt_box_3"><input name="shipping-{$methods[a].shippingid}" type="text" class="" value="{$methods[a].shipping}" /></td>
          <td align="center" id="popt_box_4"><input name="shipping_time-{$methods[a].shippingid}" type="text" value="{$methods[a].shipping_time}" size="15" /></td>
          <td align="center" id="popt_box_5"><input name="weight_min-{$methods[a].shippingid}" type="text" id="textfield" size="5" value="{$methods[a].weight_min}" />
          -
          <input name="weight_limit-{$methods[a].shippingid}" type="text" id="textfield2" size="5" value="{$methods[a].weight_limit}" /></td>
          <td align="center" id="popt_box_6"><select name="destination-{$methods[a].shippingid}" id="destination">
            <option value="L" {if $methods[a].destination eq "L"}selected{/if}>{$lang.national}</option>
            <option value="I" {if $methods[a].destination eq "I"}selected{/if}>{$lang.international}</option>
          </select></td>
          <td align="center" id="popt_box_7"><input name="base_cost-{$methods[a].shippingid}" type="text" id="base_cost-{$methods[a].shippingid}" value="{$methods[a].base_cost}" size="10" /></td>
          <td align="center" id="popt_box_7"><select name="active-{$methods[a].shippingid}" onchange="document.getElementsByName('ids[{$methods[a].shippingid}]')[0].checked='true'">
            <option value=0 {if $methods[a].active eq 0}selected{/if}>{$lang.disabled}</option>
            <option value=1 {if $methods[a].active eq 1}selected{/if}>{$lang.enabled}</option>
          </select></td>
        </tr>
        {/section}
                <tr>
                  <td colspan="8" align="left" id="popt_box_4"><h2>{$lang.add_new}</h2></td>
                </tr>
                <tr>
          <td align="left" id="popt_box_1">&nbsp;</td>
          <td align="left" id="popt_box_2"><input name="new_orderby" type="text" id="new_orderby" size="4" /></td>
          <td id="popt_box_3"><input name="new_shipping" type="text" class="" id="new_shipping" value="{$methods[a].shipping}" /></td>
          <td align="center" id="popt_box_4"><input name="new_shipping_time" type="text" id="new_shipping_time" size="15" /></td>
          <td align="center" id="popt_box_5"><input name="new_weight_min" type="text" id="textfield" size="5" value="{$methods[a].weight_min}" />
          -
          <input name="new_weight_limit" type="text" id="textfield2" size="5" value="{$methods[a].weight_limit}" /></td>
          <td align="center" id="popt_box_6"><select name="new_destination" id="destination">
            <option value="L" {if $methods[a].destination eq "N"}selected{/if}>{$lang.national}</option>
            <option value="I" {if $methods[a].destination eq "I"}selected{/if}>{$lang.international}</option>
          </select></td>
          <td align="center" id="popt_box_7"><input name="new_base_cost" type="text" id="new_base_cost" size="10" /></td>
          <td align="center" id="popt_box_7"><select name="new_active" id="new_active" onchange="document.getElementsByName('ids[{$methods[a].shippingid}]')[0].checked='true'">
            <option value="0" {if $methods[a].active eq 0}selected{/if}>{$lang.disabled}</option>
            <option value="1" {if $methods[a].active eq 1}selected{/if}>{$lang.enabled}</option>
          </select></td>
        </tr>
                
        <tr><td colspan="9" align="center"><input name="submit" type="submit" class="button" value="{$lang.save}" />
          <input name="button" type="submit" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.methods_form.action.value="delete";{rdelim}' value="Delete Checked" />
          <input name="action" type="hidden" id="action" value="modify" />
          <input name="id" type="hidden" id="id" value="{$id}" /></td>
        </tr>
      </table>
</div>
</form>
</div>
{/if}
<div style="clear:both"></div>
</div>
