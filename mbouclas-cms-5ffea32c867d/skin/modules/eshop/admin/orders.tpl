<div class="dp100">
	<div class="box_c" />
		<div class="padding wrap">
			<h3 class="box_c_heading cf">
				{$lang.lbl_results}
			</h3>
			<div class="debug"></div>
			<div id="resultsGrid">
			</div>
		</div><!-- END WRAP -->
	</div>
</div>

{if count($jsIncludes) gt 0}
	{foreach from=$jsIncludes key=k item=v}
		<script type="text/javascript" src="{$v}"></script>
	{/foreach}

{/if}

<script type="text/x-kendo-template" id="gridTemplate">
{if isset($gridTemplate)}
	{$gridColumns}
{else}
	Title #= titleVar #
{/if}
</script>

<script type="text/x-kendo-template" id="ordersFiltersModalTemplate">
	<div class="ofmBox">
	    <label class="Order-Status-label" for="orderStatusDDL" style="width: 230px; display: block; float: left;">Show Orderd by Status:</label>
	    <input type="search" id="orderStatusFilterList" style="width: 230px"></input>
	</div>
	<div class="ofmBox">
	    <label class="Order-Status-label" for="orderStatusDDL" style="width: 230px; display: block; float: left;">Show Orderd by Payment Method:</label>
	    <input type="search" id="orderPMethodsFilterList" style="width: 230px"></input>
	</div>
	<div class="ofmBox">
		<label class="Order-Status-label" for="orderStatusDDL" style="width: 230px; display: block; float: left;">Show Orderd by Shipping Method:</label>
		<input type="search" id="orderShipMethodsFilterList" style="width: 230px"></input>
	</div>
	<div class="ofmBox">
		<label class="Order-Status-label" for="orderStatusDDL" style="width: 230px; display: block; float: left;">Show Archived:</label>
		<input type="checkbox" id="orderArchiveFilterCB" style="width: 230px" ></input>
	</div>
	<div class="ofmBox">
		<label class="Order-Status-label" for="orderStatusDDL" style="width: 230px; display: block; float: left;">Filter Amount:</label>
		<input type="search" id="orderAmountOperatorFilterList" style="width: 95px"></input>
		<input type="number" value="0" min="0" id="orderAmountValueFilterList" style="width: 135px"></input>
	</div>
	<div class="ofmBox">
		<a class="k-button k-search k-button-icon float-right" id="doSearchFiltersBtn" ><span class=" "></span><span id="text">Search Orders</span></a>
	</div>
</script>

