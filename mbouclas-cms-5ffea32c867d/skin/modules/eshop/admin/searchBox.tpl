

<div class="dp100">
<div class="box_c" />
	<div class="padding wrap">
		<h3 class="box_c_heading cf">
			{$lang.lbl_results}
		</h3>
		<div class="debug"></div>
		<div id="resultsGrid">
		</div>
	</div><!-- END WRAP -->
</div>
</div>


<script type="text/x-kendo-template" id="gridTemplate">
{if isset($gridTemplate)}
	{$gridColumns}
{else}
	
{/if}
</script>

<script type="text/javascript" id="gridColumns">

var gridColumns = new Array();
gridColumns = [
{if isset($gridColumns)}
	{$gridColumns}
{else}

 { "title":"ID", "field":"id" },
 { "title":"Value", "field":"email" },
 { "title":"Description", "field":"status" }

{/if}
];
</script>

<style scoped>
#eshopSearchForm {
	display: block;
	height: 71px;
}
#searchBox {
	margin: auto;
}
.k-autocomplete {
	width: 49%;
	margin: 10px auto;
	padding: 5px;
    vertical-align: middle;
	float: left;
}
.k-combobox {
	width: 49%;
	margin: 10px auto;
	padding: 4px 5px;
	vertical-align: middle;
	float: left;
}
.dp50
{
	margin: auto !important;
}
}
</style>