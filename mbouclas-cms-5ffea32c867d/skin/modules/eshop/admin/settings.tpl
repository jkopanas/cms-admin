<div class="dp100">
	<div class="box_c" />
		<div class="padding wrap">
			<h3 class="box_c_heading cf">
				{$lang.lbl_results}
			</h3>
			<div class="debug"></div>
			<div id="resultsGrid">
			</div>
		</div><!-- END WRAP -->
	</div>
</div>

{if count($jsIncludes) gt 0}
<script type="text/javascript">
	{foreach from=$jsIncludes key=k item=v}
		head.js('{$v}');
	{/foreach}
</script>
{/if}

<script type="text/x-kendo-template" id="gridTemplate">
{if isset($gridTemplate)}
	{$gridColumns}
{else}
	Title #= titleVar #
{/if}
</script>


<style scoped>
#eshopSearchForm {
	display: block;
	width: 100%;
	height: 71px;
}
#searchBox {
	margin: auto;
}
.k-autocomplete {
	width: 49%;
	margin: 10px auto;
	padding: 5px;
    vertical-align: middle;
	float: left;
}
.k-combobox {
	width: 49%;
	margin: 10px auto;
	padding: 4px 5px;
	vertical-align: middle;
	float: left;
}
.dp50
{
	margin: auto !important;
}
}
</style>