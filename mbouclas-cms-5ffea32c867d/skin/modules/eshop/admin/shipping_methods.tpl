<div class="dp100">
	<div class="box_c" />
		<div class="padding wrap">
			<h3 class="box_c_heading cf">
				Shipping Methods
			</h3>
			<div class="debug"></div>
			<div class="k-toolbar k-grid-toolbar  dp100">
				<a class="k-button k-button-icontext" id="resetShipMethodBtn" ><span class="k-icon "></span>Reset</a>
		        <a class="k-button k-button-icontext k-add-button" ><span class="k-icon k-add"></span>Add new record</a>
		        <a class="k-button k-button-icontext" style="display: none;" id="backShipMethodBtn" ><span class="k-icon "></span>Back</a>
		        <a class="" ><span class="hidden" id="parrentBreadcrumb">Childs Of: </span></a>
		    </div>
			<div class=" dp100" id="resultsList">
			</div>
			<div class="k-pager-wrap dp100">
		        <div id="pager"></div>
		    </div>
		</div><!-- END WRAP -->
	</div>
</div>

{if count($jsIncludes) gt 0}
	{foreach from=$jsIncludes key=k item=v}
		<script type="text/javascript" src="{$v}"></script>
	{/foreach}
{/if}

<script type="text/x-kendo-template" id="gridTemplate">
{if isset($gridTemplate)}
	{$gridColumns}
{else}
	Title #= titleVar #
{/if}
</script>

{literal}
<script type="text/x-kendo-tmpl" id="listTemplate">
	<div class="list-box-view list-item-${id}">
		<span class="hidden" id="shipmethod_id">${id}</span>
	   	 <dl>
		 <div  id="left-pm-details" >
		        <dt>Shipping Method ID</dt>
		        <dd>
		        	<span id="shipmethod_id">${id}</span>
		        </dd>
		        <dt>Ονομασία</dt>
		        <dd>${shipping}</dd>
		        <dt>Κωδ Ονομ</dt>
		        <dd>${shipping_time}</dd>
		        <dt>Ενεργοποίηση</dt>
		        <dd> #if ( active > 0) { # Ενεργοποίημένο # } else { # Ανενεργό # } #</dd>
		        <dt>Υπερτίμηση</dt>
		        <dd>${base_cost}</dd>
		        <dt>Ελάχιστο Φορτίο</dt>
		        <dd>${weight_min} Kg</dd>
		        <dt>Όριο Φορτίου</dt>
		        <dd>${weight_limit} Kg</dd>
		     </div >   
	    	<div class="clear" id="right-pm-details" >
	    		${description}
	    	</div>
		    </dl>
	    <div class="edit-buttons">
	    	<a class="k-button k-button-icontext" id="fetchChildsBtn" ><span class="k-icon k-update"></span>Fetch Childrens<span class="hidden" id="fetchChildsVal" >#= id #</span><span class="hidden" id="fetchChildsTitle" >${shipping}</span></a>
		    <a class="k-button k-button-icontext k-edit-button" id="shipmethodListEditBtn"><span class="k-icon k-edit"></span>Edit</a>
		    <a class="k-button k-button-icontext k-delete-button" id="shipmethodListDeleteBtn"><span class="k-icon k-delete"></span>Delete</a>
		</div>
	</div>
</script>
{/literal}

{literal}
<script type="text/x-kendo-tmpl" id="editTemplate">
<div class="list-box-view list-item-${id}">
	<div id="tabStrip" >
			<ul>
				<li class="k-state-active">Ρυθμίσεις</li>
				<li>Περιγραφή</li>
				<li>Λειτουργικές Ρυθμίσεις</li>
			</ul>
			<div >
		    <dl >
		        <dt>Shipping Method ID</dt>
		        <dd>
		        	<span id="shipmethod_id">${id}</span>
		        </dd>
		        <dt>Ονομασία</dt>
		        <dd>
		            <input type="text" data-bind="value:shipping" name="shipping" id="ShippingShipping" required="required" validationMessage="required"/>
		            <span data-for="ShippingShipping" class="k-invalid-msg"></span>
		        </dd>
		        <dt>Κωδ Ονομ</dt>
		        <dd>
		            <input type="text" data-bind="value:shipping_time" name="shipping_time" id="ShippingShipping_time" required="required" validationMessage="required"/>
		            <span data-for="ShippingShipping" class="k-invalid-msg"></span>
		        </dd>
		        <dt>Ενεργοποίηση</dt>
		        <dd>
		            <input type="checkbox" id="ShippingActive" data-bind="checked:active" name="active" #if ( (active > 0) || active) { # checked="checked" # } # />
		        </dd>
		        <dt>Υπερτίμηση</dt>
		        <dd>
		            <input type="text" id="ShippingBase_cost" data-bind="value:base_cost" data-role="numerictextbox" name="base_cost" required="required" data-type="number" min="0" validationMessage="required" />
		            <span data-for="ShippingBase_cost" class="k-invalid-msg"></span>
		        </dd>
		        <dt>Ελάχιστο Φορτίο</dt>
		        <dd>
		            <input type="text" id="ShippingWeight_min" data-bind="value:weight_min" data-role="numerictextbox" name="weight_min" required="required" data-type="number" min="0" validationMessage="required" /> Kg
		            <span data-for="ShippingWeight_min" class="k-invalid-msg"></span>
		        </dd>
		        <dt>Όριο Φορτίου</dt>
		        <dd>
		            <input type="text" id="ShippingWeight_limit" data-bind="value:weight_limit" data-role="numerictextbox" name="weight_limit" required="required" data-type="number" min="0" validationMessage="required" /> Kg
		            <span data-for="ShippingWeight_limit" class="k-invalid-msg"></span>
		        </dd>
		    </dl>
		</div>
			<div  id="right-pm-details" >
        		<input type="textarea" class="dp100" data-role="editor" id="ShippingDescription" data-bind="value:description" name="description"  />
	    	</div>
	    	<div  id="right-pm-opsettings" >
	    		
	    		<dl >
		    		<dt>Ανηκει</dt>
			        <dd>
			        	<input type="text" id="ShippingParent" data-bind="value:parent" data-role="numerictextbox" name="parent" data-type="number" min="0" #if ( parent == null ) { # value="0" # } # />
			        </dd>
			        <dt>Μεταβλιτές</dt>
			        <dd>
			        	<input type="text" class="dp100" data-bind="value:costequationvars" id="ShippingEquationVars" name="equationVars"  /> π.χ. baseCost=1; weightLimit=2;
			        </dd>
			        <dt>Συνάρτηση</dt>
			        <dd>
			        	<input type="textarea" class="dp100" data-bind="value:costequation" id="ShippingEquation" name="equation"  /> π.χ. (baseCost*weightLimit)+2
			        </dd>
			    </dl>
	    	</div>
	</div>
	<div class="edit-buttons">
         <a class="k-button k-button-icontext k-update-button" id="shipmethodEditSaveBtn"><span class="k-icon k-update"></span>Save</a>
         <a class="k-button k-button-icontext k-cancel-button" id="shipmethodEditCancelBtn"><span class="k-icon k-cancel"></span>Cancel</a>
	</div>
</div>
</script>
{/literal}

<style scoped>
	.box-row
	{
		width: 153px;
		float: left;
		display: inline-block;
		margin: 3px;
	}
	.list-box-view
	{
	    float: left;
	    width: 45%;
	    margin: 10px;
	    padding: 3px;
	    -moz-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    -webkit-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    box-shadow: inner 0 0 50px rgba(0,0,0,0.1);
	    border-top: 1px solid rgba(0,0,0,0.1);
	    -webkit-border-radius: 8px;
	    -moz-border-radius: 8px;
	    border-radius: 8px;
	}
	#right-pm-details, #left-pm-details
	{
		margin: 10px;
	    padding: 3px;
		margin: 10px 0;
	}
	
	.list-box-view dl
	{
	    margin: 10px 0;
	    padding: 0;
	    min-width: 0;
		height: 200px;
	}
	.list-box-view dt, dd
	{
	    float: left;
	    margin: 0;
	    padding: 0;
	    //height: 30px;
	    //line-height: 30px;
	}
	.list-box-view dt
	{
	    clear: left;
	    padding: 0 5px 0 15px;
	    text-align: right;
	    opacity: 0.6;
	    width: 120px;
	}
	.k-listview
	{
	    border: 0;
	    padding: 0;
	    min-width: 0;
	}
	.k-listview:after, .product-view dl:after
	{
	    content: ".";
	    display: block;
	    height: 0;
	    clear: both;
	    visibility: hidden;
	}
	.edit-buttons
	{
		text-align: right;
	    padding: 5px;
	    border-top: 1px solid rgba(0,0,0,0.1);
	    -webkit-border-radius: 8px;
	    -moz-border-radius: 8px;
	    border-radius: 8px;
	}
	.k-toolbar, #resultsList, .k-pager-wrap
	{
	    margin: 0 auto;
	    -webkit-border-radius: 11px;
	    -moz-border-radius: 11px;
	    border-radius: 11px;s
	}
	span.k-invalid-msg
	{
	    position: absolute;
	    margin-left: 160px;
	    margin-top: -26px;
	}
	
	.k-tabstrip .k-content, .k-panelbar .k-tabstrip .k-content {
		border-style: solid;
    	border-width: 1px;
    	position: static;
    	padding 0.3em 0.3em;
    	margin: 0px;
	}
	
</style>
