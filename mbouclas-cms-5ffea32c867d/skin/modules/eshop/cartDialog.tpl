<div id="cartDialog">
    Content of the Window
</div>

<script type="text/x-kendo-template" id="cartDialogTemplate">
 <img src="#= item.image_full.thumb #" align="left" class="spacer-right" /> <strong>#= item.title #</strong> was successfully added to your shopping cart. 
 <div class="seperator"></div>
 <button type="button" class="button closeModal spacer-right"><span><span>Continue shopping</span></span></button> <a href="/#= mcms.settings.lang #/checkout.html" class="button"><span><span>Checkout</span></span></button>
</script>