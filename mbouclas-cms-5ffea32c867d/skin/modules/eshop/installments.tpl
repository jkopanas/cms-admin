<div class="installments box spacer-top spacer-bottom {if !$payment_methods[0].settings.installments}hidden{/if}" id="installments">
<h1 class="header">Δόσεις</h1>
<div class="content">
<select name="rates">
<option value="0" title="0">Επιλέξτε αριθμό δόσεων</option>
{foreach item=a from=$rates}
<option value="{$a.monthly_charge}" title="{$a.payments}" rel="{$a.payments}">{$a.payments} δόσεις {if $a.monthly_charge eq 0}(άτοκα){/if}</option>
{/foreach}
</select>
<div class="monthly_charge hidden spacer-top">Από <strong>&euro; <span class="installment"></span></strong> το μήνα</div>
</div>
</div><!-- END BOX -->