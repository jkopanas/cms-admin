
<div class="col2-set ">

<div class="col-1">
<h3>invoice type</h3>    
<div class="seperator"></div>  
<ul id="invoice-type">
	<li><input type="radio" value="Απόδειξη" name="invoice_type" id="receipt" class="radio invoice_method save" checked /> <label for="receipt">Απόδειξη</label></li>
	<li><input type="radio" value="Τιμολόγιο" name="invoice_type" id="invoice" class="radio invoice_method save" /> <label for="invoice">Τιμολόγιο</label></li>
</ul>

</div>
<div class="col-2">
<h3>Στοιχεία Τιμολόγησης</h3>  
<div class="content" id="payment_method">

</div><!-- END CONTENT -->

</div>

 <script id="invoice_field" type="text/x-kendo-template">
<div class="seperator"></div>
<table cellpadding="5" cellspacing="5" class=" invoice_table">
		<tr>
			<td  valign="middle" width="50%"><h5>* Επωνυμία:</h5></td>
			<td  valign="middle">
				<input gtbfieldid="37" size="40" name="order_name" value="" type="text"  required class="spacer-bottom input-text AccountDataInvoice">
			</td>
		</tr>
		<tr>
			<td  valign="middle" width="50%"><h5>* Επάγγελμα:</h5></td><td  valign="middle">
				<input gtbfieldid="38" size="40" name="proffession" value="" type="text" required  class="spacer-bottom input-text AccountDataInvoice">
			</td>
		</tr>
		<tr>
			<td  valign="middle" width="50%"><h5>* Διεύθυνση:</h5></td>
			<td  valign="middle">
				<input gtbfieldid="39" size="40" name="order_address" value="" type="text" required  class="spacer-bottom input-text AccountDataInvoice">
			</td>
		</tr>
		<tr>
			<td  valign="middle" width="50%"><h5>* Τ.Κ.:</h5></td>
			<td  valign="middle"><input gtbfieldid="41" size="40" name="order_postcode" value="" type="text" required class="spacer-bottom input-text AccountDataInvoice"></td>
		</tr>
		<tr>
			<td  valign="middle" width="50%"><h5>* ΑΦΜ:</h5></td>
			<td  valign="middle">
				<input gtbfieldid="40" size="40" name="afm" value="" type="text" required class="spacer-bottom input-text  AccountDataInvoice">
			</td>
		</tr>
		<tr>
			<td  valign="middle" width="50%"><h5>* ΔΟΥ:</h5></td>
			<td  valign="middle">
				<input gtbfieldid="41" size="40" name="doy" value="" type="text" required class="spacer-bottom input-text  AccountDataInvoice">
			</td>
		</tr>
		<tr>
			<td  valign="middle" width="50%"><h5>* Email:</h5></td>
			<td  valign="middle">
				<input gtbfieldid="42" size="40" name="email" value="" type="text" required class="spacer-bottom input-text  AccountDataInvoice">
			</td>
		</tr>
		<tr>
			<td  valign="middle" width="50%"><h5>* τηλέφωνο εταιρίας:</h5></td>
			<td  valign="middle">
				<input gtbfieldid="42" size="40" name="order_phone" value="" type="text" required class="spacer-bottom input-text  AccountDataInvoice">
			</td>
		</tr>
</table>
</script>

<script id="receipt_field" type="text/x-kendo-template">	
<div class="seperator"></div> 	
<strong> Όνοματεπώνυμο: </strong><input size="15" name="order_name" value="{$USER_NAME}" type="text" required class="spacer-bottom spacer-left spacer-right input-text  AccountDataInvoice"><input  data-bind="value: user_surname" size="15" name="order_surname" value="{$USER_SURNAME}" type="text" required class="spacer-bottom input-text  AccountDataInvoice">
<div class="seperator"></div> 
<strong> Διεύθυνση: </strong> <input size="46" name="order_address" value="{$settings_user.order_address}" type="text" required class="spacer-bottom spacer-left input-text AccountDataInvoice"/>
<div class="seperator"></div> 
<strong> Τηλέφωνο: </strong><input size="46" name="order_phone" value="" type="text" required class="spacer-bottom spacer-left input-text AccountDataInvoice" />
<div class="seperator"></div> 
<strong> Κινητό: </strong><input size="51" name="order_mobile" value="{$USER_MOBILE}" type="text" required class="spacer-bottom spacer-left input-text AccountDataInvoice" />
<div class="seperator"></div> 
<strong> Email: </strong> <input size="52" name="email" value="{$EMAIL}" type="text" required class="spacer-bottom spacer-left input-text  AccountDataInvoice" />
</script>		
					
</div>

