{include file="modules/listings/admin/menu.tpl"}

{include file="modules/listings/admin/additional_links.tpl" mode="menu" base_file="content_modify"}
<div class="seperator">
<h2>{if $nav ne "0"} {section name=w loop=$nav}
<a href="{$MODULE_FOLDER}/category_content.php?cat={$nav[w].categoryid}" class="navpath" title="{$nav[w].category}">{$nav[w].category}</a> / {/section}{/if} #{$item.id} {$item.title}</h2>
</div>
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}


<div class="wrap seperator">
<div class="padding">
  <h2>Είδος αρχείου (<a href="#view">Δείτε τα αρχεία</a>)</h2>
{if $media eq "thumb"}<strong>{$lang.thumb}</strong>{else}<a href="{$e_FILE}?id={$id}&media=thumb">{$lang.thumb}</a>{/if} | {if $media eq "image"}<strong>Φωτογραφίες</strong>{else}<a href="{$e_FILE}?id={$id}&media=image">Φωτογραφίες</a>{/if} | {if $media eq "video"}<strong>Βίντεο</strong>{else}<a href="{$e_FILE}?id={$id}&media=video">Βίντεο</a>{/if} | {if $media eq "document"}<strong>Αρχεία κειμένου</strong>{else}<a href="{$e_FILE}?id={$id}&media=document"> Αρχεία κειμένου </a>{/if}
{if $media eq "image"}
<div style="margin-left:80px; margin-top:5px;">Κατηγορία : {section name=b loop=$image_categories}
{if $image_categories[b].id ne 0}
{if $image_categories[b].id eq $type}<strong>{$image_categories[b].display_title}</strong>{else}<a href="{$e_FILE}?id={$id}&media=image&type={$image_categories[b].id}">{$image_categories[b].display_title}</a> {/if}
{if !$smarty.section.b.last} | {/if}
{/if}
{/section}</div>
{/if}
{if $media eq "video"}
<div style="margin-left:80px; margin-top:5px;">{if $type eq "FromFile"}<strong>Από αρχείο</strong>{else}<a href="{$e_FILE}?id={$id}&media=video&type=FromFile">Από αρχείο</a>{/if} | {if $type eq "FromEmbed"}<strong>Από embed</strong>{else}<a href="{$e_FILE}?id={$id}&media=video&type=FromEmbed">Από embed</a>{/if} 
</div>
{/if}
</div>
<div style="clear:both"></div>
</div>

<script src="/scripts/fileManager/fileManager.js"></script>
{if $CropImage}
<div class="wrap padding seperator">
<h2>Τροποποίηση μικρής φωτογραφίας</h2>
<div class="float-left spacer-right"><img src="{$CropImage.image_url}" id="cropbox" width="{$CropImage.image_x}" height="{$CropImage.image_y}" />{$CropImage.image_ur}</div>
<div class="float-left">
		<div style="width:{$image_types.thumb.width}px;height:{$image_types.thumb.height}px;overflow:hidden;">
			<img src="{$CropImage.image_url}" id="preview" width="{$image_types.thumb.width}" height="{$image_types.thumb.height}" />
		</div>
        <div class="seperator"></div>
        <input type="button" id="SaveCroppedThumb" value="Αποθήκευση" class="button" />
        </div>
        <div class="clear"></div>
      		<input type="hidden" id="x" name="x" class="ThumbData" />
			<input type="hidden" id="y" name="y" class="ThumbData" />
			<input type="hidden" id="w" name="w" class="ThumbData" />
			<input type="hidden" id="h" name="h" class="ThumbData" />
            <input type="hidden" id="id" name="itemid" value="{$item.id}" class="ThumbData" />
            <input type="hidden" id="id" name="imageID" value="{$CropImage.imageid}" class="ThumbData" />
</div>
{/if}
{if $media eq "image" OR $media eq "document" OR $type eq "FromFile"}
<div class="seperator padding wrap">
<h2>Προσθήκη αρχείων ( <a href="javascript:void(0);" class="Slide" rel="#UploadFile"><img src="/images/admin/add.gif"  /> Άνοιγμα</a>)</h2>
<div class="seperator hidden" id="UploadFile">
{if $media eq "image"}
<div  class="float-left spacer-right"><input type="file" name="uploadify" id="uploadifyMulti"  />

<ul class="msg spacer-top"></ul></div>
<div id="fileQueue" class="FileQueue float-left spacer-left spacer-right"></div>
<div class="clear">
<h2>Επιλογές</h2>
<label><input type="checkbox" value="1" name="CustomImageSet"  class="SaveData ShowImageCopies" alt="uploadifyMulti"  /> Επιλογή Αντιγράφων</label>
<ul class="hidden" id="ImageSet">
{foreach from=$image_types item=a}
<li><label><input type="checkbox" name="ImageSet{$a.id}" value="{$a.id}" class="SaveData ImageSet" {if $a.required}disabled{/if}  alt="uploadifyMulti"  /> {$a.title} ({$a.width}x{$a.height}px)</label> {if $a.required}<span class="red">*</span>{/if}</li>
{/foreach}
<li>Τα πεδιία με <span class="red">*</span> είναι υποχρεωτικά</li>
</ul>
<p>Τρόπος δημιουργίας αντιγράφων :
</p><ul class="ImageResizeSettings">
<li><label><input type="radio" name="ImageFix" value="ImageBox" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ImageBox"}checked="checked"{/if}/> 
  <strong>Image Box</strong> - Γεμίζει με κενό τον καμβά ώστε να διατηρηθεί το μέγεθος που θέλουμε</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWidth" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ByWidth"}checked="checked"{/if} /> 
  <strong>By Width</strong> - Διατηρεί σταθερό το πλάτος. Το ύψος μεταβάλεται</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWHeight" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ByWHeight"}checked="checked"{/if} /> 
  <strong>By Height</strong> - Διατηρεί σταθερό το ύψος. Το πλάτος μεταβάλεται</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWidthAndHeight" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ByWidthAndHeight"}checked="checked"{/if} /> 
  <strong>By Both</strong> - Διατηρεί σταθερό το ύψος και το πλάτος. Πιθανότητα παραμόρφοσης</label></li>
</ul>
<p></p>
</div>
<div class="seperator"></div>
<div class="debug"></div>
<div align="center"><a href="javascript:$('#uploadifyMulti').uploadifyUpload();">Αποστολή Αρχείων</a></div>
<input type="hidden" name="module" value="listings" class="SaveData" />
<input type="hidden" name="action" value="Save{$media}" class="SaveData" />
<input type="hidden" name="itemid" value="{$item.id}" class="SaveData" />
<input type="hidden" name="return" value="Thumb" class="SaveData" />
<input type="hidden" name="ImageCategory" value="{$type}" class="SaveData" />
{elseif $media eq "thumb"}
<div style="padding:3px; border:1px solid #000000; width:{$image_types.thumb.width}px" id="ItemThumb"><img src="{$item.image_full.thumb|default:$content_module.settings.default_thumb}" /></div>
<div class="seperator" id="UploadFile">
<div class="float-left spacer-right"><input type="file" name="uploadify" id="uploadify"  /><ul class="msg spacer-top"></ul></div>
<div id="fileQueue" class="FileQueue float-left spacer-left spacer-right height-100"></div>
<div class="clear">
<h2>Επιλογές</h2>
<label><input type="checkbox" value="1" name="CustomImageSet"  class="SaveData ShowImageCopies" alt="uploadify"  /> Επιλογή Αντιγράφων</label>
<ul class="hidden" id="ImageSet">
{foreach from=$image_types item=a}
<li><label><input type="checkbox" name="ImageSet{$a.id}" value="{$a.id}" class="SaveData ImageSet" {if $a.required}disabled{/if}  alt="uploadify"  /> {$a.title} ({$a.width}x{$a.height}px)</label> {if $a.required}<span class="red">*</span>{/if}</li>
{/foreach}
<li>Τα πεδιία με <span class="red">*</span> είναι υποχρεωτικά</li>
</ul>
<p>Τρόπος δημιουργίας αντιγράφων :
</p><ul class="ImageResizeSettings">
<li><label><input type="radio" name="ImageFix" value="ImageBox" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ImageBox"}checked="checked"{/if}/> 
  <strong>Image Box</strong> - Γεμίζει με κενό τον καμβά ώστε να διατηρηθεί το μέγεθος που θέλουμε</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWidth" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ByWidth"}checked="checked"{/if} /> 
  <strong>By Width</strong> - Διατηρεί σταθερό το πλάτος. Το ύψος μεταβάλλεται</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWHeight" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ByWHeight"}checked="checked"{/if} /> 
  <strong>By Height</strong> - Διατηρεί σταθερό το ύψος. Το πλάτος μεταβάλλεται</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWidthAndHeight" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ByWidthAndHeight"}checked="checked"{/if} /> 
  <strong>By Both</strong> - Διατηρεί σταθερό το ύψος και το πλάτος. Πιθανότητα παραμόρφοσης</label></li>
</ul>
<p></p>
</div>
<div class="debug"></div>
<div align="center"><a href="javascript:$('#uploadify').uploadifyUpload();">Αποστολή Αρχείου</a></div>
</div>
<input type="hidden" name="module" value="listings" class="SaveData" />
<input type="hidden" name="action" value="Savethumb" class="SaveData" />
<input type="hidden" name="itemid" value="{$item.id}" class="SaveData" />
<input type="hidden" name="return" value="Thumb" class="SaveData" />
<input type="hidden" name="ExistingThumb" value="{$item.image_full.thumb}" class="SaveData"/>
<input type="hidden" name="OnAllComplete" value="redirect" class="SaveData" />
{else} {* ALL OTHER TYPES *}
<div class="seperator" id="UploadFile">
  <div class="float-left spacer-right"><input type="file" name="uploadify" id="UploadMedia"  /><ul class="msg spacer-top"></ul></div>
<div id="fileQueue" class="FileQueue float-left spacer-left spacer-right height-100"></div>
<div class="clear">

</div>
<div class="debug"></div>
<div align="center"><a href="javascript:$('#UploadMedia').uploadifyUpload();">Αποστολή Αρχείου</a></div>
</div>
<input type="hidden" name="module" value="listings" class="SaveData" />
<input type="hidden" name="action" value="SaveMedia" class="SaveData" />
<input type="hidden" name="media" value="{$media}" class="SaveData" />
<input type="hidden" name="itemid" value="{$item.id}" class="SaveData" />
<input type="hidden" name="media" value="{$media}" class="SaveData" />
<input type="hidden" name="FilesAllowed" value="{$FilesAllowed}" class="SaveData" />
<input type="hidden" name="OnAllComplete" value="redirect" class="SaveData" />
{/if}

</div><!-- END HIDDEN UPLOAD DIV -->
</div>
{elseif $media eq "video" AND $type eq "FromEmbed"}
<div class="seperator padding wrap">
<h2>Νέο Βίντεο από embed</h2>
<div class="debug"></div>
<ul id="IdTabs"  class="idTabs">
<li><a  href="#tab1" rel="tab1" class="selected">Από YouTube</a></li>
<li><a  href="#tab2" rel="tab2">Από άλλη υπηρεσία</a></li>
</ul>
<div class="clear"></div>
<div id="tab-container" class="clear">
<div id="tab1" class="tabcontent">
<form name="EmbedVideoForm">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="14%">Διεύθυνση YouTube video</td>
    <td width="86%"><input name="url" type="text" class="settings" size="40" /> <a href="javascript:void(0);" class="GetYoutubeVideo">Έλεγχος βίντεο</a></td>
  </tr>
  <tr class="hidden ShowVideo">
    <td width="14%">'Ονομα Βίντεο</td>
    <td width="86%"><input name="title" type="text" class="settings" size="40" disabled="disabled" /></td>
  </tr>
  <tr class="hidden ShowVideo">
    <td>Κώδικας embed</td>
    <td><textarea name="embed" id="embed" cols="70" rows="6" class="settings" disabled="disabled"></textarea><br /><a href="javascript:void(0);" class="Preview-Video" rel="embed">Προεπισκόπιση Βίντεο</a></td>
  </tr>
    <tr class="hidden ShowVideo">
    <td>Διάρκεια</td>
    <td><span id="duration"></span><input type="hidden" name="duration" class="settings" /></td>
  </tr>
  <tr class="hidden ShowVideo">
    <td>επιλογή μικρής φωτογραφίας</td>
    <td><ul id="VideoThumbs"></ul></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><input name="submit" type="button" class="button SaveVideoEmbed" value="Αποθήκευση" /> <div class="debug"></div></td>
    </tr>
</table>
<input type="hidden" name="itemid" value="{$item.id}" class="settings" /> <input type="hidden" name="media" value="{$media}" class="settings" />
</form>
</div><!-- END TAB 1 -->
<div id="tab2" class="tabcontent">
<form name="EmbedVideoForm">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="14%">'Ονομα Βίντεο</td>
    <td width="86%"><input name="title" type="text" class="settings" size="40" /></td>
  </tr>
  <tr>
    <td>Κώδικας embed</td>
    <td><textarea name="embed" id="embedVid" cols="70" rows="6" class="settings"></textarea><br /><a href="javascript:void(0);" class="Preview-Video" rel="embedVid">Προεπισκόπιση Βίντεο</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><input name="submit" type="button" class="button SaveVideoEmbed" value="Αποθήκευση" /> <div class="debug"></div></td>
    </tr>
</table>
<input type="hidden" name="itemid" value="{$item.id}" class="settings" /> <input type="hidden" name="media" value="{$media}" class="settings" />
</form>
</div><!--- END TAB 2 -->
</div><!-- END TABS -->
</div><!-- END EMBED -->
{/if}
<div class="DebugSingle"></div>
<div id="SingleUpload" class="seperator padding wrap hidden">
<h2>Αντικατάσταση αντίγραφου φωτογραφίας <span class="ImageCopyName"></span></h2>
<div style="padding:3px; border:1px solid #000000; width:{$image_types.thumb.width}px" id="SingleItem"><img src="{$item.image_full.thumb|default:$content_module.settings.default_thumb}" id="SingleItemThumb" /></div>
<div class="seperator" id="UploadFileSigle">
<div class="float-left spacer-right"><input type="file" name="uploadifySingle" id="uploadifySingle"  /><ul class="msg spacer-top"></ul></div>
<div id="fileQueueSingle" class="FileQueue float-left spacer-left spacer-right height-100"></div>
<div class="clear">
<h2>Επιλογές</h2>
<p>Τρόπος δημιουργίας αντίγραφου :
</p><ul class="ImageResizeSettings">
<li><label><input type="radio" name="ImageFix" value="ImageBox" class="SaveDataSingle" {if $loaded_modules.listings.settings.imageFix eq "ImageBox"}checked{/if}/> 
  <strong>Image Box</strong> - Γεμίζει με κενό τον καμβά ώστε να διατηρηθεί το μέγεθος που θέλουμε</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWidth" class="SaveDataSingle" {if $loaded_modules.listings.settings.imageFix eq "ByWidth"}checked{/if} /> 
  <strong>By Width</strong> - Διατηρεί σταθερό το πλάτος. Το ύψος μεταβάλλεται</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWHeight" class="SaveDataSingle" {if $loaded_modules.listings.settings.imageFix eq "ByWHeight"}checked{/if} /> 
  <strong>By Width</strong> - Διατηρεί σταθερό το ύψος. Το πλάτος μεταβάλλεται</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWidthAndHeight" class="SaveDataSingle" {if $loaded_modules.listings.settings.imageFix eq "ByWidthAndHeight"}checked{/if} /> 
  <strong>By Both</strong> - Διατηρεί σταθερό το ύψος και το πλάτος. Πιθανότητα παραμόρφοσης</label></li>
</ul>
<p></p>
</div>
<div class="debug"></div>
<div align="center"><a href="javascript:$('#uploadifySingle').uploadifyUpload();">Αποστολή Αρχείου</a></div>
</div>
<input type="hidden" name="module" value="listings" class="SaveDataSingle" />
<input type="hidden" name="action" value="ReplaceImageCopy" class="SaveDataSingle" />
<input type="hidden" name="itemid" value="{$item.id}" class="SaveDataSingle" />
<input type="hidden" name="return" value="ReplaceImageCopy" class="SaveDataSingle" />
<input type="hidden" name="ImageCopy" value="" class="SaveDataSingle"/>
<input type="hidden" name="ImageID" value="" class="SaveDataSingle"/>

</div>






<div class="wrap seperator padding">

<a name="view" id="view"></a>
<FORM action="{$e_FILE}#view" method="POST" name="DetailedImages" id="DetailedImagesForm">
{if $media eq "image"}
<h2>{$lang.images}</h2>
<div id="DetailedImages-div"></div>
<div id="debugArea"></div>

<table width="100%" border="0" cellspacing="5" cellpadding="5" class="widefat" id="ImagesTable">
      <tr class="nodrop nodrag">
        <td colspan="8">{$lang.withSelectedDo}  (<span class="messages">0</span> {$lang.selected} ): <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">{$lang.enable}</option>
        <option value="DeactivateChecked">{$lang.disable}</option>
        <option value="DeleteChecked">{$lang.delete}</option>
        <option value="SavePrefs">{$lang.save}</option>
        </select>
         <input type="button" class="button PerformActionMedia" value="{$lang.apply}" />

        </td>
      </tr>
  <tr class="nodrop nodrag">
    <th width="16">&nbsp;<input type="hidden" name="TableOrder" class="settings" value="" /></th>
    <th width="120">&nbsp;</th>
    <th width="16"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
    <th width="145" scope="col">{$lang.availability}</th>
    <th width="150" scope="col">{$lang.category}</th>
    <th scope="col">{$lang.alt}</th>
    <th scope="col">{$lang.title}</th>
    <th>&nbsp;</th>
  </tr>
  {foreach from=$images item=a}
  <tr id="Copies{$a.id}">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
    <td><a href="{$a.main}" rel="pick"><img src="{$a.thumb}" width="60" height="60" /></a></td>
    <td><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /></td>
    <td>{if $a.available eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
    <td width="150"><select name="type-{$a.id}" id="type-{$a.id}" class="settings">
{section name=b loop=$image_categories}
<option value="{$image_categories[b].id}" {if $image_categories[b].id eq $a.type}selected{/if}>{$image_categories[b].title}</option>
{/section}
</select></td>
    <td width="150"><input type="text" value="{$a.alt}" name="alt-{$a.id}" class="settings" /></td>
    <td width="150"><input type="text" value="{$a.title}" name="title-{$a.id}" class="settings" /></td>
    <td><a href="content_modify_img.php?id={$item.id}&media={$media}&type={$type}&imageid={$a.id}&action=EditImage">Τροποποίηση</a> - <a href="#TB_inline?height=400&width=800&inlineId=CopiesTable{$a.id}&modal=true" rel="CopiesTable{$a.id}" class="thickbox">Αντίγραφα</a></td>
  </tr>
  {/foreach}
</table>
  {foreach from=$images item=a}
<table width="100%" border="1" cellpadding="5" cellspacing="5" class="hidden" id="CopiesTable{$a.id}">
  <tr>
  <th scope="col">Αντίγραφο</th>
  <th scope="col">Αρχείο</th>
  <th scope="col">&nbsp;</th>
  </tr>
  {foreach from=$image_types item=b}
  <tr>
  <td>{$b.title}</td>
  {if !$b.title|array_key_exists:$a}<td class="red"><a href="javascript:void(0);" id="{$b.title}-{$a.id}" class="red">Δεν υπάρχει αντίγραφο</a></td><td><a href="javascript:void(0);" class="ReplaceImageCopy" rel="{$b.title}|{$a.id}|{$a.thumb}">Ανεβάστε αντίγραφο</a></td>
  {else}
  <td><a href="{$a[$b.title]}" rel="pick" id="{$b.title}-{$a.id}">{$a[$b.title]}</a></td>
  <td><a href="javascript:void(0);" class="ReplaceImageCopy" rel="{$b.title}|{$a.id}|{$a.thumb}">Αντικατάσταση αρχείου</a></td>
  {/if}
  </tr>
  {/foreach}
  </table>
  {/foreach}


{elseif $media eq "thumb"}
<h2>{$lang.thumb}</h2>

<table width="100%" border="0" cellspacing="5" cellpadding="5" class="widefat">
  <tr>
    <th width="120">&nbsp;</th>
    <th scope="col">{$lang.alt}</th>
    <th scope="col">{$lang.title}</th>
    <th>&nbsp;</th>
  </tr>
  {foreach from=$images item=a}
  <tr>
    <td><a href="{$a.main}"  rel="pick"><img src="{$URL}/{$a.thumb}" width="60" height="60" /></a></td>
    <td width="150"><input type="text" value="{$a.alt}" name="alt-{$a.id}" class="settings" /></td>
    <td width="150"><input type="text" value="{$a.title}" name="title-{$a.id}" class="settings" /></td>
    <td><a href="content_modify_img.php?id={$item.id}&media={$media}&type={$type}&imageid={$a.id}&action=EditImage">{$lang.modify}</a> - <a href="content_modify_img.php?id={$item.id}&media={$media}&type={$type}&imageid={$a.id}&action=DeleteImage" rel="#Copies{$a.id}"><img src="/images/admin/delete.gif" align="absmiddle" /> {$lang.delete}</a> <input type="button" name="SaveSingleMedia" value="{$lang.save}" class="button" /> <input type="hidden" name="CheckedActions" value="SavePrefs" /> <input type="hidden" value="{$a.id}" class="check-values" name="id" /> </td>
  </tr>
  <tr id="Copies{$a.id}"><td colspan="4">
  <table width="100%" border="0" cellpadding="5" cellspacing="5" id="ImageCopies">
  <tr>
  <th scope="col">Αντίγραφο</th>
  <th scope="col">Αρχείο</th>
  <th scope="col">&nbsp;</th>
  </tr>
  {foreach from=$image_types item=b}
  <tr>
  <td>{$b.title}</td>
  {if !$b.title|array_key_exists:$a}<td><a href="javascript:void(0);" id="{$b.title}-{$a.id}" class="red">Δεν υπάρχει αντίγραφο</a></td><td><a href="javascript:void(0);" class="ReplaceImageCopy" rel="{$b.title}|{$a.id}">Ανεβάστε αντίγραφο</a></td>
  {else}
  <td><a href="{$a[$b.title]}" rel="pick" id="{$b.title}-{$a.id}">{$a[$b.title]}</a></td>
  <td><a href="javascript:void(0);" class="ReplaceImageCopy" rel="{$b.title}|{$a.id}">Αντικατάσταση αρχείου</a></td>
  {/if}
  </tr>
  {/foreach}
  </table>
  </td></tr>
  {/foreach}
</table>
{elseif $media eq "document"}

<h2>Αρχεία</h2>
<div id="debugArea"></div>
<FORM action="{$e_FILE}#view" method="POST" name="DocumentsForm" id="DocumentsForm">
<table width="100%" border="0" cellspacing="5" cellpadding="5" class="widefat DragDrop" >
      <tr class="nodrop nodrag">
        <td colspan="7">Με τα επιλεγμένα  (<span id="messages">0 επιλέχθηκαν</span> ): <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">Ενεργοποίηση</option>
        <option value="DeactivateChecked">Απενεργοποίηση</option>
        <option value="DeleteChecked">Διαγραφή</option>
        <option value="SavePrefs">Αποθήκευση προτιμήσεων</option>
        </select>
         <input type="button" class="button PerformActionMedia" value="Εκτέλεση" />

        </td>
      </tr>
  <tr class="nodrop nodrag">
    <th width="16">&nbsp;<input type="hidden" value="SaveDocumentsOrder" name="action" /> <input type="hidden" name="TableOrder" class="settings" value="" />
<input type="hidden" name="media" value="{$media}" class="settings" /></th>
    <th width="16"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
    <th scope="col">Διαθεσιμότητα</th>
    <th scope="col">Όνομα αρχείου</th>
    <th scope="col">Τύπος αρχείου</th>
    <th scope="col">Μέγεθος αρχείου</th>
    <th scope="col">Ημερομηνία εισαγωγής</th>
    </tr>
  {foreach from=$docs item=a}
  <tr id="Doc{$a.id}">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
    <td><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /></td>
    <td align="center">{if $a.available eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
    <td width="150"><input name="title-{$a.id}" type="text" value="{$a.title}" class="settings" size="40" /></td>
    <td align="center">{$a.type}</td>
    <td width="150" align="center">{$a.FileSize}</td>
    <td width="150">{$a.date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
    </tr>
  {/foreach}
</table>
</form>
{elseif $media eq "video"}
<script type="text/javascript" src="/scripts/flowplayer-3.2.2.min.js"></script>
<div id="VideoBox" class="hidden">
		<a  
			 href="http://e1h13.simplecdn.net/flowplayer/flowplayer.flv"  
			 style="display:block;width:500px;height:400px;"  
			 id="player"> 
		</a> 
  </div>
	
<h2>Βίντεο</h2>
<div id="debugArea"></div>
<FORM action="{$e_FILE}#view" method="POST" name="DocumentsForm" id="DocumentsForm">
<table width="100%" border="0" cellspacing="5" cellpadding="5" class="widefat DragDrop" >
      <tr class="nodrop nodrag">
        <td colspan="9">Με τα επιλεγμένα  (<span id="messages">0 επιλέχθηκαν</span> ): <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">Ενεργοποίηση</option>
        <option value="DeactivateChecked">Απενεργοποίηση</option>
        <option value="DeleteChecked">Διαγραφή</option>
        <option value="SavePrefs">Αποθήκευση προτιμήσεων</option>
        </select>
         <input type="button" class="button PerformActionMedia" value="Εκτέλεση" />

        </td>
      </tr>
  <tr class="nodrop nodrag">
    <th width="16">&nbsp;<input type="hidden" value="SaveVideosOrder" name="action" /> <input type="hidden" name="TableOrder" class="settings" value="" />
<input type="hidden" name="media" value="{$media}" class="settings" /></th>
    <th width="16"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
    <th scope="col">Διαθεσιμότητα</th>
    <th scope="col">Όνομα αρχείου</th>
    <th scope="col">Embed</th>
    <th scope="col">Τύπος αρχείου</th>
    <th scope="col">Μέγεθος αρχείου</th>
    <th scope="col">Ημερομηνία εισαγωγής</th>
    <th scope="col">&nbsp;</th>
    </tr>
  {foreach from=$videos item=a}
  <tr id="Doc{$a.id}">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
    <td><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /></td>
    <td align="center">{if $a.available eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
    <td><input name="title-{$a.id}" type="text" value="{$a.title}" class="settings" size="30" /></td>
    <td align="center">{if $a.embed}<textarea name="embed-{$a.id}" id="embed-{$a.id}" class="settings">{$a.embed}</textarea>{/if}</td>
    <td align="center">{$a.type|default:"Embed"}</td>
    <td align="center">{$a.FileSize}</td>
    <td>{$a.date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
    <td><a href="javascript:void(0);" rel="{if $a.embed AND !$a.file_url}embed-{$a.id}{else}{$a.file_url}{/if}" class="{if $a.embed AND !$a.file_url}Preview-Video{else}Preview-Video-File{/if}">Προεπισκόπιση</a></td>
    </tr>
  {/foreach}
</table>
</form>


{/if}

</div>

<div class="seperator"></div>