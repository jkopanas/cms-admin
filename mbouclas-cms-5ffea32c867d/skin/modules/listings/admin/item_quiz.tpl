{include file="modules/content/admin/menu.tpl"}
{include file="modules/content/admin/additional_links.tpl" mode="menu" base_file="content_modify"}
<div class="seperator">
<h2>{if $nav ne "0"} {section name=w loop=$nav}
<a href="{$MODULE_FOLDER}/category_content.php?cat={$nav[w].categoryid}" class="navpath" title="{$nav[w].category}">{$nav[w].category}</a> / {/section}{/if} <a href="modify.php?id={$item.id}">#{$item.id} {$item.title}</a> / <a href="item_quiz.php?id={$item.id}">Ερωτηματολόγια</a>  {if $QuizItem}/ {$QuizItem.title}{elseif $action eq "add"}/ Νέο ερωτηματολόγιο{/if}</h2>
</div>
{if $action eq "add" OR $action eq "modify"}
<div class="debugArea"></div>
{include file="modules/quiz/admin/new_quiz.tpl"}

{else}
<div class="seperator padding wrap">
<h2>Διαθέσιμα ερωτηματολόγια ( <img src="/images/admin/add.gif" align="absmiddle" /> <a href="item_quiz.php?id={$item.id}&action=add">Προσθήκη νέου</a> )</h2>
<form name="form" id="form" action="" method="post">

<div id="debugArea"></div>
<table cellpadding="5" cellspacing="5" class="widefat{if $reorder} DragDrop{/if}">
      <tr class="nodrop nodrag">
        <td colspan="7">Με τα επιλεγμένα (<span id="messages">0 επιλέχθηκαν</span> ) : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">Ενεργοποίηση</option>
        <option value="DeactivateChecked">Απενεργοποίηση</option>
        <option value="DeleteChecked">Διαγραφή</option>
        </select>
         <input type="button" class="button PerformAction" value="Εκτέλεση" />
        </td>
      </tr>

<tr class="nodrop nodrag">
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
  <th scope="col"><input type="hidden" value="SaveQuizOrder" name="action" /></th>
  <th scope="col">ID</th>
  <th scope="col">Τίτλος</th>
<th scope="col">#Ερωτήσεων</th>
<th scope="col">Ημερομηνία δημιουργίας</th>
<th scope="col">Διαθεσιμότητα</th>
{if $loaded_modules.content.settings.use_provider}{/if}</tr>

{foreach from=$items item=a}
<tr {cycle values="'FFFFFF', class='alternate'"} id="Order{$a.id}">
            <td><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /></td>
            <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
            <td><strong>{$a.id}</strong></td>
            <td><a href="item_quiz.php?id={$item.id}&quiz_id={$a.id}">{$a.title}</a></td>
            <td>{$a.num_questions}</td>
            <td>{$a.date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $a.active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
      {if $loaded_modules.content.settings.use_provider}  {/if}  </tr>
{/foreach}
      <tr>
        <td colspan="7">Με τα επιλεγμένα : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">Ενεργοποίηση</option>
        <option value="DeactivateChecked">Απενεργοποίηση</option>
        <option value="DeleteChecked">Διαγραφή</option>
        </select>
         <input type="button" class="button PerformAction" value="Εκτέλεση" />
        </td>
      </tr>
</table>
  </form>
</div><!-- END LIST -->
{/if}