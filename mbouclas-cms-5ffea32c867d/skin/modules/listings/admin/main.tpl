{include file="$MODULE_FOLDER/menu.tpl"}
{include file="modules/content/search_box.tpl" mode="start_page"}
<div class="wrap seperator padding">
  <h2 class="tab">{$lang.latest} ({$latest|@count} {$lang.items})</h2>
<br /><br />
<form name="latest_content_form" method="post">
<input name="ResultsDiv" type="hidden"  value="#ResultsDiv" class="SearchData">
<div id="ResultsDiv">
{include file="modules/content/admin/display_content_table.tpl" items=$latest type="edit" form_name="latest_content_form" page="index.php"}
</div>
</form>
</div>