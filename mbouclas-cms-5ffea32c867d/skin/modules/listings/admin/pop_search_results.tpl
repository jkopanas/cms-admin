{$query}
{if !$in_form}
<form id="pop_search_results_form" name="pop_search_results_form">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="4">Check All/None <input type="checkbox" id="total_check" name="ids[1]" onclick="multi_check_all_boxes(this.form.check_values,'total_check')" /> <input type="button" name="Bookmark Selected" value="Bookmark Selected" id="bookmark_selected" onclick="submitAddBookmark('bookmarks','pop_search_results_form','bookmark_loading')" />
    <input type="button" name="Add Selected" value="Add Selected" id="add_selected" onclick="submitSetproduct('pop_search_results_form','related_content')" /></td>
  </tr>
  {section name=a loop=$content}
  <tr bgcolor="{cycle values='#eeeeee,#d0d0d0'}">
    <td align="center"><input name="ids[{$content[a].id}]" type="checkbox" class="checkbox" id="check_values"  value="1" /></td>
    <td><a href="modify.php?id={$content[a].id}">{$content[a].title}</a></td>
		<td><input type="checkbox" name="bidirectional-{$content[a].id}" id="bidirectional-{$content[a].id}" value="1" /> Bidirectional Link</td>
	<td>{if $content[a].active eq 1}<font color="#00FF33">Active{else}<font color="#FF0000">Disabled{/if}</font></td>
  </tr>
  {/section}
  <tr>
    <td colspan="4">Check All/None <input type="checkbox" id="total_check" name="ids[1]" onclick="multi_check_all_boxes(this.form.check_values,'total_check')" /> <input type="button" name="Bookmark Selected" value="Bookmark Selected" id="bookmark_selected" onclick="submitAddBookmark('bookmarks','pop_search_results_form','bookmark_loading')" />
	<input type="hidden" value="" name="id" id="id" />
    <input type="hidden" value="{$smarty.get.cat}" name="category" id="category" />
    <input type="button" name="Add Selected" value="Add Selected" id="add_selected" onclick="submitSetProduct('pop_search_results_form','related_content')" /></td>
  </tr>
</table>
</form>
{else}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  {section name=a loop=$content}
  <tr bgcolor="{cycle values='#eeeeee,#d0d0d0'}">
    <td align="center"><input name="ids[{$content[a].id}]" type="checkbox" class="checkbox" id="check_values" onclick="selectRow({$content[a].id});" value="1" /></td>
    <td><a href="modify.php?id={$content[a].id}">{$content[a].title}</a></td>
		<td><input type="checkbox" name="bidirectional-{$content[a].id}" id="bidirectional-{$content[a].id}" value="1" /> Bidirectional Link</td>
	<td>{if $content[a].active eq 1}<font color="#00FF33">Active{else}<font color="#FF0000">Disabled{/if}</font></td>
  </tr>
  {/section}
  <tr>
    <td colspan="4">Check All/None <input type="checkbox" id="total_check" name="ids[1]" onclick="multi_check_all_boxes(this.form.check_values,'total_check')" /> <input type="button" name="Bookmark Selected" value="Bookmark Selected" id="bookmark_selected" onclick="submitAddBookmark('bookmarks','pop_search_results_form','bookmark_loading')" />
    <input type="button" name="Add Selected" value="Add Selected" id="add_selected" onclick="submitSetproduct('{$in_form}','related_products')" /></td>
  </tr>
</table>
{/if}