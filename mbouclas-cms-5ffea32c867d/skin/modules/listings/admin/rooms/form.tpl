<script type="text/javascript" src="/scripts/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="/scripts/common.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="/manage/tools/ckeditor/lang/el.js"></script>
<script type="text/javascript" src="/scripts/booking.js"></script>
<link href="/skin/css/style.css" rel="stylesheet" type="text/css" />
<div class="wrap spacer-bottom">
<h2>Period Titles</h2>
<div class="padding">
<div id="roomform">

<table>
<tr>
<input type="hidden" name="id" id="id" class="roomdata" value="{$item.id}" />
<input type="hidden" name="listing_id" class="roomdata" value="{$item.listing_id}" />
    <td>Title</td>
    <td><input type="text" name="name" class="roomdata" value="{$item.name}" /></td>
</tr>
<tr>
    <td>Period 1 price</td>
    <td><input type="text" name="period_1" class="roomdata" value="{$item.period_1}" /></td>
</tr>
<tr>
    <td>Period 2 price</td>
    <td><input type="text" name="period_2" class="roomdata" value="{$item.period_2}" /></td>
</tr>
<tr>
    <td>Period 3 price</td>
    <td><input type="text" name="period_3" class="roomdata" value="{$item.period_3}" /></td>
</tr>
<tr>
    <td>Period 4 price</td>
    <td><input type="text" name="period_4" class="roomdata" value="{$item.period_4}" /></td>
</tr>
<tr>
    <td>Order</td>
    <td><input type="text" name="sort" class="roomdata" value="{$item.sort}" /></td>
</tr>
<tr>
<td colspan="2">
Description
</td>
</tr>
<tr>
<td colspan="2">
<textarea name="description" class="roomdata" cols="80" rows="10" id="room_description">{$item.description}</textarea> <br /><a href="javascript:void(0);" class="open-editor" rel="room_description">{$lang.advanced_editor}</a>
</td>
</tr>
<tr>
    <td colspan="2"><button class="submitRoom">Submit</button></td>
</tr>
</table>
</div>
</div></div>