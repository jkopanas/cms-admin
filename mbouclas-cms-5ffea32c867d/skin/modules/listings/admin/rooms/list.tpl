<table id="rooms_table" width="700" cellspacing="5" cellpadding="5" border="0">

<tr>
<th>{$lang.title}</th>
<th>{$lang.period} 1</th>
<th>{$lang.period} 2</th>
<th>{$lang.period} 3</th>
<th>{$lang.period} 4</th>
<th><a href="#" class="addroom" rel="new" >{$lang.newroom}</a></th>
</tr>
{foreach from=$rooms item=room}
<tr class="{cycle values='normal,alternate'}">
<td>{$room.name}</td>
<td>{$room.period_1}</td>
<td>{$room.period_2}</td>
<td>{$room.period_3}</td>
<td>{$room.period_4}</td>
<td>
<a href="#" rel="{$room.id}" class="editroom">{$lang.edit}</a> | 
<a href="#" rel="{$room.id}" class="deleteRoom">{$lang.delete}</a><br />
<a href="#" class="translateRoom" title="{$lang.edit_all_translations}" rel="{$room.id}"><img src="/images/admin/language_16.png" width="16" height="16" /> {$lang.translate}</a></td>
</tr>
{/foreach}
</table>