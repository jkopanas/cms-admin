{include file="modules/content/admin/menu.tpl"}
<div class="clear"></div>
<div class="wrap seperator padding">
<h2>Units ( <a href="#add_new">Add new</a> )</h2>
<form action="" name="units-form" method="post">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td><strong>#ID</strong></td>
    <td><strong>Name</strong></td>
    <td><strong>Description</strong></td>
    <td><strong>Value</strong></td>
    <td><strong>Catid</strong></td>
    <td><strong>Base</strong></td>
  </tr>
  {foreach from=$units item=a}
  <tr>
    <td>{$a.id}</td>
    <td><input type="text" name="name-{$a.id}" value="{$a.name}" /></td>
    <td><textarea name="description-{$a.id}">{$a.description}</textarea></td>
    <td><input type="text" name="value-{$a.id}" value="{$a.value}" /></td>
    <td><select name="catid-{$a.id}" >
    {foreach from=$base_units item=b}
    <option value="{$b.catid}" {if $b.catid eq $a.catid}selected{/if}>{$b.name}</option>
    {/foreach}
    </select></td>
    <td><input type="checkbox" value="1" name="base-{$a.id}" {if $a.base} checked{/if} /></td>
  </tr>
  {/foreach}
  <tr><td colspan="6"><h2>Add New</h2></td></tr>
    <tr>
    <td>&nbsp;</td>
    <td><strong>Name</strong></td>
    <td><strong>Description</strong></td>
    <td><strong>Value</strong></td>
    <td><strong>Catid</strong></td>
    <td><strong>Base</strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="text" name="name" /></td>
    <td><textarea name="description"></textarea></td>
    <td><input type="text" name="value" value="" /></td>
    <td><select name="catid" >
    {foreach from=$base_units item=b}
    <option value="{$b.catid}">{$b.name}</option>
    {/foreach}
    </select></td>
    <td><input type="checkbox" value="1" name="base" /></td>
  </tr>
    <tr><td colspan="6"><input type="submit" value="{$lang.save}" class="button" /><a name="add_new"></a></td></tr>
</table>
</form>
 </div>