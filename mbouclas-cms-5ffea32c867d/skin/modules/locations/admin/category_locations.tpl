{include file="modules/locations/admin/menu.tpl"}
{include file="modules/locations/search_box.tpl" mode="small_admin" action_title=$locations.title}
<div class="wrap">

<h2><a href="{$MODULE_FOLDER}/categories.php">{$lang.categories}</a> (<a href="{$MODULE_FOLDER}/categories.php#addcat">add new</a>) {include file="modules/locations/nav_categories.tpl" management=$lang.categories target="admin_categories"}</h2>


<form name="latest_locations_form" method="post">
{include file="modules/locations/admin/display_locations_table.tpl" locations=$locations type="edit" form_name="latest_locations_form" page="category_locations.php"}
</form>
</div>