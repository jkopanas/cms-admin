{include file="admin/popup_locations_js.tpl"}
<div class="pagetitle"><img src="{$ImagesDir}/individual/admin/folder.jpg" width="54" height="55" align="absmiddle"> {$lang.categories}</div>
<div id="NavMenu">{include file="admin/tree_nav_menu.tpl" management=$lang.categories}</div>
<div><a href="{$ADMIN_URL}/cuisine_add.php?cat={$smarty.get.cat}">Add a new category under {$category.category}</a> OR <a href="{$ADMIN_URL}/cuisine_add.php?cat={$category.parentid}">Add a new category under the same level</a>
<br />
</div>
{include file="common/loading_js.tpl" divname="siteLoader" message="Saving Data...."}

<br>
<div id="SomeElementId"></div>
{capture name=dialog}
<form name="form1" id="form1" {if $use_ajax}action="javascript:void(null);" onsubmit="submitCategory();"{else} action="{$PHP_SELF}" method="post"{/if} >
  <table width="100%"  border="0" cellspacing="0" cellpadding="4">
    <tr>
      <td colspan="2">
      <input type="hidden" name="cat" value="{if $smarty.get.cat}{$smarty.get.cat}{else}{$smarty.post.cat}{/if}">
      <input type="hidden" name="mode" value="modify"></td>
    </tr>
    <tr>
      <td colspan="2"><TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
        <TR>
          <TD class="productDetailsTitle">{$lang.cat_icon}</TD>
        </TR>
        <TR>
          <TD class="Line" height="1"><IMG src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><BR></TD>
        </TR>
        <TR>
          <TD height="10"><IMG src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><BR></TD>
        </TR>
      </TABLE></td>
    </tr>
    <tr>
      <td colspan="2">
	  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
  <div><img src="{if $category.image}{$category.image}{else}{$product_IMAGES}{$DEFAULT_THUMB}{/if}" name="preview"  border="1">
			  <br>
              <input type="hidden" name="image" {if $category.image}value="{$category.image}"{/if} id="image">
              <input name="tmpimage" type="hidden" id="tmpimage" {if $category.image}value="{$category.image}"{/if}>		  
		<input type="button" name="select" value="{$lang.add_image}" onclick="ImageSelector.select('image','form1');"/>
  </div>
              <input name="button2" type=button value="{$lang.delete_image}" onclick='javascript: if (confirm("{$lang.u_sure|strip_tags}")){ldelim}delete_image();{rdelim}'>
              <input type="hidden" name="newthumb" {if $category.newthumb}value="{$category.newthumb}"{/if}>
              <input type="hidden" name="image_folder" {if $category.image_folder}value="{$category.image_folder}"{/if}>
              <input name="tmpimage" type="hidden" id="tmpimage" {if $category.image}value="{$category.image}"{/if}></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td width="120">{$lang.pos}</td>
      <td><input type="text" name="orderby" value="{$category.order_by}"></td>
    </tr>
    <tr>
      <td>{$lang.category}</td>
      <td><input name="title" type="text" id="title" value="{$category.category}">{if $error_list.category ne ""} <img src="{$ImagesDir}/error.gif"> <font color="#FF0000">{$error_list.category}</font>{/if}</td>
    </tr>
	{if $allcategories}
    <tr>
      <td>{$lang.parent_category}</td>
      <td>
<select name="parent_catid">     
<option value="0">{$lang.root_category}</option>    
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $category.parentid}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select>
	  </td>
    </tr>
{/if}
    <tr>
      <td>{$lang.description}</td>
      <td>
 		{$description->set_name('desc')}
		{$description->removebuttons('spacer3, spacer4')}
		{$description->set_code($category.description)}
		{$description->print_editor('550', 450)}
	  </td>
    </tr>
    <tr>
      <td>{$lang.meta_desc}</td>
      <td><textarea name="meta_desc" cols="65" rows="4">{$category.meta_descr}</textarea></td>
    </tr>
    <tr>
      <td>{$lang.meta_keyw}</td>
      <td><textarea name="meta_keyw" cols="65" rows="4">{$category.meta_keywords}</textarea></td>
    </tr>
    <tr>
      <td><input type="submit" name="Submit" value="{$lang.save}" id="submitButton" class="button" /></td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
{/capture}{include file="admin/dialog.tpl" locations=$smarty.capture.dialog title=$action_title extra="width=100%"}
{if $category}
<br>
{capture name=dialog}
<form name="form2" method="post" action="{$PHP_SELF}" id="form2">
  <table width="100%"  border="0" cellspacing="0" cellpadding="2">
    <TR class="TableHead">
      <TD>{$lang.language}</TD>
      <TD>{$lang.category}</TD>
      <TD>{$lang.description}</TD>
      <TD>{$lang.meta_desc}</TD>
      <TD>{$lang.meta_keyw}</TD>
      <TD>{$lang.image}</TD>
      <TD>&nbsp;</TD>
    </TR>
{if $trans}
	{section name=b loop=$trans}
    <tr valign="top">
      <td><SPAN class='productTitle'>{$trans[b].country}</span></td>
      <td><input type="text" size="20" name="category${$trans[b].code}" value="{$trans[b].category}"></td>
      <td><TEXTAREA name="description${$trans[b].code}" cols="30" rows="6">{$trans[b].description}</TEXTAREA><br>
	    <input name="button4" type="button" onclick="openEditor(document.form2.description${$trans[b].code});" value="{$lang.advanced_edit}"/></td>
      <td><TEXTAREA name="meta_descr${$trans[b].code}" cols="30" rows="6">{$trans[b].meta_descr}</TEXTAREA><br>
      </td>
      <td><TEXTAREA name="meta_keywords${$trans[b].code}" cols="30" rows="6">{$trans[b].meta_keywords}</TEXTAREA><br>
      </td>
      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>  <div><img src="{if $trans[b].image}{$trans[b].image}{else}{$product_IMAGES}{$DEFAULT_THUMB}{/if}" name="preview"  border="1">
			  <input type="hidden" name="image${$trans[b].code}" id="image${$trans[b].code}" {if $trans[b].image}value="{$trans[b].image}"{/if}>
			  <br>

		<input type="button" name="select" value="{$lang.add_image}" onclick="ImageSelector.select('image${$trans[b].code}','form2');"/>
  </div></td>
        </tr>
      </table></td>
      <td><input name="button2" type=button value="{$lang.delete}"  onclick='javascript: if (confirm("{$lang.u_sure|strip_tags}")){ldelim}self.location="{$_PHP_SELF}?mode=del&code={$trans[b].code}&cat={$category.categoryid}";{rdelim}'></td>
    </tr>
	{/section}
    <tr valign="top">
      <td><input type="submit" name="Submit" value="{$lang.update}">
        <input type="hidden" value="{if $smarty.get.cat}{$smarty.get.cat}{else}{$smarty.post.cat}{/if}" name="cat">          
        <input type="hidden" name="mode" value="upd_lng"></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<br>
{/if}
{if $cat_lng}
<form name="f" method="post" action="{$PHP_SELF}" id="f">
  <table width="100%"  border="0" cellspacing="0" cellpadding="2">
    <tr valign="top">
      <td colspan="6"><TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
        <TR>
          <TD class="productDetailsTitle">{$lang.add_lang}</TD>
        </TR>
        <TR>
          <TD class="Line" height="1"><IMG src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><BR></TD>
        </TR>
        <TR>
          <TD height="10"><IMG src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><BR></TD>
        </TR>
      </TABLE></td>
    </tr>
    <tr valign="top">
     <td><select name="cat_lng">
     {section name=a loop=$cat_lng}       
        <option value="{$cat_lng[a].code}">{$cat_lng[a].country}</option>
{/section}
</select></td>
      <td><INPUT type="text" size="20" name="cat_new_title"></td>
      <td><textarea name="cat_new_text" cols="30" rows="6"></textarea>
        <br>
      <input type="button" value="{$lang.advanced_edit}" onclick="openEditor(document.form2.cat_new_text);"/></td>
      <td><textarea name="meta_description_new" cols="30" rows="6"></textarea>
      <br>
      </td>
      <td><textarea name="meta_keywords_new" cols="30" rows="6"></textarea>
      <br>
      </td>
    </tr>
    <tr valign="top">
      <td><input type="submit" name="Submit" value="{$lang.add}">
	        <input type="hidden" value="{if $smarty.get.cat}{$smarty.get.cat}{else}{$smarty.post.cat}{/if}" name="cat">
      <input type="hidden" name="mode" value="insert"></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>

    </tr>
  </table>
</form>
{/if}
{/capture}{include file="admin/dialog.tpl" locations=$smarty.capture.dialog title=$lang.international_descriptions extra="width=100%"}
{/if}