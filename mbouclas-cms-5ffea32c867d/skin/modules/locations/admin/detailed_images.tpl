{include file="modules/locations/admin/menu.tpl"}
{include file="modules/locations/search_box.tpl" mode="small_admin" action_title=$locations.title}
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}
<div class="wrap">

<div id="zeitgeist">
  <h3>{$lang.in_this_section}</h3>
<div><img src="{if $locations.image}{$URL}{$locations.image}{else}{$locations_IMAGES}{$DEFAULT_THUMB}{/if}" name="preview"  border="1" align="left" style="border:#000000 1px solid">
<div style=" margin-left:130px;">{include file="modules/locations/admin/additional_links.tpl"}</div>
<Br />
<h3>More locations...</h3>
<ul>
{section name=a loop=$more_locations}
<li>{if $locations.id eq $more_locations[a].id}<strong>{$more_locations[a].title}</strong>{else}<a href="{$MODULE_FOLDER}/locations_modify_img.php?id={$more_locations[a].id}">{$more_locations[a].title}</a>{/if}</li>
{/section}
</ul>
</div>

</div>

<h2>{$lang.media} (<a href="#view">Inspect Uploaded media</a>)</h2>
<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
    <TR>
      <TD class="Line" height="1"><IMG src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><BR></TD>
    </TR>
    <TR>
      <TD height="10"><IMG src="/skin1/images/spacer.gif" width="1" height="1" border="0" alt=""><BR></TD>
    </TR>
</TABLE>

<div>
<form name="images_form" id="images_form">
{include file="common/uploader.tpl" mode="mass_product_images" form="images_form" id=$id redir="locations_modify_img.php" type=$type}
<input name="create_set" type="checkbox" id="create_set" value="1" checked="checked" /> 
Create Image Set
</form>
</div>
  <br>
<a name="view" id="view"></a>
<FORM action="{$PHP_SELF}#view" method="POST" name="processcategoryform" id="processcategoryform">
<div>
{section name=a loop=$image_categories}<span class="{if $image_categories[a].id eq $type}wpv-tab-button{else}wpv-tab-button-disabled{/if}"><a href="{$MODULE_FOLDER}/locations_modify_img.php?id={$id}&type={$image_categories[a].id}#view">{$image_categories[a].title}</a></span>
{/section}</div>
<div style="    border: 5px solid #c0c0c0;
    border-top: 10px solid #c0c0c0;
    border-bottom: 10px solid #c0c0c0;
    padding: 5px 5px 5px 5px;
    margin: 0px 0px 0px 0px; ">
<TABLE width="100%" border="0" cellpadding="2" cellspacing="1" class="widefat">



<INPUT type="hidden" name="cat_org" value="{$smarty.get.cat|escape:"html"}"> {assign var="form_name" value="processcategoryform"} 
<TR>
  <td colspan="8"><select name="image_cat" id="image_cat">
{section name=b loop=$image_categories}
{if $image_categories[b].id ne 0}
<option value="{$image_categories[b].id}" {if $image_categories[b].id eq $type}selected{/if}>{$image_categories[b].title}</option>
{/if}
{/section}
</select>
    <input type="button" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim};document.{$form_name}.action.value="delete_image_category"; document.{$form_name}.submit();{rdelim}' value="{$lang.delete_category}"/>
    <br />
    <input type="text" name="new_image_category" id="new_image_category" />    </td>
</TR>
<TR>
<th scope="col">&nbsp;</th>
<th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
<th scope="col">{$lang.thumb}</th>
<th scope="col">{$lang.pos}</th>
<th scope="col">{$lang.availability}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.alt}</th>
<th scope="col">{$lang.title}</th>
</TR>
{if $images}

{section name=a loop=$images}
<TR {cycle values=", class='TableSubHead'"}>
<TD width="1%"><a href="{$URL}/{$images[a].main}" target="_blank"><img src="{$URL}/{$images[a].thumb}" width="30" height="30"></a></TD>
<TD width="2%"><input name="ids[{$images[a].id}]" type="checkbox" id="check_values" value="1"></TD>
<TD width="2%" align="center"><input type="radio" name="thumb" id="thumb" value="{$images[a].id}" {if $images[a].type eq 0}checked{/if} /></TD>
<TD width="2%"><input name="orderby-{$images[a].id}" type="text" value="{$images[a].orderby}" size="3" maxlength="3"></TD>
<TD><select name="available-{$images[a].id}">
<option value="0" {if $images[a].available eq "0"}selected{/if}>{$lang.disabled}</option>
<option value="1" {if $images[a].available eq "1"}selected{/if}>{$lang.enabled}</option>
</select></TD>
<TD><select name="type-{$images[a].id}" id="type-{$images[a].id}">
{section name=b loop=$image_categories}
<option value="{$image_categories[b].id}" {if $image_categories[b].id eq $images[a].type}selected{/if}>{$image_categories[b].title}</option>
{/section}
</select></TD>
<TD><input type="text" value="{$images[a].alt}" name="alt-{$images[a].id}"></TD>
<TD><input type="text" value="{$images[a].title}" name="title-{$images[a].id}"></TD>
</TR>






{/section}

{else}

<TR>
<TD colspan="8" align="center">{$lang.txt_no_images}</TD>
</TR>

{/if}
<TR>
<TD colspan="8"><BR>
<INPUT type="button" class="button" onClick="document.processcategoryform.mode.value='modify'; document.processcategoryform.submit();" value="{$lang.update}">
&nbsp;
<input name="button" type="button" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete_images"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
&nbsp;
<input name="button" type="button" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate_all_images"; document.{$form_name}.submit();{rdelim}' value="Activate ALL" /></TD>
</TR>
</table>
</div>
<input type="hidden" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}" name="id">
<input type="hidden" name="action" value="modify">
<input type="hidden" value="{$type}" name="type" />
<input type="hidden" name="page" id="page" value="locations_modify_img.php?id={$id}&type={$type}#view">
<input type="hidden" name="mode" id="mode" value="">
</FORM>
