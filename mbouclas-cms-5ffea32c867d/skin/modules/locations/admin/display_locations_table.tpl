{if $type eq "edit"}
<table cellpadding="0" cellspacing="0" class="widefat">
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
<th scope="col">{$lang.provider}</th>
</tr>

{section name=w loop=$locations}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$locations[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$locations[w].id}</strong></td>
            <td><a href="locations_modify.php?id={$locations[w].id}">{$locations[w].title}</a></td>
            <td><a href="category_locations.php?cat={$locations[w].catid}">{$locations[w].category|default:$lang.root_category}</a></td>
            <td>{$locations[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $locations[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td>{if $ADMIN eq 1}<a href="users_locations.php?id={$locations[w].uid}">{$locations[w].user.uname}</a>{else}{$locations[w].provider}{/if}</td>
    </tr>
		  {/section}
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
  <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "editor"}
<table cellpadding="3" cellspacing="0" class="widefat">
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.availability}</th>
</tr>

{section name=w loop=$locations}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$locations[w].id}]"  type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$locations[w].id}</strong></td>
            <td><img src="{$locations[w].image}" /></td>
            <td><input type="text" name="locations_title-{$locations[w].id}" value="{$locations[w].title}" onkeyup="document.getElementsByName('ids[{$locations[w].id}]')[0].checked='true'" /></td>
            <td><select name="categoryid-{$locations[w].id}"  onchange="document.getElementsByName('ids[{$locations[w].id}]')[0].checked='true'">
                       
{section name=cat_num loop=$allcategories}

              <option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $locations[w].catid}selected{/if}>{$allcategories[cat_num].category}</option>
              
{/section}

            </select>            </td>
    <td align="center"><select name="active-{$locations[w].id}"  onchange="document.getElementsByName('ids[{$locations[w].id}]')[0].checked='true'">
        <option value="0" {if $locations[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $locations[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
    </select></td>
    </tr>
		  {/section}
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "search_results_admin"}
<table cellpadding="3" cellspacing="0" class="widefat">
      <tr>
        <td colspan="9">{include file="modules/locations/pagination.tpl"}</td>
      </tr>

      <tr>
        <th colspan="7" scope="col"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />       </th>
      </tr>
  <tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">{$lang.pos}</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.availability}</th>
</tr>

{section name=w loop=$locations}
          <tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$locations[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong><a href="{$MODULE_FOLDER}/locations_modify.php?id={$locations[w].id}">{$locations[w].id}</a></strong></td>
            <td><a href="{$MODULE_FOLDER}/locations_modify.php?id={$locations[w].id}"><img src="{$locations[w].image}" /></a></td>
            <td><input name="orderby-{$locations[w].id}" type="text" id="orderby-{$locations[w].id}" size="3" value="{$locations[w].orderby}" onkeyup="document.getElementsByName('ids[{$locations[w].id}]')[0].checked='true'"  /></td>
            <td><input type="text" name="locations_title-{$locations[w].id}" value="{$locations[w].title}" onkeyup="document.getElementsByName('ids[{$locations[w].id}]')[0].checked='true'" /></td>
            <td><select name="categoryid-{$locations[w].id}" onchange="document.getElementsByName('ids[{$locations[w].id}]')[0].checked='true'">         
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $locations[w].catid}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select></td>
            <td align="center"><select name="active-{$locations[w].id}" onchange="document.getElementsByName('ids[{$locations[w].id}]')[0].checked='true'">
        <option value="0" {if $locations[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $locations[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select></td>
    </tr>
		  {/section}
      <tr>
        <td colspan="9"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}{if $search AND !$QUERY_STRING}?search=1{elseif $search AND $QUERY_STRING}&search=1{/if}">
       <input type="hidden" name="mode" id="mode" value="" /></td>
      </tr>
</table>
{else}
<table cellpadding="0" cellspacing="0" class="widefat">
<thead>
<tr>
  <th scope="col">ID</th>
<th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
<th scope="col">{$lang.provider}</th>
</tr>
</thead>
{section name=w loop=$locations}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><strong>{$locations[w].id}</strong></td>
            <td><a href="locations_modify.php?id={$locations[w].id}">{$locations[w].title}</a></td>
            <td><a href="category_locations.php?cat={$locations[w].catid}">{$locations[w].category}</a></td>
            <td>{$locations[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $locations[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td>{if $ADMIN eq 1}<a href="users_locations.php?id={$locations[w].uid}">{$locations[w].user.uname}</a>{else}{$locations[w].provider}{/if}</td>
    </tr>
		  {/section}
</table>
{/if}