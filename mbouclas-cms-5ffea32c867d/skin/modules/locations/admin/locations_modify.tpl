{include file="modules/locations/admin/menu.tpl"}
{include file="modules/locations/search_box.tpl" mode="small_admin" action_title=$locations.title}
{if $error_list}{include file="common/error_list.tpl"}{/if}
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}

<div class="wrap">
{if $locations}
<div id="zeitgeist">
  <h3>{$lang.in_this_section}</h3>
<div><img src="{if $locations.image}{$locations.image}{else}{$locations_IMAGES}{$DEFAULT_THUMB}{/if}" name="preview"  border="1" align="left" style="border:#000000 1px solid">
<div style=" margin-left:130px;">{include file="modules/locations/admin/additional_links.tpl"}</div>
<Br />
<br />
<h3>More locations...</h3>
<ul>
{section name=a loop=$more_locations}
<li>{if $locations.id eq $more_locations[a].id}<strong>{$more_locations[a].title}</strong>{else}<a href="{$MODULE_FOLDER}/locations_modify.php?id={$more_locations[a].id}">{$more_locations[a].title}</a>{/if}</li>
{/section}
</ul>
</div>

</div>
{/if}
<h2>{$lang.classification}</h2>


<br><br>
<form name="form1" method="POST" action="{$PHP_SELF}" id="form1" style="width:99%;">
<input type="hidden" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}" name="id">
<input name="action" type="hidden" id="action" value="{$action|default:"add"}">
<table width="100"  border="0" cellspacing="0" cellpadding="4">
    <tr>
      <td>{$lang.pos}</td>
      <td><input name="orderby" type="text" id="orderby" size="3" value="{$locations.orderby}" /></td>
    </tr>
    <tr>
      <td>{$lang.availability}</td>
      <td><select name="active">
        <option value="0" {if $locations.active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $locations.active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select></td>
    </tr>
    <tr>
      <td width="120">{$lang.main_cat}</td>
      <td><select name="categoryid" id="categoryid" onchange="set_efields(document.getElementById('categoryid').options[document.getElementById('categoryid').selectedIndex].value,'extra_fields_div',{$locations.id|default:0})">         
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $locations.catid}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select>      </td>
    </tr>
    <tr>
      <td width="120">{$lang.aditional_cat}</td>
      <td>
      <SELECT name="categoryids[]" multiple size="8">
<option value="">{$lang.none}</option>
{section name=cat_num loop=$allcategories}
<OPTION value="{$allcategories[cat_num].categoryid}" {foreach from=$categoryids item=catid}{if $catid.catid eq $allcategories[cat_num].categoryid}selected{/if}{/foreach}>{$allcategories[cat_num].category}</OPTION>
{/section}
</SELECT></td>
    </tr>
  </table>
  <h2>{$lang.details}</h2>
  <table width="100%"  border="0" cellspacing="0" cellpadding="4">
  {if $locations_module.settings.use_sku}
    {/if}
    <tr>
      <td width="120"><FONT class="FormButton">{$lang.title}</font></td>
      <td><input name="locations_title" type="text" id="locations_title" size="45" value="{$locations.title}"></td>
    </tr>
    <tr>
      <td width="120"><FONT class="FormButton">{$lang.short_desc}</font></td>
      <td>
 		{$description->set_name('short_desc')}
		{$description->removebuttons('spacer3, spacer4')}
		{$description->set_code($locations.description)}
		{$description->print_editor('550', 150)}      </td>
    </tr>
    <tr>
      <td width="120"><FONT class="FormButton">{$lang.long_desc}</font></td>
      <td>
 		{$long->set_name('long_desc')}
		{$long->removebuttons('spacer3, spacer4')}
		{$long->set_code($locations.full_description)}
		{$long->print_editor('550', 450)}      </td>
    </tr>
  {if $locations_module.settings.use_locations_tags}
    <tr>
      <td>Tags</td>
      <td><input type="text" name="tags" id="tags" size="60" class="tags-input" value="{$tags}"></td>
    </tr>
    {/if}
    <tr>
      <td>{$lang.meta_desc}</td>
      <td><textarea name="meta_desc" cols="65" rows="4">{$locations.meta_desc}</textarea></td>
    </tr>
    <tr>
      <td>{$lang.meta_keyw}</td>
      <td><textarea name="meta_keywords" cols="65" rows="4">{$locations.meta_keywords}</textarea></td>
    </tr>
    </table>
    {if $extra_fields}
    
     <h2>{$lang.extra_fields}</h2>
     <div id="extra_fields_div">{include file="common/extra_fields_modify.tpl"}
     </div>
     {/if}
<input type="submit" name="Submit" value="{$lang.save}">
 
</form>

</div>