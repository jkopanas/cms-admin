{include file="modules/locations/admin/menu.tpl"}
{include file="modules/locations/search_box.tpl" mode="small_admin" action_title=$locations.title}
<div class="wrap">
<div id="zeitgeist">
  <h3>{$lang.in_this_section}</h3>
<div><img src="{if $locations.image}{$locations.image}{else}{$locations_IMAGES}{$DEFAULT_THUMB}{/if}" name="preview"  border="1" align="left" style="border:#000000 1px solid">
<div style=" margin-left:130px;">{include file="modules/locations/admin/additional_links.tpl"}</div>
<Br />
<h3>More locations...</h3>
<ul>
{section name=a loop=$more_locations}
<li>{if $locations.id eq $more_locations[a].id}<strong>{$more_locations[a].title}</strong>{else}<a href="{$MODULE_FOLDER}/locations_modify.php?id={$more_locations[a].id}">{$more_locations[a].title}</a>{/if}</li>
{/section}
</ul>
</div>

</div>
 <h2>{$lang.featured_locations} (<a href="#featured_add">add new</a>)</h2>
 <table width="600" cellpadding="4" cellspacing="1" id="FeaturedTable">

  <tr>
    <td valign="top">
<div>
<a name="featured_add" id="featured_add"></a>
<form name="search_cat" id="search_cat">
<input type="hidden" name="featured" value="0" id="featured" />
<input type="hidden" name="locationsid" value="{$id}" id="locationsid" />
<SELECT name="cat" size="10" style="width: 100%" onChange="submitGetCategory('search_cat','cat_results','cat');return false;">
{section name=cat_idx loop=$allcategories}
<OPTION value="{$allcategories[cat_idx].categoryid}"{if $smarty.get.cat eq $allcategories[cat_idx].categoryid} selected{/if}>{$allcategories[cat_idx].category_path}{$allcategories[cat_idx].category}</OPTION>
{/section}
</SELECT>
</form>
</div></td>
    <td><div id="cat_results" style="border:#000000 solid 1px"></div></td>
  </tr>
 </table>
 <br />
 <div id="related_products" style="width:600px">{include file="modules/locations/admin/related_locations_table.tpl" links=$related}</div>
 <div style="clear:both"></div>
</div>