{include file="modules/locations/admin/menu.tpl"}
{include file="modules/locations/search_box.tpl" mode="small_admin"}
<div class="wrap">
  <h2 class="tab">{$lang.latest_locations}</h2>
<br><br>
<form name="latest_locations_form" method="post">
{include file="modules/locations/admin/display_locations_table.tpl" locations=$latest_locations type="edit" form_name="latest_locations_form" page="index.php"}
</form>
</div>