{if $type eq "edit"}
<table cellpadding="0" cellspacing="0" class="widefat">
      <tr>
        <td colspan="7"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onClick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
</tr>

{section name=w loop=$maps}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$maps[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$maps[w].id}</strong></td>
            <td><a href="map_modify.php?id={$maps[w].id}">{$maps[w].title}</a></td>
            <td><a href="category_products.php?cat={$maps[w].catid}">{$maps[w].category|default:$lang.root_category}</a></td>
            <td>{$maps[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $maps[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
  </tr>
		  {/section}
      <tr>
        <td colspan="7"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
  <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "item"}
<table cellpadding="3" cellspacing="0" class="widefat">
<tr>
<th scope="col">Title</th>
</tr>
{foreach from=$maps item=a}
<tr><td><a href="products_modify_map.php?id={$id}&mapid={$a.id}">{$a.title}</a></td></tr>
{/foreach}
</table>
{elseif $type eq "editor"}
<table cellpadding="3" cellspacing="0" class="widefat" width="{$WIDTH}">
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onClick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.availability}</th>
</tr>

{section name=w loop=$maps}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$maps[w].id}]"  type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$maps[w].id}</strong></td>
            <td><img src="{$URL}/{$maps[w].image}" /></td>
            <td><input type="text" name="products_title-{$maps[w].id}" value="{$maps[w].title}" onKeyUp="document.getElementsByName('ids[{$maps[w].id}]')[0].checked='true'" /></td>
            <td><select name="categoryid-{$maps[w].id}"  onchange="document.getElementsByName('ids[{$maps[w].id}]')[0].checked='true'">
                       
{section name=cat_num loop=$allcategories}

              <option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $maps[w].catid}selected{/if}>{$allcategories[cat_num].category}</option>
              
{/section}

            </select>            </td>
    <td align="center"><select name="active-{$maps[w].id}"  onchange="document.getElementsByName('ids[{$maps[w].id}]')[0].checked='true'">
        <option value="0" {if $maps[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $maps[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
    </select></td>
  </tr>
		  {/section}
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "search_results_admin"}
<table cellpadding="3" cellspacing="0" class="widefat">
      <tr>
        <td colspan="9">{include file="modules/products/pagination.tpl"}</td>
      </tr>

      <tr>
        <th colspan="7" scope="col"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />       </th>
      </tr>
  <tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onClick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">{$lang.pos}</th>
  <th scope="col">{$lang.title}</th>
{if $eshop_module}
<th scope="col">{$lang.price}</th>
{/if}
{if $locations_module}
<th scope="col">{$lang.location}</th>
{/if}
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.availability}</th>
</tr>

{section name=w loop=$maps}
          <tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$maps[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong><a href="{$MODULE_FOLDER}/products_modify.php?id={$maps[w].id}">{$maps[w].id}</a></strong></td>
            <td><a href="{$MODULE_FOLDER}/products_modify.php?id={$maps[w].id}"><img src="{$URL}/{$maps[w].image}" /></a></td>
            <td><input name="orderby-{$maps[w].id}" type="text" id="orderby-{$maps[w].id}" size="3" value="{$maps[w].orderby}" onKeyUp="document.getElementsByName('ids[{$maps[w].id}]')[0].checked='true'"  /></td>
            <td><input type="text" name="products_title-{$maps[w].id}" value="{$maps[w].title}" onKeyUp="document.getElementsByName('ids[{$maps[w].id}]')[0].checked='true'" /></td>
            {if $eshop_module}
            <td><input type="text" name="eshop_price-{$maps[w].id}" id="eshop_price-{$maps[w].id}" value="{$maps[w].eshop.price}"  onkeyup="document.getElementsByName('ids[{$maps[w].id}]')[0].checked='true'"/></td>
            {/if}
            {if $locations_module}
            <td><select name="locations_id-{$maps[w].id}" id="locations_id-{$maps[w].id}" onChange="document.getElementsByName('ids[{$maps[w].id}]')[0].checked='true'">
            {section name=a loop=$all_locations}
            <option value="{$all_locations[a].id}" {if $maps[w].locations.id eq $all_locations[a].id}selected{/if}>{$all_locations[a].category} - {$all_locations[a].title}</option>
            {/section}
            </select></td>
            {/if}
            <td><select name="categoryid-{$maps[w].id}" onChange="document.getElementsByName('ids[{$maps[w].id}]')[0].checked='true'">         
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $maps[w].catid}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select></td>
            <td align="center"><select name="active-{$maps[w].id}" onChange="document.getElementsByName('ids[{$maps[w].id}]')[0].checked='true'">
        <option value="0" {if $maps[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $maps[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select></td>
    </tr>
		  {/section}
      <tr>
        <td colspan="9"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}{if $search AND !$QUERY_STRING}?search=1{elseif $search AND $QUERY_STRING}&search=1{/if}">
       <input type="hidden" name="mode" id="mode" value="" /></td>
      </tr>
</table>
{else}
<table cellpadding="0" cellspacing="0" class="widefat">
<thead>
<tr>
  <th scope="col">ID</th>
<th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
<th scope="col">{$lang.provider}</th>
</tr>
</thead>
{section name=w loop=$maps}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><strong>{$maps[w].id}</strong></td>
            <td><a href="products_modify.php?id={$maps[w].id}">{$maps[w].title}</a></td>
            <td><a href="category_products.php?cat={$maps[w].catid}">{$maps[w].category}</a></td>
            <td>{$maps[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $maps[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td>{if $ADMIN eq 1}<a href="users_products.php?id={$maps[w].uid}">{$maps[w].user.uname}</a>{else}{$maps[w].provider}{/if}</td>
  </tr>
		  {/section}
</table>
{/if}