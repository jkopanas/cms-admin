{include file="modules/maps/admin/menu.tpl"}
<div id="toolbar">
		<h1>{$map.title}</h1>
        <div style="float:left">
		<ul id="options">
			<li><a href="map_modify.php?id={$map_id}&type=product">Products</a></li>
			<li><a href="map_modify.php?id={$map_id}&type=content">Content</a></li>
			<li><a href="map_modify.php?id={$map_id}&type=location">Locations</a></li>
		</ul>
	<ul id="categories">
    {foreach from=$allcategories item=a}
    <li><a href="#" rel="{$a.categoryid}">{$a.category}</a></li>
    {/foreach}
    </ul>
    Properties: <input type="checkbox" id="productsbox" onclick="boxclick(this,'products')" /> &nbsp;&nbsp;
	</div>
	</div>
    <div style="clear:both"></div>
    <div id="errors"></div>
    <div style="clear:both"></div>
	<div id="content" class="sidebar-right">
		<div id="map-wrapper">
			<div id="map"></div>
		</div>
		<div id="sidebar">
        <div id="missing_items"></div>
      	<div id="sidebarContent">
			<ul id="sidebar-list">
            
			</ul>
            </div>
            Properties: <input type="checkbox" id="productsbox" onclick="boxclick(this,'products')" /> &nbsp;&nbsp;
		</div><!-- END SIDEBAR -->
	</div>
{literal}
<script>
(function($){
var map = initGmap({mapCenter: [{/literal}{$map.map_x},{$map.map_y}],  mapZoom:{$map.config.mapZoom}, mapControlSize:'large',map_id:{$map_id}{literal}},"map");
$("#categories a").click(function(){
var catid = $(this).attr('rel');


initGmap.addFeed(map,{sidebar_div:'sidebarContent',missing_items: 'missing_items',catid: catid, feedUrl:'/ajax/draw_map.php?catid=' + catid + '&mode={/literal}{$type}&active=1&mapid={$map_id}&EditMode=1',type:'{$type}', active:1, pointIsDraggable:'true', EditMode: 1,map_id:{$map_id},UseSidebar:1 {literal}});
});
})(jQuery);
</script>
{/literal}