{include file="modules/news/admin/menu.tpl"}
{include file="modules/news/search_box.tpl" mode="small_admin" action_title=$news.title}
<div class="wrap">

<h2><a href="{$MODULE_FOLDER}/categories.php">{$lang.categories}</a> (<a href="{$MODULE_FOLDER}/categories.php#addcat">add new</a>) {include file="modules/news/nav_categories.tpl" management=$lang.categories target="admin_categories"}</h2>


<form name="latest_news_form" method="post">
{include file="modules/news/admin/display_news_table.tpl" news=$news type="edit" form_name="latest_news_form" page="category_news.php"}
</form>
</div>