{include file="modules/news/admin/menu.tpl"}
{include file="modules/news/search_box.tpl" mode="small_admin"}
<div class="wrap">
  <h2 class="tab">{$lang.latest_news}</h2>
<br><br>
<form name="latest_news_form" method="post">
{include file="modules/news/admin/display_news_table.tpl" news=$latest_news type="edit" form_name="latest_news_form" page="index.php"}
</form>
</div>