<ul id="adminmenu2">
	<li><a href="{$MODULE_FOLDER}/" {if $submenu eq "main"}class="current"{/if}>{$lang.home}</a></li>
	<li><a href="{$MODULE_FOLDER}/news_new.php" {if $submenu eq "add"}class="current"{/if}>Add News</a></li>
	<li><a href="{$MODULE_FOLDER}/search.php" {if $submenu eq "search"}class="current"{/if}>Search News</a></li>
	<li><a href="{$MODULE_FOLDER}/search.php" {if $submenu eq "modify"}class="current"{/if}>Modify News</a></li>
    <li><a href="{$MODULE_FOLDER}/categories.php" {if $submenu eq "categories"}class="current"{/if}>Categories</a></li>
    <li><a href="{$MODULE_FOLDER}/extra_fields.php" {if $submenu eq "extra_fields"}class="current"{/if}>Extra Fields</a></li>
	<li><a href="{$MODULE_FOLDER}/settings.php" {if $submenu eq "settings"}class="current"{/if}>Module Settings</a></li>
</ul>