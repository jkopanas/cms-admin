{literal}
<script type="text/javascript">
		function submitCategory()
		{
			xajax.$('submitButton').disabled=true;
			xajax.$('submitButton').value="please wait...";
			xajax_processForm(xajax.getFormValues("form1"));
			xajax.$('SomeElementId').innerHTML = '';
			xajax.$('NavMenu').innerHTML = '';
			return false;
		}
		function submitSuggest(search_field)
		{
			xajax.$('search_suggest').value='';
			xajax_autoSuggest(escape(document.getElementById(search_field).value));
			return false;
		}
  //This dispatches the XAJAX in the server to get the table data.
  function tab_go(sid,target)
  {
    xajax.$(target).innerHTML = ''; 
    xajax_tab_go(sid);
  }
function SubmitdeleteCat(i,table,cat)
{
document.getElementById(table).deleteRow(i)
xajax_DeleteCategory(xajax.getFormValues("categories_form"),cat);
return false;

}
function SubmitUpdateCat(i,table,cat)
{
			xajax.$('ModifyButton').disabled=true;
			xajax.$('ModifyButton').value="please wait...";
xajax_UpdateCategories(xajax.getFormValues("categories_form"));
return false;
}

function submitIngredients(t)
{
xajax.$('Submit2').disabled=true;
xajax.$('Submit2').value="please wait...";
xajax.$('ingredients_list').value = t;
xajax.$('ingredients_div').innerHTML = '';
xajax_editIngredients(xajax.getFormValues("form1"));
return false;
}

function write_suggest_script(iteration)
{
  var writer = '\n<script type=\"text/javascript\">\n';
  writer += 'var as' + iteration + ' = new AutoSuggest(\'base$' + iteration + '\', options1);\n';
  writer += "<\/script>\n";
  document.getElementById('scripts').innerHTML += writer;
 }
function updateOrder()
{
xajax.$('movies_1').value = Sortable.serialize('movies_list');
}

function add_list(n)
{
var anchorTags = document.getElementById("movies_list");
for (var i = 0; i < anchorTags.length ; i++)
{
   alert("Href of " + i + "-th element is : " + anchorTags[i].href + "\n");
}
//document.getElementById("movies_list").innerHTML +="<li>asdasd</li>";
}

function deleteLastRow(tblId)
{var tbl=document.getElementById(tblId);if(tbl.rows.length>0)tbl.deleteRow(tbl.rows.length-1);}

function addRowToTrivia(tableid)
{
  var tbl = document.getElementById(tableid);
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
  
  // left cell
  var cellLeft = row.insertCell(0);
  var textNode = document.createTextNode(iteration  + '.');
  cellLeft.appendChild(textNode);
  
 
    // right cell
  var cellRight = row.insertCell(1);
  cellRight.align = "center";
  var el = document.createElement('select');
  el.name = 'letter$' + iteration;
  el.id = 'letter$' + iteration;
  
   var y=document.createElement('option');

  {/literal}
  {foreach from=$alphabet item=k}
  var y=document.createElement('option');
  y.text='{$k|capitalize}';
  y.value='{$k}';
  {literal}
  try
    {
    el.add(y,null); // standards compliant
    }
  catch(ex)
    {
    el.add(y); // IE only
    }
	{/literal}
	{/foreach}
	{literal}
  cellRight.appendChild(el);
  //cellRight1.appendChild(el1);
  
    // right cell
  var cellRight1 = row.insertCell(2);
  cellRight1.align = "center";
  var el = document.createElement('input');
  el.type = 'text';
  el.name = 'tag$' + iteration;
  el.id = 'tag$' + iteration;

  cellRight1.appendChild(el);
  
//write_suggest_script(iteration);
}

function overlay(id,obj) 
{
	//Dialog.alert($(obj).innerHTML, {windowParameters: {className: "alphacube",  width:250}})
	Dialog.alert({url: "/manage/popup_news.php{/literal}{if $bidirectional}?bidirectional=1{/if}{literal}", options: {method: 'get'}}, {windowParameters: {className: "alphacube", width:800}, okLabel: "Close", overlayShowEffectOptions:{duration:0.0} });
	return false;
}

function submitGetCategory(form,res_div,mode)
{
display_ajax_loader(res_div);
xajax_get_search_categories(xajax.getFormValues(form),res_div,mode);
}

function submitDeleteBookmark(list,node,productid,res_div)
{
display_ajax_loader(res_div);
document.getElementById(list).removeChild(document.getElementById(list).childNodes[node - 1]);//Remove from list
xajax_delete_bookmark(productid,res_div);
}

function submitAddBookmark(list,form,res_div)
{
xajax_add_bookmark(xajax.getFormValues(form),res_div);
display_ajax_loader(res_div);
}

function add_list_element(name)
{
var list=document.getElementById(name);
var iteration = list.childNodes.length + 1;
var newText = document.createTextNode(iteration + '. ');
	var newNode = document.createElement("li");
	newNode.id = 'item_' + iteration;
	newNode.appendChild(newText);
	list.appendChild(newNode);
	
  var el6 = document.createElement('input');
  el6.type = 'hidden';
  el6.name = 'ingredient_order$' + iteration;
  el6.id = 'ingredient_order$' + iteration;
  el6.value = iteration;
  
  var el7 = document.createElement('div');
  el7.style.display = 'none';
  el7.id = 'ingr' + iteration + '_disp';
  el7.className = 'autocomplete_choices';
  
  var el = document.createElement('input');
  el.type = 'text';
  el.name = 'quantity$' + iteration;
  el.id = 'txtRow' + iteration;
  el.size = 5;
  
  var el2 = document.createElement('input');
  el2.type = 'text';
  el2.name = 'ingredient$' + iteration;
  el2.id = 'ingredient$' + iteration;
  el2.size = 45;  
  
  var el3 = document.createElement('input');
  el3.name = 'base$' + iteration;
  el3.onkeyup = function() { auto('base$'+iteration); };
  el3.autocomplete = 'off';
  el3.type = 'text';
  el3.id = 'base$' + iteration;
  el3.value = "";
  
  
  var el4 = document.createElement('input');
  el4.type = 'checkbox';
  el4.name = 'active$' + iteration;
  el4.id = 'active$' + iteration;
  el4.value = "1";

  var el8 = document.createElement('button');
  theText=document.createTextNode("up");
  el8.appendChild(theText);
  el8.onclick = function() { moveItem('up',this); return false; };

  var el9 = document.createElement('button');
  theText=document.createTextNode("down");
  el9.appendChild(theText);
  el9.onclick = function() { moveItem('down',this); return false; };

  var el5 = document.createElement('select');
  el5.name = 'unit$' + iteration;
  el5.id = 'txtRow' + iteration;
  
   var y=document.createElement('option');
  y.text='No Unit';
  y.value='0'; 
    try
    {
    el5.add(y,null); // standards compliant
    }
  catch(ex)
    {
    el5.add(y); // IE only
    }
  {/literal}
  {section name=a loop=$conversion_data}
  var y=document.createElement('option');
  y.text='{$conversion_data[a].name}';
  y.value='{$conversion_data[a].id}';
  {literal}
  try
    {
    el5.add(y,null); // standards compliant
    }
  catch(ex)
    {
    el5.add(y); // IE only
    }
	{/literal}
	{/section}
	{literal}

  newNode.appendChild(el6);
  newNode.appendChild(el);
  newNode.appendChild(document.createTextNode(" "));
  newNode.appendChild(el5);
  newNode.appendChild(document.createTextNode(" "));
  newNode.appendChild(el2);
  newNode.appendChild(document.createTextNode(" "));
  newNode.appendChild(el3);
  newNode.appendChild(el7);
  newNode.appendChild(document.createTextNode(" "));
  newNode.appendChild(el4);
  newNode.appendChild(el8);
  newNode.appendChild(el9);
  
}

function deleteListLastRow(listid)
{

var list=document.getElementById(listid);

if(list.childNodes.length>0)
{
list.removeChild(list.childNodes[list.childNodes.length-1]);
//alert(list.childNodes.length);
}

}//END FUNCTION

function deleteListElements(listid)
{

		var list = document.getElementById(listid);
		//Iterate the list

chk = list.getElementsByTagName("input");

	for(j=0;j<chk.length;j++)
	{
		if (chk[j].checked)
		{

		chk[j].parentNode.parentNode.removeChild(chk[j].parentNode);
		}
	}
for(i=0;i<list.childNodes.length;i++)
{


//list.removeChild(list.childNodes[i]);
}//END OF FOR
}//END OF FUNCTION

function addproduct(form,res_div,mode)
{
//xajax.$('submitButton').disabled=true;
xajax.$('submitButton').value="please wait...";
if (mode == 'add')
{
xajax_addproduct(xajax.getFormValues(form),res_div);
}
else
{
xajax_updateproduct(xajax.getFormValues(form),res_div);
}
display_ajax_loader('errors_bottom');
}

function ajax_menu_chooser(menu,res_div,id)
{
var ullist=document.getElementById('maintab');


document.getElementById('menu_main').className=""  //deselect all tabs
document.getElementById('menu_related').className=""  //deselect all tabs
document.getElementById('menu_'+menu).className = 'selected';
display_ajax_loader(res_div);
xajax_productMenu(menu,id);
}

function submitSetproduct(form,set_div)
{

	if (document.getElementById('featured').value == 0)
	{
	document.getElementById('id').value = document.getElementById('newsid').value
	xajax_set_related(xajax.getFormValues(form),set_div);
	}
	else if (document.getElementById('featured').value == 1)
	{
	xajax_set_featured(xajax.getFormValues(form),set_div);
	}
	else if (document.getElementById('featured').value == 2)
	{
	xajax_set_trivia_featured(xajax.getFormValues(form),set_div);
	}
	display_ajax_loader(set_div);
	return false;
}
function deleteRelated(id,set_div)
{
	if (document.getElementById('featured').value != 1)
	{
	xajax_deleteRelated(id,set_div);
	}
	else
	{
	xajax_deleteFeatured(id,set_div);
	}
	display_ajax_loader(set_div);
	return false;
}

function auto(text)
{
display_ajax_loader('inr_display');
xajax_autocomplete(document.getElementById(text).value,text);
}

function setAutoComplete(target,val)
{
document.getElementById(target).value = val;
}

function moveItem(direction, btn) {
	// get a reference to the LI

	var li = btn.parentNode;

	// get a reference to this input
	var inp = li.getElementsByTagName("input")[0];
	
	// get a reference to the UL
	var ul = li.parentNode;
	
	// get a collection of inputs in the UL
	var inps = ul.getElementsByTagName("input");

	switch (direction) {
	case "up":
		if (inp.value == 1) {
			// clicked item is already the first one
			return;
		} else {
			// switch the clicked li with the one above it
			for (var i=0; i < inps.length; i++) {
				if (inps[i].value == inp.value - 1) {
					inps[i].value = parseInt(inps[i].value) + 1;
					inp.value = parseInt(inp.value) - 1;
					
					// get a reference to the other LI
					var otherLI = inps[i].parentNode;
					
					// now move the LI up
					ul.insertBefore(li, otherLI);
					
					break;
				}
			}
		}
		break;
	case "down":
		if (inp.value == inps.length) {
			// clicked item is already the last one
			return;
		} else {
			// switch the clicked li with the one below it
			for (var i=0; i < inps.length; i++) {
				if (inps[i].value == parseInt(inp.value) + 1) {
					inps[i].value = parseInt(inps[i].value) - 1;
					inp.value = parseInt(inp.value) + 1;
					
					// get a reference to the other LI
					var otherLI = inps[i].parentNode;
					
					// now move the LI down
					ul.insertBefore(otherLI, li);
					
					break;
				}
			}
		}
		break;
	default:
		break;	
	}

}
function user_products(id,div)
{
xajax_user_products(id);
display_ajax_loader(div);
}
</script>
{/literal}