{include file="modules/news/admin/menu.tpl"}

<div class="wrap">
{if $all_modules}
<div id="zeitgeist">
  <h3>{$lang.in_this_section}</h3>

<ul>
{section name=a loop=$all_modules}
<li>{if $MODULE_CAT eq $all_modules[a].category}<strong>{$all_modules[a].category}</strong>{else}<a href="{$MODULE_FOLDER}/settings.php?cat={$all_modules[a].category}">{$all_modules[a].category}</a>{/if}</li>
{/section}
</ul>


</div>
{/if}
<h2>{$MODULE_PROPERTIES.name} {$lang.settings}</h2>
<form name="settings_form" id="settings_form" action="{$SELF}" method="post">
<table width="500" border="0">
{section name=a loop=$module}
<tr>
<td>{$module[a].name}</td>
<td>
{if $module[a].type eq "select"}
<select name="{$module[a].name}" id="{$module[a].name}">
{foreach key=key item=item from=$module[a].options_array}
{foreach key=k item=i from=$item}
<option value="{$i}" {if $module[a].value eq $i}selected{/if}>{$key}</option>
{/foreach}
{/foreach}
</select>
{elseif $module[a].type eq "text"}
<input type="text" name="{$module[a].name}" id="{$module[a].name}" value="{$module[a].value}">
{elseif $module[a].type eq "textarea"}
<textarea name="{$module[a].name}" rows="5" id="{$module[a].name}">{$module[a].value}</textarea>
{elseif $module[a].type eq "image"}
<table width="100%" cellpadding="2" cellspacing="2" border="0">
<tr><td>
<input type="text" name="{$module[a].name}" id="{$module[a].name}" value="{$module[a].value}" disabled="disabled" /><button type="button" {literal}onClick="OpenFileBrowser('image', function(url) {document.settings_form.{/literal}{$module[a].name}{literal}.value=url;
{/literal}document.getElementById('img${$module[a].name}').src=document.getElementById('{$module[a].name}').value;{literal}
}, function() {return document.settings_form.{/literal}{$module[a].name}{literal}.value;} )">Choose Image...{/literal}</button>
</td>
<td><img id="img${$module[a].name}" name="img${$module[a].name}" src="/images/no_photo.jpg"></td>
</tr>
</table>
{/if}</td></tr>
{/section}
<Tr><Td><input name="submit" type="submit" value="{$lang.save}" id="submitButton" class="button"></Td></Tr>
</table>
</form>
</div>