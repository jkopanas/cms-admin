{include file="modules/products/admin/menu.tpl"}
{include file="modules/products/admin/additional_links.tpl" mode="menu" base_file="content_modify"}

{if $error_list}{include file="common/error_list.tpl"}{/if}
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}
{if $item}
<div class="seperator">
  <h2>{if $nav ne "0"} {section name=w loop=$nav}
  <a href="{$MODULE_FOLDER}/category_content.php?cat={$nav[w].categoryid}" class="navpath" title="{$nav[w].category}">{$nav[w].category}</a> / {/section}{/if} #{$item.id} {$item.title}</h2>
</div>
{/if}

<form action="" name="EditForm" method="post">
<input type="hidden" name="FormAction" value="{if $item}update{else}add{/if}" />
<input type="hidden" name="id" value="{$item.id}" />
<link rel="stylesheet" type="text/css" href="/scripts/maps/style.css" />
<div class="seperator" id="Wrapper">
<div id="ColumnLeft" class="float-left spacer-right">





<div class="wrap spacer-bottom">
<h2>{$lang.title}</h2>
<div class="padding">
<input name="title" type="text" id="title" size="45" value="{$item.title}" class="ContentTitle">
    </div><!-- END PADDING -->
</div><!-- END BOX -->
<div class="wrap spacer-bottom">
<h2>Permalink</h2>
<div class="padding">
<input name="permalink" type="text" id="permalink" size="45" value="{$item.permalink}" class="ContentTitle">
    </div><!-- END PADDING -->
</div><!-- END BOX -->
<div class="wrap spacer-bottom">
<h2>{$lang.categories}</h2>
{if $item.category}
      <div class="UsedCategories">
       <h3>Κατηγορίες σε χρήση</h3>
      <ul id="ItemCategoriesList" class="spacer-bottom">
      {foreach from=$allcategories item=a name=b key=k}{assign var=tmp value=$a.categoryid}
      {if $simple_categories|is_array AND  $a.categoryid|array_key_exists:$simple_categories}
       <li id="CatList-{$a.categoryid}"><a href="javascript:void(0);" class="DeleteCatList" rel="{$a.categoryid}"><img src="/images/admin/delete.gif" width="12" height="12" border="0" align="absmiddle" /></a> <span {if $simple_categories[$tmp]}class="red"{/if}>{$a.category}</span></li>
       {/if}
      {/foreach}
      </ul>
      </div>
      {/if}
<div class="padding">
 	<div class="categories_list" id="categories_list">
    <ul>
    <li><strong>Κατηγορία</strong><span class="float-right"><strong>Κεντρική</strong></span></li>
    {foreach from=$allcategories item=a name=b key=k}{assign var=tmp value=$a.categoryid}
    <li {if $simple_categories|is_array AND  $a.categoryid|array_key_exists:$simple_categories}class="alternate"{/if}><input type="checkbox" name="cat[]" id="cat-{$a.categoryid}" value="{$a.categoryid}" {if $simple_categories|is_array AND  $a.categoryid|array_key_exists:$simple_categories}checked{/if} class="choose-cat" /> <label for="cat-{$a.categoryid}">{$a.category}</label><span class="float-right"><input type="radio" name="main-cat" value="{$a.categoryid}" id="main-cat-{$a.categoryid}" {if $simple_categories|is_array AND !$a.categoryid|array_key_exists:$simple_categories}disabled="disabled"{/if}  {if $simple_categories[$tmp]}checked{/if}  /></span></li>
    {/foreach}
    </ul>
    </div>     
    <a id="category-add-toggle" href="javascript:void(0);" class="hide-if-no-js" tabindex="3" rel="category-add">+ Προσθήκη κατηγορίας</a>
    <div id="category-add" style="display: none">
	<p id="category-add" class="wp-hidden-child">
	<label class="screen-reader-text" for="newcat">Τίτλος κατηγορίας</label> <input name="category" id="newcat" class="new-cat" tabindex="3" aria-required="true" type="text">

    <label class="screen-reader-text" for="alias">Alias</label><input name="alias" id="alias" class="new-cat" tabindex="3" aria-required="true" type="text"><br />
	<label for="newcat_parent">Κεντρική κατηγορία:</label>
    <select name="parent_catid" class="new-cat" id="parent_catid">
        <option value="0">{$lang.root_category}</option>    
{foreach from=$allcategories item=a}
<option value="{$a.categoryid}" {if $a.categoryid eq $CurrentCat.parentid}selected{/if}>{$a.category}</option>                
{/foreach} 
</select>
	<input id="category-add-sumbit" class="add-cat button" value="Προσθήκη" alt="products" tabindex="3" type="button">

    </div>
    </div><!-- END PADDING -->
</div><!-- END BOX -->
<div class="wrap spacer-bottom">
  <h2>{$lang.description}</h2>
<div class="padding">
<textarea name="description" cols="80" rows="5" id="description">{$item.description}</textarea><br /><a href="javascript:void(0);" class="open-editor" rel="description">{$lang.advanced_editor}</a>
</div><!-- END PAADDING -->
</div><!-- END BOX -->

<div class="wrap spacer-bottom">
<h2>{$lang.long_desc}</h2>
<div class="padding">
<textarea name="description_long" cols="80" rows="10" id="description_long">{$item.description_long}</textarea> <br /><a href="javascript:void(0);" class="open-editor" rel="description_long">{$lang.advanced_editor}</a>
</div><!-- END PAADDING -->
</div><!-- END BOX -->

<p align="center"><input type="submit" value="{$lang.save}" class="button" /></p>
    {if $extra_fields}
<div class="wrap spacer-bottom">
<h2>{$lang.extra_fields}</h2>
<div class="padding">
<div id="tmp" class="hidden"><div id="tmpContent"></div></div>
     <div id="extra_fields_div">{include file="common/extra_fields_modify.tpl"}</div>
</div><!-- END PAADDING -->
</div><!-- END BOX -->
<p align="center"><input type="submit" value="{$lang.save}" class="button" /></p>
     {/if}


</div><!-- END LEFT -->
<div id="ColumnRight" class="float-left spacer-left">
<div class="wrap spacer-bottom">
<h2>Βασικά στοιχεία</h2>
<div class="padding">
<ul>
<li><select name="active" id="active" {if $cron} disabled="disabled"{/if}>
        <option value="0" {if $item.active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $item.active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select> {$lang.availability}
</li>
<li>
<select name="settings_allow_comments" id="allow_comments">
        <option value="0" {if $item.settings.allow_comments eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $item.settings.allow_comments eq "1"}selected{/if}>{$lang.enabled}</option>
      </select> Σχόλια
</li>

{if $loaded_modules.eshop AND $item}
<li><input type="text" name="price" value="{$item.eshop.price}" /> {$lang.price}</li>
<li><input type="text" name="list_price" value="{$item.eshop.list_price}" /> {$lang.list_price}</li>
{/if}
<li><p align="center"><input type="submit" value="{$lang.save}" class="button" /></p></li>
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->
{if $item}
<div class="wrap spacer-bottom">
<h2>Κεντρική Φωτογραφία</h2>
<div class="padding">

<div style="padding:3px; border:1px solid #000000; width:{$image_types.thumb.width}px" id="ItemThumb"><img src="{$item.image_full.thumb|default:$content_module.settings.default_thumb}" /></div>
<div class="seperator" id="UploadFile">
<input type="file" name="uploadify" id="uploadify"  />
<p><label><input type="checkbox" value="1" name="CustomImageSet"  class="SaveData ShowImageCopies" alt="uploadify"  /> {$lang.select_copies}</label>
<ul class="hidden" id="ImageSet">
{foreach from=$image_types item=a}
<li><label><input type="checkbox" name="ImageSet{$a.id}" value="{$a.id}" class="SaveData ImageSet" {if $a.required}disabled{/if} alt="uploadify"  /> {$a.title} ({$a.width}x{$a.height}px)</label> {if $a.required}<span class="red">*</span>{/if}</li>
{/foreach}
<li>Τα πεδιία με <span class="red">*</span> είναι υποχρεωτικά</li>
</ul>
</p>
<div id="fileQueue" style="width:180px;"></div>
<ul class="msg hidden"></ul>
<a href="javascript:$('#uploadify').uploadifyUpload();">{$lang.upload_file}</a>
</div>
<input type="hidden" name="module" value="products" class="SaveData" />
<input type="hidden" name="action" value="Savethumb" class="SaveData" />
<input type="hidden" name="itemid" value="{$item.id}" class="SaveData" />
<input type="hidden" name="return" value="Thumb" class="SaveData" />
<input type="hidden" name="ExistingThumb" value="{$item.image_full.thumb}" class="SaveData"/>


</div><!-- END PADDING -->
</div><!-- END BOX -->
{/if}

{if $loaded_modules.locations AND $item.id}

<div class="wrap spacer-bottom">
<h2>{$lang.location}</h2>
<div class="padding">
<select name="commonCategoryTree-{$allTreeCategories.locations.id}" id="allow_comments" style="width:250px">
<option value="">{$lang.select_one}</option>
{foreach from=$allTreeCategories.locations.categories item=a}
<option value="{$a.categoryid}" {if $item.locations.catid eq $a.categoryid}selected{/if}>{$a.category}</option>      
{/foreach}
</select>
<div class="seperator"></div>
<a href="#" class="placeItemOnMap" rel="edit">{$lang.lbl_placeOnMap}</a>

<div id="mapInfo">
<script type="text/javascript" src="http://www.google.com/jsapi?autoload=%7Bmodules%3A%5B%7Bname%3A%22maps%22%2Cversion%3A3%2Cother_params%3A%22sensor%3Dfalse%22%7D%5D%7D"></script> 
<script type="text/javascript" src="/scripts/maps/MarkerClusterer.js"></script> 
<script type="text/javascript" src="/scripts/maps/progressBar.js"></script>
<script type="text/javascript" src="/scripts/maps/infobox.js"></script> 
<script type="text/javascript" src="/scripts/maps/gMaps2.js"></script> 
<script type="text/javascript" src="/scripts/maps/loadMapItem.js"></script> 

<input type="hidden" name="geocoderAddress" value="{$mapItem.geocoderAddress}" class="mapItem" />
<input type="hidden" name="lat" class="mapItem" value="{$mapItem.lat}" />
<input type="hidden" name="lng" class="mapItem" value="{$mapItem.lng}" />
<input type="hidden" name="zoomLevel" class="mapItem" value="{$mapItem.zoomLevel}" />
<input type="hidden" name="MapTypeId" class="mapItem" value="{$mapItem.MapTypeId}" />
<input type="hidden" name="mapItem" value='{$mapItemEncoded}' />
</div>

<div id="mapItem" style="width:100%; height:300px; background-color:#999;" {if !$mapItem}class="hidden"{/if}></div>
<div class="clear"></div>
</div><!-- END PADDING -->

</div><!-- END BOX -->

{/if}

<div class="wrap spacer-bottom">
<h2>Tags</h2>
<div class="padding">
<label>{$lang.new} Tag : </label> <input name="AddTagsForm" type="text" class="tags-input" id="tags-content" value="" size="16" /> <input type="button" class="button" name="AddTagToList" value="Προσθήκη" />
      <input name="tags" type="hidden" id="HiddenTagsField" value="{$item.tags_string}" />
	   <ul id="TagsList" class="seperator">
      {foreach from=$item.tags item=a}
      <li><a href="javascript:void(0);" class="DeleteTag" rel="{$a.id}"><img src="/images/admin/delete.gif" width="12" height="12" border="0" align="absmiddle" /></a> <span>{$a.tag}</span></li>
      {/foreach}
      </ul>
      <div class="clear"></div>
</div><!-- END PADDING -->
</div><!-- END BOX -->


</div><!-- END RIGHT -->
</div><!-- END WRAP -->
</form>

<div class="seperator"></div>
{if $extra_fields}<script src="/scripts/AdminLanguages.js"></script>{/if}