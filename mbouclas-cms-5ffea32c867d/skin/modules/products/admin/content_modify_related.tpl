{include file="modules/products/admin/menu.tpl"}
{include file="modules/products/admin/additional_links.tpl" mode="menu" base_file="content_modify"}
<div class="seperator">
  <h2>{if $nav ne "0"} {section name=w loop=$nav}
  <a href="{$MODULE_FOLDER}/category_content.php?cat={$nav[w].categoryid}" class="navpath" title="{$nav[w].category}">{$nav[w].category}</a> / {/section}{/if} #{$item.id} {$item.title}</h2>
</div>

<div class="wrap seperator padding">
<h2>Σχετικές σελίδες</h2>
<form name="RelatedItemsForm">
 <input type="hidden" name="DisplayDiv" value="SearchBoxResultsForm-div" />
 Αναζήτηση σε : <select name="field" id="QuickSearchField" class="SearchData">

      <option value="id">#ID</option>
      <option value="substring">Τίτλο</option>
    </select>
     <input name="search_substring" type="text" class="search-style SearchData" id="QuickSearchVal" size="40"/>  
     <select name="categoryid" id="catid" class="SearchData">
        <option value="%">Όλες οι κατηγορίες</option>    
{foreach from=$root_categories item=a}
<option value="{$a.categoryid}">{$a.category}</option>
{/foreach}
</select> <input type="button" id="QuickSearchBtn1" value="Εμφάνιση αποτελεσμάτων" class="button" name="PerformSearch" /> <input name="ResultsDiv" type="hidden"  value="#RelatedResultsPlaceHolder-div" class="SearchData">
<input name="action" type="hidden"  value="QuickSearch" class="SearchData"> <input name="mode" type="hidden"  value="RelatedContent" class="SearchData"> <input name="CurrentCat" type="hidden"  value="{$CurrentCat.categoryid}" class="SearchData"> <input name="itemid" type="hidden"  value="{$item.id}" class="SearchData"> <input name="sort" type="hidden"  value="orderby" class="SearchData"> <input name="module_dest" type="hidden"  value="products" class="SearchData"> <input name="itemid" type="hidden"  value="{$item.id}" class="SearchData">


   <div class="seperator clear"></div>

<div id="RelatedResultsPlaceHolder-div"></div>
</form>



<form name="ExistingItems" method="post"><input type="hidden" name="type" value="RelatedContent" class="SearchData" /><input type="hidden" name="CurrentCat" value="{$CurrentCat.categoryid|default:0}" class="SearchData" />
<input type="hidden" value="SaveRelatedOrder" name="action" />
<div id="debugArea"></div>
 <div id="existing_items">
 
 {include file="modules/products/admin/quick_search_results.tpl" mode="results" items=$related}</div></form>
</div><!-- END BOX -->
<div class="seperator"></div>
