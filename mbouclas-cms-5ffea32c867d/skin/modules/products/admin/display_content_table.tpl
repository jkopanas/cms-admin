<input type="hidden" name="type" value="{$type}" class="settings" />
<input type="hidden" name="WIDTH" value="{$WIDTH}" class="settings" />
<input type="hidden" name="form_name" value="{$form_name}" class="settings" />
<input type="hidden" class="settings" name="SavedData" value="{$SavedData}" />
{if $type eq "edit"}

<div id="{$form_name}-div">
{if $pagination_mode  AND $list.total gt $items|@count}
{include file="modules/content/pagination.tpl" mode=$pagination_mode}
{/if}
<div id="debugArea"></div>
<table cellpadding="5" cellspacing="5" class="widefat{if $reorder} DragDrop{/if}">
      <tr class="nodrop nodrag">
        <td colspan="8">{$lang.withSelectedDo} (<span class="messages">0</span> {$lang.selected} ) : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">{$lang.enable}</option>
        <option value="DeactivateChecked">{$lang.disable}</option>
        <option value="DeleteChecked">{$lang.delete}</option>
        </select>
         <input type="button" class="button PerformAction" value="{$lang.apply}" />
        </td>
      </tr>

<tr class="nodrop nodrag">
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
  {if $reorder}<th scope="col"><input type="hidden" value="SaveContentOrder" name="action" /></th>{/if}
  <th scope="col">ID</th>
  <th scope="col">Τίτλος</th>
<th scope="col">Κατηγορία</th>
<th scope="col">Ημερομηνία δημιουργίας</th>
<th scope="col">Διαθεσιμότητα</th>
{if $loaded_modules.content.settings.use_provider}<th scope="col">{$lang.provider}</th>{/if}
<th width="84" scope="col">&nbsp;</th>
</tr>

{section name=w loop=$items}
<tr {cycle values="'FFFFFF', class='alternate'"} id="Order{$items[w].id}">
            <td><input name="id" type="checkbox" id="check_values" value="{$items[w].id}" class="check-values" /></td>
            {if $reorder}<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>{/if}
            <td><strong>{$items[w].id}</strong></td>
            <td><a href="modify.php?id={$items[w].id}">{$items[w].title}</a></td>
            <td>{foreach from=$items[w].category item=a name=b}<a href="category_content.php?cat={$a.categoryid}">{$a.category}</a>{if !$smarty.foreach.b.last}, {/if}{/foreach}</td>
            <td>{$items[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $items[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
          {if $loaded_modules.content.settings.use_provider}  <td>{if $ADMIN eq 1}<a href="users_content.php?id={$items[w].uid}">{$items[w].user.user_data.uname}</a>{else}{$items[w].user.user_data.uname}{/if}</td>{/if}
            <td><a href="{$URL}/content_page.php?id={$items[w].id}&preview=1&keepThis=true&TB_iframe=true&height=700&width=900" class="thickbox" target="_blank" title="Preview Item"><img src="/images/admin/preview_16.png" width="16" height="16" border="0" /></a> <a href="content_modify_img.php?id={$items[w].id}" title="Media Files"><img src="/images/admin/mediafolder.gif" width="16" height="16" border="0" /></a> <a href="related_items.php?id={$items[w].id}" title="Related Items"><img src="/images/admin/redo.gif" width="16" height="16" border="0" /></a></td>
  </tr>
		  {/section}
      <tr>
        <td colspan="8">{$lang.withSelectedDo} : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">{$lang.enable}</option>
        <option value="DeactivateChecked">{$lang.disable}</option>
        <option value="DeleteChecked">{$lang.delete}</option>
        </select>
         <input type="button" class="button PerformAction" value="{$lang.apply}" />
        </td>
      </tr>
</table>
{if $pagination_mode  AND $list.total gt $items|@count}
{include file="modules/content/pagination.tpl" mode=$pagination_mode}
{/if}
</div>
{elseif $type eq "FileManager"}
{if $pagination_mode  AND $list.total gt $items|@count}
{include file="modules/content/pagination.tpl" mode=$pagination_mode}
{/if}{$media}
<table cellpadding="0" cellspacing="0" class="widefat">
  <tr>
    <th scope="col">ID</th>
    <th scope="col">Τίτλος</th>
  <th scope="col">Ημερομηνία δημιουργίας</th>
  {if $loaded_modules.content.settings.use_provider}{/if}</tr>

{section name=w loop=$items}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><strong>{$items[w].id}</strong></td>
            <td><a href="file_manager.php?itemid={$itemid}&id={$items[w].id}&module={$CURRENT_MODULE.name}&rel={$rel}&tab=site&media={$media}&mode={$Mode}">{$items[w].title}</a></td>
            <td>{$items[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
  {if $loaded_modules.content.settings.use_provider}  {/if}  </tr>
		  {/section}
</table>
{elseif $type eq "dashboard"}
<div id="messages">{$message}</div>
<div id="{$form_name}-div">
<table cellpadding="0" cellspacing="0" class="widefat" {if $WIDTH}style="width:{$WIDTH}px"{/if}>
      <tr>
        <td colspan="7">{$lang.withSelectedDo} (<span class="messages">0</span> {$lang.selected} ) : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">{$lang.enable}</option>
        <option value="DeactivateChecked">{$lang.disable}</option>
        <option value="DeleteChecked">{$lang.delete}</option>
        </select>
         <input type="button" class="button PerformAction" value="{$lang.apply}" />
        </td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
  <th scope="col">ID</th>
  <th scope="col">Τίτλος</th>
<th scope="col">Ημερομηνία δημιουργίας</th>
<th scope="col">Διαθεσιμότητα</th>
<th width="56" scope="col">&nbsp;</th>
</tr>

{section name=w loop=$items}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="id" type="checkbox" id="check_values" value="{$items[w].id}" class="check-values" /></td>
            <td><strong>{$items[w].id}</strong></td>
            <td><a href="{$MODULE_FOLDER}/modify.php?id={$items[w].id}">{$items[w].title}</a></td>
            <td>{$items[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $items[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td><a href="{$URL}/content_page.php?id={$items[w].id}&preview=1&keepThis=true&TB_iframe=true&height=700&width=900" class="thickbox" target="_blank" title="Preview Item"><img src="/images/admin/preview_16.png" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/content_modify_img.php?id={$items[w].id}"><img src="/images/admin/mediafolder.gif" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/content_modify_opt.php?id={$items[w].id}"><img src="/images/admin/optiongroup.gif" width="16" height="16" border="0" /></a></td>
            
    </tr>
		  {/section}
      <tr>
        <td colspan="7">{$lang.withSelectedDo} : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">{$lang.enable}</option>
        <option value="DeactivateChecked">{$lang.disable}</option>
        <option value="DeleteChecked">{$lang.delete}</option>
        </select>
         <input type="button" class="button PerformAction" value="{$lang.apply}" />
        </td>
      </tr>
</table>
</div>
{elseif $type eq "editor"}
<div id="messages">{$message}</div>
<div id="{$form_name}-div">
<table cellpadding="3" cellspacing="0" class="widefat">
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" class="check-all" /></th>
  <th scope="col">ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">Τίτλος</th>
<th scope="col">Κατηγορία</th>
<th scope="col">Διαθεσιμότητα</th>
</tr>

{section name=w loop=$items}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="id" type="checkbox" id="check_values" value="{$items[w].id}" class="check-values" /></td>
            <td><strong>{$items[w].id}</strong></td>
            <td><img src="{$URL}/{$items[w].image}" /></td>
            <td><input type="text" name="content_title-{$items[w].id}" value="{$items[w].title}" onkeyup="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'" /></td>
            <td><select name="categoryid-{$items[w].id}"  onchange="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'">
                       
{section name=cat_num loop=$allcategories}

              <option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $items[w].catid}selected{/if}>{$allcategories[cat_num].category}</option>
              
{/section}

            </select>            </td>
    <td align="center"><select name="active-{$items[w].id}"  onchange="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'">
        <option value="0" {if $items[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $items[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
    </select></td>
    </tr>
		  {/section}
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}" /> <input type="hidden" name="mode" id="mode" value="" /></td></tr>
</table>
{elseif $type eq "search_results_admin"}
<table cellpadding="3" cellspacing="0" class="widefat">
      <tr>
        <td colspan="9">{include file="modules/content/pagination.tpl"}</td>
      </tr>

      <tr>
        <th colspan="7" scope="col"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />       </th>
      </tr>
  <tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" class="check-all" /></th>
  <th scope="col">ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">{$lang.pos}</th>
  <th scope="col">Τίτλος</th>
{if $eshop_module}
<th scope="col">{$lang.price}</th>
{/if}
{if $locations_module}
<th scope="col">{$lang.location}</th>
{/if}
<th scope="col">Κατηγορία</th>
<th scope="col">Διαθεσιμότητα</th>
</tr>

{section name=w loop=$items}
          <tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$items[w].id}]" type="checkbox" id="check_values" value="1" class="check-values" /></td>
            <td><strong><a href="{$MODULE_FOLDER}/modify.php?id={$items[w].id}">{$items[w].id}</a></strong></td>
            <td><a href="{$MODULE_FOLDER}/modify.php?id={$items[w].id}"><img src="{$URL}/{$items[w].image}" /></a></td>
            <td><input name="orderby-{$items[w].id}" type="text" id="orderby-{$items[w].id}" size="3" value="{$items[w].orderby}" onkeyup="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'"  /></td>
            <td><input type="text" name="content_title-{$items[w].id}" value="{$items[w].title}" onkeyup="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'" /></td>
            {if $eshop_module}
            <td><input type="text" name="eshop_price-{$items[w].id}" id="eshop_price-{$items[w].id}" value="{$items[w].eshop.price}"  onkeyup="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'"/></td>
            {/if}
            {if $locations_module}
            <td><select name="locations_id-{$items[w].id}" id="locations_id-{$items[w].id}" onchange="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'">
            {section name=a loop=$all_locations}
            <option value="{$all_locations[a].id}" {if $items[w].location.categoryid eq $all_locations[a].categoryid}selected{/if}>{$all_locations[a].category}</option>
            {/section}
            </select></td>
            {/if}
            <td>{foreach from=$items[w].category item=a name=b}<a href="category_content.php?cat={$a.categoryid}">{$a.category}</a>{if !$smarty.foreach.b.last}, {/if}{/foreach}</td>
            <td align="center"><select name="active-{$items[w].id}" onchange="document.getElementsByName('ids[{$items[w].id}]')[0].checked='true'">
        <option value="0" {if $items[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $items[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select></td>
    </tr>
		  {/section}
      <tr>
        <td colspan="9"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}{if $search AND !$QUERY_STRING}?search=1{elseif $search AND $QUERY_STRING}&search=1{/if}" />
       <input type="hidden" name="mode" id="mode" value="" /></td>
      </tr>
</table>
</div>
{else}
<table cellpadding="5" cellspacing="5"  width="700">
  <tr>
    <th scope="col">ID</th>
    <th scope="col">Τίτλος</th>
  <th scope="col">Κατηγορία</th>
  <th scope="col">Ημερομηνία δημιουργίας</th>
  
{if $loaded_modules.content.settings.use_provider}  {/if}</tr>

{section name=w loop=$items}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><strong>{$items[w].id}</strong></td>
            <td><a href="/modules/{$module}/admin/modify.php?id={$items[w].id}">{$items[w].title}</a></td>
            <td>{foreach from=$items[w].category item=a name=b}<a href="/modules/{$module}/admin/category_content.php?cat={$a.categoryid}">{$a.category}</a>{if !$smarty.foreach.b.last}, {/if}{/foreach}</td>
            <td>{$items[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            
  {if $loaded_modules.content.settings.use_provider} {/if}  </tr>
		  {/section}
</table>
{/if}