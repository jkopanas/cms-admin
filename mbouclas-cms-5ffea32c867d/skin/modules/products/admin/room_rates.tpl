<a name="bookings"></a>
<h2>Bookings</h2>
<div class="hidden">
Type : <select name="rentalType">
{foreach from=$rentalTypes item=a}
<option value="{$a.id}" data-uses="{$a.uses}" {if $a.id eq $itemType.type}selected{/if}>{$a.title}</option>
{/foreach}
</select>
</div>

{if !$seasons AND !$mode}{assign var="mode" value="edit"}{/if}
{assign var="mode" value="edit"}
<div id="bookings">{include file="modules/bookings/type_`$itemType.uses|default:periods`.tpl"}</div>
<link rel="stylesheet" media="screen" type="text/css" href="/scripts/dateRange/css/datepicker.css" />
<script type="text/javascript" src="/scripts/dateRange/datepicker.js"></script>
<script type="text/javascript" src="/scripts/site/dataStore.js"></script>
<script type="text/javascript" src="/scripts/site/bookingsAdmin.js"></script>