<div class="category-title">
<h1>{$lang.latest} products</h1>
</div>


<ul class="products-grid">
{foreach from=$latestProducts item=a name=x}
<li class="item {if $smarty.foreach.a.first}first{/if} {if not ($smarty.foreach.x.iteration mod 5)}last{/if}">
<div class="imagecontainers imagecontainers2" data-overlayid="overlayid-1" data-overlayid2="overlayid2-1" style="display:block-inline;width:156px;height:156px"><a href="/{$FRONT_LANG}/{$prefix|default:"pages"}/{$a.id}/{$a.permalink}.html" title="{$a.title}" class="product-image"><img src="{$a.image_full.big_thumb}" alt="{$a.title}" width="155" height="155" class="itemImg" title="{$a.title}" /></a></div>
<div id="overlayid-1">
<div class="items">
<h2 class="product-name"><a href="/{$FRONT_LANG}/{$prefix|default:"pages"}/{$a.id}/{$a.permalink}.html" title="{$a.title}">{$a.title}</a></h2>
<div class="hidden">
<div class="product-price">
<div class="price-box">
<span class="regular-price" id="product-price-1">
<span class="price">&euro;{$a.eshop.price}</span></span>
</div>
</div>
<div class="cartButton">		
<button type="button" title="Add to Cart" class="button btn-cart addToCart" rel="{$a.id}"><span><span>Add to Cart</span></span></button>
	</div>
</div>		
</div>
	</div>
				
				

							
                	                    <div class="ratings">
                    <div class="rating-box">
                <div class="rating" style="width:87%"></div>
            </div>
    </div>
</li>
{/foreach}
</ul>
<div class="clear"></div>
