{literal}
<script type="text/javascript">
function display_ajax_loader(res_div)
{
xajax.$(res_div).innerHTML = '';
xajax.$(res_div).innerHTML = '<img src="/images/ajax-loader.gif">' + ' Loading....';
}

function set_efields(id,res_div,cid)
{
display_ajax_loader(res_div);
xajax_set_extra_fields(id,res_div,cid);
}

</script>
{/literal}