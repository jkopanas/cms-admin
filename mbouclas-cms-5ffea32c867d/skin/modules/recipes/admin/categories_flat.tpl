{include file="modules/recipes/admin/menu.tpl"}
<div id="in_this_section"><strong>In this section :</strong> <a href="#featured_add">Add Featured Items</a> | <a href="categories.php{if $catid}?cat={$catid}{/if}#addcategory">Add new category</a></div>
{include file="modules/recipes/search_box.tpl" mode="start_page"}
{if !$edit}
<TABLE class="open_close_tab seperator ">
      <TR>
        <TD id="close3" style="display: none; cursor: hand;" onClick="visibleBox('3')"><IMG src="/skin/images/plus.gif" border="0" align="absmiddle" alt="Click to open"></TD>
        <TD id="open3" style="cursor: hand;" onClick="visibleBox('3')"><IMG src="/skin/images/minus.gif" border="0" align="absmiddle" alt="Click to close"></TD>
        <TD><A href="javascript:void(0);" onClick="visibleBox('3')"><B>Show - Hide</B></A></TD>

      </TR>
</TABLE>
<div class="wrap padding" id="box3">
<h2><a href="{$SELF}">{$area}</a></h2>
<form name="cat_form" id="cat_form" action="" method="post">
<table cellpadding="3" cellspacing="3" width="100%">
<tr>
		<th scope="col">ID</th>
		<th scope="col">{$lang.pos}</th>
        <th scope="col">{$lang.title}</th>
        <th scope="col">{$lang.description}</th>
        <th scope="col"># {$lang.items}</th>
        <th>Action</th>
	</tr>
{section name=a loop=$cat}
<tr class="{cycle values='alternate,'}"><th scope="row">{$cat[a].categoryid}</th>
  <td><input name="orderby-{$cat[a].categoryid}" type="text" value="{$cat[a].orderby}" size="3" /></td>
  <td> <a href="categories.php?cat={$cat[a].categoryid}">{$cat[a].category}</a></td>
				<td>{$cat[a].description|truncate:50:" ...":true}</td>
				<td align="center"><A href="category_content.php?cat={$cat[a].categoryid}" class="ItemsList">{$cat[a].num_content|default:0}</A></td>
        <td><a href="{$SELF}?action=edit&amp;cat={$cat[a].categoryid}" class="edit" title="Edit Category {$cat[a].category}"><img src="/images/admin/edit_16.png" width="16" height="16" /></a></td><td><a href="{$SELF}?action=delete&amp;cat={$cat[a].categoryid}" onclick="return confirm('You are about to delete the category \'{$cat[a].category}\'.  All of its items will go to the default category.\n  \'OK\' to delete, \'Cancel\' to stop.')" class="delete" title="Delete Category {$cat[a].category}"><img src="/images/admin/delete_16.png" width="16" height="16" /></a></td>
				</tr>
{/section}
<tr><td colspan="5"><input type="submit" value="{$lang.save}" class="button" />
                </table>
                <input type="hidden" name="cat" id="cat" value="{$smarty.get.cat}" />
                <input type="hidden" name="mode" id="mode" value="quick_update" />
  <input type="hidden" name="featured" id="featured" value="1" />
  </form>
</div> 

 {/if}   
 <form name="addcat" id="addcat" action="" method="post">               
          <div class="wrap seperator padding"><a name="addcategory" id="addcategory"></a>
          <div id="zeitgeist" style="width:300px;">
  <h3>{$lang.general_settings}</h3>
  <div><img src="{if $category.settings.image}{$category.settings.image}{else}/images/admin/no_photo.jpg{/if}" name="preview" id="preview"  border="1"  style="border:#000000 1px solid" width="120">
  <br />
  <input type="hidden" name="settings_image" id="settings_image" value="{$category.settings.image}" />
  <button type="button" {literal}onClick="OpenFileBrowser('image', function(url) {document.addcat.settings_image.value=url;
document.getElementById('preview').src=document.getElementById('settings_image').value;
}, function() {return document.addcat.settings_image.value;} )">Choose Category Image...{/literal}</button>
<br />
<img src="{if $category.settings.slogan}{$category.settings.slogan}{else}/images/admin/no_photo.jpg{/if}" name="preview_slogan" id="preview_slogan"  border="1"  style="border:#000000 1px solid" width="120"><br />
  <input type="hidden" name="settings_slogan" id="settings_slogan" value="{$category.settings.slogan}" />
  <button type="button" {literal}onClick="OpenFileBrowser('image', function(url) {document.addcat.settings_slogan.value=url;document.getElementById('preview_slogan').src=document.getElementById('settings_slogan').value;}, function() {return document.addcat.settings_slogan.value; } )">Choose Slogan...{/literal}</button>
<br />
  <input type="text" name="settings_cat_alias" id="settings_cat_alias" value="{$category.settings.cat_alias}" /> Category Alias
  <Br />
  <select name="settings_theme">
  {foreach item=a from=$themes}
  <option value="{$a.name}" {if $a.name eq $category.settings.theme}selected{/if}>{$a.title}</option>
  {/foreach}
  </select>
   Select Theme
  <br />
  <input type="checkbox" value="1" name="settings_htaccess" id="settings_htaccess" {if $category.settings.htaccess eq 1}checked{/if} /> Create .htaccess entry
  <br />
  <select name="settings_on_menu">
  <option value="1" {if  $category.settings.on_menu eq 1}selected{/if}>{$lang.yes}</option>
  <option value="0" {if  $category.settings.on_menu eq 0}selected{/if}>{$lang.no}</option>
  </select> Display on main menu
  <br />
{if $more_categories}
<h3>More Categories</h3>
{section name=a loop=$more_categories}
{if $smarty.get.cat eq $more_categories[a].categoryid}<strong>{$more_categories[a].category}</strong>{else}<a href="{$SELF}?action=edit&cat={$more_categories[a].categoryid}">{$more_categories[a].category}</a>{/if}<br />
{/section}
{/if}
</div>

</div>
    <h2>{if $edit eq 1}{$lang.modify}{else} Add new {/if}{$category.category}</h2>
    
         
      <p>{$lang.pos}:<br>
        <input name="orderby" type="text" class="editform" style="background-color: rgb(255, 255, 160);" value="{$category.orderby}" size="4" id="orderby">
         </p>
        <p>
        {$lang.availability} <br />
        <select name="active" id="active" {if $cron} disabled="disabled"{/if}>
        <option value="0" {if $category.active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $category.active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select>
        </p>
        <p>Name:<br>
        <input name="title" type="text" style="background-color: rgb(255, 255, 160);" value="{$category.category}">
        </p>
        <p>Alias:<br>
        <input name="alias" type="text" style="background-color: rgb(255, 255, 160);" value="{$category.alias}">
        </p>
        <p>Description: (optional) <br>
        <textarea name="description" rows="5" cols="60"  id="desc">{$category.description}</textarea>
        <br />
        <a href="javascript:openEditor(document.addcat.desc)">Advanced Editor</a>
        </p>

        <p  align="left"><input name="action" value="{$action|default:'addcat'}" type="hidden"><input type="hidden" name="cat" value="{$catid|default:$smarty.get.cat}" /><input name="submit" type="submit" class="button" value="{$lang.save}">
        </p>
    
</div>      
</form>