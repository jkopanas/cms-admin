{include file="modules/recipes/admin/menu.tpl"}
{if $error_list}{include file="common/error_list.tpl"}{/if}
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}


{include file="modules/recipes/admin/additional_links.tpl" mode="nav_menu" base_file="recipes_modify"}

<div class="clear"></div>
<div class="wrap seperator padding">
<form name="form1" method="POST" action="{$PHP_SELF}" id="form1" style="width:99%;">
<div class="float-right"><input type="submit" name="Submit" value="{$lang.save}" class="button"></div>
<input type="hidden" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}" name="id" id="itemid">
<input name="action" type="hidden" id="action" value="{$action|default:"add"}">
<h2>Title</h2><span style="display: none;" class="autosave_saving">Saving�</span>
<table width="970" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td>Title : <input name="title" type="text" id="title" size="45" value="{$item.title}"></td>
    <td align="right"></td>
  </tr>
</table>
<div class="seperator"></div>
<h2>{$lang.classification}</h2>
<table width="970" border="0" cellspacing="5" cellpadding="5">
  <tr class="TableSubHead">
    <th>{$lang.availability}</th>
    <th>Provider</th>
    <th>Allow Comments</th>
    <th>Publish</th>
  </tr>
    <tr>
    <td><select name="active" id="active" {if $cron} disabled="disabled"{/if}>
        <option value="0" {if $item.active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $item.active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select> {if $cron}<span style="color:#F00;" id="CronMessage">This article is scheduled for <strong>{$cron.date|date_format:"%d/%m/%Y @ %H:%M"}</strong></span> {/if}<span style="color:#F00; display:none" id="message">The article will be activated on the date you picked</span></td>
    <td><select name="providerid" id="providerid">
      <option value="%">{$lang.none}</option>         
{section name=cat_num loop=$providers}
<option value="{$providers[cat_num].id}" {if $providers[cat_num].id eq $item.user.uid}selected{/if}>{$providers[cat_num].user.user_name}</option>
{/section}
</select></td>
    <td><select name="settings_allow_comments2" id="settings_allow_comments">
      <option value="0" {if $item.settings.allow_comments eq "0"}selected{/if}>{$lang.disabled}</option>
      <option value="1" {if $item.settings.allow_comments eq "1"}selected{/if}>{$lang.enabled}</option>
    </select></td>
    <td><b id="When">{if $item.date_added}{$item.date_added|date_format:"%d/%m/%Y @ %H:%M"}{else}immediately{/if}</b>  <input type="button" id="EditTimestamp" value="Edit" /> 
<div style="display:none" id="TimeStamp">{html_select_date end_year="+1" time=$activation.date|default:"" day_extra="id='Day'" year_extra="id='Year'" month_extra="id='Month'"} @ {html_select_time use_24_hours=true display_seconds=false time=$activation.time|default:"" hour_extra="id='Hour'" minute_extra="id='Min'"} <br /><input type="button" value="OK" id="SaveTime" /> <input type="button" value="Cancel" id="CancelTime" /><input type="hidden" value="0" name="AutoActivate" id="AutoActivate" /></div>
</td>
  </tr>
</table>
<div class="seperator"></div>
<h2>Long Description</h2>
<span style="display: none;" class="autosave_saving">Saving�</span>
<textarea name="description_long" cols="131" rows="10" id="description_long">{$item.description_long}</textarea> <br /><a href="javascript:void(0);" class="open-editor" rel="description_long">Open Advanced Editor</a> 
<div class="float-right"><input type="submit" name="Submit" value="{$lang.save}" class="button"></div>
<div class="seperator"></div>
<h2>Ingredients</h2>
<span style="display: none;" class="autosave_saving">Saving�</span>
    <table width="100%" cellpadding="2" cellspacing="2"  id="IngredientsList">
    <tbody>
    <tr class="nodrop nodrag TableSubHead">
    <th align="center">&nbsp;</th>
    <th align="center"><strong>Header</strong></th>
    <th width="60" align="center"><strong>Quantity </strong></th>
    <th width="120" align="center"><strong>Unit</strong> ( <a id="unit-add-toggle" href="#unit-add" class="hide-if-no-js" tabindex="3" rel="new-unit">+ new</a> )</th>
    <th width="260" align="center"><strong>Ingredient    </strong></th>
    <th width="145" align="center"><strong>Base Ingredient    </strong></th>
    <th align="center"><strong>Main Ingedient      </strong></th>  
    </tr>
    <tr id="new-unit" style="display:none"><td colspan="7">
    Name : <input type="text" name="name" class="new-unit" /> 
    &nbsp; Value : 
    <input type="text" name="value" class="new-unit" /> &nbsp; Base : <input type="checkbox" value="1" name="base" class="new-unit" /> &nbsp; 
    Category&nbsp;
    <select name="catid" class="new-unit" id="units">
    {foreach from=$base_units item=a}
    <option value="{$a.catid}">{$a.name}</option>
    {/foreach}
    </select>
     &nbsp;<input type="button" id="new-unit-submit" value="{$lang.save}" class="button" />
    </td></tr>
    {if $item.ingredients}
    {foreach from=$item.ingredients item=a name=b}
    <tr id="{$smarty.foreach.b.iteration}" class="IngredientsList {cycle values=",TableSubHead"}">
    <td align="center" class="dragHandle"><img src="/images/move-arrow.png" alt="move" width="16" height="16" class="handle" /></td>
    	<td align="center"><select name="type-{$smarty.foreach.b.iteration}" class="type"><option value="header" {if $a.type eq "header"}selected{/if}>Yes</option><option value="ingredient" {if $a.type eq "ingredient"}selected{/if}>No</option></select></td><td align="center"><input name="quantity-{$smarty.foreach.b.iteration}" size="5" type="text" value="{$a.quantity}" {if $a.type eq "header"} disabled="disabled"{/if} /></td>
    	<td align="center"><select name="unit-{$smarty.foreach.b.iteration}" {if $a.type eq "header"} disabled="disabled"{/if} class="units-lists">
<option value="0" selected="selected">No Unit</option>
{foreach from=$units item=c}
<option value="{$c.id}" {if $a.unit_id eq $c.id}} selected="selected"{/if}>{$c.name}
{/foreach}
  </select></td>
	    <td align="center"><input name="ingredient-{$smarty.foreach.b.iteration}" size="45" type="text" value="{$a.ingredients}"></td>
    	<td align="center"><input name="base-{$smarty.foreach.b.iteration}" autocomplete="off" id="base-{$smarty.foreach.b.iteration}" type="text" class="base" value="{$a.base}" {if $a.type eq "header"} disabled="disabled"{/if} /></td>
	    <td align="center">
        <select name="main-{$smarty.foreach.b.iteration}" class="type" id="main-{$smarty.foreach.b.iteration}" {if $a.type eq "header"} disabled="disabled"{/if}>
        <option value="1" {if $a.main eq "1"}selected{/if}>Yes</option>
        <option value="0" {if $a.main eq "0"}selected{/if}>No</option>
        </select></td>
	    <td align="center"><a href="javascript:void(0)" class="RemoveRow"><img src="/images/admin/banlist_16.png" /></a></td>
   		<td>&nbsp;</td>
    </tr>
    {/foreach}
    {else}
    <tr id="1" class="IngredientsList">
    <td align="center" class="dragHandle"><img src="/images/move-arrow.png" alt="move" width="16" height="16" class="handle" /></td>
    	<td align="center"><select name="type-1" class="type">
        <option value="header">Yes</option>
        <option value="ingredient" selected="selected">No</option>
        </select></td>
        <td align="center"><input name="quantity-1" size="5" type="text" /></td>
    	<td align="center"><select name="unit-1"  class="units-lists">
<option value="0" selected="selected">No Unit</option>
   {foreach from=$units item=c}
<option value="{$c.id}" {if $a.unit_id eq $c.id}} selected="selected"{/if}>{$c.name}
{/foreach}
  </select></td>
	    <td align="center"><input name="ingredient-1" size="45" type="text"></td>
    	<td align="center"><input name="base-1" autocomplete="off" id="base-1" type="text" class="base"></td>
	    <td align="center"><select name="main-1" id="main-1">
        <option value="1">Yes</option>
        <option value="0" selected="selected">No</option>
        </select></td>
	    <td align="center"><a href="javascript:void(0)" class="RemoveRow"><img src="/images/admin/banlist_16.png" /></a></td>
   		<td>&nbsp;</td>
    </tr>
    {/if}
    </tbody>
</table>
<input type="button" value="Add Row" id="AddRow" class="button" /><input type="hidden" name="ingredient_order" value="" id="ingredient_order" />
<div id="debugArea"></div>

    {if $extra_fields}
<div class="seperator"></div>
<div class="float-right"><input type="submit" name="Submit" value="{$lang.save}" class="button"></div>
 <h2 class="seperator">{$lang.extra_fields}</h2>
     <div id="extra_fields_div">{include file="common/extra_fields_modify.tpl"}
     </div>
     {/if}
<div class="seperator"></div>
<div class="float-right"><input type="submit" name="Submit" value="{$lang.save}" class="button"></div>
<h2>Details</h2>
<span style="display: none;" class="autosave_saving">Saving�</span>
<table width="970" border="0" cellspacing="5" cellpadding="5">
  <tr class="TableSubHead">
    <th>Category</th>
    <th>Methods</th>
    </tr>
    <tr>
    <td valign="top">	<div class="categories_list" id="categories_list">
    <ul>
    {section name=cat_num loop=$allcategories}
    <li><input type="checkbox" name="cat[]" id="cat-{$allcategories[cat_num].categoryid}" value="{$allcategories[cat_num].categoryid}" {foreach from=$item.category item=a}{if $a.categoryid eq $allcategories[cat_num].categoryid}checked{/if}{/foreach} /> <label for="cat-{$allcategories[cat_num].categoryid}">{$allcategories[cat_num].category}</label></li>
    {/section}
    </ul>
    </div>     
    <a id="category-add-toggle" href="javascript:void(0);" class="hide-if-no-js" tabindex="3" rel="category-add">+ Add New Category</a>
    <div id="category-add" style="display: none">
	<p id="category-add" class="wp-hidden-child">
	<label class="screen-reader-text" for="newcat">Category Title</label> <input name="new_cat_title" id="newcat" class="new-cat" tabindex="3" aria-required="true" type="text">

    <label class="screen-reader-text" for="alias">Alias</label><input name="alias" id="alias" class="new-cat" tabindex="3" aria-required="true" type="text"><br />
	<label for="newcat_parent">Parent category:</label>
    <select name="parent_catid" class="new-cat" id="parent_catid">
        <option value="0">{$lang.root_category}</option>    
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $category.parentid}selected{elseif $allcategories[cat_num].categoryid eq $catid AND !$smarty.get.action}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select>
	<input id="category-add-sumbit" class="add-cat" value="Add" tabindex="3" type="button">

    </div></td>
    <td valign="top"><div class="categories_list" id="methods_list">
    <ul>
    {section name=cat_num loop=$cooking_methods}
    <li><input type="checkbox" name="cooking_methods[]" id="cooking_methods-{$cooking_methods[cat_num].categoryid}" value="{$cooking_methods[cat_num].categoryid}" {foreach from=$item.cooking_methods item=a}{if $a.categoryid eq $cooking_methods[cat_num].categoryid}checked{/if}{/foreach} /> <label for="cooking_methods-{$cooking_methods[cat_num].categoryid}">{$cooking_methods[cat_num].category}</label></li>
    {/section}
    </ul>
    </div>     
    <a id="method-add-toggle" href="#category-add" class="hide-if-no-js" tabindex="3" rel="method-add">+ Add New Method</a>
    <div id="method-add" style="display: none">
	<p class="wp-hidden-child">
	<label class="screen-reader-text" for="newcat">method Title</label> <input name="new_method_title" id="newcat" class="new-method" tabindex="3" aria-required="true" type="text">

    <label class="screen-reader-text" for="alias">Alias</label><input name="alias" id="alias" class="new-method" tabindex="3" aria-required="true" type="text"><br />
	<label for="newcat_parent">Parent method:</label>
	<input id="method-add-sumbit" class="add-method" value="Add" tabindex="3" type="button">

    </div></td>
    </tr>
</table>
<table width="970" border="0" cellspacing="5" cellpadding="5">
  <tr class="TableSubHead">
    <th>Short description</th>
    <th>Tags</th>
    <th>Difficulty</th>
    </tr>
    <tr>
    <td valign="top">	<textarea name="description" cols="60" rows="5" id="description">{$item.description}</textarea><br /><a href="javascript:void(0);" class="open-editor" rel="description">Open Advanced Editor</a></td>
    <td valign="top"><textarea name="tags" cols="40" rows="5" class="tags-input" id="tags">{$tags}</textarea></td>
    <td valign="top"><select name="difficulty_level" id="difficulty_level">       
{section name=cat_num loop=$difficulty_level}
<option value="{$difficulty_level[cat_num].categoryid}" {if $difficulty_level[cat_num].categoryid eq $item.difficulty_level.categoryid}selected{/if}>{$difficulty_level[cat_num].category}</option>
{/section}
</select></td>
    </tr>
</table>
  

<input type="submit" name="Submit" value="{$lang.save}" class="button"> <span style="display: none;" class="autosave_saving">Saving�</span>
 
</form>

</div>
{literal}
	<script type="text/javascript">

$(function(){
if ($('#action').val() == 'add')
{
	var file_to_call = 'new.php?ajax_call=1';
}
else { var file_to_call = 'modify.php?ajax_call=1';}

$('#form1').livequery('change', function() {
$.PeriodicalUpdater(file_to_call, {
  method: 'post',          // method; get or post
  data:  $('#form1 :input') ,             // array of values to be passed to the page - e.g. {name: "John", greeting: "hello"}
  minTimeout: 8000,       // starting value for the timeout in milliseconds
  maxTimeout: 8000,       // maximum length of time between requests
  multiplier: 2,          // if set to 2, timerInterval will double each time the response hasn't changed (up to maxTimeout)
  type: 'text',           // response type - text, xml, json, etc.  See $.ajax config options
  maxCalls: 0,            // maximum number of calls. 0 = no limit.
  autoStop: 0             // automatically stop requests after this many returns of the same data. 0 = disabled.
}, function(data) {
  // Handle the new data (only called when there was a change).

  if ($('#itemid').val() == '')//FIRST TIME RUN, WILL RETURN AN ID
  {
	  $('#itemid').attr({'value':data});
	  $('#action').attr({'value':'update'});
  }
  $('.autosave_saving').fadeIn().idle(2000).fadeOut('slow');
  $('#debug').html(data);
});
});//END LIVEQUERY
		});
  $.fn.idle = function(time)
  {
      var o = $(this);
      o.queue(function()
      {
         setTimeout(function()
         {
            o.dequeue();
         }, time);
      });
      return this;              //****
  }

	</script>
{/literal}