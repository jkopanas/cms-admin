{if $type eq "edit"}
<table cellpadding="0" cellspacing="0" class="widefat">
      <tr>
        <td colspan="9"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
<th scope="col">{$lang.provider}</th>
<th width="84" scope="col">&nbsp;</th>
</tr>

{section name=w loop=$content}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$content[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$content[w].id}</strong></td>
            <td><a href="modify.php?id={$content[w].id}">{$content[w].title}</a></td>
            <td>{foreach from=$content[w].category item=a name=b}<a href="category_content.php?cat={$a.categoryid}">{$a.category}</a>{if !$smarty.foreach.b.last}, {/if}{/foreach}</td>
            <td>{$content[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $content[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td>{if $ADMIN eq 1}<a href="users_content.php?id={$content[w].uid}">{$content[w].user.user_data.uname}</a>{else}{$content[w].user.user_data.uname}{/if}</td>
            <td><a href="{$URL}/content.php?id={$content[w].id}&preview=1&keepThis=true&TB_iframe=true&height=700&width=900" class="thickbox" target="_blank" title="Preview Item"><img src="/images/admin/preview_16.png" width="16" height="16" border="0" /></a> <a href="content_modify_img.php?id={$content[w].id}" title="Media Files"><img src="/images/admin/mediafolder.gif" width="16" height="16" border="0" /></a> <a href="content_modify_related.php?id={$content[w].id}" title="Related Items"><img src="/images/admin/redo.gif" width="16" height="16" border="0" /></a></td>
  </tr>
		  {/section}
      <tr>
        <td colspan="9"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
  <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "dashboard"}
<table cellpadding="0" cellspacing="0" class="widefat" {if $WIDTH}style="width:{$WIDTH}px"{/if}>
      <tr>
        <td colspan="7"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
<th width="56" scope="col">&nbsp;</th>
</tr>

{section name=w loop=$content}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$content[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$content[w].id}</strong></td>
            <td><a href="{$MODULE_FOLDER}/content_modify.php?id={$content[w].id}">{$content[w].title}</a></td>
            <td>{$content[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $content[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td><a href="{$MODULE_FOLDER}/content_modify_img.php?id={$content[w].id}"><img src="/images/admin/mediafolder.gif" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/content_modify_opt.php?id={$content[w].id}"><img src="/images/admin/optiongroup.gif" width="16" height="16" border="0" /></a> <a href="{$MODULE_FOLDER}/content_modify_related.php?id={$content[w].id}"><img src="/images/admin/redo.gif" width="16" height="16" border="0" /></a></td>
            
    </tr>
		  {/section}
      <tr>
        <td colspan="7"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" /> <input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="deactivate"; document.{$form_name}.submit();{rdelim}' value="Deactivate Checked" /> <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
  <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "editor"}
<table cellpadding="3" cellspacing="0" class="widefat">
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" /></td>
      </tr>

<tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.availability}</th>
</tr>

{section name=w loop=$content}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$content[w].id}]"  type="checkbox" id="check_values" value="1"></td>
            <td><strong>{$content[w].id}</strong></td>
            <td><img src="{$URL}/{$content[w].image}" /></td>
            <td><input type="text" name="content_title-{$content[w].id}" value="{$content[w].title}" onkeyup="document.getElementsByName('ids[{$content[w].id}]')[0].checked='true'" /></td>
            <td><select name="categoryid-{$content[w].id}"  onchange="document.getElementsByName('ids[{$content[w].id}]')[0].checked='true'">
                       
{section name=cat_num loop=$allcategories}

              <option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $content[w].catid}selected{/if}>{$allcategories[cat_num].category}</option>
              
{/section}

            </select>            </td>
    <td align="center"><select name="active-{$content[w].id}"  onchange="document.getElementsByName('ids[{$content[w].id}]')[0].checked='true'">
        <option value="0" {if $content[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $content[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
    </select></td>
    </tr>
		  {/section}
      <tr>
        <td colspan="8"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="first_activate"; document.{$form_name}.submit();{rdelim}' value="Activate Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}"> <input type="hidden" name="mode" id="mode" value=""></td></tr>
</table>
{elseif $type eq "search_results_admin"}
<table cellpadding="3" cellspacing="0" class="widefat">
      <tr>
        <td colspan="9">{include file="modules/content/pagination.tpl"}</td>
      </tr>

      <tr>
        <th colspan="7" scope="col"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />       </th>
      </tr>
  <tr>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">ID</th>
  <th scope="col">&nbsp;</th>
  <th scope="col">{$lang.pos}</th>
  <th scope="col">{$lang.title}</th>
{if $eshop_module}
<th scope="col">{$lang.price}</th>
{/if}
{if $locations_module}
<th scope="col">{$lang.location}</th>
{/if}
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.availability}</th>
</tr>

{section name=w loop=$content}
          <tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><input name="ids[{$content[w].id}]" type="checkbox" id="check_values" value="1"></td>
            <td><strong><a href="{$MODULE_FOLDER}/modify.php?id={$content[w].id}">{$content[w].id}</a></strong></td>
            <td><a href="{$MODULE_FOLDER}/content_modify.php?id={$content[w].id}"><img src="{$URL}/{$content[w].image}" /></a></td>
            <td><input name="orderby-{$content[w].id}" type="text" id="orderby-{$content[w].id}" size="3" value="{$content[w].orderby}" onkeyup="document.getElementsByName('ids[{$content[w].id}]')[0].checked='true'"  /></td>
            <td><input type="text" name="content_title-{$content[w].id}" value="{$content[w].title}" onkeyup="document.getElementsByName('ids[{$content[w].id}]')[0].checked='true'" /></td>
            {if $eshop_module}
            <td><input type="text" name="eshop_price-{$content[w].id}" id="eshop_price-{$content[w].id}" value="{$content[w].eshop.price}"  onkeyup="document.getElementsByName('ids[{$content[w].id}]')[0].checked='true'"/></td>
            {/if}
            {if $locations_module}
            <td><select name="locations_id-{$content[w].id}" id="locations_id-{$content[w].id}" onchange="document.getElementsByName('ids[{$content[w].id}]')[0].checked='true'">
            {section name=a loop=$all_locations}
            <option value="{$all_locations[a].id}" {if $content[w].location.categoryid eq $all_locations[a].categoryid}selected{/if}>{$all_locations[a].category}</option>
            {/section}
            </select></td>
            {/if}
            <td>{foreach from=$content[w].category item=a name=b}<a href="category_content.php?cat={$a.categoryid}">{$a.category}</a>{if !$smarty.foreach.b.last}, {/if}{/foreach}</td>
            <td align="center"><select name="active-{$content[w].id}" onchange="document.getElementsByName('ids[{$content[w].id}]')[0].checked='true'">
        <option value="0" {if $content[w].active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $content[w].active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select></td>
    </tr>
		  {/section}
      <tr>
        <td colspan="9"><input name="button" type="button" onclick='javascript: if (confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="mass_edit"; document.{$form_name}.submit();{rdelim}' value="Update Checked" />
          <input name="button" type="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="delete"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
       <input type="hidden" name="page" id="page" value="{$page}{if $QUERY_STRING}?{$QUERY_STRING}{/if}{if $search AND !$QUERY_STRING}?search=1{elseif $search AND $QUERY_STRING}&search=1{/if}">
       <input type="hidden" name="mode" id="mode" value="" /></td>
      </tr>
</table>
{else}
<table cellpadding="0" cellspacing="0" class="widefat">
<thead>
<tr>
  <th scope="col">ID</th>
<th scope="col">{$lang.title}</th>
<th scope="col">{$lang.category}</th>
<th scope="col">{$lang.date_added}</th>
<th scope="col">{$lang.availability}</th>
<th scope="col">{$lang.provider}</th>
</tr>
</thead>
{section name=w loop=$content}
<tr {cycle values="'FFFFFF', class='alternate'"}>
            <td><strong>{$content[w].id}</strong></td>
            <td><a href="content_modify.php?id={$content[w].id}">{$content[w].title}</a></td>
            <td><a href="category_content.php?cat={$content[w].catid}">{$content[w].category}</a></td>
            <td>{$content[w].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
            <td align="center">{if $content[w].active eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
            <td>{if $ADMIN eq 1}<a href="users_content.php?id={$content[w].uid}">{$content[w].user.uname}</a>{else}{$content[w].provider}{/if}</td>
    </tr>
		  {/section}
</table>
{/if}