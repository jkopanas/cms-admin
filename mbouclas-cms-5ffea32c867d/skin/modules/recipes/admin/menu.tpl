<ul id="adminmenu2">
	<li><a href="index.php" {if $submenu eq "main"}class="current1"{/if}>{$lang.home}</a></li>
	<li><a href="new.php" {if $submenu eq "add"}class="current1"{/if}>Add Recipe</a></li>
	<li><a href="search.php" {if $submenu eq "search"}class="current1"{/if}>Search Recipes</a></li>
	<li><a href="search.php" {if $submenu eq "modify"}class="current1"{/if}>Modify Recipe</a></li>
    <li><a href="categories.php" {if $submenu eq "categories"}class="current1"{/if}>Categories</a></li>
    <li><a href="cooking_methods.php" {if $submenu eq "cooking_methods"}class="current1"{/if}>Cooking Methods</a></li>
    <li><a href="difficulty_level.php" {if $submenu eq "difficulty_level"}class="current1"{/if}>Difficulty Levels</a></li>
    <li><a href="conversion_units.php" {if $submenu eq "units"}class="current1"{/if}>Units</a></li>
    <li><a href="extra_fields.php" {if $submenu eq "extra_fields"}class="current1"{/if}>Extra Fields</a></li>
	<li><a href="settings.php" {if $submenu eq "settings"}class="current1"{/if}>Module Settings</a></li>
</ul>