<div id="delete-confirmation"></div>

		<script id="delete-confirmation_menu" type="text/x-kendo-template">
			<p class="delete-message" >{$lang.confirm_deletion} </p> <br/>
			<button class="k-button delete-confirm" data-id="#= item #">{$lang.delete}</button>
			<a href="\#" class="float-right spacer-right delete-cancel">{$lang.lbl_cancel}</a>
		</script>


		<table id="gridview">
		 	<thead>
      		<tr>
				<th data-order="2" data-field="varName" data-width="23%" data-editable="false" data-filterable="false" data-type="string"  data-title="Variable Name">Variable Name</th>	
				<th data-order="3" data-field="title" data-width="23%" data-editable="false" data-filterable="false" data-type="string"  data-title="Title">Title</th>
			    <th data-order="5" data-field="type" data-width="10%" data-editable="false" data-filterable="false" data-type="string"  data-title="Type">Type</th>
			    <th data-order="6" data-field="file" data-width="29%" data-editable="false" data-filterable="true" data-type="string"  data-title="file">Type</th>
          		<th  data-template="">&nbsp;</th>
      		</tr>
  			</thead>
		</table>
		
		<div id="FormEdit" class="formEl_a">
		
		</div>
		
		
		<script id="OpenEditSettings" type="text/kendo-ui-template">
		<div class="tabstrip">
			<span class="float-right margin-top">	
					<span  id='SaveSetting' name="SaveSetting" data-id="# if (item.id) { ##=item.id ## } else { #0# } #" class="k-button pointer spacer-right"  >{$lang.save}</span>
					<span  id='CancelSetting' data-id="# if (item.id) { ##=item.id ## } else { #0# } #" class="CancelEdit k-button pointer spacer-top spacer-right"  > Cancel </span>
			</span>
		<ul>
			<li class="k-state-active">Βασικές Ρυθμίσεις</li>
			<li>Field Values</li>
		</ul>
		<div class="formEl_a">
                <div class="padding">
       			<div class="sepH_b  spacer-right">
       				<label class="lbl_a" for="var_name">{$lang.var_name} :</label>
        			<input type="text" class="SettingData inpt_a" value="# if (item.varName) { # #= item.varName # # } #" id="varName" data-id="# if (item.id) { # #=item.id # # } else { #0# } #" name="varName" data-bind="events: { keyup: inputValue}">
        		</div>
        		<div class="sepH_b  spacer-right">
       				<label class="lbl_a" for="field_name">{$lang.title} :</label>
        			<input type="text" class="SettingData inpt_a" value="# if (item.title) { ##= item.title # # } #" id="title" name="title" data-id="# if (item.id) { # #=item.id # # } else { #0# } #" data-bind="events: { keyup: inputValue}">
        		</div>
        		<div class="sepH_b  spacer-right">
       				<label class="lbl_a" for="file">{$lang.file} :</label>
        			<input type="text" class="SettingData inpt_a" value="# if (item.file) { ##= item.file # # } #" id="file" name="file" data-id="# if (item.id) { # #=item.id # # } else { #0# } #" data-bind="events: { keyup: inputValue}">
        		</div>
				<input type="hidden"  name="module" class="SettingData" value="{$current_module}" />
        		</div>
		<div>
        <div class="padding formEl_a">
		<div class="sepH_b ">
 			<label class="lbl_a" for="field_type" >{$lang.field_type}</label>
    		<select name="type" id="type" class="SettingData inpt_a" data-id="# if (item.id) { # #=item.id # # } else { #0# } #"  data-bind="events: { change: SelectValue }">
		          	 <option value="text" # if (item.type == "text" ) { # selected # } # >{$lang.text}</option>
          			 <option value="area"# if (item.type == "area" ) { # selected # } # >{$lang.textarea}</option>
          			 <option value="radio" # if (item.type == "radio" ) { # selected # } # >{$lang.radiobtn}</option>
          			 <option value="hidden" # if (item.type == "hidden" ) { # selected # } # >{$lang.hidden}</option>	
          			 <option value="document" # if (item.type == "document" ) { # selected # } # >{$lang.file}</option>
          			 <option value="image" # if (item.type == "image" ) { # selected # } # >{$lang.image}</option>
          			 <option value="media" # if (item.type == "media" ) { # selected # } # >{$lang.media}</option>
          			 <option value="link" # if (item.type == "link" ) { # selected # } # >{$lang.link}</option>
          			 <option value="select" # if ( item.type == "checkbox" ||  item.type == "radio" || item.type == "dropdown" || item.type == "select" ) { # selected # } # >{$lang.multiselect}</option>
      		</select> 
	 	</div>
		<div class="sepH_b multioptions # if ( item.type != "checkbox" &&  item.type != "radio" && item.type != "dropdown" && item.type != "select" ) { # hidden # } #">
 			<label class="lbl_a" for="field_type" >{$lang.data_type}</label>
    		<select name="select_type" id="select_type" class="SettingData inpt_a">
		          	<option value="checkbox" # if (item.type == "checkbox" ) { # selected # } # >{$lang.checkbox}</option>
					<option value="radio" # if (item.type == "radio" ) { # selected # } # >{$lang.radiobtn}</option>
					<option value="dropdown" # if (item.type == "dropdown" ) { # selected # } # >{$lang.dropdown}</option>
					<option value="select" # if (item.type == "select" ) { # selected # } # >Select</option>
      		</select> 
  		</div>
		<div class="sepH_b dp50 multioptions # if ( item.type != "checkbox" &&  item.type != "radio" && item.type != "dropdown" && item.type != "select" ) { # hidden # } # "  >
 			<label class="lbl_a" for="field_type" >{$lang.data}</label>
			<table border="0" cellspacing="5" cellpadding="5" id="dataTable" class="table_a smpl_tbl spacer-top" >
				<tr>
					<td class="k-header">&nbsp;<input type="hidden" name="TableOrder" class="settings" value="" /></td>
					<td class="k-header">ID</td>
					<td class="k-header">{$lang.text}</td>
					<td class="k-header">Value</td>
					<td class="k-header"><a href="\#" id="addRow" class="k-icon k-add"></a></td>
				</tr>
				<tbody class="settingcontent">
				{assign var=i value=0}
				# if ( item.options ) { #
					# for (var i = 0; i < item.options.values.length; i++) { #
							<tr class="dataRow" id="row#= i #">
								<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
								<td><input type="text" name="values[]" class="SettingDataJson inpt_a" value="#= item.options.values[i].value #"></td>
								<td><input type="text" name="translation[]" class="SettingDataJson inpt_a" value="#= item.options.values[i].translation #"></td>
								<td><input type="radio" name="default"  class="SettingData inpt_a" value="#= item.options.values[i].default #" # if ( item.options.values[i].value ) { # checked # } #></td>
								<td># if ( i != 0 ) {  #<a href="\\#" class="removeRow" rel="row#= i #">{$lang.remove}</a># } #</td>
							</tr>
					# } #
				# } else { #
						<tr class="dataRow" id="row0">
							<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
							<td><input type="text" name="values[]" class="SettingDataJson inpt_a" value=""></td>
							<td><input type="text" name="translation[]" class="SettingDataJson inpt_a" value=""></td>
							<td><input type="radio" name="default"  class="SettingData inpt_a" value="" ></td>
							<td></td>
						</tr>
					# i=1; #
			 	# } #
				</tbody>
			</table>
			<input type="hidden" value="#= i #" id="icounter"/>
  		</div>
		<div class="seperator"></div>
  		</div>
  		</div>
		</div>
		</script>
		
		 <script id="template-toolbar-PagesSearch" type="text/kendo-ui-template">	
			<div class="float-right k-button k-button-icontext CreateSet"> Create New </div>
		</script>
		
		<script  class="columns-gridview" type="text/kendo-ui-template">			
				<th data-order="1" data-field="orderby" data-width="4%" data-editable="false" data-filterable="false" data-type="string"  data-title="#">#</th>
        </script>
		
		<script  id="template-varName-gridview" type="text/kendo-ui-template">			
				<span id="varName# if (id) { ##= id## } else { #0# } #"> #= varName #</span>
        </script>
        
        <script  id="template-title-gridview" type="text/kendo-ui-template">			
				<span id="title# if (id) { ##= id## } else { #0# } #"> #= title #</span>
        </script>
        
        <script  id="template-file-gridview" type="text/kendo-ui-template">			
				<span id="file# if (id) { ##= id## } else { #0# } #"> #= file #</span>
        </script>
        
        <script  id="template-type-gridview" type="text/kendo-ui-template">			
				<span id="type# if (id) { ##= id## } else { #0# } #"> #= type #</span>
        </script>
        
        <script type="text/x-kendo-template" id="template-orderby-gridview">
			<img src="/images/admin/move-arrow.png" width="16" height="16" data-id="#= id #" data-order="#= orderby #" class="handle">			
		</script>

		<script id="id-commands-gridview" class="id-commands-gridview" type="text/kendo-ui-template">
				<span  id='SettingRow#= id #' data-itemid='#= id #' class="EditSet k-icon k-edit pointer spacer-right"  ></span>
				<span  id='#= id #' data-itemid='#= id #' class="DeleteSet k-icon k-delete pointer"  ></span>		
        </script>
        
        <script>
        head.js('/scripts/admin2/grid.js');
        head.js('/scripts/jquery-ui-1.8.18.custom.min.js');
        head.js('/scripts/admin2/settings/settings.js');
		</script>