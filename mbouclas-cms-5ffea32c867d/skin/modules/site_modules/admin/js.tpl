	<script language="JavaScript" src="/scripts/prototype.js"></script>
	<script language="JavaScript" src="/scripts/scriptaculous.js"></script>
	<script type="text/javascript" src="/scripts/builder.js"></script>
	<script type="text/javascript" src="/scripts/effects.js">
    </script><script type="text/javascript" src="/scripts/dragdrop.js"></script>
	<script type="text/javascript" src="/scripts/controls.js"></script>
	<script type="text/javascript" src="/scripts/slider.js"></script>
    
{literal}
<script language="JavaScript">
function display_ajax_loader(res_div)
{
xajax.$(res_div).innerHTML = '';
xajax.$(res_div).innerHTML = '<img src="/images/ajax-loader.gif">' + ' Loading....';
}
	sections = ['group1','group2','group3'];

	function createNewSection(name) {
		var name = $F('sectionName');
		if (name != '') {
			var newDiv = Builder.node('div', {id: 'group' + (sections.length + 1), className: 'section', style: 'display:none;' }, [
				Builder.node('h3', {className: 'handle'}, name)
			]);

			sections.push(newDiv.id);
			$('page').appendChild(newDiv);
			Effect.Appear(newDiv.id);
			destroyLineItemSortables();
			createLineItemSortables();
			createGroupSortable();
		}
	}

	function createLineItemSortables() {
		for(var i = 0; i < sections.length; i++) {
			Sortable.create(sections[i],{tag:'div',dropOnEmpty: true, containment: sections,only:'lineitem'});
		}
	}

	function destroyLineItemSortables() {
		for(var i = 0; i < sections.length; i++) {
			Sortable.destroy(sections[i]);
		}
	}

	function createGroupSortable() {
		Sortable.create('page',{tag:'div',only:'section',handle:'handle'});
	}

	/*
	Debug Functions for checking the group and item order
	*/
	function getGroupOrder(group,gid) {
		var groupid = document.getElementById(group);
		var alerttext = '';

			var order = Sortable.serialize(group);
			alerttext +=  Sortable.sequence(group);
//alert(alerttext);
		document.getElementById('list-'+gid).value = alerttext;
		//alert(alerttext);
		return false;
	}

function load_module_settings(module_id,res_div)
{

//Effect.Appear(res_div);
	display_ajax_loader(res_div);
	xajax_load_module_settings(module_id,res_div);
}
	
function save_module_box_settings(module_id,res_div,formid)
{
Effect.Pulsate(res_div);

xajax_save_module_box_settings(module_id,res_div,xajax.getFormValues("settings_form"));
}
	</script>


<style>


	div.section,div#createNew {
		border: 1px solid #CCCCCC;
		margin: 30px 5px;
		padding: 0px 0px 10px 0px;
		background-color: #EFEFEF;
	}

	div#createNew input { margin-left: 5px; }

	div#createNew h3, div.section h3{
		font-size: 14px;
		padding: 2px 5px;
		margin: 0 0 10px 0;
		background-color: #CCCCCC;
		display: block;
	}

	div.section h3 {
		cursor: move;
	}

	div.lineitem {
		margin: 3px 10px;
		padding: 25px;
		background-color: #FFFFFF;
		cursor: move;
		width:200px;
	}

	h1 {
		margin-bottom: 0;
		font-size: 18px;
	}
</style>
{/literal}