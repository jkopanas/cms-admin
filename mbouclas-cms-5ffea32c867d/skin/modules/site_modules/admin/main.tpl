{if $smarty.get.ok}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}
<div class="wrap">
<div id="zeitgeist">
  <h3>{$lang.in_this_section}</h3>
  {section name=a loop=$site_pages}
 <a href="{$MODULE_FOLDER}/index.php?page={$site_pages[a].id}">{$site_pages[a].page}</a><br />
 {/section}
 <h3>Available Modules</h3>
 {section name=a loop=$modules_list}
 <a href="{$MODULE_FOLDER}/index.php?page={$page}&mid={$modules_list[a].id}">{$modules_list[a].title}</a><br />
 {/section}
  </div>



<h2>Module Manager {if $module_page} :: <strong>{$module_page.page}</strong>{/if}</h2>





{if $module}
<table border="1" cellpadding="5" cellspacing="0">
<tr><td><div id="module_settings">{include file="modules/site_modules/admin/module_settings.tpl"}</div></td></tr>
</table>
{/if}
{if $smarty.get.page}
<br />
<br />
<form name="modules_form" id="modules_form" action="{$SELF}" method="post">
<table border="1">
<tr>
{section name=a loop=$areas}
<th scope="cols">Area {$areas[a].name}</th>
{/section}
</tr>
<Tr>
{section name=a loop=$areas}
<td valign="top">

<div style="position: relative;" id="group{$areas[a].id}" class="section">
{foreach from=$areas[a].mlist item=k}
<div style="position: relative;" id="{$areas[a].name}_{$k.id}" class="lineitem"><input name="module-{$k.id}_{$areas[a].name}" type="checkbox" id="module-{$k.id}" value="1" {foreach from=$areas[a].modules item=mid}{if $k.id eq $mid}checked{/if}{/foreach} />{$k.title} [<a href="{$MODULE_FOLDER}/index.php?page={$page}&mid={$k.id}" >Settings</a>] {* [<a href="javascript:load_module_settings({$k.id},'module_settings');" >Settings</a>]*}</div>
{/foreach}
</div>
  <input type="hidden" value="" id="list-{$areas[a].name}" name="list-{$areas[a].name}" />

      </td>
{/section}
</Tr>
</table>

<input name="save" type="submit" class="button" onclick="{section name=a loop=$areas}getGroupOrder('group{$areas[a].id}','{$areas[a].name}');{/section}" value="{$lang.save}" />
<input type="hidden" value="save" id="action" name="action" />
<input type="hidden" value="{$id}" id="id" name="id" />
</form>
{/if}
<div style="clear:both"></div>
 </div>
 {literal}
   <script type="text/javascript">
	// <![CDATA[
	{/literal}
	{section name=a loop=$areas}
	Sortable.create('group{$areas[a].id}{literal}',{tag:'div'});
	{/literal}{/section}{literal}
	
	// ]]>
 </script>
 {/literal}
