{literal}
<script type="text/javascript">
function display_ajax_loader(res_div)
{
xajax.$(res_div).innerHTML = '';
xajax.$(res_div).innerHTML = '<img src="/images/ajax-loader.gif">' + ' Loading....';
}

function show_news(cat,start,res_div,type)
{
display_ajax_loader(res_div);
xajax_ajax_get_news(cat,start,res_div,type);

}
</script>
{/literal}