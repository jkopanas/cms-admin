<div id="large">
	<img src="/images/island/1.jpg" title="Big Plaine" alt="image01.jpg" />
</div>
<ul id="thumbnail">
	<li><a href="/images/island/1.jpg"><img src="/images/island/1_tn.jpg" alt="Cretan door" width="90" border="0" title="images of Crete" /></a></li>
  <li><a href="/images/island/2.jpg"><img src="/images/island/2_tn.jpg" alt="narrow path in crete" width="90" border="0" title="images of Crete"/></a></li>
  <li style="margin-right:0px;"><a href="/images/island/3.jpg"><img src="/images/island/3_tn.jpg" alt="view of Crete" width="90" border="0" title="images of Crete" /></a></li>
  <li><a href="/images/island/4.jpg"><img src="/images/island/4_tn.jpg" alt="lighthouse in Crete" width="90" border="0" title="images of Crete" /></a></li>
  <li><a href="/images/island/5.jpg"><img src="/images/island/5_tn.jpg" alt="small house in crete" width="90" border="0" title="images of Crete" /></a></li>
  <li style="margin-right:0px;"><a href="/images/island/6.jpg"><img src="/images/island/6_tn.jpg" alt="view of Crete" width="90" border="0" title="images of Crete"/></a></li>
</ul> 
<div class="grey" align="right"><a href="{$URL}/crete/" title="read information on Crete" class="grey">Read more about Crete</a></div>