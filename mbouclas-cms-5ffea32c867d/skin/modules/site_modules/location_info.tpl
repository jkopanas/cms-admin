<div id="location_info_box">
{if $message}
{$message}
{else}
<ul class="list">
{foreach from=$locations item=a}
<li><a href="{$URL}/locations/{$a.categoryid}/" title="{$a.category}">{$a.category}</a></li>
{/foreach}
</ul>
{/if}
</div>