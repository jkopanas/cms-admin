<div id="music_news_box">
{if $message}
{$message}
{else}
<img src="/images/music_news_header.gif">
<table width="100%" border="0" cellpadding="0" cellspacing="5">
{section name=a loop=$music_news}
  <tr>
    <td valign="top">&nbsp;</td>
    <td align="right" valign="top" class="box_pagination">{$music_news[a].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
  </tr>
  <tr>
{if $music_news[a].image AND $music_news[a].image ne $music_news_box_settings.default_thumb}<td valign="top" width="{$music_news_box_settings.news_images_thumb_width}"><img src="{$music_news[a].image}" align="left" /></td>
{/if}
<td valign="top" colspan="2"><strong>{$music_news[a].title}</strong>
<br />
{$music_news[a].description|truncate:130:"..."}
</td>
</tr>
<tr><td colspan="2" align="right"><a href="{$URL}/music-news/{$music_news[a].catid}/{$music_news[a].id}/{$PAGE}/">{$lang.more} &gt;&gt;</a></td></tr>
</tr>
  <tr>
    <td style="background:#ffd800; height:1px" colspan="2"><img src="/images/filler.gif" height="1" width="1"></td>
    </tr>
{/section}
</table>
{/if}
<div align="right"><div style="float:left"><a href="{$URL}/music-news/" title="{$lang.more_news}" class="box_pagination" style="color:#000000">{$lang.more_news} &gt;&gt;</a></div>
{include file="common/pagination.tpl" mode="ajax" type="music_news" res_div="music_news_box"}</div>
</div>