<div id="news_box">
{if $message}
{$message}
{else}
<ul class="list">
{foreach from=$news item=a}
<li><a href="{$URL}/news-item/{$a.id}/" title="{$a.title}">{$a.title}</a></li>
{/foreach}
</ul>
{/if}
</div>