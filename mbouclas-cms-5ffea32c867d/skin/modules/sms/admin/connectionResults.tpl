{if $warningMessage!=""}
<div class="messagePlaceHolder messageWarning">
{$warningMessage}
</div>
{/if}
<table width="100%"  border="0" class="widefat">
<tr>
	<td>
    	<strong>ID</strong>
    </td>
	<td>
    	<strong>{$lang.conectionName}</strong>
    </td>
    <td>
    	<strong>{$lang.conectionType}</strong>
    </td>
    <td>
    	<strong>{$lang.status}</strong>
    </td>
    <td>&nbsp;</td>
</tr>
{foreach from=$items item=a}
<tr {cycle values=", class='alternate'"}>
	<td>
    	{$a.id}
    </td>
	<td>
    	{$a.title} {if $a.setDefault==1}(Default){/if}
    </td>
    <td>
    	{$a.connectionType}
    </td>
    <td>
    	{if $a.active==1}<span style="color:green;">{$lang.active}</span>
        {else}<span style="color:red;">{$lang.inactive}</span>
        {/if}
    </td>
    <td>
    	<a href="#" class="editConnection"  rel="{$a.id}"><img height="16" width="16" src="/images/admin/edit_16.png"></a>
    	<a href="#" class="deleteConnection" rel="{$a.id}"><img height="16" width="16" src="/images/admin/delete_16.png"></a>
    </td>
</tr>
{/foreach}
</table>