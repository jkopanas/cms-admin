{include file="modules/sms/admin/menu.tpl"}
<div class="seperator padding wrap">
<h2>{$lang.connections} <a href="#" id="newConnection">({$lang.addConnection})</a></h2>

<div id="connectionResults">
{include file="modules/sms/admin/connectionResults.tpl"}
</div>

<div id="FormDiv">
{include file="modules/sms/admin/connectionForm.tpl"}
</div>
<script src="/scripts/connections.js"></script>