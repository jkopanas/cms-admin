<div id="customRouteFormHolder" class="hidden">
<div class="content">
    <div class="seperator padding wrap">
    <h2>{$lang.addRoute}</h2>
		    <form id="customRouteForm">
            <table>
            <tr>
            	<td align="left">{$lang.value}</td>
                <td align="left">
                <input name="value" class="formData required" value="{$item.value}">
                <input type="hidden" name="id" class="formData" value="{$item.id}" />
                <input type="hidden" name="rid" class="formData" value="{$item.rid}" />
                
                </td>
            </tr>
            <tr>
            	<td align="left">{$lang.markup}</td>
                <td align="left">
                <select name="markup" class="formData">
                <option value="%" {if $item.markup=="%"} selected="selected" {/if}>%</option>
                <option value="Fixed" {if $item.markup=="Fixed"} selected="selected" {/if}>Fixed</option>
                <option value="Plus" {if $item.markup=="Plus"} selected="selected" {/if}>Plus</option>
                </select>
                </td>
            </tr>
            <tr>
            	<td align="left">{$lang.user}</td>
                <td align="left">
                <input name="username" class="autocomplete required" value="{$item.user.uname}"/>
                <input type="hidden" name="uid" id="userID" class="formData" value="{$item.user.id}"/>
                </td>
                
            </tr>
            <tr>
            	<td align="left">{$lang.active}</td>
                <td align="left"><input type="checkbox" name="Active" class="formData" value="1" {if $item.active==1}checked=checked{/if} /></td>
            </tr>
            <tr>
            	<td colspan="2"><input type="submit" value="Submit"/></td>
            </tr>
            </table>
            </form>
    </div>
  </div>
</div>
