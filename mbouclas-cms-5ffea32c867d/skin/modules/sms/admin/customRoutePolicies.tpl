<td colspan="9">
<table class="widefat" border="0" width="100" bgcolor="#FFFFCC">
<tr>
<td><strong>{$lang.value}</strong></td>
<td><strong>{$lang.markup}</strong></td>
<td><strong>{$lang.status}</strong></td>
<td><strong>{$lang.user}</strong></td>
<td><strong>{$lang.updateDate}</strong></td>
<td><strong>{$lang.cost}</strong></td>
<td width="65"></td>
</tr>
{foreach from=$items item=a}
<tr>
<td>{$a.value}</td>
<td>{$a.markup}</td>
<td>
{if $a.active==1}<span style="color:green;">{$lang.active}</span>
{else}<span style="color:red;">{$lang.inactive}</span> 
{/if}
</td>
<td>
{if $a.uid!=0}
{$a.user.uname}
{else}
{$lang.allusers}
{/if}
</td>
<td>{$a.updateDate|date_format:"%d/%m/%Y @ %H:%M"}</td>
<td>{$a.cost}</td>
<td>
    	<a href="#" class="editCustomRoute" rel="{$a.id}"><img height="16" width="16" src="/images/admin/edit_16.png"></a>
    	<a href="#" class="deleteCustomRoute" rel="{$a.id}"><img height="16" width="16" src="/images/admin/delete_16.png"></a>
</td>
</tr>
{/foreach}
</table>
</td>
