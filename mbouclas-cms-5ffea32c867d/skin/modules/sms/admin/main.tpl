{include file="modules/sms/admin/menu.tpl"}
<div class="seperator padding wrap">
<h2><a href="javascript:void(0)" class="ShowSearchTable Slide" rel="#SearchTable">Αναζήτηση</a></h2>
<div class="debug"></div>

<form name="searchform" id="searchMessagesForm"  method="POST">
<table width="100%" border=0 cellpadding="5" cellspacing="5" id="SearchTable">

<TR>
  <TD nowrap class=FormButton>{$lang.results_per_page}</TD>
  <TD width="933"><select name="results_per_page" id="results_per_page" class="secondarySearchData">
    <option value="10" {if $data.results_per_page eq 10}selected{/if}>10</option>
  	<option value="20" {if $data.results_per_page eq 20}selected{/if}>20</option>
    <option value="60" {if $data.results_per_page eq 60}selected{/if}>60</option>
    <option value="100" {if $data.results_per_page eq 100}selected{/if}>100</option>
  </select>  </TD>
</TR>
<TR>
  <TD nowrap class=FormButton>{$lang.order_by}</TD>
  <TD> <select name="orderby" class="secondarySearchData">
  <option value="id">#ID</option> 
   <option value="dateAdded">{$lang.sentDate}</option>
   <option value="dlrStatus">{$lang.status}</option>
   <option value="destination">{$lang.destination}</option>
   <option value="uname">{$lang.uname}</option>
</select>  
{$lang.way} : 
<select name="way" id="sort_direction" class="secondarySearchData">
<option value="desc">{$lang.descending}</option>
<option value="asc">{$lang.ascending}</option>             
        </select></TD>
</TR>
<TR>
  <TD width="94" nowrap class=FormButton>#ID</TD>
  <TD>
  <INPUT name="id" type=text id="id" value="{$data.id}" class="SearchData"  /></TD>
</TR>

<TR>
  <TD width="94" nowrap class=FormButton>msgID</TD>
  <TD>
  <INPUT name="msgid" type=text id="msgid" value="{$data.msgid}" class="likeData"  /></TD>
</TR>
<tr>
  <td nowrap class=FormButton>{$lang.uname}</td>
  <td><INPUT name="uname" type=text id="uname" value="{$data.uname}" class="SearchData"  /></td>
</tr>
<tr>
  <td nowrap class=FormButton>{$lang.originator}</td>
  <td><INPUT name="originator" type=text id="originator" value="{$data.originator}" class="likeData"  /></td>
</tr>
<tr>
  <td nowrap class=FormButton>{$lang.destination}</td>
  <td><INPUT name="destination" type=text id="destination" value="{$data.destination}" class="likeData"  /></td>
</tr>
<tr>
  <td nowrap class=FormButton>{$lang.message}</td>
  <td><INPUT name="messageText" type=text id="messageText" value="{$data.messageText}" class="likeData"  /></td>
</tr>
<tr>
  <td nowrap class=FormButton>{$lang.status}</td>
  <td>
    <select name="dlrStatus" class="SearchData">
    <option value="">{$lang.all}</option>
    {foreach from=$avStatus item=a}
    <option value="{$a.dlrStatus}">
    {$a.dlrStatus}
    </option>
    {/foreach}
    </select>
    </td>
</tr>
<tr>
<td nowrap class=FormButton>{$lang.archive}</td>
<td>
<INPUT name="archived" type="checkbox" id="archived" value="1" class="SearchData"  />
</td>
</tr>
<tr>
  <td class=FormButton nowrap>{$lang.date}</td>
  <td>{$lang.from} : <input type="text" id="dateFrom" name="dateFrom" class="datePicker secondarySearchData" size="30"/> {$lang.to} : <input type="text" id="dateTo" name="dateTo" class="datePicker secondarySearchData" size="30"/> </td>
</tr>
<tr>
  <td colspan="2" align="center" class=FormButton>
    <input type="submit" value="{$lang.search}" class="button" id="searchUsers" name="searchUsers">
    <input name="page" type="hidden" id="page" value="{$page}" class="secondarySearchData">
    <input name="ResultsDiv" type="hidden"  value="#ResultsPlaceHolder" class="secondarySearchData">
    <input name="FormToHide" type="hidden"  value="#SearchTable" class="secondarySearchData">
    <input name="FormToShow" type="hidden"  value="#ResultsPlaceHolder" class="secondarySearchData"></td>
</tr>

</table>
</form>
</div>

<div id="ResultsPlaceHolder" class="hidden">
{include file="modules/sms/admin/resultsTable.tpl"}
</div>

<div id="dlrInfobox" class="hidden">
    <div class="content" align="center">
    
    <div class="seperator wrap padding">
    <h2>{$lang.smsinfo}</h2>
    
    <table width="500"  cellpadding="5" cellspacing="5">
     <tr>
        <td align="left"><strong>message ID(Internal)</strong></td>
	    {literal}<td>{ID}</td>{/literal}
    </tr>
    <tr>
        <td align="left"><strong>Batch ID</strong></td>
	    {literal}<td>{BATCHID}</td>{/literal}
    </tr>
    <tr>
        <td align="left"><strong>message ID(Prov)</strong></td>
	    {literal}<td>{MSGID}</td>{/literal}
    </tr>
     <tr>
        <td align="left"><strong>{$lang.username}</strong></td>
        {literal}<td>{UNAME}</td>{/literal}
    </tr>
    <tr>
        <td align="left"><strong>{$lang.originator}</strong></td>
        {literal}<td>{ORIGINATOR}</td>{/literal}
    </tr>
    <tr>
        <td align="left"><strong>{$lang.destination}</strong></td>
        {literal}<td>{DESTINATION}</td>{/literal}
    </tr>
    <tr>
        <td align="left"><strong>{$lang.message}</strong></td>
        {literal}<td>{MESSAGETEXT}</td>{/literal}
    </tr>
    <tr>
        <td align="left"><strong>{$lang.dateAdded}</strong></td>
        {literal}<td>{DATEADDED}</td>{/literal}
    </tr>
    <tr>
        <td align="left"><strong>{$lang.dateSend}</strong></td>
        {literal}<td>{DATESEND}</td>{/literal}
    </tr>
    <tr>
        <td align="left"><strong>{$lang.status}</strong></td>
        {literal}<td>{DLRSTATUS}</td>{/literal}
    </tr>
    <tr>
        <td align="left"><strong>{$lang.errorno}</strong></td>
       {literal}<td>{ERROR}</td>{/literal}
    </tr>
    </table>
 
    </div>
    
    </div>
</div>
<div id="downloadBox" class="hidden">
	<div class="content" align="center">
        <div class="seperator wrap padding">
	        Download generated {literal}<a href="{HREF}">{/literal}CSV</a>
        </div>
    </div>
</div>

<div id="Tmp" class="hidden">
<div class="container1"></div>
</div>

<script src="/scripts/messages.js"></script>