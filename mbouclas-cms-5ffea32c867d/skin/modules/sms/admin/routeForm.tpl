<div id="routeFormHolder" class="hidden">
<div class="content">
    <div class="seperator padding wrap">
    <h2>{$lang.addRoute}</h2>
		    <form id="routeForm">
            <table>
            <tr>
            	<td align="left">{$lang.routeName}</td>
                <td align="left">
                <input name="title" class="formData required" value="{$item.title}">
                <input type="hidden" name="id" class="formData" value="{$item.id}" />
                </td>
            </tr>
            <tr>
            	<td align="left">{$lang.routeType}</td>
                <td align="left">
                <select name="routingType" class="formData">
                <option value="OUTBOUND" {if $item.routingType=="OUTBOUND"} selected="selected" {/if}>OUTBOUND</option>
                <option value="INBOUND" {if $item.routingType=="INBOUND"} selected="selected" {/if}>INBOUND</option>
                </select>
                </td>
            </tr>
            <tr>
            	<td align="left">{$lang.user}</td>
                <td align="left">
                <input name="username" class="autocomplete required" value="{$item.user.uname}"/>
                <input type="hidden" name="uid" id="userID" class="formData" value="{$item.user.id}"/>
                </td>
                
            </tr>
            <tr>
            	<td align="left">Regular expression</td>
                <td align="left"><input class="formData" name="regex" value="{$item.regex}"></td>
            </tr>
            <tr>
            	<td align="left">{$lang.connection}</td>
                <td align="left">
                <select name="connectionID" class="formData">
                {foreach from=$connections item=a}
                <option value="{$a.id}" {if $a.id==$item.connectionID}selected="selected"{/if}>{$a.title}</option>
                {/foreach}
                </select>
                </td>
            </tr>
            <tr>
            	<td align="left">{$lang.country}</td>
                <td>
                <select name="countryID" class="formData">
                {foreach from=$countries item=a}
                <option value="{$a.id}" {if $a.id==$item.countryID}selected="selected"{/if}>{$a.Country}</option>
                {/foreach}
                </select>
                </td>
            </tr>
            <tr>
            	<td align="left"><p>{$lang.cost}</p></td>
                <td align="left"><input class="formData required" name="cost" value="{$item.cost}"></td>
            </tr>
            <tr>
            	<td align="left">{$lang.active}</td>
                <td align="left"><input type="checkbox" name="Active" class="formData" value="1" {if $item.active==1}checked=checked{/if} /></td>
            </tr>
            <tr>
            	<td colspan="2"><input type="submit" value="Submit"/></td>
            </tr>
            </table>
            </form>
    </div>
  </div>
</div>
