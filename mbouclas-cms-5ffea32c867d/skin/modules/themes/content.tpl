{if !$mapItems && $item}
<div class="page-title category-title">
<h1>{$item.title}</h1>
</div>
{$item.description}
{$item.description_long}

{else}

<div class="seperator"></div>
<div class="col-main">
<div class="std">
<div class="page-title category-title">
<h1>{$items.0.title}</h1>
</div>

{$items.0.description_long}


{if $mapItems}
<div class="seperator"></div>
<div class="page-title category-title">
<h1>{$lang.map}</h1>
</div>

<textarea name="fomatedItemsRaw" class="hidden">{$fomatedItemsRaw}</textarea>
<div id="map" style="width:100%; height:350px;"></div>
{/if}
</div><!-- END STD -->
</div><!--END MAIN COL -->
{if $mapItems}
<div class="col-left sidebar">
<div class="block side-nav-categories" id="categories_block_left" >
	<div class="block-title">
        <strong><span>{$lang.more}</span></strong>
    </div>
    <div class="block-content">        
		<ul>
        {foreach from=$more_items item=x}		
        <li class="item"><a href="/{$FRONT_LANG}/pages/{$x.id}/{$x.permalink}.html"  title="{$x.title}">{$x.title}</a></li>
        {/foreach}
        </ul>
    </div>
</div>
</div><!-- END SIDE -->
{/if}
<script id="markerTemplate" type="text/x-kendo-template">
<div>
<h1><a href="/#= mcms.settings.lang #/pages/#= item.id #/#= item.permalink #.html">#= item.title #</a></h1>
<p>#= item.description #</p>
</div>
</script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
<script>
head.js("/scripts/site/maps/gMaps3.js");
head.js("/scripts/site/shops.js");
</script>


{/if}