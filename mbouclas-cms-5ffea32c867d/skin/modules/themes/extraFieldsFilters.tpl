<ul>
<li><a href="#" class="gMap" title="click for map view"><img src="http://maps.googleapis.com/maps/api/staticmap?center={$mapItems.map.lat},{$mapItems.map.lng}&zoom=6&size=215x215&sensor=false{foreach from=$mapItems.items item=a}&markers=icon:http://simplecrete.com/images/icons/pin1_trans.png|label:S|{$a.lat},{$a.lng}{/foreach}" width="215" height="215" name="gMap" id="gMap" /></a></li>
</ul>
<div class="seperator"></div>
<ul>
<li><h4>{$lang.price}</h4></li>
<il class="spacer-bottom">
<input type="hidden" name="minPrice" value="{$priceRanges.min}" class="price" data-enabled="false" disabled="disabled" /> <input type="hidden" name="maxPrice" value="{$priceRanges.max}" class="price" data-enabled="false" />
<div id="priceRanges"><span class="min"></span>&euro; <div class="float-right"> <span class="max"></span>&euro;</div><div class="clear"></div></div>
                <div id="priceRange">
                    <input />
                    <input />
                </div>
                
                <textarea name="fomatedItemsRaw" class="hidden">{$fomatedItemsRaw}</textarea>
                <textarea name="mapItemsRaw" class="hidden">{$mapItemsRaw}</textarea>
</li>
</ul>
<div class="seperator"></div>
<ul>
<li><h4>{$lang.location}</h4></li>
<li class="spacer-bottom">
<div class="drillDown" id="locationsList" data-table="{$commnonCategories.locations.tableID}">
<ul class="breadCrumbs">
<li><a href="#" rel="0" data-val="{"tableID:::`$commnonCategories.locations.tableID`###categoryid:::0###type:::commonCategory"|base64_encode}" class="commonAll">{$lang.lbl_allRegions}</a></li>
{foreach from=$commnonCategories.locations.nav item=a}
<li><span class="breadCrumb"><img src="/images/breadcrumbs_arrow.png"  /></span></li>
<li><a href="#" data-val="{"tableID:::`$commnonCategories.locations.tableID`###categoryid:::`$a.categoryid`###type:::commonCategory###field:::`$a.category`"|base64_encode}" class="breadCrumb">{$a.category}</a>
{/foreach}
</ul>
<ul id="{if $area eq 'commonCategories'}{$cat.categoryid}{else}0{/if}" data-table="{$commnonCategories.locations.tableID}" class="list view">
{foreach from=$commnonCategories.locations.results item=a}
<li><a href="#" data-val="{"tableID:::`$commnonCategories.locations.tableID`###categoryid:::`$a.categoryid`###type:::commonCategory###field:::`$a.category`"|base64_encode}" class="commonFilter">{$a.category} (<span class="totals">{$a.num_items}</span>) </a> {if $a.subs}<a href="#" class="subs" rel="{$a.categoryid}" data-text="{$a.category}" data-val="{"tableID:::`$commnonCategories.locations.tableID`###categoryid:::`$a.categoryid`###type:::commonCategory###field:::`$a.category`"|base64_encode}"><img src="/images/breadcrumbs_arrow.png"  /></a>{/if}</li>
{/foreach}
</ul>
</div>
<input type="hidden" name="locationFilter" value="{if $area eq 'commonCategories'}{"tableID:::`$commnonCategories.locations.tableID`###categoryid:::`$cat.categoryid`###type:::commonCategory###field:::`$cat.category`"|base64_encode}{/if}" />
{*
<select name="filter" id="location" class="mSelect"  data-type="tree" data-tableID="{$commnonCategories.locations.tableID}">
<option value="">{$lang.lbl_allRegions}</option>
{foreach from=$commnonCategories.locations.results item=a}
<option value="{"tableID:::`$commnonCategories.locations.tableID`###categoryid:::`$a.categoryid`###type:::commonCategory###field:::`$a.category`"|base64_encode}" {if $cat.categoryid eq $a.categoryid}selected{/if} class="removable" data-val="asas">{$a.category}</option>
{/foreach}
</select>
*}
</li>
</ul>
<div class="seperator"></div>
<ul>
<li><h4>{$lang.category}</h4></li>
<li><select name="categoryid" id="category" class="mSelect" >
<option value="0">{$lang.lbl_allCategories}</option>
{foreach from=$more_categories item=a}
<option value="{"categoryid:::`$a.categoryid`###type:::category"|base64_encode}" data-type="sdsd" {if $cat.categoryid eq $a.categoryid}selected{/if}>{$a.category}</option>
{/foreach}
</select></li>
</ul>
<div class="seperator"></div>
<div id="filters">
{foreach from=$efields item=a name=b key=k}

<ul>
<li><h4>{$a.title}</h4></li>
{foreach from=$a.data item=b}
<li>
{if $b.type eq "radio"}
<label><input type="checkbox" name="filters[]" value="{"type:::efield###value:::`$b.values[0].value`###fieldid:::`$b.fieldid`###field:::`$b.field`"|base64_encode}" class="efieldFilter" {if $efieldFilters["`$b.fieldid`-`$b.values[0].value`"|md5]}checked{/if} id="{"`$b.fieldid`-`$b.values[0].value`"|md5}" {if !$b.values[0].total}disabled="disabled"{/if} /> 
{$b.field} (<span id="total-{"`$b.fieldid`-`$b.values[0].value`"|md5}">{$b.values[0].total}</span>)</label>
{else}
<div id="multiSelect-methods-{"`$b.fieldid`-`$x.value`"|md5}" class="multiSelect" data-id='{"`$b.fieldid`-`$x.value`"|md5}'><div id="multiSelect-methods-title" class="title" title="{$b.field}"  style="width:100%">{$b.field}</div></div>
<div id="multiSelect-methods-content-{"`$b.fieldid`-`$x.value`"|md5}"  class="multiSelectContent hidden">
<ul>
{foreach from=$b.values item=x}
<li><label><input type="checkbox" name="filters[]" value="{"type:::efield###value:::`$x.value`###fieldid:::`$b.fieldid`###field:::`$b.field`"|base64_encode}" class="efieldFilter" {if $efieldFilters["`$b.fieldid`-`$x.value`"|md5]}checked{/if} id="{"`$b.fieldid`-`$x.value`"|md5}" {if !$x.total}disabled="disabled"{/if} /> 
  <span id="total-{"`$b.fieldid`-`$x.value`"|md5}">{$x.value} ({$x.total})</span></label>
  </li>
{/foreach}
</ul>

</div>
<div class="seperator"></div>
{/if}
</li>
{/foreach}

</ul>
<div class="seperator"></div>
{/foreach}
</div>