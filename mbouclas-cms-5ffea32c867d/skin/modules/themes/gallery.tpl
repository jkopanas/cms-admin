<div id="GalleryWindow"></div>
<div class="page-title category-title">
<h1>{$cat.category}</h1>
</div>
<div class="seperator"></div>
<ul class="products-grid first odd">

{foreach from=$items  item=c name=b}

	{foreach from=$c.detailed_images.images key=i item=a name=d}
            <li class="item {if $smarty.foreach.d.first}first{elseif $smarty.foreach.d.last}last{/if}" style="margin-left:23px;">
			  <div class="imagecontainers imagecontainers2" id="image_{$a.id}" data-next="{$c.detailed_images.images[$i+1].id}" data-prev="{$c.detailed_images.images[$i-1].id}" data-overlayid="overlayid-1" data-overlayid2="overlayid2-1" style="width: 190px; height: 80px; position: relative; overflow-x: hidden; overflow-y: hidden; ">														
                <a href="{$a.image_full.main.image_url}" title="{$a.title}" id="light{$a.id}" class="popup product-image"><img class="product_image" src="{$a.image_full.thumb.image_url}" width="155" height="155" alt="{$a.title}" title="{$a.title}"></a></div>
                <h2 class="product-name"><a href="/{$FRONT_LANG}/pages/{$a.id}/{$a.permalink}.html" title="{$a.title}">{$a.title}</a></h2>
                                                                
                <div class="actions">                    
              </div>
            </li>
	{/foreach}
{/foreach}
</ul>
<div class="seperator"></div>
<script>
head.js('/scripts/site/gallery.js');
</script>