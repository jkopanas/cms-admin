<div class="page-title category-title">
<h1>{$item.title}</h1>
</div>
<div class="seperator"></div>
<div id="slider" class="camera_wrap camera_azure_skin">
{foreach from=$item.detailed_images.images item=a key=k}
	<div data-thumb="{$a.image_full.thumb.image_url}" data-src="{$a.image_full.slider.image_url}">
           <div class="camera_caption fadeFromLeft">
           {$a.title}
           </div>
        </div><!-- END SLIDE -->
{/foreach}
</div><!-- END SLIDER -->
<script>
head.js("/scripts/slider/mcmsSlider.min.js");
head.js("/scripts/slider/jquery.easing.1.3.js");
head.js("/scripts/site/welcome.js");
</script>