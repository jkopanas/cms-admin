{*
<input type="hidden" name="posted_data" class="post" value="{$posted_data|json_encode|base64_encode}" />
<input type="hidden" name="module" class="post" value="{$current_module.name}" />
<input type="hidden" name="prefix" class="post" value="{$prefix}" />
<input type="hidden" name="filter"  value="{$filter}" />
<ol class="products-list" id="products-list">
{foreach from=$items item=a name=b}
<li class="item">
<a href="/{$FRONT_LANG}/product/{$a.id}/{$a.permalink}.html" title="{$a.title}" class="product-image"><img src="{$a.image_full.big_thumb}" alt="{$a.title}" width="155" height="155" class="itemImg" title="{$a.title}" /></a>
<div class="product-shop">
<div class="f-fix">
<h2 class="product-name"><a href="/{$FRONT_LANG}/products/{$a.id}/{$a.permalink}.html" title="{$a.title}">{$a.title}</a></h2>
<div class="price-box">
<span class="regular-price" id="product-price-15"><span class="price"><strong>&euro;{$a.eshop.price|formatprice}</strong></span></span>
                        
        </div>
<p><button type="button" title="Add to Cart" class="button btn-cart addToCart" rel="{$a.id}"><span><span>Add to Cart</span></span></button></p>
<div>
 {$a.description}
</div>
                </div>
            </div>
            <div class="clear"></div>
        </li><!-- END ITEM -->
{/foreach}
</ol>
*}
{if $smarty.post.view != "list"}
{foreach from=$items item=a name=b}
		<li class="item" style="margin-left:14px;">			
			<div class="imagecontainers imagecontainers2" data-overlayid="overlayid-1" data-overlayid2="overlayid2-1" style="width: 190px; height: 180px; position: relative; overflow: hidden; ">														
			<a href="/{$FRONT_LANG}/product/{$a.id}/{$a.permalink}.html" title="{$a.title}" class="product-image"><img src="{if $a.image_full != ''}{$a.image_full.big_thumb}{/if}" alt="{$a.title}" width="175" height="175" class="product_image" title="{$a.title}" /></a>
			</div>
                <h2 class="product-name"><a href="/{$FRONT_LANG}/product/{$a.id}/{$a.permalink}.html" title="{$a.title}">{$a.title}</a></h2>
                <div class="hidden">
						<div class="product-price">
								<div class="price-box">
									<span class="regular-price" id="product-price-1">
									<span class="price">&euro;&nbsp;{if $a.eshop.price != ""}{$a.eshop.price}{else}0{/if}</span></span>
								</div>
						</div>
				<div class="cartButton">		
					     <button type="button" title="Add to Cart" class="button btn-cart addToCart" rel="{$a.id}"><span><span>Add to Cart</span></span></button>
				</div>
				</div>                                             
        </li>
{/foreach}
{else}
{foreach from=$items item=a name=b}
<li class="item odd">
	<a href="/{$FRONT_LANG}/product/{$a.id}/{$a.permalink}.html" title="{$a.title}" class="product-image"><img src="{if $a.image_full != ''}{$a.image_full.big_thumb}{/if}" alt="{$a.title}" width="135" height="135" class="product_image" title="{$a.title}" /></a>
	<div class="product-shop">
	<div class="f-fix">
		<h2 class="product-name"><a href="/{$FRONT_LANG}/product/{$a.id}/{$a.permalink}.html" title="{$a.title}" >{$a.title}</a></h2>
		<div class="price-box">
			<span class="regular-price" id="product-price-{$a.id}"><span class="price">&euro;&nbsp;{if $a.eshop.price != ""}{$a.eshop.price}{else}0{/if}</span></span>            
        </div>
		<p><button type="button" title="Add to Cart" class="button btn-cart addToCart" rel="{$a.id}"><span><span>Add to Cart</span></span></button></p>
		<div class="desc std">
 			{$a.description}
		</div>
  </div>
</div>
<div class="clear"></div>
</li>
{/foreach}
{/if}