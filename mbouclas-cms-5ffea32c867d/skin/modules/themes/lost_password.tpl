<div id="container">
<div id="content_full">
<div id="list-categories">
<div class="section-title"><h1>Επανάκτηση Κωδικού</h1></div>
<br class="seperator" />
<div id="login">
<div class="login-form">
{if $message}
Έγινε αποστολή των στοιχείων πρόσβασης στο email που δώσατε.
{else}
<form id="login_form" name="login_form" method="post" action="{$SELF}">
      <table style="font-weight: bold;" width="100%">
      <tbody><tr>
        <td style="height: 40px;" valign="middle">{$lang.email} (Το email που χρησιμοποιήσατε κατά την εγγραφή σας στο site) :</td>

        <td style="height: 40px;" valign="middle"><input name="email" id="email"  type="text"></td>
      </tr>
      <tr>
        <td colspan="2" style="height: 40px;" align="center" valign="middle"><input name="Submit" value="{$lang.send}" type="submit" class="submit">

          <input type="hidden" name="return_to" value="{$e_REQUEST_URI}" />
          </td>
      </tr>
    </tbody></table>  
</form>
{/if}
<div class="clear"></div>
</div><!-- END LOGIN FORM -->

<div class="clear"></div>
</div><!-- END LOGIN -->
<div class="clear"></div>
</div>
</div><!-- END CONTENT FULL -->
<div class="clear"></div>
</div><!-- END CONTAINER -->
lost_password