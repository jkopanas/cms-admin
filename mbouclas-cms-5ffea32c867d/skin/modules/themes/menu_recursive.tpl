{foreach name=entry item=entry from=$menu}
  		<li class="menu-{$entry.permalink}{if $entry.type eq 'cat' AND $entry.itemid eq $cat.categoryid}{$class}{elseif $entry.type eq 'item' AND $entry.itemid eq $item.id}{$class}{elseif $entry.type eq 'cat' AND $entry.itemid eq $cat.parentid}{$class}{/if}"><a href="{if $entry.link != "#"}/{$FRONT_LANG}/{$entry.link}.html{else}#{/if}" title="{$entry.settings.title|default:$entry.title}"><span>{$entry.title}</span></a>
          {if $entry.subs}
        <ul>{include file="modules/themes/menu_recursive.tpl" menu=$entry.subs}</ul>
        {/if}
       </li>
{/foreach} 

