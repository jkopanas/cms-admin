
<div class="breadcrumbs spacer-bottom">
<ul>
	<li class="home"><a href="/{$FRONT_LANG}/index.html" title="{$lang.home}">{$lang.home}</a></li>
	<li><span class="bullet">&raquo;</span></li>
	{foreach from=$nav item=a name=b}
	<li>{if $cat.categoryid eq $a.categoryid AND !$item.id}<strong>{$a.category}</strong>{else}<a href="/{$FRONT_LANG}/{$prefix|default:$current_module.name}/{$a.alias}/index.html" title="{$a.category}">{$a.category}</a>{/if}</li>
    {if !$smarty.foreach.b.last}
    <li><span class="bullet">&raquo;</span></li>
    {/if}
    <li class=""><strong></strong></li>
    {/foreach}
    {if $item.id}
    <li><span class="bullet">&raquo;</span></li>
    <li><strong>{$item.title}</strong></li>{/if}
</ul>
</div>
<div class="seperator spacer-bottom"></div>