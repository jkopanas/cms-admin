<div class="pager">
<p class="amount">Items {$list.from} to {$list.to} of {$list.total} total</p>  
<div class="pages">
<strong>Page:</strong>
<ol>
{foreach item=a from=$num_links} 
<li {if $a.page eq $PAGE}class="current"{/if}>{if $a.page eq $PAGE}{$a.page}{else}<a href="{if $ajax}#{else}/{$FRONT_LANG}/{$prefix|default:$current_module.name}/{if $cat}{$cat.alias}/{/if}{$a.page}.html{/if}" title="{$a.category}" class="inactive {if $ajax}paginateFilter{/if}" rel="{$a.page}">{$a.page}</a>{/if}</li>
{/foreach}                                      
</ol>
<div class="clear"></div>
</div><!-- END PAGES -->
</div><!-- END PAGER -->