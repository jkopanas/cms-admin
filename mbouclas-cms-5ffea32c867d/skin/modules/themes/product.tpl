<div class="seperator"></div>
<div class="product-view spacer-top">
    <div class="product-essential">
    <div class="product-img-box">
            <div class="product-info-images">
<p class="product-image">
	<a href='../../media/catalog/product/cache/2/image/800x800/9df78eab33525d08d6e5fb8d27136e95/a/1/a1_4.jpg' class = 'cloud-zoom' id='zoom1' rel="position:'right',showTitle:1,titleOpacity:0.5,lensOpacity:0.5,adjustX: 10, adjustY:-4">
		<img src="{$item.image_full.big_thumb}" alt='' title="Caterpillar Shoes" />
	</a>
</p>
</div>

        </div><!-- END IMAGE -->
    
<div class="product-shop">

<div class="product-name">
    <h1>{$item.title}</h1>
</div>	
<div class="clear"></div>
<div class="product-left-area">
<div class="short-description">
    <div class="std">{$item.description}</div>
</div><!-- END DESCRIPTION -->
<div class="product-infobg">
<div class="price-box">
<span class="regular-price" id="product-price-32">
<span class="price">&euro;{$item.eshop.price|number_format:2:".":","}</span>                </span>
<div class="seperator"></div>
<p><button type="button" title="Add to Cart" class="button btn-cart addToCart" rel="{$item.id}"><span><span>Add to Cart</span></span></button></p>
</div><!-- END PRICE -->
</div><!-- END INFOBG -->
</div><!-- END LEFT -->
<div class="product-right-area">
<div class="block block-related">
    <div class="block-title">
        <strong><span>Related Products</span></strong>
    </div>
    <div class="block-content">

        <ol class="mini-products-list" id="block-related">
        {foreach from=$more_items item=a}
                    <li class="item">
                                                                    <div class="product">
                    <a href="/{$FRONT_LANG}/product/{$a.id}/{$a.permalink}.html" title="{$a.title}" class="product-image"><img src="{$a.image_full.thumb}" width="50" height="50" alt="{$a.title}" /></a>
                    <div class="product-details">
                        <p class="product-name"><a href="/{$FRONT_LANG}/product/{$a.id}/{$a.permalink}.html" title="{$a.title}">{$a.title}</a></p>
                        

        
    <div class="price-box">
                                                            <span class="regular-price" id="product-price-20-related">
                    <span class="price">&euro;{$a.eshop.price|formatprice}</span>                </span>
                        
        </div>

                                            </div>
                </div>
            </li>
                    {/foreach}
                </ol>
</div>
</div><!--END RIGHT -->
<div class="clear"></div>
</div><!-- END PRODUCT -->
    
    </div><!-- END ESSENTIAL -->
    <div class="seperator"></div>
    <div class="product-collateral">
		<div class="product-collateral-left">
        	
<ul class="product-tabs">
                        <li class=" active first"><a href="#" rel="productDescription">Product Description</a></li>
                        <li class=""><a href="#" rel="moreImages">More images</a></li>
                       
            </ul>
            <div class="product-tabs-content hidden" id="productDescription">

			<div class="border-left">
				<div class="border-right">
					<div class="tabs-content">	
						    <h2>Details</h2>
    <div class="std">
            {$item.description_long}     </div>
					</div>
				</div>
			</div>	
			
			
			</div><!-- END TAB -->
            <div class="product-tabs-content hidden" id="moreImages">
            <div class="more-views">
    <h2>More Views</h2>
    <ul>
    {foreach from=$item.detailed_images item=b key=k}
	{if $k ne "thumbs"}
    {foreach from=$b item=a}
            <li>
        	<a href='../../media/catalog/product/cache/2/image/800x800/9df78eab33525d08d6e5fb8d27136e95/a/1/a1_4.jpg' class='cloud-zoom-gallery' title=''
        	rel="useZoom: 'zoom1', smallImage: 'http://demo.dazmetech.com/mage3/media/catalog/product/cache/2/image/250x300/9df78eab33525d08d6e5fb8d27136e95/a/1/a1_4.jpg' ">
        	<img src="{$a.thumb}" alt=""/>
        	</a>
        </li>
     {/foreach}
	{/if}
    {/foreach}
        </ul>
        
</div>
<div class="clear"></div>
            </div><!-- END TAB -->
<div class="product-tabs-content hidden" id="moreProducts">

sdsd

</div><!-- END TAB -->

    	    		</div>

    </div>
</div><!-- END PRODUCT -->

<div class="clear"></div>


<div class="debug"></div>
