{capture name=sideBar}
<div class="block side-nav-categories" id="categories_block_left">
<div class="block-title">
<strong>
<span>
Categories
</span>
</strong>
</div>
 <div class="block-content">
<ul id="verticalcollapse">

{assign var="parent" value="/"|explode:$cat['categoryid_path']}

 {foreach from=$more_categories item=a}
  {if $parent[0] != $a.categoryid} {assign var="class"  value="hidden"}{else}{assign var="class"  value=""}{/if}
    <li class="level0 nav-1 parent  {if $parent[0] == $a.categoryid}active selected{/if}">
                <a href="/{$FRONT_LANG}/products/{$a.alias}/1.html"><span>{$a.category} ({$a.num_items})</span></a>
                <span class="head"><a href="#"></a></span>
                {include file="modules/themes/sideBarCat.tpl" class=$class active=$active current=$cat['categoryid'] sub=$a.subcategories}
    </li>
    {/foreach}
        </ul>
 </div>
 
</div>



 {/capture}{include file="modules/themes/sideBar.tpl" classes="spacer-right" content=$smarty.capture.sideBar}
{capture name=mainCol}

<div class="page-title category-title">
        <h1>{$cat.category}</h1>
</div>
 <script id="template-mode-theme" type="text/x-kendo-template">
<label>View as:</label>
# if ( mode == "grid" ) { #
	<strong title="Grid" data-mode="" class="mode grid">Grid</strong>
	<a href="\\#" title="List" data-mode="list" class="mode list">List</a>
# } else { # 
	<a href="\\#" title="List" data-mode="grid" class="mode grid">List</a>
	<strong title="Grid" data-mode="" class="mode list">Grid</strong>
# } # 
</script>
<input type="hidden" id="categoryid" name="categoryid" value="{$cat.categoryid}" />
<div class="category-products">
{include file="modules/themes/toolBar.tpl"}
<div class="seperator"></div>
<ul class="products-{if $smarty.post.view==''}grid{else}{$smarty.post.view}{/if}" id="products-list">
 {include file="modules/themes/items.tpl" prefix="product" view=$smarty.get.view navPrefix=$prefix}
</ul>
<div class="seperator"></div>
{include file="modules/themes/toolBar.tpl"}
 </div>
{/capture}{include file="modules/themes/mainCol.tpl" classes="spacer-left" content=$smarty.capture.mainCol}
<div class="seperator"></div>
<script>
head.js('/scripts/site/ProductItem.js');
</script>