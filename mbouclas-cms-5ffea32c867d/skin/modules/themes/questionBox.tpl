<div class="container"><ol></ol></div>

    <form id="contactform" class="formular" method="post">
        <fieldset>
            <input type="text" name="name" id="name" alt="{$lang.full_name}" class="required get text-input" value="{$lang.full_name}" />
            
            <input type="text" name="email" id="email" alt="{$lang.email}" class="required email get text-input" value="{$lang.email}" />
            
            <textarea class="required get text-input" name="message" id="message" rows="5" cols="10"></textarea>
            
            <input type="hidden" id="ctyouremail" name="ctyouremail" value="chris@contempographicdesign.com" />
            <input type="hidden" id="ctproperty" name="ctproperty" value="5301 Willows Rd, Alpine, CA 91901" />
            <input type="hidden" id="ctpermalink" name="ctpermalink" value="indexd365.html?post_type=listings&amp;p=741" />
            <input type="hidden" id="ctsubject" name="ctsubject" value="WP Pro Real Estate 2 - Demo Inquiry" />
            
            <input type="submit" name="Submit" value="{$lang.send}" id="submit" class="btn send-question" />  
        </fieldset>
    </form>