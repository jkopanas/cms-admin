{foreach name=b item=a from=$categories}
{if $a.num_items}
    <li {if $a.subs}class="p{$level} down"{/if}>
        {if $a.subs}{$a.category} ( {$a.num_items} )
        <ul>{include file="modules/themes/recurseCategories.tpl" categories=$a.subs level=$level+1}</ul>
        {else}
        <a  href="#" class="applyFilter" rel="{"tableID:::`$tableID`###categoryid:::`$a.categoryid`###type:::commonCategory###field:::`$a.category`"|base64_encode}" id="{"`$tableID`-`$a.categoryid`"|md5}">{$a.category} ( {$a.num_items} )</a>
        {/if}
       </li>
{/if}
{/foreach} 