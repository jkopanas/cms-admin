<textarea name="fomatedItemsRaw" class="hidden">{$fomatedItemsRaw}</textarea>
{capture name=sideBar}
<div class="block block-layered-nav">
                    <div class="block-title">
        <strong><span>{$cat.category}</span></strong>
    </div>
  <div class="block-content">
{foreach from=$items item=a}
    <p class="block-subtitle"><a href="/{$FRONT_LANG}/pages/{$a.id}/{$a.permalink}.html" title="{$a.title}">{$a.title}</a></p>
                            
    <dl id="narrow-by-list">
      {$a.description}

    </dl>
{/foreach}
  </div>
</div>
{/capture}{include file="modules/themes/sideBar.tpl" classes="spacer-right" content=$smarty.capture.sideBar}
{capture name=mainCol}

<div id="map" style="width:100%; height:350px;"></div>
<script id="markerTemplate" type="text/x-kendo-template">
<div>
<h1><a href="/#= mcms.settings.lang #/pages/#= item.id #/#= item.permalink #.html">#= item.title #</a></h1>
<p>#= item.description #</p>
</div>
</script>
{/capture}{include file="modules/themes/mainCol.tpl" classes="spacer-left" content=$smarty.capture.mainCol}

</script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
<script>
head.js("/scripts/site/maps/gMaps3.js");
head.js("/scripts/site/shops.js");
</script>