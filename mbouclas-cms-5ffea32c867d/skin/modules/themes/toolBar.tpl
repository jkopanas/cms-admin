<form name="toolbarSearch" id="toolbarSearch" method="POST" action="#">
<div class="toolbar">
{include file="modules/themes/pagination.tpl" prefix=$navPrefix ajax=$ajax}
<div class="sorter">
<p class="view-mode">
                                    <label>View as:</label>
                                                <strong title="Grid" class="grid mode" data-value="grid">Grid</strong>
                                                                <a href="#" title="List" class="list mode" data-value="list">List</a>
                                                </p>
<div class="limiter">
<label>Show</label>
<select name="results_per_page" class="takeItem">
<option value="12" {if $smarty.post.results_per_page == '12'} selected{/if}>12</option>
<option value="24" {if $smarty.post.results_per_page == '24'} selected{/if}>24</option>
<option value="36" {if $smarty.post.results_per_page == '36'} selected{/if} >36</option>
</select> per page        
</div>
<div class="sort-by">
<label>Sort By</label>
<select name="sort" class="SortBy">
<option value="id" {if $smarty.post.sort == 'id'} selected{/if}>Position</option>
<option value="title" {if $smarty.post.sort == 'title'} selected{/if}>Name</option>
<option value="price" {if $smarty.post.sort == 'price'} selected{/if}>Price</option>
</select>
<a href="#"  class="direction" data-direction="{if $smarty.post.sort_direction == ''}DESC{else}{if $smarty.post.sort_direction == 'DESC'}ASC{else}DESC{/if}{/if}" title="Set Descending Direction"><img src="/images/site/i_asc_arrow.gif" alt="Set Descending Direction" class="v-middle" /></a>
</div>
<div class="clear"></div>
</div><!-- END SORTER -->
</div>
<input type="hidden" name="sort_direction" value="{if $smarty.post.sort_direction == ''}DESC{else}{if $smarty.post.sort_direction == 'DESC'}ASC{else}DESC{/if}{/if}"/>
<input type="hidden" name="view" value="{if $smarty.post.view==''}grid{else}{$smarty.post.view}{/if}"/>
</form>