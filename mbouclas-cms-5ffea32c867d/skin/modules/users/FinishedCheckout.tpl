{if !$items}
Δεν έχετε προϊόντα στο καλάθι σας.
{else}

<ul id="mainNav" class="threeStep">
		<li id="first" class="done"><a title="Βήμα 1: Καλάθι αγορών" href="/cart.html"><em>Βήμα 1: Καλάθι αγορών</em><span>δείτε συνοπτικά την παραγγελία σας</span></a></li>
		<li id="second" class="lastDone"><a  title="Βήμα 2: Καλάθι αγορών" href="/{$FRONT_LANG}/checkout.html"><em>Βήμα 2: Πληρωμή</em><span>Επιλογή τρόπου πληρωμής</span></a></li>
        <li id="third" class="current"><a title=""><em>Βήμα 3: Ολοκλήρωση</em><span>Τέλος παραγγελίας</span></a></li>
	</ul>

<div class="clear seperator"></div>
<h1>Η παραγγελία σας</h1>

   	<table id="shopping-cart-table" class="data-table cart-table">
                <colgroup>
                	<col width="1">
                	<col>
                	<col width="1">
                	<col width="1">
                	<col width="1">
                	<col width="1">
                	<col width="1">
				</colgroup>
				<thead>
                    <tr class="first last">
                        <th rowspan="1">&nbsp;</th>
                        <th rowspan="1"><span class="nobr">Product Name</span></th>
                  		<th class="a-center" colspan="1"><span class="nobr">Unit Price</span></th>
                        <th rowspan="1" class="a-center">Qty</th>
                        <th class="a-center" colspan="1">Subtotal</th>
                        <th rowspan="1" class="a-center">&nbsp;</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr class="first last">
                        <td colspan="50" class="a-right last"></td>
                    </tr>
                </tfoot>
                <tbody>
                  {foreach from=$items item=a key=k name=b}
  					<tr class="{if $smarty.foreach.b.first}first{elseif $smarty.foreach.b.last}last{/if} odd">
    					<td><a href="/product/{$k}/{$a.permalink}.html" title="{$a.title}" class="product-image"><img src="{$a.image_full.thumb}" width="75" height="75" alt="{$a.title}"></a></td>
    					<td><h2><a href="/product/{$k}/{$a.permalink}.html">{$a.title}</a></h2></td>				
    					<td class="a-right">&euro; <span class="cart-price" id="total-{$k}"><span class="price">{$a.total|number_format:2:".":","}</span></span></td>
    					<td class="a-center"><input type="text" name="quantity-{$k}" id="quantity-{$k}" value="{$a.quantity|default:0}"  size="4" title="Qty" class="input-text qty" maxlength="12" alt="{$k}|{$a.price}"  disabled /></td>
     					<td class="a-right"><span class="cart-price"><span class="price"> &euro; {$a.price|number_format:2:".":","}</span></span></td>
  					</tr>
 				  {/foreach}
                </tbody>
            </table>
            
    <div class="seperator"></div>

    <div class="cart cart-collaterals float-right">
        <div class="totals">
                <table id="shopping-cart-totals-table">
        <colgroup><col>
        <col width="1">
        </colgroup><tfoot>
            <tr>
    <td style="" class="a-right" colspan="1">
        <strong>Γενικό Σύνολο :</strong>
    </td>
    <td style="" class="a-right">
        <strong><span class="price">{$total_price|number_format:2:".":","}</span></strong>
    </td>
</tr>
<tr><td colspan="2">{$button_pay}</td></tr>
        </tfoot>
        <tbody>
            <tr>
    <td style="" class="a-right" colspan="1">
        Μερικό Σύνολο :    </td>
    <td style="" class="a-right">
        <span class="price">{$total_price_no_vat|number_format:2:".":","}</span></td>
</tr>
<tr>
    <td style="" class="a-right" colspan="1">
        Αξία Φ.Π.Α :    </td>
    <td style="" class="a-right">
        <span class="price">{$vat_price|number_format:2:".":","}</span>    </td>
</tr>
        </tbody>
    </table>

</div>
</div>

{if !$button_pay}
<div class="float-left">
 <a href="#" title="Continue Shopping" class="button btn-continue float-left" id="Finished" ><span><span>Τέλος Παραγγελείας</span></span></a>
</div>
{/if}
								
{/if}
