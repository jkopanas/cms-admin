<div class="Comments">
<div class="commentForm">
{$lang.addacomment}
<form id="commentsForm">
    <textarea name="comment" cols="30" rows="5" class="CommentData"></textarea>
    <br />
    <input type="button" class="submitComment" value="{$lang.save}" />
    <input type="hidden" class="CommentData" name="itemid" value="{$commentitemid}" />
    <input type="hidden" class="CommentData" name="module" value="{$commentmodule}" />
    <input type="hidden" class="CommentData" name="uid" value="{$ID}" />
    <input type="hidden" class="CommentData" name="type" value="{$commenttype}" />
    <input type="hidden" class="CommentData" name="status" value="{$commentstatus}" />
</form>

<script type="text/javascript" src="/scripts/userComments.js"></script>
</div>
<div class=commentsHolder>
<ul>
	{if $comments}
    {foreach from=$comments item=a name=list}
    <li>
    <ul>
    	{if $a.userDetails.id==$ID}
        <li>
        {$lang.isaid}:
        </li>
        {else}
        <li>{$a.userDetails.uname}:</li>
        {/if}
        <li>
        <div class="hideComment">
        {$a.comment}
        </div>
        <div class="editComment hidden">
        <form>
            <textarea name="comment" cols="30" rows="2" class="CommentData">{$a.comment}</textarea>
            <br />
            <input type="button" class="updateComment" value="{$lang.update}" />
            <input type="hidden" class="CommentData" name="id" value="{$a.id}" />
        </form>
        </div>
        </li>
        <li>{$a.date_added|date_format:"%d/%m/%Y @ %H:%M"}</li>
        {if $a.userDetails.id==$ID}
        <li><a href="#" class="editThisComment">edit Comment</a> </li>
        {/if}
        {if $a.itemid==$ID}
        <li><a href="#" class="deleteThisComment" rel="{$a.id}">delete Comment</a> </li>
        {/if}
    </ul>
    </li>
    {/foreach}
    {else}
    	{$lang.nocomments}
	{/if}
    </ul>
    
    <div>
    {if $list.total gt $comments|@count}
    {include file="modules/content/pagination.tpl" mode="ajax" class="commentsPage" target="commentsForm"}
    {/if}
    </div>
</div>
</div>
