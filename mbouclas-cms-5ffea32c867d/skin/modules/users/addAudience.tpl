<link href="../../../css/userCp/style.css" rel="stylesheet" type="text/css" />
               
<section class="main-section grid_8">
  <div class="main-content grid_8 alpha">
                        <header>
                                <h2>{$lang.add}</h2>
                        </header>
<section>
<form id="UploadForm">
<input type="file" name="uploadify" id="uploadify"  />
<p>Seperator : <select name="delimiter" class="csvSettings">
<option value=";">;</option>
<option value=",">,</option>
<option value="\t">tab</option>
<option value=" ">space</option>
</select></p>
<p>Enclosure : <select name="enclose" class="csvSettings">
<option value='"'>"</option>
<option value="'">'</option>
</select></p>
<p>First row is a header : <input type="checkbox" name="firstRowIsHeader" class="csvSettings" /></p>

<div id="fileQueue"></div>
<p><button class="button button-blue Upload">{$lang.upload_file}</button></p>
</form>
<ul class="msg hidden"></ul>
<div class="messageArea"></div> 
</section>
  </div><!-- END MAIN --></section>
<link rel="stylesheet" type="text/css" href="/css/uploadify.css" />                        
<script type="text/javascript" src="/scripts/uploadify/swfobject.js"></script>
<script type="text/javascript" src="/scripts/uploadify/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="/scripts/uploadFiles.js"></script>