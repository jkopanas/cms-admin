<div class="sepH_b" data-bind="attr: { class: changeClass }">
       		<label for="id" class="lbl_a">{$lang.userid}:</label>
        	<input name="id"  type="text"  value="" data-mode="eq" id="user_id"  class="filterdata inpt_a"  />
        </div>
        <div class="sepH_b" data-bind="attr: { class: changeClass }">
        	<label for="title" class="lbl_a">{$lang.username} :</label>
        	<input name="uname" id="username"  data-mode='contains' type="text"  value=""  class="filterdata inpt_a" />
        </div>
        <div class="sepH_b" data-bind="attr: { class: changeClass }">
        	<label for="title" class="lbl_a">{$lang.user_name} :</label>
        	<input name="user_name" id="user_name"  data-mode='contains' type="text"  value=""  class="filterdata inpt_a" />
        </div>
        <div class="sepH_b" data-bind="attr: { class: changeClass }">
        	<label for="title" class="lbl_a">{$lang.surname} :</label>
        	<input name="user_surname" id="user_surname"  data-mode='contains' type="text"  value=""  class="filterdata inpt_a" />
        </div>
        <div class="sepH_b" data-bind="attr: { class: changeClass }">
        	<label for="email" class="lbl_a">{$lang.email} :</label>
        	<input name="email" id="e-mail"  type="text"  value="" data-mode='contains' class="filterdata inpt_a" />
        </div>
        <div class="sepH_b" data-bind="attr: { class: changeClass }">
        <label for="available" class="lbl_a">{$lang.availability} : </label>
        	<select name="user_login" id="active-{$ParentSettings}" data-mode='eq' class="active filterdata inpt_d"  data-placeholder="Select one">
        		<option value="" >&nbsp;</option>
        		<option value="0" >{$lang.disable}</option>
        		<option value="1" >{$lang.enable}</option>
      		</select> 
        </div>
        <div class="sepH_b" data-bind="attr: { class: changeClass }">
        	<label for="date" class="lbl_a float-left">{$lang.subscr_date} : </label>
        	<input type="hidden" name="user_join[]" data-mode="between" id="joinfrom" class="filterdata" value="" />
        	<input type="hidden" name="user_join[]" data-mode="between" id="jointo" class="filterdata" value="" />
        	<div class="calendar float-left spacer-right clear" data-trigger="joinfrom"></div>	
     	  	<div class="calendar float-left" data-trigger="jointo"></div>
     	  	<div class="clear"></div>
        </div>
        
        <input type="hidden" class="submitSearch" name="submitSearch" data-table="users" value="filterdata" />
        
<style scoped>

.k-calendar {
	width:183px;
}
</style>

<script type="text/javascript" src="/scripts/admin2/users/userfilter.js"></script>
          