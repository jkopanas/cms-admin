{if $group OR $newGroup}
<div class="wrap seperator padding">
<h2><a href="extra_fields.php">{$lang.extra_fields}</a> ({if $group}{$group.title}{/if}{if $newGroup}{$lang.newgroup}{/if})</h2>
<form action="{$PHP_SELF}" method="POST" name="upsales">
<div id="debugArea"></div>

<table width="730" cellpadding="5" cellspacing="5">
<tr>
<td width="250">{$lang.title} :</td>
<td><input type="text" name="title" value="{$group.title}" class="SaveData" /></td>
</tr>
<tr>
<td width="250">{$lang.description} :</td>
<td><input type="text" name="description" value="{$group.description}" class="SaveData" /></td>
</tr>
<tr>
<td>{$lang.extra_fields}:</td>
<td>
<table class="DragDrop">
<tr><td colspan="3"></tr>
{foreach from=$efields item=a}
    <tr id="{$group.id}_{$a.fieldid}">
        <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
        <td><input type="checkbox" name="items_{$a.fieldid}" value="{$a.fieldid}" {if $group}{if $a.fieldid|array_key_exists:$groupitems}CHECKED{/if}{/if} class="SaveData"  /></td>
        <td>{$a.field}</td>
    </tr>
{/foreach}
</table>
</td>
</tr>
<tr>
<td>Themes:</td>
<td>
<table>
{foreach from=$themes item=a}
<tr>
<td><input type="checkbox" class="SaveData" name="theme_{$a.id}" value="{$a.id}" {$a.checked}  /></td>
<td>{$a.title}</td>
</tr>
{/foreach}
</table>
</td>
</tr>
<tr>
  <td colspan="2" align="center"><input type="button" name="SaveEfield" class="button" value="Αποθήκευση" /><input type="hidden" name="groupid" value="{$group.id}" class="SaveData" /> <input type="hidden" name="mode" value="SaveEfieldGroup" /><input type="hidden" value="SaveGroupsItemsOrder" name="action" /></td>
</tr>
</table>
</form>
</div>

{/if}

{if $shown}
<div class="wrap seperator padding">
<h2><a href="extra_fields.php">{$lang.extra_fields}</a> (<a href="extra_fields.php?newGroup=1">Προσθήκη νέου group</a>)</h2>
<form action="{$PHP_SELF}" method="POST" name="upsales">
<div id="debugArea"></div>

<table border="0" cellpadding="5" cellspacing="5" width="100%" class="DragDrop">
        <tr class="nodrop nodrag">
        <td colspan="5">Με τα επιλεγμένα (<span id="messages">0 επιλέχθηκαν</span> ) : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="DeleteCheckedGroups">Διαγραφή</option>
        </select>
        <input type="button" class="button" name="SaveEfield" value="Εκτέλεση" />
        </td>
      </tr>
    <tr>
      <th scope="cols"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
	  <th scope="col" width="16"><input type="hidden" value="SaveEfieldsGroupsOrder" name="action" /></th>
      <th width="250" scope="cols">{$lang.field_name}</th>
      <th scope="cols">{$lang.description}</th>
      <th scope="cols">{$lang.numberOfElements}</th>
    </tr>
    {foreach from=$groups item=a}
    <tr {cycle values=", class='TableSubHead'"} id="group_{$a.id}">
      <td width="2%"><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /></td>
      <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
      <td><a href="extra_fields.php?groupid={$a.id}">{$a.title}</a></td>
      <td>{$a.description}</td>
      <td>{$a.count}</td>
      
      </tr>
    {/foreach}
    
    </table>
 </form>

</div>
{/if}