{include file="modules/users/admin/menu.tpl"}
<div class="seperator padding wrap">
<h2><a href="javascript:void(0)" class="ShowSearchTable Slide" rel="#SearchTable">Αναζήτηση</a> ( <img src="/images/admin/add.gif" width="18" height="18" align="absmiddle" /> <a href="#" class="addUser">{$lang.addUser}</a> )</h2>
<div class="debug"></div>

<form name="searchform" id="searchUsersForm"  method="POST">
<table width="100%" border=0 cellpadding="5" cellspacing="5" id="SearchTable">

<TR>
  <TD nowrap class=FormButton>{$lang.results_per_page}</TD>
  <TD width="933"><select name="results_per_page" id="results_per_page" class="secondarySearchData">
    <option value="20" {if $data.results_per_page eq 20}selected{/if}>20</option>
    <option value="60" {if $data.results_per_page eq 60}selected{/if}>60</option>
    <option value="100" {if $data.results_per_page eq 100}selected{/if}>100</option>
  </select>  </TD>
</TR>
<TR>
  <TD nowrap class=FormButton>{$lang.order_by}</TD>
  <TD> <select name="orderby" class="secondarySearchData">
   <option value="user_name">{$lang.user_name}</option>
   <option value="user_surname">{$lang.surname}</option>
   <option value="uname">{$lang.uname}</option>
   <option value="id">#ID</option> 
</select>  
{$lang.way} : 
<select name="way" id="sort_direction" class="secondarySearchData">
<option value="desc">{$lang.descending}</option>
<option value="asc">{$lang.ascending}</option>             
        </select></TD>
</TR>
<TR>
  <TD width="94" nowrap class=FormButton>#ID</TD>
  <TD>
  <INPUT name="id" type=text id="id" value="{$data.id}" class="SearchData"  /></TD>
</TR>
<tr>
  <td nowrap class=FormButton>{$lang.user_name}</td>
  <td><INPUT name="user_name" type=text id="id" value="{$data.id}" class="likeData"  /> {$lang.surname} : <INPUT name="user_surname" type=text id="id" value="{$data.id}" class="likeData"  /></td>
</tr>
<tr>
  <td nowrap class=FormButton>{$lang.user_class}</td>
  <td>
    <select name="user_class" class="SearchData">
	       <option value="">{$lang.all}</option>
           {if $USER_CLASS eq "SA"}<option value="SA" {if $filters.user_class eq "SA"}selected{/if}>Suser Admin</option>{/if}
           {if $USER_CLASS eq "SA" OR $USER_CLASS eq "A"}<option value="A" {if $filters.user_class eq "A"}selected{/if}>Admin</option>{/if}
           <option value="P" {if $filters.user_class eq "P"}selected{/if}>Provider</option>
           <option value="U" {if $filters.user_class eq "U"}selected{/if}>User</option>
         </select>
    </td>
</tr>
<tr>
  <td class=FormButton>{$lang.availability}</td>
  <td><select name="user_login" class="SearchData">
	      <option value="">{$lang.all}</option>
	      <option value="1" {if $filters.user_login eq 1}selected{/if}>{$lang.yes}</option>
	      <option value="0" {if $filters.user_login eq "0"}selected{/if}>{$lang.no}</option>
        </select></td>
</tr>
<tr>
  <td class=FormButton nowrap>{$lang.missing}</td>
  <td><select name="missing" class="secondarySearchData">
	      <option value="">{$lang.none}</option>
	      <option value="email" {if $filters.missing eq "email"}selected{/if}>{$lang.email}</option>
	      <option value="user_location" {if $filters.missing eq "user_location"}selected{/if}>{$lang.country}</option>
        </select></td>
</tr>
<tr>
  <td class=FormButton nowrap>{$lang.specialFilters}</td>
  <td><select name="custom_field" class="secondarySearchData">
    <option value="">{$lang.none}</option>
    <option value="uname" {if $filters.custom_field eq "uname"}selected{/if}>{$lang.username}</option>
    <option value="email" {if $filters.custom_field eq "email"}selected{/if}>{$lang.email}</option>
    </select>
    <input type="text" name="custom_value" size="30" value="{$data.substring}" class="secondarySearchData" />  </td>
</tr>
<tr> 
  <td colspan="2" align="center" class=FormButton>
    <input type="submit" value="{$lang.search}" class="button" id="searchUsers" name="searchUsers">
    <input name="page" type="hidden" id="page" value="{$page}" class="secondarySearchData">
    <input name="ResultsDiv" type="hidden"  value="#ResultsPlaceHolder" class="secondarySearchData">
    <input name="FormToHide" type="hidden"  value="#SearchTable" class="secondarySearchData"></td>
</tr>

</table>
</form>
</div>
<div id="ResultsPlaceHolder"></div>

<script src="/scripts/user.js"></script>
