<ul id="adminmenu2">
	<li><a href="/{$MODULE_FOLDER}/" {if $submenu eq "main"}class="current1"{/if}>{$lang.home}</a></li>
	<li><a href="/{$MODULE_FOLDER}/settings.php" {if $submenu eq "settings"}class="current1"{/if}>Module Settings</a></li>
    <li><a href="/{$MODULE_FOLDER}/extra_fields.php" {if $submenu eq "extra_fields"}class="current1"{/if}>{$lang.extra_fields}</a></li>
</ul>