  <div id="success" class="msg_box msg_ok" style="display:none;"><img src="/skin/images/admin2/blank.gif" class="msg_close" alt="" /></div>
<div id="fail" class="msg_box msg_error" style="display:none;"><img src="/skin/images/admin2/blank.gif" class="msg_close" alt="" /></div>

<div  class="formEl_a" id="user_validate">               

  <div class="float-left dp50" >
		<div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.username} <font color="#FF0000">*</font></label>
                <input name="uname" id="uname" type="text" for="όνομα χρήστη" class="userData uname inpt_b" value="{$user.uname}" required/> 
            	<input type="hidden" id="cur_userid" value="{$user.id}" />
            	<!-- [ <a href="#" class="cpaneltext" id="check_availability">{$lang.check_availability}</a> ] -->
            </div>
            {if !$user} 
       <div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.password} <font color="#FF0000">*</font></label>
                <input name="pass" type="password" for="κωδικός" class="userData  password inpt_b" id="pass" value="{$user.pass|text_decrypt}" required/>
                <a href="#" class="showHiddenPassword" id="show_pass" rel="{$user.pass}">{$lang.showPassword}</a> <div class="messageWarning messagePlaceHolder hidden"></div>
            </div>     
          <div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.password_confirm} <font color="#FF0000">*</font></label>
                <input name="confirm_password" for="επιβ. κωδικού" type="password" class="required confirm_password inpt_b" equalTo="#pass" id="confirm_password" value="{$user.pass|text_decrypt}" required data-verifyPasswords-msg="Οι κωδικοί δεν ταιριάζουν" />
            	<span class="k-invalid-msg" data-for="confirm_password"></span>
            </div>      
            {/if}
           <div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.userClass} <font color="#FF0000">*</font></label>
                <select name="user_class" id="user_class" class="userData inpt_b"">
       				 <option value="A" {if $user.user_class eq "A"}selected{/if}>Administrator</option>
       				 <option value="P" {if $user.user_class eq "P"}selected{/if}>User</option>
       				 {if $USER_CLASS eq "SA"}<option value="SA" {if $user.user_class eq "SA"}selected{/if}>Super Administrator</option>{/if}
      			 	{if $USER_CLASS eq "SA" OR $USER_CLASS eq "A"}<option value="U" {if $user.user_class eq "U"}selected{/if}>User</option>{/if}
      			</select>
            </div> 
             <div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.availability}</label>
                <select name="user_login" id="user_login" class="userData inpt_b">
       					 <option value="1" {if $user.user_login eq 1}selected{/if}>{$lang.yes}</option>
       					 <option value="0" {if $user.user_login eq 0}selected{/if}>{$lang.no}</option>
      			</select>
            </div> 
            
   </div>        
  
  
    <div class="float-left dp50" >
		<div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.user_name} <font color="#FF0000">*</font> </label>
                <input name="user_name" type="text"for="όνομα" class="userData required user_name inpt_b" value="{$user.user_name}" required /> 
            </div>
       <div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.surname}<font color="#FF0000"> *</font></label>
                <input name="user_surname" type="text" for="επώνυμο" class="userData required user_surname inpt_b" value="{$user.user_surname}" required />
            </div>     
          <div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.email} <font color="#FF0000">*</font></label>
               <input name="email" type="email" for="e-mail" id='email' class="userData required email inpt_b" value="{$user.email}" required data-email-msg="Η μορφή του e-mail δεν είναι σωστή" />
            </div>   
           <div class="sepH_b">
                <label class="lbl_a" for="v_text">{$lang.country} <font color="#FF0000">*</font></label>
                <select name="user_location" class="userData  ipnt_b" required validationMessage="Επιλέξτε μία χώρα">
     			   <option value="">{$lang.select_one}</option>
						{foreach from=$countries item=a}
     						<option value="{$a.code}" {if $a.code|upper eq $user.user_location|upper}selected{/if}>{$a.country}</option> 
					{/foreach}
      			</select>
            </div>  <br/><br/>
		
            
   </div>
  		<br class="clear"/><br/>
    <center>
  		<input name="mode" type="hidden" id="mode" value="add">
      {if $user}
      <input type="hidden" value="{$user.id}" name="id" id="id" class="userData" />
      <input type="submit" name="button" id="saveUserDetails" value="{$lang.save}" class="btn btn_b add_edit_user" />
      {else}
        <input type="submit" name="button" id="submit_form" value="{$lang.create_account}" class="btn btn_b add_edit_user" />
      {/if}
      <input type="hidden" value="admin" name="section" id="section" />
    </center>
</div>
<script type="text/javascript" src="/scripts/admin2/users/user_validate.js"></script>
