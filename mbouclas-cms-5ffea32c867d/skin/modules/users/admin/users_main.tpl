
<input type="hidden" id="total_users" value="{$total}"/>
<div class="dp100">
    <div class="box_c">
           <h3 class="box_c_heading cf">
	           <span class="fl spacer-left"> {$lang.manage_users} &nbsp;  </span></h3> 
       	
					    <div class="box_c_content cf" > 
					    {foreach from=$layout.boxes.C item=a}  	
					  	 <div id="{$a.name}" data-id="{$a.id}" class="LoadBox"></div>
					    {/foreach}  
					     </div>			
    </div>  
</div>

		<script id="delete-confirmation" type="text/x-kendo-template">
			<p class="delete-message" >{$lang.confirm_deletion}</p> <br/>
			<button class="delete-confirm k-button" data-id="#= item.id #" data-type="#= item.type #">{$lang.delete}</button>
			<a href="javascript:void(0);" class="float-right spacer-right delete-cancel">{$lang.lbl_cancel}</a>

		</script>
		
		<script id="template-toolbar-PagesSearch" class="template-toolbar-PagesSearch" type="text/kendo-ui-template">

				<div class="k-button k-button-icontext Filter float-right k-filter">{$lang.filter}&nbsp;&nbsp;<span class=" k-icon k-filter"></span></div>
				<div class="k-button k-button-icontext float-right margin-right k-edit_user" id="CreateUser">{$lang.addUser}&nbsp;&nbsp;<span class="k-icon k-add"></span></div>		
        </script>
		
		<script id="id-commands-gridview" class="id-commands-gridview" type="text/kendo-ui-template">
				<span  id='#= id #' data-itemid='#= id #' class="saveGrid k-icon k-edit k-edit_user pointer spacer-right"  ></span>
				<span  id='#= id #' data-itemid='#= id #' class="saveGrid k-icon k-delete pointer"  ></span>		
        </script>

{if $plugin_file} 
	{include file=$plugin_file}
{/if}
<script type="text/javascript">
	head.js('/scripts/admin2/users/users.js');
	head.js('/scripts/admin2/grid.js');
	head.js('/scripts/admin2/users/add_edit_user.js');
</script>
