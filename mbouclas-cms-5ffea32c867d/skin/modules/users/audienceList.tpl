<form>
<div class="debugArea"></div>
<div class="message warning hidden" id="messages"></div>

<div class="multiRowCheckboxMenu" id="checkBoxes">
<label class="header"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /> (<span class="messages">0</span> {$lang.selected})</label>
<ul class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all">
<li class="ui-menu-item"><a href="javascript:void(0)" class="ui-corner-all">{$lang.all}</a></li>
<li class="ui-menu-item"><a href="javascript:void(0)" class="ui-corner-all">{$lang.none}</a></li>
</ul>
</div>

<div class="audienceListsFilterButton">
<div class="buttonItem">{$lang.lbl_lists}</div>

<div id="audienceListsFilter" class="hidden">
<div class="multiSelectList">
						<div class="listItems">
                        <div class="searchField"><input type="text" id="searchLists" maxlength="40" ignoreesc="true" style="" tabindex="0" autocomplete="off"><div class="searchFieldIcon"></div></div>
                        <div class="listItemsRows">
                    		{foreach from=$lists item=a}
			                  <div class="listItem listItemMenu"><div class="checkBox" id="{$a.id}"></div><span class="list">{$a.title}</span></div> 
            		       {/foreach}
                           </div>
                            <div><hr /></div> 
                            <div class="applyChanges hidden">{$lang.save}</div>
							<div class="listMenu listItemMenu">{$lang.lbl_addList} {$lang.list}</div>
						</div>
                         <div id="newList" class="hidden">
                         <h3>{$lang.lbl_addList}</h3>
                         <div class="newListContainer searchField"><input type="text" id="newList" name="title" maxlength="40" ignoreesc="true" class="newList" style="" tabindex="0"></div>
                           <button class="button button-green saveNewList" >{$lang.save}</button> <button class="button button-red cancelNewList">{$lang.back}</button>
                           </div><!-- NEW LIST -->
                           <div class="listItemMenu"><a href="/userCP/audienceLists.html" title="">{$lang.lbl_manageLists}</a></div>
					</div>
</div>
</div><!-- END MENU -->



<div class="multiRowCheckboxMenu" id="actions">
<label class="header menu">{$lang.withSelectedDo}</label>
<a href="#" class="menu">Menu</a>
<ul class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all">
<li class="ui-menu-item"><a href="javascript:void(0)" class="ui-corner-all">{$lang.enable}</a></li>
<li class="ui-menu-item"><a href="javascript:void(0)" class="ui-corner-all">{$lang.disable}</a></li>
<li class="ui-menu-item"><a href="javascript:void(0)" class="ui-corner-all">{$lang.delete}</a></li>
</ul>
</div>
<div class="selectAllResults aligncenter hidden"><div class="seperator"></div><input type="button" class="button button-blue" value="{$lang.selectAllResults}" alt="#filtersForm" ></div>
    <div class="seperator"></div>
         <div class="message warning hidden" id="selectAllResultsInfo"><h4>{$lang.allResultsSelected}</h4> <strong><span class="messages">0</span> {$lang.total}</strong></div>
         
     
         
                            <div class="spacer-bottom">
                            <strong>{$list.from}</strong> {$lang.to} <strong>{$list.to}</strong> {$lang.from} <strong>{$list.total}</strong> {$lang.lbl_results} | {$lang.page} <strong>{$PAGE}</strong> {$lang.from} <strong>{$total_pages}</strong>
                            </div>                            <ul id="contacts" class="listing list-view clearfix">
                            {foreach from=$audience item=a}
                                <li class="contact clearfix">
                                    <div class="avatar"><img src="/images/userCp/user_32.png" /></div>
                                  <a class="more" href="{$a.id|text_crypt}">&raquo;</a>
                                    <span class="timestamp">{$a.date_added|date_format:"%d/%m/%Y"}</span>
                                    <input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /> <a href="#" class="name">{$a.name|default:"-"} {$a.surname}</a>
									<div class="entry-meta">{if $a.lists}{foreach from=$a.lists item=b name=b}<a href="#" class="filterList" rel="{$b.id}">{$b.title}</a> {/foreach}{/if}</div>
                                </li>
                            {/foreach}
                    </ul>
                            {if $list.total gt $users|@count}
<div class="spacer-top">
<strong>{$list.from}</strong> {$lang.to} <strong>{$list.to}</strong> {$lang.from} <strong>{$list.total}</strong> {$lang.lbl_results} | {$lang.page} <strong>{$PAGE}</strong> {$lang.from} <strong>{$total_pages}</strong>
                            </div>
                            <ul class="pagination clearfix">
                               {if $navigation.first}<li><a href="#{$target|default:"filtersForm"}" class="button-blue {$class|default:"audiencePage"}"  title="{$lang.page} : {$navigation.first}" id="page-{$navigation.first}" rel="{$navigation.first}">&laquo;&laquo;</a></li>{/if}
                               {if $navigation.previous}<li><a href="#{$target|default:"filtersForm"}" class="button-blue {$class|default:"audiencePage"}"  title="{$lang.page} : {$navigation.previous}" id="page-{$navigation.previous}" rel="{$navigation.previous}">&laquo;</a></li>{/if}
                               {foreach name=p from=$num_links item=a} 
                                <li><a href="#{$target|default:"filtersForm"}" class="{if $a.page eq $PAGE}current{/if} button-blue {$class|default:"audiencePage"}"  title="{$lang.page} : {$a.page}" id="page-{$a.page}" rel="{$a.page}">{$a.page}</a></li>
                                {/foreach}
                               {if $navigation.next}<li><a href="#{$target|default:"filtersForm"}" class="button-blue {$class|default:"audiencePage"}"  title="{$lang.page} : {$navigation.next}" id="page-{$navigation.next}" rel="{$navigation.next}">&raquo;</a></li>{/if}
                               {if $navigation.final}<li><a href="#{$target|default:"filtersForm"}" class="button-blue {$class|default:"audiencePage"}"  title="{$lang.page} : {$navigation.final}" id="page-{$navigation.final}" rel="{$navigation.final}">&raquo;&raquo;</a></li>{/if}
                            </ul>
                            {/if}
                            <input type="hidden" class="CheckedActions" value="0" name="allItemsSelected" />
							<input name="page" type="hidden" id="page" value="{$page}" class="CheckedActions">
</form>