{if $ID}
{if !$smarty.session.cart}
	<div class="page-title category-title">
    	<h1>Shopping</h1>
	</div>
<div class="cart-empty">
        <p>You have no items in your shopping cart.</p>
    <p>Click <a href="/{$FRONT_LANG}/index.html">here</a> to continue shopping.</p>
</div>
{else}

<div class="page-title title-buttons category-title">
        <h1>Shopping</h1>
  
    </div>
    <div class="clear seperator"></div>
<ul id="mainNav" class="threeStep">
		<li id="first" class="lastDone"><a title="Βήμα 1: Καλάθι αγορών" href="/cart.html"><em>Βήμα 1: Καλάθι αγορών</em><span>δείτε συνοπτικά την παραγγελία σας</span></a></li>
		<li id="second" class="current"><a  title="Βήμα 2: Καλάθι αγορών" href="/{$FRONT_LANG}/checkout.html"><em>Βήμα 2: Πληρωμή</em><span>Επιλογή τρόπου πληρωμής</span></a></li>
        <li id="third" class="mainNavNoBg"><a title=""><em>Βήμα 3: Ολοκλήρωση</em><span>Τέλος παραγγελίας</span></a></li>
	</ul>
<div class="clear seperator"></div>

{*
<div class="float_right spacer " >
  <h2>Τελικό ποσό πληρωμής : <strong class="price">&euro;<span class="total">{$total_price|formatprice:".":","}</span></strong></h2>
  <h3 class="hidden installments-container price_small">επιβάρυνση δόσεων : &euro; <span class="total_extra">0</span></h3>
  <div class="shipping_method_text hidden">Τρόπος αποστολής : <strong><span></span></strong></div>
</div>
*}
<div class="seperator"></div>  

  
<ol class="opc" id="checkoutSteps">
{foreach from=$layout.C key=i item=a name=b}
<li  id="{$a.id}" class="section {if $smarty.foreach.b.first}allow active{/if}">
	<div class="CheckoutTab step-title"><span class="number">{$i+1}</span><h2>{$lang[$a.title]|default:$a.title}</h2></div>
	<div class="step a-item {if !$smarty.foreach.b.first}hidden{/if}">{include file=$a.template}</div>
</li>
{/foreach}
</ol>
	
	<div class="spacer-top" align="center">
		<a href="#"  id="checkOut-Button" class="submit-order">Επισκόπηση/Ολοκλήρωση παραγγελίας</a>
	</div>
        
	


<script>
head.js("/scripts/site/UserForm.js");
head.js("/scripts/site/checkout.js");
</script>
{/if}{* PRODUCTS *}
{else}
{include file="modules/users/userLoginForm.tpl"}
{/if}
