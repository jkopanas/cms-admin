      
<div class="spacer-top">
<strong>{$list.from}</strong> {$lang.to} <strong>{$list.to}</strong> {$lang.from} <strong>{$list.total}</strong> {$lang.lbl_results} | {$lang.page} <strong>{$PAGE}</strong> {$lang.from} <strong>{$total_pages}</strong>
                            </div>
                            {if $list.total gt $users|@count}
                            <ul class="pagination clearfix">
                               {if $navigation.first}<li><a href="#{$target|default:"filtersForm"}" class="button-blue {$class|default:"paginateResults"}"  title="{$lang.page} : {$navigation.first}" id="page-{$navigation.first}" rel="{$navigation.first}">&laquo;&laquo;</a></li>{/if}
                               {if $navigation.previous}<li><a href="#{$target|default:"filtersForm"}" class="button-blue {$class|default:"paginateResults"}"  title="{$lang.page} : {$navigation.previous}" id="page-{$navigation.previous}" rel="{$navigation.previous}">&laquo;</a></li>{/if}
                               {foreach name=p from=$num_links item=a} 
                                <li><a href="#{$target|default:"filtersForm"}" class="{if $a.page eq $PAGE}current{/if} button-blue {$class|default:"paginateResults"}"  title="{$lang.page} : {$a.page}" id="page-{$a.page}" rel="{$a.page}">{$a.page}</a></li>
                                {/foreach}
                               {if $navigation.next}<li><a href="#{$target|default:"filtersForm"}" class="button-blue {$class|default:"paginateResults"}"  title="{$lang.page} : {$navigation.next}" id="page-{$navigation.next}" rel="{$navigation.next}">&raquo;</a></li>{/if}
                               {if $navigation.final}<li><a href="#{$target|default:"filtersForm"}" class="button-blue {$class|default:"paginateResults"}"  title="{$lang.page} : {$navigation.final}" id="page-{$navigation.final}" rel="{$navigation.final}">&raquo;&raquo;</a></li>{/if}
                            </ul>
                            {/if}

<ul>
{foreach from=$users item=a}
<li>{$a.id} {$a.user_name} {$a.user_surname} {$efieldsFound[$a.id].percent}% {$locationsFound[$a.id].distance|number_format:2} km away {$a.email} | {$a.age} {$a.profile.gender}</li>
{/foreach}
</ul>

