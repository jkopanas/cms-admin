{if $item.type eq multiselect}
    {if $item.settings.datatype eq dropdown}
    
    <select name="{$item.var_name}" class="SaveData">
        {foreach from=$item.settings.data item=v key=k}
		<option value="{$k}" {if $useritems|is_array AND $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}selected="selected"{/if}>{$v}</option>
		{/foreach}
    </select>	
	{/if}
    
    {if $item.settings.datatype eq radio}
        {foreach from=$item.settings.data item=v key=k}
        <input type="radio" value="{$k}" id="{$item.field}{$k}" name="{$item.var_name}" class="SaveData" {if $useritems|is_array AND  $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}checked{/if} /><label for="{$item.field}{$k}">{$v}</label><br/>
        {/foreach}
	{/if}
    
    {if $item.settings.datatype eq checkbox}
        {foreach from=$item.settings.data item=v key=k name=c}
        <label><input type="checkbox" value="{$k}" id="{$item.var_name}-{$k}" name="{$item.var_name}[]" class="SaveData" {if $useritems|is_array AND  $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}CHECKED{/if}/> {$v}</label><br/>
        {/foreach}
	{/if}
    
    {if $item.settings.datatype eq multiple}
    <select name="{$item.var_name}[]" MULTIPLE SIZE="5" class="SaveData">
		{foreach from=$item.settings.data item=v key=k}
		<option value="{$k}" {if $useritems|is_array AND $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}selected="selected"{/if}>{$v}</option>
		{/foreach}
    </select>
	{/if}
{/if}
