{if $item.type eq multiselect}
    {if $item.settings.datatype eq dropdown}
        {if $item.settings.datatyperange eq single}
        <select name="efield_{$item.var_name}" class="SaveData">
        <option value="">{$lang.all}</option>
            {foreach from=$item.settings.data item=v key=k}
            <option value="{$k}" {if $useritems|is_array AND $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}selected="selected"{/if}>{$v}</option>
            {/foreach}
        </select>	
        {/if}
        
        {if $item.settings.datatyperange eq range}
        From:
        <select name="efield_from-{$item.var_name}" class="SaveData rangeFrom">
        <option value="">{$lang.all}</option>
            {foreach from=$item.settings.data item=v key=k}
            <option value="{$k}" {if $useritems|is_array AND $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}selected="selected"{/if}>{$v}</option>
            {/foreach}
        </select>
        To:    
        <select name="efield_to-{$item.var_name}" class="SaveData rangeTo">
        <option value="">{$lang.all}</option>
            {foreach from=$item.settings.data item=v key=k}
            <option value="{$k}" {if $useritems|is_array AND $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}selected="selected"{/if}>{$v}</option>
            {/foreach}
        </select>	
        {/if}
    {/if}

    {if $item.settings.datatype eq radio}
    <ul>
            {foreach from=$item.settings.data item=v key=k}
            <li>
            <input type="radio" value="{$k}" id="{$item.field}{$k}" name="efield_{$item.var_name}" class="SaveData" {if $useritems|is_array AND  $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}checked{/if} /><label for="{$item.field}{$k}">{$v}</label><br/>
            </li>
            {/foreach}
    </ul>        
    {/if}
    
    {if $item.settings.datatype eq checkbox}
        <ul>
        {foreach from=$item.settings.data item=v key=k name=c}
        <li>
        <label for="{$item.var_name}-{$k}"><input type="checkbox" value="{$k}" id="{$item.var_name}-{$k}" name="efield_{$item.var_name}[]" class="SaveData" {if $useritems|is_array AND  $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}CHECKED{/if}/> {$v}</label><br/>
        </li>
        {/foreach}
		</ul>
    {/if}
    
    {if $item.settings.datatype eq multiple}
    <select name="efield_{$item.var_name}[]" MULTIPLE SIZE="5" class="SaveData">
		{foreach from=$item.settings.data item=v key=k}
		<option value="{$k}" {if $useritems|is_array AND $item.fieldid|cat: '_'|cat: $k|array_key_exists:$useritems}selected="selected"{/if}>{$v}</option>
		{/foreach}
    </select>
	{/if}
{/if}

