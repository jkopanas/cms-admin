<html> 
 
<head> 
<title>{$SITE_NAME}</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
</head> 
{literal}
 <style>
 body { font-size:12px; font-family:Georgia, "Times New Roman", Times, serif;}
 h1 { font-size:20px; color:#D0910B }
 .footer { font-family:Verdana, Geneva, sans-serif; font-size:11px;}



 </style>
 {/literal}
<body>
<table width="600" {literal} style="text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px;"{/literal} >
<tbody>
  <tr>
    <td colspan="2"><img src="{$URL}/images/logo.gif"></td>
  </tr>
  <tr>
											<td colspan="2">
												
		Νέα παραγγελία από {$USER_NAME}
		
	

											</td>
										</tr>
										<tr>
											<td colspan="2">
												αριθμός παραγγελίας <strong>{$orderid}</strong>
											</td>
										</tr>							
										
										<tr>
											<td colspan="2"><table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th {literal}style="text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px;"{/literal}>Κωδικός MgManager</th>
    <th {literal}style="text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px;"{/literal}>Προϊόν</th>
    <th {literal}style="text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px;"{/literal}>Τιμή μονάδας χωρίς Φ.Π.Α</th>
    <th {literal}style="text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px;"{/literal}>Ποσότητα</th>
    <th {literal}style="text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px;"{/literal}>Σύνολο</th>
  </tr>
  {foreach from=$items item=a key=k name=b}
  <tr>
    <td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}><strong>{$k}</strong></td>
    <td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}><a href="{$URL}/product_{$k}.html">{$a.title}</a></td>
    <td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>&euro; {$a.price|formatprice:".":","}</td>
    <td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$a.quantity}</td>
    <td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>&euro; <span id="total-{$k}">{$a.total|formatprice:".":","}</span></td>
  </tr>
  {/foreach}
  <tr>
    <td class="seperator"></td></tr>
  <tr><td colspan="5" align="right">Μερικό Σύνολο : <strong>&euro;<span class="total-price-no-vat">{$total_price_no_vat|formatprice:".":","}</span></strong></td></tr>
  <tr><td colspan="5" align="right">Αξία Φ.Π.Α : <strong>&euro;<span class="vat-price">{$vat_price|formatprice:".":","}</span></strong></td></tr>
  <tr><td colspan="5" align="right">Γενικό Σύνολο : <strong class="price">&euro;<span class="total-price">{$total_price|formatprice:".":","}</span></strong></td></tr>
  <tr>
    <td class="seperator"></td></tr>
  </table></td>
										</tr>												
										<tr>

											<td valign="top" width="50%">
												
												<table border="0" cellpadding="5" cellspacing="1" width="100%" {literal} style="text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px;"{/literal}>
		
													<tr width="30%">
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Τύπος Παραστατικού</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{if $details.invoice_type eq "reciept"}Απόδειξη{else}Τιμολόγιο{/if}</td>

													</tr>
                                                    {if $details.invoice_type eq "invoice"}
			
													<tr>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Επωνυμία Εταιρίας</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.company_name}</td>
													</tr>
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Δραστηριότητα</td>

														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.proffession}</td>
													</tr>
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Α.Φ.Μ.</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.afm}</td>
													</tr>
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Δ.Ο.Υ.</td>

														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.doy}</td>
													</tr>
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Τηλέφωνο</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.phone}</td>
													</tr>										
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Διεύθυνση</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.address}</td>

													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Ταχ. Κώδικας</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.postcode}</td>
													</tr>	
                                                    {/if}						
												</table>								
												
											</td>
											<td valign="top" width="50%">
												
												<table border="0" cellpadding="5" cellspacing="1" width="100%" {literal} style="text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px;"{/literal}>
													<tr>
														<td colspan="2">
															Στοιχεία αποστολής προϊόντων
														</td>
													</tr>		
													<tr width="30%">

														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Μέθοδος αποστολής</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$shipping_method.shipping}</td>
													</tr>
													<tr>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Όνομα</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.order_name}</td>
													</tr>				
													<tr>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Επώνυμο</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.order_surname}</td>

													</tr>
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Τηλέφωνο</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.order_phone}</td>
													</tr>
                                                    {if $shipping_method.parent.settings.form eq "full"}
													<tr>
													  <td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Κινητό</td>
													  <td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.order_mobile}</td>
												  </tr>
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Διεύθυνση</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.order_address}</td>

													</tr>
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Δήμος</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.municipality}</td>
													</tr>
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Πόλη</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.order_city}</td>

													</tr>
													<tr>					
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>Ταχ. Κώδικας</td>
														<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>{$details.postcode}</td>
													</tr>
                                                    {/if}
												</table>									
												
											</td>
										</tr>
										<tr>
											<td colspan="2">
													<table border="0" cellpadding="5" cellspacing="1" width="100%">
														
														<tbody><tr>
															<td {literal}  style="text-align: center; font-family: Verdana; font-weight: normal; font-size: 11px; color: #404040; background-color: #fafafa; border: 1px #6699CC solid; border-collapse: collapse; border-spacing: 0px;"{/literal}>

																Τρόπος πληρωμής: <b>{$payment_method.title}</b>
															</td>
														</tr>					
														
														
													</tbody></table>									
											</td>
										</tr>
										<tr>
											<td colspan="2">&nbsp;</td>
										</tr>
									</tbody>
                                    </table>
</body>
                                    </html>