<div class="container_8 clearfix">
                <h1 class="grid_1"><a href="dashboard.html">Control Panel</a></h1>
                <nav class="grid_7">
                    <ul class="clearfix">
                    <!--
                        <li class="action"><a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Contact</a></li>
                        <li class="action"><a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New SMS</a></li>
                        -->
                            <li {if $action eq "dashboard"}class="active"{/if}><a href="/userCP/dashboard.html">{$lang.home}</a></li>
                            <li {if $action eq "audience"}class="active"{/if}><a href="/userCP/audience.html">{$lang.lbl_audience}</a>
                            <ul><li><a href="/userCP/addAudience.html">{$lang.add}</a></li></ul>
                            </li>
                            <li {if $action eq "sendSMS"}class="active"{/if}><a href="/userCP/sendSMS.html">{$lang.lbl_sendSMS}</a></li>
                            <li {if $action eq "myOrders"}class="active"{/if}><a href="/userCP/myOrders.html">{$lang.lbl_myOrders}</a></li>
                            <li {if $action eq "myAccount"}class="active"{/if}><a href="/userCP/myAccount.html">{$lang.lbl_myAccount}</a></li>
                    </ul>
                </nav>

            </div>
