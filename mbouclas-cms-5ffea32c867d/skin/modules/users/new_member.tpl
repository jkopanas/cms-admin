<div class="col-main" style="width:850px;">					
  <div class="account-create" >
    <div class="page-title">
       <h1>{$lang.new_user}</h1>
    </div>
    {if $action eq "step1"}
    <div>MINIMA</div>
    {else}
  <form name="form" action="{$SELF}" method="post" id="signup">
	<div class="fieldset">
		        <h2 class="legend">Personal Information</h2>
		  <div style="float:left;">      
	 <ul class="form-list">
		      <li class="fields">
				       <div class="customer-name">
				    <div class="field name-firstname fields" style="width:290px;">
				        <label class="required" for="firstname"><em>*</em>First Name</label>
				        <div class="input-box ">
				            <input type="text" class="input-text required-entry adduser" for="όνομα" value="" name="user_name" id="firstname" required>
			        	
				        </div>
				    </div>
				    <div class="field name-lastname fields" style="width:290px;">
				        <label class="required" for="lastname"><em>*</em>Last Name</label>
				        <div class="input-box">
				            <input type="text" class="input-text required-entry adduser" for="επώνυμο" value="" name="user_surname" id="lastname">
				        </div>
				    </div>
				</div>
		    </li>
		         <li>
		            <label class="required" for="email_address"><em>*</em>{$lang.email}</label>
		             <div class="input-box" style="width:290px;">
		                 <input name="email" type="email"for="e-mail" class="input-text validate-email required-entry adduser" id="email_address" required email>
	              
		               </div>
		         </li>
		         
		           <li class="control">
		               <div class="input-box">
		               <input name="newsletter" type="checkbox" > 
		               </div>
		               <label for="is_subscribed">{$lang.sign_up_newsletter}</label>
		            </li>
	 </ul>
	 </div>
	 <div style="float:left; padding-left:30px;">
	  <ul class="form-list">
		      <li class="fields">
				       <div class="customer-name">
				    <div class="field name-firstname fields" style="width:290px;">
				        <label class="required" for="firstname"><em>*</em>{$lang.username}</label>
				        <div class="input-box">
				            <input type="text" class="input-text required-entry adduser"  value="" for="όνομα" name="uname" id="uname"  required>				  
				        </div>
				    </div>
				    <div class="field name-lastname" style="width:290px;">
				        <label class="required" for="lastname"><em>*</em>Address</label>
				        <div class="input-box">
				            <input type="text" class="input-text required-entry adduserSettings" for="διεύθυνση" value="" name="address" id="address">
				        </div>
				    </div>
				</div>
		    </li>
		         <li>
		            <label class="required" for="email_address"><em>*</em>City</label>
		             <div class="input-box" style="width:290px;">
		                 <input type="text" class="input-text  required-entry adduserSettings" for="πόλη" value="" id="city" name="city">
		               </div>
		         </li>
		         <li>
		            <label class="required" for="email_address"><em>*</em>Zip Code</label>
		             <div class="input-box" style="width:290px;">
		                 <input type="text" class="input-text required-entry adduserSettings" for="Ταχ. κώδικας" value="" id="ZipCode" name="ZipCode">
		               </div>
		         </li>
		         
	 </ul>
	 </div>
	 
	</div> <!-- end class="fieldset" --><div class="seperator"></div>
		        
		            <div class="fieldset">
		            <h2 class="legend">Login Information</h2>
		            <ul class="form-list">
		                <li class="fields">
		                    <div class="field" style="float:left; width:290px;">
		                        <label class="required" for="password"><em>*</em>{$lang.password}</label>
		                        <div class="input-box">
		                            <input  name="pass" type="password" class="input-text required-entry validate-password adduser" for="κωδικός" id="password"  required >
		                        </div>
		                    </div>
		                    <div class="field" style="float:left; padding-left:30px; width:290px;">
		                        <label class="required" for="confirmation"><em>*</em>{$lang.password_confirm}</label>
		                        <div class="input-box">
		                            <input type="password" name="confirm_password" class="input-text required-entry validate-cpassword" id="confirm_password" for="Επιβ. κωδικού"  required >
		                        </div>
		                    </div>
		                </li>
		                            </ul>
		            
		
		
		        </div> <div style="padding-top:20px;"></div>
		        <div class="buttons-set">
		            <p class="required">* Required Fields</p>
		            <p class="back-link"><a class="back-link" href="/{$FRONT_LANG}/member_login.html"><small>« </small>{$lang.back}</a></p>
		            <button class="button submit-button" title="Submit" type="submit" id="submit_form" value=""><span><span>{$lang.create_account}</span></span></button>
		        </div>
      </form>
    {/if}
</div> <!-- end class="account-create" -->
                           
   </div> <!-- end  class="col-main" -->

<style>

span.k-tooltip {
    border-width: 0px;
    display: inline-block;
    padding: 2px 5px 1px 6px;
    position: static;
    color:red;
    background:url("/images/site/validation_advice_bg.gif") no-repeat scroll 2px 0 transparent
}

</style>


<script>
head.js('/scripts/site/UserForm.js');
</script>