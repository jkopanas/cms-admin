{if $ID}
{if $order}
{if $order.status eq 2}
	<ul id="mainNav" class="fourStep"> 
		<li class="current"><a title=""><em>Σε αναμονή</em><span>Περιμένουμε επιβεβαίωση της παραγγελίας.<br /> Ισχύει για αντικαταβολή ή κατάθεση <br />σε τράπεζα</span></a></li> 
		<li><a title=""><em>Επιβεβαίωση παραγγελίας</em><span>Ηα παραγγελία επιβεβαιώθηκε</span></a></li> 
		<li><a title=""><em>Προετοιμασία</em><span>Συγκέντρωση προϊόντων</span></a></li> 
		<li class="mainNavNoBg"><a title=""><em>Αποστολή</em><span>Αποστολή παραγγελίας</span></a></li> 
	</ul> 
{elseif $order.status eq 4}
	<ul id="mainNav" class="fourStep"> 
		<li class="lastDone"><a title=""><em>Σε αναμονή</em><span>Περιμένουμε επιβεβαίωση της παραγγελίας.<br /> Ισχύει για αντικαταβολή ή κατάθεση <br />σε τράπεζα</span></a></li> 
		<li class="current"><a title=""><em>Επιβεβαίωση παραγγελίας</em><span>Ηα παραγγελία επιβεβαιώθηκε</span></a></li> 
		<li><a title=""><em>Προετοιμασία</em><span>Συγκέντρωση προϊόντων</span></a></li> 
		<li class="mainNavNoBg"><a title=""><em>Αποστολή</em><span>Αποστολή παραγγελίας</span></a></li> 
	</ul> 
{elseif $order.status eq 5}
	<ul id="mainNav" class="fourStep"> 
		<li class="done"><a href="/" title=""><em>Σε αναμονή</em><span>Περιμένουμε επιβεβαίωση της παραγγελίας.<br /> Ισχύει για αντικαταβολή ή κατάθεση <br />σε τράπεζα</span></a></li> 
		<li class="lastDone"><a href="/" title=""><em>Επιβεβαίωση παραγγελίας</em><span>Ηα παραγγελία επιβεβαιώθηκε</span></a></li> 
		<li class="current"><a title=""><em>Προετοιμασία</em><span>Συγκέντρωση προϊόντων</span></a></li> 
		<li class="mainNavNoBg"><a title=""><em>Αποστολή</em><span>Αποστολή παραγγελίας</span></a></li> 
	</ul> 
{elseif $order.status eq 6}
	<ul id="mainNav" class="fourStep"> 
		<li class="done"><a href="/" title=""><em>Σε αναμονή</em><span>Περιμένουμε επιβεβαίωση της παραγγελίας.<br /> Ισχύει για αντικαταβολή ή κατάθεση <br />σε τράπεζα</span></a></li> 
		<li class="done"><a href="/" title=""><em>Επιβεβαίωση παραγγελίας</em><span>Ηα παραγγελία επιβεβαιώθηκε</span></a></li> 
		<li class="lastDone"><a href="/" title=""><em>Προετοιμασία</em><span>Συγκέντρωση προϊόντων</span></a></li> 
		<li class="mainNavNoBg current"><a title=""><em>Αποστολή</em><span>Αποστολή παραγγελίας</span></a></li> 
	</ul> 
{/if}
<div class="seperator"></div>
<div class="box">
<h1 class="header">Παραγγελία #{$order.id}</h1>
<div class="content ">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="189"><strong>#Αριθμός παραγγελίας </strong></td>
    <td width="1429">{$order.id}</td>
    <td width="1429" rowspan="10" valign="top"><table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th>Κωδικός MgManager</th>
    <th>Προϊόν</th>
    <th>Τιμή μονάδας με Φ.Π.Α</th>
    <th>Ποσότητα</th>
    <th>Σύνολο</th>
  </tr>
  {foreach from=$order.products item=a key=k name=b}
  <tr>
    <td><strong>{$a.productid}</strong></td>
    <td><a href="/product/{$a.productid}/{$a.title|clean_url}.html">{$a.title}</a></td>
    <td>&euro; {$a.price|formatprice:".":","}</td>
    <td>{$a.quantity}</td>
    <td>&euro; <span id="total-{$k}">{$a.total|formatprice:".":","}</span></td>
  </tr>
  {/foreach}
  <tr>
    <td class="seperator"></td></tr>
  <tr><td colspan="5" align="right">Μερικό Σύνολο : <strong>&euro;<span class="total-price-no-vat">{$total_price_no_vat|formatprice:".":","}</span></strong></td></tr>
  <tr><td colspan="5" align="right">Αξία Φ.Π.Α : <strong>&euro;<span class="vat-price">{$vat_price|formatprice:".":","}</span></strong></td></tr>
  {if $order.details.ExtraCharge}
  <tr><td colspan="5" align="right">επιβάρυνση δόσεων : <strong>&euro; {$order.details.ExtraCharge|formatprice:".":","}</strong></td></tr>
  {/if}
  <tr><td colspan="5" align="right">Γενικό Σύνολο : <strong class="price">&euro;<span class="total-price">{$total_price_vat|formatprice:".":","}</span></strong></td></tr>
  <tr>
  <tr>
    <td class="seperator"></td></tr>
  </table></td>
  </tr>
  <tr>
    <td width="189"><strong>email</strong></td>
    <td>{$order.email}</td>
  </tr>
  <tr>
    <td width="189"><strong>Ποσό</strong></td>
    <td class="price"><strong>&euro;{$total_price_vat|formatprice:".":","}</strong></td>
  </tr>
  <tr>
    <td width="189"><strong>Κατάσταση</strong></td>
  <td>{$status_codes[$order.status]}      </tr>
  <tr>
    <td width="189"><strong>Τρόπος Πληρωμής</strong></td>
    <td>{$order.payment_method.title}</td>
  </tr>
  <tr>
    <td width="189"><strong>Ημερομηνία</strong></td>
    <td>{$order.date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
  </tr>
  <tr>
    <td width="189"><strong>Όνομα</strong></td>
    <td>{$order.details.order_name} {$order.details.order_surname}</td>
  </tr>
  <tr>
    <td width="189"><strong>Τηλέφωνο</strong></td>
    <td>{$order.details.order_phone}</td>
  </tr>
  <tr>
    <td width="189"><strong>Διεύθηνση</strong></td>
    <td>{$order.details.order_address} - {$order.details.order_city} {$order.details.order_postcode}</td>
  </tr>
  <tr>
    <td width="189"><strong>Τύπος Παραστατικού</strong></td>
    <td>{if $details.invoice_type eq "reciept"}Απόδειξη{else}Τιμολόγιο{/if}</td>
  </tr>
    {if $order.details.invoice_type eq "invoice"}
  <tr>
    <td><strong>Επωνυμία Εταιρίας</strong></td>
    <td>{$order.details.company_name}</td>
  </tr>
    <tr>
    <td><strong>Δραστηριότητα</strong></td>
    <td>{$order.details.proffession}</td>
  </tr>
    <tr>
    <td><strong>Α.Φ.Μ.</strong></td>
    <td>{$order.details.afm}</td>
  </tr>
    <tr>
    <td><strong>Δ.Ο.Υ.</strong></td>
    <td>{$order.details.doy}</td>
  </tr>
    <tr>
    <td><strong>Τηλέφωνο</strong></td>
    <td>{$order.details.phone}</td>
  </tr>
    <tr>
    <td><strong>Διεύθυνση</strong></td>
    <td>{$order.details.address}</td>
  </tr>
    <tr>
    <td><strong>Ταχ. Κώδικας</strong></td>
    <td>{$order.details.postcode}</td>
  </tr>
    <tr>
    <td><strong>Σχόλια</strong></td>
    <td><em>"{$order.details.comments}"</em></td>
  </tr>
  {/if}
  <tr>
    <td><strong>Σχόλια</strong></td>
    <td>"<em>{$order.details.order_comments}</em>"</td>
  </tr>
</table>

<div align="right"><a href="/print-order/{$order.id}.html"  class="large button blue spacer-right">Εκτύπωση Παραγγελίας</a> <a href="/orders.html"  class="large button blue spacer-right">&laquo; Πίσω στις παραγγελίες μου</a></div>
</div>
</div>
{elseif $orders}
<div class="box">
<h1 class="header">Οι παραγγελίες μου</h1>
<div class="content ">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th>Αριθμός παραγγελίας</th>
    <th>Ημερομηνία παραγγελίας</th>
    <th>Κατάσταση παραγγελίας</th>
    <th>Ποσό</th>
    <th>&nbsp;</th>
  </tr>
  {foreach from=$orders item=a}
  <tr>
    <td align="center"><a href="/orders/{$a.id}.html">{$a.id}</a></td>
    <td align="center">{$a.date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
    <td align="center">{$status_codes[$a.status]}</td>
    <td align="center" class="price">&euro;{$a.total_price_vat|formatprice:".":","}</td>
    <td><a href="/print-order/{$a.id}.html" >Εκτύπωση</a></td>
  </tr>
  {/foreach}
</table>
</div>
</div>
{else}
Δεν έχουν γίνει παραγγελίες
{/if}
{else}
{include file="modules/themes/members.tpl"}
{/if}