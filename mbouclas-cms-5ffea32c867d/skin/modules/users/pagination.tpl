{if $mode eq "search_results_admin"}
<table border=0 cellpadding=0 cellspacing="0" class="pagination">
    <tr height=20>
      <td colspan="6">{$lang.from} <font color="#FF0000">{$list.from}</font> {$lang.to} <font color="#FF0000">{$list.to}</font> {$lang.of_total} {$list.total}</td>
    </tr>
    <tr height=14>
      <td>{$lang.result_pages} : &nbsp;</td>
      {if $navigation.first}
      <td valign=middle><a href="{$MODULE_FOLDER}/search.php?search=1&page={$navigation.first}" title="{$lang.first}"><img src="{$ImagesDir}/larrow_2.gif" border="0" alt="{$lang.first}" title="{$lang.first}"></a></td>
      {/if} {if $navigation.previous}
      <td valign=middle><a href="{$MODULE_FOLDER}/search.php?search=1&page={$navigation.previous}" title="{$lang.previous}"><img src="{$ImagesDir}/larrow.gif" border="0" alt="{$lang.previous}" title="{$lang.previous}"></a></td>
      {/if} 
	  <td><table height="20"><tr>
	  {section name=p loop=$num_links} 
	  {if $num_links[p].page eq $PAGE}
      <td width=17 background="{$ImagesDir}/page.gif" align=center class="pagination"><font color="#FF0000"><b>{$num_links[p].page}</b></font></td>
      {else}
      <td width=17 background="{$ImagesDir}/page.gif" align=center><a href="{$MODULE_FOLDER}/search.php?search=1&page={$num_links[p].page}" class="pagination" title="{$lang.page} : {$num_links[p].page}">{$num_links[p].page}</a></td>
      {/if} 
	  {/section} 
	  </tr></table>
	  </td>
	  {if $navigation.next}
      <td valign=middle><a href="{$MODULE_FOLDER}/search.php?search=1&page={$navigation.next}" title="{$lang.next}"><img src="{$ImagesDir}/rarrow.gif" border="0" alt="{$lang.next}" title="{$lang.next}"></a></td>
      {/if} {if $navigation.final}
      <td valign=middle><a href="{$MODULE_FOLDER}/search.php?search=1&page={$navigation.final}" title="{$lang.final}{if $smarty.get.id}&id={$smarty.get.id}{/if}"><img src="{$ImagesDir}/rarrow_2.gif" border="0" alt="{$lang.last}" title="{$lang.last}"></a></td>
      {/if}
  </tr>
</table>
{elseif $mode eq "admin"}
<div class="pagination">
<ul>
<li><a href="{$SELF}?page={$navigation.first}" title="{$lang.first}" class="prevnext{if !$navigation.first} disablelink{/if}">&laquo; {$lang.first}</a></li>
<li><a href="{$SELF}?page={$navigation.previous}" title="{$lang.previous}" class="prevnext {if !$navigation.previous}disablelink{/if}">&laquo; {$lang.previous}</a></li>
{section name=p loop=$num_links}
<li><a href="{$SELF}?page={$num_links[p].page}{if $query}&{$query}{/if}" title="{$lang.page} : {$num_links[p].page}" {if $num_links[p].page eq $PAGE}class="currentpage"{/if}>{$num_links[p].page}</a></li>
{/section}
<li><a href="{$SELF}?page={$navigation.next}" title="{$lang.next}" class="prevnext{if !$navigation.next} disablelink{/if}">{$lang.next} &raquo;</a></li>
<li><a href="{$SELF}?page={$navigation.final}" title="{$lang.last}" class="prevnext{if !$navigation.final} disablelink{/if}">{$lang.last} &raquo;</a></li>
</ul>
</div>
{elseif $mode eq "tags"}
<div class="pagination">
<ul>
<li><a href="{$URL}/tag/{$tag}/{$navigation.first}/" title="{$lang.first}" class="prevnext{if !$navigation.first} disablelink{/if}">&laquo; {$lang.first}</a></li>
<li><a href="{$URL}/tag/{$tag}/{$navigation.previous}/" title="{$lang.previous}" class="prevnext {if !$navigation.previous}disablelink{/if}">&laquo; {$lang.previous}</a></li>
{section name=p loop=$num_links}
<li><a href="{$URL}/tag/{$tag}/{$num_links[p].page}/" title="{$lang.page} : {$num_links[p].page}" {if $num_links[p].page eq $PAGE}class="currentpage"{/if}>{$num_links[p].page}</a></li>
{/section}
<li><a href="{$URL}/tag/{$tag}/{$navigation.next}/" title="{$lang.next}" class="prevnext{if !$navigation.next} disablelink{/if}">{$lang.next} &raquo;</a></li>
<li><a href="{$URL}/tag/{$tag}/{$navigation.final}/" title="{$lang.last}" class="prevnext{if !$navigation.final} disablelink{/if}">{$lang.last} &raquo;</a></li>
</ul>
</div>
{else}
<div class="pagination">
<ul>
<li><a href="{$URL}/{$area}_{$navigation.first}.html" title="{$lang.first}" class="prevnext{if !$navigation.first} disablelink{/if}">&laquo; {$lang.first}</a></li>
<li><a href="{$URL}/{$area}_{$navigation.previous}.html" title="{$lang.previous}" class="prevnext {if !$navigation.previous}disablelink{/if}">&laquo; {$lang.previous}</a></li>
{section name=p loop=$num_links}
<li><a href="{$URL}/{$area}_{$num_links[p].page}.html" title="{$lang.page} : {$num_links[p].page}" {if $num_links[p].page eq $PAGE}class="currentpage"{/if}>{$num_links[p].page}</a></li>
{/section}
<li><a href="{$URL}/{$area}_{$navigation.next}.html" title="{$lang.next}" class="prevnext{if !$navigation.next} disablelink{/if}">{$lang.next} &raquo;</a></li>
<li><a href="{$URL}/{$area}_{$navigation.final}.html" title="{$lang.last}" class="prevnext{if !$navigation.final} disablelink{/if}">{$lang.last} &raquo;</a></li>
</ul>
</div>

 {/if}
  