<div class="col-main" style="width:850px;">	
<div id ="profileContainer" class="profileContainer">				
  <div class="account-create" >
    <div class="page-title">
       <h1>Eπεξεργασία Χρήστη</h1>
    </div>
    {if $action eq "step1"}
    <div>Subscription was successful</div>
    {else}
  
	<div class="fieldset">
		        <h2 class="legend">Personal Information</h2>
		  <div style="float:left;">      
	 <ul class="form-list">
		      <li class="fields">
				       <div class="customer-name">
				    <div class="field name-firstname fields" style="width:290px;">
				        <label class="required" for="firstname">First Name</label>

          					  <div class="fieldValue" data-class="input-text UpdateUser" data-name="user_name" data-value="{$user.user_name}" data-type="text">{$user.user_name}</div>
				    </div>
				    <div class="field name-lastname fields" style="width:290px;">
				        <label class="required" for="lastname">Last Name</label>
				        <div class="fieldValue" data-class="input-text UpdateUser" data-name="user_surname" data-value="{$user.user_surname}" data-type="text">{$user.user_surname}</div>
				    </div>
				    
				    <div class="field email fields" style="width:290px;">
				        <label class="required" for="lastname">E-mail</label>
				        <div class="fieldValue" data-class="input-text UpdateUser" data-name="email" data-value="{$user.email}" data-type="text">{$user.email}</div>
				    </div>
				     <div class="field email fields" style="width:290px;">
				        <label class="required" for="lastname">Mobile Phone</label>
					<div class="fieldValue" data-class="input-text UpdateUser" data-name="user_mobile" data-value="{$user.user_mobile}" data-type="text">{$user.user_mobile}</div>				    
				</div>
				</div>
		    </li>
		        
		           
	 </ul>
	 </div>
	 <div style="float:left; padding-left:30px;">
	  <ul class="form-list">
		      <li class="fields">
				       <div class="customer-name">
				     <div class="field email fields" style="width:290px;">
				        <label class="required" for="lastname">City</label>
           			 <div class="fieldValue" data-class="input-text UpdateUserSettings" data-name="city" data-value="{$settings_user.city}" data-type="text">{$settings_user.city}</div>
				    </div>
				    <div class="field email fields" style="width:290px;">
				        <label class="required" for="lastname">Address</label>
            <div class="fieldValue" data-class="input-text UpdateUserSettings" data-name="address" data-value="{$settings_user.address}" data-type="text">{$settings_user.address}</div>
				    </div>
				     <div class="field email fields" style="width:290px;">
				        <label class="required" for="lastname">T.k</label>
            <div class="fieldValue" data-class="input-text UpdateUserSettings" data-name="zipcode" data-value="{$settings_user.zipcode}" data-type="text">{$settings_user.zipcode}</div>
				    </div>
				    
				</div>
		    </li>
		         
		         
	 </ul>
	 </div>
	 
	</div> <!-- end class="fieldset" --><div class="seperator"></div>
		        
		            <div ></div>
		        <div class="buttons-set">
		           <div class="buttons"><a class="k-button k-button-icontext" href="#" data-edit="false" data-bind="click: editMode"><span class="k-icon k-edit"></span>Edit</a></div>
         <div class="buttons hidden"><a class="k-button k-button-icontext" href="#"  data-bind="click: close"><span class="k-icon k-close"></span>Close</a></div>
        <div class="buttons hidden"><a class="k-button k-button-icontext" href="#"  data-bind="click: save"><span class="k-icon k-save"></span>Save</a></div>
		        </div>
    {/if}
</div> <!-- end class="account-create" -->
                           
   </div>
</div> <!-- end  class="col-main" -->
<style>

span.k-tooltip {
    border-width: 0px;
    display: inline-block;
    padding: 2px 5px 1px 6px;
    position: static;
    color:red;
    background:url("/images/site/validation_advice_bg.gif") no-repeat scroll 2px 0 transparent
}

</style>


<script>
head.js('/scripts/site/UserForm.js');
head.js('/scripts/site/UserEdit.js');
</script>

{* if $ID}
{assign var="commentitemid" value=$user.id}
{assign var="commentmodule" value="users"}
{assign var="commenttype" value="profile"}
{assign var="commentstatus" value="1"}
{assign var="comments" value=$user.comments}

{include file="modules/users/UserComment.tpl"}
{/if *}
