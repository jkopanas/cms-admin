<div class="content-middle-full">
<h1 class="cpanel-head">{$user.uname}</h1>
<div class="seperator"></div>
<img src="{$user.user_image}" align="left" class="spacer-right" /><div class="float-left">
<div class="spacer-bottom">
<ul>
<li><strong>Λίγα λόγια γα εμένα:</strong>  {$user.profile.profile.about}</li>
<li><strong>Μ'αρέσει να μαγειρεύω:</strong> {$user.profile.profile.cook}</li>
<li><strong>Οι θρίαμβοι μου:</strong> {$user.profile.profile.triumphs}</li>
<li><strong>Οι ήττες μου:</strong> {$user.profile.profile.tragedies}</li>
</ul>
</div>
</div>
<div class="clear"></div>
<h1 class="cpanel-head">Το βιβλίο συνταγών μου</h1> 
{if $favorites}
{include file="modules/themes/recipe_box_table.tpl" mode=public}
{else}
Ο χρήστης {$user.uname} δεν έχει συνταγές στο συνταγολόγιο του
{/if}
</div><!-- END MIDDLE -->
<div class="recipe-far-right">

{include file="modules/themes/ads.tpl" place="right_sidebar"}
</div><!-- END RIGHT -->