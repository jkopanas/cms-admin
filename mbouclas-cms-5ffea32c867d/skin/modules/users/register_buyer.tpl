<div>
<form id="userForm" action="" method="POST">
		<div class="DivTable" style="width:450px;">
            <input type="hidden" id="action" value="save_buyer.html" />
			<div class="HeadRow">{$lang.newbuyertitle}</div>
			<div class="DivRow">
				<div class="DivTd" style="font-size:13px;">{$lang.fillregisterform}<a href="/register_seller.html" style="font-size:13px;">{$lang.clickhereseller}</a></div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.uname} : </div>
				<div class="DivTd FormInput"><input type="text" maxlength="255" name="uname" value="" class="effect userData" style="width:207px;"> <font color="red">*</font></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.email} : </div>
				<div class="DivTd FormInput"><input type="text" maxlength="255" name="email" value="" class="effect userData" style="width:207px;"> <font color="red">*</font></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.password} :</div>
				<div class="DivTd FormInput"><input type="password" id="password" maxlength="10" name="pass" class="effect userData" style="width:207px;"> <font color="red">*</font></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.confirm_pass} :</div>
				<div class="DivTd FormInput"><input type="password" maxlength="10" name="confirm_password" class="effect" style="width:207px;"> <font color="red">*</font></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.first_name} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="user_name" value="" class="effect userData" style="width:207px;"> <font color="red">*</font></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.last_name} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="user_surname" value="" class="effect userData" style="width:207px;"> <font color="red">*</font></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.country} :</div>
				<div class="DivTd FormInput">
                <select class="effect userData" name="user_location">
                    {foreach from=$countries item="a"}
                    <option value="{$a.code}">{$a.country}</option>
                    {/foreach}
                </select>
            </div></div>
			<div class="DivRow">
				<div style="text-align:center;"><input type="checkbox" name="tosAgree" id="tosAgree" value="Y"><label for="tosAgree">{$lang.ihavereadthe} <a href='javascript:showTermsPop();' class='link'>{$lang.useragreement}</a> {$lang.andacceptit}</label></div>
			</div>
			<div class="DivRow">
				<div style="text-align:center;"><input type="submit" name="Enter" value=" {$lang.register} " class="baseeffect"></div>
			</div>
		</div>

</form>
<script src="/scripts/member.js"></script>
</div>