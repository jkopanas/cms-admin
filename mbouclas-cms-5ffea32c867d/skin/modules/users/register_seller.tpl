<div>
<form id="userForm" action="" method="POST">
		<div class="DivTable" style="width:450px;">
            <input type="hidden" id="action" value="save_seller.html" />
            <input type="hidden" name="id" value="{$ID}" />
			<div class="HeadRow">{$lang.newsellertitle}</div>
			<div class="DivRow">
				<div class="DivTd" style="font-size:13px;">{$lang.fillregisterform}<a href="/register_seller.html" style="font-size:13px;">{$lang.clickhereseller}</a></div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.uname} : </div>
				<div class="DivTd FormInput"><input type="text" maxlength="255" {if $UNAME} disabled="disabled" {/if} name="uname" value="{$UNAME}" class="effect userData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.email} : </div>
				<div class="DivTd FormInput"><input type="text" maxlength="255" name="email" {if $UNAME} disabled="disabled" {/if} value="{$EMAIL}" class="effect userData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
            {if !$ID}
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.password} :</div>
				<div class="DivTd FormInput"><input type="password" id="password" maxlength="10" name="pass" class="effect userData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.confirm_pass} :</div>
				<div class="DivTd FormInput"><input type="password" maxlength="10" name="confirm_password" class="effect" style="width:207px;" /> <font color="red">*</font></div>
			</div>
            {/if}
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.first_name} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="user_name" value="{$USER_NAME}" class="effect userData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.last_name} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="user_surname" value="{$USER_SURNAME}" class="effect userData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
            {*
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.country} :</div>
				<div class="DivTd FormInput">
                <select class="effect userData" name="user_location">
                    {foreach from=$countries item="a"}
                    <option value="{$a.code}">{$a.country}</option>
                    {/foreach}
                </select>
                </div>
            </div>
			*}
            <div class="DivRow">
            
                Receive purchase offers from buyers? :
                <input id="dealsY" class="sellerData" type="radio" checked="" value="1" name="get_deals" />
                <label for="dealsY">Yes</label>
                <input id="dealsN" class="sellerData" type="radio" value="0" name="get_deals"/>
                <label for="dealsN">No</label>
            </div>
            
            <div class="DivRow">
            
                Enable your store on galoo.com? :
                <input id="storeY" class="sellerData" type="radio" checked="" value="1" name="store_enabled" />
                <label for="storeY">Yes</label>
                <input id="storeN" class="sellerData" type="radio" value="0" name="store_enabled"/>
                <label for="storeN">No</label>
            </div>
            
            <div class="HeadRow">{$lang.addressinfo}</div>
            
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.lbl_company_name} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="company_name" value="" class="effect sellerData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
            
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.contact_person} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="contact_person" value="" class="effect sellerData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.lbl_address} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="address" value="" class="effect sellerData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.lbl_city} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="city" value="" class="effect sellerData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.lbl_region} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="region" value="" class="effect sellerData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.country} :</div>
				<div class="DivTd FormInput">
                <select class="effect sellerData" name="country">
                    {foreach from=$countries item="a"}
                    <option value="{$a.code}">{$a.country}</option>
                    {/foreach}
                </select>
                </div>
            </div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.lbl_postal_code} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="postal_code" value="" class="effect sellerData" style="width:207px;" /> </div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.phone} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="phone" value="" class="effect sellerData" style="width:207px;" /> <font color="red">*</font></div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.lbl_mobileNumber} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="mobile" value="" class="effect sellerData" style="width:207px;" /> </div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.lbl_vat} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="vat" value="" class="effect sellerData" style="width:207px;" /></div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.url} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="url" value="" class="effect sellerData" style="width:207px;" /> </div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.pref_lang} :</div>
                <div class="DivTd FormInput">
                    <select class="effect" name="pref_language" id="languageId" class="sellerData"><option value="de">Deutsch</option><option selected="" value="en">English</option><option value="es">Español</option><option value="fr">Français</option><option value="it">Italiano</option><option value="gr">Ελληνικά</option><option value="1008">Pусский</option><option value="1010">Română</option><option value="1011">Shqip</option><option value="1002">Srpski</option><option value="1003">Türkçe</option><option value="1009">български</option></select>
                </div>
            </div>
            <div class="HeadRow">{$lang.otherinfo}</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.seller_code} :</div>
				<div class="DivTd FormInput"><input type="text" maxlength="50" name="priority_code" value="" class="effect sellerData" style="width:207px;" /> 
                    <br />
                    <font size="1">{$lang.seller_code_note}</font>
                </div>
			 
                
            </div>
            
            <div class="DivRow">
				<div style="text-align:center;"><input type="checkbox" name="tosAgree" id="tosAgree" value="Y" /><label for="tosAgree">{$lang.ihavereadthe} <a href='javascript:showTermsPop();' class='link'>{$lang.useragreement}</a> {$lang.andacceptit}</label></div>
			</div>
			<div class="DivRow">
				<div style="text-align:center;"><input type="submit" name="Enter" value=" {$lang.register} " class="baseeffect" /></div>
			</div>
		</div>

</form>
<script src="/scripts/member.js"></script>
</div>