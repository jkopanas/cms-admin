                            <h4>{if $user.name}{$user.name} {$user.surname}{else}{$lang.lbl_audienceMember} #{$user.id}{/if}</h4>
                            <hr />
                            <ul class="profile-info">
                                {if $user.email}<li class="email"><span>email</span>{$user.email}</li>{/if}
                                {if $user.phone}<li class="phone"><span>{$lang.phone}</span>{$user.phone}</li>{/if}
                                {if $user.mobile}<li class="mobile"><span>{$lang.lbl_mobileNumber}</span>{$user.mobile}</li>{/if}
                            </ul>
                            <h4>{$lang.lbl_audienceDetails}</h4>
                            <hr />
                            <ul class="profile-info">
                                <li class="calendar-day"><span>{$lang.added}</span>{$user.date_added|date_format:"%d/%m/%Y @ %H:%M:%S"}</li>
                            </ul>

