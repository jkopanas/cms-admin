<!DOCTYPE html>
<html>
<head>
<title>{if $item.title}{foreach from=$nav item=a name=b} {$a.category} - {/foreach} {$item.title} {elseif $nav_area eq "contact"}{$lang.contact} {elseif $cat.category} {$cat.category} {elseif $type eq "tag"}{$tag.tag} {else} {$lang.html_SiteTitleMoto}{/if}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" media="screen" href="/css/userCp/reset.css" />
<link rel="stylesheet" media="screen" href="/css/userCp/grid.css" />
<link rel="stylesheet" media="screen" href="/css/userCp/style.css" />
<link rel="stylesheet" media="screen" href="/css/userCp/messages.css" />
<link rel="stylesheet" media="screen" href="/css/userCp/forms.css" />
<link rel="stylesheet" media="screen" href="/css/userCp/tables.css" />
<link rel="stylesheet" href="/css/jquery-ui-1.8.10.custom.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="/css/jquery.multiselect1.css" />
<link rel="stylesheet" type="text/css" href="/css/jquery.multiselect.filter.css" />
<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="/css/ie.css" />
<![endif]-->

<script src="/scripts/jquery-1.5.min.js" type="text/javascript"></script>
<script src="/scripts/jquery-ui-1.8.10.custom.min.js"></script>
<script src="/scripts/common.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="/scripts/html5.js"></script>
<script type="text/javascript" src="/scripts/PIE.js"></script>
<script type="text/javascript" src="/scripts/IE9.js"></script>
<script type="text/javascript" src="/scripts/ie.js"></script>
<![endif]-->
</head>

<body>
<input type="hidden" name="loaded_language" value="{$FRONT_LANG}" />
<div id="wrapper">
{if $ID}
<header>
{include file="modules/users/menu.tpl"}
</header>
<section>
            <div class="container_8 clearfix">


                
{include file=$hooks[$action].tpl}
                <!-- Main Section -->

                

                <!-- Main Section End -->

            </div>
            <div id="push"></div>
        </section>



{else}
{include file="modules/themes/members.tpl"}
{/if}
</div><!-- END WRAPPER -->
{if $webmasterMode}
<div id="tmp" class="hidden" style="display:none"><div id="tmpContent"></div></div>
<link href="/css/colorbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/scripts/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/scripts/webmasterMode.js"></script>
{/if}
<script src="/scripts/jquery.bgiframe.min.js" type="text/javascript"></script> 
<script src="/scripts/jquery.multiSelect.js"></script>
<script src="/scripts/jquery.tools.min.js"></script>
<script type="text/javascript" src="/scripts/global.js"></script>

</body>
</html>