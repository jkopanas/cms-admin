    <form name="F" id="frmLogin" action="" method="POST">
		<input type="hidden" name="return"			value="/member_verify_email.php">
		<input type="hidden" name="userProductId"	value="">
		<input type="hidden" name="muserId"			value="">
		<input type="hidden" name="productId"		value="">

		<div class="DivTable" style="width:450px;">
			<div class="HeadRow" style="text-align:center;">Είσοδος </div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.username} : </div>
				<div class="DivTd FormInput"><input name="username" id="username"  type="text"/></div>
			</div>
			<div class="DivRow">
				<div class="DivTd FormLabel">{$lang.password} : </div>
				<div class="DivTd FormInput"><input name="password" type="password"/></div>
			</div>
            <div class="DivRow">
				<div class="DivTd FormLabel">{$lang.remember_me} : </div>
				<div class="DivTd FormInput"><input type="checkbox" name="autologin" value="1" /></div>
			</div>
			<div class="DivRow">
				<div style="text-align:center;"><input type="submit" id="btnLogin" name="Enter" value=" {$lang.login} " class="baseeffect"></div>
                
                {foreach from=$smarty.post item=a key=k}
                {if $k ne "password" AND $k ne "username"}
                <input name="{$k}" value="{$a}" type="hidden">
                {/if}
    			{/foreach}
			</div>
						<div class="DivRow">
				<div style="text-align:center;">
					<b> | 
					<a href="http://www.galoo.com/member_register.htm?return=%2Fmember_verify_email.php" class="link">{$lang.register}</a> | 
					<a href="{$URL}/lost-password.html">{$lang.lost_password} (password)</a> | 
					<a href="http://www.galoo.com/member_verify_email.php" class="link">Παρακαλούμε επιβεβαιώστε την ηλεκτρονική σας διεύθυνση</a> |
					</b>
				</div>
			</div>
		</div>
    </form>