
<div class="col-main" style="width:850px;">					
                                        <div class="account-login">
    <div class="page-title">
        <h1>Login or Create Account</h1>
    </div>
        <form id="login_form" name="login_form" method="post" action="{$SELF}">
        <div class="col2-set">
            <div class="col-1 new-users">
                <div class="content" style="height:210px;" >
                    <h2>New Customers</h2>
                    <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                </div>
            </div>
            <div class="col-2 registered-users">
                <div class="content" style="height:210px;" >
                
                    <h2>Registered Customers</h2>
                    <p>If you have an account with us, please log in.</p>
                {if $LOGINMESSAGE} <div class="spacer-bottom error" style="color:red;">Error:{$LOGINMESSAGE}</div>{/if}
                    <ul class="form-list">
                        <li>
                            <label class="required" for="email"><em>*</em>{$lang.username}</label><br/>
                            <div class="input-box">
                                <input type="text"  class="input-text" name="username" id="username" ><div style="" id="advice-required-entry-email" class="validation-advice">This is a required field.</div>
                            </div>
                        </li>
                        <li>
                            <label class="required" for="pass"><em>*</em>{$lang.password}</label><br/>
                            <div class="input-box">
                                <input name="password" type="password" class="input-text  " ><div style="" id="advice-required-entry-pass" class="validation-advice">This is a required field.</div>
                            </div>
                        </li>
                                            </ul>

                    <p class="required">* Required Fields</p>
                </div>
            </div>
        </div>
        <div class="separator"></div>
        <div class="col2-set">
            <div class="col-1 new-users"> 
                <div class="buttons-set">
                    
                	<a href="/signup.html" title="{$lang.become_a_member}"  class=" validation-passed"><button class="button validation-passed" title="Create an Account" type="button"><span><span>{$lang.create_new_account}</span></span></button></a>
                </div>
            </div>
            <div class="col-2 registered-users">
                <div class="buttons-set">
                    <a class="f-left" href="/{$FRONT_LANG}/lost-password.html">{$lang.lost_password} (password)</a>
                    <button id="send2" name="Submit" title="Login" class="button validation-passed submit" type="submit"><span><span>{$lang.login}</span></span></button>
                </div>
            </div>
        </div>
            </form>
    
</div>
                </div>


