{if !$items}
				
	<div class="page-title category-title">
    	<h1>Shopping</h1>
	</div>
<div class="cart-empty">
        <p>You have no items in your shopping cart.</p>
    <p>Click <a href="/{$FRONT_LANG}/index.html">here</a> to continue shopping.</p>
</div>

{else}


<div class="cart">
    <div class="page-title title-buttons category-title">
        <h1>Shopping</h1>
  
    </div>
    <div class="clear seperator"></div>
<ul id="mainNav" class="threeStep">
		<li class="current"><a title="Βήμα 1: Καλάθι αγορών" href="/{$FRONT_LANG}/cart.html"><em>Βήμα 1: Καλάθι αγορών</em><span>δείτε συνοπτικά την παραγγελία σας</span></a></li>
		<li><a title="Βήμα 2: Πληρωμή"><em>Βήμα 2: Πληρωμή</em><span>Επιλογή τρόπου πληρωμής</span></a></li>
        <li class="mainNavNoBg"><a title=""><em>Βήμα 3: Ολοκλήρωση</em><span>Τέλος παραγγελίας</span></a></li>
	</ul>
<div class="seperator"></div>
<div class="seperator"></div>
	<form  id="shopping-cart-table" action="/{$FRONT_LANG}/checkout.html" method="POST">
   	<table id="shopping-cart-table" class="data-table cart-table">
                <colgroup><col width="1">
                <col>
                <col width="1">
                                        <col width="1">
                                        <col width="1">
                            <col width="1">
                                        <col width="1">

                            </colgroup><thead>
                    <tr class="first last">
                        <th rowspan="1">&nbsp;</th>
                        <th rowspan="1"><span class="nobr">Product Name</span></th>
                        <th rowspan="1"></th>
                                                <th class="a-center" colspan="1"><span class="nobr">Unit Price</span></th>
                        <th rowspan="1" class="a-center">Qty</th>
                        <th class="a-center" colspan="1">Subtotal</th>
                        <th rowspan="1" class="a-center">&nbsp;</th>
                    </tr>
                                    </thead>
                <tfoot>
                    <tr class="first last">
                        <td colspan="50" class="a-right last">
                                                     <a href="/{$FRONT_LANG}/index.html" title="Continue Shopping" class="button btn-continue float-left" ><span><span>Continue Shopping</span></span></a>
                                                     <a href="/empty_cart.html"  class="button btn-continue spacer-right float-right" rel="{$a.id}"><span><span>Αφαίρεση όλων</span></span></a> 
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                  {foreach from=$items item=a key=k name=b}
  					<tr class="{if $smarty.foreach.b.first}first{/if} {if $smarty.foreach.b.last}last{/if} odd">
    					<td><a href="/product/{$k}/{$a.permalink}.html" title="{$a.title}" class="product-image"><img src="{$a.image_full.thumb}" width="75" height="75" alt="{$a.title}"></a></td>
    					<td><h2><a href="/product/{$k}/{$a.permalink}.html">{$a.title}</a></h2></td>				
    					<td class="a-center"><a href="#" class="edit_quantity" data-id="{$k}" data-price="{$a.price}" title="Edit item parameters">Edit</a></td>
    					<td class="a-right"><span class="cart-price" id="total-{$k}"><span class="price uqPrice" >&euro;{$a.price|number_format:2:".":","}</span></span></td>
    					<td class="a-center"><input type="text" name="quantity-{$k}" id="quantity-{$k}" value="{$a.quantity|default:0}" data-price="{$a.price|number_format:2:".":","}"  size="4" title="Qty" class="input-text qty disable" maxlength="12" alt="{$k}|{$a.price}"  disabled /></td>
     					<td class="a-right"><span class="cart-price"><span class="price totalPrice" id="allTotal-{$k}" data-total="{$a.total|number_format:2:".":","}">&euro;{$a.total|number_format:2:".":","}</span></span></td>
    					<td align="center last"><a href="#" class="remove-item btn-remove btn-remove2" rel="{$k}"><img src="/images/delete.jpg" width="16" height="16" /></a></td>
  					</tr>
 				  {/foreach}
                </tbody>
            </table>
	</form>
    <div class="seperator"></div>
    <div class="cart-collaterals float-left">
        <div class="totals">
                <table id="shopping-cart-totals-table">
        <colgroup><col>
        <col width="1">
        </colgroup><tfoot>
            <tr>
    <td style="" class="a-right" colspan="1">
        <strong>Γενικό Σύνολο :</strong>
    </td>
    <td style="" class="a-right">
        <strong><span class="price" id="totalAll">{$total_price|number_format:2:".":","}  &euro;</span></strong>
    </td>
</tr>
        </tfoot>
        <tbody>
            <tr>
    <td style="" class="a-right" colspan="1">
        Μερικό Σύνολο :    </td>
    <td style="" class="a-right">
        <span class="price" id="t">{$total_price_no_vat|number_format:2:".":","} &euro;</span></td>
</tr>
<tr>
    <td style="" class="a-right" colspan="1">
        Αξία Φ.Π.Α :    </td>
    <td style="" class="a-right">
    	<input type="hidden" id="vat"  name="vat" value="{$vat|default:0}" />
        <span class="price">{$vat_price|number_format:2:".":","}</span>    </td>
</tr>
        </tbody>
    </table>
			<ul class="checkout-types float-left">
				<li>
					<a href="#" rel="{$a.id}" class="button btn-proceed-checkout btn-checkout">
						<span><span>Proceed to Checkout</span> </span>
					</a>
				</li>
			</ul>

		</div>
    </div>
</div>
{/if}
<script>head.js("/scripts/site/eshop.js");</script>

