<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$SITE_NAME}</title>
<link href="/css/plugins/facebook/styleNew.css" rel="stylesheet" type="text/css" />
<link href="/scripts/slider/mcmsSlider.css" rel="stylesheet" type="text/css" />
<link href="/scripts/kendo/styles/kendo.common.min.css" rel="stylesheet"/>
<link href="/scripts/kendo/styles/kendo.metro.min.css" rel="stylesheet"/>

<script type="text/javascript" src="/scripts/head.load.min.js"></script>
<script type="text/javascript">
var mcms;
head.js('/scripts/jquery-1.7.1.min.js');
head.js('/scripts/mcmsFront.js');
head.js('/scripts/mcmsMenu.js');
head.js('/scripts/kendo/js/kendo.core.min.js');
head.js('/scripts/kendo/js/kendo.validator.min.js');
head.js('/scripts/kendo/js/kendo.data.min.js');
head.js('/scripts/kendo/js/kendo.binder.min.js');
head.js('/scripts/kendo/js/kendo.window.min.js');
head.js('/scripts/kendo/js/kendo.upload.min.js');
head.js('/scripts/kendo/js/kendo.list.min.js');
head.js('/scripts/kendo/js/kendo.listview.min.js');
head.js('/scripts/kendo/js/kendo.pager.min.js');

head.ready(function(){
mcms =  $(document).mCms({ lang:$('input[name=loaded_language]').val()}).data().mcms;//Initialize
$.lang = mcms.getLang();
});
</script>
</head>

<body>
<div id="fb-root"></div>

<input type="hidden" value="{$FRONT_LANG}" name="loaded_language" />
<div class="wrapper">
<div id="page" class="page">
<div class="header-container">
 <div class="header">
 <h1 class="logo"><strong>Magento Commerce</strong><a href="/{$FRONT_LANG}/index.html" title="{$SITE_NAME}" class="logo"><img src="/images/site/logo.png" alt="{$SITE_NAME}" width="144" height="79" /></a></h1>
 <div class="language-switcher">{include file="langSelect.tpl"}</div>
</div>
</div><!-- END HEADER -->


<div class="main-container {if $area eq 'home' OR ($item AND $current_module.name eq "products")}col1-layout{elseif $item AND $current_module.name eq "content"}col2-left-layout{/if}">

<div class="main">

 <div class="results" data-for="title"></div>

<a href="#" class="invite">inviteFriends</a>

<a href="#"  id="drimoi" data-bind="click: doActions" data-href="/facebook/ajax/en/content/27/index.html" data-method="actionUrl" title="Προτείνουμε">Drimoi</a>
<a href="#"  id="link" data-bind="click: doActions" data-href="/facebook/ajax/en/content/22/index.html" data-method="actionUrl" title="Προτείνουμε">Προτείνουμε</a>
<a href="#"  id="home" data-bind="click: doActions" data-href="/facebook/ajax/" data-method="actionUrl" title="Προτείνουμε">Home</a>

<div id="user"></div>
 
	
<div id="load_template">
</div>
	
<div id="results" ></div>

<div class="seperator"></div>
</div><!-- END MAIN -->
</div><!-- END MAIN CONTAINER -->


</div><!-- END PAGE -->
</div><!-- END WRAP -->
<script>


head.js('/scripts/plugins/facebook/ControllerPlugin.js');
head.js('/scripts/plugins/facebook/fbControl.js');
head.js('/scripts/plugins/facebook/fb.plugin.js');
head.js('/scripts/plugins/facebook/facebook.js');

head.js('/scripts/site/kendoMix.js');
head.js('/scripts/autoComplete.js');
head.js('/scripts/site/global.js');
</script>

</body>
</html>
