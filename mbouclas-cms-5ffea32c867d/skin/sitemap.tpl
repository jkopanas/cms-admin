<div class="navpath">
<a href="index.php" title="{$lang.home}" class="navpath">{$lang.home}</a> :: {$lang.sitemap}
</div>
<br><br>
{capture name=dialog}
<a href="index.php" title="{$lang.home}">{$lang.home}</a>
<br>
<a href="sitemap.php" title="{$lang.sitemap}">{$lang.sitemap}</a>
<br>
<a href="search.php" title="{$lang.search}">{$lang.search}</a>
<br>
<br>
{if $menus}
{section name=b loop=$menus}
{$menus[b].name}<br>
{foreach name=outer item=sub from=$menus[b].content}
 -- <a href="content.php?id={$sub.id}" title="{$sub.title}">{$sub.title}</a><br>
{/foreach}
{/section}
<br>
{/if}
{if $products}
{$lang.products}<br>
{section name=c loop=$products}
- {$products[c].cat}<br>
{foreach name=outer item=sub from=$products[c].product}
 -- <a href="product.php?id={$sub.id}" title="{$sub.title}">[ #{$sub.sku} ] {$sub.title}</a><br>
{/foreach}
{/section}
<br>
{/if}
{if $all_categories}
{$lang.product} {$lang.categories}<br>
{section name=c loop=$all_categories}
-- <a href="cat.php?cat={$all_categories[c].categoryid}" title="{$all_categories[c].category}">{$all_categories[c].category}</a><br>
{/section}
{/if}
{if $news_items}
<br>
{$lang.news}<br>
{section name=l loop=$news_items}
-- <a href="news.php?id={$news_items[l].id}" title="{$news_items[l].title}">{$news_items[l].title}</a><br>
{/section}
{/if}
{/capture}{include file="dialog.tpl" content=$smarty.capture.dialog title=$lang.sitemap extra="width=100%"}
