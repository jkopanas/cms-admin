<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">

   <url>
      <loc>{$URL}/</loc>
      <lastmod>{$smarty.now|date_format:"%Y-%m-%d"}</lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.9</priority>
   </url>
{foreach from=$main_menu item=a}
   <url>
      <loc>{$URL}/content/{$a.alias}.html</loc>
      <lastmod>{$a.date_added|date_format:"%Y-%m-%d"}</lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.8</priority>
   </url>
{/foreach}

{foreach from=$items item=a}
   <url>
      <loc>{$URL}/pages/{$a.permalink|default:$a.id}.html</loc>
      <lastmod>{$a.date_modified|date_format:"%Y-%m-%d"}</lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.8</priority>
   </url>
{/foreach}
{foreach from=$tags item=a}
   <url>
      <loc>{$URL}/clients/{$a.permalink}.html</loc>
      <lastmod>{$smarty.now|date_format:"%Y-%m-%d"}</lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.8</priority>
   </url>
{/foreach}
</urlset>