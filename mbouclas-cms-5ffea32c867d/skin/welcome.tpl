
<div id="slider" class="camera_wrap camera_azure_skin">
	<div data-thumb="/media_files/images/gallery_1/slide1.jpg" data-src="/images/slider/1.jpg" data-link="#">
           <div class="camera_caption fadeFromLeft">
           {$a.item.description|strip_tags|truncate:200}
           </div>
        </div><!-- END SLIDE -->
	<div data-thumb="{$a.item.image_full.thumb}" data-src="/images/slider/2.jpg" data-link="#">
           <div class="camera_caption fadeFromLeft">
           {$a.item.description|strip_tags|truncate:200}
           </div>
        </div><!-- END SLIDE -->
	<div data-thumb="{$a.item.image_full.thumb}" data-src="/images/slider/3.jpg" data-link="#">
           <div class="camera_caption fadeFromLeft">
           {$a.item.description|strip_tags|truncate:200}
           </div>
        </div><!-- END SLIDE -->
</div><!-- END SLIDER -->
<div class="seperator"></div>
<div class="page-title category-title">
{*
<h1>{$item.title}</h1>
</div>
{$item.description}
{$item.description_long}
*}
{capture name=mainCol}
{foreach from=$layout.C item=a name=b}	
{include file=$a.template}
<div class="seperator"></div>
{/foreach}


{/capture}{include file="modules/themes/mainCol.tpl" spacer="right" content=$smarty.capture.mainCol}
<script>
head.js("/scripts/slider/mcmsSlider.min.js");
head.js("/scripts/slider/jquery.easing.1.3.js");
head.js("/scripts/site/welcome.js");
</script>