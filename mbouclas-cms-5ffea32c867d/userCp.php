<?php
include("init.php");

if (is_numeric(ID)) {
	$user = new user();
	if ($_GET['action']) {
		include_once('hooks/users/'.$_GET['action'].".php");
		$smarty->assign("hooks",$hooks);
	}//END HOOKS
	else {
		$include_file = "modules/users/welcome.tpl";//DEFAULT DASHBOARD
	}//DEFAULT DASHBOARD
	$smarty->assign("action",$_GET['action']);
	$smarty->assign("include_file",$include_file);//assigned template variable include_file
}//END LOGED IN
else {
	$smarty->assign("include_file","modules/users/userLoginForm.tpl");//assigned template variable include_file
}
$smarty->display("modules/users/userCp.tpl");//Display the home.tpl template
?>